
------=_Part_25335_15210728.1453310732754
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

North Carolina 811

EMERGENCY

Ticket : C160201101 Date: 01/20/16 Time: 12:20 Oper: LJS Chan:999
Old Tkt: C160201101 Date: 01/20/16 Time: 12:24 Oper: LJS Rev :00C

State: NC Cnty: WAKE Place: RALEIGH In/Out: B
Subdivision: NO

Address : 4720
Street  : COURTNEY LN   Intersection: N
Cross 1 : UNKNOWN
Location: LOCATE BY 11:59 PM ON 01/25/2016
LOCATE THE ENTIRE FRONT PROPERTY

CREW ON SITE: NOW
:
Grids   : 3550A7834C   3550A7834D   3550B7834C   3550B7834D

Work type:REPAIR DAMAGED CABLE
Work date:01/20/16  Time: 12:20  Hours notice: 0/0  Priority: EMER
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: Y
Duration: 2 DAYS  Done for: AT&T / ANSCO

Company : ANSCO & ASSOCIATES, LLC  Type: CONT
Co addr : 226 HWY 70 E
City    : GARNER State: NC Zip: 27529
Caller  : DONNA MCLAMB Phone: 919-779-4411
Contact : EO HORTON Phone: 919-427-6808
BestTime:
Email   : donna.mclamb@anscollc.com

Submitted date: 01/20/16 Time: 12:24
Members: ATT322*       CLS01  COR01  CPL10* CSR02  CVI04* PSG01
------=_Part_25335_15210728.1453310732754--