
------=_Part_27643_33327098.1453397383096
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

North Carolina 811

Ticket : C160211146 Date: 01/21/16 Time: 12:24 Oper: STEVELSKI Chan:WEB
Old Tkt: C160211146 Date: 01/21/16 Time: 12:25 Oper: STEVELSKI Rev :00C

State: NC Cnty: WAKE Place: RALEIGH In/Out: B
Subdivision:

Address : 903
Street  : FIFTH AVE   Intersection: N
Location: LOCATE BY 11:59 PM ON 01/26/2016
LOCATE AND MARK ALL UTILITIES FROM ROAD TO FIRST SEWER CLEAN OUT AND UP TO THE
PROPERTY LINE OF HOMES/PROPERTIES.  DUE TO WEATHER CONDITIONS, PLEASE FLAG ALL
MARKED LOCATIONS:


903 FIFTH AVE

PLEASE MARK THE ENTIRE PROPERTY OF 903 FIFTH AVE FOR BORE ACROSS PROPERTY TO
REAR EASEMENT.


MARK ALL UTILITIES WITHIN A 50 FT RADIUS OF ANY EXISTING POWER/UTILITY EQUIPMENT
ON THESE PROPERTIES

MARK ALL HARD SURFACES, SIDEWALKS, DRIVEWAYS AND ROADWAYS

PLS REMOVE ALL DEBRIS AND LEAVES BEFORE MARKING
:
Grids   : 3542B7837C   3542B7837D   3542C7837C   3542C7837D

Work type:INSTALL UNDG CABLE
Work date:01/27/16  Time: 00:00  Hours notice: 72/72  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: Y  Railroad: N     Emergency: N
Duration: 2 WEEKS  Done for: AT&T/ANSCO

Company : MALIBU UTILITIES  Type: CONT
Co addr : 2020 GENERAL BOOTH BLVD, SUIT 200
City    : VIRGINIA BEACH State: VA Zip: 23454
Caller  : STEVEN LEPKOWSKI Phone: 757-581-3273
Contact : PAT O'BRIEN Phone: 404-775-0720
BestTime:
Cellular: 757-581-3273
Email   : malibutilities@aol.com

Remarks : JOB NAME: 52D06107N

NOTE THAT THIS JOB IS NOT A PART OF AT&T PROJECT.

LOCATE AND MARK ALL UTILITIES FROM ROAD TO FIRST SEWER CLEAN OUT AND UP TO THE
PROPERTY LINE OF HOMES/PROPERTIES.  DUE TO WEATHER CONDITIONS, PLEASE FLAG ALL
MARKED LOCATIONS:


903 FIFTH AVE

PLEASE MARK THE ENTIRE PROPERTY OF 903 FIFTH AVE FOR BORE ACROSS PROPERTY TO
REAR EASEMENT.


MARK ALL UTILITIES WITHIN A 50 FT RADIUS OF ANY EXISTING POWER/UTILITY EQUIPMENT
ON THESE PROPERTIES

MARK ALL HARD SURFACES, SIDEWALKS, DRIVEWAYS AND ROADWAYS

PLS REMOVE ALL DEBRIS AND LEAVES BEFORE MARKING
:
Submitted date: 01/21/16 Time: 12:25
Members: ATT322*       CLS01  COR01  CPL10* CSR02  CVI04* DET01* DOT05  FSC01
Members: PSG01
------=_Part_27643_33327098.1453397383096--