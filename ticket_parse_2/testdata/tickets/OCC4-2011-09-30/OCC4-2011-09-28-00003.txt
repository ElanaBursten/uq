

CGV466 00003 VUPSa 09/28/11 14:45:48 A127100481-00A          RETRANSMIT

Ticket No:  A127100481-00A                        NEW  GRID EMER LREQ RSND
Transmit        Date: 09/28/11   Time: 02:45 PM   Op: 1GTF
Call            Date: 09/28/11   Time: 09:22 AM
Due By          Date: 09/28/11   Time: 12:22 PM
Update By       Date:            Time:
Expires         Date:            Time:
Old Tkt No: A127100481
Original Call   Date: 09/28/11   Time: 09:22 AM   Op: 1GTF

City/Co:CLIFTON FORGE CITY    Place:                                    State:VA
Address:                      Street: FIFTH ST
Cross 1:     ALLEGHANY ST

Type of Work:   WATER - FIRE HYDRANT REPAIR OR REPLACE
Work Done For:  TOWN OF CLIFTON FORGE
Excavation area:CREW IS ON SITE--- 20FT RADIUS AROUND THE FIRE HYDRANT WHICH IS
                LOCATED AT THE ABOVE INTERSECTION.
Excavation Map Link: http://newtina.vups.org/newtinweb/map_tkt.nap?Operation=MAPTKT&TRG=A12710048100A&OPR=OJG71xsgPE7xcLxgP

Instructions:

Whitelined: N   Blasting: N   Boring: N

Company:        CLIFTON FORGE PUBLIC WORKS                Type: UTIL
Co. Address:    520 HOWARD ST  First Time: N
City:           CLIFTON FORGE  State:VA  Zip:24422
Company Phone:  540-863-2517
Contact Name:   DERRICK HATCHER             Contact Phone:540-863-2517
Contact Fax:    540-863-2541
Field Contact:  DERRICK HATCHER
Fld. Contact Phone:540-960-0409

Mapbook:
Grids:    3748A7950C-43  3748A7950C-44  3748B7950C-03  3748B7950C-04

Members:
CGV466 = COLUMBIA GAS OF VIRGINIA (CGV) CTF440 = TOWN OF CLIFTON FORGE (CTF)
DOM086 = DOMINION VIRGINIA POWER (DOM)  NTL105 = NTELOS (NTL)
STC105 = SHENANDOAH TELECOMMUNICAT(STC)

Seq No:   3 A

[Ticket (re)sent at your request]
