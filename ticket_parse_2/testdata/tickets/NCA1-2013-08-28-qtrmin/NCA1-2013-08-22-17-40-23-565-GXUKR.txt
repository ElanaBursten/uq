
COMSJ2 00137 USAN 08/22/13 14:37:46 0206910 NORMAL NOTICE EXTENSION

Message Number: 0206910 Received by USAN at 14:37 on 08/22/13 by BMD

Work Begins:    06/06/13 at 07:00   Notice: 028 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 09/23/13 at 23:59   Update By: 09/19/13 at 16:59

Caller:         CHRIS REILLY             
Company:        WOODS CONSTRUCTION                 
Address:        2718 PACHECO ST, SAN FRANCISCO          
City:           SAN FRANCISCO                 State: CA Zip: 94116
Business Tel:   415-740-0337                  Fax: 415-468-1359                
Email Address:  OFFICE@KJWOODS.COM                                          

Nature of Work: HORIZONTAL BORING,EXC TO INST WTR,SWR LS
Done for:       CITY OF SUNNYVALE             Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 415-740-0337           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 

    ALL/O TAYLOR AVE FR LASTRETO AVE GO 50'E & W (INCL 20'BEH EDGE/O
    PVMT FOR ENT DIST)

Place: SUNNYVALE                    County: SANTA CLARA          State: CA

Long/Lat Long: -122.020955 Lat:  37.383274 Long: -122.018862 Lat:  37.384518 

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:
#1 EXTEND TO 07/29/2013 RE-MARK NO ORIG DATE 06/03/2013-TRS 06/27/2013
#2 EXTEND TO 08/26/2013 RE-MARK NO ORIG DATE 06/03/2013-MED 07/26/2013
PER CONNOR
#3 EXTEND TO 09/23/2013 RE-MARK NO ORIG DATE 06/03/2013-BMD 08/22/2013
PER CONNOR

Sent to:
CTYSUN = CITY SUNNYVALE               COMSJO = COMCAST-SAN JOSE             
PACBEL = PACIFIC BELL                 PGECUP = PGE DISTR CUPERTINO          
XOCOMM = XO COMM SVCS DBA XO COMM     



