
COMSC5 00056 USAN 08/22/13 14:36:03 0169126 NORMAL NOTICE EXTENSION

Message Number: 0169126 Received by USAN at 14:35 on 08/22/13 by INTERNET

Work Begins:    05/08/13 at 13:15   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 09/23/13 at 23:59   Update By: 09/19/13 at 16:59

Caller:         KARINA VILLANUEVA        
Company:        WEST VALLEY CONSTRUCTION           
Address:        809 HURLINGAME AVE, REDWOOD CITY        
City:           REDWOOD CITY                  State: CA Zip: 94063
Business Tel:   650-364-9464                  Fax: 650-364-8290                
Email Address:  KARINAV@WV-INC.COM                                          

Nature of Work: HAND DIG TO RELOC WTR SVC               
Done for:       CALIFORNIA WATER SERVICES     Explosives: N
Foreman:        LATU                     
Field Tel:                                    Cell Tel: 650-690-0947           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 
Street Address:         816 E 4TH AVE
  Cross Street:         S GRANT ST

    WRK EXT APP 15' INTO ST AT FRT/O ADDR & EXT APP 10' ONTO PROP  

Place: SAN MATEO                    County: SAN MATEO            State: CA

Long/Lat Long: -122.318206 Lat:  37.566983 Long: -122.314856 Lat:  37.56944  

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:
#1 EXTEND TO 07/01/2013 RE-MARK NO ORIG DATE 05/06/2013-IAAM370
05/30/2013
#2 EXTEND TO 07/29/2013 RE-MARK NO ORIG DATE 05/06/2013-IAAM370
06/26/2013
#3 EXTEND TO 08/26/2013 RE-MARK NO ORIG DATE 05/06/2013-IAAM370
07/23/2013
#4 EXTEND TO 09/23/2013 RE-MARK NO ORIG DATE 05/06/2013-IAAM370
08/22/2013

Sent to:
ASTOUN = ASTOUND BROADBAND LLC        CWSSMA = CALIF WTR SVC-SAN MATEO      
CTYFOS = CITY FOSTER CITY             CTYSMA = CITY SAN MATEO               
COMSC2 = COMCAST-SAN CARLOS2          PACBEL = PACIFIC BELL                 
PGEBEL = PGE DISTR BELMONT            REDFLX = REDFLEX TRAFFIC SYSTEMS      
XOCOM2 = XO COMM SVCS DBA XO COMM     XOCOMM = XO COMM SVCS DBA XO COMM     



