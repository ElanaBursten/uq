
UTIL15 00003 VUPSb 02/24/11 13:23:01 B105500943-00B          NORMAL

Ticket No:  B105500943-00B                        NEW  GRID NORM LREQ
Transmit        Date: 02/24/11   Time: 01:23 PM   Op: WDFINCH
Call            Date: 02/24/11   Time: 01:12 PM
Due By          Date: 03/01/11   Time: 07:00 AM
Update By       Date: 03/15/11   Time: 11:59 PM
Expires         Date: 03/18/11   Time: 07:00 AM
Old Tkt No: B105500943
Original Call   Date: 02/24/11   Time: 01:12 PM   Op: WDFINCH

City/Co:ALEXANDRIA CITY       Place:                                    State:VA
Address:     216              Street: EVANS LA
Cross 1:     LYNHAVENN LA

Type of Work:   ANCHORS FOR UTILITY POLE - INSTALL
Work Done For:  DOMINION
Excavation area:LOCATE THIRTY FOOT RADIUS AROUND POLE C01016 QB7900 LOCATED IN
                FRONT OF THIS ADDRESS  THIS IS THE SECOND POLE FROM LYNHAVEN LA
Instructions:   CALLER MAP REF: 5648B7

Whitelined: N   Blasting: N   Boring: N

Company:        DOMINION VIRGINIA POWER                   Type: UTIL
Co. Address:    7888 BACKLICK RD  First Time: N
City:           SPRINGFIELD  State:VA  Zip:22150
Company Phone:  804-771-3001
Contact Name:   DEBBIE FINCH                Contact Phone:703-440-5322
Email:          debbie.finch@dom.com
Field Contact:  JOHN INTIHAR
Fld. Contact Phone:703-216-5552

Mapbook:  5648B7
Grids:    3850D7703D-10  3850D7703D-20

Members:
ALX901 = CITY OF ALEXANDRIA (ALX)       CMC703 = COMCAST (CMC)
DOM400 = DOMINION VIRGINIA POWER (DOM)  PEP904 = POTOMAC ELECTRIC POWER CO(PEP)
VAW902 = VIRGINIA AMERICAN WATER C(VAW) VZN104 = VERIZON (VZN)
WGL904 = WASHINGTON GAS (WGL)

Seq No:   3 B
