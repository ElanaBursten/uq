
UTIL15 00004 VUPSa 02/24/11 14:49:13 A105501290-00A          EMERGENCY

Ticket No:  A105501290-00A                        NEW  GRID EMER LREQ
Transmit        Date: 02/24/11   Time: 02:49 PM   Op: 1MAR
Call            Date: 02/24/11   Time: 02:45 PM
Due By          Date: 02/24/11   Time: 05:45 PM
Update By       Date:            Time:
Expires         Date:            Time:
Old Tkt No: A105501290
Original Call   Date: 02/24/11   Time: 02:45 PM   Op: 1MAR

City/Co:ARLINGTON             Place:ROSSLYN                             State:VA
Address:                      Street: 19TH ST N
Cross 1:     N KENT ST

Type of Work:   WATER MAIN - REPAIR, REPLACE OR ABANDON
Work Done For:  SAME
Excavation area:CREW IS ON SITE
                A 50FT RADIUS OF THE LEAKING MAIN IN THE INTERSECTION
Instructions:   CALLER MAP REF: 17E1

Whitelined: N   Blasting: N   Boring: N

Company:        ARLINGTON CNTY WATER SEWER STREETS BUREA  Type: UTIL
Co. Address:    4200 S 28TH ST  First Time: N
City:           ARLINGTON  State:VA  Zip:22206
Company Phone:  703-228-6555
Contact Name:   HAROLD TURNER               Contact Phone:703-228-6555
Email:          hturner@arlingtonva.us
Field Contact:  MR KAMARIA
Fld. Contact Phone:571-238-1067

Mapbook:  5527J7
Grids:    3853A7704D-31

Members:
APW901 = ARLINGTON COUNTY DPW (APW)     CMC703 = COMCAST (CMC)
DOM400 = DOMINION VIRGINIA POWER (DOM)  JUC371 = JONES UTILITIES CONSTRUCT(JUC)
MCII81 = MCI (MCI)                      MFN902 = ABOVENET COMMUNICATIONS I(MFN)
PEP904 = POTOMAC ELECTRIC POWER CO(PEP) QGS901 = QWEST GOVERNMENT SERVICES(QGS)
VZN104 = VERIZON (VZN)                  WGL904 = WASHINGTON GAS (WGL)

Seq No:   4 A
