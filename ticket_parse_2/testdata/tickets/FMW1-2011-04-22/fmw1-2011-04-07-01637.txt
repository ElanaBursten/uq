
NOTICE OF INTENT TO EXCAVATE        
Ticket No: 11172198 
Transmit      Date: 04/07/11      Time: 04:58 PM           Op: wiches 
Original Call Date: 04/07/11      Time: 04:41 PM           Op: wiches 
Work to Begin Date: 04/12/11      Time: 12:01 AM 
Expiration    Date: 04/26/11      Time: 11:59 PM 

Place: SILVER SPRING 
Address: 2000        Street: SANDSTONE CT 
Nearest Intersecting Street: SERPENTINE WAY 

Type of Work: INSTALL DECK 
Extent of Work: MARK WITHIN 8FT OF THE BACK OF THE HOUSE FOR THE WIDTH OF THE
: HOUSE. 
Remarks:  

Company      : HOMEOWNER 
Contact Name : ZELALEM YENEFANTA              Fax          :  
Contact Phone: (240)461-5805                  Ext          :  
Caller Address: 2000 SANDSTONE CT 
                SILVER SPRING, MD 20904 
Email Address:  zyenefanta@yahoo.com 
Alt. Contact :                                Alt. Phone   :  
Work Being Done For: HOMEOWNER 
State: MD              County: MONTGOMERY 
MPG:  N 
Caller    Provided:  Map Name: MONT  Map#: 5167 Grid Cells: E10 
Computer Generated:  Map Name: MONT  Map#: 5167 Grid Cells: E10 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.0687633Lon: -76.9750310 SE Lat: 39.0625040Lon: -76.9687875 
Explosives: N 
ACSI03     FBLM01     HNI01      MCI01      PEPCOMC    WGL06      WSS01 
Send To: TRU02     Seq No: 0271   Map Ref:  
         VMG               0502 
