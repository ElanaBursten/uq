���MISUTIL2003010700661	PG NON PIPELINE	2003/01/07 16:55:16	00654


NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008848
Transmit      Date: 01/07/03      Time: 16:45    Op: kelley
Original Call Date: 01/07/03      Time: 16:37    Op: kelley
Work to Begin Date: 01/09/03      Time: 04PM

Place: CLINTON
Address: 8905        Street: WOODYARD RD

Type of Work: INST ALL UG UTIL
Extent of Work:   LOC ENTIRE LOT IN FRONT OF LOWES SHOPPING CENTER AT THE
: BENCH MARK BLDG CONTRACTORS SIGN
Remarks: LEGALS NOT PROVIDED FOR THIS LOCATE REQUEST
:     �45-6766

Company     : BENCHMARK BLDG CONTRACTOR INC
Contact Name: GENA ROOKS            Contact Phone: (770)945-8081
Alt. Contact:                       Alt. Phone   :
Work Being Done For: I HOP
State: MD              County: PG
Map: PG    Page: 026   Grid Cells: A04,A05
Explosives: N
MTV01      TPG01      WGL06      WSS01
Send To: PEP05     Seq No: 0454   Map Ref:


MISUTIL2003010700666	DC PIPELINE	2003/01/07 16:55:19	00655


NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008851
Transmit      Date: 01/07/03      Time: 16:46    Op: carol
Original Call Date: 01/07/03      Time: 16:38    Op: carol
Work to Begin Date: 01/09/03      Time: 04PM

Place: NW
Address: 5037        Street: GARFIELD ST NW
Nearest Intersecting Street: ARIZONA AV NW

Type of Work: INST ELEC SVC
Extent of Work:   LOC FRONT OF PROP FROM GARFIELD ST TO HSE AT ABOVE
: ADDRESS     MARKINGS ARE FOR GAS ONLY
Remarks: LEGALS NOT PROVIDED FOR THIS LOCATE REQUEST
:                                                        Fax: (703)846-9110

Company     : HITT CONTRACTING
Contact Name: MARTY DAVIS           Contact Phone: (703)846-9000
Alt. Contact: MIKE SEASTER          Alt. Phone   : (703)846-9000
Work Being Done For: SMITH
State: DC              County: NW
Map: NW    Page: 008   Grid Cells: K09
Explosives: N
DCC01      LTC02      TDC01      WASA02     WGL07
Send To: PEP06     Seq No: 0196   Map Ref:


MISUTIL2003010700656	MONT NON PIPELINE	2003/01/07 16:55:22	00656


NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008853
Transmit      Date: 01/07/03      Time: 16:41    Op: sabrin
Original Call Date: 01/07/03      Time: 16:39    Op: sabrin
Work to Begin Date: 01/09/03      Time: 04PM

Place: LAYTONSVILLE
Address: 6109        Street: GOLF ESTATES CT
Nearest Intersecting Street: GOLF ESTATES DR

Type of Work: INST FOOTERS FOR A DECK
Extent of Work:   LOC ENTIRE REAR OF PROP
Remarks:
:                                                        Fax: (301)590-9248

Company     : MURPHY SUNDECKS
Contact Name: TRAVIS MURPHY         Contact Phone: (301)590-9600
Alt. Contact: GLEN MURPHY           Alt. Phone   :
Work Being Done For: MORRISON
State: MD              County: MONT
Map: MONT  Page: 011   Grid Cells: G09
Explosives: N
TMT01      TRU01      WGL06
Send To: PEP05     Seq No: 0450   Map Ref:


MISUTIL2003010700657	MONT NON PIPELINE	2003/01/07 16:55:25	00657

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008855               Update Of: 00807987
Transmit      Date: 01/07/03      Time: 16:41    Op: r.city
Original Call Date: 01/07/03      Time: 16:40    Op: r.city
Work to Begin Date: 01/09/03      Time: 04PM

Place: POTOMAC
Address: 8601        Street: RAPLEY GATE TERRACE
Nearest Intersecting Street: PRALEY GATE DRIVE

Type of Work: SEWER AND WATERHOUSE CONNECTION (3530)
Extent of Work:   LOCATE IN FRONT OF THE PROPERTY &50FT ON EITHER SIDE OF
: THE R&L PROPERTY LINES,IN THE STREET,ON BOTH SIDES OF THE STREET FROME
: PROPERTY LINE TO PROPERTY LINE ALL SERVICE AND MAINS ESPICALLY GAS MAIN
: GAS SERVICES AND ANY OTHER UNDERGROUND UTILITIES IN THE AREA
Remarks: IF THE PROPERTY IS A CORNER LOT ALL SIDE STREET ARE TO BE LOCATED
: AS SSTATED ABOVE                                       Fax: (301)599-0921

Company     : CITY CONTRACTORS INC
Contact Name: CHUCK DULIN           Contact Phone: (301)599-8484
Alt. Contact: MILUS LEWIS           Alt. Phone   : (301)599-8485
Work Being Done For: WSSC
State: MD              County: MONT
Map: MONT  Page: 034   Grid Cells: G09
Explosives: N
TMT01      TRU01      WGL06      WSS01
Send To: PEP05     Seq No: 0451   Map Ref:


MISUTIL2003010700658	MONT NON PIPELINE	2003/01/07 16:55:29	00658


NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008857               Update Of: 00807991
Transmit      Date: 01/07/03      Time: 16:42    Op: r.city
Original Call Date: 01/07/03      Time: 16:41    Op: r.city
Work to Begin Date: 01/09/03      Time: 04PM

Place: POTOMAC
Address: 8521        Street: BRICKYARD ROAD
Nearest Intersecting Street: HACKAMORE DRIVE

Type of Work: SEWER AND WATERHOUSE CONNECTION (3530)
Extent of Work:   LOCATE IN FRONT OF THE PROPERTY &50FT ON EITHER SIDE OF
: THE R&L PROPERTY LINES,IN THE STREET,ON BOTH SIDES OF THE STREET FROME
: PROPERTY LINE TO PROPERTY LINE ALL SERVICE AND MAINS ESPICALLY GAS MAIN
: GAS SERVICES AND ANY OTHER UNDERGROUND UTILITIES IN THE AREA
Remarks: IF THE PROPERTY IS A CORNER LOT ALL SIDE STREET ARE TO BE LOCATED
: AS SSTATED ABOVE                                       Fax: (301)599-0921

Company     : CITY CONTRACTORS INC
Contact Name: CHUCK DULIN           Contact Phone: (301)599-8484
Alt. Contact: MILUS LEWIS           Alt. Phone   : (301)599-8485
Work Being Done For: WSSC
State: MD              County: MONT
Map: MONT  Page: 034   Grid Cells: C10
Explosives: N
TMT01      TRU01      WGL06      WSS01
Send To: PEP05     Seq No: 0452   Map Ref:


MISUTIL2003010700660	DC NON PIPELINE	2003/01/07 16:55:32	00659

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008859
Transmit      Date: 01/07/03      Time: 16:43    Op: eric
Original Call Date: 01/07/03      Time: 16:42    Op: eric
Work to Begin Date: 01/09/03      Time: 04PM

Place: SE
Address: 1532        Street: E ST SE
Nearest Intersecting Street: 16TH ST SE

Type of Work: REPLACE WTR SVC  N/E
Extent of Work:   LOC FRONT OF HSE TO CURB
Remarks:
:                                                        Fax: (202)388-0654

Company     : SEIDEL PLUMBING & HEATING
Contact Name: OTTO SEIDEL           Contact Phone: (202)397-7000
Alt. Contact: JACKIE / OTTO/ CELL   Alt. Phone   : (202)359-3283
Work Being Done For: THOMAS
State: DC              County: SE
Map: SE    Page: 016   Grid Cells: H04
Explosives: N
ATT01      DCC01      MCI03      STS02      TDC01      WASA02     WGL07
Send To: PEP06     Seq No: 0194   Map Ref:


MISUTIL2003010700659	MONT NON PIPELINE	2003/01/07 16:55:35	00660


NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008861               Update Of: 00807999
Transmit      Date: 01/07/03      Time: 16:43    Op: r.city
Original Call Date: 01/07/03      Time: 16:42    Op: r.city
Work to Begin Date: 01/09/03      Time: 04PM

Place: BROOKEVILLE
Address: 2713        Street: GOLD MINE ROAD
Nearest Intersecting Street: GEORGIA AVENUE

Type of Work: SEWER AND WATERHOUSE CONNECTION (3530)
Extent of Work:   LOCATE IN FRONT OF THE PROPERTY &50FT ON EITHER SIDE OF
: THE R&L PROPERTY LINES,IN THE STREET,ON BOTH SIDES OF THE STREET FROME
: PROPERTY LINE TO PROPERTY LINE ALL SERVICE AND MAINS ESPICALLY GAS MAIN
: GAS SERVICES AND ANY OTHER UNDERGROUND UTILITIES IN THE AREA
Remarks: IF THE PROPERTY IS A CORNER LOT ALL SIDE STREET ARE TO BE LOCATED
: AS SSTATED ABOVE                                       Fax: (301)599-0921

Company     : CITY CONTRACTORS INC
Contact Name: CHUCK DULIN           Contact Phone: (301)599-8484
Alt. Contact: MILUS LEWIS           Alt. Phone   : (301)599-8485
Work Being Done For: WSSC
State: MD              County: MONT
Map: MONT  Page: 021   Grid Cells: G03
Explosives: N
CTR05      TMT02      TRU02      WGL06      WSS01
Send To: PEP05     Seq No: 0453   Map Ref:


MISUTIL2003010700662	PG NON PIPELINE	2003/01/07 16:55:38	00661

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008862               Update Of: 00809668
Transmit      Date: 01/07/03      Time: 16:45    Op: a.lewi
Original Call Date: 01/07/03      Time: 16:43    Op: a.lewi
Work to Begin Date: 01/09/03      Time: 04PM

Place: FRIENDLY
Address:             Street: ELIZABETH IDA DR
Nearest Intersecting Street: MARY CATHERINE DR

Type of Work: NEW HOME CONSTRUCTION
Extent of Work:   PLS MEET WITH PETE PARRECO AT THE END OF THE ABOVE ST
: WHICH IS W OF THE INTER....ST WILL DEAD END
: CALLER REQUEST THAT UTILITIES MARK FOR THE PREVIOUS MEETING ON UPDATE
Remarks: MEETING REQUESTED BY CALLER ON 10/10 @ 9AM
: CALL PETE PARRECO AT 3012778220 WITH ?`S               Fax: (301)277-8224

Company     : ARNOLD PARRECO & SON
Contact Name: FRED TERREL           Contact Phone: (301)277-8220
Alt. Contact: RICK PARRECO          Alt. Phone   :
Work Being Done For: P D C
State: MD              County: PG
Map: PG    Page: 031   Grid Cells: A04
Explosives: N
TPG01      WGL06
Send To: PEP05     Seq No: 0455   Map Ref:


MISUTIL2003010700663	MONT NON PIPELINE	2003/01/07 16:55:42	00662

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008863               Update Of: 00808008
Transmit      Date: 01/07/03      Time: 16:45    Op: r.city
Original Call Date: 01/07/03      Time: 16:43    Op: r.city
Work to Begin Date: 01/09/03      Time: 04PM

Place: LAYTONSVILLE
Address: 21616       Street: RIPPLEMEAD DRIVE
Nearest Intersecting Street: RIGGS ROAD

Type of Work: SEWER AND WATERHOUSE CONNECTION (3530)
Extent of Work:   LOCATE IN FRONT OF THE PROPERTY &50FT ON EITHER SIDE OF
: THE R&L PROPERTY LINES,IN THE STREET,ON BOTH SIDES OF THE STREET FROME
: PROPERTY LINE TO PROPERTY LINE ALL SERVICE AND MAINS ESPICALLY GAS MAIN
: GAS SERVICES AND ANY OTHER UNDERGROUND UTILITIES IN THE AREA
Remarks: IF THE PROPERTY IS A CORNER LOT ALL SIDE STREET ARE TO BE LOCATED
: AS SSTATED ABOVE                                       Fax: (301)599-0921

Company     : CITY CONTRACTORS INC
Contact Name: CHUCK DULIN           Contact Phone: (301)599-8484
Alt. Contact: MILUS LEWIS           Alt. Phone   : (301)599-8485
Work Being Done For: WSSC
State: MD              County: MONT
Map: MONT  Page: 011   Grid Cells: K09
Explosives: N
ATM01      BGE05      TMT01      TRU01      WSS01
Send To: PEP05     Seq No: 0456   Map Ref:


MISUTIL2003010700665	DC NON PIPELINE	2003/01/07 16:55:45	00663

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008865               Update Of: 00803769
Transmit      Date: 01/07/03      Time: 16:45    Op: j.thom
Original Call Date: 01/07/03      Time: 16:44    Op: j.thom
Work to Begin Date: 01/09/03      Time: 04PM

Place: NW
Address: 1421        Street: P ST
Nearest Intersecting Street: 15TH ST

Type of Work: INST ELEC MANHOLES AND CONDUITS
Extent of Work:   LOC THE ENTIRE FRONT OF THE PROP INCLUDING THE RDWY AND
: SIDEWALK IN FRONT OF THE ABOVE ADDRESS
Remarks:
:                                                        Fax: (703)430-4208

Company     : RB HINKLE CONSTRUCTION, INC.
Contact Name: KRIS BROWN            Contact Phone: (703)430-0200
Alt. Contact: KRIS BROWN            Alt. Phone   :
Work Being Done For: P.N. HOFFMAN
State: DC              County: NW
Map: NW    Page: 009   Grid Cells: K12
Explosives: N
ATT01      DCC01      LTC02      MCI03      MFN03      QWESTM02   TDC01
 WASA02     WGL07
Send To: PEP06     Seq No: 0195   Map Ref:


MISUTIL2003010700664	MONT NON PIPELINE	2003/01/07 16:55:48	00664

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008866               Update Of: 00808014
Transmit      Date: 01/07/03      Time: 16:45    Op: r.city
Original Call Date: 01/07/03      Time: 16:44    Op: r.city
Work to Begin Date: 01/09/03      Time: 04PM

Place: LAYTONSVILLE
Address: 21509       Street: RIPPLEMEAD DRIVE
Nearest Intersecting Street: RIGGS ROAD

Type of Work: SEWER AND WATERHOUSE CONNECTION (3530)
Extent of Work:   LOCATE IN FRONT OF THE PROPERTY &50FT ON EITHER SIDE OF
: THE R&L PROPERTY LINES,IN THE STREET,ON BOTH SIDES OF THE STREET FROME
: PROPERTY LINE TO PROPERTY LINE ALL SERVICE AND MAINS ESPICALLY GAS MAIN
: GAS SERVICES AND ANY OTHER UNDERGROUND UTILITIES IN THE AREA
Remarks: IF THE PROPERTY IS A CORNER LOT ALL SIDE STREET ARE TO BE LOCATED
: AS SSTATED ABOVE                                       Fax: (301)599-0921

Company     : CITY CONTRACTORS INC
Contact Name: CHUCK DULIN           Contact Phone: (301)599-8484
Alt. Contact: MILUS LEWIS           Alt. Phone   : (301)599-8485
Work Being Done For: WSSC
State: MD              County: MONT
Map: MONT  Page: 011   Grid Cells: K09
Explosives: N
ATM01      BGE05      TMT01      TRU01      WSS01
Send To: PEP05     Seq No: 0457   Map Ref:


MISUTIL2003010700667	MONT NON PIPELINE	2003/01/07 16:55:52	00665

NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008867               Update Of: 00808019
Transmit      Date: 01/07/03      Time: 16:46    Op: r.city
Original Call Date: 01/07/03      Time: 16:45    Op: r.city
Work to Begin Date: 01/10/03      Time: 07AM

Place: GAITHERSBURG
Address: 433         Street: GAITHER STREET
Nearest Intersecting Street: E DEAR PARK DRIVE

Type of Work: SEWER AND WATERHOUSE CONNECTION (3530)
Extent of Work:   LOCATE IN FRONT OF THE PROPERTY &50FT ON EITHER SIDE OF
: THE R&L PROPERTY LINES,IN THE STREET,ON BOTH SIDES OF THE STREET FROME
: PROPERTY LINE TO PROPERTY LINE ALL SERVICE AND MAINS ESPICALLY GAS MAIN
: GAS SERVICES AND ANY OTHER UNDERGROUND UTILITIES IN THE AREA
Remarks: IF THE PROPERTY IS A CORNER LOT ALL SIDE STREET ARE TO BE LOCATED
: AS SSTATED ABOVE                                       Fax: (301)599-0921

Company     : CITY CONTRACTORS INC
Contact Name: CHUCK DULIN           Contact Phone: (301)599-8484
Alt. Contact: MILUS LEWIS           Alt. Phone   : (301)599-8485
Work Being Done For: WSSC
State: MD              County: MONT
Map: MONT  Page: 019   Grid Cells: G10
Explosives: N
MCI01      TMT01      TRU01      WGL06      WSS01
Send To: PEP05     Seq No: 0458   Map Ref:


MISUTIL2003010700668	MONT NON PIPELINE	2003/01/07 16:55:55	00666


NOTICE OF INTENT TO EXCAVATE
Ticket No: 01008871
Transmit      Date: 01/07/03      Time: 16:50    Op: webusr
Original Call Date: 01/07/03      Time: 16:48    Op: webusr
Work to Begin Date: 01/09/03      Time: 05PM

Place: BROOKEVILLE
Address: 5           Street: GREGG COURT
Nearest Intersecting Street: GREGG RD

Type of Work: WELL DRILLING AND WATER LINE INSTALLATION
Extent of Work:   LOCATE ENTIRE PROPERTY WITH FLAGS & PAINT,  IMPORTANT T
: O MARK WITH  FLAGS!!!!!!
Remarks:
:                                                        Fax: (301)829-2667

Company     : L FRANKLIN EASTERDAY INC
Contact Name: SARA EASTERDAY        Contact Phone: (301)829-1640
Alt. Contact: SANDI                 Alt. Phone   : (301)831-5170
Work Being Done For: MAXINE FINKLESTEIN
State: MD              County: MONT
Map: MONT  Page: 012   Grid Cells: B10
Explosives: N
TMT02      TRU02
Send To: PEP05     Seq No: 0459   Map Ref:


����I
