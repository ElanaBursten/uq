
NOTICE OF INTENT TO EXCAVATE      NOTI-2D REQUEST
Ticket No: 29400736
Transmit      Date: 10/21/02      Time: 09:57AM  Op: retahe
Original Call Date: 10/21/02      Time: 08:55AM  Op: retahe
Work to Begin Date: 10/21/02      Time: 09:00AM

Place: KATY
Address:             Street: BARTON MEADOW LN
Nearest Intersecting Street: LAUREL CHASE LN

Type of Work: INSTALLING UNDERGROUND GAS AND-INSTALLING UNDERG
Extent of Work: LSTK=29104239 SEQNO=1340 UPDATEOF=28303690 DUR=UNKNOW
: START MARK  ING FROM THE INTER GOING NE MARK THE FRONT OF PROPERTY AND
: THE RD R/W  ON B/S OF RD FOR 600FT. SUBDIV:FALCON RANCH SECTION TWO
: PAGE:485 GRID N  RC-022916715--NO RESPONSE FOR ALL UTIL CREW IS ON SITE
: PLEASE  RESPOND ASAP.  FAX #: 713-691-6054  MATERIALROOM@NHPLC.COM
: FAXBACKREQ: N

Remarks: WK TO BEGIN DT 10/22/02 16:45:00
: TESSTOC: 022945175 SEQ: 0534 ON 21-OCT-02 AT 0849 RCD: 10/21/02 08:55:49

Company     : DID NOT PROVIDE
Contact Name: PHILLIP TAMBORELLO    Contact Phone: (713)691-3616
Alt. Contact: PHILLIP TAMBORELLO    Alt. Phone   : (713)333-2134
Work Being Done For: RELIANT ENERGY HLP
State: TX              County: FORT BEND
Map: TX    Page: 485   Grid Cells: N
Map: *MORE Page: 484   Grid Cells: M,R,V
Explosives: N
RELIAN01   TXUC01
Send To: TWC30     Seq No: 0029   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE
Ticket No: 29401305
Transmit      Date: 10/21/02      Time: 09:57AM  Op: april
Original Call Date: 10/21/02      Time: 09:52AM  Op: april
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: HOUSTON CITY
Address:             Street: LAKE HOUSTON PKWY
Nearest Intersecting Street: RALSTON RD

Type of Work: INSTALLATION OF UTILITY POLE
Extent of Work: FROM RALSTON RD GO W ON LAKE HOUSTON PKWY APPROX 2500FT
: TO STAKED AREA S SIDE OF RD.MARK 10FT RADIUS OF STAKED AREA.

Remarks: CALLER STATES APPROX 500FT E OF GREENS BAYOU

Company     : CENTER POINT ENERGY
Contact Name: SHASTA MARTIN         Contact Phone: (713)945-8785
Alt. Contact: JEFF D CALL 1ST       Alt. Phone   : (713)945-8706
Work Being Done For: CENTER POINT ENERGY
State: TX              County: HARRIS
Map: TX    Page: 416   Grid Cells: J
Map: *MORE Page: 416   Grid Cells: J,N,K,P
Explosives: N
RELIAN01
Send To: TWC20     Seq No: 0012   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE
Ticket No: 29401307
Transmit      Date: 10/21/02      Time: 09:57AM  Op: maggie
Original Call Date: 10/21/02      Time: 09:53AM  Op: maggie
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: CHANNELVIEW CDP
Address:             Street: DELLDALE
Nearest Intersecting Street: WALLISVILLE ROAD

Type of Work: INSTALLING CULVERTS
Extent of Work: MARK FROM SW CORNER OF ABOVE INTERSECTION GOING S ON W
: SIDE DELLDALE APPROX 100YARDS.

Remarks:

Company     : CROSSROAD INDUSTRIES INC
Contact Name: JAMES P               Contact Phone: (713)356-6611
Alt. Contact: CELL                  Alt. Phone   : (832)573-7491
Work Being Done For: COSTAL BEND
State: TX              County: HARRIS
Map: TX    Page: 457   Grid Cells: V
Map: *MORE Page: 458   Grid Cells: N,S,W
Explosives: N
CITGO01    COLPL02    OILTK01    RELIAN01
Send To: TWC20     Seq No: 0013   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE
Ticket No: 29401376
Transmit      Date: 10/21/02      Time: 09:57AM  Op: jamie
Original Call Date: 10/21/02      Time: 09:55AM  Op: jamie
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: HOUSTON CITY
Address:   6734      Street: SEINFELD CT
Nearest Intersecting Street: CHAMPION PARK

Type of Work: INSTALLATION OF SPRINKLER SYSTEMS
Extent of Work: MARK ENTIRE LOT.
Remarks:

Company     : GREEN TURF IRRIGATION
Contact Name: PAULA H               Contact Phone: (281)376-6951
Alt. Contact: MEL H /CELL PHONE     Alt. Phone   : (832)250-7980
Work Being Done For: ADAM JOHNSTON
State: TX              County: HARRIS
Map: TX    Page: 370   Grid Cells: B
Map: TX    Page: 370   Grid Cells: B,A
Explosives: N
LOG01      RELIAN01
Send To: TWC10     Seq No: 0052   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE
Ticket No: 29401387               Update Of: 28402802
Transmit      Date: 10/21/02      Time: 09:58AM  Op: fxlisa
Original Call Date: 10/21/02      Time: 09:57AM  Op: fxlisa
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: SUGAR LAND DIVISION
Address:             Street: US 59
Nearest Intersecting Street: SUGAR CREEK BLVD

Type of Work: INSTALLING U/G ELECTRICAL
Extent of Work: (99400 US 59) MARK ALONG US 59 ON THE EAST AND WEST SIDES
: OF THE NORTH AND SOUTH BOUND SERVICE ROADS IN THE STATE RIGHT OF WAY,
: STARTING AT SUGAR CREEK BLVD AND ENDING AND KIRKWOOD. MARK A 500FT RADIUS
: OF SUGAR CREEK AND KIRKWOOD INTERSECTIONS.

Remarks: FAXED LOCATE
: UPDATE PER CALLER-MARKINGS HAVE BEEN WASHED AWAY

Company     : MICA CORP.
Contact Name: ROSIE ARRATIA         Contact Phone: (713)896-4288
Alt. Contact: SAME                  Alt. Phone   :
Work Being Done For: TX DOT
State: TX              County: FORT BEND
Map: TX    Page: 569   Grid Cells: E
Map: TX    Page: 568   Grid Cells: M,R
Explosives: N
FBWCID02   NGPL912    RELIAN01   TWC30      TXUC01     XMICA01    XOCS01
Send To: SUGAR01   Seq No: 0003   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE
Ticket No: 29401387               Update Of: 28402802
Transmit      Date: 10/21/02      Time: 09:58AM  Op: fxlisa
Original Call Date: 10/21/02      Time: 09:57AM  Op: fxlisa
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: SUGAR LAND DIVISION
Address:             Street: US 59
Nearest Intersecting Street: SUGAR CREEK BLVD

Type of Work: INSTALLING U/G ELECTRICAL
Extent of Work: (99400 US 59) MARK ALONG US 59 ON THE EAST AND WEST SIDES
: OF THE NORTH AND SOUTH BOUND SERVICE ROADS IN THE STATE RIGHT OF WAY,
: STARTING AT SUGAR CREEK BLVD AND ENDING AND KIRKWOOD. MARK A 500FT RADIUS
: OF SUGAR CREEK AND KIRKWOOD INTERSECTIONS.

Remarks: FAXED LOCATE
: UPDATE PER CALLER-MARKINGS HAVE BEEN WASHED AWAY

Company     : MICA CORP.
Contact Name: ROSIE ARRATIA         Contact Phone: (713)896-4288
Alt. Contact: SAME                  Alt. Phone   :
Work Being Done For: TX DOT
State: TX              County: FORT BEND
Map: TX    Page: 569   Grid Cells: E
Map: TX    Page: 568   Grid Cells: M,R
Explosives: N
FBWCID02   NGPL912    RELIAN01   SUGAR01    TXUC01     XMICA01    XOCS01
Send To: TWC30     Seq No: 0030   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE
Ticket No: 29401390               Update Of: 28402803
Transmit      Date: 10/21/02      Time: 09:58AM  Op: fxlisa
Original Call Date: 10/21/02      Time: 09:57AM  Op: fxlisa
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: SUGAR LAND DIVISION
Address:             Street: US 90
Nearest Intersecting Street: S KIRKWOOD

Type of Work: INSTALLING U/G ELECTRICAL
Extent of Work: (99440 US 90) MARK ALONG US 90 ON THE NORTH AND SOUTH
: SIDES OF THE EAST AND WEST BOUND SERVICE ROADS IN THE START RIGHT OF WAY
: STARTING AT S KIRKWOOD AND ENDING AT DAIRY ASHFORD. MARK A 500FT RADIUS
: OF S KIRKWOOD, US 59, DAIRY ASHFORD INTERSECTIONS.

Remarks: FAXED LOCATE REQUEST
: UPDATE PER CALLER-MARKINGS HAVE BEEN WASHED AWAY

Company     : MICA CORP.
Contact Name: ROSIE ARRATIA         Contact Phone: (713)896-4288
Alt. Contact: SAME                  Alt. Phone   :
Work Being Done For: TX DOT
State: TX              County: FORT BEND
Map: TX    Page: 569   Grid Cells: J
Map: TX    Page: 568   Grid Cells: M
Explosives: N
FBWCID02   NGPL912    RELIAN01   TWC30      XMICA01    XOCS01
Send To: SUGAR01   Seq No: 0004   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE
Ticket No: 29401390               Update Of: 28402803
Transmit      Date: 10/21/02      Time: 09:58AM  Op: fxlisa
Original Call Date: 10/21/02      Time: 09:57AM  Op: fxlisa
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: SUGAR LAND DIVISION
Address:             Street: US 90
Nearest Intersecting Street: S KIRKWOOD

Type of Work: INSTALLING U/G ELECTRICAL
Extent of Work: (99440 US 90) MARK ALONG US 90 ON THE NORTH AND SOUTH
: SIDES OF THE EAST AND WEST BOUND SERVICE ROADS IN THE START RIGHT OF WAY
: STARTING AT S KIRKWOOD AND ENDING AT DAIRY ASHFORD. MARK A 500FT RADIUS
: OF S KIRKWOOD, US 59, DAIRY ASHFORD INTERSECTIONS.

Remarks: FAXED LOCATE REQUEST
: UPDATE PER CALLER-MARKINGS HAVE BEEN WASHED AWAY

Company     : MICA CORP.
Contact Name: ROSIE ARRATIA         Contact Phone: (713)896-4288
Alt. Contact: SAME                  Alt. Phone   :
Work Being Done For: TX DOT
State: TX              County: FORT BEND
Map: TX    Page: 569   Grid Cells: J
Map: TX    Page: 568   Grid Cells: M
Explosives: N
FBWCID02   NGPL912    RELIAN01   SUGAR01    XMICA01    XOCS01
Send To: TWC30     Seq No: 0031   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE
Ticket No: 29401403               Update Of: 28402804
Transmit      Date: 10/21/02      Time: 09:58AM  Op: fxlisa
Original Call Date: 10/21/02      Time: 09:57AM  Op: fxlisa
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: HOUSTON DIVISION
Address:             Street: WEST LOOP 610
Nearest Intersecting Street: RICHMOND

Type of Work: INSTALLING U/G ELECTRICAL
Extent of Work: (202757 W LOOP 610) MARK ALONG WEST LOOP 610 FREEWAY ON
: THE EAST AND WEST SIDES OF THE NORTH AND SOUTH BOUND SERVICE ROADS
: STARTING AT RICHMOND BLVD AND ENDING AT IH 10. MARK ALL STATE RIGHT OF
: WAY, INCLUDING A 500 FT RADIUS OF RICHMOND BLVD, ALABAMA, WESTHEIMER,

Remarks: SAN FELIPE, POST OAK, WOODWAY, MEMORIAL AND IH 10.
: FAX LOCATE      UPDATE PER CALLER-MARKINGS HAVE BEEN WASHED AWAY

Company     : MICA CORP.
Contact Name: ROSIE ARRATIA         Contact Phone: (713)896-4288
Alt. Contact: SAME                  Alt. Phone   :
Work Being Done For: TX DOT
State: TX              County: HARRIS
Map: TX    Page: 491   Grid Cells: H
Explosives: N
PSCOPE01   RELIAN01   TCG01      TVMAX01    TXUC01     XMICA01    XOCS01
Send To: TWC30     Seq No: 0032   Map Ref:





NOTICE OF INTENT TO EXCAVATE      INSU-SHORT NOT
Ticket No: 29401379
Transmit      Date: 10/21/02      Time: 09:58AM  Op: marye
Original Call Date: 10/21/02      Time: 09:55AM  Op: marye
Work to Begin Date: 10/22/02      Time: 08:00AM

Place: SPRING CDP
Address:   3746      Street: BLUE LAKE
Nearest Intersecting Street: ELLA

Type of Work: INSTALLATION OF FENCE
Extent of Work: MARK ENTIRE LOT.
Remarks:

Company     : SOUTHLAND FENCE
Contact Name: AMY H                 Contact Phone: (281)355-0707
Alt. Contact: SHANE T               Alt. Phone   :
Work Being Done For: JOHN GRAHAMS
State: TX              County: HARRIS
Map: TX    Page: 331   Grid Cells: G
Map: TX    Page: 331   Grid Cells: C,G,D
Explosives: N
RELIAN01
Send To: TWC10     Seq No: 0053   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE
Ticket No: 29401410               Update Of: 28402810
Transmit      Date: 10/21/02      Time: 09:58AM  Op: fxlisa
Original Call Date: 10/21/02      Time: 09:57AM  Op: fxlisa
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: HOUSTON DIVISION
Address:             Street: STUEBNER AIRLINE
Nearest Intersecting Street: CREEKFIELD RD

Type of Work: INSTALLING UNDERGROUND ELECTRICAL
Extent of Work: STUEBNER AIRLINE 200610.  MARK 100 FT RADIUS OF
: INTERSECTION OF STUEBNER AIRLINE AND CREEKFIELD ROAD.  JOB: 200610

Remarks: FAXED LOCATE REQUEST          DIGGING DEEPER THAN 16 INCHES? YES
: UPDATE PER CALLER-MARKINGS HAVE BEEN WASHED AWAY

Company     : MICA CORP.
Contact Name: JESSICA CAMPOS        Contact Phone: (713)896-4288
Alt. Contact: ROBERT BOBO           Alt. Phone   : (210)532-3270
Work Being Done For: TX D.O.T
State: TX              County: HARRIS
Map: TX    Page: 330   Grid Cells: F
Map: TX    Page: 330   Grid Cells: B,F
Explosives: N
RELIAN01   XMICA01    XOCS01
Send To: TWC10     Seq No: 0054   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE
Ticket No: 29401411               Update Of: 28402816
Transmit      Date: 10/21/02      Time: 09:58AM  Op: fxlisa
Original Call Date: 10/21/02      Time: 09:58AM  Op: fxlisa
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: HOUSTON DIVISION
Address:             Street: LOOP 610
Nearest Intersecting Street: WEST PARK

Type of Work: INSTALLING U./G ELECTRIC
Extent of Work: (201727 LOOP 610) MARK ALONG THE NORTH BOUND SERVICE ROAD
: AONG LOOP 610 STARTING AT WEST PARK AND ENDING AT NEW CASTLE AT US 59.
: MARK BOTH SIDE OF THE FEEDER RD.

Remarks: FAXED LOCATE REQUEST
: UPDATE PER CALLER-MARKINGS HAVE BEEN WASHED AWAY

Company     : MICA CORP.
Contact Name: JESSICA CAMPOS        Contact Phone: (713)896-4288
Alt. Contact: SAME                  Alt. Phone   :
Work Being Done For: TX DOT
State: TX              County: HARRIS
Map: TX    Page: 491   Grid Cells: Y
Map: *MORE Page: 491   Grid Cells: Y,Z,U,V
Explosives: N
RELIAN01   TVMAX01    TXUC01     XMICA01    XOCS01
Send To: TWC30     Seq No: 0033   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE
Ticket No: 29401413               Update Of: 28402817
Transmit      Date: 10/21/02      Time: 09:58AM  Op: fxlisa
Original Call Date: 10/21/02      Time: 09:58AM  Op: fxlisa
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: HOUSTON DIVISION
Address:             Street: W SAM HOUSTON PKWY SOUTH
Nearest Intersecting Street: BETWEEN BEECHNUT AND BISSONETT

Type of Work: INSTALL ELECTRICAL UNDERGROUND
Extent of Work: (W SAM HOUSTON PKWY SOUTH 201640) MARK ALONG RIGHT AWAY
: OF THE SOUTH BOUND SERVICE ROAD.

Remarks: FAXED LOCATE REQUEST
: UPDATE PER CALLER-MARKINGS HAVE BEEN WASHED AWAY

Company     : MICA CORP.
Contact Name: ROBERT BOBO           Contact Phone: (713)896-4288
Alt. Contact: SAME                  Alt. Phone   :
Work Being Done For: TX DOT
State: TX              County: HARRIS
Map: TX    Page: 529   Grid Cells: U
Map: TX    Page: 529   Grid Cells: E,F,G,J,K,L,M,N,P,Q,R,S,T,U,V,Y,H,W,X,Z
Explosives: N
PSCOPE01   RELIAN01   TVMAX01    XMICA01    XOCS01
Send To: TWC30     Seq No: 0034   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE
Ticket No: 29401409
Transmit      Date: 10/21/02      Time: 09:58AM  Op: jamie
Original Call Date: 10/21/02      Time: 09:57AM  Op: jamie
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: HOUSTON CITY
Address:   6802      Street: SEINFELD CT
Nearest Intersecting Street: CHAMPIONS PARK

Type of Work: INSTALLATION OF SPRINKLER SYSTEMS
Extent of Work: MARK ENTIRE LOT.
Remarks:

Company     : GREEN TURF IRRIGATION
Contact Name: PAULA H               Contact Phone: (281)376-6951
Alt. Contact: MEL H /CELL PHONE     Alt. Phone   : (832)250-7980
Work Being Done For: PATTY EASON
State: TX              County: HARRIS
Map: TX    Page: 370   Grid Cells: B
Map: TX    Page: 370   Grid Cells: A,B
Explosives: N
LOG01      RELIAN01
Send To: TWC10     Seq No: 0055   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE                  CORRECTION
Ticket No: 29401304
Transmit      Date: 10/21/02      Time: 09:59AM  Op: txcaro
Original Call Date: 10/21/02      Time: 09:51AM  Op: txcaro
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: KATY CITY
Address:             Street: CROSS GROVE LN
Nearest Intersecting Street: SICA HOLLOW LN

Type of Work: INSTALLING UNDERGROUND GAS AND ELECTRIC LINE
Extent of Work: START MARKING FROM THE INTERSECTION GOING NORTHEAST. MARK
: THE FRONT OF PROPERTY AND THE ROAD RIGHT OF WAY ON BOTH SIDES OF THE ROAD
: INCLUDING THE ENTIRE ROAD FOR 300 FEET.

Remarks: FAXED LOCATE REQUEST JOB:33486336 SUB:FALCON RANCH SECTION THREE
: FAX TO LONE STAR 10/21     COR PER REMARK

Company     : NORTH HOUSTON POLE LINE
Contact Name: PHILLIP TAMBORELLO    Contact Phone: (713)691-3616
Alt. Contact: CALL 1ST              Alt. Phone   : (713)333-2134
Work Being Done For: RELIANT ENERGY HLP
State: TX              County: FORT BEND
Map: TX    Page: 485   Grid Cells: N
Map: *MORE Page: 485   Grid Cells: J,K,N,P,S,T
Explosives: N
RELIAN01   TXUC01     XNHP01
Send To: TWC30     Seq No: 0035   Map Ref:





NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE
Ticket No: 29401431               Update Of: 28402836
Transmit      Date: 10/21/02      Time: 09:59AM  Op: fxlisa
Original Call Date: 10/21/02      Time: 09:59AM  Op: fxlisa
Work to Begin Date: 10/23/02      Time: 10:00AM

Place: HOUSTON CITY
Address: 200610      Street: LOUETTA
Nearest Intersecting Street: FM 249

Type of Work: DRILL, BORE, TRENCH
Extent of Work: MARK NORTH AND SOUTH SIDES OF EAST AND WEST BOUND LOUETTA
: BLVD. STARTING AT FM 249 AND ENDING AT OLD LOUETTA.

Remarks: CALLER GAVE MAPSCO:329 T,U,V/DIGGING MORE THAN 16 INCHES DEEP:NO
: FAXED LOCATE REQUEST         UPDATE PER MARKINGS WASHED AWAY

Company     : MICA CORP.
Contact Name: ROSIE ARRATIA         Contact Phone: (713)896-4288
Alt. Contact: ROBERT BOBO           Alt. Phone   :
Work Being Done For: TX D.O.T.
State: TX              County: HARRIS
Map: TX    Page: 329   Grid Cells: T
Map: TX    Page: 330   Grid Cells: N,S
Explosives: N
RELIAN01   XMICA01    XOCS01
Send To: TWC10     Seq No: 0056   Map Ref:



