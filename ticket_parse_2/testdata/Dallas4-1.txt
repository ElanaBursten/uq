

SEQUENCE NUMBER 0295   CDC = BY1
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TAMU - TELECOMM  BRYAN ELECTRIC   ATMOS-MIDTX-UQ   CITY COL STAT  
ENERGY TRANSFER  VERIZON-UNIV     MANAGED NETWORK  QWEST COMM.    
SDL-BRYAN        SPRINT NEXTEL    LONE STAR XCHG

Locate Request No. 072414585

Prepared By CHRISTINA T          On 29-AUG-07  At 1522

MapRef :                            Grid: 303700096210A  Footprint: D12

Location:     County: BRAZOS  Town: COLLEGE STATION

             Address: 3403 F & B RD 

Beginning Work Date 08/31/07 Time of Day: 03:30 pm   Duration: 07 DAYS 

Fax-A-Locate Date          at 

Excavation Type : OTHER                         
Nature of Work  : DEMOLITION                    

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : JERRY FALER
Company Name   : CST ENVIRONMENTAL
Work by CST ENVIRONMENTAL      For TEXAS A&M

Person to Contact : SEVERO BALLESTEROS

Phone No.  ( 281 )541-8629 /( 281 )449-5911 ( Hours: 06:00 am/04:00 pm )
Fax No.    ( 281 )443-3469
Email:     JFALER@CSTENV.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: WELLBORN RD 
WORK DATE: 2 WORKING DAY NOTICE   MAPSCO:   SUBDIVISION:   GATE CODE: 
  GRIDS:   LAT/LONG:   MARK ENTIRE PROPERTY AROUND HOUSE.  WORK INVOLV
ES DEMOLITION OF EXISTING STRUCTURE ABOVE AND BELOW GRADE, TO INCLUDE 
FOUNDATIONS AND UNDERGROUND UTILITIES.  707334.XML                    
                                                                      
                                                                      
                                                                      
JFALER@CSTENV.COM

Map Cross Reference : GTMAPS 375,R                        

FaxBack Requested ? YES    Lone Star Xref: 


072414585 to EMAIL ADDRESS at 15:31:29 on WED, 08/29/07 for BY1 #0295



SEQUENCE NUMBER 0301   CDC = BY1
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TAMU - TELECOMM  BRYAN ELECTRIC   ATMOS-MIDTX-UQ   COLLEGE STATION
CITY COL STAT    ENERGY TRANSFER  VERIZON-UNIV     MANAGED NETWORK
QWEST COMM.      SDL-BRYAN        SPRINT NEXTEL    LONE STAR XCHG

Locate Request No. 072414626

Prepared By CHRISTINA T          On 29-AUG-07  At 1525

MapRef :                            Grid: 303700096210A  Footprint: D11

Location:     County: BRAZOS  Town: COLLEGE STATION

             Address: 3454 F & B RD 

Beginning Work Date 08/31/07 Time of Day: 03:30 pm   Duration: 07 DAYS 

Fax-A-Locate Date          at 

Excavation Type : OTHER                         
Nature of Work  : DEMOLITION                    

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : JERRY FALER
Company Name   : CST ENVIRONMENTAL
Work by CST ENVIRONMENTAL      For TEXAS A&M

Person to Contact : SEVERO BALLESTEROS

Phone No.  ( 281 )541-8629 /( 281 )449-5911 ( Hours: 06:00 am/04:00 pm )
Fax No.    ( 281 )443-3469
Email:     JFALER@CSTENV.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: WELLBORN RD 
WORK DATE: 2 WORKING DAY NOTICE   MAPSCO:   SUBDIVISION:   GATE CODE: 
  GRIDS:   LAT/LONG:   MARK ENTIRE PROPERTY AROUND HOUSE.  WORK INVOLV
ES THE DEMOLITION OF THE EXISTING STRUCTURE ABOVE AND BELOW GRADE, TO 
INCLUDE FOUNDATIONS AND UNDERGROUND UTILITIES.  707336.XML            
                                                                      
                                                                      
                                                                      
JFALER@CSTENV.COM

Map Cross Reference : GTMAPS 375,R                        

FaxBack Requested ? YES    Lone Star Xref: 


072414626 to EMAIL ADDRESS at 15:31:43 on WED, 08/29/07 for BY1 #0301

