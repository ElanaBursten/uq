
USW01  00027 GAUPC 12/17/01 08:25:08 12171-039-011-000 NORMAL
Underground Notification
Ticket : 12171-039-011 Date: 12/17/01 Time: 08:17 Revision: 000

State: GA  County: FULTON       Place: ROSWELL
Addr : From: 8930   To:        Name:    MARTIN                         RD
Cross: From:        To:        Name:
Offset:
Subdivision: MARTINS LANDING
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC PERIMETER OF HOME

Grids    : 3400D8418A   3400C8418A   3400B8418A   3400A8418A   3400D8419D
Grids    : 3400B8419D   3400D8419C   3400B8419C   3400D8419B   3400C8419B
Grids    : 3400B8419B   3400C8419A   3400C8418C   3400B8418C   3400A8418C
Grids    : 3400C8418B   3400B8418B   3400A8418B   3359A8419D   3359A8419C
Grids    : 3359A8419B
Work type: INSTL SENTRICON
Work date: 12/20/01 Time: 07:00 Hrs notc: 070 Priority: 3
Legal day: 12/20/01 Time: 07:00 Good thru: 01/07/02 Restake by: 01/02/02
RespondBy: 12/19/01 Time: 23:59 Duration : 1 HOUR
Done for : ERIC LAIDHOLD
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   MARTINS LANDING DR

Company : COOKS PEST CONTROL                        Type: CONT
Co addr : 1830 ATKINSON RD
City    : LAWRENCEVILLE                  State   : GA Zip: 30043
Caller  : CHRIS ESERY                    Phone   : 770-277-5470
Fax     :                                Alt. Ph.: 770-277-5470
Email   : ATLANTAN.E@MINDSPRING.COM

Submitted date: 12/17/01 Time: 08:17 Oper: 039 Chan: WEB
Mbrs : ACS02  AGL14  AGL19  BSNW   CBL01  DOTI   FUL01  FUL02  GP801B GP814
: OMN01  SAW71  USW01
-------------------------------------------------------------------------------
TKD17082.804 #00031
TKD17082.804
Page & Grid: 180 D9

USW01  00030 GAUPC 12/17/01 08:25:32 12171-037-027-000 NORMAL RESTAKE
Underground Notification
Ticket : 12171-037-027 Date: 12/17/01 Time: 08:24 Revision: 000
Old Tkt: 11291-027-013 Date: 12/04/01 Time: 00:06

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 122    To:        Name:    CLEVELAND                      AV
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE ENTIRE PROPERTY

Grids    : 3340A8422A   3340A8423D   3340A8423C   3340A8423B   3340A8423A
Grids    : 3340A8424D   3340A8424C   3340A8424B
Work type: CLEANING OUT RETENTION POND
Work date: 11/16/01 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 12/20/01 Time: 07:00 Good thru: 01/07/02 Restake by: 01/02/02
RespondBy: 12/19/01 Time: 23:59 Duration : 1 WEEK
Done for : CONSTRUCTION DIMENSIONS INC
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : RESTAKE OF: 11131-020-036
: RESTAKE OF: 11291-027-013
: *** NEAR STREET ***   OLD HAPEVILLE RD
: *** LOOKUP BY MANUAL

Company : RANDY WILLIAMS GRADING                    Type: CONT
Co addr : 718 EAST HWY 78
City    : TEMPLE                         State   : GA Zip: 30179
Caller  : MIKKI WILSON                   Phone   : 770-562-1440
Fax     :                                Alt. Ph.:

Submitted date: 12/17/01 Time: 08:24 Oper: 037 Chan: WEB
Mbrs : AGL10  AGL18  ATT02  ATTBB  BSCA   DOTI   EPT50  EPT52  FUL02  GP141B
: GP145  OMN01  USW01
-------------------------------------------------------------------------------
TKD17082.828 #00034
TKD17082.828
Page & Grid: 299 D3

USW01  00050 GAUPC 12/18/01 07:55:06 12181-020-017-000 EMERG
Underground Notification
Ticket : 12181-020-017 Date: 12/18/01 Time: 07:50 Revision: 000

State: GA  County: DEKALB       Place: ATLANTA
Addr : From: 1539   To:        Name:    CECILIA                        DR   SE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE IN FRONT OF THE CHURCH AND ENTIRE PROPERTY

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3343D8419B   3343D8419A
Work type: REPR SWR SVC
Work date: 12/18/01 Time: 07:52 Hrs notc: 000 Priority: 1
Legal day: 12/18/01 Time: 07:52 Good thru: 12/18/01 Restake by: 12/18/01
RespondBy: 12/18/01 Time: 07:52 Duration : 2 HRS
Done for : DEKALB COUNTY
Crew on Site: Y  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : EMERGENCY: SERVICE IS OUT
: *** NEAR STREET ***   BRANNEN RD

Company : DEKALB CNTY WTR & SWR                     Type: MEMB
Co addr : 1580 ROADHAVEN DR.
City    : WADE DAVIS                     State   : GA Zip: 30083
Caller  : ERICA KING                     Phone   : 770-270-6243
Fax     :                                Alt. Ph.:
Contact : HOLCOMB

Submitted date: 12/18/01 Time: 07:50 Oper: 020 Chan: WEB
Mbrs : AGL10  ATTBB  BSCA   DCTS01 DCWS01 DOTI   GP832  USW01
-------------------------------------------------------------------------------
TKD18075.739 #00056
TKD18075.739
Page & Grid: 276 B8

USW01  00012 GAUPC 12/27/01 00:18:36 12211-098-001-001 NORMAL LATE
Underground Notification
Ticket : 12211-098-001 Date: 12/27/01 Time: 00:16 Revision: 001
Old Tkt: 12211-098-001 Date: 12/20/01 Time: 15:05

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name: W  MARIETTA                       ST   NW
Cross: From:        To:        Name:    RICE                           ST   NW
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC ENTIRE INTERSECTION - 200FT IN ALL DIRECTIONS - BOTH SIDES OF ROAD

Grids    : 3346A8424A   3346A8425D   3347D8425D   3347D8425C   3347D8425B
Grids    : 3347D8425A   3347D8426D   3347C8426D
Work type: KILLING A GAS SERVICE
Work date: 12/27/01 Time: 07:00 Hrs notc: 006 Priority: 3
Legal day: 12/27/01 Time: 07:00 Good thru: 01/10/02 Restake by: 01/07/02
RespondBy: 12/26/01 Time: 23:59 Duration : 1DAY
Done for : AGL
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : *** USW01 IS LATE RESPONDING.  PLEASE RESPOND ASAP.
: *** WILL BORE N0NE

Company : ATLANTA GAS LIGHT                         Type: CONT
Co addr : 1251  CAROLINE ST
City    : ATLANTA                        State   : GA Zip: 30307
Caller  : RANDY HEATH                    Phone   : 404-584-4464
Fax     :                                Alt. Ph.:

Submitted date: 12/27/01 Time: 00:16 Oper: 098 Chan: WEB
Mbrs : AGL10  AGL18  ATTBB  BSCA   DOTI   FUL02  GP105  LEV3   MCI02  MEA70
: OMN01  QWEST8 SPR99  USW01  WCI01
-------------------------------------------------------------------------------
TKD27002.132 #00017
TKD27002.132

USW01  00039 GAUPC 12/17/01 08:34:28 12141-947-017-001 NORMAL CANCEL
Underground Notification
Ticket : 12141-947-017 Date: 12/17/01 Time: 08:33 Revision: 001
Old Tkt: 12141-947-017 Date: 12/14/01 Time: 12:04

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    GLENRIDGE                      DR   NE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE BOTH SIDES OF GLENRIDGE DR FROM HAMMOND DR TO JOHNSON FERRY RD

Grids    : 3355C8421A   3355D8422D   3355C8422D
Work type: BURING SVC CABLE
Work date: 12/19/01 Time: 07:00 Hrs notc: 046 Priority: 3
Legal day: 12/19/01 Time: 07:00 Good thru: 01/04/02 Restake by: 12/31/01
RespondBy: 12/18/01 Time: 23:59 Duration : ?
Done for : BELLSOUTH
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : WRONG COMPANY ENTERED FOR WORK BEING DONE FOR
: CANCELED, PLEASE DISREGARD
: *** WILL BORE ROAD, DRIVEWAY & SIDEWALK
: *** LOOKUP BY MANUAL

Company : ANSCO & ASSOCIATES, INC.                  Type: CONT
Co addr : 830 PETTY RD
City    : LAWRENCEVILLE                  State   : GA Zip: 30043
Caller  : SHERRI DREW                    Phone   : 770-513-1975
Fax     : 770-513-4640                   Alt. Ph.:
Contact : BRET/HAMMOND DR&GLENRIDGE DR

Submitted date: 12/17/01 Time: 08:33 Oper: 947 Chan: 999
Mbrs : ACS02  ACS04  AGL18  AGL19  ATT02  ATTBB  BSNE   DOTI   FUL02  GP801B
: LEV3   MCI02  NU804  OMN01  USW01  XOC90
-------------------------------------------------------------------------------
TKD17083.725 #00043
TKD17083.725
Page & Grid: 203 F10

USW01  00020 GAUPC 12/21/01 08:19:59 12211-035-006-000 EMERG
Underground Notification
Ticket : 12211-035-006 Date: 12/21/01 Time: 08:03 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 2226   To:        Name:    BOLLINGBROOK                   DR
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC ENTIRE FTYD

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3343A8427A   3344D8427A
Work type: RPL WTR SERV
Work date: 12/21/01 Time: 08:12 Hrs notc: 000 Priority: 1
Legal day: 12/21/01 Time: 08:12 Good thru: 12/21/01 Restake by: 12/21/01
RespondBy: 12/21/01 Time: 08:12 Duration : 1 DA
Done for : RALPH HAMILTON
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : CREW IN ROUTE
: EMERGENCY: SERVICE IS OUT
: *** NEAR STREET ***   BEECHER CIR

Company : ZACHARYS PLUMBING SERVICE                 Type: CONT
Co addr : 1941 FAIRBURN RD
City    : ATLANTA                        State   : GA Zip: 30331
Caller  : KEITH ZACHERY                  Phone   : 678-428-0924
Fax     :                                Alt. Ph.:
Contact : KEITH ZACHERY

Submitted date: 12/21/01 Time: 08:03 Oper: 035 Chan: WEB
Mbrs : AGL10  AGL18  ATTBB  BSCA   DOTI   FUL02  GP131  OMN01  USW01
-------------------------------------------------------------------------------
TKD21082.255 #00025
TKD21082.255
Page & Grid: 274 C6

USW01  00084 GAUPC 12/18/01 08:30:22 12181-097-036-000 NORMAL UPDATE
Underground Notification
Ticket : 12181-097-036 Date: 12/18/01 Time: 08:28 Revision: 000
Old Tkt: 12031-039-017 Date: 12/06/01 Time: 00:08

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    NORTHSIDE                      DR   NW
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC ON W SIDE OF NORTHSIDE DR FROM INTER OF 14TH TO 500FT N  TO ENTRANCE
: TO WATER TREATMENT PLANT  FROM WEST CURB LINE TO CENTER OF ST

Grids    : 3347D8424C   3347C8424C
Work type: INSTL WTR MAIN
Work date: 12/21/01 Time: 07:00 Hrs notc: 070 Priority: 3
Legal day: 12/21/01 Time: 07:00 Good thru: 01/08/02 Restake by: 01/03/02
RespondBy: 12/20/01 Time: 23:59 Duration : 1 MONTHS
Done for : CITY OF ATLANTA
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : RESTAKE OF: 11151-104-016
: *** NEAR STREET ***   14TH ST
: *** LOOKUP BY MANUAL

Company : W. L  HAILEY   &  COMPANY  INC            Type: CONT
Co addr : P O BOX 14026
City    : ATLANTA                        State   : GA Zip: 30324
Caller  : MARCUS MOORE                   Phone   : 404-875-2871
Fax     : 770-621-0238                   Alt. Ph.: 770-621-0238

Submitted date: 12/18/01 Time: 08:28 Oper: 097 Chan: WEB
Mbrs : ACS03  ACS04  AGL10  AGL11  AGL18  ATTBB  BSCA   DOTI   FUL02  GP106
: MCI02  OMN01  USW01  WCI01
-------------------------------------------------------------------------------
TKD18083.317 #00090
TKD18083.317
Page & Grid: 251 A9

USW01  00107 GAUPC 12/18/01 08:42:54 12181-097-041-000 NORMAL UPDATE
Underground and Overhead Notification
Ticket : 12181-097-041 Date: 12/18/01 Time: 08:31 Revision: 000
Old Tkt: 12031-039-022 Date: 12/06/01 Time: 00:08

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    14TH                           ST   NW
Cross: From:        To:        Name:    CURRAN                         ST
Offset:

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    14TH                           ST   NW
Cross: From:        To:        Name:    CENTER                         ST
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC EASTBOUND OF 14TH ST FROM INTERSECT OF CURRAN ST TO INTERSECT OF
: CENTER  ST LOC CURBLINE TO CURBLINE.

Grids    : 3347D8423A   3347D8424D   3347D8424C
Work type: INSTL WTR MAIN
Work date: 12/21/01 Time: 07:00 Hrs notc: 070 Priority: 3
Legal day: 12/21/01 Time: 07:00 Good thru: 01/08/02 Restake by: 01/03/02
RespondBy: 12/20/01 Time: 23:59 Duration : 20DYS
Done for : CITY OF ATLANTA
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : RESTAKE OF: 10301-096-051
: RESTAKE OF: 11151-102-006
: OVERHEAD WORK BEGIN DATE: 10/10/01
: OVERHEAD WORK COMPLETION DATE: 10/14/01
: *** LOOKUP BY MANUAL

Company : WL HAILEY AND COMPANY                     Type: CONT
Co addr : 2436 CHESHIRE BRIDGR RD NORTH
City    : ATLANTA                        State   : GA Zip: 30324
Caller  : MARCUS MOORE                   Phone   : 404-248-9355
Fax     :                                Alt. Ph.:

Submitted date: 12/18/01 Time: 08:31 Oper: 097 Chan: WEB
Mbrs : ACS03  ACS04  AGL10  AGL11  AGL18  ATTBB  BSCA   DOTI   FUL02  GP106
: HMGCT1 HMMED1 MCI02  OACS02 OGP101 OMN01  USW01  WCI01
-------------------------------------------------------------------------------
TKD18084.552 #00113
TKD18084.552
Page & Grid: 251 B9

USW01  00135 GAUPC 12/18/01 09:32:08 12181-511-018-000 NORMAL
Underground Notification
Ticket : 12181-511-018 Date: 12/18/01 Time: 09:25 Revision: 000

State: GA  County: FULTON       Place: FAIRBURN
Addr : From: 3800   To:        Name:    COCHRAN                        DR   UNDE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE ENTIRE PROPERTY

Grids    : 3339A8438B   3339B8438A   3338A8439D   3339B8439D   3338A8439C
Grids    : 3339D8439C   3339C8439C   3339C8439B   3339C8439A   3339C8440D
Work type: BSW
Work date: 12/21/01 Time: 07:00 Hrs notc: 069 Priority: 3
Legal day: 12/21/01 Time: 07:00 Good thru: 01/08/02 Restake by: 01/03/02
RespondBy: 12/20/01 Time: 23:59 Duration :
Done for : BST
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : RATREE RD AND CASCADE PALMETTO HWY
: *** NEAR STREET ***   RATREE RD
: *** LOOKUP BY MANUAL

Company : BELLSOUTH                                 Type: MEMB
Co addr : 700 POWERS FERRY RD
City    : ATLANTA                        State   : GA Zip: 30067
Caller  : GAYLA WILDER                   Phone   : 770-578-3114
Fax     :                                Alt. Ph.:

Submitted date: 12/18/01 Time: 09:25 Oper: 511 Chan: 999
Mbrs : AGL18  ATTBB  BSNW   BSSA   DOTI   DOU90  FCA02  FUL02  GRS70  OMN01
: SNG80  USW01
-------------------------------------------------------------------------------
TKD18093.503 #00141
TKD18093.503
Page & Grid: 295 F6

ABS01  00201 GAUPC 12/18/01 10:43:46 12181-106-010-000 DAMAGE
Underground Notification
Ticket : 12181-106-010 Date: 12/18/01 Time: 10:40 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    SANDY SPRINGS                  CIR  NE
Cross: From:        To:        Name:    CLIFTWOOD                      DR
Offset:

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    SANDY SPRINGS                  CIR  NE
Cross: From:        To:        Name:    HAMMOND                        DR   NE
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC BOTH SIDES OF SANDY SPRINGS CIR FROM CLIFTWOOD DR TO HAMMOND DR

Grids    : 3354A8422A   3355D8422A   3354A8423D   3355D8423D
Work type: INSTL GAS MAIN/SERV LINE
Work date: 12/18/01 Time: 10:41 Hrs notc: 000 Priority: 6
Legal day: 12/18/01 Time: 10:41 Good thru: 12/18/01 Restake by: 12/18/01
RespondBy: 12/18/01 Time: 10:41 Duration : 1-2 WKS
Done for : ATLANTA GAS LIGHT
Crew on Site: Y  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : SECOND REQUEST TO LOCATE ALL THE UTILITIES IN THEN AREA   ASAP   CREW
: IS ON     SITE AND THERE ARE NO MARKS     !!!!!!!!
: RESTAKE OF: 12031-017-034
: MAIN LINE DAMAGE TO UNKNOWN **ON THE SIDE OF THE ROAD  CALLER SD IT
: WAS A STREET LIGHT ** **EXTENT: CUT IN HALF  **----------DAMAGE OF
: 12031-017-034 WHICH WAS UPDATED BY 12181-098-044
: *** WILL BORE BOTH SIDES OF ROAD
: *** LOOKUP BY MANUAL

Company : COFER AND JACKSON PIPELINE                Type: CONT
Co addr : 478 NORTHDALE RD
City    : LAWRENCVILLE                   State   : GA Zip: 30019
Caller  : CHARLOTTE AUSTIN               Phone   : 770-277-2180
Fax     :                                Alt. Ph.:
Contact : TODD

Submitted date: 12/18/01 Time: 10:40 Oper: 106 Chan: 999
Mbrs : ABS01  ACS04  AGL18  AGL19  ATT02  ATTBB  BSNE   DOTI   FUL02  GP801B
: ICG01  LEV3   MCI02  NU804  OMN01  USW01  XOC90
-------------------------------------------------------------------------------
TKD18104.618 #00207
TKD18104.618
Page & Grid: 227 D1

USW01  00217 GAUPC 12/18/01 10:47:11 12181-034-015-000 NORMAL UPDATE
Underground and Overhead Notification
Ticket : 12181-034-015 Date: 12/18/01 Time: 10:39 Revision: 000
Old Tkt: 11291-035-033 Date: 12/04/01 Time: 00:07

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    EDGEWOOD                       AV   SE
Cross: From:        To:        Name:    COURTLAND                      ST   NE
Offset:

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    EDGEWOOD                       AV   SE
Cross: From:        To:        Name:    PIEDMONT                       AV   NE
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC B/S OF EDGEWOOD AV FROM COURTLAND ST TO PIEDMONT AV

Grids    : 3345D8422A   3345C8422A   3345D8423D   3345C8423D
Work type: INSTALL SANITARY SEWER AND STORM DRAIN
Work date: 12/21/01 Time: 07:00 Hrs notc: 068 Priority: 3
Legal day: 12/21/01 Time: 07:00 Good thru: 01/08/02 Restake by: 01/03/02
RespondBy: 12/20/01 Time: 23:59 Duration : 1 MTH
Done for : BYERS CONSTRUCTION/GA STATE
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : BORING MANHOLE
: RESTAKE OF: 10261-018-106
: RESTAKE OF: 11131-101-002
: * LGN01 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
: *** WILL BORE SIDEWALK
: OVERHEAD WORK BEGIN DATE: 10/31/01
: OVERHEAD WORK COMPLETION DATE: 10/15/01
: *** LOOKUP BY MANUAL

Company : TAYCO CONTRACTORS                         Type: CONT
Co addr : PO BOX 517 3221-B HILL ST SUITE 101
City    : DULUTH                         State   : GA Zip: 30136
Caller  : MIKE ROBINSON                  Phone   : 770-623-0457
Fax     : 770-623-5763                   Alt. Ph.:

Submitted date: 12/18/01 Time: 10:39 Oper: 034 Chan: WEB
Mbrs : ACS03  AGL10  AGL11  AGL18  ATT02  ATTBB  BSCA   DOTI   EPC01  FUL02
: GP103  GP104  HMGCT1 HMMED1 IFN01  LEV3   LGN01  MCI02  MFN02  MT103
: MT104  NU104  OACS02 OGP101 OMN01  QWEST8 ST005  TWT90  USW01  WCI01
-------------------------------------------------------------------------------
TKD18105.009 #00223
TKD18105.009
Page & Grid: 275 D4

USW01  00306 GAUPC 12/18/01 12:34:59 12181-052-015-000 NORMAL RESTAKE
Underground Notification
Ticket : 12181-052-015 Date: 12/18/01 Time: 12:30 Revision: 000
Old Tkt: 12031-037-053 Date: 12/06/01 Time: 00:08

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    TIMBER                         TRL  S
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE TIMBER TRAIL SOUTH FROM TIMBERLAND DRIVE FOR 260 FEET SOUTH.
: LOCATE ALL INTERSECTIONS, INCLUDING BUT NOT LIMITED TO TIMBERLAND DRIVE.
: PAINT AND  STAKE BOTH SIDES OF STREET, ALL UTILITIES, MAINS AND SERVICE
: LINES. LOCATE ALL INTERSECTIONS 200 FEET IN ALL  DIRECTIONS. LOCATE AS
: REQUESTED. NO VERBAL MODIFICATIONS. POSSIBLE BORE.

Grids    : 3353B8422C
Work type: INSTALL/MAINTAIN GAS MAIN
Work date: 11/02/01 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 12/21/01 Time: 07:00 Good thru: 01/08/02 Restake by: 01/03/02
RespondBy: 12/20/01 Time: 23:59 Duration :
Done for : ATLANTA GAS LIGHT
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : SEE LOCATE INSTRUCTIONS.
: RESTAKE OF 10121-965-011.
: RESTAKE OF: 10301-965-003
: RESTAKE OF: 11151-022-017
: RESTAKE OF: 12031-037-053
: *** NEAR STREET ***   TIMBERLAND DR
: *** WILL BORE ROAD, DRIVEWAY & SIDEWALK
: *** LOOKUP BY MANUAL

Company : BENTON - GEORGIA, INC.                    Type: CONT
Co addr : P.O. BOX 838
City    : DOUGLASVILLE                   State   : GA Zip: 30133
Caller  : TARA WYSNER                    Phone   : 770-942-8180
Fax     :                                Alt. Ph.:
Email   : TWYSNER@BENTON-GEORGIA.COM

Submitted date: 12/18/01 Time: 12:30 Oper: 052 Chan: WEB
Mbrs : AGL18  AGL19  ATTBB  BSNE   DOTI   FUL02  GP801B OMN01  USW01
-------------------------------------------------------------------------------
TKD18123.756 #00312
TKD18123.756
Page & Grid: 227 E4

USW01  00041 GAUPC 12/28/01 08:37:56 12281-039-011-000 EMERG
Underground Notification
Ticket : 12281-039-011 Date: 12/28/01 Time: 08:35 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 3585   To:        Name:    GLENVIEW                       CIR  SW
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC FRONT OF PROPERTY

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3342D8430D   3342D8430C   3342C8430C
Work type: REPLACE SEWER SERVICE
Work date: 12/28/01 Time: 08:36 Hrs notc: 000 Priority: 1
Legal day: 12/28/01 Time: 08:36 Good thru: 12/28/01 Restake by: 12/28/01
RespondBy: 12/28/01 Time: 08:36 Duration : 1 DAY
Done for : ANGELICE FULLHAM
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : EMERGENCY: SERVICE OUT/REF TO ADJOINING TICKET 12281-039-010
: *** NEAR STREET ***   FAIRBURN RD
: *** LOOKUP BY MANUAL

Company : ZACHARYS PLUMBING SERVICE                 Type: CONT
Co addr : 1941 FAIRBURN RD
City    : ATLANTA                        State   : GA Zip: 30331
Caller  : KEITH ZACHERY                  Phone   : 678-428-0924
Fax     :                                Alt. Ph.:
Contact : KEITH ZACHERY

Submitted date: 12/28/01 Time: 08:35 Oper: 039 Chan: WEB
Mbrs : AGL10  AGL18  ATTBB  BSCA   DOTI   FUL02  GP131  OMN01  USW01
-------------------------------------------------------------------------------
TKD28082.826 #00046
TKD28082.826
Page & Grid: 273 F10

USW01  00031 GAUPC 12/17/01 08:25:40 12171-028-018-000 NORMAL
Underground Notification
Ticket : 12171-028-018 Date: 12/17/01 Time: 08:21 Revision: 000

State: GA  County: DEKALB       Place: ATLANTA
Addr : From: 1364   To:        Name:    EASTGATE                       CT
Cross: From:        To:        Name:
Offset:
Subdivision: EASTGATE
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE POLE IN FRONT

Grids    : 3343B8420B   3343A8420B   3344D8420B
Work type: RELOCATE ST LITE POLES
Work date: 12/20/01 Time: 07:00 Hrs notc: 070 Priority: 3
Legal day: 12/20/01 Time: 07:00 Good thru: 01/07/02 Restake by: 01/02/02
RespondBy: 12/19/01 Time: 23:59 Duration : 3 HRS
Done for : GA POWER
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : *** NEAR STREET ***   STOKESWOOD AV
: *** LOOKUP BY MANUAL

Company : GEORGIA POWER COMPANY                     Type: MEMB
Co addr : 1790 MONTREAL CIR
City    : TUCKER                         State   : GA Zip: 30084
Caller  : MIKE NOLAND                    Phone   : 770-621-2437
Fax     :                                Alt. Ph.:

Submitted date: 12/17/01 Time: 08:21 Oper: 028 Chan: WEB
Mbrs : AGL10  ATTBB  BSCA   DCTS01 DCWS01 DOTI   GP832  USW01
-------------------------------------------------------------------------------
TKD17082.835 #00035
TKD17082.835
Page & Grid: 275 H7

USW01  00138 GAUPC 12/21/01 12:14:09 12211-032-041-000 EMERG
Underground Notification
Ticket : 12211-032-041 Date: 12/21/01 Time: 11:55 Revision: 000

State: GA  County: FULTON       Place: ATLANTA
Addr : From: 2900   To:        Name:    LOOKOUT                        PL   NE
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOC ENTIRE PROP

Emergency *** Urgent work *** GAAS main & service lines
Grids    : 3349A8422C   3350D8422C   3350C8422C   3349A8422B   3350D8422B
Grids    : 3350C8422B
Work type: RPL SWR SERV
Work date: 12/21/01 Time: 11:57 Hrs notc: 000 Priority: 1
Legal day: 12/21/01 Time: 11:57 Good thru: 12/21/01 Restake by: 12/21/01
RespondBy: 12/21/01 Time: 11:57 Duration : SEV DA
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : PER CALLER GAS HAS NOT BEEN MARKED, PLS MARK, SEE OLD TKT #
: 12191-035-033
: EMERGENCY: SWG IS BACKING UP,  CREW ON SITE TODAY WITHIN 1 HR
: *** NEAR STREET ***   PHARR RD
: *** WILL BORE N0NE
: *** LOOKUP BY MANUAL

Company :                                           Type: HOME
Co addr :
City    :                                State   :
Caller  : MARGARITA TSELESIN             Phone   : 404-261-1271
Fax     :                                Alt. Ph.:
Contact : DMITRY CHOLAK
Fax     : 678-361-6889

Submitted date: 12/21/01 Time: 11:55 Oper: 032 Chan: WEB
Mbrs : ACS03  AGL18  AGL19  ATT02  ATTBB  BSCA   DOTI   FUL02  GP107  ICG01
: LEV3   MCI02  NU107  OMN01  USW01
-------------------------------------------------------------------------------
TKD21121.644 #00143
TKD21121.644
Page & Grid: 251 E3


