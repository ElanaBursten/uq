Idaho Dig Line
NOTICE OF INTENT TO EXCAVATE                   Header Code: STANDARD LOCATE
                                              Request Type: CORRECTION
Ticket No:    2004051120 Seq. No: 20
Update of:             
Original Call Date:     04/03/2006     Time:      09:15:36 AM  OP: 187
Transmit Date:          04/03/2006     Time:      09:22:28 AM
Locate By Date:         04/05/2006     Time:      09:15:00 AM

Company:           QUALITY ELECTRIC                       
Contact Name:      BRIAN LANGE                Contact Phone:   (208)375-1300
Alternate Contact:                            Alternate Phone:

Best Time to Call:                            Fax No:          (208)377-1329
Cell Phone:        (208)871-7783              Pager No:


State: ID       County: ADA             City: BOISE CITY
Address:          , S PEPPERTREE AVE                       
To Address:      
Nearest Intersecting Street: E GRAND FOREST DR                      
2nd Intersecting Street:     E PECAN ST                             
Subdivision:

Latitude: 43.54497400     Longitude: -116.13977350

Location of Work: THIS IS A TEST PLEASE DO NOT PROCESS THIS IS A   
                  TEST PLEASE DO NOT PROCESS THIS IS A TEST PLEASE 
                  DO NOT PROCESS                                   


Remarks: THIS IS A TEST PLEASE DO NOT PROCESS             
         TEST CORRECTION                                  

Type of Work: INSTALL TRAFFIC LIGHT(S)                    
Private Property: Y    Street:            Y   Legal Given: Y   Blasting: 
Easement:              Mechanical Boring:     Premarked:      
Excavator/Owner: QUALITY ELECTRIC                                 
Sending to: (listing of utilities tkt sent to)
*IDPOCL00       IPC01           QLNID51         IMG01           LVLCOM01
LVLCOM00       

                                 FOR MEMBER USE ONLY
Ticket No: 2004051120
Located by____________________________________Date of
Location_________________
Remarks:____________________________________________________________________
___
____________________________________________________________________________
___
____________________________________________________________________________
___
Excavator Notified (Not located)__________________ Who
Notified________________
Notified
by:__________________________________Date:______________Time:_________


Idaho Dig Line
NOTICE OF INTENT TO EXCAVATE                   Header Code: EMERGENCY
                                              Request Type:
Ticket No:    2004051121 Seq. No: 2
Update of:             
Original Call Date:     04/03/2006     Time:      09:22:32 AM  OP: 187
Transmit Date:          04/03/2006     Time:      09:22:44 AM
Locate By Date:         04/03/2006     Time:      09:37:00 AM

Company:           QUALITY ELECTRIC                       
Contact Name:      BRIAN LANGE                Contact Phone:   (208)375-1300
Alternate Contact:                            Alternate Phone:

Best Time to Call:                            Fax No:          (208)377-1329
Cell Phone:        (208)871-7783              Pager No:


State: ID       County: ADA             City: BOISE CITY
Address:          , S PEPPERTREE AVE                       
To Address:      
Nearest Intersecting Street: E GRAND FOREST DR                      
2nd Intersecting Street:     E PECAN ST                             
Subdivision:

Latitude: 43.54497400     Longitude: -116.13977350

Location of Work: THIS IS A TEST PLEASE DO NOT PROCESS             
                                                                   
                  THIS IS A TEST PLEASE DO NOT PROCESS             
                                                                   
                  THIS IS A TEST PLEASE DO NOT PROCESS             
Remarks:                                                  
         THIS IS A TEST PLEASE DO NOT PROCESS             

Type of Work: INSTALL TRAFFIC LIGHT(S)                    
Private Property: Y    Street:            Y   Legal Given: Y   Blasting: 
Easement:              Mechanical Boring:     Premarked:      
Excavator/Owner: QUALITY ELECTRIC                                 
Sending to: (listing of utilities tkt sent to)
*IDPOCL00       IPC01           QLNID51         IMG01           LVLCOM00




                                 FOR MEMBER USE ONLY
Ticket No: 2004051121
Located by____________________________________Date of
Location_________________
Remarks:____________________________________________________________________
___
____________________________________________________________________________
___
____________________________________________________________________________
___
Excavator Notified (Not located)__________________ Who
Notified________________
Notified
by:__________________________________Date:______________Time:_________

Idaho Dig Line
NOTICE OF INTENT TO EXCAVATE                   Header Code: 2ND REQUEST
                                              Request Type:
Ticket No:    2004051122 Seq. No: 4
Update of:             
Original Call Date:     04/03/2006     Time:      09:22:47 AM  OP: 187
Transmit Date:          04/03/2006     Time:      09:22:59 AM
Locate By Date:         04/04/2006     Time:      08:22:00 AM

Company:           QUALITY ELECTRIC                       
Contact Name:      BRIAN LANGE                Contact Phone:   (208)375-1300
Alternate Contact:                            Alternate Phone:

Best Time to Call:                            Fax No:          (208)377-1329
Cell Phone:        (208)871-7783              Pager No:


State: ID       County: ADA             City: BOISE CITY
Address:          , S PEPPERTREE AVE                       
To Address:      
Nearest Intersecting Street: E GRAND FOREST DR                      
2nd Intersecting Street:     E PECAN ST                             
Subdivision:

Latitude: 43.54497400     Longitude: -116.13977350

Location of Work: THIS IS A TEST PLEASE DO NOT PROCESS             
                                                                   
                  THIS IS A TEST PLEASE DO NOT PROCESS             
                                                                   
                  THIS IS A TEST PLEASE DO NOT PROCESS             
Remarks:                                                  
         THIS IS A TEST PLEASE DO NOT PROCESS             

Type of Work: INSTALL TRAFFIC LIGHT(S)                    
Private Property: Y    Street:            Y   Legal Given: Y   Blasting: 
Easement:              Mechanical Boring:     Premarked:      
Excavator/Owner: QUALITY ELECTRIC                                 
Sending to: (listing of utilities tkt sent to)
*IDPOCL00       IPC01           QLNID51         IMG01           LVLCOM00




                                 FOR MEMBER USE ONLY
Ticket No: 2004051122
Located by____________________________________Date of
Location_________________
Remarks:____________________________________________________________________
___
____________________________________________________________________________
___
____________________________________________________________________________
___
Excavator Notified (Not located)__________________ Who
Notified________________
Notified
by:__________________________________Date:______________Time:_________


Idaho Dig Line
NOTICE OF INTENT TO EXCAVATE                   Header Code: STANDARD LOCATE
                                              Request Type:
Ticket No:    2004051123 Seq. No: 11
Update of:             
Original Call Date:     04/03/2006     Time:      09:23:01 AM  OP: 187
Transmit Date:          04/03/2006     Time:      09:23:43 AM
Locate By Date:         04/05/2006     Time:      09:23:00 AM

Company:           QUALITY ELECTRIC                       
Contact Name:      BRIAN LANGE                Contact Phone:   (208)375-1300
Alternate Contact:                            Alternate Phone:

Best Time to Call:                            Fax No:          (208)377-1329
Cell Phone:        (208)871-7783              Pager No:


State: ID       County: ADA             City: BOISE CITY
Address:          , N HERTFORD CT                          
To Address:      
Nearest Intersecting Street: N HERTFORD WAY                         
2nd Intersecting Street:                                            
Subdivision:

Latitude: 43.66621600     Longitude: -116.27329400

Location of Work: THIS IS A TEST PLEASE DO NOT PROCESS             
                                                                   
                  THIS IS A TEST PLEASE DO NOT PROCESS             
                                                                   
                  THIS IS A TEST PLEASE DO NOT PROCESS             
Remarks: THIS IS A TEST PLEASE DO NOT PROCESS             
                                                          
         THIS IS A TEST PLEASE DO NOT PROCESS                            
Type of Work: BUILDING A PATIO                            
Private Property:      Street:                Legal Given:     Blasting: 
Easement:              Mechanical Boring:     Premarked:      
Excavator/Owner: QUALITY ELECTRIC                                 
Sending to: (listing of utilities tkt sent to)
*IDPOCL00       IPC01           QLNID52         IMG01           LVLCOM00




                                 FOR MEMBER USE ONLY
Ticket No: 2004051123
Located by____________________________________Date of
Location_________________
Remarks:____________________________________________________________________
___
____________________________________________________________________________
___
____________________________________________________________________________
___
Excavator Notified (Not located)__________________ Who
Notified________________
Notified
by:__________________________________Date:______________Time:_________



