

NOTICE OF INTENT TO EXCAVATE
48HR-48 HOURS NOTICE                    NEW TICKET
Ticket No:  4063734
Update Of:        0                     Updated By:         0
Transmit      Date:  5/17/04            Time: 10:24     Op: velma.53
Original Call Date:  4/30/04            Time: 13:23     Op: velma.53

Company     : LOUISIANA ONE CALL
Contact Name: LISA CASHIO TEST TERMINAL       Contact Phone: (800)
272-3020
Alt. Contact: EXT 459                         Alt. Phone   : (225)
275-3700

Type of Work : TEST
Work Done For: TEST

State: LA       Parish: ST. TAMMANY
Place: SLIDELL CITY
Address: 123         Street: TEST
Nearest Intersecting Street: TEST

Location:       TESTING THE NEW SYSTEM--IF YOU RECEIVE THIS TICKET &
: THERE IS A PROBLEM WITH THE FONT OR ANY OTHER CONCERNS--PLSE CALL LISA
AT
: 1-800-272-3020

Remarks: TESTING NEW COMPUTER SYSTEM


Work to Begin Date:  5/04/04            Time:  1:30 PM
Mark By       Date:  5/04/04            Time:  1:30 PM

Ex. Coord: NW Lat: 30. 59 50 Lon: -89. 19 31 SE Lat: 30. 50 50 Lon:  -89. 10 6

Additional Members:  ATT01, CROSS01, CSI01, DHL01, LAGS01, LWS01, MBLSW01
 ROW01, TKL01, TKL03, WSE01

Send To:  CHART02   Seq No:   1
Send To:  CLES03    Seq No:   1
Send To:  HM02      Seq No:   1


