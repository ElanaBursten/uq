
MESSAGE BEGIN SEQUENCE NUMBER 0017   CDC = PUB
Texas One Call System
*  ROUTINE  * 

MESSAGES Sent to  Office(s) as follows : 
NO UTILITIES     UQ-RGV           BRONSVILLE PUB   BROWNSVILLE    
AEP-CP&L-UTLQST  TEXAS GAS SVCS   TCI CABLE S TX   LONE STAR XCHG

Locate Request No. 040448342

Prepared By LINDA SMITH           On 13-FEB-04  At 1621

MapRef :                            Grid: 255800097310A

Footprint: D10

Location:     County: CAMERON  Town: BROWNSVILLE

             Address: 0 TANDY 

Beginning Work Date 02/17/04 Time of Day: 04:15 pm   Duration: 90 DAYS 

Fax-A-Locate Date          at 

Excavation Type : UTILITIES AND CONSTR          
Nature of Work  : UTILITIES AND CONSTR          

Blasting ? NO           48 Hr Notice ? NO   
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : ADRIANNA ROSALES
Company Name   : G & T PAVING
Address:         F-M RD 802 & MERCEDES RD
City:            BROWNSVILLE TX  78523

Work by GT PAVING      For CADCON

Person to Contact : ROBERT GONZALES

Phone No.  ( 956 )592-1744 /( 956 )546-3633 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 956 )546-5333
Email:     GPAVINGCO@RGV.RR.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: NORTHWOOD 
MARK FROM ABOVE INTER GOING N FOR 800FT ON TANDY--MARK B/S OF RD      
CALL ROBERT BEFORE MARKING                                            
PLEASE MARK W/FLAG'S                                                  
                                                                      
                                                                      
                                                                      
FAX #: 956-546-5333                                                   
GPAVINGCO@RGV.RR.COM

FaxBack Requested ? NO     Lone Star Xref: 


Total Grids: 10

255800097310A, 255800097310B, 255830097310A, 255830097310B, 255830097310C, 
255830097310D, 255800097313A, 255800097313B, 255830097313C, 255830097313D

040448342 to EMAIL ADDRESS at 16:22:08 on FRI, 02/13/04 for PUB #0017
MESSAGE END


MESSAGE BEGIN SEQUENCE NUMBER 0018   CDC = PUB
Texas One Call System
*  ROUTINE  * 

MESSAGES Sent to  Office(s) as follows : 
AEP-CP&L-UTLQST  UQ-RGV           MAGIC VLY ELE    BRONSVILLE PUB 
TEXAS GAS SVCS   TCI CABLE S TX   LONE STAR XCHG

Locate Request No. 040443617

Prepared By EDDIE MARQUEZ         On 13-FEB-04  At 1622

MapRef :                            Grid: 255630097243A

Footprint: D02

Location:     County: CAMERON  Town: BROWNSVILLE

             Address: 2863 N VERMILLION AVE 

Beginning Work Date 02/17/04 Time of Day: 04:30 pm   Duration: 03 HOURS

Fax-A-Locate Date          at 

Excavation Type : WATER                         
Nature of Work  : INSTL WATER TAP               

Blasting ? NO           48 Hr Notice ? YES  
White Line ? YES        Digging Deeper Than 16 Inches ? YES    

Person Calling : RAMON CISNEROS
Company Name   : BROWNSVILLE PUBLIC UTILITIES BOARD
Address:         1425 ROBINHOOD DR
City:            BROWNSVILLE TX  78521

Work by BROWNSVILLE PUBLIC UTILITIES BOARD      For BROWNSVILLE PUBLIC UTILITIES B

Person to Contact : RAMON CISNEROS

Phone No.  ( 956 )983-6341 /( 956 )466-9678 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 956 )983-6343
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: ILLINOI S
WILL LOC THE FRONT OF THE PROP. AREA WILL BE MARKED WITH WHITE PAINT. 
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

FaxBack Requested ? NO     Lone Star Xref: 


Total Grids:  2

255630097243A, 255700097243C

040443617 to EMAIL ADDRESS at 16:27:10 on FRI, 02/13/04 for PUB #0018
MESSAGE END


MESSAGE BEGIN SEQUENCE NUMBER 0019   CDC = PUB
Texas One Call System
*  ROUTINE  * 

MESSAGES Sent to  Office(s) as follows : 
BROWNSVILLE      AEP-CP&L-UTLQST  UQ-RGV           UQ-RGV         
MAGIC VLY ELE    BRONSVILLE PUB   TCI CABLE S TX   LONE STAR XCHG

Locate Request No. 040443625

Prepared By EDDIE MARQUEZ         On 13-FEB-04  At 1623

MapRef :                            Grid: 255830097290B

Footprint: D01

Location:     County: CAMERON  Town: BROWNSVILLE

             Address: 1800 E ALTON GLOOR BLVD 

Beginning Work Date 02/17/04 Time of Day: 04:30 pm   Duration: 03 HOURS

Fax-A-Locate Date          at 

Excavation Type : WATER                         
Nature of Work  : INSTL WATER TAP               

Blasting ? NO           48 Hr Notice ? YES  
White Line ? YES        Digging Deeper Than 16 Inches ? YES    

Person Calling : RAMON CISNEROS
Company Name   : BROWNSVILLE PUBLIC UTILITIES BOARD
Address:         1425 ROBINHOOD DR
City:            BROWNSVILLE TX  78521

Work by BROWNSVILLE PUBLIC UTILITIES BOARD      For BROWNSVILLE PUBLIC UTILITIES B

Person to Contact : RAMON CISNEROS

Phone No.  ( 956 )983-6341 /( 956 )466-9678 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 956 )983-6343
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: PAREDES 
WILL LOC THE FRONT OF THE PROP. AREA WILL BE MARKED WITH WHITE PAINT. 
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

FaxBack Requested ? NO     Lone Star Xref: 


Total Grids:  1

255830097290B

040443625 to EMAIL ADDRESS at 16:27:13 on FRI, 02/13/04 for PUB #0019
MESSAGE END


MESSAGE BEGIN SEQUENCE NUMBER 0020   CDC = PUB
Texas One Call System

 @@@@@ @   @ @@@@@ @@@@   @@@@ @@@@@ @   @  @@@@ @   @
 @     @@ @@ @     @   @ @     @     @@  @ @     @   @
 @@@@  @ @ @ @@@@  @@@@  @     @@@@  @ @ @ @      @ @ 
 @     @   @ @     @ @   @  @@ @     @ @ @ @       @  
 @     @   @ @     @  @  @   @ @     @  @@ @       @  
 @@@@@ @   @ @@@@@ @   @  @@@@ @@@@@ @   @  @@@@   @  

*  EMERGENCY  * 

MESSAGES Sent to  Office(s) as follows : 
AEP-CP&L-UTLQST  UQ-RGV           UQ-RGV           MAGIC VLY ELE  
BRONSVILLE PUB   TEXAS GAS SVCS   TCI CABLE S TX   LONE STAR XCHG

Locate Request No. 040443890

Prepared By ALVARO ORTEGA         On 13-FEB-04  At 2118

MapRef :                            Grid: 255530097260C

Footprint: D02

Location:     County: CAMERON  Town: BROWNSVILLE

             Address: 804 ERIKA CIR 

Beginning Work Date 02/13/04 Time of Day: 09:30 pm   Duration: 04 HOURS

Fax-A-Locate Date          at 

Excavation Type : WATER                         
Nature of Work  : EMER- REPLACING CUT UP WATER V

Blasting ? NO           48 Hr Notice ? NO   
White Line ? YES        Digging Deeper Than 16 Inches ? YES    

Person Calling : FRANK BERMUDEZ
Company Name   : BROWNSVILLE PUBLIC UTILITY BOARD
Address:         1425 ROBINHOOD
City:            BROWNSVILLE TX  78520

Work by BROWNSVILLE PUBLIC UTILITY BOARD      For BROWNSVILLE PUBLIC UTILITY BOA

Person to Contact : FRANK BERMUDEZ

Phone No.  ( 956 )983-6300 /( 956 )459-1426 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 956 )983-6322
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: ELLOY 
EMER - REPLACING CUT UP WATER VALVE - CREW EN ROUTE   - CUST W/ SVC - 
WATER VISIBLE - WILL LOC IN FRONT OF PROP  MARKED IN WHITE PAINT      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

FaxBack Requested ? NO     Lone Star Xref: 


Total Grids:  2

255530097260C, 255530097263D

040443890 to EMAIL ADDRESS at 21:21:41 on FRI, 02/13/04 for PUB #0020
MESSAGE END


MESSAGE BEGIN SEQUENCE NUMBER 0021   CDC = F77
Texas One Call System

 @@@@@ @   @ @@@@@ @@@@   @@@@ @@@@@ @   @  @@@@ @   @
 @     @@ @@ @     @   @ @     @     @@  @ @     @   @
 @@@@  @ @ @ @@@@  @@@@  @     @@@@  @ @ @ @      @ @ 
 @     @   @ @     @ @   @  @@ @     @ @ @ @       @  
 @     @   @ @     @  @  @   @ @     @  @@ @       @  
 @@@@@ @   @ @@@@@ @   @  @@@@ @@@@@ @   @  @@@@   @  

*  EMERGENCY  * 

MESSAGES Sent to  Office(s) as follows : 
AEP-CP&L-UTLQST  EPFS-GILMORE     UQ-RGV           MISSION P/L    
SMITH PRODUCTN   TEXAS GAS SVCS   EL PASO FIELD    LONE STAR XCHG

Locate Request No. 040443909

Prepared By OSCAR ORTEGA          On 13-FEB-04  At 2150

MapRef : 956-585-MISSION            Grid: 261430098263A

Footprint: D02

Location:     County: HIDALGO  Town: PENITAS

             Address: 0 TOM GILL RD 

Beginning Work Date 02/13/04 Time of Day: 10:00 pm   Duration: 03 HOURS

Fax-A-Locate Date          at 

Excavation Type : POLE/SIGN INSTALLATION        
Nature of Work  : EMER- POLE REPAIR             

Blasting ? NO           48 Hr Notice ? NO   
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : RAY AGUAYO
Company Name   : RED SIMPSON
Address:         25065 FM 509
City:            HARLINGEN TX  78552

Work by RED SIMPSON      For A E P

Person to Contact : RAY AGUAYO

Phone No.  ( 956 )535-1298 /( 956 )607-0033 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 956 )412-7352
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: HWY 83 
EMER- POLE REPAIR / CREW IS ON SITE / CUST W\OUT SERVICE / FROM THE IN
TERSECTION GO N APPX 0.3 MI ON TOM GILL TO LOC ON THE E SIDE OF THE RO
AD                                                                    
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

FaxBack Requested ? YES    Lone Star Xref: 


Total Grids:  2

261430098263A, 261430098263B

040443909 to EMAIL ADDRESS at 21:52:59 on FRI, 02/13/04 for F77 #0021
MESSAGE END


MESSAGE BEGIN SEQUENCE NUMBER 0022   CDC = F80
Texas One Call System

 @@@@@ @   @ @@@@@ @@@@   @@@@ @@@@@ @   @  @@@@ @   @
 @     @@ @@ @     @   @ @     @     @@  @ @     @   @
 @@@@  @ @ @ @@@@  @@@@  @     @@@@  @ @ @ @      @ @ 
 @     @   @ @     @ @   @  @@ @     @ @ @ @       @  
 @     @   @ @     @  @  @   @ @     @  @@ @       @  
 @@@@@ @   @ @@@@@ @   @  @@@@ @@@@@ @   @  @@@@   @  

*  EMERGENCY  * 

MESSAGES Sent to  Office(s) as follows : 
AEP-CP&L-UTLQST  UQ-RGV           TOTAL E&P USA    NGPL-KMTP-TEJAS
TEXAS GAS SVCS   EL PASO FIELD    LONE STAR XCHG

Locate Request No. 040450026

Prepared By KARLA SANCHEZ         On 14-FEB-04  At 0932

MapRef : 956-787-ALAMO/PHARR/SAN J  Grid: 260930098103A

Footprint: D03

Location:     County: HIDALGO  Town: PHARR

             Address: 2819 TARA DR 

Beginning Work Date 02/14/04 Time of Day: 09:45 am   Duration: 03 HOURS

Fax-A-Locate Date          at 

Excavation Type : WATER                         
Nature of Work  : EMER WATER LINE REPAIR        

Blasting ? NO           48 Hr Notice ? NO   
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : RUBEN RAMIREZ
Company Name   : CITY OF PHARR
Address:         801 E SAM HOUSTON AVE
City:            PHARR TX  78577

Work by CITY OF PHARR      For CITY OF PHARR

Person to Contact : RUBEN RAMIREZ

Phone No.  ( 956 )227-1512 /( 000 )000-0000 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 956 )783-4688
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: RHETT DR 
EMER WATER LINE REPAIR/CREW ON SITE/ CUST W.O SERV/ WATER IS VISIBLE/ 
WORKING IN THE FRONT OF THE PROP/ AREA WILL BE MARKED IN BLUE         
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

FaxBack Requested ? NO     Lone Star Xref: 


Total Grids:  3

260930098103A, 260930098103C, 260930098103D

040450026 to EMAIL ADDRESS at 09:34:54 on SAT, 02/14/04 for F80 #0022
MESSAGE END


MESSAGE BEGIN SEQUENCE NUMBER 0023   CDC = F80
Texas One Call System

 @@@@@ @   @ @@@@@ @@@@   @@@@ @@@@@ @   @  @@@@ @   @
 @     @@ @@ @     @   @ @     @     @@  @ @     @   @
 @@@@  @ @ @ @@@@  @@@@  @     @@@@  @ @ @ @      @ @ 
 @     @   @ @     @ @   @  @@ @     @ @ @ @       @  
 @     @   @ @     @  @  @   @ @     @  @@ @       @  
 @@@@@ @   @ @@@@@ @   @  @@@@ @@@@@ @   @  @@@@   @  

*  EMERGENCY  * 

MESSAGES Sent to  Office(s) as follows : 
AEP-CP&L-UTLQST  UQ-RGV           TOTAL E&P USA    NGPL-KMTP-TEJAS
TEXAS GAS SVCS   LONE STAR XCHG

Locate Request No. 040450042

Prepared By CHAD GODDARD          On 14-FEB-04  At 1055

MapRef : 956-787-ALAMO/PHARR/SAN J  Grid: 260930098103C

Footprint: D01

Location:     County: HIDALGO  Town: PHARR

             Address: 2810 TARA DR 

Beginning Work Date 02/14/04 Time of Day: 11:00 am   Duration: 03 HOURS

Fax-A-Locate Date          at 

Excavation Type : WATER                         
Nature of Work  : EMER-WATER LINE REPAIR        

Blasting ? NO           48 Hr Notice ? NO   
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : RUBEN RAMIREZ
Company Name   : CITY OF PHARR
Address:         801 E SAM HOUSTON AVE
City:            PHARR TX  78577

Work by CITY OF PHARR      For CITY OF PHARR

Person to Contact : RUBEN RAMIREZ

Phone No.  ( 956 )227-1512 /( 000 )000-0000 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 956 )783-4688
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: RHETT DR 
EMER-WATER LINE REPAIR-CREW ON SITE-CUSTOMERS WITHOUT SERVICES-LOCATE 
FRONT OF PROPERTY WHERE MARKED IN BLUE PAINT-WATER IS VISIBLE.        
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

FaxBack Requested ? NO     Lone Star Xref: 


Total Grids:  1

260930098103C

040450042 to EMAIL ADDRESS at 10:57:57 on SAT, 02/14/04 for F80 #0023
MESSAGE END


MESSAGE BEGIN SEQUENCE NUMBER 0024   CDC = F77
Texas One Call System
*  ROUTINE  * 

MESSAGES Sent to  Office(s) as follows : 
NO UTILITIES     UQ-RGV           MCLEODUSA        AEP-CP&L-UTLQST
MAGIC VLY ELE    TEXAS GAS SVCS   TCI CABLE S TX   LONE STAR XCHG

Locate Request No. 040458069

Prepared By AMY HICKS             On 14-FEB-04  At 1522

MapRef : 956-585-MISSION            Grid: 261130098173A

Footprint: D23

Location:     County: HIDALGO  Town: MISSION

             Address: 500 N BRYAN RD 

Beginning Work Date 02/17/04 Time of Day: 04:15 pm   Duration: 05 DAYS 

Fax-A-Locate Date          at 

Excavation Type : INSTL ELEC LINE               
Nature of Work  : INSTL ELEC LINE               

Blasting ? NO           48 Hr Notice ? NO   
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : MIKE GUENETTE
Company Name   : MIKE GUENETTE
Address:         500 N BRYAN RD
City:            MISSION TX  78572

Work by MIKE GUENETTE      For MIKE GUENETTE

Person to Contact : MIKE GUENETTE

Phone No.  ( 956 )584-1118 /( 956 )583-1185 ( Hours: 08:00 am/08:00 am )
Fax No.    (    )    
Email:     CALLER COULD NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: EXPRESSWAY 83 
WORKING AT LOT M1 IN THE WINTERGREEN ESTATES: MARK ENTIRE REAR OF LOT 
TO EXTEND APPX 50FT BEYOND PROPERTY LINE TO ELEC METERS.              
LOT M1 IS AT 1522 SANTA MARIA (911 ADDRESS).                          
                                                                      
                                                                      
                                                                      
FAX #: NOT PROVIDED                                                   
CALLER COULD NOT PROVIDE EMAIL

FaxBack Requested ? NO     Lone Star Xref: 


Total Grids: 23

261130098173A, 261200098173A, 261200098173C, 261230098173C, 261130098180A, 
261130098180B, 261200098180A, 261200098180B, 261200098180C, 261200098180D, 
261230098180C, 261230098180D, 261130098183A, 261130098183B, 261200098183A, 
261200098183B, 261200098183C, 261200098183D, 261230098183C, 261230098183D, 
261130098190B, 261200098190B, 261200098190D

040458069 to EMAIL ADDRESS at 15:23:28 on SAT, 02/14/04 for F77 #0024
MESSAGE END


MESSAGE BEGIN SEQUENCE NUMBER 0025   CDC = F80
Texas One Call System
*  ROUTINE  * 

MESSAGES Sent to  Office(s) as follows : 
NO UTILITIES     UQ-RGV           AEP-CP&L-UTLQST  MAGIC VLY ELE  
TEXAS GAS SVCS   TCI CABLE S TX   LONE STAR XCHG

Locate Request No. 040458081

Prepared By MARC WEIDLER          On 14-FEB-04  At 2030

MapRef : 956-787-ALAMO/PHARR/SAN J  Grid: 261200098083C

Footprint: D02

Location:     County: HIDALGO  Town: SAN JUAN

             Address: 119 E GONZALEZ 

Beginning Work Date 02/17/04 Time of Day: 04:15 pm   Duration: 03 DAYS 

Fax-A-Locate Date          at 

Excavation Type : INSTALL FENCE                 
Nature of Work  : INSTALL FENCE                 

Blasting ? NO           48 Hr Notice ? NO   
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : LEE
Company Name   : LIFETIME FENCES
Address:         P.O. BOX 86
City:            WESLACO TX  78596

Work by LIFETIME FENCES      For AARON GOMEZ

Person to Contact : LEE LEAL

Phone No.  ( 956 )781-1078 /( 956 )460-5831  CELL ( Hours: 08:00 am/08:00 am )
Fax No.    ( 956 )781-1078
Email:     CALLER COULD NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: RAUL LONGORIA 
LOCATE FROM THE FRONT OF THE HOUSE TO THE ALLEY.                      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
FAX #: 956-781-1078                                                   
CALLER COULD NOT PROVIDE EMAIL

FaxBack Requested ? NO     Lone Star Xref: 


Total Grids:  2

261200098083C, 261200098090D

040458081 to EMAIL ADDRESS at 20:31:27 on SAT, 02/14/04 for F80 #0025
MESSAGE END


MESSAGE BEGIN SEQUENCE NUMBER 0023   CDC = F77
*** N O  R E S P O N S E ***
Texas One Call System
*  NO RESPONSE  * 

MESSAGES Sent to  Office(s) as follows : 
AEP-CP&L-UTLQST  UQ-RGV           TEXAS GAS SVCS   TCI CABLE S TX 
EL PASO FIELD    LONE STAR XCHG

Locate Request No. 040477532

Prepared By JUDY RHEINGANS        On 16-FEB-04  At 0942

MapRef : 956-585-MISSION            Grid: 261300098170A

Footprint: D04

Location:     County: HIDALGO  Town: MISSION

             Address: 0 GLASSCOCK 

Beginning Work Date 02/11/04 Time of Day: 08:00 am   Duration: 01 DAY  

Fax-A-Locate Date          at 

Excavation Type : GAS LINE                      
Nature of Work  : REPLACE RECTIFIER             

Blasting ? NO           48 Hr Notice ? NO   
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : HEATHER
Company Name   : SOUTHERN UNION GAS CO
Address:         5602 E. GRIMES RD.
City:            HARLINGEN TX  78550

Work by SOUTHERN UNION GAS CO      For TX GAS

Person to Contact : HEATHER

Phone No.  ( 956 )444-3928 /( 956 )444-3958 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 956 )423-0549
Email:     WSEIDEL@TXGAS.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY -->NoResponseTo 040401988
Near Intersection: FM 495 
WORK DATE: 02-11-2004 08:00 AM  EXPLOSIVES: N  WHITE PAINT: N  16 INCH
: Y  MAPSCO:   GRIDS:   NORTHWEST FOR INTERSECTION 350' NORTH  134670.
XML                                                                   
NO RESPONSE 040401988 CALLER REQUESTS ALL UTILITIES TO RESOND ASAP.   
ON FIRST LOCATE UTILITY COMPANIES DID NOT MARK COMPLETELY.

FaxBack Requested ? NO     Lone Star Xref: 


Total Grids:  4

261300098170A, 261330098170C, 261300098173B, 261330098173D

040477532 to EMAIL ADDRESS at 10:24:04 on MON, 02/16/04 for F77 #0023
MESSAGE END


