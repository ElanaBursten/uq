X%3
NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00139336               Update Of: 00111226
Transmit      Date: 03/07/02      Time: 01:01    Op: sylvia
Original Call Date: 02/28/02      Time: 08:35    Op: sylvia
Work to Begin Date: 03/04/02      Time: 08AM

Place: UPPER MARLBORO
Address:             Street: N MARWOOD BLVD
Nearest Intersecting Street: HAVENWOOD CT

Type of Work: INSTALLING UG ELEC SVC
Extent of Work:   LOC ENTIRE PROPERTIES OF 9905, 9907, AND 9909 N MARWOOD
: BLVD
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT. PER CALLER ALL UTILS PLS
: RELOC ASAP DUE TO GRADING. SLM 2/21                    Fax: (301)736-5937

Company     : C W  WRIGHT
Contact Name: HEATHER SHAUT         Contact Phone: (301)736-2396
Alt. Contact: NEAL ALDERMAN         Alt. Phone   :
Work Being Done For: PEPCO
State: MD              County: PG
Map: PG    Page: 025   Grid Cells: K03,K04
Explosives: N
PEP05
Send To: MTV   01  Seq No: 0001   Map Ref:
         TPG   01          0001
         WGL   06          0001
         WSS   01          0001



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00139596               Update Of: 00108225
Transmit      Date: 03/07/02      Time: 01:01    Op: france
Original Call Date: 02/28/02      Time: 09:13    Op: france
Work to Begin Date: 03/04/02      Time: 09AM

Place: CAMP SPRINGS
Address:             Street: LEROYS LA
Nearest Intersecting Street: SOUTH PERIMETER RD

Type of Work: INST MONITORING WELLS, SOIL AND WTR TESTING
Extent of Work:   LOC BOTH SIDES OF LEROYS LA STARTING 1FT E OF THE ABOVE
: INTER TO 1000FT E. THEN STARTING 500FT E OF THE ABOVE INTER LOC FROM THE
: CENTER OF THE RD 500 N OF LEROYS LA AND 500FT S OF LEROYS LA. DIR: GO
: APPROX 1500FT S OF WATSON DR ON SOUTH PERIMETER RD TO E ON LEROYS LA.
Remarks: THIS IS LANDFILL 5. LEGALS NOT PROVIDED
: BEST INFORMATION CALLER COULD PROVIDE                  Fax: (301)981-7125

Company     : ANDREWS AIRFORCE BASE
Contact Name: ANNE WHIBLEY          Contact Phone: (301)981-1426
Alt. Contact: BRIAN DOLAN           Alt. Phone   : (301)981-7121
Work Being Done For: ANDREWS AIRFORCE BASE
State: MD              County: PG
Map: PG    Page: 025   Grid Cells: G05,G06
Explosives: N
PEP05      STS01
Send To: MTV   01  Seq No: 0002   Map Ref:
         TPG   01          0002
         WGL   06          0002
         WSS   01          0002



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00139800
Transmit      Date: 03/07/02      Time: 01:02    Op: brandi
Original Call Date: 02/28/02      Time: 09:44    Op: brandi
Work to Begin Date: 03/04/02      Time: 09AM

Place: CLINTON
Address: 6923        Street: BRIARCLIFF DR
Nearest Intersecting Street: GROVETON DR

Type of Work: REPLACE GAS SVC
Extent of Work:   LOC, MARK, FLAG ENTIRE PROP TO OPPOSITE CURB INCLUDING
: OPPOSITE CURB PUBLIC UTIL EASEMENT
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT, MARK GAS A & B
:                                                        Fax: (301)330-5750

Company     : LINEAL INDUSTRY((XLIN))
Contact Name: JOE CUMMINGS          Contact Phone: (301)330-9077
Alt. Contact: BOB CASAMESE          Alt. Phone   : (301)330-9077
Work Being Done For: WASHINGTON GAS
State: MD              County: PG
Map: PG    Page: 031   Grid Cells: B04
Explosives: N
PEP05      XLIN
Send To: MTV   01  Seq No: 0003   Map Ref:
         TPG   01          0003
         WGL   06          0003
         WSS   01          0003



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00140070               Update Of: 00109852
Transmit      Date: 03/07/02      Time: 01:02    Op: brandi
Original Call Date: 02/28/02      Time: 10:17    Op: brandi
Work to Begin Date: 03/04/02      Time: 10AM

Place: TEMPLE HILLS
Address: 4712        Street: TEMPLE HILLS RD
Nearest Intersecting Street: SAINT BARNABAS RD

Type of Work: INST GAS SVC
Extent of Work:   LOC ENTIRE PROP TO CURB
Remarks: PAINT & FLAG UTIL MARKS ON SITE.MARK GAS A & B.
:                                                        Fax: (443)203-0102

Company     : LINEAL INDUSTRIES INC.
Contact Name: JJ GILLETTE           Contact Phone: (410)798-5416
Alt. Contact: PAUL                  Alt. Phone   :
Work Being Done For: WASHINGTON GAS
State: MD              County: PG
Map: PG    Page: 024   Grid Cells: C05
Explosives: N
PEP05      XLII
Send To: MTV   01  Seq No: 0004   Map Ref:
         TPG   01          0004
         WGL   06          0004
         WSS   01          0004



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00141354
Transmit      Date: 03/07/02      Time: 01:02    Op: amy.c
Original Call Date: 02/28/02      Time: 13:20    Op: amy.c
Work to Begin Date: 03/04/02      Time: 01PM

Place: GERMANTOWN
Address:             Street: FATHER HURLEY BLVD
Nearest Intersecting Street: CRYSTAL ROCK DR

Type of Work: TREE PLANTING
Extent of Work:   LOC THE MEDIAN STRIP ON FATHER HURLEY BLVD FROM CRYSTAL
: ROCK DR TO WISTERIA DR
Remarks: TKT 1 OF 2
:                                                        Fax: (301)253-4096

Company     : C & C FARMS INC  (XCCF)
Contact Name: TERRY CHALFANT JR     Contact Phone: (301)253-3122
Alt. Contact:                       Alt. Phone   :
Work Being Done For: MONTGOMERY COUNTY
State: MD              County: MONT
Map: MONT  Page: 009   Grid Cells: E12,E13,D13
Explosives: N
MCI01      PTE02
Send To: TMT   01  Seq No: 0001   Map Ref:
         TRU   01          0001
         WGL   06          0005
         WSS   01          0005



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00141387
Transmit      Date: 03/07/02      Time: 01:02    Op: amy.c
Original Call Date: 02/28/02      Time: 13:24    Op: amy.c
Work to Begin Date: 03/04/02      Time: 01PM

Place: GERMANTOWN
Address:             Street: FATHER HURLEY BLVD
Nearest Intersecting Street: CRYSTAL ROCK DR

Type of Work: TREE PLANTING
Extent of Work:   LOC THE MEDIAN STRIP ON FATHER HURLEY BLVD FROM CRYSTAL
: ROCK DR TO WISTERIA DR
Remarks: TKT 2 OF 2
:                                                        Fax: (301)253-4096

Company     : C & C FARMS INC  (XCCF)
Contact Name: TERRY CHALFANT JR     Contact Phone: (301)253-3122
Alt. Contact:                       Alt. Phone   :
Work Being Done For: MONTGOMERY COUNTY
State: MD              County: MONT
Map: MONT  Page: 018   Grid Cells: D13
Explosives: N
PEP05
Send To: TMT   01  Seq No: 0002   Map Ref:
         TRU   01          0002
         WGL   06          0006
         WSS   01          0006



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00141734
Transmit      Date: 03/07/02      Time: 01:02    Op: r.cabl
Original Call Date: 02/28/02      Time: 14:26    Op: r.cabl
Work to Begin Date: 03/04/02      Time: 02PM

Place: COLLINGTON ESTATES
Address: 13766       Street: CARLENE DRIVE
Nearest Intersecting Street: CARLENE DRIVE

Type of Work: PLACING BURIED CABLE
Extent of Work:   LOCATE ALL MAIN LINES AND SERVICES FROM THE ABOVE ADDRESS
: WEST ALONG THE SOUTH SIDE OF CARLENE DR TO THE ABOVE INTERSECTION THEN
: LOCATE SOUTH ALONG THE EAST SIDE OF CARLENE DR TO THE PROPERTY LINE AT
: LOT 10 AND LOT 11.
Remarks: CALLER REQUESTS CALLBACKS FROM ALL UTILITIES
: CALLER REQUESTS BOTH FLAGS AND PAINT                   Fax: (410)867-4340

Company     : SOUTHERN MARYLAND CABLE,INC.
Contact Name: TAMMY BRASHEARS       Contact Phone: (410)867-7577
Alt. Contact: CHERYL TURNER         Alt. Phone   : (301)261-5002
Work Being Done For: VERIZON
State: MD              County: PG
Map: PG    Page: 021   Grid Cells: A13
Explosives: N
PEP05
Send To: MTV   01  Seq No: 0005   Map Ref:
         TPG   01          0005
         WGL   06          0007
         WSS   01          0007



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00141747
Transmit      Date: 03/07/02      Time: 01:02    Op: r.cabl
Original Call Date: 02/28/02      Time: 14:28    Op: r.cabl
Work to Begin Date: 03/04/02      Time: 02PM

Place: UPPER MARLBORO
Address: 5208        Street: STARTING GATE DRIVE
Nearest Intersecting Street: BACK STRETCH BLVD

Type of Work: PLACING BURIED CABLE
Extent of Work:   LOCATE ALL MAIN LINES AND SERVICES FROM THE ABOVE ADDRESS
: SOUTH APPROX 150 FT TO PED 5212 STARTING GATE DRIVE.
Remarks: CALLER REQUESTS CALLBACKS FROM ALL UTILITIES
: CALLER REQUESTS BOTH FLAGS AND PAINT                   Fax: (410)867-4340

Company     : SOUTHERN MARYLAND CABLE,INC.
Contact Name: TAMMY BRASHEARS       Contact Phone: (410)867-7577
Alt. Contact: CHERYL TURNER         Alt. Phone   : (301)261-5002
Work Being Done For: VERIZON
State: MD              County: PG
Map: PG    Page: 026   Grid Cells: K03,K02
Explosives: N
PEP05
Send To: MTV   01  Seq No: 0006   Map Ref:
         TPG   01          0006
         WGL   06          0008
         WSS   01          0008



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00141843
Transmit      Date: 03/07/02      Time: 01:02    Op: charro
Original Call Date: 02/28/02      Time: 14:41    Op: charro
Work to Begin Date: 03/04/02      Time: 02PM

Place: UPPER MARLBORO
Address: 12712       Street: CENTER PKWY
Nearest Intersecting Street: MARLTON CENTER DR

Type of Work: INST ST LIGHTING
Extent of Work:   LOC 50FT ON BOTH SIDES OF ASPHALT PATH FROM MARLTON
: CENTER DR TO CENTER PKWY THIS AREA IS ACROSS THE ST FROM THE ABOVE
: ADDRESS
Remarks: CALLER REQUESTS FAXBACKS FROM ALL UTILITIES
:                                                        Fax: (301)948-3643

Company     : JAMES L MUSCATELLO ELECTRIC
Contact Name: JERRI LIVINGSTON      Contact Phone: (301)840-1930
Alt. Contact: PORTER                Alt. Phone   :
Work Being Done For: CRESCENT DEVELOPMENT
State: MD              County: PG
Map: PG    Page: 026   Grid Cells: G12
Explosives: N
PEP05
Send To: JTV   06  Seq No: 0001   Map Ref:
         MTV   01          0007
         TPG   01          0007
         WGL   06          0009
         WSS   01          0009



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00146882
Transmit      Date: 03/07/02      Time: 01:02    Op: sandra
Original Call Date: 03/04/02      Time: 09:11    Op: sandra
Work to Begin Date: 03/06/02      Time: 09AM

Place: BRYANS ROAD
Address:             Street: HEATHER DR
Nearest Intersecting Street: WINDMILL CT

Type of Work: FINE GRADE AND EXCAVATE SWALES
Extent of Work:   LOC ENTIRE PROPS AT 7039/LOT 42,7041/LOT 41,7043/LOT 40,
: 7045/LOT 39,7053/LOT 35,7057/LOT 33,7061/LOT 31 HEATHER DR MARKING
: WITH FLAGS AND PAINT
Remarks:
:                                                        Fax: (301)645-4047

Company     : LONGWOOD GARDENS
Contact Name: KYLE OLSEN            Contact Phone: (301)843-6202
Alt. Contact:                       Alt. Phone   :
Work Being Done For: HOME FIRST COMMUNITIES
State: MD              County: CHARLES
Map: CHAS  Page: 002   Grid Cells: B10,B11
Explosives: N
CHAS01     JICT02     JICT03     TAN02      WP04
Send To: WGL   06  Seq No: 0010   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147399
Transmit      Date: 03/07/02      Time: 01:02    Op: lorett
Original Call Date: 03/04/02      Time: 10:02    Op: lorett
Work to Begin Date: 03/06/02      Time: 10AM

Place: ROCKVILLE
Address:             Street: PORTER LA
Nearest Intersecting Street: GARCIA LA

Type of Work: INST UG ELEC SVC & MAIN
Extent of Work:   LOC ENTIRE PROP OF LOT 31
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (301)607-4042

Company     : C W WRIGHT
Contact Name: NEIL ALDERMAN         Contact Phone: (301)607-4016
Alt. Contact: TRISH DYSON           Alt. Phone   :
Work Being Done For: PEPCO
State: MD              County: MONT
Map: MONT  Page: 028   Grid Cells: F04
Explosives: N
HNI01      MCI01      PEP05
Send To: TMT   01  Seq No: 0003   Map Ref:
         TRU   01          0003
         WGL   06          0011
         WSS   01          0010



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147417
Transmit      Date: 03/07/02      Time: 01:03    Op: lorett
Original Call Date: 03/04/02      Time: 10:03    Op: lorett
Work to Begin Date: 03/06/02      Time: 10AM

Place: ROCKVILLE
Address:             Street: GARCIA LA
Nearest Intersecting Street: CASEY LA

Type of Work: INST UG ELEC SVC & MAIN
Extent of Work:   LOC ENTIRE PROPS & ALLEY IN REAR OF 102 THROUGH 138
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (301)607-4042

Company     : C W WRIGHT
Contact Name: NEIL ALDERMAN         Contact Phone: (301)607-4016
Alt. Contact: TRISH DYSON           Alt. Phone   :
Work Being Done For: PEPCO
State: MD              County: MONT
Map: MONT  Page: 028   Grid Cells: F04
Explosives: N
HNI01      MCI01      PEP05
Send To: TMT   01  Seq No: 0004   Map Ref:
         TRU   01          0004
         WGL   06          0012
         WSS   01          0011



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147607
Transmit      Date: 03/07/02      Time: 01:03    Op: janice
Original Call Date: 03/04/02      Time: 10:22    Op: janice
Work to Begin Date: 03/06/02      Time: 10AM

Place: DARNESTOWN
Address: 15330       Street: DARNESTOWN RD
Nearest Intersecting Street: BERRYVILLE RD

Type of Work: PERC TEST
Extent of Work:   LOC THE ENTIRE 300 ACRES AT THE INTER OF BERRYVILLE RD
: & DARNESTOWN RD & AREA WILL BE STAKED
Remarks:
:                                                        Fax: (301)349-2086

Company     : J MAURICE CARLISLE INC
Contact Name: SUZIE BODMER          Contact Phone: (301)428-8599
Alt. Contact: MR CARLISLE           Alt. Phone   :
Work Being Done For: BUTZ FARM
State: MD              County: MONT
Map: MONT  Page: 026   Grid Cells: F02,G02
Explosives: N
ATM01      PEP05
Send To: TMT   01  Seq No: 0005   Map Ref:
         WGL   06          0013



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147610
Transmit      Date: 03/07/02      Time: 01:03    Op: joann.
Original Call Date: 03/04/02      Time: 10:23    Op: joann.
Work to Begin Date: 03/06/02      Time: 10AM

Place: GAITHERSBURG
Address: 19401       Street: WOODFIELD RD
Nearest Intersecting Street: BRENISH DR

Type of Work: SEWER HSE CONNECTION INSTALL
Extent of Work:   LOC ENTIRE PROP
Remarks:
:                                                        Fax: (301)263-0268

Company     : DIAMOND CONSTRUCTION COMPANY
Contact Name: MIKE STELLO           Contact Phone: (301)953-7032
Alt. Contact: MIKE STELLO           Alt. Phone   : (301)343-6688
Work Being Done For: ISLAMIC CENTER OF MD
State: MD              County: MONT
Map: MONT  Page: 020   Grid Cells: B03
Explosives: N
PEP05
Send To: TMT   01  Seq No: 0006   Map Ref:
         TRU   01          0005
         WGL   06          0014
         WSS   01          0012



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147804
Transmit      Date: 03/07/02      Time: 01:03    Op: keisha
Original Call Date: 03/04/02      Time: 10:47    Op: keisha
Work to Begin Date: 03/06/02      Time: 11AM

Place: WHITE PLAINS
Address: 4151        Street: STATTON PL
Nearest Intersecting Street: SMUGGLERS NOTCH CT

Type of Work: INST UG ELEC SVC
Extent of Work:   LOC THE ENTIRE PROP OF LOT 32..WO#17420 ASPEN WOODS SUBD
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (301)705-8692

Company     : SOUTHERN MARYLAND ELECTRIC
Contact Name: MOLLY MCNEY           Contact Phone: (301)843-6142
Alt. Contact: A.J. VAN HORN         Alt. Phone   : (301)645-3636
Work Being Done For: LENHART DEVELOPMENT
State: MD              County: CHARLES
Map: CHAS  Page: 009   Grid Cells: J07
Explosives: N
ATM01      CHAS01     JICT02     TAN02      WP02       XSMECO
Send To: WGL   06  Seq No: 0015   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147811
Transmit      Date: 03/07/02      Time: 01:03    Op: keisha
Original Call Date: 03/04/02      Time: 10:48    Op: keisha
Work to Begin Date: 03/06/02      Time: 11AM

Place: INDIAN HEAD
Address: 4545        Street: STRAUSS AV
Nearest Intersecting Street: WOODLAND DR

Type of Work: REPLACE POLE AND INST UG SVC N/E
Extent of Work:   APPROX 1/10 MILES W OF WOODLAND DR ON THE S SIDE OF
: STAUSS AV LOC ALL OF 4525 AND 4545 STRAUSS AV..SEE PRINT IN OFFICE.
: WO#17208
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (301)705-8692

Company     : SOUTHERN MARYLAND ELECTRIC
Contact Name: MOLLY MCNEY           Contact Phone: (301)843-6142
Alt. Contact: A.J. VAN HORN         Alt. Phone   : (301)645-3636
Work Being Done For: GARY HOYLE
State: MD              County: CHARLES
Map: CHAS  Page: 006   Grid Cells: J07
Explosives: N
JICT02     TAN02      WP02       XSMECO
Send To: WGL   06  Seq No: 0016   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147849               Update Of: 00114523
Transmit      Date: 03/07/02      Time: 01:03    Op: karen.
Original Call Date: 03/04/02      Time: 10:52    Op: karen.
Work to Begin Date: 03/06/02      Time: 11AM

Place: GAITHERSBURG
Address:             Street: WHETSTONE DR
Nearest Intersecting Street: KEEFER WY

Type of Work: REPLACING WTR MAIN N-E
Extent of Work:   LOC FROM THE ABOVE INTER TO ECLIPSE PL LOC BOTH SIDES
: OF ST
Remarks:
:                                                        Fax: (301)206-4315

Company     : WSSC
Contact Name: ORLANDO BOONE         Contact Phone: (301)206-4050
Alt. Contact: DERRICK PHILLIPS      Alt. Phone   :
Work Being Done For: WSSC
State: MD              County: MONT
Map: MONT  Page: 019   Grid Cells: F04
Explosives: N
PEP05
Send To: TMT   01  Seq No: 0007   Map Ref:
         TRU   01          0006
         WGL   06          0017
         WSS   01          0013



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147853               Update Of: 00114526
Transmit      Date: 03/07/02      Time: 01:04    Op: karen.
Original Call Date: 03/04/02      Time: 10:52    Op: karen.
Work to Begin Date: 03/06/02      Time: 11AM

Place: GAITHERSBURG
Address:             Street: WHETSTONE DR
Nearest Intersecting Street: KEEFER WY

Type of Work: REPLACING WTR MAIN N-E
Extent of Work:   LOC FROM THE ABOVE INTER TO OX CART PL LOC BOTH SIDES
: OF ST
Remarks:
:                                                        Fax: (301)206-4315

Company     : WSSC
Contact Name: ORLANDO BOONE         Contact Phone: (301)206-4050
Alt. Contact: DERRICK PHILLIPS      Alt. Phone   :
Work Being Done For: WSSC
State: MD              County: MONT
Map: MONT  Page: 019   Grid Cells: F04
Explosives: N
PEP05
Send To: TMT   01  Seq No: 0008   Map Ref:
         TRU   01          0007
         WGL   06          0018
         WSS   01          0014



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147855               Update Of: 00114530
Transmit      Date: 03/07/02      Time: 01:04    Op: karen.
Original Call Date: 03/04/02      Time: 10:53    Op: karen.
Work to Begin Date: 03/06/02      Time: 11AM

Place: GAITHERSBURG
Address:             Street: WHETSTONE DR
Nearest Intersecting Street: ECLIPSE PL

Type of Work: REPLACING WTR MAIN N-E
Extent of Work:   LOC FROM THE ABOVE INTER TO BECKENRIDGE PL LOC BOTH SIDES
: OF ST
Remarks:
:                                                        Fax: (301)206-4315

Company     : WSSC
Contact Name: ORLANDO BOONE         Contact Phone: (301)206-4050
Alt. Contact: DERRICK PHILLIPS      Alt. Phone   :
Work Being Done For: WSSC
State: MD              County: MONT
Map: MONT  Page: 019   Grid Cells: F04
Explosives: N
PEP05
Send To: TMT   01  Seq No: 0009   Map Ref:
         TRU   01          0008
         WGL   06          0019
         WSS   01          0015



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147857               Update Of: 00114534
Transmit      Date: 03/07/02      Time: 01:04    Op: karen.
Original Call Date: 03/04/02      Time: 10:53    Op: karen.
Work to Begin Date: 03/06/02      Time: 11AM

Place: GAITHERSBURG
Address: 9841        Street: WHETSTONE DR
Nearest Intersecting Street: SOUTH MEADOWFENCE RD

Type of Work: REPLACING A WTR MAIN N-E
Extent of Work:   LOC ENTIRE ST OF WHETSTONE DR FROM ABOVE INTER TO NORTH
: PIKE CREEK PL
Remarks: TKT PER CALLER
:                                                        Fax: (301)206-4315

Company     : WSSC
Contact Name: ORLANDO BOONE         Contact Phone: (301)206-4050
Alt. Contact: DERRICK PHILLIPS      Alt. Phone   :
Work Being Done For: WSSC
State: MD              County: MONT
Map: MONT  Page: 019   Grid Cells: F04
Explosives: N
PEP05
Send To: TMT   01  Seq No: 0010   Map Ref:
         TRU   01          0009
         WGL   06          0020
         WSS   01          0016



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147863
Transmit      Date: 03/07/02      Time: 01:04    Op: keisha
Original Call Date: 03/04/02      Time: 10:54    Op: keisha
Work to Begin Date: 03/06/02      Time: 11AM

Place: WALDORF
Address:             Street: SUGAR MILL CT
Nearest Intersecting Street: ST PETERS CHURCH RD

Type of Work: INST UG SVC
Extent of Work:   SUBD PLANTATION PINES..AT THE ABOVE INTER AT LOT A1. LOC
: ENTIRE PROP OF LOT A1.. WO#17525
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (301)705-8692

Company     : SOUTHERN MARYLAND ELECTRIC
Contact Name: MOLLY MCNEY           Contact Phone: (301)843-6142
Alt. Contact: A.J. VAN HORN         Alt. Phone   : (301)645-3636
Work Being Done For: NR HOMES INC
State: MD              County: CHARLES
Map: CHAS  Page: 005   Grid Cells: E11
Explosives: N
CHAS01     JICT02     TAN02      WP03       XSMECO
Send To: WGL   06  Seq No: 0021   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147869
Transmit      Date: 03/07/02      Time: 01:04    Op: keisha
Original Call Date: 03/04/02      Time: 10:56    Op: keisha
Work to Begin Date: 03/06/02      Time: 11AM

Place: WALDORF
Address:             Street: NORBECK CT
Nearest Intersecting Street: AUGUSTA ST

Type of Work: INST UG SVC
Extent of Work:   SUBD FAIRWAY VILLAGE..ON THE E SIDE OF NORBECK CT 1/10
: MILE N OF AUGUSTA ST AT LOT 83.LOC A 50FT RADIUS OF TRANS 31 AND THE
: ENTIRE PROP OF LOT 83 WO#17344
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (301)705-8692

Company     : SOUTHERN MARYLAND ELECTRIC
Contact Name: MOLLY MCNEY           Contact Phone: (301)843-6142
Alt. Contact: A.J. VAN HORN         Alt. Phone   : (301)645-3636
Work Being Done For: RAINBO CONST
State: MD              County: CHARLES
Map: CHAS  Page: 010   Grid Cells: E08
Explosives: N
CHAS01     JICT02     TAN02      WP03       XSMECO
Send To: WGL   06  Seq No: 0022   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147881               Update Of: 00058391
Transmit      Date: 03/07/02      Time: 01:05    Op: janice
Original Call Date: 03/04/02      Time: 10:58    Op: janice
Work to Begin Date: 03/06/02      Time: 11AM

Place: WALDORF
Address:             Street: BREAK WATER CT
Nearest Intersecting Street: STONY COVE

Type of Work: SWR REPAIR N/E
Extent of Work:   LOC FRONT OF ADDRESS 2474 & 2478
Remarks:
:                                                        Fax: (301)877-6105

Company     : GERBHARDT INC.
Contact Name: MARY JO               Contact Phone: (301)877-6100
Alt. Contact: ED                    Alt. Phone   :
Work Being Done For: WSSC
State: MD              County: CHARLES
Map: CHAS  Page: 004   Grid Cells: D11
Explosives: N
CHAS01     JICT02     TAN02      WP04
Send To: WGL   06  Seq No: 0023   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147898
Transmit      Date: 03/07/02      Time: 01:05    Op: keisha
Original Call Date: 03/04/02      Time: 11:02    Op: keisha
Work to Begin Date: 03/06/02      Time: 11AM

Place: WALDORF
Address:             Street: AVENEL CT
Nearest Intersecting Street: AGRYLE AV

Type of Work: INST UG SVC
Extent of Work:   SUBD FAIRWAY VILLAGE..AT THE ABOVE INTER AT LOT 9. LOC
: ENTIRE PROP..WR#17154
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (301)705-8692

Company     : SOUTHERN MARYLAND ELECTRIC
Contact Name: MOLLY MCNEY           Contact Phone: (301)843-6142
Alt. Contact: A.J. VAN HORN         Alt. Phone   : (301)645-3636
Work Being Done For: WASHINGTON HOMES
State: MD              County: CHARLES
Map: CHAS  Page: 010   Grid Cells: E08
Explosives: N
CHAS01     JICT02     TAN02      WP03       XSMECO
Send To: WGL   06  Seq No: 0024   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147913
Transmit      Date: 03/07/02      Time: 01:05    Op: keisha
Original Call Date: 03/04/02      Time: 11:04    Op: keisha
Work to Begin Date: 03/06/02      Time: 11AM

Place: WALDORF
Address:             Street: PINEWOOD DR
Nearest Intersecting Street: DEVONSHIRE RD

Type of Work: RELACE BAD UG SVC CABLE N/E
Extent of Work:   SUBD PINEFIELD..ON THE S SIDE OF PINEWOOD DR OPPOSITE HSE
: 2775.LOC APPROX 50FT RADIUS AROUND TRANS (BEHIND FENCE) AND THE ENTIRE
: PROP OF HSE #'S 2775 AND 2777 ON THE N SIDE OF PINEWOOD DR.. WO#18223
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (301)705-8692

Company     : SOUTHERN MARYLAND ELECTRIC
Contact Name: MOLLY MCNEY           Contact Phone: (301)843-6142
Alt. Contact: A.J. VAN HORN         Alt. Phone   : (301)645-3636
Work Being Done For: MERTON G MYERS
State: MD              County: CHARLES
Map: CHAS  Page: 005   Grid Cells: E10
Explosives: N
CHAS01     JICT02     TAN02      WP03       XSMECO
Send To: WGL   06  Seq No: 0025   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147948               Update Of: 00127062
Transmit      Date: 03/07/02      Time: 01:05    Op: keisha
Original Call Date: 03/04/02      Time: 11:09    Op: keisha
Work to Begin Date: 03/06/02      Time: 11AM

Place: WALDORF
Address: 2812        Street: SCENIC MEADOW ST
Nearest Intersecting Street: BENSVILLE RD

Type of Work: INST UG SVC WIRE
Extent of Work:   N SIDE OF SCENIC MEADOW ST 4/10 MILE W OF BENSVILLE RD
: NEXT TO TRANS# 10, LOC ALL OF LOTS 20 & 21, SUBD: HERITAGE CROSSING
Remarks: W/R# 13810
:                                                        Fax: (301)705-8692

Company     : SOUTHERN MARYLAND ELECTRIC
Contact Name: MOLLY MCNEY           Contact Phone: (301)843-6142
Alt. Contact: A.J. VAN HORN         Alt. Phone   : (301)645-3636
Work Being Done For: RAINBOW CONST
State: MD              County: CHARLES
Map: CHAS  Page: 008   Grid Cells: J01
Explosives: N
CHAS01     JICT02     LTC01      TAN02      WP02       XSMECO
Send To: WGL   06  Seq No: 0026   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00147969               Update Of: 00102772
Transmit      Date: 03/07/02      Time: 01:06    Op: keisha
Original Call Date: 03/04/02      Time: 11:12    Op: keisha
Work to Begin Date: 03/06/02      Time: 11AM

Place: WALDORF
Address:             Street: SCENIC MEADOW ST
Nearest Intersecting Street: SPUR CT

Type of Work: INST UG SVC
Extent of Work:   SUBD HERITAGE CROSSING..LOC APPROX 50FT RADIUS AROUND
: SMECO TRANS 9 AND THE ENTIRE PROP OF LOT 28 WO#16100
Remarks: REMARK ON UPDATE
:                                                        Fax: (301)705-8692

Company     : SOUTHERN MARYLAND ELECTRIC
Contact Name: MOLLY MCNEY           Contact Phone: (301)843-6142
Alt. Contact: A.J. VAN HORN         Alt. Phone   : (301)645-3636
Work Being Done For: RAINBOW CONSTR
State: MD              County: CHARLES
Map: CHAS  Page: 008   Grid Cells: J01
Explosives: N
CHAS01     JICT02     LTC01      TAN02      WP02       XSMECO
Send To: WGL   06  Seq No: 0027   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00148490
Transmit      Date: 03/07/02      Time: 01:06    Op: e.mack
Original Call Date: 03/04/02      Time: 12:49    Op: e.mack
Work to Begin Date: 03/06/02      Time: 01PM

Place: BRYANS ROAD
Address: 7035        Street: INDIANHEAD HWY
Nearest Intersecting Street: RT 227

Type of Work: PULLING FUEL TANKS
Extent of Work:   LOC ENTIRE PROP
Remarks:
:                                                        Fax: (301)855-3302

Company     : HOPKINS & WAYSON BLDG INC.
Contact Name: DEBBIE LEWELLEN       Contact Phone: (301)855-3303
Alt. Contact: SHIRLEY LYNCH         Alt. Phone   :
Work Being Done For: SOUTHERN MARYLAND OIL
State: MD              County: CHARLES
Map: CHAS  Page: 008   Grid Cells: A01
Explosives: N
CHAS01     JICT02     TAN02      WP04
Send To: WGL   06  Seq No: 0028   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00148706
Transmit      Date: 03/07/02      Time: 01:06    Op: carol
Original Call Date: 03/04/02      Time: 13:21    Op: carol
Work to Begin Date: 03/06/02      Time: 01PM

Place: DERWOOD
Address: 18604       Street: WILLOW OAK DR
Nearest Intersecting Street: AZALEA DR

Type of Work: FENCE INST
Extent of Work:   LOC ENTIRE PROP
Remarks:
:                                                        Fax: (301)881-0320

Company     : CALCO FENCE
Contact Name: KATHY JAHASKE         Contact Phone: (301)881-0552
Alt. Contact: CRAIG WARNER          Alt. Phone   :
Work Being Done For: REDDIN
State: MD              County: MONT
Map: MONT  Page: 020   Grid Cells: G05
Explosives: N
PEP05
Send To: TMT   01  Seq No: 0011   Map Ref:
         TRU   01          0010



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00148830
Transmit      Date: 03/07/02      Time: 01:06    Op: janice
Original Call Date: 03/04/02      Time: 13:39    Op: janice
Work to Begin Date: 03/06/02      Time: 01PM

Place: WEST BETHESDA
Address: 9009        Street: HOLLY LEAF LA
Nearest Intersecting Street: PERSIMMON TREE RD

Type of Work: INSTALL UG CATV SERV LINES
Extent of Work:   LOC FROM NEAREST CATV PED TO POWER METER AT ABOVE ADDRESS
: WITH A DRIVEWAY BORE
Remarks:

Company     : CTI UNDERGROUND LLC
Contact Name: PAM BECK              Contact Phone: (301)482-1100
Alt. Contact: ROBIN OR DON          Alt. Phone   :
Work Being Done For: COMCAST
State: MD              County: MONT
Map: MONT  Page: 034   Grid Cells: G11
Explosives: N
PEP05
Send To: TMT   01  Seq No: 0012   Map Ref:
         TRU   01          0011
         WGL   06          0029
         WSS   01          0017



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00149433
Transmit      Date: 03/07/02      Time: 01:06    Op: sandra
Original Call Date: 03/04/02      Time: 15:23    Op: sandra
Work to Begin Date: 03/06/02      Time: 03PM

Place: GAITHERSBURG
Address:             Street: KENTLANDS BLVD
Nearest Intersecting Street: GREAT SENECA HWY

Type of Work: BUILD AN ENTRANCE PIER
Extent of Work:   LOC FRONT OF KENTLANDS SUBD ENTRANCE AT ABOVE INTER ON
: KENTLAND BLVD MARK BOTH SIDES OF RD
Remarks: LEGALS NOT PROVIDED FOR THIS LOCATE REQUEST
:                                                        Fax: (703)354-0819

Company     : TARGET MASONRY
Contact Name: LINDA SAVINO          Contact Phone: (703)354-3385
Alt. Contact: MIKE SAVINO           Alt. Phone   : (703)926-4239
Work Being Done For: GREAT SENECA DEVELOPMENT
State: MD              County: MONT
Map: MONT  Page: 018   Grid Cells: K12
Explosives: N
MCI01      PEP05
Send To: TMT   01  Seq No: 0013   Map Ref:
         TRU   01          0012
         WGL   06          0030
         WSS   01          0018



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00149661
Transmit      Date: 03/07/02      Time: 01:06    Op: carol
Original Call Date: 03/04/02      Time: 15:57    Op: carol
Work to Begin Date: 03/06/02      Time: 04PM

Place: WALDORF
Address: 3           Street: DOOLITTLE DR
Nearest Intersecting Street: POST OFFICE RD

Type of Work: INST GAS SVC
Extent of Work:   THIS IS THE SAINT CHARLES ANIMAL HOSPITAL LOC REAR OF
: BLDG CONTINUING AROUND RIGHT SIDE OF BLDG AS FACING CONTINUE OUT TO CURB
: ON DOOLITTLE DR
Remarks:
:                                                        Fax: (301)843-2682

Company     : CABLE AND CONDUIT CONST INC
Contact Name: LINDA DYSON           Contact Phone: (301)843-6111
Alt. Contact: ROBERT HARDY          Alt. Phone   :
Work Being Done For: WASH GAS
State: MD              County: CHARLES
Map: CHAS  Page: 010   Grid Cells: F04
Explosives: N
CHAS01     JICT02     JICT03     TAN02      WP03
Send To: WGL   06  Seq No: 0031   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    NO RESPONSE
Ticket No: 00149810
Transmit      Date: 03/07/02      Time: 01:07    Op: dawn.s
Original Call Date: 03/04/02      Time: 16:37    Op: dawn.s
Work to Begin Date: 03/06/02      Time: 04PM

Place: BETHESDA
Address: 8810        Street: EGGERT DR
Nearest Intersecting Street: MACARTHUR BLVD

Type of Work: FENCE INSTALLATION
Extent of Work:   LOC ENTIRE PROP
Remarks:
:                                                        Fax: (301)407-0172

Company     : CAPITAL FENCE & DECK
Contact Name: KRISTEN CHESHIRE      Contact Phone: (301)972-8400
Alt. Contact: ANS MACHINE OR CELL#  Alt. Phone   : (301)674-0978
Work Being Done For: MR BRUCE LEE
State: MD              County: MONT
Map: MONT  Page: 034   Grid Cells: H13
Explosives: N
LTC01      PEP05
Send To: TMT   01  Seq No: 0014   Map Ref:
         TRU   01          0013
         WGL   06          0032
         WSS   01          0019



