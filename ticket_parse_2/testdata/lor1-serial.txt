Ticket No:  5040808               48 HOUR NOTICE
Send To: PPL25      Seq No:    5  Map Ref:
Send To: TCI18      Seq No:    4  Map Ref:

Transmit      Date:  3/09/05   Time:  8:00 AM    Op: julie
Original Call Date:  3/09/05   Time:  7:52 AM    Op: julie
Work to Begin Date:  3/11/05   Time:  8:00 AM

State: OR            County: JACKSON                 Place: GOLD HILL
Address:             Street: 4TH AVE
Nearest Intersecting Street: ESTREMADO ST

Twp: 36S   Rng: 3W    Sect-Qtr: 21-NE,15-SW,22-NW
Twp:       Rng:       Sect-Qtr:
Ex. Coord NW Lat: 42.4336210Lon: -123.0515860SE Lat: 42.4318000Lon: -123.0480380

Type of Work: DEMOLITION
Location of Work: SITE AT SW CORNER OF INTER.MARK ENTIRE PROP AT ABV ADD
: INCL FROM ALLEY TO HOUSE

Remarks: ESTREMADO LOC APX 1 STREET W FROM 4TH ST ON 4TH AVE.
: 4TH AVE AKA HWY 234
Overhead Lines: Y

Company     : CREATIVE LAND DESIGN             Best Time:
Contact Name: MIKE NEWMAN                      Phone: (541)855-1161
Email Address:
Alt. Contact:                                  Phone: (541)301-8816
Contact Fax :
Work Being Done For: OWNER
Additional Members:
COGH01     ODOTD08    QLNOR26    QWEST02    WPNG03


OOC2005030900065	541774	2005/03/09 10:23:29	00135

OUNC
Ticket No:  5040808               48 HOUR NOTICE
Send To: QLNOR26    Seq No:    4  Map Ref:

Transmit      Date:  3/09/05   Time:  7:59 am    Op: julie
Original Call Date:  3/09/05   Time:  7:52 am    Op: julie
Work to Begin Date:  3/11/05   Time:  8:00 am

State: OR            County: JACKSON                 Place: GOLD HILL
Address:             Street: 4TH AVE
Nearest Intersecting Street: ESTREMADO ST

Twp: 36S   Rng: 3W    Sect-Qtr: 21-NE,15-SW,22-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: DEMOLITION
Location of Work: SITE AT SW CORNER OF INTER.MARK ENTIRE PROP AT ABV ADD
: INCL FROM ALLEY TO HOUSE

Remarks: ESTREMADO LOC APX 1 STREET W FROM 4TH ST ON 4TH AVE.
: 4TH AVE AKA HWY 234                                                OH: Y

Company     : CREATIVE LAND DESIGN
Contact Name: MIKE NEWMAN                      Phone: (541)855-1161
Alt. Contact:                                  Phone: (541)301-8816
Contact Fax :
Work Being Done For: OWNER
Additional Members:
COGH01     ODOTD08    PPL25      QWEST02    TCI18      WPNG03

