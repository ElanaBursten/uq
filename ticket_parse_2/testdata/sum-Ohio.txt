
 sent     03/14/02  12:12 AM sequence 0001 to OUPS station # 2972 for AGP


                      OHIO UTILITIES PROTECTION SERVICE 


                          MESSAGE LIST BY STATION
                               FOR: 03/13/02

STATION: 2972   CDC: AGP   MEMBER:  ALLEGHENY POWER

 + DENOTES TICKETS WITH LESS THAN 4 HOURS NOTICE

0001  0313-076-020-00     0002  0313-077-007-00     0003  0313-036-020-00     
0004  0313-036-031-00     0005  0313-070-004-00     0006  0313-076-082-00     
0007  0313-076-083-00     0008  0313-079-039-00     0009  0308-062-033-01 +   
0010  0313-022-034-00     0011  0313-075-077-00     0012  0313-076-148-00     
0013  0313-038-110-00 +   0014  0313-029-063-00     0015  0313-077-100-00     

                          AMOUNT OF ADVANCE NOTICE

                 LESS THAN 1 HR   1 TO 4 HRS   4 TO 48 HRS  MORE THAN 48 HRS

NEW MESSAGES            2             0             6             7
RETRANSMISSIONS         0             0             0             0
SPECIAL MESSAGES        0             0             0             0
                     ----          ----          ----          ----
TOTALS FOR CDC AGP      2             0             6             7

 eom 





