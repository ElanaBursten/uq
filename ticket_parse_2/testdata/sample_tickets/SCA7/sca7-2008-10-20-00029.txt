
SDG09  00028A USAS 10/20/08 10:10:55 A82940559-00A NORM NEW GRID

Ticket : A82940559 Date: 10/20/08 Time: 10:05 Oper: EVA Chan: 100
Old Tkt: A82940559 Date: 10/20/08 Time: 10:10 Oper: EVA Revision: 00A
State: CA County: SAN DIEGO            Place: DEL MAR
Locat: CALLER STATES ADDRESS IS CORRECT    BTWN X/ST    ( MOST WESTERLY AVOCADO
     : PL   )
Address: 716 AVOCADO PL
X/ST 1 : HIGHLAND DR
X/ST 2 : JEFFREY RD
Grids: 1187H0113                                            Delineated: Y
Lat/Long  : 32.988025/-117.252090 32.988176/-117.250189
          : 32.982832/-117.251677 32.982983/-117.249775
Caller GPS:

Boring: N  Explosives: N  Vacuum: N

Re-Mark: N

Work : SEPTIC REPAIR
Work date: 10/22/08 Time: 10:05 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                Permit   : NOT REQUIRED
Done for : H/O ELLISON

Company: MCKENNA CONSTRUCTION COMPANY   Caller: JUDY LEE
Address: 507 S CEDROS
City&St: SOLANA BEACH, CA               Zip: 92075      Fax: 858-755-8022
Phone: 858-755-2290 Ext:      Call back: 7:30-3:30
Formn: BRUCE                Phone: 619-980-5029
Email: MCKENNACONST@ATT.NET
Mbrs : ATTD28SD      SBE01  SDG02  SDG04  SDG09  SFI01  SND01  TWCSD  USDG09
UTWCCARL
