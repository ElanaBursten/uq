
SDG09  00032A USAS 10/20/08 11:53:25 A82940827-00A NORM NEW GRID

Ticket : A82940827 Date: 10/20/08 Time: 11:51 Oper: KDB Chan: 100
Old Tkt: A82940827 Date: 10/20/08 Time: 11:53 Oper: KDB Revision: 00A
State: CA County: SAN DIEGO            Place: DEL MAR
Locat: FRONT OF THE ADDRESS        (ADDRESS IS AT THE INTER OF LOZANA RD &
     : MERCADO DR)
Address: 2438 LOZANA RD
X/ST 1 : MERCADO DR
Grids: 1187H053     1187H061                                Delineated: Y
Lat/Long  : 32.953475/-117.251029 32.953017/-117.250251
          : 32.952799/-117.251428 32.952340/-117.250650
Caller GPS:

Boring: N  Explosives: N  Vacuum: N

Re-Mark: N

Work : HAND DIG TO REPAIR TEL LINE
Work date: 10/22/08 Time: 11:51 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                Permit   : NOT REQUIRED
Done for : AT&T

Company: HCI                            Caller: VICTOR RAMIS - AT&T
Address: UNK
City&St:                                Zip:
Phone: 858-268-6981 Ext:      Call back: 7AM-4PM
Formn: MARK KRAMER
Mbrs : ATTD28SD      NEXTG  SDG04  SDG09  SND01  SND02  TWCSD  USDG09 UTWCCARL
