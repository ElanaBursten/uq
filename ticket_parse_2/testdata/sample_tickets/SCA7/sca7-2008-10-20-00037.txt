
SDG09  00036A USAS 10/20/08 14:30:08 A82941275-00A SHRT NEW GRID

Ticket : A82941275 Date: 10/20/08 Time: 14:26 Oper: ZIM Chan: 100
Old Tkt: A82941275 Date: 10/20/08 Time: 14:29 Oper: ZIM Revision: 00A
State: CA County: SAN DIEGO            Place: BORREGO SPRINGS/ANZA BORREGO DSRT ST PK
Locat: E/SIDE OF BORREGO VALLEY RD AT APPROX 500FT N/OF TILTING T DR
Address: BORREGO VALLEY RD
X/ST 1 : TILTING T DR
Grids: 1079C054                                             Delineated: Y
Lat/Long  : 33.233738/-116.348199 33.233279/-116.347417
          : 33.233060/-116.348597 33.232601/-116.347816
Caller GPS:

Boring: N  Explosives: N  Vacuum: N

Re-Mark: N

Work : EMERGENCY REPAIR FAILING TELE LINE
Work date: 10/21/08 Time: 07:00 Hrs notc: 016 Work hrs: 016 Priority: 1
Instruct : MARK BY                Permit   : NOT REQUIRED
Done for : AT&T

Company: PAULEY CONSTRUCTION            Caller: DAVID WALTERS - AT&T
Address: UNK TO CALLER
City&St: CA                             Zip:
Phone: 760-504-3964 Ext:      Call back: ANYTIME
Formn: DAVE WALTERS         Phone: 760-504-3964
Mbrs : ATTD28SD      BORWTR CTVUSA SDG05  SDG09  USDG09
