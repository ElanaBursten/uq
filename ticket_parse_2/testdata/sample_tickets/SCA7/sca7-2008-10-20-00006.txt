
SDG09  00005A USAS 10/20/08 09:19:12 A82940406-00A RUSH NEW GRID

Ticket : A82940406 Date: 10/20/08 Time: 09:12 Oper: LMC Chan: 100
Old Tkt: A82940406 Date: 10/20/08 Time: 09:19 Oper: LMC Revision: 00A
State: CA County: SAN DIEGO            Place: RANCHO SANTA FE
Locat:
Address: 6407 RANCHO SANTA FE FARMS DR
X/ST 1 : RANCHO SANTA FE FARMS RD
Grids: 1188G0112                                            Delineated: Y
Lat/Long  : 32.985441/-117.182226 32.985447/-117.178775
          : 32.984617/-117.182225 32.984623/-117.178774
Caller GPS:

Boring: N  Explosives: N  Vacuum: N

Re-Mark: N

Work : DIG FOOTINGS FOR BLOCK WALL
Work date: 10/20/08 Time: 09:18 Hrs notc: 000 Work hrs: 000 Priority: 0
Instruct : NOW                    Permit   : NOT REQUIRED
Done for : H/O-JERRY SALANTI

Company: MARTZ MASONRY                  Caller: STEVE MARTZ
Address: 594 CAMINITO ACASA
City&St: SAN MARCOS, CA                 Zip: 92078
Phone: 619-203-1112 Ext: CELL Call back: 5-5
Formn: STEVE
Email: MARTZMASONRY@COX.NET
Mbrs : ATTD28SD      COX04  NEXTG  OMW62  SDG04  SDG09  USDG09 WHISPM
