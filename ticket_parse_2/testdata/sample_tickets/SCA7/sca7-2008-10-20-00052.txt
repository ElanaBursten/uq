
SDG09  00051A USAS 10/20/08 16:23:08 A82671688-05A NORM UPDT GRID

Ticket : A82671688 Date: 10/20/08 Time: 16:22 Oper: EMB Chan: 100
Old Tkt: A82671688 Date: 09/23/08 Time: 16:42 Oper: ZIM Revision: 05A
State: CA County: SAN DIEGO            Place: DEL MAR
Locat: ADDRESS IS FOR A SHELL GAS STATION
Address: 2205 VIA DE LA VALLE
X/ST 1 : 5 FWY
Grids: 1187G022     1187H021                                Delineated: Y
Lat/Long  : 32.980317/-117.253508 32.980606/-117.251980
          : 32.979507/-117.253355 32.979796/-117.251827
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N

Re-Mark: N

Work : SOIL VAPOR WELLS TO APPROX 5FT DEPTH
Work date: 10/20/08 Time: 16:22 Hrs notc: 000 Work hrs: 000 Priority: 2
Instruct : WORK CONTINUING        Permit   : NOT AVAILABLE
Done for : SHELL OIL CORP

Company: CONESTOGA ROVERS & ASSOCIATES  Caller: AMY ALDINGER
Address: 175 TECHNOLOGY, SUITE 150
City&St: IRVINE, CA                     Zip: 92618      Fax: 949-648-5299
Phone: 949-648-5200 Ext:      Call back: ANYTIME
Formn: AMY ALDINGER         Phone: 949-279-4851
Email: AALDINGER@CRAWORLD.COM

                                  COMMENTS

**RESEND**CALLER ADVISES NO SHOW FROM CITY - C/OF SOLANA BEACH, SAN ELIJO JOINT
POWERS, C/OF SAN DIEGO STREETS, PLEASE CALL AMY @ 949-279-4851 ASAP WITH E.T.A.
PER AMY--[LRL 09/30/08 09:02]
**RESEND**CALLER ADVISES NO SHOW FROM ELECTRIC, WATER, STREET LIGHTS, TRAFFIC
LIGHTS - CALLER STATES STILL NO SHOW FROM C/OF SOLANA BEACH, SAN ELIJO JOINT
POWERS AND C/OF SAN DIEGO STREETS REQ CALL TO CONFIRM ETA ASAP PER AMY--[LMC
09/30/08 10:11]
**RESEND**CALLER ADVISES NO SHOW FROM GAS, ELECTRIC, CITY, TRAFFIC LIGHTS PER
AMY--[EMB 09/30/08 11:21]
**RESEND**CALLER ADVISES NO SHOW FROM TRAFFIC LIGHTS - NEED C/OF SAN DIEGO
TRAFFIC TO PLEASE CALL AMY AT 949-279-4851 CONFLICT OR NO CONFLICT ASAP PER
AMY--[KMC 09/30/08 12:56]
**RESEND**UPDATE ONLY-WORK CONT PER AMY--[EMB 10/20/08 16:22]

Mbrs : ATTATL ATTD28SD      COX04  DEL01  LVL3CM SBE01  SDG04  SDG09  SDGET
SEJDIST       SFI01  SND01  SND02  TWCSD  USDG09 UTWCCARL
