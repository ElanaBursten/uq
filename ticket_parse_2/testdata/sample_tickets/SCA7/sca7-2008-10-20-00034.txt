
SDG09  00033A USAS 10/20/08 12:07:33 A82940856-00A NORM NEW GRID

Ticket : A82940856 Date: 10/20/08 Time: 12:02 Oper: KKP Chan: 100
Old Tkt: A82940856 Date: 10/20/08 Time: 12:07 Oper: KKP Revision: 00A
State: CA County: SAN DIEGO            Place: DEL MAR
Locat: CALLER STATES ADDRESS IS LOC APPROX 100YDS E/OF CAMINO DEL MAR
Address: 235 25TH ST
X/ST 1 : CAMINO DEL MAR
Grids: 1187F0334                                            Delineated: Y
Lat/Long  : 32.970104/-117.267527 32.970370/-117.266658
          : 32.969351/-117.267296 32.969617/-117.266428
Caller GPS:

Boring: N  Explosives: N  Vacuum: N

Re-Mark: N

Work : TRENCHING TO INSTALL UGRND ELEC
Work date: 10/22/08 Time: 12:02 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                Permit   : 743185
Done for : PROP OWNER SCOTT CROUCH

Company: JIM NICHOLS CONSTRUCTION       Caller: JIM NICHOLS
Address: 1749 GROSSMONT VIEW DRIVE
City&St: EL CAJON, CA                   Zip: 92020      Fax: 619-593-7147
Phone: 619-322-5680 Ext:      Call back: 6AM-6PM
Formn: RICK
Email: JNICHOLS100@COX.NET
Mbrs : ATTD28SD      DEL01  MCISOCAL      NEXTG  SCG48T SDG04  SDG09  USDG09
UTWCCARL
