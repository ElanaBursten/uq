
SDG09  00029A USAS 10/20/08 10:54:59 A82940666-00A NORM NEW GRID

Ticket : A82940666 Date: 10/20/08 Time: 10:50 Oper: PEG Chan: 200
Old Tkt: A82940666 Date: 10/20/08 Time: 10:54 Oper: PEG Revision: 00A
State: CA County: SAN DIEGO            Place: RANCHO SANTA FE
Locat: FENCE IS GOING TO BE ON BOTH SIDES OF THE DRIVEWAY-(CALLER STATES LOC IS
     : IN SAN DIEGO)
Address: 14795 RANCHO SANTA FE FARMS RD
X/ST 1 : RANCHO DIEGUENO RD
Grids: 1188G013     1188G021                                Delineated: Y
Lat/Long  : 32.982781/-117.182030 32.982689/-117.180834
          : 32.979093/-117.182314 32.979001/-117.181117
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N

Re-Mark: N

Work : FENCE INSTALLATION
Work date: 10/22/08 Time: 10:51 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                Permit   : NOT REQUIRED
Done for : H/O-SEAY

Company: QUALITY FENCE                  Caller: ROBERT GRAY
Address: 1312 LA MEDA RD
City&St: EL CAJON, CA                   Zip: 92020      Fax: 619-440-8613
Phone: 619-445-3078 Ext:      Call back: 630-430
Formn: ROBERT GRAY          Phone: 619-339-3078
Email: QUALITYFENCE@GMAIL.COM
Mbrs : ATTD28SD      COSDTS COX04  NEXTG  OMW62  SDG04  SDG09  USDG09 WHISPM
