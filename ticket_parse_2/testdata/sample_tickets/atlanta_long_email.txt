
BGAWM  00312 GAUPC 07/18/02 12:18:20 07182-777-011-000 NORMAL
Underground Notification
Ticket : 07182-777-011 Date: 07/18/02 Time: 12:05 Revision: 000

State: GA  County: PEACH        Place: BYRON
Addr : From: 199    To:        Name:    COCHRAN                        DR   
Cross: From:        To:        Name:
Offset:
Subdivision: 
RR Subdivision:            RR Marker:            Mile Marker:     
Locat: ENTIRE PROPERTY

Grids    : 3237B8348A   3237B8348C   3237B8348B   3237C8348A   3237D8346A  
Grids    : 3237D8347D   3237C8347A   3237C8348D   3237C8348C   3237D8348B  
Grids    : 3237C8348B  
Work type: SETTING POLES AND ANCHORS
Work date: 07/23/02 Time: 07:00 Hrs notc: 114 Priority: 3
Legal day: 07/23/02 Time: 07:00 Good thru: 08/08/02 Restake by: 08/05/02
RespondBy: 07/22/02 Time: 23:59 Duration : 3 HRS
Done for : FLINT EMC
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: N

Remarks : ADDING A POLE/TRANSFORMER/SERVICE
        : *** NEAR STREET ***   NEWELL RD

Company : FLINT EMC                                 Type: MEMB
Co addr : 900 HWY 96
City    : WARNER ROBINS                  State   : GA Zip: 31093
Caller  : LINDA CLEMENTS                 Phone   : 478-988-3542                
Fax     :                                Alt. Ph.: 
Email   : LCLEMENTS@FLINTEMC.COM                                               

Submitted date: 07/18/02 Time: 12:05 Oper: 777 Chan: 999
Mbrs : ALL06  ATT02  BGAWM  CCM01  FLI70  MEA70  PFNET1 QWEST8