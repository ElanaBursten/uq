

SEQUENCE NUMBER 0314   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ATMOS-MT-HP-STS  ONCOR ELEC-STS   ATMOS-MIDTX-STS  VERIZ-KG- METRO
CHARTER COMM     ATMOS-MT-HP-STS  SM&P-F           TR CNTY NE/1SRC  
LONE STAR XCHG

Locate Request No. 072761196

Prepared By MELODY C             On 03-OCT-07  At 0931

MapRef :                            Grid: 325400097140A  Footprint: D03

Location:     County: TARRANT  Town: KELLER

             Address: 628 WILLOWWOOD TRL 

Beginning Work Date 10/05/07 Time of Day: 09:30 am   Duration: 02 DAYS 

Fax-A-Locate Date          at 

Excavation Type : LANDSCAPE                     
Nature of Work  : LANDSCAPING                   

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : KYLE HUFF
Company Name   : THE LONE RANGER
Work by THE LONE RANGER      For HOMEOWNER

Person to Contact : KYLE HUFF/GARY HUFF

Phone No.  ( 682 )286-9619 /(    )     ( Hours: 08:00 am/05:00 pm )
Fax No.    (    )    
Email:     TLRLMC@SBCGLOBAL.NET

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: SYCAMORE DR 
RCL-072761178  CALLER REQ TO ADD EMAIL ADDRESS.    WORK WILL BE DONE O
N THE FRONT AND BACK OF THE PROP.                                     
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
TLRLMC@SBCGLOBAL.NET

Map Cross Reference : MAPSCO 37,C                         

FaxBack Requested ? NO     Lone Star Xref: 


072761196 to EMAIL ADDRESS at 09:35:09 on WED, 10/03/07 for FN3 #0314
