
UTIL11 00007 VUPSa 09/30/07 20:10:35 A727300015-00A          EMERGENCY

Ticket No:  A727300015-00A                        NEW  GRID EMER LREQ
Transmit        Date: 09/30/07   Time: 08:10 PM   Op: 1DGL
Call            Date: 09/30/07   Time: 08:05 PM
Due By          Date: 09/30/07   Time: 11:05 PM
Update By       Date:            Time:
Expires         Date:            Time:
Old Tkt No: A727300015
Original Call   Date: 09/30/07   Time: 08:05 PM   Op: 1DGL

City/Co:RICHMOND CITY         Place:                                    State:VA
Address:                      Street: DOUGLASDALE AVE
Cross 1:     S BELMONT AVE

Type of Work:   GAS LEAK
Work Done For:  SAME
Excavation area:LOCATE THE ENTIRE STREET AND INTERSECTION AND SEE CREW
Instructions:   CALLER MAP REF: 315A3
                CREW IS ON SITE

Whitelined: N   Blasting: N   Boring: N

Company:        CITY OF RICHMOND UTILITIES                Type: UTIL
Co. Address:    400 JEFFERSON DAVIS HWY  First Time: N
City:           RICHMOND  State:VA  Zip:23224
Company Phone:  804-646-8426
Contact Name:   DONALD BANKS                Contact Phone:804-646-8475
Contact Fax:    804-646-8479
Field Contact:  ERIC COLEMAN
Fld. Contact Phone:804-400-3157

Mapbook:  315A3
Grids:    3732B7729D-01  3732B7729D-02  3732B7729D-03  3732B7729D-12
Grids:    3732B7729D-13

Members:
CIRS04 = CITY OF RICHMOND (COR)         CRGR04 = CITY OF RICHMOND - GAS (COR)
CRST03 = RICH - STREET LIGHT DEPT (COR) CRWR04 = CITY OF RICHMOND - WATER (COR)
DOM100 = DOMINION VIRGINIA POWER (DOM)  VZN338 = VERIZON (VZN)

Seq No:   7 A

