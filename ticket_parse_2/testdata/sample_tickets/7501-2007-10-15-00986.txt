
KF     00264 POCS 10/15/07 23:51:12 2888015-000 NEW  XCAV EMER

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[2888015]-[000] Channel#--[2346026][0294]

Message Type--[NEW][EXCAVATION][EMERGENCY]

County--[CHESTER]         Municipality--[BIRMINGHAM TWP]
Work Site--[CREEK RD]
     Nearest Intersection--[BRINTONS BRIDGE RD]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[Y]
     Location Information--
     [WORKING AT POLE 19, APPX ONE QUARTER MILE S OF BRINTONS BRIDGE RD]
     Caller Lat/Lon--[]
     Mapped Type--[P] Mapped Lat/Lon--
     [39.881824/-75.604388,39.884070/-75.600354,39.878122/-75.593472,
      39.876605/-75.597190]

Type of Work--[PLACING ANCHOR ROD]                           Depth--[6FT]
Extent of Excavation--[]                Method of Excavation--[AUGER]
Street--[ ] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[ ] Other--[EMBANKMENT]
Owner/Work Being Done for--[VERIZON]

              Lawful Start Dates--[         ] Through [         ]
        Scheduled Excavation Date--[15-Oct-07] Dig Time--[] Duration--[]

Contractor--[VERIZON PENNSYLVANIA]           Homeowner/Business--[B]
Address--[966 S MATLACK ST]
City--[WEST CHESTER]                 State--[PA] Zip--[19382]

Caller--[DAVID]                            Phone--[610-431-5831] Ext--[]
FAX--[610-738-3305]  Email--[none]
Person to Contact--[DAVID]                     Phone--[610-717-8792] Ext--[]
Best Time to Call--[0700-1500]

Prepared--[15-Oct-07] at [2351] by [JACKIE COLCLASER]

Remarks--
     [CREW ENROUTE]

BRH0  BRH=BIRMINGHAM TWP   CD 0  CD =COLUMBIA GAS TR  CR 0  CR =CHESTER WA     
HT 0  HT =AQUA PA INC      HT40  HT4=AQUA PA INC AHR  JZ 0  JZ =COMCAST CABLE  
KF 0  KF =PECO CTVL        YJ 0  YJ =VERIZON

Serial Number--[2888015]-[000]

========== Copyright (c) 2007 by Pennsylvania One Call System, Inc. ==========
