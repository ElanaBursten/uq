

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326276 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:08     Op: lajonath.1060 
Original Call Date:  7/31/09            Time:  9:04     Op: lajonath.1060 

Company     : MECHE DIRECTIONAL DRILLING 
Contact Name: TRAVIS MECHE                    Contact Phone: (318)922-4145 
Alt. Contact: CELL #                          Alt. Phone   : (318)240-3569 

Type of Work : INSTALL TELEPHONE 
Work Done For: AT&T 

State: LA       Parish: RAPIDES 
Place: ALEXANDRIA CITY 
Address:             Street: INDUSTRIAL PARK RD 
Nearest Intersecting Street: N 3RD ST 

Location:       FROM INTERSECTION MARK 300FT E IN FRONT ON S SIDE OF
: INDUSTRIAL PARK; BACK AT INTERSECTION MARK 300FT S IN FRONT ON E SIDE OF N
: 3RD 

Remarks:  
 

Work to Begin Date:  8/04/09            Time:  9:15 AM 
Mark By       Date:  8/04/09            Time:  9:15 AM 

Ex. Coord: NW Lat: 31.3321080Lon: -92.4703209SE Lat: 31.3299780Lon:  -92.4689515 

Additional Members:  APTV01, CAL01, CAL02 

Link To Map for AL01: http://la.itic.occinc.com/R922-Z29-92T-NMZ 
Link To Map for CLEC02: http://la.itic.occinc.com/5922-Z39-U2T-NMZ 

Send To:  AL01      Seq No:  29 
Send To:  CLEC02    Seq No:  12 
