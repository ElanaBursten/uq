

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326161 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  8:48     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  8:48     Op: lastacy.1102 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: RAPIDES 
Place: BOYCE TOWN 
Address: 511         Street: BROWNS CREEK RD 
Nearest Intersecting Street: MUDGE RD 

Location:       MARK ENTIRE PROPERTY. FOLLOW TEMP LINE LAID OUT. APX 85 YDS E
: OF INTERSECTION. FOR MORE INFO CALL TEAL @ (318)542-6201. 

Remarks: LA0902502                SJB             7/31/7 
 

Work to Begin Date:  8/04/09            Time:  9:00 AM 
Mark By       Date:  8/04/09            Time:  9:00 AM 

Ex. Coord: NW Lat: 31.2926479Lon: -92.7158185SE Lat: 31.2918597Lon:  -92.7136704 

Additional Members:  GARWTR01 

Link To Map for AL01: http://la.itic.occinc.com/TT2Z-2N9-29Z-A9T 
Link To Map for CLEC02: http://la.itic.occinc.com/YT2Z-3NU-29Z-A9T 

Send To:  AL01      Seq No:  19 
Send To:  CLEC02    Seq No:   9 
