

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326690 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time: 10:28     Op: webusr2.0 
Original Call Date:  7/31/09            Time: 10:27     Op: lastacy.1102 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: CADDO 
Place: SHREVEPORT CITY 
Address: 8207        Street: TRIGGER TR 
Nearest Intersecting Street: MCCUTCHEON AVE 

Location:       MARK ENTIRE FRONT OF PROPERTY. AT SW CRNR OF INTERSECTION. 

Remarks: LM0902438          SJB             7/31/39 
 

Work to Begin Date:  8/04/09            Time: 10:30 AM 
Mark By       Date:  8/04/09            Time: 10:30 AM 

Ex. Coord: NW Lat: 32.4238738Lon: -93.7958244SE Lat: 32.4233262Lon:  -93.7951756 

Additional Members:  CSHREV01, SWE01 

Link To Map for ARK05: http://la.itic.occinc.com/VZGN-ATA-9W2-92N 
Link To Map for SH01: http://la.itic.occinc.com/YZVN-AT5-9W2-92N 

Send To:  ARK05     Seq No:  18 
Send To:  SH01      Seq No:  78 
