

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326237 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:08     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:08     Op: latorran.455 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: RAPIDES 
Place: KOLIN 
Address: 41          Street: HAUK RD 
Nearest Intersecting Street: LOCKER RD 

Location:       MARK ENTIRE PROPERTY. FOLLOW TEMP LINE LAID OUT. APX 100 YDS
: E OF INTERSECTION. CALL BEFORE GOING TO GET THE GATE OPEN @ (318)447-9261.
: FOR MORE INFO CALL NUGENT @ (318)419-1928. 

Remarks: LA0902508                SJB             7/31/13 
 

Work to Begin Date:  8/04/09            Time:  9:15 AM 
Mark By       Date:  8/04/09            Time:  9:15 AM 

Ex. Coord: NW Lat: 31.2785684Lon: -92.2960151SE Lat: 31.2778697Lon:  -92.2930840 

Additional Members:  ANRJ01, KORUBY01 

Link To Map for AL01: http://la.itic.occinc.com/3T24-2N9-29Z-A9V 
Link To Map for CLEC02: http://la.itic.occinc.com/TT24-3NU-29Z-A9V 

Send To:  AL01      Seq No:  28 
Send To:  CLEC02    Seq No:  11 
