

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326765 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time: 10:54     Op: webusr2.0 
Original Call Date:  7/31/09            Time: 10:53     Op: lalenora.1065 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: CAROLYN NORMAN                  Contact Phone: (337)261-4595 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: EAST CARROLL 
Place: LAKE PROVIDENCE TOWN 
Address: 1408        Street: SHORT 7TH ST 
Nearest Intersecting Street: KATE ST 

Location:       65 YDS S OF INTER--MARK ENTIRE PROPERTY--ALSO MARK FRONT OF
: PROPERTY FROM ADDRESS TO PED OPPOSITE 1411 KATE ST--FOLLOW TEMPORARY WIRE ON
: GROUND. 

Remarks: JOB#  LL0902315  CBN   07/31/28 
 

Work to Begin Date:  8/04/09            Time: 11:00 AM 
Mark By       Date:  8/04/09            Time: 11:00 AM 

Ex. Coord: NW Lat: 32.8009672Lon: -91.1856764SE Lat: 32.8002646Lon:  -91.1849942 

Additional Members:  ENTGY01, GSSTE03, LAGN01 

Link To Map for MO01: http://la.itic.occinc.com/5T2B-MNG-29Z-A9P 

Send To:  MO01      Seq No:  45 
