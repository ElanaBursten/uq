

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326101 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  8:37     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  8:34     Op: lacandac.1054 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: AVOYELLES 
Place: BUNKIE CITY 
Address: 193         Street: WINDMILL LN 
Nearest Intersecting Street: STATE HWY 3041 

Location:       MARK ENTIRE PROPERTY. FOLLOW TEMP LINE LAID OUT. APX 1/4 MI
: ESE OF LUKE MARTIN ON N OF HWY 3041 TO WINDMILL LN. FOR MORE INFO CALL MORAS
: @ (318)542-3155. 

Remarks: LA0902498                SJB             7/31/3 
 

Work to Begin Date:  8/04/09            Time:  8:45 AM 
Mark By       Date:  8/04/09            Time:  8:45 AM 

Ex. Coord: NW Lat: 31.0244818Lon: -92.1663046SE Lat: 31.0211587Lon:  -92.1644918 

Additional Members:  CLEC03, ETM01, GSLAF02, TEVERG01 

Link To Map for AL01: http://la.itic.occinc.com/7T2N-2N9-29Z-A9W 

Send To:  AL01      Seq No:  15 
