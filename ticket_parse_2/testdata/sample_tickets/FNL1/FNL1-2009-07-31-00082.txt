

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326124 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  8:42     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  8:41     Op: lajonath.1060 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: RAPIDES 
Place: PINEVILLE CITY 
Address: 144         Street: GUNTER RD 
Nearest Intersecting Street: CREDEUR RD 

Location:       MARK ENTIRE FRONT OF PROPERTY. FOLLOW TEMP LINE LAID OUT. APX
: 400 YDS SW OF INTERSECTION. FOR MORE INFO CALL NUGENT @ (318)419-1928. 

Remarks: LA0902500                SJB             7/31/5 
 

Work to Begin Date:  8/04/09            Time:  8:45 AM 
Mark By       Date:  8/04/09            Time:  8:45 AM 

Ex. Coord: NW Lat: 31.3418503Lon: -92.3007595SE Lat: 31.3390220Lon:  -92.2986350 

Additional Members:  APTV01, BUCK01, TLP03 

Link To Map for AL01: http://la.itic.occinc.com/B9A2-Z29-92T-N36 
Link To Map for CLEC02: http://la.itic.occinc.com/U9A2-Z39-U2T-N36 

Send To:  AL01      Seq No:  18 
Send To:  CLEC02    Seq No:   8 
