

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326516 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:54     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:53     Op: lajonath.1060 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: CADDO 
Place: SHREVEPORT CITY 
Address: 5431        Street: SHREVE HILLS NORTH 
Nearest Intersecting Street: SHREVE PARK DR 

Location:       SHEREV HILLS NORTH SUBD--MARK ENTIRE REAR OF PROPERTY. FOLLOW
: TEMP LINE LAID OUT. 

Remarks: LM0902435          SJB             7/31/36 
 

Work to Begin Date:  8/04/09            Time: 10:00 AM 
Mark By       Date:  8/04/09            Time: 10:00 AM 

Ex. Coord: NW Lat: 32.4343558Lon: -93.8421950SE Lat: 32.4333526Lon:  -93.8410349 

Additional Members:  CSHREV01, SWE01 

Link To Map for ARK05: http://la.itic.occinc.com/T9A2-ZA9-G2T-N7H 
Link To Map for SH01: http://la.itic.occinc.com/L9A2-Z59-V2T-N7H 

Send To:  ARK05     Seq No:  13 
Send To:  SH01      Seq No:  60 
