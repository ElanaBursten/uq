

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326496 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:51     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:50     Op: latorran.455 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: WEBSTER 
Place: MINDEN CITY 
Address:             Street: SOUTHRIDGE DR 
Nearest Intersecting Street: RIDGEWOOD CIR 

Location:       MARK ENTIRE REAR OF PROPERTY FROM 712 THRU 1000 SOUTHRIDGE
: DR. APX 170 YDS S OF SOUTHERN PART OF INTERSECTION. 

Remarks: LM0902433          SJB             7/31/34 
 

Work to Begin Date:  8/04/09            Time: 10:00 AM 
Mark By       Date:  8/04/09            Time: 10:00 AM 

Ex. Coord: NW Lat: 32.6156418Lon: -93.2595152SE Lat: 32.6138581Lon:  -93.2578101 

Additional Members:  MINDEN01 

Link To Map for ARK02: http://la.itic.occinc.com/M9A2-ZA9-Z2T-N72 
Link To Map for SH01: http://la.itic.occinc.com/G9A2-Z59-V2T-N72 

Send To:  ARK02     Seq No:  19 
Send To:  SH01      Seq No:  57 
