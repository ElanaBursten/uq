

NOTICE OF INTENT TO EXCAVATE 
UPDT-UPDATE                             TICKET DEFAULT 
Ticket No: 90326735 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time: 10:41     Op: latony.1072 
Original Call Date:  7/31/09            Time: 10:40     Op: latony.1072 

Company     : DEVINEY CONSTRUCTION 
Contact Name: ARMANDO                         Contact Phone: (318)220-9064 
Alt. Contact: ANYONE 

Type of Work : BURYING TELEPHONE CABLE 
Work Done For: AT&T 

State: LA       Parish: CADDO 
Place: SHREVEPORT CITY 
Address:             Street: FERNWOOD LN 
Nearest Intersecting Street: TIMBERVIEW 

Location:       3433 FERNWOOD LN ---MARK REAR EASEMENT OF PROP GOING E APX
: 1900FT TO REAR EASEMENT OF PROP AT 3201 VALLEY VIEW DR 

Remarks: UPDATE 90306172 ONGOING 
 

Work to Begin Date:  8/04/09            Time: 10:45 AM 
Mark By       Date:  8/04/09            Time: 10:45 AM 

Ex. Coord: NW Lat: 32.4354126Lon: -93.7942150SE Lat: 32.4347652Lon:  -93.7904730 

Additional Members:  CSHREV01, CSHREV02, SWE01 

Link To Map for ARK05: http://la.itic.occinc.com/F9Z9-G22-TAN-927 
Link To Map for SH01: http://la.itic.occinc.com/89Z9-V22-T5N-927 

Send To:  ARK05     Seq No:  19 
Send To:  SH01      Seq No:  82 
