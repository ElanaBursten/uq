

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326375 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:40     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:39     Op: lajameel.1052 

Company     : DEVINEY CONSTRUCTION COMPANY 
Contact Name: KEN SMITH                       Contact Phone: (318)361-5146 
Alt. Contact: KEN SMITH                       Alt. Phone   : (318)381-2303 

Type of Work : PLACE TELEPHONE CABLE 
Work Done For: AT&T 

State: LA       Parish: UNION 
Place: FARMERVILLE TOWN 
Address:             Street: WEST PORT UNION RD. 
Nearest Intersecting Street: ELDON ROGERS RD. 

Location:       BEGIN AT THE SOUTHWEST CORNER OF THE INTERSECTION OF WEST
: PORT UNION RD.&ELDON ROGERS RD.MARK THE SOUTHSIDE(FRONT OF PROPERTY)OF WEST
: PORT UNION RD.TO THE WEST FOR 250 FT. 

Remarks:  
 

Work to Begin Date:  8/04/09            Time:  9:45 AM 
Mark By       Date:  8/04/09            Time:  9:45 AM 

Ex. Coord: NW Lat: 32.7385580Lon: -92.2860603SE Lat: 32.7370790Lon:  -92.2842802 

Additional Members:  

Link To Map for ARK02: http://la.itic.occinc.com/UKZ9-Z22-TAN-9AG 
Link To Map for MO01: http://la.itic.occinc.com/RKZ9-G22-TMN-9AG 

Send To:  ARK02     Seq No:  14 
Send To:  MO01      Seq No:  30 
