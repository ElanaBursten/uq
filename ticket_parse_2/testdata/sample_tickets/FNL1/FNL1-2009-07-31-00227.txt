

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326804 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time: 11:18     Op: webusr2.0 
Original Call Date:  7/31/09            Time: 11:16     Op: lajonath.1060 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: CAROLYN NORMAN                  Contact Phone: (337)261-4595 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: UNION 
Place: FARMERVILLE TOWN 
Address: 212         Street: PINEWOODS RD 
Nearest Intersecting Street: DOGWOOD DR 

Location:       MARK ENTIRE PROPERTY--ALSO MARK FRONT OF PROPERTY FROM
: ADDRESS TO PED 5 ON PINEWOODS RD. 

Remarks: JOB#  LL0902321  CBN   07/31/34 
 

Work to Begin Date:  8/04/09            Time: 11:30 AM 
Mark By       Date:  8/04/09            Time: 11:30 AM 

Ex. Coord: NW Lat: 32.7203878Lon: -92.3448112SE Lat: 32.7166414Lon:  -92.3431491 

Additional Members:  ENTGY01 

Link To Map for ARK02: http://la.itic.occinc.com/K9A2-ZA9-Z2T-NCH 
Link To Map for MO01: http://la.itic.occinc.com/A9A2-ZM9-G2T-NCH 

Send To:  ARK02     Seq No:  29 
Send To:  MO01      Seq No:  59 
