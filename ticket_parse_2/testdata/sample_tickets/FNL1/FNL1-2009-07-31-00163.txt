

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326506 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:52     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:52     Op: latorran.455 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: CADDO 
Place: BLANCHARD TOWN 
Address: 5790        Street: PINE HILL RD 
Nearest Intersecting Street: LINDA LN 

Location:       MARK ENTIRE PROPERTY. APX 170 YDS E OF INTERSECTION. 

Remarks: LM0902434          SJB             7/31/35 
 

Work to Begin Date:  8/04/09            Time: 10:00 AM 
Mark By       Date:  8/04/09            Time: 10:00 AM 

Ex. Coord: NW Lat: 32.5841741Lon: -93.8571976SE Lat: 32.5832240Lon:  -93.8546591 

Additional Members:  BLANUT01, CABLEM01, CADDOS01, ELPASO02, PANOLA01, PHWTR01 
 SWE01 

Link To Map for ARK01: http://la.itic.occinc.com/YZ3N-ATA-972-92D 
Link To Map for SH01: http://la.itic.occinc.com/VZVN-AT5-972-92D 

Send To:  ARK01     Seq No:  14 
Send To:  SH01      Seq No:  58 
