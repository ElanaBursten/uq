

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326676 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time: 10:23     Op: webusr2.0 
Original Call Date:  7/31/09            Time: 10:23     Op: lajameel.1052 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: DE SOTO 
Place: KEACHI TOWN 
Address: 1066        Street: KEATCHIE RD 
Nearest Intersecting Street: STATE HWY 172 

Location:       MARK ENTIRE PROPERTY. FOLLOW TEMP LINE LAID OUT. APX 3/10 MI
: N OF INTERSECTION. 

Remarks: LM0902437          SJB             7/31/38 
 

Work to Begin Date:  8/04/09            Time: 10:30 AM 
Mark By       Date:  8/04/09            Time: 10:30 AM 

Ex. Coord: NW Lat: 32.1949965Lon: -93.9116563SE Lat: 32.1927081Lon:  -93.9099387 

Additional Members:  GOODPE06, KEAWTR01, TLN02, VEFAUL01 

Link To Map for CLEC01: http://la.itic.occinc.com/P9A2-Z39-Z2T-NHH 
Link To Map for SH01: http://la.itic.occinc.com/Q9A2-Z59-V2T-NHH 

Send To:  CLEC01    Seq No:  16 
Send To:  SH01      Seq No:  76 
