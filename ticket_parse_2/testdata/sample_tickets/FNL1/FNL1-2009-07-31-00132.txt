

NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No: 90326192 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time:  9:11     Op: webusr2.0 
Original Call Date:  7/31/09            Time:  9:11     Op: lajonath.1060 

Company     : AT&T UTILITY OPERATIONS 
Contact Name: SHARON BRASSEAUX                Contact Phone: (337)261-4611 
Alt. Contact: PAM WELTY                       Alt. Phone   : (337)261-4491 

Type of Work : BURY TELEPHONE SERVICE WIRE 
Work Done For: AT&T UTILITY OPERATIONS 

State: LA       Parish: RAPIDES 
Place: PINEVILLE CITY 
Address: 4137        Street: STATE HWY 28 E 
Nearest Intersecting Street: FROUST ST 

Location:       MARK ENTIRE FRONT OF PROPERTY. FOLLOW TEMP LINE LAID OUT. APX
: 40 YDS ENE OF INTERSECTION FOLLOWING DIRECTION OF RD. FOUND ON YAHOO MAPS.
: FOR MORE INFO CALL NUGENT @ (318)419-1928. 

Remarks: LA0902505                SJB             7/31/10 
 

Work to Begin Date:  8/04/09            Time:  9:15 AM 
Mark By       Date:  8/04/09            Time:  9:15 AM 

Ex. Coord: NW Lat: 31.3393159Lon: -92.3699846SE Lat: 31.3377482Lon:  -92.3680759 

Additional Members:  APTV01, CPINE01, TLP03 

Link To Map for AL01: http://la.itic.occinc.com/F9N2-Z29-92T-NUF 
Link To Map for CLEC02: http://la.itic.occinc.com/X9N2-Z39-U2T-NUF 

Send To:  AL01      Seq No:  31 
Send To:  CLEC02    Seq No:  13 
