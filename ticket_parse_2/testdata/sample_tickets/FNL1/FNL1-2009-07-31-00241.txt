

NOTICE OF INTENT TO EXCAVATE 
UPDT-UPDATE                             TICKET DEFAULT 
Ticket No: 90326841 
Update Of:        0                     Updated By:         0 
Transmit      Date:  7/31/09            Time: 11:28     Op: lacandac.1054 
Original Call Date:  7/31/09            Time: 11:27     Op: lacandac.1054 

Company     : DEVINEY CONSTRUCTION 
Contact Name: ARMANDO                         Contact Phone: (318)220-9064 
Alt. Contact: ANYONE 

Type of Work : BURYING TELEPHONE CABLE 
Work Done For: AT&T 

State: LA       Parish: CADDO 
Place: SHREVEPORT CITY 
Address: 6971        Street: W 70TH ST 
Nearest Intersecting Street: CARAVAN RD 

Location:       ON S SIDE OF RD----MARK FRONT OF PROP GOING W APX 400FT 

Remarks: UPDATE 90306309 ONGOING 
 

Work to Begin Date:  8/04/09            Time: 11:30 AM 
Mark By       Date:  8/04/09            Time: 11:30 AM 

Ex. Coord: NW Lat: 32.4427619Lon: -93.8998901SE Lat: 32.4419365Lon:  -93.8911263 

Additional Members:  ATT01, CSHREV01, PANOLA01, SWE01 

Link To Map for ARK05: http://la.itic.occinc.com/3T2E-ANG-29Z-29E 
Link To Map for SH01: http://la.itic.occinc.com/UT2E-5NV-29Z-29E 

Send To:  ARK05     Seq No:  24 
Send To:  SH01      Seq No:  96 
