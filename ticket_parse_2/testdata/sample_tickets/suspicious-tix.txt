UTIL11 00654 VUPSa 08/26/02 11:53:39 A223801150-00A NORM NEW GRID

Ticket : A223801150 Rev: 00A Taken: 08/26/02 10:50AM Oper: WSWR

State: VA Cnty: LOUISA Place:
Subdivision: FRESHWATER ESTATES  Lot: 41

Address :  368  Street: DICKINSON DR
Cross 1 : HENSLEY RD  Intersection: N
Lot side:    Whitelined: N  Blasting: N  Boring: N
Location: END OF RT. 720 OFF RT. 522
Instruct: SPLICE EXISTING UNDERGROUND AND RUN NEW UNDERGROUND 50' TO NEW METER
        : LOCATION.
WorkType: INSTALLING UG ELECTRIC SERVICE
Done for: CUSTOMER/BUTLER CONST.

Map book: 47-25
Grids: 3803B7750A    3803B7750B    3803C7750A    3803C7750B    3803B7751D
Grids: 3803C7751C    3803C7751D

Work date: 08/29/02 07:00AM  Due by   : 08/29/02 07:00AM
Expires  : 09/18/02 07:00AM  Update by: 09/13/02 12:00AM  Sent: 08/26/02 11:53AM

Company : RAPPAHANNOCK ELECTRIC COOPERAT  Type: UTIL
Co addr : PO  BOX 308 Phone: 804-633-5011 Ext: 5545
City    : BOWLING GREEN State: VA Zip: 22427
Caller  : SHARON WRIGHT Phone: 804-633-5565
Call Back Time: 8AM-5PM  Email: SWRIGHT@RAPPELEC.COM
Field contact : SHARON WRIGHT Phone: 804-632-5565 Ext: 5565
Fax     : 804-632-5556

Members:
BAO281 = VERIZON COMM.-UTILIQUEST     CLSP02 = DO NOT READ TO CALLER
REL423 = RAPP ELEC COOP - LOUISAUTIL11 00657 VUPSa 08/26/02 11:55:11 A223801157-00A EMER NEW GRID

Ticket : A223801157 Rev: 00A Taken: 08/26/02 11:51AM Oper: 1BMF

State: VA Cnty: AMHERST Place:

Address :  172  Street: ANN ST
Cross 1 : UNITED STATES HIGHWAY 29  Intersection: N
Lot side:    Whitelined: Y  Blasting: N  Boring: Y
Location: MARK BOTH SIDES OF THE ROAD BETWEEN THE BLUE FLAGS AND PAINT
Instruct: ***EMERGENCY*** CREW WILL BE ON SITE AT 1PM
WorkType: REPLACE WATER LINE
Done for: AMHERST CO SERV AUTH

Map book: 127
Grids: 3728A7907C    3728A7907D    3728B7907C    3728B7907D

Emergency: Y
Work date: 08/26/02 11:53AM
Expires  : 09/18/02 07:00AM  Update by: 09/13/02 12:00AM  Sent: 08/26/02 11:54AM

Company : MADISON HEIGHTS WATER DEPT.  Type: CONT
Co addr : P. O. BOX 100 Phone: 804-384-1979
City    : MADISON HEIGHTS State: VA Zip: 24521
Caller  : MACK MAYS* Phone: 804-384-1979
Call Back Time: 8AM-5PM
Fax     : 804-845-1613

Members:
ACA211 = AMHERST CO. SERV. AUTH.      ALY275 = ADELPHIA
APRO61 = AEP-ROANOKE                  CPLY72 = VERIZON COMM. - PROMARK
LYGC82 = COLUMBIA GAS/UTILIQUEST      MCII81 = MCI WORLDCOMNOTICE OF INTENT TO EXCAVATE
Ticket No: 00598382               Update Of: 00578114
Transmit      Date: 08/26/02      Time: 11:53    Op: brandi
Original Call Date: 08/26/02      Time: 11:53    Op: brandi
Work to Begin Date: 08/28/02      Time: 12PM

Place: DAMASCUS
Address: 9307        Street: HEATHERFIELD CT
Nearest Intersecting Street: BONNY BROOKE LA

Type of Work: DIG BASEMENT,BACKFILL,FINE GRADE,INST DRIVEWAY &
Extent of Work:   LOC ENTIRE LOT# 76, SUBD- SENECA BROOKE
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (301)831-3015

Company     : VALLEY EXCAVATING
Contact Name: JUANITA CULLOP        Contact Phone: (301)831-6706
Alt. Contact:                       Alt. Phone   :
Work Being Done For: CULVERTS/  ROCKY GORGE
State: MD              County: MONT
Map: MONT  Page: 004   Grid Cells: F11
Explosives: N
ATM01      PTE02      TFH02      XZVALLEY
Send To: TRU   01  Seq No: 0124   Map Ref:
         WGL   06          0438NOTICE OF INTENT TO EXCAVATE
Ticket No: 00598380
Transmit      Date: 08/26/02      Time: 11:53    Op: shekia
Original Call Date: 08/26/02      Time: 11:53    Op: shekia
Work to Begin Date: 08/28/02      Time: 12PM

Place: BOWIE
Address:             Street: JUNIPER DR
Nearest Intersecting Street: CHINABERRY CT

Type of Work: INST CATV MAIN LINE
Extent of Work:   LOC ENTIRE PROP FROM ADDRESSES 10008 THROUGH 10014
: JUNIPER DR
Remarks:
:                                                        Fax: (301)218-8523

Company     : EASTCOMM
Contact Name: KIM TYSON             Contact Phone: (301)218-8520
Alt. Contact: AMBER                 Alt. Phone   :
Work Being Done For: EASTCOMM
State: MD              County: PG
Map: PG    Page: 013   Grid Cells: K11
Explosives: N
PEP05
Send To: JTV   02  Seq No: 0065   Map Ref:
         TPG   01          0122
         WGL   06          0439
         WSS   01          0330NOTICE OF INTENT TO EXCAVATE
Ticket No: 00598383
Transmit      Date: 08/26/02      Time: 11:54    Op: shekia
Original Call Date: 08/26/02      Time: 11:53    Op: shekia
Work to Begin Date: 08/28/02      Time: 12PM

Place: BOWIE
Address:             Street: JUNIPER DR
Nearest Intersecting Street: CHINABERRY CT

Type of Work: INST CATV MAIN LINE
Extent of Work:   LOC ENTIRE PROP FROM ADDRESSES 10016 THROUGH 10026
: JUNIPER DR
Remarks:
:                                                        Fax: (301)218-8523

Company     : EASTCOMM
Contact Name: KIM TYSON             Contact Phone: (301)218-8520
Alt. Contact: AMBER                 Alt. Phone   :
Work Being Done For: EASTCOMM
State: MD              County: PG
Map: PG    Page: 013   Grid Cells: K11
Explosives: N
PEP05
Send To: JTV   02  Seq No: 0066   Map Ref:
         TPG   01          0123
         WGL   06          0440
         WSS   01          0331