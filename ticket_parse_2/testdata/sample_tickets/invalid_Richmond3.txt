UTIL11 00166 VUPSa 07/01/02 09:37:46 A218200335-00A NORM NEW GRID

Ticket : A218200335 Rev: 00A Taken: 07/01/02 09:16AM Oper: 1AMM

State: VA Cnty: RAPPAHANNOCK Place:

Address :  242  Street: TIGER VALLEY RD
Lot side:    Whitelined: N  Blasting: N  Boring: N
Location: LOCATE:  605 FT ON STATE ROUTE 626 STARTING AT THE INTERSECTION OF
        : PINE TREE LANE GOING SW FROM ALLEGANY POLE #L47118 APPROX 600 FEET
        : WITHIN THE THE EXISTING 30 FT WIDE EASEMENT ON THE EAST SIDE OF STATE
        : ROUTE 626 AFTER THE 605 FEET TRENCHING WILL CONTINUE EAST ON PRIVATE
        : PROPERTY, THIS IS A FIELD AREA TO POWER PED THAT SITS AT THE RIGHT
        : SIDE OF 242 TIGER VALLEY RD.  THIS IS APPROX 1100 FEET THIS IS THE
        : FIRST PART OF THE LOCATE. 2ND PART OF LOCATE ENTIRE PROPERTY OF 242
        : TIGER VALLEY RD (HOUSE SITS OFF ROAD APPROX 500 FEET)
WorkType: REPAIR CATV/FIBER LINE
Done for: ALDEPHIA CABLE

Grids: 3841A7808A    3841A7808B    3842B7809C    3842B7809D    3842C7808A
Grids: 3842C7808B    3842C7809C    3842C7809D    3842D7808A    3842D7808B
Grids: 3842D7809C    3842D7809D

Work date: 07/08/02 07:00AM  Due by   : 07/08/02 07:00AM
Expires  : 07/25/02 07:00AM  Update by: 07/22/02 12:00AM  Sent: 07/01/02 09:37AM

Company : *MCLAUGLIN COMMUNICATIONS  Type: CONT
Co addr : 136 LIKENS WAY Phone: 540-722-0242
City    : WINCHESTER State: VA Zip: 22602
Caller  : MARK SCHUTT Phone: 540-722-0242
Call Back Time: 10AM-5PM  Email: maschutt@aldephia.cnet
Field contact : MARK SCHUTT Phone: 540-974-0593

Members:
BAC265 = VERIZON COMM.-UTILIQUEST     PEP119 = ALLEGHENY POWER-UTILIQUE
SLTD54 = SPRINT LOCAL TELEPHONE DIV   UTIL11 = DO NOT READ TO CALLER

