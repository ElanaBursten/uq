\
NOTICE OF INTENT TO EXCAVATE      EMERGENCY
Ticket No: 00357291
Transmit      Date: 05/14/02      Time: 10:29    Op: ellen.
Original Call Date: 05/14/02      Time: 10:26    Op: ellen.
Work to Begin Date: 05/14/02      Time: 10AM

Place: NE
Address: 1732        Street: MONTELLO AV NE
Nearest Intersecting Street: MT OLIVE AV NE

Type of Work: REPAIRING ELEC SVC
Extent of Work:   SEE CREW ON SITE FOR LOC INSTRUCTIONS.
Remarks: EMERGENCY ** CREW ON SITE
:                                                        Fax: (301)699-7750

Company     : B FRANK JOY
Contact Name: ERICA WILSON          Contact Phone: (301)699-6000
Alt. Contact: ERICA WILSON          Alt. Phone   : (301)699-6029
Work Being Done For: PEPCO
State: DC              County: NE
Map: NE    Page: 010   Grid Cells: G11
Explosives: N
ATT01      DCC01      LTC02      MCI03      PEP06      QWESTM02
Send To: TDC   01  Seq No: 0120   Map Ref:
         WGL   07          0120



NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 00339322
Transmit      Date: 05/14/02      Time: 10:29    Op: carol
Original Call Date: 05/08/02      Time: 07:46    Op: carol
Work to Begin Date: 05/10/02      Time: 08AM

Place: GAITHERSBURG
Address: 701         Street: HIGHLAND RIDGE AV
Nearest Intersecting Street: SUMMER WALK DR

Type of Work: INST SWR AND WTR SVC
Extent of Work:   LOC ENTIRE PROP OF LOT 48G WHICH IS THE ABOVE ADDRESS
: KATHY MCALLISTER STATES ALL UTILS PLEASE REMARK MARKS WASHED AWAY-OJLG
: 5/14 10:32AM
Remarks:
:                                                        Fax: (301)428-1781

Company     : BEN LEWIS INC
Contact Name: TAMMY KITZMILLER      Contact Phone: (301)428-3900
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: RYAN HOMES
State: MD              County: MONT
Map: MONT  Page: 019   Grid Cells: B12
Explosives: N
MCI01      PEP05
Send To: TMT   01  Seq No: 0105   Map Ref:
         TRU   01          0102
         WGL   06          0353
         WSS   01          0273



NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 00339335
Transmit      Date: 05/14/02      Time: 10:29    Op: carol
Original Call Date: 05/08/02      Time: 07:48    Op: carol
Work to Begin Date: 05/10/02      Time: 08AM

Place: GAITHERSBURG
Address:             Street: HIGHLAND RIDGE AV
Nearest Intersecting Street: SUMMER WALK DR

Type of Work: INST SWR AND WTR SVC
Extent of Work:   LOC ENTIRE PROP OF LOTS 45G AND 46G ADDRESS
: KATHY MCALLISTER STATES ALL UTILS PLEASE REMARK MARKS WERE WASHED AWAY
: OJLG 5/14 10:30AM
Remarks:
:                                                        Fax: (301)428-1781

Company     : BEN LEWIS INC
Contact Name: TAMMY KITZMILLER      Contact Phone: (301)428-3900
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: RYAN HOMES
State: MD              County: MONT
Map: MONT  Page: 019   Grid Cells: B12
Explosives: N
MCI01      PEP05
Send To: TMT   01  Seq No: 0106   Map Ref:
         TRU   01          0103
         WGL   06          0354
         WSS   01          0274



NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 00339363
Transmit      Date: 05/14/02      Time: 10:29    Op: carol
Original Call Date: 05/08/02      Time: 07:51    Op: carol
Work to Begin Date: 05/10/02      Time: 08AM

Place: BOYDS
Address:             Street: COUNTRY MEADOWS RD
Nearest Intersecting Street: CLEAR SMOKE RD

Type of Work: INST SWR AND WTR SVC
Extent of Work:   LOC ENTIRE PROP OF LOTS 5M AND 2M WHICH IS THE ABOVE
: ADDRESS
: KATHY MCALLISTER STATES ALL UTILS PLEASE REMARK MARKS WERE WASHED
: AWAY-OJLG 5/14 10:29AM
Remarks:
:                                                        Fax: (301)428-1781

Company     : BEN LEWIS INC
Contact Name: TAMMY KITZMILLER      Contact Phone: (301)428-3900
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: RYAN HOMES
State: MD              County: MONT
Map: MONT  Page: 017   Grid Cells: K04
Explosives: N
PEP05      PTE02
Send To: TMT   01  Seq No: 0107   Map Ref:
         TRU   01          0104
         WGL   06          0355
         WSS   01          0275



NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 00339383
Transmit      Date: 05/14/02      Time: 10:29    Op: carol
Original Call Date: 05/08/02      Time: 07:53    Op: carol
Work to Begin Date: 05/10/02      Time: 08AM

Place: BOYDS
Address:             Street: COUNTRY MEADOWS RD
Nearest Intersecting Street: ASHMEADE RD

Type of Work: INST SWR AND WTR SVC
Extent of Work:   LOC ENTIRE PROP OF LOTS 10M AND 3M WHICH IS THE ABOVE
: ADDRESS
: KATHY MCALLISTER STATES ALL UTILS PLEASE REMARK MARKS WERE WASHED
: AWAY-OJLG 5/14 10:27AM
Remarks:
:                                                        Fax: (301)428-1781

Company     : BEN LEWIS INC
Contact Name: TAMMY KITZMILLER      Contact Phone: (301)428-3900
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: RYAN HOMES
State: MD              County: MONT
Map: MONT  Page: 017   Grid Cells: K04
Explosives: N
PEP05      PTE02
Send To: TMT   01  Seq No: 0108   Map Ref:
         TRU   01          0105
         WGL   06          0356
         WSS   01          0276



NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 00339404
Transmit      Date: 05/14/02      Time: 10:29    Op: carol
Original Call Date: 05/08/02      Time: 07:55    Op: carol
Work to Begin Date: 05/10/02      Time: 08AM

Place: BOYDS
Address: 18405       Street: CLEAR SMOKE RD
Nearest Intersecting Street: COUNTRY MEADOWS RD

Type of Work: INST SWR AND WTR SVC
Extent of Work:   LOC ENTIRE PROP OF LOT 16E WHICH IS THE ABOVE ADDRESS
: KATHY MCALLISTER STATES ALL UTILS PLEASE REMARK MARKS WERE WASHED
: AWAY-OJLG 5/14 10:28AM
Remarks:
:                                                        Fax: (301)428-1781

Company     : BEN LEWIS INC
Contact Name: TAMMY KITZMILLER      Contact Phone: (301)428-3900
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: RYAN HOMES
State: MD              County: MONT
Map: MONT  Page: 017   Grid Cells: K04
Explosives: N
PEP05      PTE02
Send To: TMT   01  Seq No: 0109   Map Ref:
         TRU   01          0106
         WGL   06          0357
         WSS   01          0277



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356746
Transmit      Date: 05/14/02      Time: 10:29    Op: emily
Original Call Date: 05/14/02      Time: 09:26    Op: emily
Work to Begin Date: 05/16/02      Time: 09AM

Place: LAUREL
Address: 14207       Street: GREENVIEW DR
Nearest Intersecting Street: CLUBHOUSE BLVD

Type of Work: INST DECK
Extent of Work:   LOC REAR OF PROP, PROP IS S OF THE INTER JUST 1BLOCK PASS
: SUMMIT DR, BUT NOT IN CUL-DE-SAC
Remarks: PER CALLER--WOULD NOT GIVE APPROX, FOOTAGE OR MILEAGE, BEST
: INFORMATION, LEGALS NOT PROVIDED FOR THIS LOCATE REQUEST

Company     : PRISCILLA LINAN
Contact Name: HOMEOWNER             Contact Phone: (301)490-8356
Alt. Contact: WORK                  Alt. Phone   : (410)737-6094
Work Being Done For: PRISCILLA LINAN
State: MD              County: PG
Map: PG    Page: 004   Grid Cells: H06,J06
Explosives: N
ABS01      ACSI01     BGE09      LSW04      MCI01      TAN01
Send To: JTV   02  Seq No: 0025   Map Ref:
         WGL   06          0358
         WSS   01          0278



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00357054
Transmit      Date: 05/14/02      Time: 10:29    Op: r.site
Original Call Date: 05/14/02      Time: 10:02    Op: r.site
Work to Begin Date: 05/16/02      Time: 10AM

Place: SOMERSET
Address:             Street: BUTLER ROAD
Nearest Intersecting Street: RIVER ROAD

Type of Work: GRADING AND UTILITY WORK
Extent of Work:   THIS IS AN EXISTING WORKING PROJECT. THE JOB IS ON THE
: RIGHT ON BUTLER ROAD COMING FROM RIVER ROAD. MARK AS PER PREVIOUS MEETING
: HELD ON 08-31-01 @ 10:00 AM. THIS REPLACES UPDATE TICKET #311787. JOBSITE
: NAME IS STORAGE U.S.A. @ BUTLER ROAD.
Remarks:
:                                                        Fax: (410)686-8559

Company     : IACOBONI SITE SPECIALIST INC
Contact Name: JEFF MILLER           Contact Phone: (410)686-2100
Alt. Contact: LINDA CROMWELL        Alt. Phone   :
Work Being Done For: CHAMBERLIN CONSTRUCTION
State: MD              County: MONT
Map: MONT  Page: 040   Grid Cells: K02
Explosives: N
ACSI03     LSW04      MCI01      PEP05
Send To: TMT   02  Seq No: 0098   Map Ref:
         TRU   02          0079
         WGL   06          0359
         WSS   01          0279



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00357241
Transmit      Date: 05/14/02      Time: 10:29    Op: dawn.s
Original Call Date: 05/14/02      Time: 10:21    Op: dawn.s
Work to Begin Date: 05/16/02      Time: 10AM

Place: SE
Address:             Street: S CAPTIAL ST SE
Nearest Intersecting Street: VIRGINA AV SE

Type of Work: INST FIBER DUCT
Extent of Work:   LOC S CAPTIAL ST SE CURB TO CURB FROM VIRGINA AV SE
: APPROX 300FT HEADING N ON S CAPTIAL ST SE INCLUDING A 15FT RADIUS IN
: SIDEWALK ON BOTH SIDES
Remarks: BEST INFORMATION CALLER COULD PROVIDE
:                                                        Fax: (703)327-5825

Company     : JONES UTILITIES CONST, INC.
Contact Name: LAUREN JONES          Contact Phone: (703)327-0024
Alt. Contact: BUTCH SIMMONS         Alt. Phone   : (703)283-4065
Work Being Done For: FIRST SOUTH UTILITIES
State: DC              County: SE
Map: SE    Page: 016   Grid Cells: D04
Explosives: N
ACSI04     ATT01      DCC01      LSW02      LTC02      MCI03      MFN03
 PEP06      QWESTM02
Send To: TDC   01  Seq No: 0121   Map Ref:
         WGL   07          0121



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00357263
Transmit      Date: 05/14/02      Time: 10:29    Op: l.boot
Original Call Date: 05/14/02      Time: 10:23    Op: l.boot
Work to Begin Date: 05/16/02      Time: 10AM

Place: LEXINGTON PARK
Address: 21921       Street: SPRING VALLEY DR
Nearest Intersecting Street: TIMBER VALLEY CT

Type of Work: INST CATV MAIN
Extent of Work:   LOC ENTIRE PROP
Remarks:
:                                                        Fax: (301)373-3757

Company     : G M P CABLE
Contact Name: QUAN BALL             Contact Phone: (301)373-3201
Alt. Contact: PAUL                  Alt. Phone   :
Work Being Done For: G M P CABLE
State: MD              County: ST MARYS COUNTY
Map: STMY  Page: 018   Grid Cells: G09
Explosives: N
LT02       SIM01      SMMC01     TAN02
Send To: WGL   06  Seq No: 0360   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00357300
Transmit      Date: 05/14/02      Time: 10:30    Op: janis
Original Call Date: 05/14/02      Time: 10:26    Op: janis
Work to Begin Date: 05/16/02      Time: 10AM

Place: NW
Address: 3475        Street: HOLMEAD PL NW
Nearest Intersecting Street: OTIS PL NW

Type of Work: REPLACING WTR SERV LINES N/E
Extent of Work:   LOC FRONT OF THE HSE TO THE CURB
Remarks: THIS TKT IS UPDATE OF TKT 109092 ( TKT NUMBER NOT GIVEN UNTIL END
: OF TKT )                                               Fax: (202)388-0654

Company     : SEIDEL PLUMBING & HEATING
Contact Name: JACKIE BELL           Contact Phone: (202)397-7000
Alt. Contact: JACKIE / OTTO  / CEL  Alt. Phone   : (202)359-3283
Work Being Done For: GARY CASE
State: DC              County: NW
Map: NW    Page: 009   Grid Cells: K08
Explosives: N
ATT01      DCC01      LTC02      MCI03      PEP06
Send To: STAR  02  Seq No: 0040   Map Ref:
         TDC   01          0122
         WGL   07          0122



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00357326               Update Of: 00319154
Transmit      Date: 05/14/02      Time: 10:30    Op: kanesh
Original Call Date: 05/14/02      Time: 10:29    Op: kanesh
Work to Begin Date: 05/16/02      Time: 10AM

Place: BOWIE
Address:             Street: CHURCH RD
Nearest Intersecting Street: MOUNT OAK RD

Type of Work: EXCAVATION FOR ACCESS RD
Extent of Work:   MEET WITH PETE PARRECO AT THE TALL OAKS VOCATIONAL HIGH
: SCHOOL IN THE PARKING LOT IN FRONT OF THE SCHOOL WHICH IS APPROX 1/2 MI
: FROM THE ABOVE INTER ON CHURCH RD.
Remarks: REMARK PER PREVIOUS MEETING INSTRUCTIONS ON UPDATE
: ANY ?'S CALL FRED TERRELL AT ABOVE #                   Fax: (301)277-8224

Company     : ARNOLD PARRECO & SON
Contact Name: FRED TERRELL          Contact Phone: (301)277-8220
Alt. Contact: RICK PARRECO          Alt. Phone   :
Work Being Done For: JAMES G SAKELLARIS
State: MD              County: PG
Map: PG    Page: 015   Grid Cells: A10
Explosives: N
BGE09
Send To: JTV   02  Seq No: 0026   Map Ref:
         TPG   01          0110
         WGL   06          0361



s<+
