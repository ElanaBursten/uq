SEQUENCE NUMBER 1209   CDC = PL4
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
MCI              ATMOS-MIDTX-UQ   TXU-ELEC-UQ      SM&P-D           
LONE STAR XCHG

Locate Request No. 072685747

Prepared By DANIELLE W           On 25-SEP-07  At 1614

MapRef :                            Grid: 325630096443B  Footprint: D02

Location:     County: DALLAS  Town: RICHARDSON

             Address: 700 S FLOYD RD 

Beginning Work Date 09/27/07 Time of Day: 04:15 pm   Duration: 02 DAYS 

Fax-A-Locate Date          at 

Excavation Type : POLE/SIGN INSTALLATION        
Nature of Work  : INSTALLING A SIGN             

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : DENNIS BERRY
Company Name   : CITY OF RICHARDSON
Work by CITY OF RICHARDSON      For CITY OF RICHARDSON

Person to Contact : DENNIS BERRY

Phone No.  ( 972 )744-4466 /(    )     ( Hours: 08:00 am/03:00 pm )
Fax No.    ( 972 )783-7251
Email:     DENNIS.BERRY@COR.GOV

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: JAMES DR 
WORK DATE: 2 WORKING DAY NOTICE   MAPSCO:   SUBDIVISION:   GATE CODE: 
  GRIDS:   LAT/LONG:   I NEED MARKINGS ON FLOYD ABOUT 120' NORTH OF JA
MES DR.I HAVE A STAKE ON THE RIGHT SIDE OF THE ROAD WITH HOT PINK PAIN
T ON IT.  724738.XML                                                  
                                                                      
                                                                      
                                                                      
DENNIS.BERRY@COR.GOV

Map Cross Reference : MAPSCO 16,H                         

FaxBack Requested ? YES    Lone Star Xref: 


072685747 to EMAIL ADDRESS at 17:06:34 on TUE, 09/25/07 for PL4 #1209