
NOTICE OF INTENT TO EXCAVATE        
Ticket No: 7 
Transmit      Date: 10/29/07      Time: 02:52 PM           Op: mdshan 
Original Call Date: 10/29/07      Time: 02:41 PM           Op: mdshan 
Work to Begin Date: 10/31/07      Time: 02:45 PM 

Place: TEST 
Address:             Street: TEST 
Nearest Intersecting Street: TEST 

Type of Work: TEST 
Extent of Work: THIS IS A TEST TICKET DO NOT LOCATE. THIS TICKET SHOULD INCLUDE
: 4 DIGITS IN THE MAP NUMBER FIELD, THE TIME OUTPUT AS 12 HOUR TIME (AM/PM)
: INCLUDING MINUTES, THE MAP FIELD SHOULD BE RENAMED TO MAP NAME AND THE PAGE
: FIELD RENAMED TO MAP#. PLEASE CONFIRM RECEIPT OF THIS TICKET ALONG WITH THE
: DISTRICT CODE IN THE SEND TO FIELD IN WRITING BY FAX TO SHANNON STULTZ AT
: 410-712-0062 OR EMAIL AT SHANNONSTULTZ@MISSUTILITY.NET. IF YOU HAVE ANY
: QUESTIONS OR ARE INTERESTED IN THE TICKET FORMAT WITH THE MEMBER NAMES AND
: NUMBERS PLEASE CONTACT SHANNON STULTZ AT 410-782-2057. 
Remarks: THIS IS A TEST DO NOT LOCATE 

Company      : MISS UTILITY CENTER 
Contact Name : SHANNON STULTZ                 Fax          : (410)712-0062 
Contact Phone: (410)782-2057                  Ext          :  
Alt. Contact : CALLER ID FOR SHANNO           Alt. Phone   :  
Work Being Done For: TEST 
State: MD              County: AA 
MPG:  N 
Caller    Provided:  Map Name: AA    Map#: 1234 Grid Cells: A1 
Computer Generated:  Map Name:       Map#:      Grid Cells:  

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 0         Lon: 0           SE Lat: 0         Lon: 0 
Explosives: N 
ACC01      CAM01      DTI02      ECC01      ELG01      FNT01      FRG01 
 FRTV01     FTECH02    GART01     GORE01     GORE01EE   GWU01      HCU01 
 HDG01      HNI01      HTV01      HTV02      KFD01      KFD01C     LT00 
 LTC01      LVL3DC     MADI01     MAN01      MCI01      MES01      MFN04 
 MISSU03    MKA01      MLV01      MMP01      NEON01     OCSI01     PDWS01 
 PEP05      PEPCOEM    PERRY01    PF00       PFTC01     PLV01      PRAX01 
 PSC01      PTE00      QGS02      QWESTM01   RMS01      ROSE01     SCH01 
 SCT01      VAA 
Send To: HTV01U    Seq No: 0001   Map Ref:  

