

FROM TNOCS
UTILIQUEST - JACKSON
B05002RCV
AUDIT FOR 1/19/2009

FOR CODE B05002RCV

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
!     0001  090190084                 DELIVERED
      0002  090190099                 DELIVERED
      0003  090190110                 DELIVERED
      0004  090190175                 DELIVERED
      0005  090190179                 DELIVERED
      0006  090190233                 DELIVERED
      0007  090190240                 DELIVERED
      0008  090190265                 DELIVERED
      0009  090190296                 DELIVERED
!     0010  090190341                 DELIVERED
      0011  090190342                 DELIVERED
      0012  090190343                 DELIVERED
      0013  090190356                 DELIVERED
      0014  090190409                 DELIVERED
      0015  090190416                 DELIVERED
      0016  090190421                 DELIVERED
      0017  090190426                 DELIVERED
      0018  090190434                 DELIVERED
      0019  090190439                 DELIVERED
      0020  090190459                 DELIVERED
      0021  090190467                 DELIVERED
      0022  090190478                 DELIVERED
      0023  090190561                 DELIVERED
      0024  090190592                 DELIVERED
!     0025  090190627                 DELIVERED
      0026  090190634                 DELIVERED
      0027  090190638                 DELIVERED
      0028  090190658                 DELIVERED
      0029  090190702                 DELIVERED
      0030  090190762                 DELIVERED
      0031  090190773                 DELIVERED
!     0032  090190790                 DELIVERED
!     0033  090190792                 DELIVERED
      0034  090190826                 DELIVERED
      0035  090190829                 DELIVERED
      0036  090190846                 DELIVERED
      0037  090190848                 DELIVERED
      0038  090190883                 DELIVERED
      0039  090190885                 DELIVERED
      0040  090190889                 DELIVERED
      0041  090190896                 DELIVERED
      0042  090190899                 DELIVERED
      0043  090190901                 DELIVERED
      0044  090190902                 DELIVERED
      0045  090190904                 DELIVERED
      0046  090190923                 DELIVERED
      0047  090190931                 DELIVERED
      0048  090190939                 DELIVERED
      0049  090190943                 DELIVERED
      0050  090190944                 DELIVERED
      0051  090190950                 DELIVERED
      0052  090190954                 DELIVERED
      0053  090190963                 DELIVERED
      0054  090190991                 DELIVERED
      0055  090190994                 DELIVERED
      0056  090191009                 DELIVERED
      0057  090191015                 DELIVERED
      0058  090191022                 DELIVERED
      0059  090191024                 DELIVERED
      0060  090191031                 DELIVERED
      0061  090191038                 DELIVERED
      0062  090191041                 DELIVERED
!     0063  090191044                 DELIVERED
      0064  090191053                 DELIVERED
      0065  090191086                 DELIVERED
!     0066  090191090                 DELIVERED
      0067  090191112                 DELIVERED
      0068  090191136                 DELIVERED
      0069  090191141                 DELIVERED
      0070  090191156                 DELIVERED
!     0071  090191214                 DELIVERED
      0072  090191228                 DELIVERED
      0073  090191231                 DELIVERED
      0074  090191232                 DELIVERED
      0075  090191235                 DELIVERED
      0076  090191236                 DELIVERED
      0077  090191237                 DELIVERED
      0078  090191238                 DELIVERED
      0079  090191240                 DELIVERED
      0080  090191252                 DELIVERED
      0081  090191253                 DELIVERED

NORMAL    : 73
RESEND    : 0
EMERGENCY : 8
FAILED    : 0
TOTAL FOR B05002RCV: 81




LEGEND
-----------------------
  - NORMAL
* - RESEND
! - EMERGENCY


