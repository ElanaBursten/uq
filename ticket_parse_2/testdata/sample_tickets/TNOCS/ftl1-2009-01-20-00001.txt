

FROM TNOCS
UTILIQUEST - (KNOXVILLE)
UTILKNOX
AUDIT FOR 1/19/2009

FOR CODE UTILKNOX

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  090190017                 DELIVERED
!     0002  090190020                 DELIVERED
!     0003  090190021                 DELIVERED
      0004  090190022                 DELIVERED
!     0005  090190023                 DELIVERED
      0006  090190027                 DELIVERED
      0007  090190028                 DELIVERED
!     0008  090190031                 DELIVERED
      0009  090190036                 DELIVERED
      0010  090190045                 DELIVERED
      0011  090190049                 DELIVERED
      0012  090190054                 DELIVERED
      0013  090190056                 DELIVERED
      0014  090190059                 DELIVERED
      0015  090190060                 DELIVERED
!     0016  090190061                 DELIVERED
      0017  090190063                 DELIVERED
!     0018  090190070                 DELIVERED
      0019  090190074                 DELIVERED
!     0020  090190080                 DELIVERED
      0021  090190088                 DELIVERED
      0022  090190090                 DELIVERED
!     0023  090190093                 DELIVERED
      0024  090190101                 DELIVERED
      0025  090190105                 DELIVERED
      0026  090190106                 DELIVERED
      0027  090190108                 DELIVERED
      0028  090190109                 DELIVERED
      0029  090190111                 DELIVERED
      0030  090190118                 DELIVERED
      0031  090190120                 DELIVERED
      0032  090190124                 DELIVERED
!     0033  090190125                 DELIVERED
      0034  090190127                 DELIVERED
      0035  090190130                 DELIVERED
      0036  090190135                 DELIVERED
      0037  090190138                 DELIVERED
      0038  090190142                 DELIVERED
      0039  090190145                 DELIVERED
      0040  090190150                 DELIVERED
      0041  090190153                 DELIVERED
      0042  090190156                 DELIVERED
!     0043  090190167                 DELIVERED
      0044  090190174                 DELIVERED
      0045  090190178                 DELIVERED
      0046  090190183                 DELIVERED
!     0047  090190185                 DELIVERED
      0048  090190186                 DELIVERED
      0049  090190187                 DELIVERED
      0050  090190188                 DELIVERED
      0051  090190189                 DELIVERED
      0052  090190197                 DELIVERED
      0053  090190202                 DELIVERED
      0054  090190203                 DELIVERED
!     0055  090190204                 DELIVERED
      0056  090190219                 DELIVERED
      0057  090190243                 DELIVERED
      0058  090190255                 DELIVERED
      0059  090190261                 DELIVERED
!     0060  090190272                 DELIVERED
      0061  090190275                 DELIVERED
      0062  090190287                 DELIVERED
      0063  090190298                 DELIVERED
      0064  090190300                 DELIVERED
      0065  090190302                 DELIVERED
      0066  090190303                 DELIVERED
      0067  090190304                 DELIVERED
      0068  090190305                 DELIVERED
      0069  090190307                 DELIVERED
      0070  090190309                 DELIVERED
      0071  090190311                 DELIVERED
      0072  090190312                 DELIVERED
      0073  090190313                 DELIVERED
      0074  090190314                 DELIVERED
      0075  090190316                 DELIVERED
      0076  090190318                 DELIVERED
      0077  090190320                 DELIVERED
!     0078  090190322                 DELIVERED
      0079  090190321                 DELIVERED
      0080  090190323                 DELIVERED
      0081  090190325                 DELIVERED
      0082  090190326                 DELIVERED
      0083  090190327                 DELIVERED
      0084  090190329                 DELIVERED
      0085  090190330                 DELIVERED
      0086  090190331                 DELIVERED
      0087  090190332                 DELIVERED
      0088  090190333                 DELIVERED
      0089  090190334                 DELIVERED
      0090  090190335                 DELIVERED
      0091  090190336                 DELIVERED
      0092  090190338                 DELIVERED
      0093  090190339                 DELIVERED
!     0094  090190340                 DELIVERED
      0095  090190344                 DELIVERED
      0096  090190345                 DELIVERED
!     0097  090190346                 DELIVERED
      0098  090190348                 DELIVERED
      0099  090190351                 DELIVERED
      0100  090190353                 DELIVERED
      0101  090190355                 DELIVERED
      0102  090190358                 DELIVERED
      0103  090190359                 DELIVERED
      0104  090190360                 DELIVERED
      0105  090190361                 DELIVERED
      0106  090190363                 DELIVERED
      0107  090190365                 DELIVERED
      0108  090190366                 DELIVERED
      0109  090190368                 DELIVERED
      0110  090190369                 DELIVERED
      0111  090190372                 DELIVERED
      0112  090190374                 DELIVERED
      0113  090190377                 DELIVERED
      0114  090190382                 DELIVERED
      0115  090190385                 DELIVERED
!     0116  090190388                 DELIVERED
      0117  090190389                 DELIVERED
      0118  090190392                 DELIVERED
!     0119  090190401                 DELIVERED
!     0120  090190403                 DELIVERED
      0121  090190408                 DELIVERED
!     0122  090190410                 DELIVERED
      0123  090190414                 DELIVERED
!     0124  090190419                 DELIVERED
      0125  090190431                 DELIVERED
!     0126  090190435                 DELIVERED
      0127  090190438                 DELIVERED
      0128  090190443                 DELIVERED
      0129  090190444                 DELIVERED
      0130  090190448                 DELIVERED
      0131  090190450                 DELIVERED
      0132  090190453                 DELIVERED
      0133  090190466                 DELIVERED
      0134  090190468                 DELIVERED
      0135  090190472                 DELIVERED
      0136  090190474                 DELIVERED
      0137  090190477                 DELIVERED
      0138  090190480                 DELIVERED
      0139  090190482                 DELIVERED
      0140  090190484                 DELIVERED
      0141  090190485                 DELIVERED
      0142  090190487                 DELIVERED
      0143  090190489                 DELIVERED
      0144  090190492                 DELIVERED
      0145  090190495                 DELIVERED
      0146  090190497                 DELIVERED
      0147  090190500                 DELIVERED
      0148  090190501                 DELIVERED
      0149  090190503                 DELIVERED
      0150  090190505                 DELIVERED
      0151  090190507                 DELIVERED
      0152  090190509                 DELIVERED
      0153  090190511                 DELIVERED
      0154  090190512                 DELIVERED
      0155  090190513                 DELIVERED
      0156  090190515                 DELIVERED
      0157  090190516                 DELIVERED
      0158  090190517                 DELIVERED
      0159  090190518                 DELIVERED
      0160  090190520                 DELIVERED
      0161  090190521                 DELIVERED
      0162  090190522                 DELIVERED
      0163  090190523                 DELIVERED
      0164  090190524                 DELIVERED
      0165  090190526                 DELIVERED
      0166  090190527                 DELIVERED
      0167  090190528                 DELIVERED
      0168  090190530                 DELIVERED
      0169  090190531                 DELIVERED
      0170  090190532                 DELIVERED
      0171  090190533                 DELIVERED
      0172  090190535                 DELIVERED
      0173  090190536                 DELIVERED
      0174  090190537                 DELIVERED
      0175  090190538                 DELIVERED
      0176  090190540                 DELIVERED
      0177  090190542                 DELIVERED
      0178  090190544                 DELIVERED
      0179  090190546                 DELIVERED
      0180  090190547                 DELIVERED
      0181  090190551                 DELIVERED
      0182  090190553                 DELIVERED
      0183  090190557                 DELIVERED
      0184  090190560                 DELIVERED
      0185  090190562                 DELIVERED
      0186  090190566                 DELIVERED
      0187  090190567                 DELIVERED
      0188  090190570                 DELIVERED
      0189  090190572                 DELIVERED
      0190  090190575                 DELIVERED
      0191  090190582                 DELIVERED
      0192  090190583                 DELIVERED
      0193  090190587                 DELIVERED
      0194  090190591                 DELIVERED
      0195  090190598                 DELIVERED
      0196  090190604                 DELIVERED
      0197  090190608                 DELIVERED
      0198  090190626                 DELIVERED
      0199  090190632                 DELIVERED
      0200  090190645                 DELIVERED
      0201  090190653                 DELIVERED
      0202  090190656                 DELIVERED
      0203  090190659                 DELIVERED
      0204  090190662                 DELIVERED
      0205  090190665                 DELIVERED
      0206  090190667                 DELIVERED
      0207  090190670                 DELIVERED
      0208  090190675                 DELIVERED
      0209  090190679                 DELIVERED
      0210  090190682                 DELIVERED
      0211  090190689                 DELIVERED
      0212  090190690                 DELIVERED
      0213  090190699                 DELIVERED
      0214  090190701                 DELIVERED
      0215  090190709                 DELIVERED
!     0216  090190718                 DELIVERED
      0217  090190731                 DELIVERED
!     0218  090190732                 DELIVERED
      0219  090190739                 DELIVERED
!     0220  090190740                 DELIVERED
      0221  090190768                 DELIVERED
      0222  090190770                 DELIVERED
      0223  090190776                 DELIVERED
      0224  090190778                 DELIVERED
      0225  090190780                 DELIVERED
      0226  090190781                 DELIVERED
      0227  090190784                 DELIVERED
      0228  090190794                 DELIVERED
      0229  090190796                 DELIVERED
      0230  090190798                 DELIVERED
      0231  090190801                 DELIVERED
      0232  090190802                 DELIVERED
      0233  090190806                 DELIVERED
      0234  090190812                 DELIVERED
!     0235  090190822                 DELIVERED
!     0236  090190823                 DELIVERED
!     0237  090190830                 DELIVERED
!     0238  090190841                 DELIVERED
      0239  090190842                 DELIVERED
      0240  090190850                 DELIVERED
!     0241  090190851                 DELIVERED
!     0242  090190852                 DELIVERED
      0243  090190854                 DELIVERED
      0244  090190862                 DELIVERED
      0245  090190865                 DELIVERED
!     0246  090190873                 DELIVERED
      0247  090190874                 DELIVERED
!     0248  090190875                 DELIVERED
      0249  090190884                 DELIVERED
      0250  090190886                 DELIVERED
      0251  090190890                 DELIVERED
      0252  090190893                 DELIVERED
      0253  090190913                 DELIVERED
      0254  090190915                 DELIVERED
      0255  090190925                 DELIVERED
      0256  090190935                 DELIVERED
      0257  090190937                 DELIVERED
      0258  090190938                 DELIVERED
      0259  090190947                 DELIVERED
      0260  090190949                 DELIVERED
      0261  090190951                 DELIVERED
      0262  090190959                 DELIVERED
      0263  090190967                 DELIVERED
      0264  090190979                 DELIVERED
      0265  090190982                 DELIVERED
      0266  090190985                 DELIVERED
      0267  090190993                 DELIVERED
      0268  090190998                 DELIVERED
      0269  090191002                 DELIVERED
      0270  090191007                 DELIVERED
      0271  090191011                 DELIVERED
      0272  090191013                 DELIVERED
!     0273  090191043                 DELIVERED
      0274  090191047                 DELIVERED
!     0275  090191062                 DELIVERED
!     0276  090191064                 DELIVERED
      0277  090191065                 DELIVERED
      0278  090191066                 DELIVERED
      0279  090191071                 DELIVERED
      0280  090191075                 DELIVERED
      0281  090191092                 DELIVERED
      0282  090191094                 DELIVERED
      0283  090191096                 DELIVERED
!     0284  090191097                 DELIVERED
      0285  090191100                 DELIVERED
      0286  090191101                 DELIVERED
      0287  090191102                 DELIVERED
      0288  090191103                 DELIVERED
      0289  090191107                 DELIVERED
      0290  090191108                 DELIVERED
      0291  090191111                 DELIVERED
      0292  090191124                 DELIVERED
      0293  090191130                 DELIVERED
      0294  090191135                 DELIVERED
      0295  090191159                 DELIVERED
      0296  090191161                 DELIVERED
      0297  090191162                 DELIVERED
      0298  090191165                 DELIVERED
!     0299  090191166                 DELIVERED
      0300  090191167                 DELIVERED
      0301  090191171                 DELIVERED
      0302  090191172                 DELIVERED
      0303  090191173                 DELIVERED
      0304  090191174                 DELIVERED
      0305  090191175                 DELIVERED
      0306  090191187                 DELIVERED
      0307  090191216                 DELIVERED
!     0308  090191219                 DELIVERED
      0309  090191258                 DELIVERED
!     0310  090191276                 DELIVERED
!     0311  090191280                 DELIVERED
!     0312  090191286                 DELIVERED
!     0313  090191288                 DELIVERED

NORMAL    : 270
RESEND    : 0
EMERGENCY : 43
FAILED    : 0
TOTAL FOR UTILKNOX: 313




LEGEND
-----------------------
  - NORMAL
* - RESEND
! - EMERGENCY


