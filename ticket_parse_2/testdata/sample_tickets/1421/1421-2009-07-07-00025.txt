

PUPS AUDIT
SCEG ELECTRIC 
SCEJZ40
AUDIT FOR 7/6/2009

FOR CODE SCEJZ40

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060056                DELIVERED
      0002  0907060067                DELIVERED
      0003  0907060083                DELIVERED
!     0004  0907060138                DELIVERED
!     0005  0907060140                DELIVERED
 U    0006  0907060180                DELIVERED
 U    0007  0907060182                DELIVERED
 U    0008  0907060189                DELIVERED
 U    0009  0907060192                DELIVERED
 U    0010  0907060199                DELIVERED
 U    0011  0907060201                DELIVERED
 U    0012  0907060208                DELIVERED
 U    0013  0907060265                DELIVERED
      0014  0907060300                DELIVERED
      0015  0907060313                DELIVERED
      0016  0907060315                DELIVERED
      0017  0907060337                DELIVERED
      0018  0907060408                DELIVERED
      0019  0907060449                DELIVERED
      0020  0907060464                DELIVERED
      0021  0907060491                DELIVERED
!     0022  0907060547                DELIVERED
      0023  0907060555                DELIVERED
!     0024  0907060560                DELIVERED
      0025  0907060588                DELIVERED
      0026  0907060646                DELIVERED
      0027  0907060648                DELIVERED
      0028  0907060670                DELIVERED
!     0029  0907060739                DELIVERED
      0030  0907060773                DELIVERED
      0031  0907060784                DELIVERED
      0032  0907060802                DELIVERED
      0033  0907060828                DELIVERED
      0034  0907060878                DELIVERED
!     0035  0907060971                DELIVERED
      0036  0907060991                DELIVERED
*     0037  0907061012                DELIVERED
      0038  0907061055                DELIVERED
      0039  0907061066                DELIVERED
      0040  0907061123                DELIVERED
      0041  0907061124                DELIVERED
      0042  0907061154                DELIVERED
      0043  0907061166                DELIVERED
      0044  0907061176                DELIVERED
      0045  0907061177                DELIVERED
      0046  0907061187                DELIVERED
      0047  0907061196                DELIVERED
      0048  0907061203                DELIVERED
      0049  0907061218                DELIVERED
      0050  0907061229                DELIVERED
!     0051  0907061298                DELIVERED
      0052  0907061308                DELIVERED
      0053  0907061321                DELIVERED
      0054  0907061324                DELIVERED
      0055  0907061327                DELIVERED
      0056  0907061338                DELIVERED
 M    0057  0907061351                DELIVERED
      0058  0907061352                DELIVERED
*     0059  0907061357                DELIVERED
      0060  0907061365                DELIVERED
 U    0061  0907061371                DELIVERED
      0062  0907061381                DELIVERED
      0063  0907061399                DELIVERED
      0064  0907061459                DELIVERED
      0065  0907061462                DELIVERED
      0066  0907061473                DELIVERED
      0067  0907061477                DELIVERED
      0068  0907061495                DELIVERED
      0069  0907061508                DELIVERED
      0070  0907061512                DELIVERED
      0071  0907061514                DELIVERED
      0072  0907061519                DELIVERED
      0073  0907061540                DELIVERED
      0074  0907061553                DELIVERED
      0075  0907061559                DELIVERED
      0076  0907061566                DELIVERED
      0077  0907061569                DELIVERED
      0078  0907061576                DELIVERED
      0079  0907061582                DELIVERED
      0080  0907061586                DELIVERED
      0081  0907061590                DELIVERED
      0082  0907061594                DELIVERED
      0083  0907061605                DELIVERED
      0084  0907061606                DELIVERED
      0085  0907061612                DELIVERED
      0086  0907061660                DELIVERED
      0087  0907061783                DELIVERED
      0088  0907061794                DELIVERED
      0089  0907061803                DELIVERED
      0090  0907061834                DELIVERED
      0091  0907061858                DELIVERED
      0092  0907061877                DELIVERED
!     0093  0907061967                DELIVERED
      0094  0907062156                DELIVERED
      0095  0907062159                DELIVERED
      0096  0907062171                DELIVERED
      0097  0907062184                DELIVERED
      0098  0907062290                DELIVERED
!     0099  0907062294                DELIVERED
!     0100  0907062333                DELIVERED
 C    0101  0907062495                DELIVERED
 C    0102  0907062501                DELIVERED
      0103  0907062514                DELIVERED
      0104  0907062522                DELIVERED
      0105  0907062531                DELIVERED
      0106  0907062540                DELIVERED
      0107  0907062542                DELIVERED
      0108  0907062547                DELIVERED
      0109  0907062559                DELIVERED
      0110  0907062571                DELIVERED
      0111  0907062577                DELIVERED
      0112  0907062584                DELIVERED
      0113  0907062597                DELIVERED
      0114  0907062691                DELIVERED
FOR CODE SCEJZ40


NORMAL    : 90
RESEND    : 2
EMERGENCY : 10
CANCEL    : 2
MEET      : 1
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 9
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEJZ40: 114




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

