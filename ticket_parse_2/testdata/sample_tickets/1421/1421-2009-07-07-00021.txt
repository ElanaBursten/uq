

PUPS AUDIT
SCEG ELECTRIC 
SCEDZ95
AUDIT FOR 7/6/2009

FOR CODE SCEDZ95

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060032                DELIVERED
      0002  0907060069                DELIVERED
      0003  0907060250                DELIVERED
      0004  0907060308                DELIVERED
*     0005  0907060333                DELIVERED
      0006  0907060343                DELIVERED
 C    0007  0907060353                DELIVERED
!     0008  0907060391                DELIVERED
 N    0009  0907060404                DELIVERED
      0010  0907060419                DELIVERED
 U    0011  0907060738                DELIVERED
      0012  0907061062                DELIVERED
      0013  0907061120                DELIVERED
      0014  0907061172                DELIVERED
      0015  0907061328                DELIVERED
      0016  0907061387                DELIVERED
      0017  0907061435                DELIVERED
      0018  0907061484                DELIVERED
      0019  0907062048                DELIVERED
 U    0020  0907062072                DELIVERED
 U    0021  0907062078                DELIVERED
 U    0022  0907062083                DELIVERED
 U    0023  0907062085                DELIVERED
 U    0024  0907062100                DELIVERED
 U    0025  0907062110                DELIVERED
 U    0026  0907062116                DELIVERED
 U    0027  0907062123                DELIVERED
 U    0028  0907062127                DELIVERED
 U    0029  0907062130                DELIVERED
 U    0030  0907062134                DELIVERED
 U    0031  0907062138                DELIVERED
 U    0032  0907062142                DELIVERED
 U    0033  0907062145                DELIVERED
      0034  0907062165                DELIVERED
      0035  0907062270                DELIVERED
 U    0036  0907062311                DELIVERED
      0037  0907062336                DELIVERED
*     0038  0907062472                DELIVERED
      0039  0907062645                DELIVERED
FOR CODE SCEDZ95


NORMAL    : 18
RESEND    : 2
EMERGENCY : 1
CANCEL    : 1
MEET      : 0
NO SHOW   : 1
SURVEY    : 0
UPDATE    : 16
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEDZ95: 39




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

