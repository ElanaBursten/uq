

PUPS AUDIT
SCEG ELECTRIC 
SCEDZ36
AUDIT FOR 7/6/2009

FOR CODE SCEDZ36

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060012                DELIVERED
      0002  0907060014                DELIVERED
      0003  0907060018                DELIVERED
      0004  0907060047                DELIVERED
!     0005  0907060077                DELIVERED
      0006  0907060212                DELIVERED
      0007  0907060306                DELIVERED
      0008  0907060323                DELIVERED
      0009  0907060436                DELIVERED
      0010  0907060603                DELIVERED
      0011  0907060660                DELIVERED
      0012  0907060679                DELIVERED
      0013  0907060686                DELIVERED
      0014  0907060744                DELIVERED
 U    0015  0907060831                DELIVERED
      0016  0907060911                DELIVERED
      0017  0907061058                DELIVERED
      0018  0907061125                DELIVERED
!     0019  0907061195                DELIVERED
      0020  0907061214                DELIVERED
      0021  0907061316                DELIVERED
      0022  0907061322                DELIVERED
*     0023  0907061392                DELIVERED
!     0024  0907061430                DELIVERED
      0025  0907061563                DELIVERED
 R    0026  0907061595                DELIVERED
 R    0027  0907061603                DELIVERED
      0028  0907062113                DELIVERED
      0029  0907062143                DELIVERED
      0030  0907062286                DELIVERED
      0031  0907062392                DELIVERED
      0032  0907062424                DELIVERED
      0033  0907062427                DELIVERED
      0034  0907062431                DELIVERED
      0035  0907062432                DELIVERED
      0036  0907062442                DELIVERED
      0037  0907062637                DELIVERED
FOR CODE SCEDZ36


NORMAL    : 30
RESEND    : 1
EMERGENCY : 3
CANCEL    : 0
MEET      : 0
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 1
REMARK    : 2
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEDZ36: 37




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

