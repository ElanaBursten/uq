

PUPS AUDIT
SCEG ELECTRIC 
SCEDZ81
AUDIT FOR 7/6/2009

FOR CODE SCEDZ81

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
*     0001  0907060717                DELIVERED
!     0002  0907061071                DELIVERED
      0003  0907062360                DELIVERED
FOR CODE SCEDZ81


NORMAL    : 1
RESEND    : 1
EMERGENCY : 1
CANCEL    : 0
MEET      : 0
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 0
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEDZ81: 3




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

