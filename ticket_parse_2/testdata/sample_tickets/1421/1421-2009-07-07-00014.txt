

PUPS AUDIT
SCEG ELECTRIC
SCETZ09
AUDIT FOR 7/6/2009

FOR CODE SCETZ09

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060048                DELIVERED
      0002  0907060051                DELIVERED
      0003  0907060057                DELIVERED
*     0004  0907060073                DELIVERED
      0005  0907060173                DELIVERED
      0006  0907060193                DELIVERED
      0007  0907060216                DELIVERED
!     0008  0907060227                DELIVERED
      0009  0907060405                DELIVERED
      0010  0907060561                DELIVERED
      0011  0907061108                DELIVERED
      0012  0907061121                DELIVERED
      0013  0907061210                DELIVERED
      0014  0907061222                DELIVERED
      0015  0907061232                DELIVERED
      0016  0907061248                DELIVERED
      0017  0907061260                DELIVERED
      0018  0907061272                DELIVERED
      0019  0907061601                DELIVERED
      0020  0907061634                DELIVERED
      0021  0907061661                DELIVERED
      0022  0907061666                DELIVERED
      0023  0907061673                DELIVERED
      0024  0907061689                DELIVERED
 C    0025  0907061693                DELIVERED
      0026  0907061695                DELIVERED
      0027  0907061707                DELIVERED
      0028  0907062027                DELIVERED
      0029  0907062064                DELIVERED
      0030  0907062167                DELIVERED
      0031  0907062185                DELIVERED
      0032  0907062201                DELIVERED
 M    0033  0907062466                DELIVERED
FOR CODE SCETZ09


NORMAL    : 29
RESEND    : 1
EMERGENCY : 1
CANCEL    : 1
MEET      : 1
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 0
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCETZ09: 33




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

