

PUPS AUDIT
SCEG ELECTRIC 
SCEDZ70
AUDIT FOR 7/6/2009

FOR CODE SCEDZ70

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907060009                DELIVERED
      0002  0907060066                DELIVERED
!     0003  0907060079                DELIVERED
      0004  0907060331                DELIVERED
      0005  0907060346                DELIVERED
      0006  0907060470                DELIVERED
      0007  0907060525                DELIVERED
      0008  0907060548                DELIVERED
      0009  0907060565                DELIVERED
      0010  0907060598                DELIVERED
      0011  0907060775                DELIVERED
      0012  0907060783                DELIVERED
      0013  0907060792                DELIVERED
      0014  0907060799                DELIVERED
      0015  0907060801                DELIVERED
      0016  0907060810                DELIVERED
      0017  0907060822                DELIVERED
      0018  0907060823                DELIVERED
*     0019  0907060841                DELIVERED
      0020  0907060988                DELIVERED
      0021  0907061069                DELIVERED
      0022  0907061077                DELIVERED
      0023  0907061122                DELIVERED
      0024  0907061358                DELIVERED
      0025  0907061429                DELIVERED
      0026  0907061438                DELIVERED
 M    0027  0907061452                DELIVERED
 U    0028  0907061465                DELIVERED
      0029  0907061471                DELIVERED
 U    0030  0907061479                DELIVERED
      0031  0907062004                DELIVERED
!     0032  0907062089                DELIVERED
      0033  0907062206                DELIVERED
      0034  0907062209                DELIVERED
      0035  0907062264                DELIVERED
      0036  0907062366                DELIVERED
FOR CODE SCEDZ70


NORMAL    : 30
RESEND    : 1
EMERGENCY : 2
CANCEL    : 0
MEET      : 1
NO SHOW   : 0
SURVEY    : 0
UPDATE    : 2
REMARK    : 0
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR SCEDZ70: 36




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

