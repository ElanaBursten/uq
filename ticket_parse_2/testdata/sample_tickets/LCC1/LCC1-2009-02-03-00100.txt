

Ticket No:  9022688               2 FULL BUSINESS DAYS 
Send To: CC7760     Seq No:   30  Map Ref:  

Transmit      Date:  2/03/09   Time: 10:31 AM    Op: orshan 
Original Call Date:  2/03/09   Time: 10:24 AM    Op: orshan 
Work to Begin Date:  2/06/09   Time: 12:00 AM 

State: WA            County: KING                    Place: KENT 
Address:    405      Street: NOVAK LANE 
Nearest Intersecting Street: 84TH AVE S 

Twp: 22N   Rng: 4E    Sect-Qtr: 13-SE-NE 
Twp: 22N   Rng: 5E    Sect-Qtr: 18-SW-NW 
Ex. Coord NW Lat: 47.3955032Lon: -122.2316402SE Lat: 47.3936459Lon: -122.2279038 

Type of Work: INSTALL SAMPLE STATION 
Location of Work: ADD IS APX 450 FT W FROM THE ABV INTERS ON THE N SIDE OF
: NOVAK LANE.MARK APX 20 FT WIDE AREA ON NOVAK LANE IN FRONT OF THE ABV
: ADD.ALSO MARK APX 15 FT INTO THE ADD PAST THE SHOULDER.AREA IS MARKED IN
: WHITE+CALLER STATES 84TH AVE IS AKA CENTRAL 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : CITY OF KENT WATER DEPARTMENT    Best Time:   
Contact Name: GAGE ELDER                       Phone: (253)856-5600 
Email Address:         
Alt. Contact: JASON ROLLER                     Phone:  
Contact Fax : (253)856-6600 
Work Being Done For: CITY OF KENT WATER DEPARTMENT 
Additional Members:  
ATT08      KCMTRO01   KENT01     PSEELC33   PSEGAS34   QLNWA16

