

Ticket No:  9022708               2 FULL BUSINESS DAYS 
Send To: CC7711     Seq No:   63  Map Ref:  

Transmit      Date:  2/03/09   Time: 10:33 AM    Op: orjust 
Original Call Date:  2/03/09   Time: 10:28 AM    Op: orjust 
Work to Begin Date:  2/06/09   Time: 12:00 AM 

State: WA            County: LEWIS                   Place: CENTRALIA 
Address:   2340      Street: HARRISON AVE 
Nearest Intersecting Street: HORSLEY 

Twp: 15N   Rng: 3W    Sect-Qtr: 36-SE-NE 
Twp: 15N   Rng: 2W    Sect-Qtr: 40-SW-NW 
Ex. Coord NW Lat: 46.7431367Lon: -122.9968505SE Lat: 46.7392501Lon: -122.9927966 

Type of Work: INSTALL IRRIGATION LINES 
Location of Work: ADD IS APX 100FT S OF ABV INTER, ON E SIDE OF HARRISON.
: MARK ENTIRE PROPERTY INCLUDING ALL ROWS AND EASMENTS AT ABV ADD. AREA WILL
: BE MARKED IN WHITE PAINT. 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : INSIGNA ENTERPISES               Best Time:   
Contact Name: LESTER AMELL                     Phone: (360)219-7409 
Email Address:         
Alt. Contact: LAND LINE -                      Phone: (360)736-7357 
Contact Fax :  
Work Being Done For: DISCOVERY TOURS 
Additional Members:  
CENTRA01   LACOM01    LCDPW01    LCPUD02    PSEGAS42   QLNWA20

