
Ticket No:  9019128               48 HOUR NOTICE 
Send To: PGE01      Seq No:  102  Map Ref:  

Transmit      Date:  2/03/09   Time: 10:39 AM    Op: orjean 
Original Call Date:  2/03/09   Time: 10:35 AM    Op: webusr 
Work to Begin Date:  2/05/09   Time: 10:45 AM 

State: OR            County: MULTNOMAH               Place: GRESHAM 
Address:   3395      Street: SW 25TH CT 
Nearest Intersecting Street: SW PLEASANT VIEW DR 

Twp: 1S    Rng: 3E    Sect-Qtr: 17-SW-SE 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 45.4792248Lon: -122.4676871SE Lat: 45.4783241Lon: -122.4649031 

Type of Work: INSTALL CATV SERVICE 
Location of Work: ADDRESS IS EAST OF THE INTERSECTION, ON THE NORTH SIDE OF
: SW 25TH CT. MARK A 20FT RADIUS, FOLLOWING THE TEMP LINE FROM THE PED/POLE TO
: THE NORTH EAST CORNER OF THE HOUSE. THE PED/POLE IS LOCATED NORTH OF THE
: HOUSE, ON SW 24TH TER. 

Remarks: CALLER GAVE THOMAS GUIDE PAGE 628 H5 
:  

Company     : FISK COMMUNICATION CONTRACTING   Best Time:   
Contact Name: CAROL LEGGETT                    Phone: (360)314-4454 
Email Address:  FCCLOCATES@COMCAST.NET 
Alt. Contact: MARK FISK                        Phone: (360)314-4454 
Contact Fax :  
Work Being Done For: COMCAST 
Additional Members:  
CMCST01    GRESH01    GTC04      NNG01      NWN01 

