
Ticket No:  9128746               48 HOUR NOTICE 
Send To: FALCON02   Seq No:   32  Map Ref:  

Transmit      Date:  6/29/09   Time:  2:19 PM    Op: orjudy 
Original Call Date:  6/29/09   Time:  2:15 PM    Op: orjudy 
Work to Begin Date:  7/01/09   Time:  2:30 PM 

State: OR            County: MARION                  Place: WOODBURN 
Address:             Street: MILL ST 
Nearest Intersecting Street: FRONT ST 

Twp: 5S    Rng: 1W    Sect-Qtr: 18-NE,7-SE 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 45.145885 Lon: -122.8539395SE Lat: 45.1442898Lon: -122.8518378 

Type of Work: REPAIR RR TRACK WIRES 
Location of Work: SITE IS APX 50FT E OF ABV INTER AT THE RR TRACKS.  MARK
: FROM MILL ST N APX 100FT AND S APX 300FT ALONG E SIDE OF RR TRACKS AS MARKED
: IN WHITE. 

Remarks:  
:  

Company     : UNION PACIFIC RAILROAD           Best Time:   
Contact Name: MIKE PROWELL                     Phone: (541)341-5786 
Email Address:   
Alt. Contact: MIKE CELL - CALL 1ST             Phone: (541)953-0178 
Contact Fax :  
Work Being Done For: UNION PACIFIC RAILROAD 
Additional Members:  
GERTEL01   L3C01      NWN01      PGE04      QLNOR14    QWEST02    WILBRB02 
 WOOD01 
