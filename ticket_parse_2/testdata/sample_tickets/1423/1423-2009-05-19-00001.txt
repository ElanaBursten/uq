

CPLZ05 3 PUPS Voice 05/18/2009 10:08:44 AM 0905121374 Emergency

Ticket Number: 0905121374
Old Ticket Number: 
Created By: AMS
Seq Number: 3

Created Date: 05/12/2009 11:53:20 AM
Work Date/Time: 05/12/2009 12:00:00 PM
Update: 05/29/2009 Good Through: 06/03/2009

Excavation Information:
State: SC     County: FLORENCE
Place: FLORENCE
Address Number: 
Street: WARLEY ST
Inters St: SPRUCE ST & EVANS ST
Subd: 

Type of Work: GAS, REPAIR SERVICE
Duration: 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: SCE&G

Remarks/Instructions: STARTING @ THE CORNER OF WARLEY ST & SPRUCE ST LOCATE   
ALONG WARLEY ST UP TO ADDRESS 657 WARLEY ST ON THE SAME ADDRESS SIDE OF THE   
ROAD                                                                          
CREW IS ON SITE & WORK IS IN PROGRESS  //  ANY QUESTIONS OR PROBLEMS          
CONTACT LUCY NANCE                                                            
**EMERGENCY DUE TO:  GAS, REPAIR SERVICE  //  CALLER STATES THAT GAS LINE     
HAS BEEN CUT  //  THIS IS A DANGER TO LIFE, HEALTH, & PROPERTY**              

Caller Information: 
Name: LUCY NANCE                            SCE&G                                 
Address: 1812 N IRBY ST
City: FLORENCE State: SC Zip: 29501
Phone: (843) 676-3613 Ext:  Type: Business
Fax: (843) 676-3675 Caller Email: LNANCY@SCANA.COM

Contact Information:
Contact:LUCY NANCE Email: LNANCY@SCANA.COM
Call Back: Fax: (843) 676-3675

Grids: 
Lat/Long: 34.1873230221574, -79.7741340640363
Secondary: 34.1867817216298, -79.7740672802049
Lat/Long Caller Supplied: N 

Members Involved: BSZU45 CPLZ05 SCEGT01 SCG73 TWFZ66                          


Map Link: (NEEDS DEVELOPMENT)
