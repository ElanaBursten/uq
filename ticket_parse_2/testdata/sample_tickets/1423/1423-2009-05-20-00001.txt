

This is your End of Day Audit from IRTHNet for items sent
since 5/15/2009 7:00 AM (Eastern Standard Time)

Seq   Ticket                    Registration Code                  
-------------------------------------------------------------------
1     0905150031                CPLZ05 (PUPS)                      
1     0905121374                CPLZ05 (PUPS)                      
2     0905140764                CPLZ05 (PUPS)                      
2     0905180050                CPLZ05 (PUPS)                      
3     0905121374                CPLZ05 (PUPS)                      
3     0905120158                CPLZ05 (PUPS)                      
4     0905180050                CPLZ05 (PUPS)                      
4     0905121239                CPLZ05 (PUPS)                      
5     0905180050                CPLZ05 (PUPS)                      
5     0905131587                CPLZ05 (PUPS)                      
6     0905141200                CPLZ05 (PUPS)                      
6     0905110322                CPLZ05 (PUPS)                      
7     0905120158                CPLZ05 (PUPS)                      
7     A091270725                TST03 (NCOC)                       
8     0905150391                CPLZ05 (PUPS)                      
8     A091192096                TST03 (NCOC)                       
9     0905110322                CPLZ05 (PUPS)                      
9     A091270841                TST03 (NCOC)                       
10    0905121239                CPLZ05 (PUPS)                      
10    A091201478                TST03 (NCOC)                       
11    0905140356                CPLZ05 (PUPS)                      
11    A091242075                TST03 (NCOC)                       
12    0905131587                CPLZ05 (PUPS)                      
12    C091260252                TST03 (NCOC)                       

Total Items Sent: 24
