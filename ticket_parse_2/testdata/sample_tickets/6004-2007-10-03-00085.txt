

SEQUENCE NUMBER 0063   CDC = 2FW
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ATMOS-MT-HP-STS  CROSSTEX ENERGY  EnCANA OIL/GAS   ONCOR ELEC-STS 
ATMOS-MIDTX-STS  ONCOR ELEC-STS   VERIZ-KG- METRO  BROADWING      
CHARTER COMM     ATMOS-MT-HP-STS  SM&P-F           TR CNTY NE/1SRC
BARNETT GATH     LONE STAR XCHG

Locate Request No. 072760124

Prepared By MELODY C             On 03-OCT-07  At 0656

MapRef : HP                         Grid: 325730097143A  Footprint: D08

Location:     County: TARRANT  Town: FORT WORTH

             Address: 0 WESTPORT PKY 

Beginning Work Date 10/05/07 Time of Day: 07:00 am   Duration: 07 DAYS 

Fax-A-Locate Date          at 

Excavation Type : POLE/SIGN INSTALLATION        
Nature of Work  : FOUNDATION/LIGHT POLE BASES   

Blasting ? NO           48 Hr Notice ? YES  
White Line ? YES        Digging Deeper Than 16 Inches ? YES    

Person Calling : WILL WHTTEN
Company Name   : AUGER DRILLING
Work by AUGER DRILLING      For POTTER CONCRETE

Person to Contact : WILL WHTTEN

Phone No.  ( 214 )707-2708 /( 214 )707-2833 ( Hours: 07:00 am/04:00 pm )
Fax No.    ( 972 )815-2956
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: HIGHWAY 377 
WORK WILL BE DONE ON THE  SW CORNER OF THE INTER AND LOC ON THE ENTIRE
 PROP GOING 1000' IN EACH DIRECTION.  AREA IS MARKED W/ WHITE FLAGS.  
MAPSCO 9,T.                                                           
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 9,T                          

FaxBack Requested ? YES    Lone Star Xref: 


072760124 to EMAIL ADDRESS at 06:57:48 on WED, 10/03/07 for 2FW #0063
