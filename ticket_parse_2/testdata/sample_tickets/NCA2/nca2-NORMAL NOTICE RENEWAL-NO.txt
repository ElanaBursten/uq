
                                                                                     
 PBTMDO 00002 USAN 06/17/09 06:25:11 0178368 NORMAL NOTICE RENEWAL                   
                                                                                     
 Message Number: 0178368 Received by USAN at 06:24 on 06/17/09 by PGS                
                                                                                     
 Work Begins:    06/19/09 at 07:00   Notice: 020 hrs      Priority: 2                
                                                                                     
 Expires: 07/15/09 at 17:00   Update By: 07/13/09 at 16:59                           
                                                                                     
 Caller:         BRAD SILVA                                                          
 Company:        LAYMAN ELECTRIC                                                     
 Address:        1628 CULPEPPER AVE STE"A", MODESTO                                  
 City:           MODESTO                       State: CA Zip: 95351                  
 Business Tel:   209-522-2244                  Fax: 209-522-3259                     
 Email Address:  BSILVA@LAYMANELECTRIC.COM                                           
                                                                                     
 Nature of Work: TR TO INST ELEC CON                                                 
 Done for:       CALIFORNIA BEAUTY COLLEGE     Explosives: N                         
 Foreman:        SARKIS                                                              
 Field Tel:                                    Cell Tel: 209-596-1083                
 Area Premarked: Y   Premark Method: WHITE PAINT                                     
 Permit Type:    NO                                                                  
 Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N        
 Excavation Enters Into Street Or Sidewalk Area: N                                   
                                                                                     
 Location:                                                                           
 Street Address:         1115 15TH ST                                                
   Cross Street:         NEEDHAM AVE                                                 
     WRK IN THE PARKING LOT AREA IN BK OF ADDR(EXT APP 200' INTO PROP)               
                                                                                     
 Place: MODESTO                      County: STANISLAUS           State: CA          
                                                                                     
 Long/Lat Long: -120.998435 Lat:  37.644236 Long: -120.995623 Lat:  37.646528        
                                                                                     
 Excavator Requests Operator(s) To Re-mark Their Facilities: N                       
 Comments:                                                                           
 RENEWAL OF TICKET RN#141390 ORIG DATE 06/05/2009 RE-MARK NO-PGS                     
 06/17/2009                                                                          
                                                                                     
                                                                                     
 Sent to:                                                                            
 ATTCAL = AT&T TRANSMISSION CAL        COMMOD = COMCAST-MODESTO                      
 COSTAN = COUNTY STANISLAUS            CTYMDO = CITY MODESTO          **             
 FBCHUR = FIRST BAPTIST CHURCH         LEVCAL = LEVEL 3 COMM - CALIF                 
 METTRA = MODESTO&EMPIRE TRACT         MODIRR = MODESTO IRRIG                        
 PBTMDO = PACIFIC BELL MODESTO         PGEMDO = PGE DISTR MODESTO                    
 TWABF2 = TIME WARNER BFD 2            TWABFD = TIME WARNER TELECOM BFD              
                                                                                     
                                                                                     
 At&t Ticket Id: 21839971     clli code:   MDSTCA02