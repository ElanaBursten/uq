UTIL13 00249 VUPSb 03/03/08 10:34:13 B805301636-01B          3 HOUR

Ticket No:  B805301636-01B                        3HRS GRID RUSH LREQ
Transmit        Date: 03/03/08   Time: 10:34 AM   Op: 1FMC
Call            Date: 03/03/08   Time: 10:32 AM
Due By          Date: 03/03/08   Time: 01:32 PM
Update By       Date: 03/12/08   Time: 11:59 PM
Expires         Date: 03/17/08   Time: 07:00 AM
Old Tkt No: B805301636
Original Call   Date: 02/22/08   Time: 05:36 PM   Op: 1KMD

City/Co:FAIRFAX               Place:ANNANDALE                           State:VA
Address:     3353-3359        Street: TALEEN CT
Cross 1:     NICOLE CT

Type of Work:   COMMUNICATIONS CABLE - REPAIR OR REPLACE
Work Done For:  COX COMMUNICATIONS
Excavation area:NEED THE ENTIRE FRONT OF PROPERTIES OF ABOVE ADDRESS TO INCLUDE
                TO THE STREET

                ADDRESS IS ADJACENT
Instructions:   3 HOUR NOTICE - THESE MEMBER UTILITIES DID NOT RESPOND: DOMINION
                VA POWER, VERIZON AND WASHINGTON GAS. ALSO, CABLE, WATER AND
                SEWER NO MARKS PRESENT
                CALLER MAP REF: 14J9

Whitelined: N   Blasting: N   Boring: N

Company:        L & B SOLUTIONS INC                       Type: CONT
Co. Address:    40 MARJORIE LN  First Time: N
City:           STAFFORD  State:VA  Zip:22556
Company Phone:  540-379-8340
Contact Name:   BUSTER WASHINGTON           Contact Phone:540-379-8340
Email:          lbsolutionsinc@aol.com
Field Contact:  BUSTER WASHINGTON
Fld. Contact Phone:540-379-8340

Mapbook:  14J9
Grids:    3851C7714C-32  3851C7714C-33  3851C7714C-42  3851C7714C-43
Grids:    3851D7714C-02  3851D7714C-03

Members:
COX902 = COX COMMUNICATIONS (COX)       DOM400 = DOMINION VIRGINIA POWER (DOM)
FCU901 = FAIRFAX COUNTY DPW SEWER (FCU) FCW902 = FAIRFAX WATER (FCW)
VZN906 = VERIZON (VZN)                  WGL904 = WASHINGTON GAS (WGL)

Seq No:   249 B


