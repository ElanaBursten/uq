WGL904 00032 VUPSb 02/27/08 07:00:36 B805301636-00B          NO SHOW

Ticket No:  B805301636-00B                         LATE GRID NORM LREQ
Transmit        Date: 02/27/08   Time: 07:00 AM   Op: 1KMD
Call            Date: 02/22/08   Time: 05:34 PM
Requested By    Date: 02/27/08   Time: 07:00 AM
Update By       Date: 03/12/08   Time: 11:59 PM
Due By          Date: 02/27/08   Time: 07:00 AM
Expires         Date: 03/17/08   Time: 07:00 AM
Old Tkt No: B805301636
Original Call   Date: 02/22/08   Time: 05:34 PM   Op: 1KMD

City/Co:FAIRFAX               Place:ANNANDALE                           State:VA
Address:     3353-3359        Street: TALEEN CT
Cross 1:     NICOLE CT                                         Intersection: N

Type of Work:   COMMUNICATIONS CABLE - REPAIR OR REPLACE
Work Done For:  COX COMMUNICATIONS
Location:       NEED THE ENTIRE FRONT OF PROPERTIES OF ABOVE ADDRESS TO INCLUDE
                TO THE STREET

                ADDRESS IS ADJACENT
Instructions:   CALLER MAP REF: 14J9

Whitelined: N   Blasting: N   Boring: N

Company:        L & B SOLUTIONS INC                           Type: CONT
Co. Address:    40 MARJORIE LN
City:           STAFFORD  State:VA  Zip:22556
Company Phone:  540-379-8340
Contact Name:   BUSTER WASHINGTON           Contact Phone:540-379-8340
Call Back Time:
Email:          lbsolutionsinc@aol.com

Mapbook:  14J9
Grids:    3851C7714C-32  3851C7714C-33  3851C7714C-42  3851C7714C-43
Grids:    3851D7714C-02  3851D7714C-03

Members:
COX902 = COX COMMUNICATIONS           DOM400 = DOMINION VIRGINIA POWER
FCU901 = FAIRFAX COUNTY DPW SEWER     FCW902 = FAIRFAX WATER
VZN906 = VERIZON                      WGL904 = WASHINGTON GAS

Seq No:   32 B


