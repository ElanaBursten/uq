

WGL904 00176 VUPSa 04/25/08 12:09:23 B811202153-01A          3 HOUR

Ticket No:  B811202153-01A                        3HRS GRID RUSH LREQ
Transmit        Date: 04/25/08   Time: 12:09 PM   Op: 1JZA
Call            Date: 04/25/08   Time: 12:08 PM
Due By          Date: 04/25/08   Time: 03:08 PM
Update By       Date: 05/08/08   Time: 11:59 PM
Expires         Date: 05/13/08   Time: 07:00 AM
Old Tkt No: B811202153
Original Call   Date: 04/21/08   Time: 02:23 PM   Op: WAKHAN

City/Co:FAIRFAX               Place:                                    State:VA
Address:     5429-5501        Street: CLONMEL CT
Cross 1:     GREENDALE VILLAGE DR

Type of Work:   CABLE TV SERVICE DROP- INSTALL
Work Done For:  COX COMMUNICATIONS
Excavation area:MARK FROM PED FRONT LEFT OF 5429 ACROSS GREENDALE VILLAGE DR
                ENTIRE PROPERTY OF 5501 INCLUDE ANY DRIVEWAYS, SIDEWALKS, AND
                ROADS  - WILL BORE IF REQUIRED
Instructions:   3 HOUR NOTICE - THE CALLER DISPUTES THE RESPONSE POSTED BY THESE
                UTILITIES: WASHINGTON GAS, DOMINION VIRGINIA POWER, VERIZON.
                CODE 60. CALLER STATES NO AGREEMENT WAS MADE
                CALLER MAP REF: NONE

Permit: 947-88145
Whitelined: N   Blasting: N   Boring: Y

Company:        W C C CABLE INC                           Type: CONT
Co. Address:    4809 EWEL RD  First Time: N
City:           FREDERICKSBURG  State:VA  Zip:22408
Company Phone:  540-898-9315
Contact Name:   APRIL KHAN                  Contact Phone:540-898-9315
Email:          akhan@wcccable.com
Field Contact:  APRIL KHAN
Fld. Contact Phone:540-898-9315

Mapbook:  23F11
Grids:    3846C7707A     3846C7707B

Members:
DOM400 = DOMINION VIRGINIA POWER (DOM)  VZN906 = VERIZON (VZN)
WGL904 = WASHINGTON GAS (WGL)

Seq No:   176 A

