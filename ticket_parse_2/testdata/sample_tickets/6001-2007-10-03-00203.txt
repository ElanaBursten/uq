SEQUENCE NUMBER 0116   CDC = FN2
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ATMOS-MT-HP-STS  ONCOR ELEC-STS   ATMOS-MIDTX-STS  VERIZ-KG- METRO
CHARTER COMM     ATMOS-MT-HP-STS  QWEST COMM.      TR CNTY NE/1SRC  
LONE STAR XCHG

Locate Request No. 072760949

Prepared By BEVERLY M            On 03-OCT-07  At 0902

MapRef :                            Grid: 325530097133A  Footprint: D12

Location:     County: TARRANT  Town: KELLER

             Address: 709 BLUEBONNET DR 

Beginning Work Date 10/05/07 Time of Day: 09:00 am   Duration: 02 DAYS 

Fax-A-Locate Date          at 

Excavation Type : CULVERT/DRAINS                
Nature of Work  : INSTL DRAIN SYS               

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : DAVE CHRISTIANSON
Company Name   : PRECISION LANDSCAPING
Work by PRECISION LANDSCAPING      For HOME OWNER

Person to Contact : DAVE CHRISTIANSON

Phone No.  ( 817 )430-2095 /( 817 )680-2094 ( Hours: 08:00 am/05:00 pm )
Fax No.    ( 817 )430-2652
Email:     CALLER COULD NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: PATE ORR N
MAP: 23,L - WORKING ON ENTIRE PROPERTY - LOCATE ENTIRE PROP - THIS IS 
A DUPLEX, ONLY WORKING @ 709 BLUEBONNET                               
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CALLER COULD NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 23,M                         

FaxBack Requested ? YES    Lone Star Xref: 


072760949 to EMAIL ADDRESS at 09:08:13 on WED, 10/03/07 for FN2 #0116