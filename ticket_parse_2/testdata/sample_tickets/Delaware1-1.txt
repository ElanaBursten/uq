
NOTICE OF INTENT TO EXCAVATE      EMERGENCY
Ticket No: 01069281
Transmit      Date: 02/20/03      Time: 10:00    Op: joann.
Original Call Date: 02/20/03      Time: 09:42    Op: joann.
Work to Begin Date: 02/20/03      Time: 12PM

Place: SMYRNA
Address: 241         Street: BEAR SWAMP RD
Nearest Intersecting Street: RAYMOND NECK RD

Type of Work: REPLACING WATER WELLL
Extent of Work:   LOC ENTIRE PROP
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
: EMERGENCY ** CREW ETA 12:00PM                          Fax: (302)378-2931

Company     : MIDDLETOWN WELL DRILLING
Contact Name: CONNIE FIOCCO         Contact Phone: (302)378-9396    Ext.:
Alt. Contact: DAVE BORRELL CELL     Alt. Phone   : (609)517-3327
Work Being Done For: BEN BURROWS 3RD
State: DE              County: KENT
Map: KENTD Page: 006   Grid Cells: J12
Explosives: N
DECO26
Send To: KCBA  01  Seq No: 0005   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01069293
Transmit      Date: 02/20/03      Time: 10:00    Op: l.boot
Original Call Date: 02/20/03      Time: 09:46    Op: l.boot
Work to Begin Date: 02/24/03      Time: 10AM

Place:
Address:             Street: ORCHARD LA
Nearest Intersecting Street: ELKS LODGE RD

Type of Work: BURY CATV MAIN
Extent of Work:   LOC FROM ABOVE INTER ENTIRE ST BOTH SIDES-BEST INFO GIVEN
Remarks: CALLER COULD NOT PROVIDE CITY-TKT TAKEN PER CALLER
:                                                        Fax: (302)628-3661

Company     : EASTERN TECH COMMUNICATION
Contact Name: WAYNE HILL            Contact Phone: (302)628-3245    Ext.:
Alt. Contact:                       Alt. Phone   : (804)276-0883
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 005   Grid Cells: G06
Explosives: N
CIMI35     CUDE01     DPML12     STOR01     XETCI
Send To: DSTS  45  Seq No: 0012   Map Ref:



NOTICE OF INTENT TO EXCAVATE      INSUFFICIENT NOTICE
Ticket No: 01069255
Transmit      Date: 02/20/03      Time: 09:38    Op: terrya
Original Call Date: 02/20/03      Time: 09:23    Op: terrya
Work to Begin Date: 02/20/03      Time: 11AM

Place: SELBYVILLE
Address: 28          Street: SANDOLLAR LA
Nearest Intersecting Street: SALTY WAY DR

Type of Work: INST A FENCE
Extent of Work:   LOC ENTIRE PROP AT ABOVE ADDRESS
Remarks: INSUFFICIENT NOTICE
: TKT TAKEN PER CALLER                                   Fax: (302)436-9525

Company     : EASTERN SHORE PORCH & PATIO
Contact Name: MARGO TESTERMAN       Contact Phone: (302)436-9520    Ext.:
Alt. Contact: BILL SPELLMAN         Alt. Phone   :
Work Being Done For: CRANE
State: DE              County: SUSSEX
Map: SUSSX Page: 024   Grid Cells: C10
Explosives: N
ARTW34     DECO26     SCED01     SDHY24     UNCA29
Send To: DSTS  45  Seq No: 0008   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01069134
Transmit      Date: 02/20/03      Time: 09:38    Op: janice
Original Call Date: 02/20/03      Time: 08:55    Op: janice
Work to Begin Date: 02/24/03      Time: 09AM

Place: MILLSBORO
Address: 118         Street: E DUPONT HWY
Nearest Intersecting Street: OLD LANDING RD

Type of Work: SWR & WTR SERV HOOK UP
Extent of Work:   LOC ENTIRE PROP
Remarks: CALLER REQUESTS CALLBACKS FROM ALL UTILITIES
:                                                        Fax: (302)645-1819

Company     : GEORGE SHERMAN CORP
Contact Name: LAYTON DRAYTON        Contact Phone: (302)684-4545    Ext.:
Alt. Contact:                       Alt. Phone   :
Work Being Done For: LYON CONKLIN
State: DE              County: SUSSEX
Map: SUSSX Page: 016   Grid Cells: G11
Explosives: N
DPML12     SDHY24     SISX44
Send To: DSTS  45  Seq No: 0009   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01069149
Transmit      Date: 02/20/03      Time: 09:38    Op: janice
Original Call Date: 02/20/03      Time: 08:57    Op: janice
Work to Begin Date: 02/24/03      Time: 09AM

Place: GEORGETOWN
Address:             Street: PAR CT
Nearest Intersecting Street: FAIRWAY AV

Type of Work: WTR SERV CONNECTION
Extent of Work:   LOT 26    LOC ENTIRE PROP    SUBD-COUNTRY CLUB VILLAGE
Remarks: CALLER REQUESTS CALLBACKS FROM ALL UTILITIES
:                                                        Fax: (302)645-1819

Company     : GEORGE SHERMAN CORP
Contact Name: LAYTON DRAYTON        Contact Phone: (302)684-4545    Ext.:
Alt. Contact:                       Alt. Phone   :
Work Being Done For: MARK HUDSON
State: DE              County: SUSSEX
Map: SUSSX Page: 009   Grid Cells: J12
Explosives: N
DPML12     STOR01     TOG01
Send To: DSTS  45  Seq No: 0010   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01069210
Transmit      Date: 02/20/03      Time: 09:38    Op: nicole
Original Call Date: 02/20/03      Time: 09:07    Op: nicole
Work to Begin Date: 02/24/03      Time: 09AM

Place: BEAR
Address: 155         Street: HONORA DR
Nearest Intersecting Street: VISCAYA DR

Type of Work: BURYING INVISIBLE FENCE
Extent of Work:   LOC ENTIRE PROP AT ABOVE ADDRESS  -SUBD:CARAVEL WOODS
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (302)378-7045

Company     : INVISIBLE FENCE OF CENTRAL DE
Contact Name: MARY LOUIE            Contact Phone: (302)378-3140    Ext.:
Alt. Contact: ROBERT LOUIE          Alt. Phone   :
Work Being Done For: JOHN & VIRGINIA HYSOCK
State: DE              County: NEW CASTLE
Map: NEWC  Page: 017   Grid Cells: K01
Map: NEWC  Page: 018   Grid Cells: A01
Explosives: N
ARTW34     DPU104     DPUG07     HRCA20
Send To: TNC   01  Seq No: 0015   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01069221
Transmit      Date: 02/20/03      Time: 09:38    Op: nicole
Original Call Date: 02/20/03      Time: 09:12    Op: nicole
Work to Begin Date: 02/24/03      Time: 09AM

Place: WILMINGTON
Address: 3004        Street: RIDGE VALE RD
Nearest Intersecting Street: CRESTLINE RD

Type of Work: BURYING INVISIBLE FENCE
Extent of Work:   LOC ENTIRE PROP AT ABOVE ADDRESS  -SUBD:HILLS OF SKYLINE
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (302)378-7045

Company     : INVISIBLE FENCE OF CENTRAL DE
Contact Name: MARY LOUIE            Contact Phone: (302)378-3140    Ext.:
Alt. Contact: ROBERT LOUIE          Alt. Phone   :
Work Being Done For: KARL LEHMAN
State: DE              County: NEW CASTLE
Map: NEWC  Page: 006   Grid Cells: E13
Explosives: N
ARTW34     DPU104     DPUG07     HRCA20
Send To: TNC   01  Seq No: 0016   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01069249
Transmit      Date: 02/20/03      Time: 09:38    Op: j.mess
Original Call Date: 02/20/03      Time: 09:20    Op: j.mess
Work to Begin Date: 02/24/03      Time: 09AM

Place: OCEAN VIEW
Address:             Street: CAROLINA RD
Nearest Intersecting Street: PARKER HOUSE RD

Type of Work: REPLACE CULVERT PIPE N/E
Extent of Work:   LOC ENTRANCE AT THE INTER  AREA IS MARKED AND STAKED
Remarks: BEST INFORMATION CALLER COULD PROVIDE
:                                                        Fax: (302)539-5814

Company     : PAULS TREE SERVICE INC
Contact Name: ANITA JUSTICE         Contact Phone: (302)539-9123    Ext.:
Alt. Contact: GAIL                  Alt. Phone   :
Work Being Done For: PLANTATION PARK
State: DE              County: SUSSEX
Map: SUSSX Page: 024   Grid Cells: C05
Explosives: N
DECO26     DPML12     SCED01     SE02       SISX44     TIDE02
Send To: DSTS  45  Seq No: 0011   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01069272
Transmit      Date: 02/20/03      Time: 09:38    Op: l.boot
Original Call Date: 02/20/03      Time: 09:34    Op: l.boot
Work to Begin Date: 02/24/03      Time: 09AM

Place: DOVER
Address: 3010        Street: JUDITH LA
Nearest Intersecting Street: LOCKWOOD CHAPEL RD

Type of Work: BURY CATV MAIN
Extent of Work:   LOC ENTIRE PROP OF 3010 JUDITH LA
Remarks:
:                                                        Fax: (302)628-3661

Company     : EASTERN TECH COMMUNICATION
Contact Name: WAYNE HILL            Contact Phone: (302)628-3245    Ext.:
Alt. Contact:                       Alt. Phone   : (804)276-0883
Work Being Done For: COMCAST
State: DE              County: KENT
Map: KENTD Page: 015   Grid Cells: E01
Explosives: N
DECO26     XETCI
Send To: KCBA  01  Seq No: 0003   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01069276
Transmit      Date: 02/20/03      Time: 09:38    Op: l.boot
Original Call Date: 02/20/03      Time: 09:36    Op: l.boot
Work to Begin Date: 02/24/03      Time: 09AM

Place: CAMDEN
Address:             Street: SHARP LA
Nearest Intersecting Street: PAINTERS WY

Type of Work: BURY CATV MAIN
Extent of Work:   LOC FROM ABOVE INTER ENTIRE ST BOTH SIDES OF ST
Remarks:
:                                                        Fax: (302)628-3661

Company     : EASTERN TECH COMMUNICATION
Contact Name: WAYNE HILL            Contact Phone: (302)628-3245    Ext.:
Alt. Contact:                       Alt. Phone   : (804)276-0883
Work Being Done For: COMCAST
State: DE              County: KENT
Map: KENTD Page: 023   Grid Cells: A02
Explosives: N
ATT32      CUDE14     CWSW01     DPML12     XETCI
Send To: KCBA  01  Seq No: 0004   Map Ref:




K

NOTICE OF INTENT TO EXCAVATE      EMERGENCY
Ticket No: 01068173
Transmit      Date: 02/19/03      Time: 11:23    Op: j.mess
Original Call Date: 02/19/03      Time: 11:12    Op: j.mess
Work to Begin Date: 02/19/03      Time: 11AM

Place: MILTON
Address: 307         Street: BROADKILN RD
Nearest Intersecting Street: PALMER ST

Type of Work: REPAIR SWR SVC
Extent of Work:   LOC REAR OF PROP
Remarks: EMERGENCY ** CREW ON SITE
: LEGALS NOT PROVIDED FOR THIS LOCATE REQUEST

Company     : JOHN COLLINS
Contact Name: HOMEOWNER             Contact Phone: (302)684-0886    Ext.:
Alt. Contact:                       Alt. Phone   :
Work Being Done For: JOHN COLLINS
State: DE              County: SUSSEX
Map: SUSSX Page: 010   Grid Cells: E04
Explosives: N
DPML12     SDHY24     STOR01     TOM01
Send To: DSTS  45  Seq No: 0041   Map Ref:



NOTICE OF INTENT TO EXCAVATE                                    CORRECTION
Ticket No: 01065012
Transmit      Date: 02/19/03      Time: 11:23    Op: diane
Original Call Date: 02/13/03      Time: 12:01    Op: diane
Work to Begin Date: 02/18/03      Time: 12PM

Place: WILMINGTON
Address: 1700        Street: N.RODNEY ST
Nearest Intersecting Street: SHALLCROSS AVE

Type of Work: REPAIR WATER SERVICE...NON EMERGENCY
Extent of Work:   WATER OFF SHALLCROSS AVE SIDE OF PROPERTY MARKED IN
: WHITE. ATTEN ALL UTILS PLS LOC ASAP NO MARKS JAJ 2/19 2/19 10:09AM
Remarks:
:                                                        Fax: (302)995-2945

Company     : WORTHY CONSTRUCTION
Contact Name: BOB MAZZONI           Contact Phone: (302)892-2702    Ext.:
Alt. Contact: SAME                  Alt. Phone   : (302)892-2704
Work Being Done For: CITY OF WILMINGTON
State: DE              County: NEW CASTLE
Map: NEWC  Page: 008   Grid Cells: E07
Explosives: N
DPU104     DPUG07     ETC01      HRCA20     MCIC40     MFN05      SDHY23
 SDHY25     TDCW01     WIM01
Send To: TNC   01  Seq No: 0012   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01067925
Transmit      Date: 02/19/03      Time: 11:23    Op: j.thom
Original Call Date: 02/19/03      Time: 09:31    Op: j.thom
Work to Begin Date: 02/21/03      Time: 09AM

Place: MIDDLETOWN
Address: 141         Street: BAKERFIELD DR
Nearest Intersecting Street: LANDVIEW CT

Type of Work: INST UG ELEC SVC
Extent of Work:   LOC ENTIRE PROP OF LOT#39
: SUBD: COMMODORE ESTATES
Remarks:
:                                                        Fax: (302)454-4262

Company     : CONECTIV POWER DELIVERY
Contact Name: VERA YOUNG            Contact Phone: (302)454-4222    Ext.:
Alt. Contact: MICHELLE              Alt. Phone   : (302)454-4115
Work Being Done For: CONECTIV
State: DE              County: NEW CASTLE
Map: NEWC  Page: 022   Grid Cells: K04
Explosives: N
ARTW34     CUDE01     DPU104     TMC01
Send To: TNC   01  Seq No: 0013   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01067932
Transmit      Date: 02/19/03      Time: 11:23    Op: j.thom
Original Call Date: 02/19/03      Time: 09:33    Op: j.thom
Work to Begin Date: 02/21/03      Time: 09AM

Place: TOWNSEND
Address: 239         Street: LABRADOR LA
Nearest Intersecting Street: BEAGLE DR

Type of Work: INST UG ELEC SVC
Extent of Work:   LOC ENTIRE PROP OF LOT#21
: SUBD: SPRING CREEK
Remarks:
:                                                        Fax: (302)454-4262

Company     : CONECTIV POWER DELIVERY
Contact Name: VERA YOUNG            Contact Phone: (302)454-4222    Ext.:
Alt. Contact: MICHELLE              Alt. Phone   : (302)454-4115
Work Being Done For: CONECTIV
State: DE              County: NEW CASTLE
Map: NEWC  Page: 026   Grid Cells: A01
Explosives: N
ARTW34     CUDE01     DPU104     TMC01
Send To: TNC   01  Seq No: 0014   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01067990
Transmit      Date: 02/19/03      Time: 11:23    Op: tanya
Original Call Date: 02/19/03      Time: 09:46    Op: tanya
Work to Begin Date: 02/21/03      Time: 10AM

Place: NEWARK
Address: 13          Street: VALLEY STREAM CIR N
Nearest Intersecting Street: WALTHER RD

Type of Work: REPAIR WTR SVC N/E
Extent of Work:   LOC FRONT YARD & LEFT SIDE AS FACING PROP OF ABOVE
: ADDRESS, CALLER REQUESTS FLAGS & PAINT
Remarks:
:                                                        Fax: (302)738-2566

Company     : WEGMAN BROTHERS
Contact Name: DWYANE BURNS          Contact Phone: (302)738-4328    Ext.:
Alt. Contact:                       Alt. Phone   :
Work Being Done For: JACKIE TIER
State: DE              County: NEW CASTLE
Map: NEWC  Page: 013   Grid Cells: F01
Explosives: N
ARTW34     ATT32      DPU104     DPUG07     HRCA20     LTC04      SDHY23
 SDHY25     TEXA01     WIM01
Send To: TNC   01  Seq No: 0015   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068046
Transmit      Date: 02/19/03      Time: 11:23    Op: joann.
Original Call Date: 02/19/03      Time: 10:06    Op: joann.
Work to Begin Date: 02/21/03      Time: 11AM

Place: WILMINGTON
Address: 501         Street: BELLEVUE PKWY
Nearest Intersecting Street: CARR RD

Type of Work: INSTALL A SHORT SIDE FIRE SERVICE
Extent of Work:   BELLVUE PARK CORPORTE CENTER 1500 S OF CARR RD ON E SIDE
: OF BELLVUE PKWY MEET WITH TOMMY
Remarks: MEETING REQUESTED BY CALLER - 2/21 11:00AM
:                                                        Fax: (302)995-2945

Company     : WORTHY CONSTRUCTION
Contact Name: BOB MAZZONI           Contact Phone: (302)892-2702    Ext.:
Alt. Contact: TOM F MOBILE          Alt. Phone   : (302)218-5329
Work Being Done For: UNITED WWATER
State: DE              County: NEW CASTLE
Map: NEWC  Page: 009   Grid Cells: D03
Explosives: N
DPU104     DPUG07     ETC01      SDHY23     WSUB34
Send To: TNC   01  Seq No: 0016   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068067
Transmit      Date: 02/19/03      Time: 11:23    Op: sandra
Original Call Date: 02/19/03      Time: 10:20    Op: sandra
Work to Begin Date: 02/21/03      Time: 10AM

Place: NEWARK
Address:             Street: RT 4
Nearest Intersecting Street: MARROWS RD

Type of Work: BUILDING AN ADDITION
Extent of Work:   LOC SIDEWALK IN FRONT OF BLDG AT 19 CHESTNUT HILL PLAZA
: WHICH IS LOCATED AT ABOVE INTER
Remarks: LEGALS NOT PROVIDED.
:                                                        Fax: (215)657-6316

Company     : NORTHWOOD CONSTRUCTION CO
Contact Name: CATHY BRYSON          Contact Phone: (215)657-5533    Ext.:
Alt. Contact: BOB    JOB FOREMAN C  Alt. Phone   : (215)778-8040
Work Being Done For: WAKEFERN FOOD CORPORATION
State: DE              County: NEW CASTLE
Map: NEWC  Page: 010   Grid Cells: K11
Explosives: N
DPU104     DPUG07     HRCA20     SDHY23     WSUB34
Send To: TNC   01  Seq No: 0017   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068096
Transmit      Date: 02/19/03      Time: 11:23    Op: j.thom
Original Call Date: 02/19/03      Time: 10:43    Op: j.thom
Work to Begin Date: 02/21/03      Time: 10AM

Place: REHOBOTH BEACH
Address: 33          Street: WANOMA CIR
Nearest Intersecting Street: COATUE BLVD

Type of Work: INST GUTTER DRAINAGE PIPES
Extent of Work:   LOC FROM FRONT DOWN SPOUTS TO ENTIRE REAR OF PROP
: AT ABOVE ADDRESS
: DIR: FROM DOVER SOUTH ON RT 1, TURN RIGHT ONTO OLD LANDING RD, GO
: STRAIGHT THRU FIRST STOP SIGN, GO 3/4MILE TURN LEFT ONTO COATUE BLVD
Remarks: TO WANOMA CIR, ACROSS RD FROM ARNELL CREEK
: LEGALS NOT PROVIDED FOR THIS LOCATE REQUEST  MAP & GRID APPROX

Company     : JEFFERY THOMAS
Contact Name: HOMEOWNER             Contact Phone: (717)244-4351    Ext.:
Alt. Contact: WORK                  Alt. Phone   : (717)246-1611
Work Being Done For: JEFFERY THOMAS
State: DE              County: SUSSEX
Map: SUSSX Page: 011   Grid Cells: J11,J12,K11
Explosives: N
CMC01      DECO26     SCED01     SISX44     TIDE02
Send To: DSTS  45  Seq No: 0042   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068099
Transmit      Date: 02/19/03      Time: 11:23    Op: geneva
Original Call Date: 02/19/03      Time: 10:44    Op: geneva
Work to Begin Date: 02/21/03      Time: 10AM

Place: WILMINGTON
Address: 622         Street: N. JEFFERSON ST
Nearest Intersecting Street: 6TH ST

Type of Work: DEMOLITION OF BLDG & SITE GRADING
Extent of Work:   LOCATE ENTIRE PROPERTY.
Remarks:
:                                                        Fax: (302)429-6925

Company     : DESIGN CONTRACTING, INC.
Contact Name: ANDREW DIFFLEY        Contact Phone: (302)429-6900    Ext.:
Alt. Contact: ANS MACHINE           Alt. Phone   :
Work Being Done For: CITY OF WILMINGTON
State: DE              County: NEW CASTLE
Map: NEWC  Page: 008   Grid Cells: E09
Explosives: N
DPU104     DPUG07     ETC01      HRCA20     MCIC40     MFN05      PFNT04
 SDHY23     SDHY25     TDCW01     WIM01
Send To: CNTV  01  Seq No: 0011   Map Ref:
         TNC   01          0018



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068143
Transmit      Date: 02/19/03      Time: 11:23    Op: j.thom
Original Call Date: 02/19/03      Time: 11:03    Op: j.thom
Work to Begin Date: 02/21/03      Time: 11AM

Place: REHOBOTH BEACH
Address: 33          Street: WANOMA CIR
Nearest Intersecting Street: COATUE BLVD

Type of Work: INST GUTTER DRAINAGE PIPES
Extent of Work:   LOC FROM FRONT DOWN SPOUTS TO ENTIRE REAR OF PROP
: AT ABOVE ADDRESS
: DIR: FROM DOVER SOUTH ON RT 1, TURN RIGHT ONTO OLD LANDING RD, GO
: STRAIGHT THRU FIRST STOP SIGN, GO 3/4MILE TURN LEFT ONTO COATUE BLVD
Remarks: TO WANOMA CIR, ACROSS RD FROM ARNELL CREEK
: LEGALS NOT PROVIDED FOR THIS LOCATE REQUEST  MAP & GRID APPROX

Company     : JEFFERY THOMAS
Contact Name: HOMEOWNER             Contact Phone: (717)244-4351    Ext.:
Alt. Contact: WORK                  Alt. Phone   : (717)246-1611
Work Being Done For: JEFFERY THOMAS
State: DE              County: SUSSEX
Map: SUSSX Page: 011   Grid Cells: K12
Explosives: N
CMC01      DECO26     SCED01     TIDE02
Send To: DSTS  45  Seq No: 0043   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068149
Transmit      Date: 02/19/03      Time: 11:24    Op: tanya
Original Call Date: 02/19/03      Time: 11:04    Op: tanya
Work to Begin Date: 02/21/03      Time: 11AM

Place: CLARKSVILLE
Address:             Street: RD 348
Nearest Intersecting Street: RD 346

Type of Work: REPLACING UG CATV SVC N/E
Extent of Work:   LOC ENTIRE PROP & ANY EASEMENTS TO NEAREST CATV PED OR
: POLE<> CALLER REQUESTS FLAGS & PAINT  ADDRESS: BOX 160 B ON RD 348
Remarks:

Company     : BAYSIDE UNDERGROUND
Contact Name: BILL GARDNER          Contact Phone: (302)934-1682    Ext.:
Alt. Contact: JOE SULLIVAN          Alt. Phone   :
Work Being Done For: MORGAN
State: DE              County: SUSSEX
Map: SUSSX Page: 023   Grid Cells: J02
Explosives: N
DECO26     DPML12     SDHY24     SISX44
Send To: DSTS  45  Seq No: 0044   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068158
Transmit      Date: 02/19/03      Time: 11:24    Op: tanya
Original Call Date: 02/19/03      Time: 11:07    Op: tanya
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILLSBORO
Address:             Street: AMERICAN AV
Nearest Intersecting Street: RD 22

Type of Work: REPLACING UG CATV SVC N/E
Extent of Work:   LOC ENTIRE PROP & ANY EASEMENTS TO NEAREST CATV PED OR
: POLE OF LOT #405 SUBD: LEISURE POINT, CALLER REQUESTS FLAGS & PAINT
Remarks:

Company     : BAYSIDE UNDERGROUND
Contact Name: BILL GARDNER          Contact Phone: (302)934-1682    Ext.:
Alt. Contact: JOE SULLIVAN          Alt. Phone   :
Work Being Done For: RUSO
State: DE              County: SUSSEX
Map: SUSSX Page: 017   Grid Cells: F06
Explosives: N
DECO26     SCED01     SDHY24     SISX44     TIDE02     TUNN01
Send To: DSTS  45  Seq No: 0045   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068163
Transmit      Date: 02/19/03      Time: 11:24    Op: tanya
Original Call Date: 02/19/03      Time: 11:09    Op: tanya
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILLSBORO
Address: 159         Street: TEAL DR
Nearest Intersecting Street: RD 22

Type of Work: REPLACING UG CATV SVC N/E
Extent of Work:   LOC ENTIRE PROP & ANY EASEMENTS TO NEAREST CATV PED OR
: POLE SUBD: CREEKS END, CALLER REQUESTS FLAGS & PAINT
Remarks:

Company     : BAYSIDE UNDERGROUND
Contact Name: BILL GARDNER          Contact Phone: (302)934-1682    Ext.:
Alt. Contact: JOE SULLIVAN          Alt. Phone   :
Work Being Done For: LEE
State: DE              County: SUSSEX
Map: SUSSX Page: 017   Grid Cells: G06
Explosives: N
DECO26     LNWC01     SCED01     SISX44     TIDE02
Send To: DSTS  45  Seq No: 0046   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068166
Transmit      Date: 02/19/03      Time: 11:24    Op: tanya
Original Call Date: 02/19/03      Time: 11:10    Op: tanya
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILLSBORO
Address:             Street: PLUM ST
Nearest Intersecting Street: RD 22

Type of Work: REPLACING UG CATV SVC N/E
Extent of Work:   LOC ENTIRE PROP & ANY EASEMENTS TO NEAREST CATV PED OR
: POLE OF LOT#197 SUBD: BAY CITY, CALLER REQUESTS FLAGS & PAINT
Remarks:

Company     : BAYSIDE UNDERGROUND
Contact Name: BILL GARDNER          Contact Phone: (302)934-1682    Ext.:
Alt. Contact: JOE SULLIVAN          Alt. Phone   :
Work Being Done For: MAUER
State: DE              County: SUSSEX
Map: SUSSX Page: 017   Grid Cells: G06
Explosives: N
DECO26     LNWC01     SCED01     SISX44     TIDE02
Send To: DSTS  45  Seq No: 0047   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068170
Transmit      Date: 02/19/03      Time: 11:24    Op: tanya
Original Call Date: 02/19/03      Time: 11:11    Op: tanya
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILLSBORO
Address:             Street: ARROWOOD CT
Nearest Intersecting Street: RD 22

Type of Work: REPLACING UG CATV SVC N/E
Extent of Work:   LOC ENTIRE PROP & ANY EASEMENTS TO NEAREST CATV PED OR
: POLE OF LOT#27 & 28 SUBD: POT NETS LAKESIDE, CALLER REQUESTS FLAGS &
: PAINT
Remarks:

Company     : BAYSIDE UNDERGROUND
Contact Name: BILL GARDNER          Contact Phone: (302)934-1682    Ext.:
Alt. Contact: JOE SULLIVAN          Alt. Phone   :
Work Being Done For: CROOKALL
State: DE              County: SUSSEX
Map: SUSSX Page: 017   Grid Cells: G07
Explosives: N
DECO26     LNWC01     SCED01     SISX44     TIDE02
Send To: DSTS  45  Seq No: 0048   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068175
Transmit      Date: 02/19/03      Time: 11:25    Op: tanya
Original Call Date: 02/19/03      Time: 11:13    Op: tanya
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILLSBORO
Address: 16          Street: ENCHANTED DR
Nearest Intersecting Street: RD 22

Type of Work: REPLACING UG CATV SVC N/E
Extent of Work:   LOC ENTIRE PROP & ANY EASEMENTS TO NEAREST CATV PED OR
: POLE  SUBD: ENCHANTED ACRES, CALLER REQUESTS FLAGS & PAINT
Remarks:

Company     : BAYSIDE UNDERGROUND
Contact Name: BILL GARDNER          Contact Phone: (302)934-1682    Ext.:
Alt. Contact: JOE SULLIVAN          Alt. Phone   :
Work Being Done For: BELEVINS
State: DE              County: SUSSEX
Map: SUSSX Page: 017   Grid Cells: H07
Explosives: N
DECO26     LNWC01     SCED01     SISX44
Send To: DSTS  45  Seq No: 0049   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068180
Transmit      Date: 02/19/03      Time: 11:25    Op: tanya
Original Call Date: 02/19/03      Time: 11:14    Op: tanya
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILLSBORO
Address: 117         Street: CONGRESS RD
Nearest Intersecting Street: RD 306  A

Type of Work: REPLACING UG CATV SVC N/E
Extent of Work:   LOC ENTIRE PROP & ANY EASEMENTS TO NEAREST CATV PED OR
: POLE  SUBD: HOLIDAY PINES, CALLER REQUESTS FLAGS & PAINT
Remarks:

Company     : BAYSIDE UNDERGROUND
Contact Name: BILL GARDNER          Contact Phone: (302)934-1682    Ext.:
Alt. Contact: JOE SULLIVAN          Alt. Phone   :
Work Being Done For: WILKERSON
State: DE              County: SUSSEX
Map: SUSSX Page: 017   Grid Cells: D05
Explosives: N
DECO26     SISX44
Send To: DSTS  45  Seq No: 0050   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068185
Transmit      Date: 02/19/03      Time: 11:25    Op: tanya
Original Call Date: 02/19/03      Time: 11:15    Op: tanya
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILLSBORO
Address: 2           Street: BOBLIN CT
Nearest Intersecting Street: RT 24

Type of Work: REPLACING UG CATV SVC N/E
Extent of Work:   LOC ENTIRE PROP & ANY EASEMENTS TO NEAREST CATV PED OR
: POLE  SUBD: WOODSBORO, CALLER REQUESTS FLAGS & PAINT
Remarks:

Company     : BAYSIDE UNDERGROUND
Contact Name: BILL GARDNER          Contact Phone: (302)934-1682    Ext.:
Alt. Contact: JOE SULLIVAN          Alt. Phone   :
Work Being Done For: MAGEE
State: DE              County: SUSSEX
Map: SUSSX Page: 016   Grid Cells: B11
Explosives: N
DECO26     SISX44
Send To: DSTS  45  Seq No: 0051   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068202               Update Of: 01065257
Transmit      Date: 02/19/03      Time: 11:25    Op: j.mess
Original Call Date: 02/19/03      Time: 11:18    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILTON
Address: 15          Street: W GREENWING DR
Nearest Intersecting Street: E GREENWING DR

Type of Work: INST CATV MAIN LINE
Extent of Work:   LOC ALL UTILS ON FRONT EASEMENT ON W SIDE OF W GREENWING
: DR BEGINNING AT HSE #15 TO HSE #21.
Remarks:
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: GREG HENSON           Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 010   Grid Cells: E02
Explosives: N
DECO26     DPML12     STOR01
Send To: DSTS  45  Seq No: 0052   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068210               Update Of: 01065245
Transmit      Date: 02/19/03      Time: 11:25    Op: j.mess
Original Call Date: 02/19/03      Time: 11:19    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILTON
Address: 25446       Street: SMITH WY
Nearest Intersecting Street: UNION ST

Type of Work: INST CATV MAIN LINE
Extent of Work:   LOC ALL UTILS ON FRONT EASEMENT ON S SIDE OF SMITH WY
: BEGINNING AT HSE #25446 & ENDING AT HSE #25456.
Remarks:
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: GREG HENSON           Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 010   Grid Cells: F01
Explosives: N
DECO26     STOR01
Send To: DSTS  45  Seq No: 0053   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068215               Update Of: 01065712
Transmit      Date: 02/19/03      Time: 11:26    Op: j.mess
Original Call Date: 02/19/03      Time: 11:20    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILFORD
Address:             Street: WASHINGTON ST
Nearest Intersecting Street: 2ND ST

Type of Work: INST CATV MAIN
Extent of Work:   LOC ON E SIDE OF WASHINGTON ST FROM ABOVE INTER N TO PARK
: AV
Remarks:
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 005   Grid Cells: F04
Explosives: N
CIMI35     SDHY24     STOR01
Send To: DSTS  45  Seq No: 0054   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068216               Update Of: 01065718
Transmit      Date: 02/19/03      Time: 11:26    Op: j.mess
Original Call Date: 02/19/03      Time: 11:20    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: GEORGETOWN
Address:             Street: PINE ST
Nearest Intersecting Street: W MARKET ST

Type of Work: INST CATV MAIN
Extent of Work:   LOC ON BOTH SIDES OF PINE ST FROM SUSSEX CENTRAL SCHOOL
: ACROSS PINE ST TO TELE POLE   AREA IS MARKED WITH ORANGE ARROWS
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 015   Grid Cells: J02
Explosives: N
DPML12     SDHY24     STOR01     TOG01
Send To: DSTS  45  Seq No: 0055   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068218               Update Of: 01066261
Transmit      Date: 02/19/03      Time: 11:26    Op: j.mess
Original Call Date: 02/19/03      Time: 11:21    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: GEORGETOWN
Address:             Street: COUNTY SEAT HWY #9
Nearest Intersecting Street: ASBURY RD     RD 446

Type of Work: INST CATV FEEDER CABLE
Extent of Work:   LOC EASEMENT 150FT N & S OF P#52509 LOCATED ON W SIDE OF
: COUNTY SEAT HWY #9 APPROX 1200FT S OF ABOVE INTER
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 015   Grid Cells: B07
Explosives: N
DECO26     STOR01
Send To: DSTS  45  Seq No: 0056   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068220               Update Of: 01066312
Transmit      Date: 02/19/03      Time: 11:26    Op: j.mess
Original Call Date: 02/19/03      Time: 11:21    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILFORD
Address:             Street: PINE HAVEN RD       RD 224
Nearest Intersecting Street: HERRING RANCH RD    RD 624

Type of Work: INST CATV MAIN
Extent of Work:   ON EASEMENT S SIDE OF PINE HAVEN RD AT THE INTER OF
: HERRING RANCH RD GO 750FT W TO HSE #22326 LOC 460FT W TO NEXT RD INTER
: WHICH HAS NO NAME
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 006   Grid Cells: A08,B08
Explosives: N
DECO26     DPML12     SDHY24     STOR01
Send To: DSTS  45  Seq No: 0057   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068224               Update Of: 01066239
Transmit      Date: 02/19/03      Time: 11:26    Op: j.mess
Original Call Date: 02/19/03      Time: 11:22    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: GEORGETOWN
Address:             Street: DONOVANS RD    RD 243
Nearest Intersecting Street: WILSON RD      RD 244

Type of Work: INST CATV TRUNK LINE
Extent of Work:   LOC E SIDE OF DONOVANS RD FROM TRAILOR#034142 AT ABOVE
: INTER S 1200FT TO TRAILOR#05859
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 009   Grid Cells: H11
Explosives: N
DECO26     DPML12     SDHY24     STOR01
Send To: DSTS  45  Seq No: 0058   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068226               Update Of: 01066112
Transmit      Date: 02/19/03      Time: 11:26    Op: j.mess
Original Call Date: 02/19/03      Time: 11:22    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place:
Address:             Street: CEDAR CREEK RD  RD 212
Nearest Intersecting Street: NEAL RD  RD 637

Type of Work: PLACING CONDUIT AND PIPE
Extent of Work:   LOCATE EASEMENT ALONG THE E SIDE OF CEDAR CREEK RD/RD212
: GO FROM ADDRESS 47113 HEADING N TO 47116 AT INTER OF NEAL RD/RD637
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 006   Grid Cells: A09
Explosives: N
DECO26     STOR01
Send To: DSTS  45  Seq No: 0059   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068228               Update Of: 01066280
Transmit      Date: 02/19/03      Time: 11:27    Op: j.mess
Original Call Date: 02/19/03      Time: 11:23    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILFORD
Address:             Street: DONOVAN ST
Nearest Intersecting Street: RT 113 N

Type of Work: INST CATV FEEDER CABLE
Extent of Work:   LOC EASEMENT ON S SIDE OF DONOVAN ST STARTING AT P#530376
: LOCATED ON CORNER OF ABOVE INTER GO E APPROX 195FT
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 005   Grid Cells: E06
Explosives: N
CIMI35     DPML12     STOR01
Send To: DSTS  45  Seq No: 0060   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068230               Update Of: 01066298
Transmit      Date: 02/19/03      Time: 11:27    Op: j.mess
Original Call Date: 02/19/03      Time: 11:23    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILFORD
Address:             Street: PINE HAVEN RD    RD 224
Nearest Intersecting Street: CEDAR CREEK RD   RD 212

Type of Work: INST CATV MAIN
Extent of Work:   LOC EASEMENT N SIDE OF PINE HAVEN RD FROM CORNER OF CEDAR
: CREEK RD GO E TO HSE #21783 WHICH IS APPROX 2200FT
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 005   Grid Cells: K08
Explosives: N
DECO26     DPML12     STOR01
Send To: DSTS  45  Seq No: 0061   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068232               Update Of: 01066331
Transmit      Date: 02/19/03      Time: 11:27    Op: j.mess
Original Call Date: 02/19/03      Time: 11:24    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILFORD
Address:             Street: NEAL RD           RD 637
Nearest Intersecting Street: SLAUGHTERNECK RD

Type of Work: INST CATV MAIN
Extent of Work:   LOC N SIDE OF EASEMENT ALONG NEAL RD W APPROX 1700FT FROM
: SLAUGHTERNECK RD TO HSE #2369
Remarks: CALLER REQUESTS FLAGS AND PAINT
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 006   Grid Cells: B10
Explosives: N
DECO26     STOR01
Send To: DSTS  45  Seq No: 0062   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068234               Update Of: 01066341
Transmit      Date: 02/19/03      Time: 11:27    Op: j.mess
Original Call Date: 02/19/03      Time: 11:24    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: MILFORD
Address:             Street: SLAUGHTERNECK RD
Nearest Intersecting Street: NEAL RD            RD 637

Type of Work: INST CATV MAIN
Extent of Work:   LOC N SIDE OF EASEMENT ALONG SLAUGHTERNECK RD FROM ABOVE
: INTER E TO HSE #22723
Remarks: CALLER REQUESTS BOTH FLAGS AND PAINT
:                                                        Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 006   Grid Cells: B10
Explosives: N
DECO26     STOR01
Send To: DSTS  45  Seq No: 0063   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068238               Update Of: 01051830
Transmit      Date: 02/19/03      Time: 11:27    Op: j.mess
Original Call Date: 02/19/03      Time: 11:25    Op: j.mess
Work to Begin Date: 02/21/03      Time: 11AM

Place: GEORGETOWN
Address:             Street: EBONY ST
Nearest Intersecting Street: GARDEN CIR

Type of Work: INST UG FIBER OPTICS
Extent of Work:   LOC FROM FRONT OF TRAILOR #20 ON EBONY ST TO FRONT OF
: TRAILOR #2 ON GARDEN CIR--PROPS ARE AT ABOVE INTER
Remarks: SUBD--GARDENS TRAILOR PARK
: TKT TAKEN PER CALLER                                   Fax: (804)745-6741

Company     : FIBERTECH
Contact Name: RAY HAMILTON          Contact Phone: (804)745-6740    Ext.:
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: COMCAST
State: DE              County: SUSSEX
Map: SUSSX Page: 009   Grid Cells: H11
Explosives: N
DECO26     DPML12     SDHY24     STOR01
Send To: DSTS  45  Seq No: 0064   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 01068240
Transmit      Date: 02/19/03      Time: 11:29    Op: emily
Original Call Date: 02/19/03      Time: 11:26    Op: emily
Work to Begin Date: 02/21/03      Time: 11AM

Place: CLAYTON
Address: 202         Street: N BASSETT ST
Nearest Intersecting Street: INDUSTRIAL DR

Type of Work: SOIL TEST BORINGS
Extent of Work:   PLS USE PIN FLAGS & PAINT, SITE IS ONE STORY METAL BLDG.
: LOC ENTIRE PROP INCLUDING PARKING LOT
Remarks:
: CALLER REQUESTS BOTH FLAGS AND PAINT                   Fax: (302)322-8921

Company     : WIK ASSOCIATES
Contact Name: JOSH SOBELMAN         Contact Phone: (302)322-2559    Ext.: 99
Alt. Contact: NONE                  Alt. Phone   :
Work Being Done For: WIK ASSOCIATES
State: DE              County: KENT
Map: KENTD Page: 005   Grid Cells: A06,A07
Explosives: N
ATT32      CUDE14     DPML12     SDHY24     SMYR01     STOR24     TOC01
Send To: KCBA  01  Seq No: 0007   Map Ref:





