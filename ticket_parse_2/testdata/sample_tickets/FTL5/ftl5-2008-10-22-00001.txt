
--===SEE838999156780171===MIX===
Content-Type: text/plain
Content-Transfer-Encoding: quoted-printable





KORTERRA JOB MARY01-SMTP-1 082691473 Seq: 2 10/22/2008 10:52:53
U07        00021 TNOCS 09/25/08 14:38:00 082691473 EMERGENCY GRID

=3D=3D=3D=3D=3D=3D=3D=3D//  TNOCS LOCATE REQUEST  //=3D=3D=3D=3D=3D=3D=3D=3D

TICKET NUMBER--[082691473]   =3D=3D E M E R G E N C Y =3D=3D
OLD TICKET NUM-[]

MESSAGE TYPE---[EMERGENCY] LEAD TIME--[0]
PREPARED-------[09/25/08]  AT  [1438]  BY  [CHANCE]

CONTRACTOR--[MIKE DAVIS PLUMBING]
CALLER--[JIM MAJORS]
ADDRESS-----[PO BOX 6346]
CITY--------[MARYVILLE]   STATE--[TN]   ZIP--[37802]
CALL BACK---[(865) 740-8809 OR ABOVE NUMBER]
PHONE--[(865) 856-3639]
CONTACT-----[JIM MAJORS]   PHONE--[(865) 856-3639]
CONTACT FAX-[]

WORK TO BEGIN--[09/25/08] AT [1445]      STATE--[TN]
COUNTY---[BLOUNT]  PLACE--[MARYVILLE]

ADDRESS--[910]   STREET--[][YOUNG][AVE][]
NEAREST INTERSECTION--------[N MAPLE ST]
LATITUDE--[35.7483355050178]    LONGITUDE--[-83.9852340000009]
SECONDARY LATITUDE--[35.7449241337784] SECONDARY LONGITUDE--[-83.9817022274236]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[EMERGENCY - UTIL IS REQUIRED BY LAW TO MARK U/G FACILITIES WITHIN 2 HOURS     
[OF NOTIFICATION...CREW IS ON SITE...FROM THE INTER GO N APPX 300-400 YARDS    
[TO THE PROP...MARK FRONT OF PROP...AREA MARKED IN WHITE PAINT...              

WORK TYPE--[SEWER LINE, INSTL AND/OR REPAIR]
DONE FOR--[MILLERS FUNERAL HOME]
EXTENT-----[]

EXPLOSIVES--[N]    WHITE PAINT--[Y]    GRID GIVEN--[N]

MAP REF.--[]
GRIDS-----
          [57F              ] [57E              ] [57K              ]
          [57L              ]

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME
  ---------- --------------------------   ---------- --------------------------
  ALW        ALCOA UTILITIES - WATER...   B03        ATT/D-KNOX (615)661-3758
  CK4        CHARTER COMMUNICATIONS ...   CM         MARYVILLE UTILITIES, CI...
  U07        ATMOS ENERGY (UNITED CI...



--===SEE838999156780171===MIX===--


