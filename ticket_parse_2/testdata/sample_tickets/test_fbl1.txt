


NOTICE OF INTENT TO EXCAVATE 
24HR-24 HOURS NOTICE                    NEW TICKET 
Ticket No:   173246 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:12     Op: heathe.0 
Original Call Date:  6/14/02            Time: 11:57     Op: .0 

Company     : ENTERPRISE PRODUCTS 
Contact Name: ALFRED EGLAND                   Contact Phone: (337) 477-7640 
Alt. Contact: CELL                            Alt. Phone   : (713) 553-5142 

Type of Work : RELOCATING PIPELINE VENTS & TEST LEADS 
Work Done For: ENTERPRISE PRODUCTS 

State: LA       Parish: CALCASIEU 
Place: LAKE CHARLES CITY 
Address:             Street: WEAVER RD  
Nearest Intersecting Street: HAM REID RD  

Location:       ON WEAVER APX 200FT N OF HAM REID ON THE E SIDE OF RD  
: MARK THE FRONT  

Remarks:  
 

Work to Begin Date:  6/18/02            Time:  8:00 AM 
Mark By       Date:  6/18/02            Time: 12:00 PM 

Ex. Coord: NW Lat: 30 9 29   Lon: -93 15 26  SE Lat: 30 9 12   Lon:  -93 15 24 

Additional Members:  BRIDGE01, COLP01, EPC01, EQPLLC01, GSU01, LCW01, MBLSW01 
 TRDNT01 

Send To:  ETL01     Seq No:  35 
Send To:  LC01      Seq No:  97 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173249 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:12     Op: allison.0 
Original Call Date:  6/14/02            Time: 11:59     Op: .0 

Company     : CLECO 
Contact Name: ROSS BEST                       Contact Phone: (985) 867-4624 
Alt. Contact:                                 Alt. Phone   : (985) 867-6330 

Type of Work : RELOCATING A POLE/INSTALLING AN ANCHOR ELECTRIC 
Work Done For: CLECO 

State: LA       Parish: ST. TAMMANY 
Place: COVINGTON CITY 
Address:             Street: STATE HWY 1085  
Nearest Intersecting Street: BRICKER RD  

Location:       ON STATE HWY 1085 GO NW APX 300FT FROM BRICKER RD--@ THAT  
: POINT GO DUE W APX 360FT TO STAKED POLE--MARK AROUND THE POLE 

Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:00 PM 
Mark By       Date:  6/18/02            Time: 12:00 PM 

Ex. Coord: NW Lat: 30 27 35  Lon: -90 7 56   SE Lat: 30 27 30  Lon:  -90 7 48 

Additional Members:  BUC01, CLEC02, HM02, LAGC01, LWS01 

Send To:  CHART02   Seq No:  37 
Send To:  HM02      Seq No:  65 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173254 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: jennifer.0 
Original Call Date:  6/14/02            Time: 12:03     Op: jennifer.0 

Company     : ATMOS ENERGY LOUISIANA 
Contact Name: JEFF                            Contact Phone: (985) 892-2122 
Alt. Contact: OFFICE #                        Alt. Phone   : (985) 643-0084 

Type of Work : INSTALL GAS SVC 
Work Done For: ATMOS ENERGY LOUISIANA 

State: LA       Parish: ST. TAMMANY 
Place: MANDEVILLE CITY 
Address: 7139        Street: CREEKWOOD  
Nearest Intersecting Street: MEADOWBROOK DR  

Location:       MARK ENTIRE PROPERTY & MARK IN FRONT ON BOTH SIDES OF ST  
: -- IN MEADOWBROOK SUBDV 

Remarks: UPDATE:158479-ONGOING 
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 23 43  Lon: -90 4 47   SE Lat: 30 23 26  Lon:  -90 4 30 

Additional Members:  CLEC02, ETB01, GLU01, LAGC01, LWS01 

Send To:  CHART02   Seq No:  38 
Send To:  HM02      Seq No:  66 



NOTICE OF INTENT TO EXCAVATE 
REPR-REPAIR                             NEW TICKET 
Ticket No:   173255 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: casandra.0 
Original Call Date:  6/14/02            Time: 12:04     Op: .0 

Company     : CITY OF COVINGTON 
Contact Name: TOMMY                           Contact Phone: (985) 898-4710 
Alt. Contact:                                 Alt. Phone   : (985) 898-4700 

Type of Work : REPAIR SEWER PIPE 
Work Done For: CITY OF COVINGTON 

State: LA       Parish: ST. TAMMANY 
Place: COVINGTON CITY 
Address:             Street: S JEFFERSON AVE  
Nearest Intersecting Street: E 20TH AVE  

Location:       ON S JEFFERSON AVE - B/N E 20TH AVE & E TEMPERANCE - MARK  
: IN FRONT ON E SIDE OF RD 

Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 28 29  Lon: -90 6 6    SE Lat: 30 28 25  Lon:  -90 6 2 

Additional Members:  CLEC02, COVING01, LAGC01 

Send To:  HM02      Seq No:  67 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173256 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: candice.0 
Original Call Date:  6/14/02            Time: 12:05     Op: candice.0 

Company     : HOME CERTIFICATION SVCS OF LA 
Contact Name: RODNEY                          Contact Phone: (985) 542-9004 
Alt. Contact: MOBILE #                        Alt. Phone   : (985) 687-2047 

Type of Work : BURYING CABLE TV 
Work Done For: CHARTER COMMUNICATIONS 

State: LA       Parish: ST. TAMMANY 
Place: COVINGTON CITY 
Address: 80491       Street: LARK  
Nearest Intersecting Street: KENZIE  

Location:       MARK THE ENTIRE PROP--KENZIE IS OFF OF HWY 1129 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 36 8   Lon: -90 5 15   SE Lat: 30 35 59  Lon:  -90 5 11 

Additional Members:  CLEC02, LAGC01, LEE01, WSE01 

Send To:  CHART01   Seq No:  13 
Send To:  HM02      Seq No:  68 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173258 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: candice.0 
Original Call Date:  6/14/02            Time: 12:06     Op: candice.0 

Company     : HOME CERTIFICATION SVCS OF LA 
Contact Name: RODNEY                          Contact Phone: (985) 542-9004 
Alt. Contact: MOBILE #                        Alt. Phone   : (985) 687-2047 

Type of Work : BURYING CABLE TV 
Work Done For: CHARTER COMMUNICATIONS 

State: LA       Parish: TANGIPAHOA 
Place: TICKFAW VILLAGE 
Address: 48609       Street: ROBERTSON  
Nearest Intersecting Street: STAFFORD  

Location:       MARK THE ENTIRE PROP- 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 33 6   Lon: -90 31 32  SE Lat: 30 33 5   Lon:  -90 30 58 

Additional Members:  LAGH01, TANGW01 

Send To:  CHART01   Seq No:  14 
Send To:  HM01      Seq No:  27 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173259 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: jennifer.0 
Original Call Date:  6/14/02            Time: 12:06     Op: .0 

Company     : EMANUEL LANDRY 
Contact Name: EMANUEL LANDRY                  Contact Phone: (337) 836-5479 
Alt. Contact:                                 Alt. Phone   : (337) 201-0412 

Type of Work : LEVEL/GRINDING STUMP-LANDSCAPING 
Work Done For: EMANUEL LANDRY 

State: LA       Parish: ST. MARY 
Place: CENTERVILLE 
Address: 203         Street: DARWIN RD  
Nearest Intersecting Street: STATE HWY 182  

Location:       MARK L SIDE OF PROP AS FACING 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 29 45 35  Lon: -91 25 27  SE Lat: 29 45 13  Lon:  -91 25 18 

Additional Members:  COXCOM02, ETF01, HILENE01, SMARY01 

Send To:  HO01      Seq No:  55 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173257 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: heathe.0 
Original Call Date:  6/14/02            Time: 12:06     Op: heathe.0 

Company     : DON GULLEY 
Contact Name: DON GULLEY                      Contact Phone: (337) 566-2779 
Alt. Contact: CELL 945-8207                   Alt. Phone   : (337) 594-3230 

Type of Work : DEEPEN EXSTISTING DRAINAGE DITCH 
Work Done For: THREE MILE COMMUNITY 

State: LA       Parish: ST. LANDRY 
Place: KROTZ SPRINGS TOWN 
Address:             Street: THREE MILE AVE  
Nearest Intersecting Street: FRONT ST  

Location:       ON THREE MILE APX 10FT W OF FRONT MARK ON THE N SIDE OF  
: RD GOING N INTO PROP FOR APX 30FT ALONG DRAINAGE DITCH 

Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 32 58  Lon: -91 49 55  SE Lat: 30 32 57  Lon:  -91 49 54 

Additional Members:  ATT01, MCI01, RWW01, TEXAS01, USS01 

Send To:  LF01      Seq No: 118 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173260 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: allison.0 
Original Call Date:  6/14/02            Time: 12:07     Op: .0 

Company     : A & B CONSTRUCTION 
Contact Name: DUANE CORMIER                   Contact Phone: (337) 233-2195 
Alt. Contact: MOBILE #                        Alt. Phone   : (337) 319-4937 

Type of Work : BURY CABLE TV 
Work Done For: COX COMMUNICATIONS 

State: LA       Parish: LAFAYETTE 
Place: LAFAYETTE CITY 
Address:             Street: EASTLAND DR  
Nearest Intersecting Street: CAMELLIA BLVD  

Location:       START MARKING IN FRONT OF 205 EASTLAND DR--CONT MARKING  
: IN FRONT GOING E APX 155FT TO THE INTERS OF EASTLAND DR AND CAMELLIA  
: BLVD--CONT MARKING IN FRONT ON THE W SIDE OF CAMELLIA BLVD GOING N APX  
: 235FT--STARTING AND STOPPING POINTS ARD MARKED IN WHITE PAINT 

Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 11 31  Lon: -92 3 24   SE Lat: 30 11 29  Lon:  -92 3 20 

Additional Members:  

Send To:  CLW02     Seq No:  39 
Send To:  LF01      Seq No: 119 
Send To:  TLG02     Seq No:  58 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173261 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: candice.0 
Original Call Date:  6/14/02            Time: 12:07     Op: candice.0 

Company     : HOME CERTIFICATION SVCS OF LA 
Contact Name: RODNEY                          Contact Phone: (985) 542-9004 
Alt. Contact: MOBILE #                        Alt. Phone   : (985) 687-2047 

Type of Work : BURYING CABLE TV 
Work Done For: CHARTER COMMUNICATIONS 

State: LA       Parish: TANGIPAHOA 
Place: ROBERT 
Address: 22320       Street: KOVEL LN  
Nearest Intersecting Street: CHEMEKETTE  

Location:       MARK THE ENTIRE PROP- 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 31 45  Lon: -90 21 20  SE Lat: 30 31 42  Lon:  -90 21 2 

Additional Members:  LAGH01 

Send To:  CHART01   Seq No:  15 
Send To:  HM01      Seq No:  28 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173262 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: candice.0 
Original Call Date:  6/14/02            Time: 12:08     Op: candice.0 

Company     : HOME CERTIFICATION SVCS OF LA 
Contact Name: RODNEY                          Contact Phone: (985) 542-9004 
Alt. Contact: MOBILE #                        Alt. Phone   : (985) 687-2047 

Type of Work : BURYING CABLE TV 
Work Done For: CHARTER COMMUNICATIONS 

State: LA       Parish: TANGIPAHOA 
Place: NATALBANY CDP 
Address: 48290       Street: TIN CAN ALLEY  
Nearest Intersecting Street: STATE HWY 1064  

Location:       MARK THE ENTIRE PROP- 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 33 35  Lon: -90 26 55  SE Lat: 30 32 53  Lon:  -90 26 52 

Additional Members:  HAMWTR01, LAGH01 

Send To:  HM01      Seq No:  29 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    CANCELLED 
Ticket No:   172325 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: allison.0 
Original Call Date:  6/13/02            Time: 15:35     Op: carolyn.0 

Company     : ERROL JUNCA 
Contact Name: ERROL JUNCA                     Contact Phone: (337) 923-4757 
Alt. Contact:                                 Alt. Phone   : (337) 578-1852 

Type of Work : INSTALL SEWER LINE 
Work Done For: MABLE MARTIN 

State: LA       Parish: ST. MARY 
Place: CHARENTON CDP 
Address: 208         Street: E MARTIN LUTHER KING RD  
Nearest Intersecting Street: JOE WHITE ST  

Location:       MARK THE FRONT OF PROP 
Remarks: CANCEL PER CUST REQ--JOB WAS COMPLETED-CCB@ 12:13PM 
6/14 CRD 

Work to Begin Date:  6/17/02            Time:  3:45 PM 
Mark By       Date:  6/17/02            Time:  3:45 PM 

Ex. Coord: NW Lat: 29 52 53  Lon: -91 31 39  SE Lat: 29 52 47  Lon:  -91 31 30 

Additional Members:  CLEC07, COXCOM02, SMARYW01 

Send To:  HO01      Seq No:  56 
Send To:  TLG02     Seq No:  59 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173263 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: casandra.0 
Original Call Date:  6/14/02            Time: 12:08     Op: .0 

Company     : TOM SARRADET 
Contact Name: TOM SARRADET                    Contact Phone: (225) 756-8680 
Alt. Contact: CELL PHONE                      Alt. Phone   : (225) 953-2280 

Type of Work : POURING CONCRETE FOR FOUNDATION APX 4" 
Work Done For: TOM SARRADET 

State: LA       Parish: EAST BATON ROUGE 
Place: BATON ROUGE CITY 
Address: 1737        Street: HOBBITON RD  
Nearest Intersecting Street: TUCKBOROUGH  

Location:       THE SHIRE SUB - MARK IN REAR 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 21 51  Lon: -91 4 2    SE Lat: 30 21 39  Lon:  -91 3 54 

Additional Members:  BRW01, GSU04, MCI01, TCIBTR01 

Send To:  BTR01     Seq No: 109 
Send To:  GSUBR01   Seq No:  67 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173253 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:13     Op: jennifer.0 
Original Call Date:  6/14/02            Time: 12:03     Op: jennifer.0 

Company     : ROY FRITZERTZ CONSTRUCTION 
Contact Name: ROBERT                          Contact Phone: (504) 734-1805 
Alt. Contact: MIKE BERRY                                      

Type of Work : DEMOLITON OF CONCRETE & DRIVING PILINGS 
Work Done For: ST. GABRIEL CHURCH 

State: LA       Parish: ORLEANS 
Place: NEW ORLEANS CITY 
Address: 4700        Street: PINEDA ST  
Nearest Intersecting Street: PIETY DR  

Location:       MARK ENTIRE PROP--DO NOT MARK FOOTBALL FIELDS 
Remarks: CCB:6/14 @ 12;20-JLK 
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 79 24  Lon: -90 2 15   SE Lat: 30 75 46  Lon:  -90 2 10 

Additional Members:  ENTGY11, NOPSI01 

Send To:  GENT02    Seq No:  32 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173264 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:14     Op: candice.0 
Original Call Date:  6/14/02            Time: 12:09     Op: candice.0 

Company     : HOME CERTIFICATION SVCS OF LA 
Contact Name: RODNEY                          Contact Phone: (985) 542-9004 
Alt. Contact: MOBILE #                        Alt. Phone   : (985) 687-2047 

Type of Work : BURYING CABLE TV 
Work Done For: CHARTER COMMUNICATIONS 

State: LA       Parish: TANGIPAHOA 
Place: PONCHATOULA CITY 
Address: 23353       Street: STEPP  
Nearest Intersecting Street: STATE HWY 445  

Location:       MARK THE ENTIRE PROP- 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 29 27  Lon: -90 20 19  SE Lat: 30 29 11  Lon:  -90 19 39 

Additional Members:  HM01, LAGH01 

Send To:  HM01      Seq No:  30 



NOTICE OF INTENT TO EXCAVATE 
REPR-REPAIR                             NEW TICKET 
Ticket No:   173265 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:14     Op: jennifer.0 
Original Call Date:  6/14/02            Time: 12:09     Op: .0 

Company     : CONSOLIDATED WATER WORKS 
Contact Name: LIONELL                         Contact Phone: (985) 879-2495 
Alt. Contact: EMERGENCY # ONLY                Alt. Phone   : (985) 868-1242 

Type of Work : REPAIR WATER LEAK 
Work Done For: CONSOLIDATED WATER WORKS 

State: LA       Parish: TERREBONNE 
Place: HOUMA CITY 
Address: 302         Street: KEYSTONE LOOP  
Nearest Intersecting Street: VICTORIA  

Location:       MARK IN FRONT ON BOTH SIDES OF RD 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 29 34 46  Lon: -90 44 23  SE Lat: 29 34 41  Lon:  -90 44 9 

Additional Members:  HTV01, TCG01, TERPAR01 

Send To:  HO01      Seq No:  57 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173266 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:14     Op: carolyn.0 
Original Call Date:  6/14/02            Time: 12:10     Op: .0 

Company     : ERROL JUNCA 
Contact Name: ERROL JUNCA                     Contact Phone: (337) 923-4757 
Alt. Contact:                                 Alt. Phone   : (337) 578-1852 

Type of Work : CONNECTING SEWER LINE 
Work Done For: ELMER CARTER 

State: LA       Parish: ST. MARY 
Place: CHARENTON CDP 
Address: 200         Street: MARTIN LUTHER KING  
Nearest Intersecting Street: CHITIMACHA TRL  

Location:       MARK THE FRONT OF PROP 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 29 52 52  Lon: -91 32 22  SE Lat: 29 52 14  Lon:  -91 32 16 

Additional Members:  CLEC07, COXCOM02, SMARYW01 

Send To:  HO01      Seq No:  58 
Send To:  TLG02     Seq No:  60 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173267 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:14     Op: candice.0 
Original Call Date:  6/14/02            Time: 12:10     Op: candice.0 

Company     : HOME CERTIFICATION SVCS OF LA 
Contact Name: RODNEY                          Contact Phone: (985) 542-9004 
Alt. Contact: MOBILE #                        Alt. Phone   : (985) 687-2047 

Type of Work : BURYING CABLE TV 
Work Done For: CHARTER COMMUNICATIONS 

State: LA       Parish: LIVINGSTON 
Place: LIVINGSTON TOWN 
Address: 20615       Street: BLIND ST  
Nearest Intersecting Street: OHIO ST  

Location:       MARK THE ENTIRE PROP- 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 29 51  Lon: -90 44 48  SE Lat: 30 29 47  Lon:  -90 44 29 

Additional Members:  DIX01, GSUBRE02, MCI01, TLV01, USS01 

Send To:  BTR01     Seq No: 110 
Send To:  GSUBRE02  Seq No:  13 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173268 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:14     Op: allison.0 
Original Call Date:  6/14/02            Time: 12:10     Op: .0 

Company     : A B C WELL & SEWAGE 
Contact Name: CHRISTA                         Contact Phone: (504) 386-3890 
Alt. Contact: TERRI                                           

Type of Work : INSTALLING A SEWER SYSTEM 
Work Done For: WENDALL MILEY 

State: LA       Parish: TANGIPAHOA 
Place: LORANGER 
Address: 21273       Street: CHAPPAPEELA RD  
Nearest Intersecting Street: BLOUNT RD  

Location:       MARK THE FRONT OF PROP  
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 34 40  Lon: -90 23 11  SE Lat: 30 33 59  Lon:  -90 22 14 

Additional Members:  

Send To:  HM01      Seq No:  31 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173269 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:14     Op: candice.0 
Original Call Date:  6/14/02            Time: 12:11     Op: candice.0 

Company     : HOME CERTIFICATION SVCS OF LA 
Contact Name: RODNEY                          Contact Phone: (985) 542-9004 
Alt. Contact: MOBILE #                        Alt. Phone   : (985) 687-2047 

Type of Work : BURYING CABLE TV 
Work Done For: CHARTER COMMUNICATIONS 

State: LA       Parish: LIVINGSTON 
Place: ALBANY VILLAGE 
Address: 30670       Street: STRAWBERRY LN  
Nearest Intersecting Street: STATE HWY 43  

Location:       MARK THE ENTIRE PROP- 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 28 30  Lon: -90 34 41  SE Lat: 30 28 23  Lon:  -90 33 50 

Additional Members:  DIX01, FSWC01, LAGH01 

Send To:  CHART01   Seq No:  16 
Send To:  GSU05     Seq No:   7 
Send To:  HM01      Seq No:  32 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173270 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:14     Op: candice.0 
Original Call Date:  6/14/02            Time: 12:12     Op: candice.0 

Company     : HOME CERTIFICATION SVCS OF LA 
Contact Name: RODNEY                          Contact Phone: (985) 542-9004 
Alt. Contact: MOBILE #                        Alt. Phone   : (985) 687-2047 

Type of Work : BURYING CABLE TV 
Work Done For: CHARTER COMMUNICATIONS 

State: LA       Parish: LIVINGSTON 
Place: WALKER TOWN 
Address: 18945       Street: LOD STAFFORD  
Nearest Intersecting Street: STATE HWY 1024  

Location:       MARK THE ENTIRE PROP- 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 31 38  Lon: -90 48 15  SE Lat: 30 31 29  Lon:  -90 47 14 

Additional Members:  DENRES01, DIX01, EPC01, EQPLJ01, TC05, TWK01 

Send To:  BTR01     Seq No: 111 
Send To:  CHART02   Seq No:  39 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173272 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:15     Op: heathe.0 
Original Call Date:  6/14/02            Time: 12:13     Op: heathe.0 

Company     : DON GULLEY 
Contact Name: DON GULLEY                      Contact Phone: (337) 566-2779 
Alt. Contact: CELL 945-8207                   Alt. Phone   : (337) 594-3230 

Type of Work : DEEPEN EXSTISTING DRAINAGE DITCH 
Work Done For: THREE MILE COMMUNITY 

State: LA       Parish: ST. LANDRY 
Place: KROTZ SPRINGS TOWN 
Address:             Street: THREE MILE AVE  
Nearest Intersecting Street: BOXWOOD ST  

Location:       ON THREE MILE APX 90FT W OF BOXWOOD MARK ON THE N SIDE OF  
: RD GOING N INTO PROP FOR APX 60FT ALONG DRAINAGE DITCH 

Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 33 73  Lon: -91 50 10  SE Lat: 30 32 57  Lon:  -91 49 56 

Additional Members:  ATT01, MCI01, RWW01, TEXAS01, USS01 

Send To:  LF01      Seq No: 120 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173273 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:15     Op: allison.0 
Original Call Date:  6/14/02            Time: 12:13     Op: allison.0 

Company     : MELVIN DUNCAN 
Contact Name: MELVIN DUNCAN                   Contact Phone: (225) 664-8577 
Alt. Contact: ANSWERING MACHINE                               

Type of Work : INSTALLING SEWER LINES 
Work Done For: KENECO PLUMBING 

State: LA       Parish: EAST BATON ROUGE 
Place: BATON ROUGE CITY 
Address:             Street: RIVERINE DR  
Nearest Intersecting Street: TWELVE OAKS  

Location:       LOT 130--MARK THE FRONT OF PROP  
Remarks: 129869/144260/156380 ONGOING 
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 22 55  Lon: -91 12 13  SE Lat: 30 22 37  Lon:  -91 12 93 

Additional Members:  BRW01, GSU04, LSU01, PRAXLC01, TCIBTR01 

Send To:  BTR01     Seq No: 112 
Send To:  GSUBR01   Seq No:  68 



NOTICE OF INTENT TO EXCAVATE 
48HR-48 HOURS NOTICE                    NEW TICKET 
Ticket No:   173274 
Update Of:        0                     Updated By:         0 
Transmit      Date:  6/14/02            Time: 12:15     Op: candice.0 
Original Call Date:  6/14/02            Time: 12:13     Op: candice.0 

Company     : HOME CERTIFICATION SVCS OF LA 
Contact Name: RODNEY                          Contact Phone: (985) 542-9004 
Alt. Contact: MOBILE #                        Alt. Phone   : (985) 687-2047 

Type of Work : BURYING CABLE TV 
Work Done For: CHARTER COMMUNICATIONS 

State: LA       Parish: LIVINGSTON 
Place: WALKER TOWN 
Address:             Street: WALKER RD N 
Nearest Intersecting Street: STATE HWY 1019  

Location:       AT ADDRESS 38630 1/2 WALKER RD N--MARK THE ENTIRE PROP- 
Remarks:  
 

Work to Begin Date:  6/18/02            Time: 12:15 PM 
Mark By       Date:  6/18/02            Time: 12:15 PM 

Ex. Coord: NW Lat: 30 35 46  Lon: -90 52 17  SE Lat: 30 34 18  Lon:  -90 51 51 

Additional Members:  DIX01, TWK01, WTW01 

Send To:  BTR01     Seq No: 113 
Send To:  CHART02   Seq No:  40 



