SEQUENCE NUMBER 1120   CDC = CR4
Texas Excavation Safety System

 @@@@@ @   @ @@@@@ @@@@   @@@@ @@@@@ @   @  @@@@ @   @
 @     @@ @@ @     @   @ @     @     @@  @ @     @   @
 @@@@  @ @ @ @@@@  @@@@  @     @@@@  @ @ @ @      @ @ 
 @     @   @ @     @ @   @  @@ @     @ @ @ @       @  
 @     @   @ @     @  @  @   @ @     @  @@ @       @  
 @@@@@ @   @ @@@@@ @   @  @@@@ @@@@@ @   @  @@@@   @  

*  EMERGENCY  * MESSAGES Sent to  Office(s) as follows : 
ONCOR ELEC-UQ    ATMOS-MIDTX-UQ   ATT/D_(SMP-D)    ATT/D_(SMP-D)    
LONE STAR XCHG

Locate Request No. 073570138

Prepared By TERRANCE M           On 23-DEC-07  At 1810

MapRef :                            Grid: 325300096520B  Footprint: D06

Location:     County: DALLAS  Town: DALLAS

             Address: 3035 WOODWIND LN 

Beginning Work Date 12/23/07 Time of Day: 06:15 pm   Duration: 01 DAYS 

Fax-A-Locate Date          at 

Excavation Type : WATER                         
Nature of Work  : EMER-WATER MAIN REPAIR        

Blasting ? NO           48 Hr Notice ? NO   
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : JEREMY REED
Company Name   : DALLAS WATER UTILITIES
Work by DALLAS WATER UTILITIES      For DALLAS WATER UTILITIES

Person to Contact : ANYONE IN DISPATCH

Phone No.  ( 214 )670-1770 /(    )     ( Hours: 12:00 am/12:00 pm )
Fax No.    ( 214 )670-5487
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: CHANNEL DR 
EMER-WATER MAIN REPAIR  CREW IS EN ROUTE, CUS W/SVC, WATER IS VISIBLE.
 WORKING ON FRONT OF PROPERTY IN ST. AREA MARKED W/BARRICADE.         
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 23,G                         

FaxBack Requested ? YES    Lone Star Xref: 


073570138 to EMAIL ADDRESS at 18:12:42 on SUN, 12/23/07 for CR4 #1120