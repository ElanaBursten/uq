

SEQUENCE NUMBER 1157   CDC = AMP
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ATMOS-MIDTX-UQ   SUDDENLINK       GCEC TECHNOLOGI  GRAY-COLL ELEC 
ATT/D_(SMP-D)    LONE STAR XCHG

Locate Request No. 082122045

Prepared By RC_MEGAN FINE        On 30-JUL-08  At 1041

MapRef :                            Grid: 331630096463A  Footprint: D06

Location:     County: COLLIN  Town: CELINA

             Address: 0 CARTER RANCH RD 

Beginning Work Date 08/01/08 Time of Day: 10:45 am   Duration: 01 HOURS

Fax-A-Locate Date          at 

Excavation Type : POLE/SIGN INSTALLATION        
Nature of Work  : INSTALL SIGN                  

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : MEGAN FINE
Company Name   : FIRST GRAPHIC SERVICES INC
Work by FIRST GRAPHIC SERVICES INC      For FTH

Person to Contact : MEGAN FINE

Phone No.  ( 972 )494-6199 /(    )     ( Hours: 09:00 am/05:00 pm )
Fax No.    ( 972 )494-9399
Email:     MEGAN@FIRSTGRAPHICSERVICES.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: S.H. 2089 (PRESTON RD) 
PLEASE MARK 50'(EAST)X250'(SOUTH) AREA ON SE CORNER OF INTERSECTION, W
E WILL BE INSTALLING 3 FLGAPOLES ALONG THIS.                          
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
MEGAN@FIRSTGRAPHICSERVICES.COM

Map Cross Reference : MAPSCO 469 R COLLIN/GRAYSON         

FaxBack Requested ? YES    Lone Star Xref: 


082122045 to EMAIL ADDRESS at 10:58:39 on WED, 07/30/08 for AMP #1157

