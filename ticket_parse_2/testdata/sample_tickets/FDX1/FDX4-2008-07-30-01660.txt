

SEQUENCE NUMBER 1569   CDC = PMP
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ATMOS-MIDTX-UQ   TIME WARNER      VERI-CARROLLTON  CITY LEWISVILLE
ATMOS-MT-HP-UQ   TX NEW MEX PWR   TW TELECOM       ATT/D_(SMP-D)    
LONE STAR XCHG

Locate Request No. 082123077

Prepared By VALERIE H            On 30-JUL-08  At 1223

MapRef : HP                         Grid: 330030096593C  Footprint: D06

Location:     County: DENTON  Town: LEWISVILLE

             Address: 2470 SUNDERLAND LN 

Beginning Work Date 08/01/08 Time of Day: 12:30 pm   Duration: 02 DAYS 

Fax-A-Locate Date          at 

Excavation Type : FENCES                        
Nature of Work  : FENCE                         

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : DIANE BROWN
Company Name   : SOAK N GROW
Work by SOAK N GROW      For MERCEDES MODEL HOMES

Person to Contact : DIANE BROWN

Phone No.  ( 903 )364-2634 /(    )     ( Hours: 08:00 am/05:00 pm )
Fax No.    ( 903 )364-2912
Email:     DIANNE@SOAKNGROW.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: WALLINGTON WAY 
WORKING ON ENTIRE PROPERTY. DALLAS MAPSCO: 1A,A                       
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
DIANNE@SOAKNGROW.COM

Map Cross Reference : MAPSCO DENTON COOKE: 662,A          

FaxBack Requested ? NO     Lone Star Xref: 


082123077 to EMAIL ADDRESS at 12:28:15 on WED, 07/30/08 for PMP #1569

