SEQUENCE NUMBER 1123   CDC = TM2
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ATT/D_(SMP-D)    ONCOR ELEC-UQ    LONE STAR XCHG

Locate Request No. 073570149

Prepared By TERRANCE M           On 23-DEC-07  At 2007

MapRef :                            Grid: 311230097193A  Footprint: D29

Location:     County: BELL  Town: TROY

             Address: 3488 LUTHER CURTIS RD 

Beginning Work Date 12/27/07 Time of Day: 12:00 am   Duration: 14 DAYS 

Fax-A-Locate Date          at 

Excavation Type : OTHER                         
Nature of Work  : DRILLING HOLES                

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : DAVID CLAY
Company Name   : DAVID JONES CLAY WELDING
Work by DAVID JONES CLAY WELDING      For JOSH CLAY

Person to Contact : DAVID CLAY

Phone No.  ( 979 )836-0880 /( 979 )251-0628 ( Hours: 08:00 am/05:00 pm )
Fax No.    (    )    
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: FRANKLIN RD 
WORKING ON FRONT OF PROPERTY IN MAIN DRIVEWAY TO HOUSE. ADDRESS IS B/W
 FRANKLIN AND VAUGHN ON N SIDE OF LUTHER CURTIS RD. WORKING WITHIN 15F
T OF PROPERTY LINE.                                                   
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


073570149 to EMAIL ADDRESS at 20:09:38 on SUN, 12/23/07 for TM2 #1123