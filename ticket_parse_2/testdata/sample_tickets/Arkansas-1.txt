VALOR      7     AROCS 10/23/02 12:40:00 021023-0678 NORMAL NOGRID 

==============//  AOC TICKET  //==============

TICKET NUMBER--[021023-0678]  
OLD TICKET NUM-[]  

MESSAGE TYPE---[NORMAL] LEAD TIME--[20]
PREPARED-------[10/23/02]  AT  [1240]  BY  [JRUTLEDGE]

CONTRACTOR--[SOUTHWEST ARKANSAS ELECTRIC COOPERATIVE]   CALLER--[JUDY RUTLEDGE]
ADDRESS-----[PO BOX 1807]
CITY--------[TEXARKANA]   STATE--[AR]   ZIP--[75504]
CALL BACK---[CELL 903-748-0088 OR OFFICE 870-772-2743]   PHONE--[(870) 772-2743]
CONTACT-----[MIKE POLLARD]   PHONE--[(870) 772-2743]
CONTACT FAX-[(870) 773-2408]

WORK TO BEGIN--[10/25/02] AT [1245]      STATE--[AR]
COUNTY---[MILLER]  PLACE--[TEXARKANA]

ADDRESS--[]   STREET--[][SUMMIT][DR][]
NEAREST INTERSECTION--------[DUDES DR]
LATITUDE--[33.4173611111111]    LONGITUDE--[-93.9572777777778]
SECONDARY LATITUDE--[0]    SECONDARY LONGITUDE--[0]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[** TRENCHING 100' UNDERGROUND SERVICE**                                       ]
[CITY OF TEXARKANA -  INT SUMMIT DR & DUDES DR - APPX 170' S - E SIDE OF RD    ]
[- N E SIDE OF PPTY                                                            ]

WORK TYPE--[ELECTRIC, INSTALL SERVICE]  DONE FOR--[CHARLES SANTIFER]
EXTENT-----[4 HOURS]

EXPLOSIVES--[N]    WHITE PAINT--[Y]    GRID GIVEN--[N]

MAP REF.--[]
GRIDS-----
          [                 ] 

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME                      
  ---------- --------------------------   ---------- --------------------------
  REAFC091   RELIANT ENERGY ARKLA - ...   TXARKANA   TEXARKANA WATER UTILITIES 
  VALOR      VALOR TELECOMMUNICATION... 




VALOR      8     AROCS 10/23/02 13:34:00 021023-0709 NORMAL NOGRID 

==============//  AOC TICKET  //==============

TICKET NUMBER--[021023-0709]  
OLD TICKET NUM-[]  

MESSAGE TYPE---[NORMAL] LEAD TIME--[20]
PREPARED-------[10/23/02]  AT  [1334]  BY  [JWARRIOR]

CONTRACTOR--[CENTERPOINT ENERGY ARKLA]   CALLER--[RON ELKINS]
ADDRESS-----[2802 N STATE LINE]
CITY--------[TEXARKANA]   STATE--[AR]   ZIP--[71854]
CALL BACK---[MOBILE]   PHONE--[(870) 779-6332]
CONTACT-----[RON ELKINS]   PHONE--[(903) 824-5921]
CONTACT FAX-[(870) 235-0213]

WORK TO BEGIN--[10/25/02] AT [1345]      STATE--[AR]
COUNTY---[MILLER]  PLACE--[TEXARKANA]

ADDRESS--[812]   STREET--[][COUCH][ST][]
NEAREST INTERSECTION--------[MARIETTA ST]
LATITUDE--[33.429504962449]    LONGITUDE--[-94.0214414650132]
SECONDARY LATITUDE--[0]    SECONDARY LONGITUDE--[0]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[APPX 200' N OF THE INT - MARK FRONT OF PPTY & BOTH SIDES OF RD                ]

WORK TYPE--[GAS, RETIRE SERVICE]  DONE FOR--[CENTERPOINT ENERGY ARKLA]
EXTENT-----[1 HR]

EXPLOSIVES--[N]    WHITE PAINT--[Y]    GRID GIVEN--[N]

MAP REF.--[]
GRIDS-----
          [                 ] 

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME                      
  ---------- --------------------------   ---------- --------------------------
  CABLE      CABLE ONE                    SWPC03     SWEPC0 -TEXARKANA         
  TXARKANA   TEXARKANA WATER UTILITIES    VALOR      VALOR TELECOMMUNICATION...




VALOR      9     AROCS 10/23/02 13:36:00 021023-0711 NORMAL NOGRID 

==============//  AOC TICKET  //==============

TICKET NUMBER--[021023-0711]  
OLD TICKET NUM-[]  

MESSAGE TYPE---[NORMAL] LEAD TIME--[20]
PREPARED-------[10/23/02]  AT  [1336]  BY  [JWARRIOR]

CONTRACTOR--[CENTERPOINT ENERGY ARKLA]   CALLER--[RON ELKINS]
ADDRESS-----[2802 N STATE LINE]
CITY--------[TEXARKANA]   STATE--[AR]   ZIP--[71854]
CALL BACK---[MOBILE]   PHONE--[(870) 779-6332]
CONTACT-----[RON ELKINS]   PHONE--[(903) 824-5921]
CONTACT FAX-[(870) 235-0213]

WORK TO BEGIN--[10/25/02] AT [1345]      STATE--[AR]
COUNTY---[MILLER]  PLACE--[TEXARKANA]

ADDRESS--[2504]   STREET--[][DELAWARE][ST][]
NEAREST INTERSECTION--------[RAY ST]
LATITUDE--[33.4279025460302]    LONGITUDE--[-94.0162590631475]
SECONDARY LATITUDE--[0]    SECONDARY LONGITUDE--[0]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[APPX 100' E OF THE INT - MARK FRONT OF PPTY & BOTH SIDES OF RD                ]

WORK TYPE--[GAS, RETIRE SERVICE]  DONE FOR--[CENTERPOINT ENERGY ARKLA]
EXTENT-----[1 HR]

EXPLOSIVES--[N]    WHITE PAINT--[Y]    GRID GIVEN--[N]

MAP REF.--[]
GRIDS-----
          [                 ] 

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME                      
  ---------- --------------------------   ---------- --------------------------
  CABLE      CABLE ONE                    SWPC03     SWEPC0 -TEXARKANA         
  TXARKANA   TEXARKANA WATER UTILITIES    VALOR      VALOR TELECOMMUNICATION...




VALOR      10    AROCS 10/23/02 13:37:00 021023-0714 NORMAL NOGRID 

==============//  AOC TICKET  //==============

TICKET NUMBER--[021023-0714]  
OLD TICKET NUM-[]  

MESSAGE TYPE---[NORMAL] LEAD TIME--[20]
PREPARED-------[10/23/02]  AT  [1337]  BY  [JWARRIOR]

CONTRACTOR--[CENTERPOINT ENERGY ARKLA]   CALLER--[RON ELKINS]
ADDRESS-----[2802 N STATE LINE]
CITY--------[TEXARKANA]   STATE--[AR]   ZIP--[71854]
CALL BACK---[MOBILE]   PHONE--[(870) 779-6332]
CONTACT-----[RON ELKINS]   PHONE--[(903) 824-5921]
CONTACT FAX-[(870) 235-0213]

WORK TO BEGIN--[10/25/02] AT [1345]      STATE--[AR]
COUNTY---[MILLER]  PLACE--[TEXARKANA]

ADDRESS--[315]   STREET--[][MARY][ST][]
NEAREST INTERSECTION--------[EDWARD ST]
LATITUDE--[33.41779272897]    LONGITUDE--[-94.0269197736467]
SECONDARY LATITUDE--[0]    SECONDARY LONGITUDE--[0]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[APPX 100' S OF THE NORTHERN MOST INT - MARK FRONT OF PPTY & BOTH SIDES OF     ]
[RD                                                                            ]

WORK TYPE--[GAS, RETIRE SERVICE]  DONE FOR--[CENTERPOINT ENERGY ARKLA]
EXTENT-----[1 HR]

EXPLOSIVES--[N]    WHITE PAINT--[Y]    GRID GIVEN--[N]

MAP REF.--[]
GRIDS-----
          [                 ] 

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME                      
  ---------- --------------------------   ---------- --------------------------
  CABLE      CABLE ONE                    SWPC03     SWEPC0 -TEXARKANA         
  TXARKANA   TEXARKANA WATER UTILITIES    VALOR      VALOR TELECOMMUNICATION...




VALOR      11    AROCS 10/23/02 13:38:00 021023-0717 NORMAL NOGRID 

==============//  AOC TICKET  //==============

TICKET NUMBER--[021023-0717]  
OLD TICKET NUM-[]  

MESSAGE TYPE---[NORMAL] LEAD TIME--[20]
PREPARED-------[10/23/02]  AT  [1338]  BY  [JWARRIOR]

CONTRACTOR--[CENTERPOINT ENERGY ARKLA]   CALLER--[RON ELKINS]
ADDRESS-----[2802 N STATE LINE]
CITY--------[TEXARKANA]   STATE--[AR]   ZIP--[71854]
CALL BACK---[MOBILE]   PHONE--[(870) 779-6332]
CONTACT-----[RON ELKINS]   PHONE--[(903) 824-5921]
CONTACT FAX-[(870) 235-0213]

WORK TO BEGIN--[10/25/02] AT [1345]      STATE--[AR]
COUNTY---[MILLER]  PLACE--[TEXARKANA]

ADDRESS--[704]   STREET--[][DRAUGHN][ST][]
NEAREST INTERSECTION--------[HAYS ST]
LATITUDE--[33.4142095495724]    LONGITUDE--[-94.0234648063702]
SECONDARY LATITUDE--[0]    SECONDARY LONGITUDE--[0]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[APPX 100' S OF THE INT - MARK FRONT OF PPTY & BOTH SIDES OF RD                ]

WORK TYPE--[GAS, RETIRE SERVICE]  DONE FOR--[CENTERPOINT ENERGY ARKLA]
EXTENT-----[1 HR]

EXPLOSIVES--[N]    WHITE PAINT--[Y]    GRID GIVEN--[N]

MAP REF.--[]
GRIDS-----
          [                 ] 

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME                      
  ---------- --------------------------   ---------- --------------------------
  CABLE      CABLE ONE                    SWPC03     SWEPC0 -TEXARKANA         
  TXARKANA   TEXARKANA WATER UTILITIES    VALOR      VALOR TELECOMMUNICATION...




+++PxR,
