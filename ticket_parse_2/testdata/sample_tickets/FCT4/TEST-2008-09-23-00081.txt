
GWI91  00005 GAUPC 09/23/08 12:46:51 08198-205-005-000 DAMAGE       
UNDERGROUND NOTIFICATION             
Notice : 08198-205-005 Date: 08/19/08  Time: 16:34  Revision: 000 

State : GA County: GWINNETT      Place: DULUTH                                  
Addr  : From: 3400   To:        Name:    SUMMIT RIDGE                   PKWY    
Near  : Name:    PLEASANT HILL                  RD  

Subdivision:                                         
Locate:  ENTIRE PROPERTY                                                       

Grids       : 3359D8409A 3359C8409A 3359D8410D 3359C8410D 
Work type   : INSTALL BUSHES, PLANTS, TREES, AND IRRIGATION                       

Start date: 08/19/08 Time: 00:00 Hrs notc : 000
Legal day : 08/19/08 Time: 00:00 Good thru: 08/20/08 Restake by: 08/19/08
RespondBy : 08/19/08 Time: 00:00 Duration : 1 YEAR     Priority: 6
Done for  : UTILITIES PROTECTION CENTER, INC        
Crew on Site: N White-lined: Y Blasting: N  Boring: Y

Remarks : TEST TICKET, WILL ONLY BE SENT TO DESTINATIONS REQUESTING TEST TICKE
        : TS                                                                  
        : DAMAGE TO SERVICE/DROP  EXTENT: CUT **SERVICE OUT
        : *** WILL BORE Road                          

Company : UPC                                       Type: CONT                
Co addr : 3400 SUMMIT RIDGE PKWY                   
City    : DULUTH                         State   : GA Zip: 30096              
Caller  : CC EXCAVATOR                   Phone   :  404-444-3606              
Fax     :                                Alt. Ph.:  770-476-6042              
Email   : IWEATHERS@GAUPC.COM                                                 
Contact :                                                           

Submitted date: 08/19/08  Time: 16:34  Oper: 209
Mbrs : AGLN01 GWI91 JCK70 OGP811 COMCEN GP813 GP814 GP814 
-------------------------------------------------------------------------------



