
NOTICE OF INTENT TO EXCAVATE      FTTP                          NO RESPONSE 
Ticket No: 9596618 
Transmit      Date: 12/11/09      Time: 12:00 AM           Op: webusr 
Original Call Date: 11/30/09      Time: 04:54 PM           Op: webusr 
Work to Begin Date: 12/02/09      Time: 05:00 PM 

Place: ROCKVILLE 
Address: 9325        Street: FALLS CHAPEL WAY 
Nearest Intersecting Street: BENTRIDGE AVE 

Type of Work: VZN/8A14611/07761/5845C 
Extent of Work: LOCATE FROM THE ABOVE ADD CONT ON FALLS CHAPEL WAY TO ITERS
: BENTRIDGE AVE LOCATE ALL SERV FRONT EASMENTS AND CROSS AREA WHITELINED 
Remarks:  

Company      : GARCIA CABLE INC 
Contact Name : LYDIA MARTINEZ                 Fax          : (410)489-6591 
Contact Phone: (703)928-8302                  Ext          :  
Alt. Contact : ARACELY MARTINEZ               Alt. Phone   : (703)928-8302 
Work Being Done For: VZN/FTTP/S&N COMM 
State: MD              County: MONTGOMERY 
MPG:  Y 
Caller    Provided:  Map Name: MONT  Map#: 28   Grid Cells: G12 
Computer Generated:  Map Name: *MORE Map#: 5283 Grid Cells: K2,K3 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.0552550Lon: -77.1910030 SE Lat: 39.0498059Lon: -77.1843422 
Explosives: N 
PEP05      TRU01      VMG        WSS01 
Send To: WGL06     Seq No: 0001   Map Ref:  

