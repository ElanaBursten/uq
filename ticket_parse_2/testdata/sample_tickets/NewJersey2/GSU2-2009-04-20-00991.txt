
GSUPLS091102459-00	JC	2009/04/20 18:46:42	00970

 New Jersey One Call System        SEQUENCE NUMBER 1049   CDC = GPE

Transmit:  Date: 04/20/09   At: 1845

*** R O U T I N E         *** Request No.: 091102459

Operators Notified:
    NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON   |ECSM/ GPE=/JCP&L      |UTQ/ 
    JTM=/JACKSON TWP MUA/ 

Start Date/Time:    04/24/09   At 0700   Expiration Date: 06/23/09

Location Information:
   County: OCEAN     Municipality: JACKSON
   Subdivision/Community: 
   Street:               11 BEATRICE LN
   Nearest Intersection: PERRINEVILLE RD 
   Other Intersection:   SARAH CT
   Lat/Long: 
   Type of Work :        INSTALL IRRIGATION
   Extent of Work: CURB TO ENTIRE PROPERTY                  DEPTH: 1 FOOT
   Remarks:

   Working For:  MR SICIAS
   Address:      11 BEATRICE LN
   City:         JACKSON, NJ  08527
   Phone:        732-833-7077

Excavator Information:
   Caller:       DENISE CONOVER          
   Phone:        732-367-4408            

   Excavator:    NJ LAWN & IRRIGATION
   Address:      P.O. BOX 610
   City:         JACKSON, NJ  08527
   Phone:        732-367-4408            Fax:  732-367-4622
   Cellular:     
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 06:46PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102459        0970  GSUPLS091102459-00   Regular Notice   2009/04/24 07:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
