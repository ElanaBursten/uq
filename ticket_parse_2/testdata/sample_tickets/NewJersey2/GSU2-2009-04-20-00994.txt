
GSUPLS091102457-00	JC	2009/04/20 18:46:42	00968

 New Jersey One Call System        SEQUENCE NUMBER 1047   CDC = GPS

Transmit:  Date: 04/20/09   At: 1845

*** R O U T I N E         *** Request No.: 091102457

Operators Notified:
    CC3=/COMCST-CENT|UTQ/ ESW=/E WINDSOR MUA  / P31=/PSE&G GAS DIV  / 
    BAN=/VERIZON   |ECSM/ GPS=/JCP&L      |UTQ/ 

Start Date/Time:    04/24/09   At 0700   Expiration Date: 06/23/09

Location Information:
   County: MERCER     Municipality: EAST WINDSOR
   Subdivision/Community: 
   Street:               16 COPERNICUS CT
   Nearest Intersection: HUBBLE BLVD 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        INSTALL CONDUIT
   Extent of Work: CURB TO ENTIRE PROPERTY                  DEPTH: 5FT
   Remarks:

   Working For:  VERIZON FIOS
   Address:      183 BROAD ST
   City:         RED BANK, NJ  07109
   Phone:        609-351-2073

Excavator Information:
   Caller:       JACK WOOD               
   Phone:        609-584-1100            

   Excavator:    WATERS & BUGBEE
   Address:      75 S GOLD DR
   City:         HAMILTON, NJ  08691
   Phone:        609-584-1100            Fax:  609-245-2014
   Cellular:     
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 06:46PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102457        0968  GSUPLS091102457-00   Regular Notice   2009/04/24 07:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
