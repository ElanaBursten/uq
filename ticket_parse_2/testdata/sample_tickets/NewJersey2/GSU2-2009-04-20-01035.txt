
GSUPLS091102668-00	JC	2009/04/20 21:43:30	01013

 New Jersey One Call System        SEQUENCE NUMBER 1099   CDC = GPC

Transmit:  Date: 04/20/09   At: 2142

*** R O U T I N E         *** Request No.: 091102668

Operators Notified:
    STF=/STAFFORD MUA   / AE1=/ATLNTC ELEC|UTQ/ NJN=/NJ NATL GAS|UTQ/ 
    BAN=/VERIZON   |ECSM/ GPC=/JCP&L      |UTQ/ CCC=/COMCST-TMSR|UTQ/ 

Start Date/Time:    04/24/09   At 0800   Expiration Date: 06/23/09

Location Information:
   County: OCEAN     Municipality: STAFFORD
   Subdivision/Community: 
   Street:               124 - 146 MARY BELL RD
   Nearest Intersection: KRITTER CT 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        INSTL CATV
   Extent of Work: CURB TO CURB,CURB TO 20FT BEHIND CURB    DEPTH: 3FT
   Remarks:
     CONSECUTIVE EVEN 124-146

   Working For:  JV INSTALLATIONS
   Address:      54 11TH AVE
   City:         TOMS RIVER, NJ  08753
   Phone:        732-581-0133

Excavator Information:
   Caller:       GREG MILLER             
   Phone:        732-920-5732            

   Excavator:    MILLER UNDERGROUND
   Address:      540 DOROTHY PL
   City:         BRICK, NJ  08723
   Phone:        732-920-5732            Fax:  
   Cellular:     908-278-1509
   Email:        
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2009/04/20 09:43PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
091102668        1013  GSUPLS091102668-00   Regular Notice   2009/04/24 08:00AM


-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.
