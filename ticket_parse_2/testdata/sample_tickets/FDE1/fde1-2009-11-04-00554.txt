
NOTICE OF INTENT TO EXCAVATE        
Ticket No: 102780036 
Transmit      Date: 11/04/09      Time: 02:22 PM           Op: witrai 
Original Call Date: 11/02/09      Time: 10:56 AM           Op: witrai 
Work to Begin Date: 11/05/09      Time: 07:00 AM 

Place: GLASGOW 
Address: 21          Street: RED SUNSET DR 
Nearest Intersecting Street: HAWTHORNE DR 

Type of Work: INSTALLING A FENCE 
Extent of Work: MARK THE ENTIRE PROPERTY 
Remarks: LEGALS NOT PROVIDED FOR THIS LOCATE REQUEST 

Company      : JERRY LITTLE 
Contact Name : HOMEOWNER                      Fax          :  
Contact Phone: (302)684-3333                  Ext          :  
Caller Address: PO BOX 159 
                HAROLD, DE 19951 
Email Address:  jerry@brakewaterfenceanddeck.com 
Alt. Contact : CELL                           Alt. Phone   : (302)245-4290 

Work Being Done For: JERRY LITTLE 
State: DE              County: NEW CASTLE 
MPG:  N 
Caller    Provided:  Map Name: NEWC  Map#: 14   Grid Cells: E9 
Computer Generated:  Map Name: NEWC  Map#: 14   Grid Cells: E9 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.6056142Lon: -75.7625949 SE Lat: 39.6001023Lon: -75.7558377 
Explosives: N 
ARTW34     ATT32      CU04NEW    DPU104     DPUG07     LTC04      NCSS01 
 SDHY23     TNC01 
Send To: HRCA20    Seq No: 0001   Map Ref:  
