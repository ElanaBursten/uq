
UQSTSO 00868A USAS 02/18/09 10:59:46 A90490654-00A NORM NEW GRID

Ticket : A90490654 Date: 02/18/09 Time: 10:57 Oper: LAC Chan: 100
Old Tkt: A90490654 Date: 02/18/09 Time: 10:59 Oper: LAC Revision: 00A
State: CA County: VENTURA              Place: CAMARILLO
Locat: * MARK WITH CHALK BASE PAINT & ONLY  & FLAGS AS NEEDED ****** DO NOT MARK
     : ON PAVER STONES *****
Address: 5363 SPINDRIFT CT
X/ST 1 : SUMMERFIELD ST
Grids: 0525A0113                                            Delineated: Y
Lat/Long  : 34.233413/-118.998753 34.233200/-118.996608
          : 34.232546/-118.998839 34.232333/-118.996694
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N

Re-Mark: N

Work : INSTALL FIBER OPTICS
Work date: 02/20/09 Time: 11:00 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                Permit   : NOT REQUIRED
Done for : VERIZON WO# OC9999341

Company: VERIZON                        Caller: RICK EDWARDS
Address: 201 FLYNN RD
City&St: CAMARILLO, CA                  Zip: 93012
Phone: 805-445-8129 Ext:      Call back: 7AM-3:30PM
Formn: ALONZO               Phone: 805-217-9170
Mbrs : CAM35  CAM35A CWD01  MCISOCAL      SCG4UR USCE39 UTWCNW39      UVZCAM
UVZMDV2

