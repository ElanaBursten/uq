
UQSTSO 01066A USAS 08/21/09 13:56:44 A92110291-02A REMK REMK GRID

Ticket : A92110291 Date: 08/21/09 Time: 13:55 Oper: NHR Chan: 100
Old Tkt: A91530312 Date: 06/30/09 Time: 15:03 Oper: MMD Revision: 02A
State: CA County: SAN DIEGO            Place: SAN DIEGO
Locat: BOTH SIDES OF EL CAMINO REAL FRM A POINT APPROX 1 MILE N OF HALF MILE DR
     : CONT N/E TO SAN DIEGUITO RD THEN FRM THAT POINT GO E ON BOTH SIDES OF SAN
     : DIEGUITO FOR APPROX 500FT
Address: EL CAMINO REAL
X/ST 1 : HALF MILE DR
Grids: 1187J024     1187J032     1188A0234    1188A031      Delineated: Y
Lat/Long  : 32.975093/-117.238427 32.976481/-117.229418
          : 32.973299/-117.238151 32.974687/-117.229141
Caller GPS:

Boring: N  Explosives: N  Vacuum: N

Re-Mark: Y  Re-Mark Type: TELEPHONE

Work : INSTALL UTILS
Work date: 08/21/09 Time: 13:56 Hrs notc: 000 Work hrs: 000 Priority: 2
Instruct : REQUEST REMARKS        Permit   : 2007901579
Done for : PARDEE CONST

Company: BASILE CONSTRUCTION            Caller: JENNIFER
Address: 7840 DUNBROOK ROAD
City&St: SAN DIEGO, CA                  Zip: 92126      Fax: 858-586-7809
Phone: 858-586-7800 Ext:      Call back: 7-4
Formn: STEVE                Phone: 619-813-7809
Email: JEN@BASILE-DIG.COM

                                  COMMENTS

REF EXP TICKET A091530312, UPDATE ONLY-WORK CONT PER JOLIE--[LAB 07/30/09 09:05]
**RESEND**REQUEST REMARKS FROM TELEPHONE-WORK CONT  REQ ATT TO REMARK LINES ASAP
AND CALL FORMAN GABE TO CONFIRM ETA AT 619-813-7806 PER JOLINDA--[LMC 08/19/09
08:32]
**RESEND**REQUEST REMARKS FROM TELEPHONE-WORK CONT REQUESTING REMARKS FROM AT&T
ASAP AS PREVIOUSLY REQUESTED PLEASE CALL 619-813-7806 WITH ETA PER JOLINDA--[NHR
08/21/09 13:56]

Mbrs : ATTD28SD      KMEPFUEL02    NEXTG  SDG01  SDG02  SDG09  SFI01  SND01
SND02  TWCSD  UATLSD
