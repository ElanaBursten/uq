
TESTDC     8     AROCS 12/30/08 09:02:00 081230-0197 NORMAL NOGRID 

==============//  AOC TICKET  //==============

TICKET NUMBER--[081230-0197]  
OLD TICKET NUM-[]  

MESSAGE TYPE---[NORMAL] LEAD TIME--[20]
PREPARED-------[12/30/08]  AT  [0902]  BY  [ASHAMBLIN]

CONTRACTOR--[ATKINS WATERWORKS]   CALLER--[WILLIE HARVILLE]
ADDRESS-----[PO BOX 128]
CITY--------[ATKINS]   STATE--[AR]   ZIP--[72823]
CALL BACK---[SAME]   PHONE--[(479) 641-7853]
COMPANY FAX-[(479) 641-7052]
CONTACT-----[WILLIE HARVILLE]   PHONE--[(479) 641-7853]
EMAIL-------[]
CONTACT FAX-[(479) 641-7052]

WORK TO BEGIN--[01/02/09] AT [0915]      STATE--[AR]
COUNTY---[POPE]  PLACE--[RURAL]

ADDRESS--[]   STREET--[SE][4TH][][]
NEAREST INTERSECTION--------[AUSTIN SCHOOL CROSSING RD]
LATITUDE--[35.2377362283302]    LONGITUDE--[-92.9076511097714]
SECONDARY LATITUDE--[0]    SECONDARY LONGITUDE--[0]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[MAIN                                                                          ]
[                                                                              ]
[RURAL OF ATKINS - WORKING AT INT                                              ]

WORK TYPE--[WATER, REPAIR LEAK]  DONE FOR--[ATKINS WATERWORKS]
EXTENT-----[1 DAY]

EXPLOSIVES--[N]    WHITE PAINT--[Y]    GRID GIVEN--[N]
DIRECTIONAL BORING--[N]

MAP REF.--[]
GRIDS-----
          [                 ] 

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME                      
  ---------- --------------------------   ---------- --------------------------
  CT90RLVL   CENTURYTEL OF RUSSELLVILLE   TRICNTYW   TRI-COUNTY REGIONAL WAT...




