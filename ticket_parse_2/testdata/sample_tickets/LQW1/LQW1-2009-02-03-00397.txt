
UULC2009020300379	360754	2009/02/03 12:45:59	00390

UULC 
Ticket No:  9022706               2 FULL BUSINESS 
Send To: QLNWA31    Seq No:   13  Map Ref:  

Transmit      Date:  2/03/09   Time: 10:44 am    Op: ormary 
Original Call Date:  2/03/09   Time: 10:27 am    Op: webusr 
Work to Begin Date:  2/06/09   Time: 12:00 am 

State: WA            County: THURSTON                Place: OLYMPIA 
Address: 4404        Street: 31ST AVE NE 
Nearest Intersecting Street: SLEATER KINNEY RD NE 

Twp: 18N   Rng: 1W    Sect-Qtr: 5 
Twp: 18N   Rng: 1W    Sect-Qtr: 4-SW-NW,5 
Legal Given:  

Type of Work: INSTALLING TELEPHONE SERVICE 
Location of Work: INTERSECTING STREET IS .1MILE EAST FROM THE ABOVE ADD.
: LOCATE ON NORTH SIDE OF 31ST FROM TEL PED 10 NORTH APROX 150FT TO POWER
: METER. LOCATE ALL EASMENTS AND ROW AREA IS MARKED IN WHITE PAINT. 

Remarks:  
:  

Company     : SOUTH BAY EXCAVATING 
Contact Name: TANYA MORRIS                     Phone: (360)866-4454 
Cont. Email : TANYA@SOUTHBAYINC.NET 
Alt. Contact: NEAL MORRIS                      Phone: (360)701-8360 
Contact Fax :  
Work Being Done For: QWEST 
Additional Members:  
CC7711     PSEELC47   PSEGAS42

