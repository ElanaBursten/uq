
New Jersey One Call System        SEQUENCE NUMBER 0657   CDC = SJG
 
Transmit:  Date: 05/30/02   At: 1123
 
*** R O U T I N E         *** Request No.: 021500760
 
Operators Notified:
    SCR=/SALEM CNTY ROAD/ SCV=/COMCST CATV-VNL/ AE1=/CONECTV PWR|UTQ/ 
    SJG=/S JERSY GAS|UTQ/ BAN=/VERIZON    |CLS/ 
 
Location Information:
   County: SALEM     Municipality: ALLOWAY
   Subdivision/Community: 
   Street:               0 ALLOWAY WOODSTOWN RD
   Nearest Intersection: LAKEVIEW DR 
   Other Intersection:   
   Type of Work :        REPL PL
   Extent of Work: 50FT RADIUS OF PL#S13023                 DEPTH: 8FT
 
   Start Date/Time:    06/05/02   At 0700   Expiration Date: 07/12/02
   Remarks:
     EXPIRATION DATE 07/12/02
     M/O APPX 553FT N OF INTER
 
   Working For:  CONNECTIV POWER
   Address:      428 ELLIS ST
   City:         GLASSBORO, NJ  08028
   Phone:        856-863-7943
   Contact:      RICH SIMONINI           Title: 
 
Excavator Information:
   Caller:       PENNY PONASELLO         Title: ADMIN ASST
   Phone:        609-545-0380            
 
   Excavator:    J. WILLIAM FOLEY
   Address:      1300 STAGE COACH RD
   City:         OCEANVIEW, NJ  08230
   Phone:        609-545-0380            Fax:  609-545-0382
   Contact:      MICHELLE RUGGERI        Title: ANALYST
   Phone:        609-545-0380            Best Time: 0930-1600
   Cellular:     
 
   Alternate Field Contact:
   Name:         CHARLES GALLAGHER       Title: GEN FOREMN
   Phone:        609-545-0380            Fax: 
   Cellular:     609-209-5853            Best Time: 0930-1600
End Request
 

