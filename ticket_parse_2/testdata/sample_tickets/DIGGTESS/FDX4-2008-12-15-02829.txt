

DIG-TESS Locate Request For AMP
----------------------------------------------------------------------------
Ticket Number:    083502993         Old Ticket:       083431647         
Priority:         Normal            By:               Ashley H          
Source:           Fax               Hours Notice:     48                
Type:             Update            Date:             12/15/2008 12:58:09 PM
Sequence:         905                                                   
Map Reference:                                                          

Company Information
----------------------------------------------------------------------------
RKM UTILITY SVC INC.                Type:             Contractor        
1544 VALWOOD PKY - SUITE 100        Contact:          SHANE HUFFMAN     
CARROLLTON, TX 75006                Caller:           HAROLD SWANN      
Phone:            (972) 241-2621    Caller Phone:     (972) 241-2621    
Fax:              (972) 241-2624    Callback:         0800 - 1700       
Alt Contact:      (214) 236-5658                                        
Caller Email:     CALLER DID NOT PROVIDE EMAIL                          

Work Information
----------------------------------------------------------------------------
State:            TX                Work Date:        12/17/08 at 1300  
County:           DENTON            Type:             WATER             
Place:            LEWISVILLE        Done For:         CITY OF LEWISVILLE
Street:           0 CHERRY HILL LN                                      
Intersection:     OLD ORCHARD LN                                        
Nature of Work:   WATER LINE REPLACEMENT                                
Explosives:       No                Deeper Than 16":  Yes               
White Lined:      No                Duration:         90 DAYS           
Mapsco:           MAPSCO 650,F                                          

Remarks
----------------------------------------------------------------------------
UPDATE & REMARK-083431647//                                                 
START LOC AT THE INTER OF CHERRY HILL LN & OLD ORCHARD LN & CONT LOC GOING W
LOC BOTH SIDES OF CHERRY HILL LN FOR APPX 1,500 FT LOCATING FROM   PROP LINE
TO PROP LINE & A 100 FT RADIUS OF ALL INTERSECTIONS. MAPSCO:650,F           


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
AMP   ATMOS-MIDTX-UTILIQUEST (METROPLEX)                No                  
CLG   TIME WARNER CABLE                                 No                  
DIS   DENTON INDEPENDENT SCHOOL DISTRICT (TOCS)   (TO...No                  
GTH   VERIZON - TX - CARROLLTON DISTRICT                No                  
LEW   CITY OF LEWISVILLE                                No                  
TNE   TEXAS NEW MEXICO POWER COMPANY                    No                  
U09   ATT/D = DISTRIBUTION CABLE (FORMERLY SBC)         No                  


Location
----------------------------------------------------------------------------
Latitude:         33.0466688098042  Longitude:        -97.0322616620328 
Second Latitude:  33.0431283358159  Second Longitude: -97.0254277238692

