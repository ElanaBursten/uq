

DIG-TESS Locate Request For AMP
----------------------------------------------------------------------------
Ticket Number:    083371804         Old Ticket:                         
Priority:         Normal            By:               Rob C             
Source:           Email             Hours Notice:     48                
Type:             Normal            Date:             12/2/2008 10:33:40 AM
Sequence:         5                                                     
Map Reference:                                                          

Company Information
----------------------------------------------------------------------------
VERIZON                             Type:             Contractor        
7979 N BELTLINE RD                  Contact:          CAROLYN ALLEN     
IRVING, TX 75062                    Caller:           CAROLYN           
Phone:            (800) 609-1603    Caller Phone:     (800) 609-1603    
Fax:              (845) 732-4846    Callback:         0800 - 1600       
Alt Contact:      (800) 609-1603                                        
Caller Email:     NWSADMIN@VERIZON.COM                                  

Work Information
----------------------------------------------------------------------------
State:            TX                Work Date:        12/04/08 at 1045  
County:           DALLAS            Type:             TELEPHONE         
Place:            IRVING            Done For:         VERIZON           
Street:           0 RIDGEVIEW LN                                        
Intersection:     NOT PROVIDED                                          
Nature of Work:   FIBER DROP                                            
Explosives:       No                Deeper Than 16":  No                
White Lined:      No                Duration:         01 DAYS           
Mapsco:           MAPSCO 31A,B                                          

Remarks
----------------------------------------------------------------------------
WORK DATE: 2 WORKING DAY NOTICE                                             
JOB I.D. E2355540 ***LOCATE ALL PROPERTIES FROM 3202 THRU 3214 RIDGEVIEW LN 
DROP TERM CON TYPE  APC  DROP TERM GPS LAT/LONG  32.851201 / -96.988349     
***LOCATE ENTIRE PROPERTY WITHIN 48 HOURS***LOCATE BOTH SIDES OF THE        
STREET***DIRECTIONAL BORING***NO LOCATES FOR VERIZON FACILITIES***FOR ALL   
INQUIRIES PLEASE CALL:    800-609-1605 (OPT. #2)                            
973745.XML                                                                  


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
AMP   ATMOS-MIDTX-UTILIQUEST (METROPLEX)                No                  
CLG   TIME WARNER CABLE                                 No                  
CQR   TIME WARNER CABLE                                 No                  
GTL   VERIZON - TX - KELLER GRAPEVINE DISTRICT (METRO...No                  
IR4   ONCOR ELECTRIC DISTRIBUTION-SMP (IRVING)          No                  
NLT   XO COMMUNICATIONS                                 No                  
PMP   ATMOS-MIDTX-PIPELINE-UQ (METROPLEX)               No                  
U09   ATT/D = DISTRIBUTION CABLE (FORMERLY SBC)         No                  
WL1   LEVEL 3 COMMUNICATIONS                            No                  


Location
----------------------------------------------------------------------------
Latitude:         32.8517724285708  Longitude:        -96.9894442380968 
Second Latitude:  32.8486295714261  Second Longitude: -96.9879204285721

