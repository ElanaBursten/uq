

DIG-TESS Locate Request For PMP
----------------------------------------------------------------------------
Ticket Number:    090058261         Old Ticket:                         
Priority:         NORMAL            By:               Administrator     
Source:           TOCS              Hours Notice:     18                
Type:             NORMAL            Date:             1/5/2009 9:27:12 AM
Sequence:         76                                                    
Map Reference:                                                          

Company Information
----------------------------------------------------------------------------
TEXAS ELECTRIC                      Type:             Contractor        
PO BOX 1207                         Contact:          BRUCE BISHOP      
HILLSBORO, TX 76645                 Caller:           BRUCE BISHOP      
Phone:            (608) 212-3784    Caller Phone:     (608) 212-3784    
Fax:                                Callback:         0700 - 1700       
Alt Contact:                                                            
Caller Email:     BBISHOP@INTERCON-CONST.COM                            

Work Information
----------------------------------------------------------------------------
State:            TX                Work Date:        01/07/09 at 0930  
County:           DALLAS            Type:             Other             
Place:            DE SOTO           Done For:         ONCOR             
Street:           413 CANTERBURY                                        
Intersection:     CHESTNUT                                              
Nature of Work:   ELEC PRIMARY REPL                                     
Explosives:       No                Deeper Than 16":  Yes               
White Lined:      No                Duration:         01 WEEK           
Mapsco:           MAPSCO 83,Q                                           

Remarks
----------------------------------------------------------------------------
MARK IN ALLEY IN REAR OF ABOVE ADDRESS & THEN GO APPX 700FT N & W      ALONG
ALLEY WAY.                                                       MARK BTWN  
TRANSFORMER 6250 & GO TO TRANSFORMER 3815.                   CALLER IS      
REQUESTING TO MEET WITH UTIL ON WEDNESDAY 1-7-09 0930.       CALL IF UNABLE 
TO ATTEND.                                                                  
FAX #: NOT PROVIDED                                                         
BBISHOP@INTERCON-CONST.COM                                                  


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
AMP   ATMOS-MIDTX-UTILIQUEST (METROPLEX)                No                  
CLG   TIME WARNER CABLE                                 No                  
GP8   ONCOR ELECTRIC DISTRIBUTION-SMP (GRAND PRAIRIE)   No                  
HIL   HILCO ELECTRIC COOPERATIVE INC.                   No                  
PMP   ATMOS-MIDTX-PIPELINE-UQ (METROPLEX)               No                  
T42   ATT/D = DISTRIBUTION CABLE (FORMERLY SBC)         No                  


Location
----------------------------------------------------------------------------
Latitude:         0                 Longitude:        0                 
Second Latitude:  0                 Second Longitude: 0

