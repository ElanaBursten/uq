

DIG-TESS Locate Request For AMP
----------------------------------------------------------------------------
Ticket Number:    090056540         Old Ticket:       083505619         
Priority:         Normal            By:               Andrew R          
Source:           Fax               Hours Notice:     48                
Type:             Update            Date:             1/5/2009 9:53:57 PM
Sequence:         1772                                                  
Map Reference:                                                          

Company Information
----------------------------------------------------------------------------
HALL ALBERT CONSTRUCTION            Type:             Contractor        
201 NE 29TH ST                      Contact:          THOMAS TINDAL     
FORT WORTH, TX 76106                Caller:           MIKE PLUNK        
Phone:            (817) 624-9391    Caller Phone:     (817) 703-9041    
Fax:              (817) 625-2941    Callback:         0700 - 1700       
Alt Contact:      (817) 624-9391                                        
Caller Email:     mwplunk@ohehac.com                                    

Work Information
----------------------------------------------------------------------------
State:            TX                Work Date:        01/07/09 at 2200  
County:           DALLAS            Type:             WATER             
Place:            DALLAS            Done For:         CITY OF DALLAS    
Street:           0 RADAR WAY                                           
Intersection:     ARMY AVE                                              
Nature of Work:   INSTALLATION OF WATER MAIN                            
Explosives:       No                Deeper Than 16":  Yes               
White Lined:      Yes               Duration:         90 DAYS           
Mapsco:           MAPSCO 52,E                                           

Remarks
----------------------------------------------------------------------------
UPDATE & REMARK-083505619                                                   
LOCATE RADAR WAY FROM ARNY AVENUE TO STILLWELL BOULEVARD. LOCATE BOTH SIDES 
OF THE ROAD AS WELL AS THE ENTIRE STREET AND THE INTERSECTIONS. CONTACT     
THOMAS TINDALL AT 817-703-9041 FOR A MEET TO VERIFY THE AREAS TO MARK.      


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
AMP   ATMOS-MIDTX-UTILIQUEST (METROPLEX)                No                  
CLG   TIME WARNER CABLE                                 No                  
GRE   ONCOR ELECTRIC DISTRIBUTION-SMP (DALLAS SOUTHWE...No                  
PMP   ATMOS-MIDTX-PIPELINE-UQ (METROPLEX)               No                  
T12   ATT/D = DISTRIBUTION CABLE (FORMERLY SBC)         No                  


Location
----------------------------------------------------------------------------
Latitude:         32.7438825882343  Longitude:        -96.9225276470597 
Second Latitude:  32.743388470587   Second Longitude: -96.9211300000001

