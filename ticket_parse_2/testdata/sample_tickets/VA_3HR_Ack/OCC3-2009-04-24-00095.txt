
WGL904 00022 VUPSa 04/24/09 07:38:24 B910502289-03A          3 HOUR

Ticket No:  B910502289-03A                        3HRS GRID RUSH LREQ
Transmit        Date: 04/24/09   Time: 07:38 AM   Op: 1KKH
Call            Date: 04/24/09   Time: 07:37 AM
Due By          Date: 04/24/09   Time: 10:37 AM
Update By       Date: 05/04/09   Time: 11:59 PM
Expires         Date: 05/07/09   Time: 07:00 AM
Old Tkt No: B910502289
Original Call   Date: 04/23/09   Time: 07:59 AM   Op: 1KMD

City/Co:  FAIRFAX             Place:  A COUNTRY PLACE               State:  VA
Address:     813                 Street: AARON CT
Cross 1:     MINBURN ST                                        

Type of Work:   VZN: FTTP
Work Done For:  VERIZON

Excavation area:START IN FRONT BUT IN BETWEEN 813 AND 812 AARON CT AND END BEHIND 808 AASON CT.
                MARK APPRX. 480'

Instructions:   ***  3***  3 HOUR NOTICE - THE CALLER DISPUTES THE RESPONSE POSTED BY THESE
                UTILITIES: DOMINION VA POWER, VERIZON, AND WASHINGTON GAS - PRS STATES CODE 60
                MARKING AGREEMENT AND AGREEMENT WAS NOT MADE , THIS IS A 3RD 3HR NOTICE
                
                VZN: FTTP WORK ORDER NUMBER 8A10058/77012/5845C

Whitelined: N   Blasting: N   Boring: N

Company:        SUNNY SOLUTIONS LLC                       Type: CONT
Co. Address:    9715 COVERED WAGON DR, M  First Time: N
City:           LAUREL  State:MD  Zip:20723
Company Phone:  301-604-0588
Contact Name:   PATTY SEGOVIA               Contact Phone:  301-996-1530
Email:          be26cool@verizon.net
Field Contact:  JOSE RODRIGUEZ
Fld. Contact Phone:2404012423

Mapbook:  5404D2
Grids:    3859B7717B-43  3859B7717B-44  3859C7717B-03  3859C7717B-04
Grids:    3859C7717B-13  3859C7717B-14  3859C7717B-24  3859C7717B-34

Members: 
DOM400 = DOMINION VIRGINIA POWER (DOM)  VZN102 = VERIZON (VZN) 
WGL904 = WASHINGTON GAS (WGL) 

Seq No:   22 A


