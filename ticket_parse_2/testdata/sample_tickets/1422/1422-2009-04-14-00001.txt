From: IRTHNet  At: 04/14/09 03:58 PM  Seq No: 53
Facility: Electric Primary

SCEGT01 679 PUPS Remote 03/02/2009 2:07:41 PM 0903021230 Remark

Ticket Number: 0903021230
Old Ticket Number: 0902200990
Created By: R-PMM
Seq Number: 679

Created Date: 03/02/2009 2:07:36 PM
Work Date/Time: 03/02/2009 2:15:01 PM
Update:           Good Through:          

Excavation Information:
State: SC     County: BERKELEY
Place: DANIEL ISLAND
Address Number: 
Street: PIERCE ST
Inters St: PIERCE ST & BEEKMAN ST
Subd: DANIEL ISLAND-PARCEL Z-6

Type of Work: GAS, INSTALL MAIN
Duration: 2 WEEKS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: ERTEL CONSTRUCTION & UNITED CABLE

Remarks/Instructions: MARK A 50 FT RADIUS IN ALL DIRECTIONS AT THE            
INTERSECTION OF PIERCE ST & BEEKMAN ST//ANY QUESTIONS PLEASE CALL RICKY       
WHITSETT @ 843-297-6189//                                                     
                                                                              
**REMARK DUE TO RAIN REMOVING MARKS**                                         

Caller Information: 
Name: PAM MCNEELY                           ERTEL CONSTRUCTION, INC.              
Address: 4738 FRANCHISE STREET
City: NORTH CHARLESTON State: SC Zip: 29418
Phone: (843) 760-8987 Ext:  Type: Business
Fax: (843) 760-8988 Caller Email: 

Contact Information:
Contact:PAM MCNEELY Email: PMCNEELY@ERTELCONST.COM
Call Back: Fax: 

Grids: 
Lat/Long: 32.848838201195, -79.9060896098956
Secondary: 32.8485283420399, -79.9057891404119
Lat/Long Caller Supplied: N 

Members Involved: BSZN42 COMZ41 CPW69 HMT85 HOT21 SCEDZ95 SCEGT01 SCGZ90      


Map Link: (NEEDS DEVELOPMENT)