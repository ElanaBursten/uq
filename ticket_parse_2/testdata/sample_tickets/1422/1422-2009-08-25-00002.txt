From: IRTHNet  At: 08/25/09 10:37 AM  Seq No: 1
Facility: Electric Primary

SCEWZ80 7 PUPS Web 08/25/2009 10:34:20 AM 0908250983 Normal

Ticket Number: 0908250983
Old Ticket Number: 
Created By: KML
Seq Number: 7

Created Date: 08/25/2009 10:34:17 AM
Work Date/Time: 08/28/2009 10:45:49 AM
Update: 09/16/2009 Good Through: 09/21/2009

Excavation Information:
State: SC     County: CHARLESTON
Place: CHARLESTON
Address Number: 24
Street: NORMAN ST
Inters St: BOGARD
Subd: 

Type of Work: LANDSCAPE, OTHER
Duration: TWO WEEKS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: STEVE HARRIS

Remarks/Instructions: LOCATE ENTIRE PROPERTY PLEASE                           
                                                                              
**PUPS MAP HAS BOGARD LISTED AS BOGARD STREET**                               

Caller Information: 
Name: STEVE HARRIS                          STEVE HARRIS                          
Address: 743 LAWTON PL
City: CHARLESTON State: SC Zip: 29412
Phone: (843) 296-6200 Ext:  Type: Mobile
Fax:  Caller Email: SGHPI@YAHOO.COM

Contact Information:
Contact: Email: SGHPI@YAHOO.COM
Call Back: Fax: 

Grids: 
Lat/Long: 32.789700330247, -79.9524165057839
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZN42 COMZ41 CPW69 ITCC76 SCEWZ80 SCGZ90 STG11             


Map Link: (NEEDS DEVELOPMENT)