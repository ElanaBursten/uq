

PUPS AUDIT
CONSOLIDATED UTILITY SERVICES, BILLING FOR AT&T/D
ZZB25
AUDIT FOR 7/13/2009

FOR CODE ZZB25

TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
      0001  0907130013                DELIVERED
      0002  0907130020                DELIVERED
      0003  0907130025                DELIVERED
      0004  0907130030                DELIVERED
      0005  0907130039                DELIVERED
!     0006  0907130040                DELIVERED
      0007  0907130044                DELIVERED
!     0008  0907130048                DELIVERED
      0009  0907130055                DELIVERED
      0010  0907130072                DELIVERED
      0011  0907130092                DELIVERED
!     0012  0907130101                DELIVERED
      0013  0907130104                DELIVERED
      0014  0907130115                DELIVERED
      0015  0907130116                DELIVERED
      0016  0907130119                DELIVERED
      0017  0907130121                DELIVERED
      0018  0907130122                DELIVERED
      0019  0907130123                DELIVERED
      0020  0907130124                DELIVERED
      0021  0907130126                DELIVERED
      0022  0907130129                DELIVERED
      0023  0907130134                DELIVERED
      0024  0907130135                DELIVERED
      0025  0907130138                DELIVERED
      0026  0907130142                DELIVERED
      0027  0907130151                DELIVERED
      0028  0907130152                DELIVERED
      0029  0907130153                DELIVERED
      0030  0907130156                DELIVERED
      0031  0907130159                DELIVERED
      0032  0907130170                DELIVERED
      0033  0907130172                DELIVERED
      0034  0907130177                DELIVERED
      0035  0907130178                DELIVERED
      0036  0907130179                DELIVERED
      0037  0907130182                DELIVERED
      0038  0907130183                DELIVERED
      0039  0907130186                DELIVERED
      0040  0907130188                DELIVERED
      0041  0907130192                DELIVERED
      0042  0907130196                DELIVERED
      0043  0907130198                DELIVERED
!     0044  0907130201                DELIVERED
      0045  0907130203                DELIVERED
      0046  0907130205                DELIVERED
      0047  0907130206                DELIVERED
      0048  0907130212                DELIVERED
      0049  0907130213                DELIVERED
      0050  0907130214                DELIVERED
      0051  0907130220                DELIVERED
!     0052  0907130222                DELIVERED
      0053  0907130224                DELIVERED
      0054  0907130228                DELIVERED
      0055  0907130235                DELIVERED
      0056  0907130240                DELIVERED
      0057  0907130241                DELIVERED
      0058  0907130242                DELIVERED
      0059  0907130257                DELIVERED
      0060  0907130260                DELIVERED
      0061  0907130261                DELIVERED
      0062  0907130263                DELIVERED
 U    0063  0907130268                DELIVERED
      0064  0907130270                DELIVERED
      0065  0907130272                DELIVERED
      0066  0907130274                DELIVERED
      0067  0907130275                DELIVERED
      0068  0907130278                DELIVERED
      0069  0907130281                DELIVERED
      0070  0907130284                DELIVERED
      0071  0907130286                DELIVERED
      0072  0907130288                DELIVERED
 R    0073  0907130291                DELIVERED
      0074  0907130294                DELIVERED
      0075  0907130295                DELIVERED
      0076  0907130296                DELIVERED
 R    0077  0907130298                DELIVERED
      0078  0907130300                DELIVERED
      0079  0907130304                DELIVERED
      0080  0907130306                DELIVERED
      0081  0907130307                DELIVERED
*     0082  0907130309                DELIVERED
      0083  0907130310                DELIVERED
 N    0084  0907130314                DELIVERED
      0085  0907130315                DELIVERED
      0086  0907130316                DELIVERED
 N    0087  0907130320                DELIVERED
!     0088  0907130322                DELIVERED
 N    0089  0907130323                DELIVERED
!     0090  0907130324                DELIVERED
      0091  0907130328                DELIVERED
!     0092  0907130329                DELIVERED
 N    0093  0907130332                DELIVERED
 N    0094  0907130337                DELIVERED
      0095  0907130344                DELIVERED
      0096  0907130345                DELIVERED
      0097  0907130347                DELIVERED
      0098  0907130348                DELIVERED
      0099  0907130352                DELIVERED
!     0100  0907130357                DELIVERED
      0101  0907130363                DELIVERED
      0102  0907130369                DELIVERED
      0103  0907130373                DELIVERED
      0104  0907130378                DELIVERED
      0105  0907130380                DELIVERED
      0106  0907130387                DELIVERED
      0107  0907130389                DELIVERED
 U    0108  0907130391                DELIVERED
*     0109  0907130392                DELIVERED
      0110  0907130395                DELIVERED
      0111  0907130400                DELIVERED
      0112  0907130402                DELIVERED
      0113  0907130405                DELIVERED
!     0114  0907130408                DELIVERED
      0115  0907130409                DELIVERED
      0116  0907130410                DELIVERED
      0117  0907130412                DELIVERED
      0118  0907130415                DELIVERED
      0119  0907130419                DELIVERED
      0120  0907130421                DELIVERED
!     0121  0907130425                DELIVERED
      0122  0907130427                DELIVERED
      0123  0907130428                DELIVERED
      0124  0907130429                DELIVERED
      0125  0907130430                DELIVERED
      0126  0907130432                DELIVERED
      0127  0907130433                DELIVERED
!     0128  0907130439                DELIVERED
      0129  0907130440                DELIVERED
      0130  0907130442                DELIVERED
      0131  0907130444                DELIVERED
      0132  0907130450                DELIVERED
      0133  0907130452                DELIVERED
      0134  0907130455                DELIVERED
      0135  0907130456                DELIVERED
      0136  0907130460                DELIVERED
      0137  0907130463                DELIVERED
 U    0138  0907130466                DELIVERED
 U    0139  0907130472                DELIVERED
      0140  0907130476                DELIVERED
      0141  0907130483                DELIVERED
      0142  0907130487                DELIVERED
      0143  0907130488                DELIVERED
!     0144  0907130490                DELIVERED
      0145  0907130491                DELIVERED
      0146  0907130494                DELIVERED
      0147  0907130496                DELIVERED
      0148  0907130495                DELIVERED
      0149  0907130498                DELIVERED
      0150  0907130504                DELIVERED
      0151  0907130507                DELIVERED
      0152  0907130509                DELIVERED
      0153  0907130516                DELIVERED
      0154  0907130518                DELIVERED
      0155  0907130521                DELIVERED
      0156  0907130522                DELIVERED
      0157  0907130526                DELIVERED
      0158  0907130527                DELIVERED
      0159  0907130529                DELIVERED
      0160  0907130531                DELIVERED
      0161  0907130533                DELIVERED
      0162  0907130535                DELIVERED
      0163  0907130537                DELIVERED
      0164  0907130544                DELIVERED
      0165  0907130548                DELIVERED
 M    0166  0907130552                DELIVERED
      0167  0907130557                DELIVERED
      0168  0907130563                DELIVERED
      0169  0907130564                DELIVERED
 M    0170  0907130570                DELIVERED
      0171  0907130574                DELIVERED
!     0172  0907130579                DELIVERED
      0173  0907130581                DELIVERED
 U    0174  0907130583                DELIVERED
      0175  0907130585                DELIVERED
!     0176  0907130588                DELIVERED
 U    0177  0907130589                DELIVERED
      0178  0907130591                DELIVERED
 N    0179  0907130596                DELIVERED
 U    0180  0907130597                DELIVERED
      0181  0907130600                DELIVERED
      0182  0907130604                DELIVERED
      0183  0907130605                DELIVERED
      0184  0907130607                DELIVERED
*     0185  0907130608                DELIVERED
 C    0186  0907130617                DELIVERED
 M    0187  0907130624                DELIVERED
      0188  0907130626                DELIVERED
      0189  0907130630                DELIVERED
      0190  0907130631                DELIVERED
      0191  0907130634                DELIVERED
      0192  0907130636                DELIVERED
      0193  0907130642                DELIVERED
      0194  0907130643                DELIVERED
      0195  0907130650                DELIVERED
 M    0196  0907130651                DELIVERED
      0197  0907130653                DELIVERED
      0198  0907130656                DELIVERED
      0199  0907130657                DELIVERED
      0200  0907130658                DELIVERED
 M    0201  0907130662                DELIVERED
      0202  0907130665                DELIVERED
      0203  0907130666                DELIVERED
      0204  0907130675                DELIVERED
      0205  0907130677                DELIVERED
      0206  0907130679                DELIVERED
      0207  0907130680                DELIVERED
      0208  0907130685                DELIVERED
      0209  0907130686                DELIVERED
      0210  0907130691                DELIVERED
      0211  0907130695                DELIVERED
      0212  0907130696                DELIVERED
      0213  0907130701                DELIVERED
!     0214  0907130703                DELIVERED
!     0215  0907130705                DELIVERED
!     0216  0907130706                DELIVERED
      0217  0907130707                DELIVERED
      0218  0907130708                DELIVERED
!     0219  0907130714                DELIVERED
      0220  0907130719                DELIVERED
      0221  0907130720                DELIVERED
      0222  0907130728                DELIVERED
 U    0223  0907130730                DELIVERED
      0224  0907130740                DELIVERED
      0225  0907130744                DELIVERED
      0226  0907130752                DELIVERED
      0227  0907130753                DELIVERED
!     0228  0907130756                DELIVERED
      0229  0907130761                DELIVERED
      0230  0907130762                DELIVERED
      0231  0907130765                DELIVERED
      0232  0907130770                DELIVERED
      0233  0907130772                DELIVERED
      0234  0907130774                DELIVERED
      0235  0907130781                DELIVERED
      0236  0907130789                DELIVERED
      0237  0907130792                DELIVERED
 U    0238  0907130794                DELIVERED
      0239  0907130796                DELIVERED
      0240  0907130797                DELIVERED
 U    0241  0907130799                DELIVERED
      0242  0907130804                DELIVERED
!     0243  0907130807                DELIVERED
      0244  0907130815                DELIVERED
      0245  0907130817                DELIVERED
      0246  0907130819                DELIVERED
      0247  0907130820                DELIVERED
      0248  0907130822                DELIVERED
      0249  0907130823                DELIVERED
      0250  0907130826                DELIVERED
      0251  0907130833                DELIVERED
      0252  0907130838                DELIVERED
      0253  0907130844                DELIVERED
      0254  0907130845                DELIVERED
 M    0255  0907130847                DELIVERED
      0256  0907130848                DELIVERED
 C    0257  0907130849                DELIVERED
      0258  0907130850                DELIVERED
 U    0259  0907130854                DELIVERED
      0260  0907130857                DELIVERED
      0261  0907130858                DELIVERED
      0262  0907130862                DELIVERED
      0263  0907130865                DELIVERED
      0264  0907130867                DELIVERED
      0265  0907130868                DELIVERED
      0266  0907130869                DELIVERED
      0267  0907130871                DELIVERED
      0268  0907130873                DELIVERED
      0269  0907130885                DELIVERED
      0270  0907130891                DELIVERED
      0271  0907130892                DELIVERED
      0272  0907130896                DELIVERED
!     0273  0907130898                DELIVERED
 U    0274  0907130902                DELIVERED
 C    0275  0907130907                DELIVERED
 U    0276  0907130910                DELIVERED
      0277  0907130911                DELIVERED
 U    0278  0907130915                DELIVERED
      0279  0907130916                DELIVERED
 U    0280  0907130918                DELIVERED
!     0281  0907130919                DELIVERED
      0282  0907130923                DELIVERED
      0283  0907130925                DELIVERED
 U    0284  0907130928                DELIVERED
      0285  0907130929                DELIVERED
      0286  0907130933                DELIVERED
 U    0287  0907130936                DELIVERED
      0288  0907130937                DELIVERED
 U    0289  0907130938                DELIVERED
      0290  0907130941                DELIVERED
 U    0291  0907130942                DELIVERED
 U    0292  0907130944                DELIVERED
 U    0293  0907130945                DELIVERED
      0294  0907130946                DELIVERED
 U    0295  0907130947                DELIVERED
      0296  0907130948                DELIVERED
      0297  0907130950                DELIVERED
      0298  0907130952                DELIVERED
      0299  0907130956                DELIVERED
      0300  0907130962                DELIVERED
      0301  0907130967                DELIVERED
*     0302  0907130971                DELIVERED
!     0303  0907130972                DELIVERED
      0304  0907130973                DELIVERED
      0305  0907130980                DELIVERED
      0306  0907130997                DELIVERED
      0307  0907130999                DELIVERED
      0308  0907131000                DELIVERED
      0309  0907131005                DELIVERED
      0310  0907131007                DELIVERED
      0311  0907131009                DELIVERED
      0312  0907131011                DELIVERED
      0313  0907131018                DELIVERED
      0314  0907131026                DELIVERED
      0315  0907131030                DELIVERED
      0316  0907131032                DELIVERED
      0317  0907131034                DELIVERED
      0318  0907131039                DELIVERED
      0319  0907131040                DELIVERED
      0320  0907131044                DELIVERED
      0321  0907131045                DELIVERED
      0322  0907131051                DELIVERED
      0323  0907131055                DELIVERED
      0324  0907131058                DELIVERED
      0325  0907131059                DELIVERED
*     0326  0907131063                DELIVERED
      0327  0907131065                DELIVERED
      0328  0907131071                DELIVERED
      0329  0907131072                DELIVERED
      0330  0907131075                DELIVERED
      0331  0907131080                DELIVERED
      0332  0907131082                DELIVERED
      0333  0907131093                DELIVERED
      0334  0907131096                DELIVERED
      0335  0907131097                DELIVERED
      0336  0907131100                DELIVERED
      0337  0907131102                DELIVERED
!     0338  0907131105                DELIVERED
      0339  0907131108                DELIVERED
      0340  0907131110                DELIVERED
      0341  0907131113                DELIVERED
      0342  0907131116                DELIVERED
      0343  0907131118                DELIVERED
      0344  0907131120                DELIVERED
      0345  0907131121                DELIVERED
      0346  0907131136                DELIVERED
      0347  0907131137                DELIVERED
      0348  0907131142                DELIVERED
      0349  0907131144                DELIVERED
      0350  0907131145                DELIVERED
      0351  0907131150                DELIVERED
      0352  0907131156                DELIVERED
      0353  0907131160                DELIVERED
      0354  0907131161                DELIVERED
      0355  0907131164                DELIVERED
!     0356  0907131168                DELIVERED
!     0357  0907131171                DELIVERED
      0358  0907131172                DELIVERED
      0359  0907131175                DELIVERED
      0360  0907131178                DELIVERED
      0361  0907131179                DELIVERED
      0362  0907131180                DELIVERED
      0363  0907131181                DELIVERED
      0364  0907131184                DELIVERED
      0365  0907131185                DELIVERED
      0366  0907131186                DELIVERED
      0367  0907131187                DELIVERED
*     0368  0907131192                DELIVERED
      0369  0907131194                DELIVERED
      0370  0907131196                DELIVERED
      0371  0907131202                DELIVERED
      0372  0907131203                DELIVERED
*     0373  0907131206                DELIVERED
      0374  0907131209                DELIVERED
      0375  0907131211                DELIVERED
      0376  0907131213                DELIVERED
      0377  0907131215                DELIVERED
      0378  0907131220                DELIVERED
      0379  0907131225                DELIVERED
 R    0380  0907131229                DELIVERED
      0381  0907131230                DELIVERED
 R    0382  0907131233                DELIVERED
 R    0383  0907131234                DELIVERED
 R    0384  0907131236                DELIVERED
 R    0385  0907131239                DELIVERED
      0386  0907131246                DELIVERED
      0387  0907131250                DELIVERED
!     0388  0907131253                DELIVERED
      0389  0907131254                DELIVERED
      0390  0907131256                DELIVERED
      0391  0907131260                DELIVERED
      0392  0907131262                DELIVERED
      0393  0907131264                DELIVERED
      0394  0907131267                DELIVERED
      0395  0907131270                DELIVERED
      0396  0907131272                DELIVERED
      0397  0907131278                DELIVERED
 U    0398  0907131280                DELIVERED
!     0399  0907131281                DELIVERED
      0400  0907131284                DELIVERED
      0401  0907131285                DELIVERED
      0402  0907131290                DELIVERED
      0403  0907131292                DELIVERED
!     0404  0907131296                DELIVERED
 U    0405  0907131300                DELIVERED
      0406  0907131301                DELIVERED
      0407  0907131305                DELIVERED
      0408  0907131306                DELIVERED
      0409  0907131307                DELIVERED
      0410  0907131309                DELIVERED
      0411  0907131311                DELIVERED
      0412  0907131314                DELIVERED
      0413  0907131315                DELIVERED
      0414  0907131317                DELIVERED
      0415  0907131319                DELIVERED
      0416  0907131321                DELIVERED
      0417  0907131322                DELIVERED
      0418  0907131323                DELIVERED
      0419  0907131324                DELIVERED
      0420  0907131329                DELIVERED
      0421  0907131333                DELIVERED
      0422  0907131334                DELIVERED
      0423  0907131337                DELIVERED
      0424  0907131338                DELIVERED
      0425  0907131340                DELIVERED
 U    0426  0907131345                DELIVERED
      0427  0907131351                DELIVERED
      0428  0907131352                DELIVERED
      0429  0907131355                DELIVERED
      0430  0907131362                DELIVERED
      0431  0907131364                DELIVERED
 R    0432  0907131367                DELIVERED
 R    0433  0907131370                DELIVERED
      0434  0907131372                DELIVERED
      0435  0907131374                DELIVERED
      0436  0907131383                DELIVERED
      0437  0907131384                DELIVERED
      0438  0907131390                DELIVERED
      0439  0907131394                DELIVERED
      0440  0907131439                DELIVERED
      0441  0907131457                DELIVERED
      0442  0907131479                DELIVERED
      0443  0907131483                DELIVERED
      0444  0907131494                DELIVERED
      0445  0907131495                DELIVERED
      0446  0907131510                DELIVERED
      0447  0907131525                DELIVERED
      0448  0907131527                DELIVERED
      0449  0907131542                DELIVERED
      0450  0907131545                DELIVERED
      0451  0907131550                DELIVERED
      0452  0907131561                DELIVERED
      0453  0907131565                DELIVERED
      0454  0907131576                DELIVERED
      0455  0907131577                DELIVERED
      0456  0907131578                DELIVERED
 U    0457  0907131579                DELIVERED
      0458  0907131582                DELIVERED
      0459  0907131587                DELIVERED
 U    0460  0907131593                DELIVERED
      0461  0907131595                DELIVERED
      0462  0907131597                DELIVERED
 U    0463  0907131599                DELIVERED
 U    0464  0907131607                DELIVERED
 U    0465  0907131610                DELIVERED
      0466  0907131612                DELIVERED
 U    0467  0907131616                DELIVERED
      0468  0907131618                DELIVERED
      0469  0907131620                DELIVERED
      0470  0907131627                DELIVERED
      0471  0907131628                DELIVERED
      0472  0907131629                DELIVERED
      0473  0907131633                DELIVERED
      0474  0907131634                DELIVERED
!     0475  0907131635                DELIVERED
      0476  0907131637                DELIVERED
      0477  0907131639                DELIVERED
      0478  0907131640                DELIVERED
      0479  0907131642                DELIVERED
      0480  0907131643                DELIVERED
      0481  0907131644                DELIVERED
      0482  0907131645                DELIVERED
      0483  0907131646                DELIVERED
      0484  0907131648                DELIVERED
      0485  0907131649                DELIVERED
      0486  0907131651                DELIVERED
      0487  0907131653                DELIVERED
      0488  0907131654                DELIVERED
 S    0489  0907131659                DELIVERED
      0490  0907131658                DELIVERED
 U    0491  0907131661                DELIVERED
      0492  0907131663                DELIVERED
 U    0493  0907131664                DELIVERED
      0494  0907131665                DELIVERED
      0495  0907131667                DELIVERED
      0496  0907131673                DELIVERED
      0497  0907131682                DELIVERED
!     0498  0907131691                DELIVERED
      0499  0907131692                DELIVERED
 U    0500  0907131693                DELIVERED
      0501  0907131694                DELIVERED
 U    0502  0907131701                DELIVERED
      0503  0907131704                DELIVERED
!     0504  0907131706                DELIVERED
      0505  0907131707                DELIVERED
      0506  0907131708                DELIVERED
      0507  0907131709                DELIVERED
      0508  0907131716                DELIVERED
      0509  0907131718                DELIVERED
 U    0510  0907131720                DELIVERED
      0511  0907131723                DELIVERED
 U    0512  0907131729                DELIVERED
 C    0513  0907131730                DELIVERED
 U    0514  0907131734                DELIVERED
 U    0515  0907131735                DELIVERED
      0516  0907131739                DELIVERED
      0517  0907131740                DELIVERED
 U    0518  0907131742                DELIVERED
 U    0519  0907131750                DELIVERED
 U    0520  0907131756                DELIVERED
      0521  0907131759                DELIVERED
 U    0522  0907131764                DELIVERED
      0523  0907131765                DELIVERED
      0524  0907131766                DELIVERED
 N    0525  0907131768                DELIVERED
 U    0526  0907131769                DELIVERED
      0527  0907131776                DELIVERED
      0528  0907131777                DELIVERED
 U    0529  0907131778                DELIVERED
      0530  0907131779                DELIVERED
      0531  0907131780                DELIVERED
      0532  0907131781                DELIVERED
      0533  0907131783                DELIVERED
 U    0534  0907131786                DELIVERED
      0535  0907131787                DELIVERED
      0536  0907131790                DELIVERED
      0537  0907131791                DELIVERED
      0538  0907131792                DELIVERED
      0539  0907131793                DELIVERED
 U    0540  0907131794                DELIVERED
 U    0541  0907131795                DELIVERED
 U    0542  0907131797                DELIVERED
      0543  0907131799                DELIVERED
      0544  0907131802                DELIVERED
      0545  0907131804                DELIVERED
 U    0546  0907131806                DELIVERED
      0547  0907131808                DELIVERED
 U    0548  0907131811                DELIVERED
      0549  0907131812                DELIVERED
 U    0550  0907131817                DELIVERED
      0551  0907131819                DELIVERED
 U    0552  0907131821                DELIVERED
 U    0553  0907131828                DELIVERED
 U    0554  0907131833                DELIVERED
      0555  0907131835                DELIVERED
 U    0556  0907131839                DELIVERED
      0557  0907131840                DELIVERED
      0558  0907131841                DELIVERED
 U    0559  0907131842                DELIVERED
      0560  0907131843                DELIVERED
      0561  0907131844                DELIVERED
 U    0562  0907131846                DELIVERED
      0563  0907131849                DELIVERED
      0564  0907131850                DELIVERED
      0565  0907131854                DELIVERED
      0566  0907131855                DELIVERED
 U    0567  0907131856                DELIVERED
      0568  0907131857                DELIVERED
      0569  0907131859                DELIVERED
      0570  0907131860                DELIVERED
      0571  0907131861                DELIVERED
 U    0572  0907131862                DELIVERED
 U    0573  0907131866                DELIVERED
      0574  0907131867                DELIVERED
 U    0575  0907131869                DELIVERED
      0576  0907131871                DELIVERED
 U    0577  0907131873                DELIVERED
 U    0578  0907131876                DELIVERED
      0579  0907131877                DELIVERED
!     0580  0907131882                DELIVERED
 U    0581  0907131883                DELIVERED
 U    0582  0907131884                DELIVERED
      0583  0907131886                DELIVERED
 U    0584  0907131887                DELIVERED
      0585  0907131888                DELIVERED
 U    0586  0907131889                DELIVERED
      0587  0907131891                DELIVERED
      0588  0907131892                DELIVERED
 U    0589  0907131895                DELIVERED
 U    0590  0907131896                DELIVERED
      0591  0907131897                DELIVERED
      0592  0907131898                DELIVERED
 U    0593  0907131899                DELIVERED
 U    0594  0907131901                DELIVERED
      0595  0907131902                DELIVERED
      0596  0907131904                DELIVERED
 U    0597  0907131905                DELIVERED
!     0598  0907131907                DELIVERED
 U    0599  0907131908                DELIVERED
 U    0600  0907131912                DELIVERED
      0601  0907131913                DELIVERED
 U    0602  0907131914                DELIVERED
      0603  0907131916                DELIVERED
 U    0604  0907131918                DELIVERED
 U    0605  0907131922                DELIVERED
      0606  0907131926                DELIVERED
      0607  0907131927                DELIVERED
 U    0608  0907131928                DELIVERED
 U    0609  0907131930                DELIVERED
 U    0610  0907131932                DELIVERED
      0611  0907131933                DELIVERED
 U    0612  0907131934                DELIVERED
      0613  0907131935                DELIVERED
 U    0614  0907131937                DELIVERED
 U    0615  0907131938                DELIVERED
 U    0616  0907131939                DELIVERED
 U    0617  0907131943                DELIVERED
      0618  0907131945                DELIVERED
 U    0619  0907131946                DELIVERED
      0620  0907131947                DELIVERED
 U    0621  0907131949                DELIVERED
 U    0622  0907131951                DELIVERED
      0623  0907131952                DELIVERED
      0624  0907131954                DELIVERED
      0625  0907131955                DELIVERED
      0626  0907131957                DELIVERED
      0627  0907131959                DELIVERED
      0628  0907131960                DELIVERED
      0629  0907131961                DELIVERED
      0630  0907131962                DELIVERED
*     0631  0907131963                DELIVERED
 U    0632  0907131965                DELIVERED
      0633  0907131969                DELIVERED
 U    0634  0907131970                DELIVERED
 U    0635  0907131974                DELIVERED
      0636  0907131975                DELIVERED
 U    0637  0907131976                DELIVERED
      0638  0907131977                DELIVERED
!     0639  0907131978                DELIVERED
      0640  0907131979                DELIVERED
 U    0641  0907131980                DELIVERED
 U    0642  0907131983                DELIVERED
      0643  0907131984                DELIVERED
 U    0644  0907131985                DELIVERED
      0645  0907131986                DELIVERED
 U    0646  0907131988                DELIVERED
 U    0647  0907131991                DELIVERED
 U    0648  0907131992                DELIVERED
      0649  0907131993                DELIVERED
      0650  0907131994                DELIVERED
 U    0651  0907131995                DELIVERED
 U    0652  0907131998                DELIVERED
 U    0653  0907132002                DELIVERED
      0654  0907132003                DELIVERED
 U    0655  0907132005                DELIVERED
      0656  0907132007                DELIVERED
 U    0657  0907132008                DELIVERED
      0658  0907132009                DELIVERED
 N    0659  0907132010                DELIVERED
 U    0660  0907132011                DELIVERED
 U    0661  0907132013                DELIVERED
 N    0662  0907132014                DELIVERED
 U    0663  0907132017                DELIVERED
 U    0664  0907132019                DELIVERED
      0665  0907132020                DELIVERED
 N    0666  0907132021                DELIVERED
 U    0667  0907132024                DELIVERED
      0668  0907132025                DELIVERED
      0669  0907132029                DELIVERED
      0670  0907132033                DELIVERED
      0671  0907132044                DELIVERED
      0672  0907132045                DELIVERED
      0673  0907132053                DELIVERED
      0674  0907132054                DELIVERED
      0675  0907132067                DELIVERED
      0676  0907132083                DELIVERED
      0677  0907132095                DELIVERED
      0678  0907132100                DELIVERED
      0679  0907132102                DELIVERED
      0680  0907132110                DELIVERED
      0681  0907132126                DELIVERED
      0682  0907132129                DELIVERED
      0683  0907132141                DELIVERED
      0684  0907132144                DELIVERED
      0685  0907132151                DELIVERED
      0686  0907132154                DELIVERED
      0687  0907132165                DELIVERED
      0688  0907132173                DELIVERED
      0689  0907132176                DELIVERED
      0690  0907132178                DELIVERED
      0691  0907132180                DELIVERED
      0692  0907132181                DELIVERED
 U    0693  0907132184                DELIVERED
      0694  0907132190                DELIVERED
      0695  0907132196                DELIVERED
      0696  0907132200                DELIVERED
      0697  0907132207                DELIVERED
      0698  0907132209                DELIVERED
 U    0699  0907132218                DELIVERED
      0700  0907132220                DELIVERED
      0701  0907132226                DELIVERED
 U    0702  0907132248                DELIVERED
 U    0703  0907132251                DELIVERED
 U    0704  0907132252                DELIVERED
 U    0705  0907132254                DELIVERED
 U    0706  0907132257                DELIVERED
 U    0707  0907132260                DELIVERED
 U    0708  0907132262                DELIVERED
 U    0709  0907132265                DELIVERED
FOR CODE ZZB25


NORMAL    : 516
RESEND    : 8
EMERGENCY : 36
CANCEL    : 4
MEET      : 6
NO SHOW   : 10
SURVEY    : 1
UPDATE    : 119
REMARK    : 9
DESIGN    : 0
RETRANSMIT: 0
FAILED    : 0
TOTAL FOR ZZB25: 709




LEGEND
-----------------------
C - CANCEL
! - EMERGENCY
M - MEET
  - NORMAL
N - NO SHOW
* - RESEND
S - SURVEY
V - VOID
U - UPDATE
R - REMARK
D - DESIGN

