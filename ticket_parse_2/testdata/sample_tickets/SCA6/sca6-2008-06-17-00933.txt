
                                                                                     
 ATTD28SD 00384A USAS 06/17/08 14:45:04 A72470912-50A NORM UPDT GRID                 
                                                                                     
 Ticket : A72470912 Date: 06/17/08 Time: 14:44 Oper: TCSD Chan: WEB                  
 Old Tkt: A72470912 Date: 09/04/07 Time: 11:40 Oper: TCSD Revision: 50A              
 State: CA County: SAN DIEGO            Place: LAKESIDE                              
 Locat: HIGHWAY 80  FROM LAKEVIEW RD W/FOR APPROX 4000FT;  FROM THE INTER OF OLD     
      : HIGHWAY 80 & E LAKEVIEW RD, MARK N & S/FOR APPROX 100FT;  REQUESTING MEET    
      : AND MARK THURSDAY SEPTEMBER 6TH AT 11:00AM CONTACT FOREMAN GREG GIBBS        
      : 619-726-3150 TO CONFIRM ETA   MEET AT THE INTER OF OLD HIGHWAY 80 & E        
      : LAKEVIEW RD                                                                  
 Address: OLD HIGHWAY 80                                                             
 X/ST 1 : EAST LAKEVIEW RD                                                           
 Grids: 1232D0634    1232D0712    1232E06123                 Delineated: Y           
 Lat/Long  : 32.841874/-116.892849 32.839902/-116.891826                             
           : 32.835674/-116.904810 32.833702/-116.903787                             
 Caller GPS:                                                                         
                                                                                     
 Boring: N  Explosives: N  Vacuum: N                                                 
                                                                                     
 Re-Mark: N                                                                          
                                                                                     
 Work : WATER REPLACEMENT                                                            
 Work date: 06/17/08 Time: 14:43 Hrs notc: 000 Work hrs: 000 Priority: 2             
 Instruct : WORK CONTINUING        Permit   : UNKNOWN                                
 Done for : HELIX WATER DISTRICT                                                     
                                                                                     
 Company: TC CONSTRUCTION                Caller: SANDY                               
 Address: 10540 PROSPECT AVE                                                         
 City&St: SANTEE, CA                     Zip: 92071                                  
 Phone: 619-448-4560 Ext:      Call back: 7-4                                        
 Formn: BOBBY K / 27-016     Phone: 619-726-3106                                     
 Email: SBOUCHER@TCINCSD.COM                                                         
                                                                                     
                                   COMMENTS                                          
                                                                                     
 **RESEND**CORRECTING TG ADDED ONLY 1232D063 1232D064 1232D071 1232D072 1232E063     
 ADDS COSDTS CLARIFIED WORK LOCATION PER SANDY--[SME 09/04/07 11:56]                 
 **RESEND**REQUESTING STAND BY FOR GAS COMPANY FRIDAY SEPTEMBER 14TH AT 8:00AM       
 CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER SANDY--[TCSD 09/12/07    
 10:36]                                                                              
 **RESEND**REQUESTING STAND BY FROM GAS COMPANY ONLY FOR 8" GAS MAIN MONDAY          
 SEPTEMBER 24TH AT 7:30AM CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA     
 PER SANDY--[TCSD 09/20/07 15:48]                                                    
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 09/21/07          
 16:17]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 10/04/07          
 16:21]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 10/04/07          
 16:31]                                                                              
 **RESEND**REQUESTING STAND BY FROM GAS COMPANY TUESDAY OCTOBER 16TH AT 7:30 AM      
 CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER SANDY--[TCSD 10/15/07    
 09:40]                                                                              
 **RESEND**REQUESTING STAND BY FROM GAS COMPANY WEDNESDAY OCTOBER 17TH AT 7:30 AM    
 CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER SANDY--[TCSD 10/16/07    
 11:44]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 10/16/07          
 15:50]                                                                              
 **RESEND**REQUESTING STAND BY FROM GAS COMPANY THURSDAY OCTOBER 18TH AT 7:30 AM     
 CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER SANDY--[TCSD 10/17/07    
 11:29]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 10/30/07          
 14:31]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE FROM ALL UTILITIES WORK ONLY PER             
 SANDY--[TCSD 11/09/07 16:44]                                                        
 **RESEND**UPDATE ONLY-WORK CONT UPDATE FROM ALL UTILITIES WORK ONLY PER             
 SANDY--[TCSD 11/09/07 16:44]                                                        
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 11/27/07          
 10:21]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 12/05/07          
 12:11]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE TICKET ONLY PER SANDY--[TCSD 12/18/07        
 11:11]                                                                              
 **RESEND**REQUESTING GAS STAND BY FROM SDG&E THURSDAY DECEMBER 20TH AT 8:00AM       
 CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER SANDY--[TCSD 12/19/07    
 14:41]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE TICKET WORK ONLY PER SANDY--[TCSD            
 12/28/07 09:27]                                                                     
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 01/11/08          
 10:57]                                                                              
 **RESEND**REQUEST REMARKS FROM ALL-WORK CONT REQUESTING REMARKS FROM ALL            
 UTILITIES CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER                
 SANDY--[TCSD 01/15/08 11:54]                                                        
 **RESEND**UPDATE ONLY-WORK CONT REQUESTING UPDATE FOR WORK ONLY PER SANDY--[TCSD    
 01/21/08 15:25]                                                                     
 **RESEND**REQUESTING STAND BY FROM GAS COMPANY MONDAY FEBRUARY 1ST AND TUESDAY      
 FEBRUARY 5TH AT 9:00AM CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA       
 PER SANDY--[TCSD 02/01/08 09:34]                                                    
 **RESEND**REQUESTING A STAND BY FROM GAS COMPANY WEDNESDAY FEBRUARY 6TH AND         
 THURSDAY FEBRUARY 7TH AT 9:00AM CONTACT FOREMAN BOBBY K 619-726-3106 TO CONFIRM
 **RESEND**CORRECTING TG ADDED ONLY 1232D063 1232D064 1232D071 1232D072 1232E063     
 ADDS COSDTS CLARIFIED WORK LOCATION PER SANDY--[SME 09/04/07 11:56]                 
 **RESEND**REQUESTING STAND BY FOR GAS COMPANY FRIDAY SEPTEMBER 14TH AT 8:00AM       
 CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER SANDY--[TCSD 09/12/07    
 10:36]                                                                              
 **RESEND**REQUESTING STAND BY FROM GAS COMPANY ONLY FOR 8" GAS MAIN MONDAY          
 SEPTEMBER 24TH AT 7:30AM CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA     
 PER SANDY--[TCSD 09/20/07 15:48]                                                    
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 09/21/07          
 16:17]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 10/04/07          
 16:21]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 10/04/07          
 16:31]                                                                              
 **RESEND**REQUESTING STAND BY FROM GAS COMPANY TUESDAY OCTOBER 16TH AT 7:30 AM      
 CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER SANDY--[TCSD 10/15/07    
 09:40]                                                                              
 **RESEND**REQUESTING STAND BY FROM GAS COMPANY WEDNESDAY OCTOBER 17TH AT 7:30 AM    
 CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER SANDY--[TCSD 10/16/07    
 11:44]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 10/16/07          
 15:50]                                                                              
 **RESEND**REQUESTING STAND BY FROM GAS COMPANY THURSDAY OCTOBER 18TH AT 7:30 AM     
 CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER SANDY--[TCSD 10/17/07    
 11:29]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 10/30/07          
 14:31]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE FROM ALL UTILITIES WORK ONLY PER             
 SANDY--[TCSD 11/09/07 16:44]                                                        
 **RESEND**UPDATE ONLY-WORK CONT UPDATE FROM ALL UTILITIES WORK ONLY PER             
 SANDY--[TCSD 11/09/07 16:44]                                                        
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 11/27/07          
 10:21]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 12/05/07          
 12:11]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE TICKET ONLY PER SANDY--[TCSD 12/18/07        
 11:11]                                                                              
 **RESEND**REQUESTING GAS STAND BY FROM SDG&E THURSDAY DECEMBER 20TH AT 8:00AM       
 CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER SANDY--[TCSD 12/19/07    
 14:41]                                                                              
 **RESEND**UPDATE ONLY-WORK CONT UPDATE TICKET WORK ONLY PER SANDY--[TCSD            
 12/28/07 09:27]                                                                     
 **RESEND**UPDATE ONLY-WORK CONT UPDATE WORK ONLY PER SANDY--[TCSD 01/11/08          
 10:57]                                                                              
 **RESEND**REQUEST REMARKS FROM ALL-WORK CONT REQUESTING REMARKS FROM ALL            
 UTILITIES CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA PER                
 SANDY--[TCSD 01/15/08 11:54]                                                        
 **RESEND**UPDATE ONLY-WORK CONT REQUESTING UPDATE FOR WORK ONLY PER SANDY--[TCSD    
 01/21/08 15:25]                                                                     
 **RESEND**REQUESTING STAND BY FROM GAS COMPANY MONDAY FEBRUARY 1ST AND TUESDAY      
 FEBRUARY 5TH AT 9:00AM CONTACT FOREMAN GREG GIBBS 619-726-3150 TO CONFIRM ETA       
 PER SANDY--[TCSD 02/01/08 09:34]                                                    
 **RESEND**REQUESTING A STAND BY FROM GAS COMPANY WEDNESDAY FEBRUARY 6TH AND         
 THURSDAY FEBRUARY 7TH AT 9:00AM CONTACT FOREMAN BOBBY K 619-726-3106 TO CONFIRM
 Mbrs : COX04  PTT28  SDG04  SDGEMG VID01  VIS01                                     
                                                                                     
 At&t Ticket Id: 311          clli code:   VISTCA12
