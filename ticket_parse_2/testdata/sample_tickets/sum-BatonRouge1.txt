

0
Date/Time :  3-05-02 at  2:17

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: LF01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58108-48HR |  48    58469-48HR |  95    58929-48HR | 141    59413-48HR |
   2    58124-48HR |  49    58578-48HR |  96    58918-48HR | 142    59419-48HR |
   3    58134-48HR |  50    58470-48HR |  97    58957-48HR | 143    59424-48HR |
   4    58141-24HR |  51    58471-48HR |  98    59003-SHRT | 144    59437-EMER |
   5    58143-48HR |  52    58587-48HR |  99    59016-48HR | 145    59426-48HR |
   6    58149-48HR |  53    58446-48HR | 100    59020-48HR | 146    59430-48HR |
   7    58183-48HR |  54    58468-48HR | 101    59049-48HR | 147    59428-48HR |
   8    58197-48HR |  55    58473-48HR | 102    59051-48HR | 148    59453-48HR |
   9    58205-48HR |  56    58474-48HR | 103    59053-48HR | 149    59454-48HR |
  10    58204-SHRT |  57    58475-48HR | 104    59054-48HR | 150    59455-48HR |
  11    58218-48HR |  58    58582-48HR | 105    59059-48HR | 151    59456-48HR |
  12    58225-48HR |  59    58605-48HR | 106    59065-48HR | 152    59457-48HR |
  13    58237-48HR |  60    58612-24HR | 107    59074-48HR | 153    59459-48HR |
  14    58280-EMER |  61    58613-48HR | 108    59086-EMER | 154    59461-48HR |
  15    53951-2NDQ |  62    58635-48HR | 109    59084-48HR | 155    59463-48HR |
  16    58293-EMER |  63    58642-48HR | 110    59082-REPR | 156    59462-48HR |
  17    58309-24HR |  64    58606-48HR | 111    59103-EMER | 157    59468-48HR |
  18    58317-48HR |  65    58340-24HR | 112    59107-EMER | 158    59469-48HR |
  19    58331-48HR |  66    58472-48HR | 113    59109-24HR | 159    59470-48HR |
  20    58337-48HR |  67    58654-48HR | 114    59112-48HR | 160    59472-48HR |
  21    58352-48HR |  68    58669-48HR | 115    59115-48HR | 161    59474-48HR |
  22    58375-48HR |  69    58639-48HR | 116    59117-SHRT | 162    59475-REPR |
  23    58388-48HR |  70    58640-48HR | 117    59122-48HR | 163    59478-48HR |
  24    58381-24HR |  71    58345-24HR | 118    59124-48HR | 164    59479-48HR |
  25    58393-48HR |  72    58705-SHRT | 119    59125-48HR | 165    59481-48HR |
  26    58389-EMER |  73    58638-48HR | 120    59131-48HR | 166    59484-48HR |
  27    58405-SHRT |  74    58645-48HR | 121    59130-48HR | 167    59485-48HR |
  28    58395-48HR |  75    58724-48HR | 122    59160-REPR | 168    59486-48HR |
  29    58440-SHRT |  76    53422-2NDQ | 123    59176-24HR | 169    59487-48HR |
  30    58424-48HR |  77    58735-48HR | 124    59189-24HR | 170    59491-48HR |
  31    58423-24HR |  78    58741-48HR | 125    59191-SHRT | 171    59494-48HR |
  32    54222-2NDQ |  79    58744-48HR | 126    59193-48HR | 172    59496-48HR |
  33*   58446-48HR |  80    58742-48HR | 127    59228-REPR | 173    59498-48HR |
  34    55035-48HR |  81    58543-48HR | 128    59240-REPR | 174    59502-48HR |
  35    58466-48HR |  82    58786-48HR | 129    59244-48HR | 175    59506-48HR |
  36    58459-48HR |  83    58785-SHRT | 130    59270-REPR | 176    59508-48HR |
  37    58462-48HR |  84    58791-SHRT | 131    59209-48HR | 177    59512-48HR |
  38    58523-48HR |  85    58812-48HR | 132    59278-REPR | 178    59514-48HR |
  39    58463-48HR |  86    56016-48HR | 133    59309-EMER | 179    59515-48HR |
  40    58547-48HR |  87    58851-48HR | 134    59310-48HR | 180    59517-48HR |
  41    58464-48HR |  88    58878-48HR | 135    59365-48HR | 181    59520-48HR |
  42    58465-48HR |  89    58400-48HR | 136    59374-EMER | 182    59533-48HR |
  43    58467-48HR |  90    58885-SHRT | 137    59375-48HR | 183    59568-48HR |
  44    58551-48HR |  91    58890-48HR | 138    59392-48HR | 184    59573-48HR |
  45    58566-48HR |  92    58898-EMER | 139    59388-24HR | 185    59574-48HR |
  46    58569-48HR |  93    58910-REPR | 140    59411-24HR | 186    59586-48HR |
  47    58570-48HR |  94    58917-48HR |

* indicates ticket # is repeated

Total Tickets: 186

48HR - 48 HOURS NOTICE                |  143
24HR - 24 HOURS NOTICE                |   12
SHRT - SHORT NOTICE                   |   10
EMER - EMERGENCY                      |   10
2NDQ - 2ND REQUEST                    |    3
REPR - REPAIR                         |    8

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: MICI01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58157-48HR |  14    58191-48HR |  27    58238-48HR |  40    58864-48HR |
   2    58161-48HR |  15    58195-48HR |  28    58241-48HR |  41*   58889-48HR |
   3    58165-48HR |  16    58202-48HR |  29    58253-48HR |  42    58889-48HR |
   4    58169-48HR |  17    58207-48HR |  30    58320-24HR |  43    58891-48HR |
   5    58172-48HR |  18    58211-48HR |  31    58328-24HR |  44    58894-48HR |
   6    58175-48HR |  19    58212-48HR |  32    58384-48HR |  45    59092-REPR |
   7    58177-48HR |  20    58217-48HR |  33    58535-48HR |  46    59100-REPR |
   8    58178-48HR |  21    58220-48HR |  34    58561-48HR |  47    59223-48HR |
   9    58180-48HR |  22    58221-48HR |  35    58715-48HR |  48    59241-48HR |
  10    58182-48HR |  23    58223-48HR |  36    58722-48HR |  49    59280-48HR |
  11    58184-48HR |  24    58231-48HR |  37    58739-48HR |  50    59296-48HR |
  12    58187-48HR |  25    58232-48HR |  38    58748-48HR |  51    59382-48HR |
  13    58188-48HR |  26    58235-48HR |  39    58750-48HR |

* indicates ticket # is repeated

Total Tickets: 51

48HR - 48 HOURS NOTICE                |   47
24HR - 24 HOURS NOTICE                |    2
REPR - REPAIR                         |    2

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: WB01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58160-48HR |  14    58407-48HR |  26    58841-48HR |  38    59304-48HR |
   2    58193-REPR |  15    58410-48HR |  27    58844-REPR |  39    59300-48HR |
   3    58215-EMER |  16    58522-48HR |  28    58848-48HR |  40    59179-48HR |
   4    58277-48HR |  17    58661-48HR |  29    58866-SHRT |  41    59395-48HR |
   5    58325-48HR |  18    58702-EMER |  30    58911-48HR |  42    59429-48HR |
   6    58358-48HR |  19    58699-48HR |  31    58931-48HR |  43    59442-SHRT |
   7    58355-48HR |  20    58725-48HR |  32    58930-48HR |  44    59444-SHRT |
   8    58366-48HR |  21    58757-48HR |  33    59022-48HR |  45    59448-SHRT |
   9    58378-48HR |  22    58787-48HR |  34    58795-48HR |  46    59449-SHRT |
  10    58387-48HR |  23    58806-48HR |  35    59180-48HR |  47    59451-48HR |
  11    58406-48HR |  24    55778-2NDQ |  36    59249-REPR |  48    59504-SHRT |
  12    58408-48HR |  25    58833-REPR |  37    59297-48HR |  49    59510-48HR |
  13    58409-48HR |

* indicates ticket # is repeated

Total Tickets: 49

48HR - 48 HOURS NOTICE                |   36
REPR - REPAIR                         |    4
EMER - EMERGENCY                      |    2
2NDQ - 2ND REQUEST                    |    1
SHRT - SHORT NOTICE                   |    6

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: GSU05

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58229-48HR |   7    59142-48HR |  13    59213-48HR |  18    59431-48HR |
   2    58391-48HR |   8    59198-48HR |  14    59225-48HR |  19    59434-48HR |
   3    58443-48HR |   9    59203-48HR |  15    59235-48HR |  20    59526-48HR |
   4    58590-48HR |  10    59211-48HR |  16    59238-48HR |  21    59587-REPR |
   5    58716-EMER |  11    59219-48HR |  17    59425-48HR |  22    59588-REPR |
   6    58766-48HR |  12    59222-48HR |

* indicates ticket # is repeated

Total Tickets: 22

48HR - 48 HOURS NOTICE                |   19
EMER - EMERGENCY                      |    1
REPR - REPAIR                         |    2

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: KMCTEL01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58152-48HR |   4    58563-48HR |   6    59335-48HR |   8    59414-48HR |
   2    58260-EMER |   5    59279-48HR |   7    59406-48HR |   9    59552-48HR |
   3    58359-48HR |

* indicates ticket # is repeated

Total Tickets: 9

48HR - 48 HOURS NOTICE                |    8
EMER - EMERGENCY                      |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: TORCTV01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    53951-2NDQ |   4    58588-48HR |   7    58771-48HR |  10    59436-24HR |
   2    58435-48HR |   5    58742-48HR |   8    58878-48HR |  11    59535-SHRT |
   3    58566-48HR |   6    58785-SHRT |   9    59240-REPR |

* indicates ticket # is repeated

Total Tickets: 11

2NDQ - 2ND REQUEST                    |    1
48HR - 48 HOURS NOTICE                |    6
SHRT - SHORT NOTICE                   |    2
REPR - REPAIR                         |    1
24HR - 24 HOURS NOTICE                |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: EBRDPW01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58117-REPR |  10    58516-48HR |  19    58945-REPR |  28    59333-48HR |
   2    58146-REPR |  11    58517-48HR |  20    59041-48HR |  29    59340-48HR |
   3*   58166-REPR |  12*   58664-48HR |  21    59045-48HR |  30    59323-48HR |
   4    58166-REPR |  13    58768-SHRT |  22    59072-EMER |  31    59389-48HR |
   5    58171-48HR |  14    58746-24HR |  23    59070-48HR |  32    59400-48HR |
   6    58245-48HR |  15    51755-2NDQ |  24    59085-48HR |  33    59405-48HR |
   7    58267-48HR |  16    58839-48HR |  25    59095-SHRT |  34    59414-48HR |
   8    58432-48HR |  17    58882-REPR |  26    59148-48HR |  35    59417-48HR |
   9    58436-48HR |  18    58664-48HR |  27    59330-48HR |

* indicates ticket # is repeated

Total Tickets: 35

REPR - REPAIR                         |    6
48HR - 48 HOURS NOTICE                |   24
SHRT - SHORT NOTICE                   |    2
24HR - 24 HOURS NOTICE                |    1
2NDQ - 2ND REQUEST                    |    1
EMER - EMERGENCY                      |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: GSUBRN02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58511-48HR |   4    59101-48HR |   7    59274-48HR |  10    59584-48HR |
   2    58746-24HR |   5    59267-48HR |   8    56706-2NDQ |  11    59585-48HR |
   3    59073-48HR |   6    59272-48HR |   9    59581-48HR |

* indicates ticket # is repeated

Total Tickets: 11

48HR - 48 HOURS NOTICE                |    9
24HR - 24 HOURS NOTICE                |    1
2NDQ - 2ND REQUEST                    |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: GSUBRW02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    57610-48HR |   6    58696-48HR |  11    59034-24HR |  15    59201-REPR |
   2    58247-48HR |   7    58518-48HR |  12    59140-48HR |  16    59282-48HR |
   3    58244-24HR |   8    58729-48HR |  13    59143-48HR |  17    59500-48HR |
   4    58227-48HR |   9    58946-48HR |  14    59118-48HR |  18    59547-48HR |
   5    58403-48HR |  10    59011-48HR |

* indicates ticket # is repeated

Total Tickets: 18

48HR - 48 HOURS NOTICE                |   15
24HR - 24 HOURS NOTICE                |    2
REPR - REPAIR                         |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: GSUBRE02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58107-48HR |   7    58515-48HR |  13    59169-48HR |  19    59360-48HR |
   2    58308-SHRT |   8    58938-48HR |  14    59172-48HR |  20    59435-EMER |
   3    58383-REPR |   9    59038-48HR |  15    59202-48HR |  21    59269-48HR |
   4    58434-48HR |  10    59044-48HR |  16    59232-48HR |  22    59443-REPR |
   5    58508-48HR |  11    59113-48HR |  17    59254-48HR |  23    59483-48HR |
   6    58509-48HR |  12    59139-48HR |  18*   59269-48HR |

* indicates ticket # is repeated

Total Tickets: 23

48HR - 48 HOURS NOTICE                |   19
SHRT - SHORT NOTICE                   |    1
REPR - REPAIR                         |    2
EMER - EMERGENCY                      |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: GSUBRS02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58206-48HR |   7    58601-48HR |  13    59014-48HR |  18    59490-48HR |
   2    58271-48HR |   8    58607-48HR |  14    59064-48HR |  19    59495-48HR |
   3    58524-48HR |   9    58666-SHRT |  15    59068-48HR |  20    59497-48HR |
   4    58559-48HR |  10    58800-48HR |  16    59123-48HR |  21    59503-48HR |
   5    58565-48HR |  11    58807-48HR |  17    59410-48HR |  22    59507-48HR |
   6    58580-48HR |  12    58919-48HR |

* indicates ticket # is repeated

Total Tickets: 22

48HR - 48 HOURS NOTICE                |   21
SHRT - SHORT NOTICE                   |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: ACSI01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58260-EMER |   2    58794-48HR |   3    59433-48HR |

* indicates ticket # is repeated

Total Tickets: 3

EMER - EMERGENCY                      |    1
48HR - 48 HOURS NOTICE                |    2

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: ACSI02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58178-48HR |   3    58739-48HR |   5    58858-48HR |   7    59226-48HR |
   2    58722-48HR |   4    58809-48HR |   6    59177-48HR |

* indicates ticket # is repeated

Total Tickets: 7

48HR - 48 HOURS NOTICE                |    7

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: CTE048

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    59061-EMER |

* indicates ticket # is repeated

Total Tickets: 1

EMER - EMERGENCY                      |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: CTE051

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58456-48HR |   3    59317-48HR |   5    59420-48HR |   7    59528-48HR |
   2    58730-48HR |   4    59378-SHRT |   6    59524-48HR |

* indicates ticket # is repeated

Total Tickets: 7

48HR - 48 HOURS NOTICE                |    6
SHRT - SHORT NOTICE                   |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:18

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: CTE056

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58142-48HR |   7    58849-48HR |  13    58981-48HR |  18    59364-48HR |
   2    58503-48HR |   8    58969-48HR |  14    58988-48HR |  19    59380-48HR |
   3    58538-24HR |   9    58982-48HR |  15*   59007-48HR |  20    59390-48HR |
   4    58548-24HR |  10    58971-48HR |  16    59007-48HR |  21    59492-48HR |
   5    58301-REPR |  11    58978-48HR |  17    59078-48HR |  22    59540-SHRT |
   6    58840-24HR |  12    58983-48HR |

* indicates ticket # is repeated

Total Tickets: 22

48HR - 48 HOURS NOTICE                |   17
24HR - 24 HOURS NOTICE                |    3
REPR - REPAIR                         |    1
SHRT - SHORT NOTICE                   |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:19

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: CTE057

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1*   58668-48HR |   3    58763-48HR |   5    56054-2NDQ |   7    59127-48HR |
   2    58668-48HR |   4    58913-48HR |   6    59120-48HR |   8    59492-48HR |

* indicates ticket # is repeated

Total Tickets: 8

48HR - 48 HOURS NOTICE                |    7
2NDQ - 2ND REQUEST                    |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:19

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: CTE056A

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58395-48HR |   4    58736-SHRT |   7    59165-24HR |   9    59184-48HR |
   2    58401-48HR |   5    58762-SHRT |   8    59251-48HR |  10    59432-48HR |
   3    58463-48HR |   6    59162-24HR |

* indicates ticket # is repeated

Total Tickets: 10

48HR - 48 HOURS NOTICE                |    6
SHRT - SHORT NOTICE                   |    2
24HR - 24 HOURS NOTICE                |    2

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:19

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: COXCAB01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58106-EMER |  15    58409-48HR |  29    58739-48HR |  43    58795-48HR |
   2    58115-EMER |  16    58407-48HR |  30    58748-48HR |  44*   59026-48HR |
   3    58161-48HR |  17    58410-48HR |  31    58757-48HR |  45    59026-48HR |
   4    58165-48HR |  18    58384-48HR |  32    58806-48HR |  46    59040-48HR |
   5    58170-48HR |  19    58364-48HR |  33    58809-48HR |  47    59050-48HR |
   6    58215-EMER |  20    58480-48HR |  34    58864-48HR |  48    56462-2NDQ |
   7    58325-48HR |  21    58526-48HR |  35*   58889-48HR |  49    59161-48HR |
   8    58372-48HR |  22    55519-2NDQ |  36    58889-48HR |  50    59296-48HR |
   9    58355-48HR |  23    55524-2NDQ |  37    58891-48HR |  51    59304-48HR |
  10    58378-48HR |  24    58581-48HR |  38    58894-48HR |  52    59395-48HR |
  11*   58364-48HR |  25    58712-48HR |  39    58533-48HR |  53    59451-48HR |
  12    58387-48HR |  26    55515-2NDQ |  40    58927-REPR |  54    53305-2NDQ |
  13    58408-48HR |  27    55520-2NDQ |  41    58944-48HR |  55    59504-SHRT |
  14    58449-48HR |  28    55521-2NDQ |  42    59022-48HR |

* indicates ticket # is repeated

Total Tickets: 55

EMER - EMERGENCY                      |    3
48HR - 48 HOURS NOTICE                |   43
2NDQ - 2ND REQUEST                    |    7
REPR - REPAIR                         |    1
SHRT - SHORT NOTICE                   |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:19

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: TLG02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58108-48HR |  29    58567-48HR |  57    58929-48HR |  84    59455-48HR |
   2    58143-48HR |  30    58569-48HR |  58    58957-48HR |  85    59456-48HR |
   3    58149-48HR |  31    58470-48HR |  59    58963-48HR |  86    59457-48HR |
   4    58197-48HR |  32    58471-48HR |  60    59003-SHRT |  87    59459-48HR |
   5    58225-48HR |  33    58577-48HR |  61    59016-48HR |  88    59461-48HR |
   6    58237-48HR |  34    58473-48HR |  62    59020-48HR |  89    59463-48HR |
   7    58280-EMER |  35    58474-48HR |  63    59049-48HR |  90    59472-48HR |
   8    58293-EMER |  36    58475-48HR |  64    59051-48HR |  91    59475-REPR |
   9    58317-48HR |  37    58605-48HR |  65    59054-48HR |  92    59481-48HR |
  10    58337-48HR |  38    58612-24HR |  66    59065-48HR |  93    59485-48HR |
  11    58344-48HR |  39    58635-48HR |  67    59084-48HR |  94    59487-48HR |
  12    58352-48HR |  40    58642-48HR |  68    59103-EMER |  95    59491-48HR |
  13    58375-48HR |  41    58340-24HR |  69    59112-48HR |  96    59496-48HR |
  14    58388-48HR |  42    58472-48HR |  70    59115-48HR |  97    59498-48HR |
  15    58393-48HR |  43    58669-48HR |  71    59131-48HR |  98    59502-48HR |
  16    58389-EMER |  44    58639-48HR |  72    59130-48HR |  99    59506-48HR |
  17    58405-SHRT |  45    58425-48HR |  73    59176-24HR | 100    59508-48HR |
  18    58395-48HR |  46    58638-48HR |  74    59191-SHRT | 101    59512-48HR |
  19    58440-SHRT |  47    58645-48HR |  75    59244-48HR | 102    59515-48HR |
  20    58424-48HR |  48    58724-48HR |  76    59310-48HR | 103    59524-48HR |
  21    58423-24HR |  49    53422-2NDQ |  77    59317-48HR | 104    59528-48HR |
  22    54222-2NDQ |  50    58730-48HR |  78    59365-48HR | 105    59533-48HR |
  23    55035-48HR |  51    58735-48HR |  79    59375-48HR | 106    59573-48HR |
  24    58459-48HR |  52    58744-48HR |  80    59392-48HR | 107    59574-48HR |
  25    58463-48HR |  53    58543-48HR |  81    59411-24HR | 108    59576-48HR |
  26    58556-48HR |  54    58791-SHRT |  82    59419-48HR | 109    59577-48HR |
  27    58564-48HR |  55    56016-48HR |  83    59454-48HR | 110    59586-48HR |
  28    58464-48HR |  56    58890-48HR |

* indicates ticket # is repeated

Total Tickets: 110

48HR - 48 HOURS NOTICE                |   93
EMER - EMERGENCY                      |    4
SHRT - SHORT NOTICE                   |    5
24HR - 24 HOURS NOTICE                |    5
2NDQ - 2ND REQUEST                    |    2
REPR - REPAIR                         |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:19

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: HM02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58100-48HR |  23    58617-48HR |  45    58892-REPR |  67    59220-48HR |
   2    58102-REPR |  24    58594-48HR |  46    58899-24HR |  68    59230-48HR |
   3    58105-REPR |  25    58330-48HR |  47    58903-48HR |  69    59233-48HR |
   4*   58012-48HR |  26    58348-48HR |  48    58906-48HR |  70    59256-48HR |
   5    58103-48HR |  27    58596-48HR |  49    58908-REPR |  71    59292-48HR |
   6    58208-SHRT |  28    58599-48HR |  50    53689-2NDQ |  72    59299-48HR |
   7    58233-REPR |  29    58677-48HR |  51    58934-48HR |  73    59301-48HR |
   8    58250-48HR |  30    58678-48HR |  52    58948-48HR |  74    59303-48HR |
   9    58281-24HR |  31    58680-48HR |  53    58955-SHRT |  75    59316-48HR |
  10    58346-48HR |  32    58351-48HR |  54    58996-48HR |  76    59294-REPR |
  11    58336-48HR |  33    58690-48HR |  55    59046-48HR |  77    59361-48HR |
  12    58361-48HR |  34    58692-48HR |  56    59042-48HR |  78    59373-48HR |
  13    58376-48HR |  35    58698-48HR |  57    59062-24HR |  79    59466-48HR |
  14    58402-48HR |  36    58703-48HR |  58    59097-SHRT |  80    59505-48HR |
  15    58454-SHRT |  37    58708-48HR |  59    59114-SHRT |  81    59538-48HR |
  16    58012-48HR |  38    58700-48HR |  60    59128-SHRT |  82    59542-48HR |
  17    58525-48HR |  39    58718-REPR |  61    59132-SHRT |  83    59553-48HR |
  18    58574-EMER |  40    58721-48HR |  62    59168-48HR |  84    59555-48HR |
  19    57768-2NDQ |  41    58683-48HR |  63    56303-2NDQ |  85    59557-48HR |
  20    58589-48HR |  42    58357-48HR |  64    59208-48HR |  86    59561-48HR |
  21    58591-48HR |  43    58843-48HR |  65    59231-48HR |  87    59564-48HR |
  22    58592-48HR |  44    58837-48HR |  66    59248-SHRT |  88    59578-24HR |

* indicates ticket # is repeated

Total Tickets: 88

48HR - 48 HOURS NOTICE                |   65
REPR - REPAIR                         |    7
SHRT - SHORT NOTICE                   |    8
24HR - 24 HOURS NOTICE                |    4
EMER - EMERGENCY                      |    1
2NDQ - 2ND REQUEST                    |    3

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:19

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: ATTNEX01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58325-48HR |

* indicates ticket # is repeated

Total Tickets: 1

48HR - 48 HOURS NOTICE                |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:19

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: CHART01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58107-48HR |  12    58386-48HR |  23    58679-48HR |  34    59219-48HR |
   2    58210-48HR |  13    58392-48HR |  24    58716-EMER |  35    59222-48HR |
   3    58229-48HR |  14    58383-REPR |  25    54282-2NDQ |  36    59225-48HR |
   4    58264-48HR |  15    58397-48HR |  26    58876-48HR |  37    59238-48HR |
   5    58255-REPR |  16    58399-48HR |  27    58850-24HR |  38    59185-SHRT |
   6    58259-REPR |  17    58402-48HR |  28    58958-48HR |  39    59291-48HR |
   7    58353-REPR |  18    58588-48HR |  29    58966-24HR |  40    59311-48HR |
   8    58365-48HR |  19    58611-48HR |  30    59062-24HR |  41    59425-48HR |
   9    58368-48HR |  20    58600-48HR |  31    59198-48HR |  42    59523-REPR |
  10    58373-48HR |  21    58657-REPR |  32    59203-48HR |  43    59527-48HR |
  11    58379-48HR |  22    58681-48HR |  33    59211-48HR |

* indicates ticket # is repeated

Total Tickets: 43

48HR - 48 HOURS NOTICE                |   31
REPR - REPAIR                         |    6
EMER - EMERGENCY                      |    1
2NDQ - 2ND REQUEST                    |    1
24HR - 24 HOURS NOTICE                |    3
SHRT - SHORT NOTICE                   |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:19

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: CHART02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58100-48HR |  14    58617-48HR |  27    58934-48HR |  40    59299-48HR |
   2    58107-48HR |  15    58594-48HR |  28    58948-48HR |  41    59301-48HR |
   3*   58012-48HR |  16    58330-48HR |  29    58955-SHRT |  42    59303-48HR |
   4    58103-48HR |  17    58348-48HR |  30    58996-48HR |  43    59294-REPR |
   5    58233-REPR |  18    58596-48HR |  31    59024-24HR |  44    59373-48HR |
   6    58250-48HR |  19    58599-48HR |  32    59114-SHRT |  45    59505-48HR |
   7    58383-REPR |  20    58677-48HR |  33    59128-SHRT |  46    59538-48HR |
   8    58454-SHRT |  21    58680-48HR |  34    59132-SHRT |  47    59542-48HR |
   9    58012-48HR |  22    58690-48HR |  35    59208-48HR |  48    59553-48HR |
  10    58433-48HR |  23    58843-48HR |  36    59248-SHRT |  49    59555-48HR |
  11    57768-2NDQ |  24    58899-24HR |  37    59220-48HR |  50    59557-48HR |
  12    58589-48HR |  25    58903-48HR |  38    59230-48HR |  51    59561-48HR |
  13    58591-48HR |  26    58908-REPR |  39    59292-48HR |  52    59578-24HR |

* indicates ticket # is repeated

Total Tickets: 52

48HR - 48 HOURS NOTICE                |   38
REPR - REPAIR                         |    4
SHRT - SHORT NOTICE                   |    6
2NDQ - 2ND REQUEST                    |    1
24HR - 24 HOURS NOTICE                |    3

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:20

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: KOCHPG01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58216-48HR |

* indicates ticket # is repeated

Total Tickets: 1

48HR - 48 HOURS NOTICE                |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:20

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: ADELPH01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58152-48HR |   4    58517-48HR |   6    59279-48HR |   8    59414-48HR |
   2    58260-EMER |   5    59149-48HR |   7    59335-48HR |   9    59433-48HR |
   3    58359-48HR |

* indicates ticket # is repeated

Total Tickets: 9

48HR - 48 HOURS NOTICE                |    8
EMER - EMERGENCY                      |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:20

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: GSUBR01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58099-EMER |  39    58249-48HR |  77    58861-SHRT | 114    59314-48HR |
   2    58101-EMER |  40    58432-48HR |  78    58870-REPR | 115    59318-48HR |
   3    55245-3RDQ |  41    58434-48HR |  79    58877-REPR | 116    59329-EMER |
   4    58114-48HR |  42    58437-48HR |  80    58879-REPR | 117    59313-48HR |
   5    58111-REPR |  43    58483-48HR |  81    58882-REPR | 118    59335-48HR |
   6    58117-REPR |  44    58436-48HR |  82    58896-EMER | 119    59323-48HR |
   7    58120-REPR |  45    58514-48HR |  83    58924-24HR | 120    59368-REPR |
   8    58125-REPR |  46    58563-48HR |  84    58664-48HR | 121    59383-48HR |
   9    58137-REPR |  47    58571-48HR |  85    58945-REPR | 122    59386-48HR |
  10    58138-EMER |  48    58573-24HR |  86    59036-48HR | 123    59389-48HR |
  11    58139-SHRT |  49    58575-REPR |  87    59039-48HR | 124    59391-48HR |
  12    58144-REPR |  50    58634-EMER |  88    59041-48HR | 125    59394-48HR |
  13    58146-REPR |  51    58513-48HR |  89    59045-48HR | 126    59397-48HR |
  14    58148-REPR |  52    58516-48HR |  90    59057-SHRT | 127    59399-48HR |
  15    58150-REPR |  53    58608-48HR |  91    59072-EMER | 128    59400-48HR |
  16    58152-48HR |  54    58519-48HR |  92    59070-48HR | 129    59401-48HR |
  17    58171-48HR |  55    58517-48HR |  93    59085-48HR | 130    59402-48HR |
  18    58201-EMER |  56    58520-48HR |  94    59091-SHRT | 131    59403-48HR |
  19    57314-REPR |  57*   58664-48HR |  95    59089-48HR | 132    59404-48HR |
  20    58190-48HR |  58    58675-EMER |  96    59095-SHRT | 133    59405-48HR |
  21    58200-48HR |  59    51242-2NDQ |  97    59144-48HR | 134    59406-48HR |
  22    58213-EMER |  60    58672-48HR |  98    59141-48HR | 135    59409-48HR |
  23    58242-48HR |  61    58512-48HR |  99    59146-48HR | 136    59412-48HR |
  24    58245-48HR |  62    58732-48HR | 100    59148-48HR | 137    59414-48HR |
  25    58251-48HR |  63    58751-48HR | 101    59149-48HR | 138    59417-48HR |
  26    54130-2NDQ |  64    58794-48HR | 102    59164-48HR | 139    59423-48HR |
  27    58260-EMER |  65    58797-48HR | 103    59186-48HR | 140    59433-48HR |
  28    58256-48HR |  66    58773-48HR | 104    59253-48HR | 141    59163-48HR |
  29    58266-EMER |  67    58801-48HR | 105    59236-48HR | 142    59544-48HR |
  30    52460-24HR |  68    51755-2NDQ | 106    59252-48HR | 143    59548-48HR |
  31    58234-REPR |  69    58847-EMER | 107    59265-48HR | 144    59549-48HR |
  32    58263-48HR |  70    58813-SHRT | 108    59268-48HR | 145    59551-48HR |
  33    58267-48HR |  71    58839-48HR | 109    59271-48HR | 146    59552-48HR |
  34    58239-48HR |  72    58845-48HR | 110    59273-48HR | 147    59556-48HR |
  35    58343-REPR |  73    58855-SHRT | 111    59279-48HR | 148    59560-48HR |
  36    58359-48HR |  74    58865-REPR | 112    59289-48HR | 149    59571-48HR |
  37    58243-48HR |  75    58854-REPR | 113    59293-48HR | 150    59572-48HR |
  38    58246-48HR |  76    58868-REPR |

* indicates ticket # is repeated

Total Tickets: 150

EMER - EMERGENCY                      |   13
3RDQ - 3RD REQUEST                    |    1
48HR - 48 HOURS NOTICE                |  101
REPR - REPAIR                         |   22
SHRT - SHORT NOTICE                   |    7
2NDQ - 2ND REQUEST                    |    3
24HR - 24 HOURS NOTICE                |    3

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:20

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: GSMP02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58255-REPR |   2    58994-REPR |   3    59587-REPR |   4    59588-REPR |

* indicates ticket # is repeated

Total Tickets: 4

REPR - REPAIR                         |    4

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:20

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: GSMP01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  

* indicates ticket # is repeated

Total Tickets: 0


Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02



0
Date/Time :  3-05-02 at  2:20

Louisiana One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON  3/04/02

Date/Time: 03/05/02 01:00:00 AM
Receiving Terminal: ADELPH02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1    58739-48HR |

* indicates ticket # is repeated

Total Tickets: 1

48HR - 48 HOURS NOTICE                |    1

Please call (800) 272-3020 if this data does
not match the tickets you received on  3/04/02




