
ATTRN2 00016 USAN 06/17/09 13:12:59 0147971 NORMAL NOTICE EXTENSION

Message Number: 0147971 Received by USAN at 13:12 on 06/17/09 by INTERNET

Work Begins:    05/26/09 at 14:30   Notice: 000 hrs      Priority: 2

Expires: 07/16/09 at 17:00   Update By: 07/14/09 at 16:59

Caller:         ALLEN MEEHAN             
Company:        FARWEST SAFETY                     
Address:        226 N.MAIN ST, LODI                     
City:           LODI                          State: CA Zip: 95240
Business Tel:   209-339-8085                  Fax: 209-339-4256                
Email Address:  SIGNSHOP@FARWESTSAFETY.COM                                  

Nature of Work: AUGER TO INST SIGN POST                 
Done for:       TOP GRADE CONST               Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 209-339-8085           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
    W/SI/O S/BOUND ABERNATHY RD 170' S/O AUTO MALL PKWY

Place: FAIRFIELD                    County: SOLANO               State: CA

Long/Lat Long: -122.085474 Lat:  38.23981  Long: -122.083174 Lat:  38.241624 

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:
#1 EXTEND TO 07/16/2009 RE-MARK NO ORIG DATE 06/17/2009-ISLB914
06/17/2009 


Sent to:
ATTRNG = ATT BROADBAND RING NETWK     CADDEL = CA DEPT/WTR RES DELTA        
COMFAI = COMCAST-FAIRFIELD            COMNPA = COMCAST-NAPA                 
CTYFAI = CITY FAIRFIELD               CTYVJO = CITY VALLEJO                 
FSUSWR = FAIRFIELD-SUISUN SWR         PBTNPA = PACIFIC BELL NAPA            
PGEVAC = PGE DISTR VACAVILLE          SOLIRR = SOLANO IRR DIST              


