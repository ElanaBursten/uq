
5TH NO RESPONSE

HIGH PRIORITY 5TH NO RESPONSE NOTICE  Communication between the excavator on
ticket # 175100  and your company has been unsuccessful.  Resolution requires
you to escalate your   efforts to directly communicate with the excavator
immediately.  Please contact the USA North Center in order to: 1.	Acknowledge
receipt of this HIGH PRIORITY notice 2.	Confirm that escalated resolution has
taken place and / or started 3.	Verify that USA North has the correct contact
information for your company  Goal ? to facilitate successful communication
between the USA North member and the excavator so both Underground Damage
Prevention Safety Stakeholders can continue safe operations.     USA North
contact information: Telephone # 800-640-5137 ext 1 Email address
usanorth@usan.org  Thank you, USA North Operations
