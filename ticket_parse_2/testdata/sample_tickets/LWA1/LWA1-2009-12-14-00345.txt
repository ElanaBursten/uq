
Ticket No:  9292327               2 FULL BUSINESS DAYS 
Send To: PSEELC45   Seq No:   21  Map Ref:  
Send To: PSEELC46   Seq No:   15  Map Ref:  
Send To: PSEGAS40   Seq No:   36  Map Ref:  
Send To: PSEGAS41   Seq No:   22  Map Ref:  

Transmit      Date: 12/14/09   Time:  2:12 PM    Op: ormoni 
Original Call Date: 12/14/09   Time:  1:22 PM    Op: webusr 
Work to Begin Date: 12/17/09   Time: 12:00 AM 

State: WA            County: PIERCE                  Place: GRAHAM 
Address:  19815      Street: 90TH AVE CT E 
Nearest Intersecting Street: 192ND ST E 

Twp: 19N   Rng: 4E    Sect-Qtr: 27-SW,34-SW-NW,29-SW-SE,28-SW-SE,33,32 
Twp: 18N   Rng: 4E    Sect-Qtr: 6-SE-NE,3-SW-NW,5,4 
Ex. Coord NW Lat: 47.0980224Lon: -122.3362269SE Lat: 47.0700521Lon: -122.293948 

Type of Work: 60 AMP TEMPORARY POWER POLE 
Location of Work: SEVERAL BLOCKS SOUTH OF INTERSECTING ST TO 90TH AVE CT E,
: LFT SIDE OF ST; WINTERWOOD PARK LOT 89; MARK 20FT RADIUS OF POWER STUB ON
: FRONT CORNER OF LOT; PLEASE DO NOT PAINT SIDEWALKS OR STREET
: 
: ++ADDRESS NOT FOUND, "SEVERAL BLOCKS" S OF INTERSECTION NOT DEFINED, SO
: UNABLE TO PINPOINT - USED TG PAGE AND GRID TO MAP++ 

Remarks: THMAS MAP PIE.895.A1; 90TH AVE CT E IS EAST OF 86TH AVE E, WEST OF 
: 90TH AVE E AND SOUTH OF 192ND ST E 

Company     : PROVIDENT ELECTRIC INC           Best Time:  8AM-3PM 
Contact Name: KAREN MCCABE                     Phone: (253)631-7750  Ext.: 113 
Email Address:  KARENM@PROVIDENTELECTRIC.NET 
Alt. Contact: BONNIE AVOLIO                    Phone: (253)631-7750 
Contact Fax : (253)631-7754 
Work Being Done For: SDC HOMES 
Additional Members:  
CC7711     FIRH2001   MASTEL01   PIERCE01   QLNWA24    RAINVW01   SPRINT03 
 TACH2O01   TACPWR01 
