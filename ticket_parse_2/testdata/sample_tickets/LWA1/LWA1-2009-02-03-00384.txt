
Ticket No:  9022705               2 FULL BUSINESS DAYS 
Send To: NLNDTV03   Seq No:    1  Map Ref:  

Transmit      Date:  2/03/09   Time: 10:34 AM    Op: orben 
Original Call Date:  2/03/09   Time: 10:28 AM    Op: orben 
Work to Begin Date:  2/06/09   Time: 12:00 AM 

State: WA            County: GRANT                   Place: EPHRATA 
Address:             Street: HWY 28 
Nearest Intersecting Street: ROAD B 

Twp: 21N   Rng: 26E   Sect-Qtr: 2-SW,3-SE,10-NW-NE,11-NW 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 47.3444378Lon: -119.5431704SE Lat: 47.3340796Lon: -119.5286471 

Type of Work: INSTALL SIGN POST 
Location of Work: SITE IS APX 300FT S OF ABV INTER ON W SIDE OF HWY 28 AT
: MILE POST 49. MARK APX 4 FT RADIUS OF STAKE MARKED WITH ORANGE FLAG AT SITE. 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : CITY OF EPHRATA                  Best Time:   
Contact Name: JEFF ALLEN                       Phone: (509)754-4601 
Email Address:         
Alt. Contact: CELL                             Phone: (509)750-1829 
Contact Fax :  
Work Being Done For: CITY OF EPHRATA 
Additional Members:  
EPHRAT01   GRAPUD02   QLNWA13    WSDOT09 

