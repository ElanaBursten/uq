
NOTICE OF INTENT TO EXCAVATE      NOTI-REPAIR 
Ticket No: 24980009 
Transmit      Date: 09/05/08      Time: 06:05AM  Op: mnfxkr 
Original Call Date: 09/05/08      Time: 06:02AM  Op: mnfxkr 
Work to Begin Date: 09/09/08      Time: 07:00AM 

Place: HOUSTON CITY 
Address:   6719      Street: LA SOMBRA DR 
Nearest Intersecting Street: LEANDRA 

Type of Work: EXCAVATE AND REPAIR TAPLINE 
Extent of Work: FRONT EASEMENT/WIDTH OF LOT. 

Remarks: FAXED LOCATE REQUEST     ECO S/O# & DISTRICT: S/O 2248665/CHF1 

Company     : SOUTHWEST WATER, INC 
Contact Name: MARCUS BISHOP         Contact Phone: (832)209-5236 
Alt. Contact: RAMIRO SALAZAR        Alt. Phone   : (832)209-5237 
Work Being Done For: SOUTHWEST WATER, INC 
State: TX              County: HARRIS 
Map: TX    Page: 527   Grid Cells: G 
Map: TX    Page: 527   Grid Cells: G 
Explosives: N                            
CPTEN01    CPTEN02 
Send To: COMCAST3  Seq No: 0001   Map Ref:  

