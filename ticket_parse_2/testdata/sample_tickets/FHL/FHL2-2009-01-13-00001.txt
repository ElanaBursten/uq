
From TESS
P78
PEDERNALES ELECTRIC COOPERATIVE
Audit For 1/12/2009

For Code P78
Type  Seq#  Ticket                    Status                                
----  ----  ------------------------  ------------------------------------- 
!     0001  090120059                 Delivered                           
      0002  090120603                 Delivered                           
      0003  090120611                 Delivered                           
      0004  090120984                 Delivered                           
      0005  090121096                 Delivered                           
      0006  090121406                 Delivered                           
      0007  090121855                 Delivered                           
      0008  090121899                 Delivered                           
      0009  090121911                 Delivered                           
      0010  090121928                 Delivered                           
      0011  090122259                 Delivered                           
      0012  090128438                 Delivered                           
      0013  090122894                 Delivered                           
      0014  090123182                 Delivered                           
      0015  090123294                 Delivered                           
      0016  090123347                 Delivered                           
      0017  090123401                 Delivered                           
      0018  090123870                 Delivered                           
      0019  090128628                 Delivered                           
      0020  090128631                 Delivered                           
      0021  090128633                 Delivered                           
      0022  090124845                 Delivered                           
      0023  090125519                 Delivered                           
      0024  090126368                 Delivered                           
      0025  090126556                 Delivered                           
      0026  090126795                 Delivered                           

Normal    : 25
Resend    : 0
Emergency : 1
Failed    : 0
Total for P78: 26



Legend
-----------------------
  - Normal
* - Resend
! - Emergency


