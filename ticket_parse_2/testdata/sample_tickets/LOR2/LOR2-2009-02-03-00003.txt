
Ticket No:  9018838               48 HOUR NOTICE 
Send To: FALCON10   Seq No:    1  Map Ref:  

Transmit      Date:  2/03/09   Time:  7:32 AM    Op: ormary 
Original Call Date:  2/03/09   Time:  6:12 AM    Op: webusr 
Work to Begin Date:  2/05/09   Time:  8:00 AM 

State: OR            County: COOS                    Place: COOS BAY 
Address:   1084      Street: S 5TH ST 
Nearest Intersecting Street: E KRUSE AVE 

Twp: 25S   Rng: 13W   Sect-Qtr: 35-SW 
Twp: 25S   Rng: 13W   Sect-Qtr: 35-SW-NW,34-SE-NE 
Ex. Coord NW Lat: 43.3584817Lon: -124.2196022SE Lat: 43.3554295Lon: -124.215978 

Type of Work: INSTALL GAS SERVICE 
Location of Work: LOCATE AREA MARKED IN WHITE ON EAST SIDE OF 5TH IN FRONT OF
: ABOVE ADDRESS 

Remarks:  
:  

Company     : NW NATURAL                       Best Time:   
Contact Name: JAMIE CHRISTOPHER                Phone: (503)226-4211  Ext.: 4408 
Email Address:  JLC@NWNATURAL.COM 
Alt. Contact: SUSAN EDDY                       Phone:  
Contact Fax :  
Work Being Done For: NWN 
Additional Members:  
CBNBW01    CCB01      GTC09      NNG10      NWN01      PPL24 

