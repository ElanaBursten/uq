PBTHA5 00001 USAN 09/13/02 06:06:07 0339300 NORMAL NOTICE EXTENSION

Message Number: 0339300 Received by USAN at 05:55 on 09/13/02 by BMC

Work Begins:    08/08/02 at 07:00   Notice: 000 hrs      Priority: 2

Caller:         TIM SIMMONS & MARTY
Company:        WOOD BROTHERS INC
Address:        POB 216, LEMOORE
City:           LEMOORE                       State: CA Zip: 93245
Telephone:      559-924-7715                  Fax: 
Alt #(s):       
Nature of Work: GR FOR DAIRY
Done for:       P/O BERRWAY                   
Foreman:        MARTIN BERHOEVEN              Area Marked in White Paint
Permit Type:    NO                            
Location: 
    S/SI/O FLORAL AVE APP 1/2 MI W/O JAMESON AVE
    (DIR PER CALLER)

Place: BURREL, CO AREA              County: FRESNO               State: CA

Map Book: 
Page Grid
0000 000
Long/Lat Long: -120.002052 Lat:  36.577343 Long: -119.995101 Lat:  36.580463 
State Grid  E:  0 N:  0 E:   0 N:  0

R E M A R K S: 
RENEWAL OF TICKET RN#307790, ORIG. DATE: 07/12/02, REMARK NO
#1 EXTEND TO 09/03/02, REMARK NO, ORIG DATE: 08/06/02 PER TIM
#2 EXTEND TO 09/17/02, REMARK NO, ORIG DATE: 08/06/02 PER TIM
#3 EXTEND TO 10/01/02, REMARK NO, ORIG DATE: 08/06/02

Sent to:
ATTCAL = AT&T COMM CALIFORNIA         PBTHA3 = PACBELL HANFORD 3            
PBTHAN = PACIFIC BELL HANFORD         PGEFNO = PGE DISTR FRESNO             
SCGVIS = SO CAL GAS VISALIA           VEREXR = VERIZON CAL EXETER           

PBTHA5 00002 USAN 09/13/02 06:15:12 0389884 PRIORITY NOTICE

Message Number: 0389884 Received by USAN at 06:11 on 09/13/02 by AMC

Work Begins:    09/13/02 at 06:13   Notice: 000 hrs      Priority: 0

Caller:         SALLY PETERSON
Company:        PETERSON
Address:        550 JACKS CREEK RD
City:           ONYX                          State: CA Zip: 93255
Telephone:      760-371-5204                  Fax: 
Alt #(s):       
Nature of Work: EXC TO LEVEL GROUND & REM TREE`S
Done for:       SAME                          
Foreman:        ABV                           Area Not Marked
Permit Type:    NO                            
Location: 
Street Address:         1800  W TULARE AVE
  Cross Street:         MOONEY BLVD
    WRK LOC ON E/SI/O THE BLDG

Place: VISALIA                      County: TULARE               State: CA

Map Book: 
Page Grid
0000 000
Long/Lat Long: -119.314738 Lat:  36.319473 Long: -119.311778 Lat:  36.320692 
State Grid  E:  0 N:  0 E:   0 N:  0

Sent to:
ATTCAL = AT&T COMM CALIFORNIA         ATTVIS = ATT BROADBAND - VISALIA      
CTYVIS = XE CITY VISALIA              CWSVIS = CALIF WTR SVC-VISALIA        
PBTHA3 = PACBELL HANFORD 3            PBTHAN = PACIFIC BELL HANFORD         
SCETUL = SO CAL EDISON TULARE         SCGVIS = SO CAL GAS VISALIA           
VEREXR = VERIZON CAL EXETER           

PBTHA5 00003 USAN 09/13/02 06:15:24 0389878 NORMAL NOTICE

Message Number: 0389878 Received by USAN at 06:03 on 09/13/02 by AMC

Work Begins:    09/17/02 at 07:00   Notice: 020 hrs      Priority: 2

Caller:         JOSEPH RABAGO
Company:        OSMOSE WOOD PERSERVING
Address:        13704 HANFORD ARMONA RD
City:           HANFORD                       State: CA Zip: 93230
Telephone:      559-584-6116                  Fax: 559-584-6111
Alt #(s):       
Nature of Work: DIG TO TREAT UTIL POLES
Done for:       PGE                           
Foreman:        CHRIS KOCK                    Area Marked in White Paint
Permit Type:    NO                            
Location: 
    ENT AREA BOUNDED BY W. BULLARD AV ON N, N. BISHOP AV ON W, W. BARSTOW
    AV ON S, N. FLOYD AV ON E

Place: FRESNO, CO AREA              County: FRESNO               State: CA

Map Book: 
Page Grid
0000 000
Long/Lat Long: -120.007232 Lat:  36.814167 Long: -119.997306 Lat:  36.823738 
State Grid  E:  0 N:  0 E:   0 N:  0

Sent to:
ATTCAL = AT&T COMM CALIFORNIA         KRMTEL = XF KERMAN TELEPHONE CO.      
PBTHA3 = PACBELL HANFORD 3            PBTHAN = PACIFIC BELL HANFORD         
PGEFNO = PGE DISTR FRESNO             VEREXR = VERIZON CAL EXETER           

PBTHA5 00004 USAN 09/13/02 06:15:35 0389882 NORMAL NOTICE

Message Number: 0389882 Received by USAN at 06:08 on 09/13/02 by DRA

Work Begins:    09/17/02 at 07:00   Notice: 020 hrs      Priority: 2

Caller:         JOSEPH RABAGO
Company:        OSMOSE WOOD PRESERVING
Address:        13704 HANFORD ARMONA ROAD
City:           HANFORD                       State: CA Zip: 93230
Telephone:      559-584-6116                  Fax: 
Alt #(s):       661-345-9949
Nature of Work: DIG TO TREAT POLES
Done for:       PGE                           
Foreman:        CHRIS KOCH                    Area Marked in White Paint
Permit Type:    NO                            
Location: 
    AREA BOUND BY: W BARSTOW AVE ON N, N BIOLA AVE ON W, W SHAW AVE ON S
    N BISHOP AVE ON E

Place: FRESNO, CO AREA              County: FRESNO               State: CA

Map Book: 
Page Grid
0000 000
Long/Lat Long: -120.016326 Lat:  36.807220 Long: -120.006111 Lat:  36.815467 
State Grid  E:  0 N:  0 E:   0 N:  0

Sent to:
ATTCAL = AT&T COMM CALIFORNIA         ATTVIS = ATT BROADBAND - VISALIA      
KRMTEL = XF KERMAN TELEPHONE CO.      PBTHA3 = PACBELL HANFORD 3            
PBTHAN = PACIFIC BELL HANFORD         PGEFNO = PGE DISTR FRESNO             
VEREXR = VERIZON CAL EXETER           

PBTHA5 00005 USAN 09/13/02 06:15:46 0389883 NORMAL NOTICE

Message Number: 0389883 Received by USAN at 06:11 on 09/13/02 by DRA

Work Begins:    09/17/02 at 07:00   Notice: 020 hrs      Priority: 2

Caller:         JOSEPH RABAGO
Company:        OSMOSE WOOD PRESERVING
Address:        13704 HANFORD ARMONA ROAD
City:           HANFORD                       State: CA Zip: 93230
Telephone:      559-584-6116                  Fax: 
Alt #(s):       661-345-9949
Nature of Work: DIG TO TREAT POLES
Done for:       PGE                           
Foreman:        CHRIS KOCH                    Area Marked in White Paint
Permit Type:    NO                            
Location: 
    AREA BOUND BY: W SHAW AVE ON N, N 7TH AVE ON W, W GETTYSBERG AVE ON
    S, N BISHOP AVE ON E

Place: FRESNO, CO AREA              County: FRESNO               State: CA

Map Book: 
Page Grid
0000 000
Long/Lat Long: -120.014900 Lat:  36.800136 Long: -120.005889 Lat:  36.807994 
State Grid  E:  0 N:  0 E:   0 N:  0

Sent to:
ATTCAL = AT&T COMM CALIFORNIA         KRMTEL = XF KERMAN TELEPHONE CO.      
PBTHA3 = PACBELL HANFORD 3            PBTHAN = PACIFIC BELL HANFORD         
PGEFNO = PGE DISTR FRESNO             VEREXR = VERIZON CAL EXETER           

