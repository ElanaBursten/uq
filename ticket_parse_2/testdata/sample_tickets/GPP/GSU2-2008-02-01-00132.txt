
GSUPLS080320339-00	JC	2008/02/01 10:43:49	00117

 New Jersey One Call System        SEQUENCE NUMBER 0125   CDC = GPP

Transmit:  Date: 02/01/08   At: 1042

*** R O U T I N E         *** Request No.: 080320339

Operators Notified:
    BIT=/BRICK TWP MUA  / CC4=/CMCST-MNMTH|UTQ/ NJN=/NJ NATL GAS|UTQ/ 
    BAN=/VERIZON   |ECSM/ GPP=/JCP&L      |UTQ/ 

Start Date/Time:    02/07/08   At 0900   Expiration Date: 04/09/08

Location Information:
   County: OCEAN     Municipality: BRICK
   Subdivision/Community: LAKE RIVIERA
   Street:               322 HUDSON DR
   Nearest Intersection: PINETREE DR 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        PLANT TREE
   Extent of Work: CURB TO 20FT BEHIND CURB                 DEPTH: 2FT
   Remarks:

   Working For:  DAVID TOBER
   Address:      322 HUDSON DR
   City:         BRICK, NJ  08723
   Phone:        732-262-9189

Excavator Information:
   Caller:       DAVID TOBER             
   Phone:        732-262-9189            

   Excavator:    DAVID TOBER
   Address:      322 HUDSON DR
   City:         BRICK, NJ  08723
   Phone:        732-262-9189            Fax:  
   Cellular:     908-910-7494
   Email:        DTOBERSERVICES@AOL.COM
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2008/02/01 10:43AM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
080320339        0117  GSUPLS080320339-00   Regular Notice   2008/02/07 09:00AM

