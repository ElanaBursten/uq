
GSUPLS080320090-00	JC	2008/02/01 08:16:39	00028

 New Jersey One Call System        SEQUENCE NUMBER 0025   CDC = GPP
     
Transmit:  Date: 02/01/08   At: 0815

*** E M E R G E N C Y     *** Request No.: 080320090

Operators Notified:
    BIT=/BRICK TWP MUA  / CC4=/CMCST-MNMTH|UTQ/ NJN=/NJ NATL GAS|UTQ/ 
    BAN=/VERIZON   |ECSM/ GPP=/JCP&L      |UTQ/ 

Start Date/Time:    02/01/08   At 0811   Expiration Date: 

Location Information:
   County: OCEAN     Municipality: BRICK
   Subdivision/Community: LIONS HEAD NORTH
   Street:               15 LANCE DR
   Nearest Intersection: KENT DR 
   Other Intersection:   
   Lat/Long: 
   Type of Work :        REPAIR SWR LINE
   Extent of Work: CURB TO 20FT BEHIND CURB                 DEPTH: 6FT
   Remarks:
     BACKING INTO HOUSE

   Working For:  BARBARA AKER
   Address:      15 LANCE DR
   City:         BRICK, NJ  08723
   Phone:        732-920-1780

Excavator Information:
   Caller:       THOMAS GARON            
   Phone:        732-920-5721            

   Excavator:    GARON T. PLUMBING & HEATING
   Address:      409 CRESTVIEW TER
   City:         BRICK, NJ  08723
   Phone:        732-920-5721            Fax:  732-920-0334
   Cellular:     732-278-6072
   Email:        TNMGARON@AOL.COM
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2008/02/01 08:16AM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
080320090        0028  GSUPLS080320090-00   Emergency (Less  2008/02/01 08:11AM

