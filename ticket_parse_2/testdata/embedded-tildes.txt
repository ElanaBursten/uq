VUPSa 04/10/07 10:42:24 A710001009-00A          NORMAL 

Ticket No:  A710001009-00A                        NEW  GRID NORM LREQ 
Transmit        Date: 04/10/07   Time: 10:42 AM   Op: WTSMITH 
Call            Date: 04/10/07   Time: 10:37 AM 
Requested By    Date: 04/13/07   Time: 07:00 AM 
Update By       Date: 04/27/07   Time: 11:59 PM 
Due By          Date: 04/13/07   Time: 07:00 AM 
Expires         Date: 05/02/07   Time: 07:00 AM 
Old Tkt No: A710001009 
Original Call   Date: 04/10/07   Time: 10:37 AM   Op: WTSMITH 

City/Co:POWHATAN              Place:                                    State:VA 
Subdivision: MACON ORCHARD                             Lot: 2 
Address:     1705             Street: MACON ORCHARD DRIVE 

Type of Work:   OTHER EXCAVATION WORK - SEE DETAILS BELOW 
Work Done For:  CHARTER HOMES 
Location:       FINE GRADING LOT 

                ~~~~~PLEASE MARK THE ENTIRE PROPERTY WITH BOTH PAINT & FLAGS 
                ...........THANK YOU !! ~~~~~~ 
Instructions: 

Whitelined: N   Blasting: N   Boring: N 

Company:        DANNY SMITH INC/DANNY SMITH CONST             Type: CONT 
Co. Address:    9600 PRIDESVILLE RD 
City:           AMELIA  State:VA  Zip:23002 
Company Phone:  804-561-3501 
Contact Name:   TONIA SMITH                 Contact Phone:804-561-3501 
Call Back Time: 
Email:          dannysmithinc@tds.net 
Field Contact:  DANNY SMITH 
Fld. Contact Phone:804-337-2937 

Mapbook: 
Grids:    3731B7757B-41  3731C7757A-04  3731C7757B-00  3731C7757B-01 
Grids:    3731C7757B-02  3731C7757B-10  3731C7757B-11  3731C7757B-12 

Members: 
PCV156 = ADELPHIA-POWHATAN            SEC027 = SOUTHSIDE ELECTRIC COOPERATI 
VZN283 = VERIZON COMM. 

Seq No:   2080 A 
