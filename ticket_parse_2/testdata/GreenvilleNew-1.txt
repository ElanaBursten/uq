ZZQ35 2 PUPS Voice 1/21/2004 3:35:01 PM 0401210012 Emergency

Ticket Number: 0401210012
Old Ticket Number: 
Created By: WGS
Seq Number: 2

Created Date: 1/21/2004 8:04:11 AM
Work Date/Time: 1/22/2004 9:00:00 AM Hours Notice: 25
Update: 2/2/2004 Good Through: 2/5/2004

Excavation Information:
State: SC     County: LAURENS
Place: LAURENS
Address Number: 1
Street: TECHNOLOGY PL
Inters St: HWY 14
Subd: 

Type of Work: SEE REMARKS
Duration: 8 HOURS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: PIEDMONT RURAL TELEPHONE

Remarks/Instructions: BURYING NEW TELEPHONE CABLE//CERAMTEC//TECHNOLOGY       
PLACE//HWY 14 IS ALSO CHURCH ST//MARK PROPERTY BETWEEN BLDGS 1 & 2 WHERE      
ORANGE FLAGS ARE LOCATED//APPROX 200 FT//IF MORE INFO IS NEEDED CONTACT       
CALLER//                                                                      

Caller Information: 
Name: GENE WHITE                            PIEDMONT RURAL TELEPHONE              
Address: UNKNOWN
City: UNKNOWN State: SC Zip: 00000
Phone: (864) 682-3101 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:GENE WHITE Email: 
Call Back: Fax: 

Grids: 1879201; 1875637; 1872073; 1879202; 1875638; 1872074; 1868510; 1879203; 1875639; 1872075; 1868511; 1864947;
1879204; 1875640; 1872076; 1868512; 1864948; 1872077; 1868513; 1864949
Lat/Long: 34.5222446246052, -82.0487500315487
Secondary: 34.5179796403755, -82.0449393059944
Lat/Long Caller Supplied: N 

Members Involved: DPCZ07 LAU27

Map Link: (NEEDS DEVELOPEMNT)




ZZQ35 3 PUPS Remote 1/21/2004 3:35:01 PM 0401210094 Normal

Ticket Number: 0401210094
Old Ticket Number: 
Created By: HDJ
Seq Number: 3

Created Date: 1/21/2004 3:30:49 PM
Work Date/Time: 1/26/2004 3:30:56 PM Hours Notice: 72
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: GREENWOOD
Place: GREENWOOD
Address Number: 
Street: 602 E HENRIETTA AVE.
Inters St: SLIGH ST.
Subd: 

Type of Work: ELECTRIC, INSTALL SECONDARY
Duration: 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: CPW

Remarks/Instructions: MARK RIGHT SIDE OF HOUSE AREA BEEN FLAGED THIS IS 602   
E HENRIETTA AVE.                                                              

Caller Information: 
Name: FREDDIE W ANDERSON                    GREENWOOD CPW                         
Address: 
City:  State:  Zip: 
Phone:  Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:FREDDIE W ANDERSON Email: Fanderson@greenwoodcpw.com
Call Back: Fax: 

Grids: 2673896; 2670332; 2666768; 2673897; 2670333; 2666769
Lat/Long: 34.2086658322559, -82.155599670576
Secondary: 34.2084374268076, -82.1553712651278
Lat/Long Caller Supplied: N 

Members Involved: DPCZ04

Map Link: (NEEDS DEVELOPEMNT)



ZZQ35 8 PUPS Voice 1/21/2004 3:35:02 PM 0401210003 Resend

Ticket Number: 0401210003
Old Ticket Number: 
Created By: RMD
Seq Number: 8

Created Date: 1/21/2004 12:47:58 AM
Work Date/Time: 1/21/2004 1:00:00 AM Hours Notice: 0
Update: 1/30/2004 Good Through: 2/4/2004

Excavation Information:
State: SC     County: GREENVILLE
Place: TAYLORS
Address Number: 
Street: HWY 290
Inters St: MCELHANEY ROAD 
Subd: 

Type of Work: GAS, OTHER
Duration: UNKNOWN

Boring/Drilling: N Blasting: Y White Lined: N Near Railroad: N 

Work Done By: CINDY GIBBS

Remarks/Instructions: TYPE OF WORK / DIGGING A GAS VALVE // MARK BOTH SIDES   
OF THE ROAD FRONT THE INTERSECTION GOING IN EACH DIRECTION // ATTIONAL        
CONTACT 843-123-1234                                                          

Caller Information: 
Name: CINDY GIBBS                           GREER CPW                             
Address: 301 MCCALL ST
City: GREER State: SC Zip: 29650
Phone: (864) 848-5500 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:CINDY GIBBS Email: 
Call Back: Fax: 

Grids: 759806; 756242; 752678; 759807; 756243; 752679
Lat/Long: 34.9547413392071, -82.4639182312775
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CCGZ85 CCMGZ36 DPCZ02 PNGZ81 WOC22

Map Link: (NEEDS DEVELOPEMNT)


ZZQ35 5 PUPS Remote 1/21/2004 3:35:01 PM 0401210042 Normal

Ticket Number: 0401210042
Old Ticket Number: 
Created By: R-NJB
Seq Number: 5

Created Date: 1/21/2004 11:26:30 AM
Work Date/Time: 1/26/2004 11:30:18 AM Hours Notice: 72
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: SPARTANBURG
Place: SPARTANBURG
Address Number: 209
Street: SEDGEWOOD COURT
Inters St: STRANGE COURT
Subd: 

Type of Work: WATER, INSTALL SERVICE
Duration: 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: SPARTANBURG WATER SYSTEM

Remarks/Instructions: MARK BOTH SIDE ROAD AND ENTIRE PROPERTY                 

Caller Information: 
Name: NORMA BURGESS                         SPARTANBURG WATER SYSTEM              
Address: 301 SOUTH AVE
City: SPARTANBURG State: SC Zip: 29306
Phone: (864) 585-8296 Ext:  Type: Business
Fax: (864)596-4927 Caller Email: 

Contact Information:
Contact:NORMA BURGESS Email: NBURGESS@SWS-SSSD.ORG
Call Back: Fax: 

Grids: 842126; 845691; 842127; 838563; 845692; 842128; 838564
Lat/Long: 34.9211786666824, -81.9795076666647
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CCSZ76 DPCZ60 MCI18 PNSZ82 SWS58 XXS18

Map Link: (NEEDS DEVELOPEMNT)



ZZQ35 11 PUPS Remote 1/21/2004 3:35:03 PM 0401210018 Normal

Ticket Number: 0401210018
Old Ticket Number: 
Created By: R-LWB
Seq Number: 11

Created Date: 1/21/2004 8:51:35 AM
Work Date/Time: 1/26/2004 8:30:11 AM Hours Notice: 71
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: SPARTANBURG
Place: INMAN
Address Number: 511
Street: RIVER OAK ROAD
Inters St: SC HWY 9
Subd: RIVER OAK

Type of Work: WATER, REPAIR LEAK
Duration: 4 HOURS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: SPARTANBURG WATER SYSTEM

Remarks/Instructions: MARK BOTH SIDES OF ROAD AND FRONT OF PROPERTY           

Caller Information: 
Name: LYNN BAGWELL                          SPARTANBURG WATER SYSTEM              
Address: 301 SOUTH AVE
City: SPARTANBURG State: SC Zip: 29306
Phone: (864) 5858296 Ext:  Type: Business
Fax: (846) 5964927 Caller Email: 

Contact Information:
Contact:DAVID WRIGHT Email: LBAGWELL@SWS-SSSD.ORG
Call Back:(864) 8090892 FROM 7 AM AT 3 PM Fax: (864) 5964923

Grids: 414418; 410854; 414419; 410855; 414420; 410856
Lat/Long: 35.0886869178404, -82.0189668075117
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 DPCZ60 PNSZ82 SWS58

Map Link: (NEEDS DEVELOPEMNT)



ZZQ35 15 PUPS Voice 1/21/2004 3:35:04 PM 0401210006 Normal

Ticket Number: 0401210006
Old Ticket Number: 
Created By: SDE
Seq Number: 15

Created Date: 1/21/2004 7:14:21 AM
Work Date/Time: 1/26/2004 7:15:42 AM Hours Notice: 72
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: GREENWOOD
Place: GREENWOOD
Address Number: 
Street: HWY 72/ 221
Inters St: MILFORD SPRINGS RD
Subd: 

Type of Work: SEE REMARKS
Duration: 2DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: LAWRENCE CONSTRUCTION SERVICES

Remarks/Instructions: REPAIR CATCH BASIN//THE REPAIR IS IN THE SIDEWALK       
AREA//MARK A 20 FT RADIUS AROUND THE STORM DRAIN//STORM DRAIN IS MARKED       
WITH FLORESCENT PAINT//THE STORM DRAIN IS LOCATED AT DISCOUNT AUTO            
SALES//IN FRONT OF THE ARMORY NATIONAL GAURD ONLY                             

Caller Information: 
Name: JOHN TIMMERMAN                        LAWRENCE CONSTRUCTION SERVICES        
Address: 4510 HWY 25 S
City: GREENWOOD  State: SC Zip: 29646
Phone: (864) 993-8264 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:JOHN TIMMERMAN Email: 
Call Back: Fax: 

Grids:  
Lat/Long: 34.2236429838473, -82.1407837613804
Secondary: 34.2098141908957, -82.1176310095447
Lat/Long Caller Supplied: N 

Members Involved: DPCZ07 GWC38 NCT22 SUTZ12 VER06

Map Link: (NEEDS DEVELOPEMNT)




ZZQ35 9 PUPS Voice 1/21/2004 3:35:02 PM 0401210002 Normal

Ticket Number: 0401210002
Old Ticket Number: 
Created By: RMD
Seq Number: 9

Created Date: 1/21/2004 12:46:36 AM
Work Date/Time: 1/26/2004 1:00:04 AM Hours Notice: 72
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: GREENVILLE
Place: GREENVILLE
Address Number: 206
Street: HUNTERS CT
Inters St: HUNTERS CIRCLE/CALMAR COURT
Subd: HUNTERS RIDGE

Type of Work: GAS, INSTALL SERVICE
Duration: 3 HOURS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: PIEDMONT NATURAL GAS-GREENVILLE

Remarks/Instructions: LOT 25//INSTALL GAS SERVICE//  TEST ONLY                

Caller Information: 
Name: JANET TOMBLEY                         PIEDMONT NATURAL GAS-GREENVILLE       
Address: PO BOX 1905
City: GREENVILLE State: SC Zip: 29602
Phone: (864) 235-5488 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:JANET TOMBLEY Email: 
Call Back: Fax: (864) 235-5844

Grids: 777631; 774067; 770503; 766939; 777632; 774068; 770504; 766940; 777633; 774069; 770505
Lat/Long: 34.9490389919919, -82.4569566292159
Secondary: 34.9478913613193, -82.4558089985433
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CCGZ85 DPCZ02 PNGZ81 WOC22

Map Link: (NEEDS DEVELOPEMNT)



ZZQ35 10 PUPS Remote 1/21/2004 3:35:03 PM 0401210022 Normal

Ticket Number: 0401210022
Old Ticket Number: 
Created By: R-RHO
Seq Number: 10

Created Date: 1/21/2004 9:20:32 AM
Work Date/Time: 1/26/2004 9:30:00 AM Hours Notice: 72
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: GREENVILLE
Place: PIEDMONT
Address Number: 
Street: OLD GUNTER RD
Inters St: HWY 86
Subd: 

Type of Work: GAS, MAINTENANCE ON PIPELINE
Duration: 4 HOURS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: RHONDA ORR

Remarks/Instructions: MARK ENTIRE INTERSECTION                                

Caller Information: 
Name: RHONDA ORR                            FORT HILL NATURAL GAS AUTHORITY       
Address: PO BOX 189
City: EASLEY State: SC Zip: 29641
Phone: (864) 850-7124 Ext:  Type: Business
Fax: (864) 850-7265 Caller Email: 

Contact Information:
Contact:RHONDA ORR Email: 
Call Back: Fax: (864) 850-7265

Grids: 1412049; 1408485; 1415614; 1412050; 1408486; 1412051; 1408487
Lat/Long: 34.6996418935743, -82.4199796767068
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 DPCZ25 LAU27 PNGZ81

Map Link: (NEEDS DEVELOPEMNT)


ZZQ35 7 PUPS Voice 1/21/2004 3:35:02 PM 0401210046 Normal

Ticket Number: 0401210046
Old Ticket Number: 
Created By: MAC
Seq Number: 7

Created Date: 1/21/2004 12:46:06 PM
Work Date/Time: 1/26/2004 12:45:00 PM Hours Notice: 71
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: GREENVILLE
Place: GREENVILLE
Address Number: 101
Street: COUNTRY CLUB DR
Inters St: AUGUSTA ST
Subd: 

Type of Work: LANDSCAPE, PLANT TREE(S)
Duration: 4 DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: TREE SPADERS OF GREENVILLE

Remarks/Instructions: MARK THE ENTIRE PROPERTY                                

Caller Information: 
Name: CRAIG RICE                            TREE SPADERS OF GREENVILLE            
Address: 411 BRIDGES ROAD
City: SIMPSONVILLE State: SC Zip: 29681
Phone: (864) 979-0485 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:CRAIG RICE Email: 
Call Back: Fax: 

Grids: 1116264; 1112700; 1109136; 1116265; 1112701; 1109137; 1116266; 1112702
Lat/Long: 34.815764557112, -82.3827131219694
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CGR12 DPCZ02 PNGZ81

Map Link: (NEEDS DEVELOPEMNT)



ZZQ35 6 PUPS Remote 1/21/2004 3:35:02 PM 0401210043 Normal

Ticket Number: 0401210043
Old Ticket Number: 
Created By: R-NJB
Seq Number: 6

Created Date: 1/21/2004 11:33:01 AM
Work Date/Time: 1/26/2004 11:30:59 AM Hours Notice: 72
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: SPARTANBURG
Place: SPARTANBURG
Address Number: 108
Street: DOVER ROAD
Inters St: POWELL MILL ROAD
Subd: 

Type of Work: WATER-WELL, REPAIR
Duration: 1 DAY

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: SPARTANBURG WATER SYSTEM

Remarks/Instructions: MARK BOTH SIDE ROAD AND ENTIRE PROPERTY                 

Caller Information: 
Name: NORMA BURGESS                         SPARTANBURG WATER SYSTEM              
Address: 301 SOUTH AVE
City: SPARTANBURG State: SC Zip: 29306
Phone: (864) 585-8296 Ext:  Type: Business
Fax: (864)596-4927 Caller Email: 

Contact Information:
Contact:NORMA BURGESS Email: NBURGESS@SWS-SSSD.ORG
Call Back: Fax: 

Grids: 792234; 788670; 785106; 792235; 788671; 785107; 792236; 788672; 785108
Lat/Long: 34.9429629410506, -81.9750087436269
Secondary: 34.9414188091582, -81.9734646117345
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CCSZ76 DPCZ60 PNSZ82 SWS58

Map Link: (NEEDS DEVELOPEMNT)


ZZQ35 16 PUPS Voice 1/21/2004 3:35:04 PM 0401210009 Normal

Ticket Number: 0401210009
Old Ticket Number: 
Created By: SDE
Seq Number: 16

Created Date: 1/21/2004 7:52:22 AM
Work Date/Time: 1/26/2004 7:30:24 AM Hours Notice: 71
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: YORK
Place: LAKE WYLIE
Address Number: 6
Street: SUNRISE POINT CT
Inters St: SUNRISE POINT RD
Subd: RIVER HILLS SUBD

Type of Work: SEE REMARKS
Duration: 1WKS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: PARAMOUNT PROPERTIES

Remarks/Instructions: FOOTINGS//RETAINING WALLS,GRADING//MARK THE ENTIRE      
PROPERTY                                                                      

Caller Information: 
Name: MIKE MCKINNEY                         PARAMOUNT PROPERTIES                  
Address: 5 MISCHIEF
City: LAKE WYLIE State: SC Zip: 29710
Phone: (704) 239-7092 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:MIKE MCKINNEY Email: 
Call Back: Fax: 

Grids: 383037; 379473; 375909; 383038; 379474; 375910
Lat/Long: 35.1019046105576, -81.0543062953228
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 DPCZ18 RHT33 YCWS68 YEC42 YOR56

Map Link: (NEEDS DEVELOPEMNT)

ZZQ35 13 PUPS Voice 1/21/2004 3:35:03 PM 0401210069 Normal

Ticket Number: 0401210069
Old Ticket Number: 
Created By: WGS
Seq Number: 13

Created Date: 1/21/2004 2:29:16 PM
Work Date/Time: 1/26/2004 8:15:25 AM Hours Notice: 66
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: YORK
Place: FORT MILL
Address Number: 113
Street: SPRINGBRANCH
Inters St: NIMS LAKE ROAD
Subd: SPRINGBRANCH GLEN

Type of Work: GRADING
Duration: 2 DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: KEITH HALL

Remarks/Instructions: LOT 46//MARK THE FRONT OF PROPERTY ADJACENT TO THE RD   
AND ALONG RD FRONTAGE//                                                       

Caller Information: 
Name: KEITH HALL                            KEITH HALL                            
Address: 11511 PROVIDENCE ROAD WEST
City: CHARLOTTE State: NC Zip: 28277
Phone: (704) 562-2707 Ext:  Type: Home
Fax:  Caller Email: 

Contact Information:
Contact:KEITH HALL Email: 
Call Back: Fax: 

Grids: 
Lat/Long: 34.9912793470035, -80.9401070977978
Secondary: 34.9855758769686, -80.9295077287065
Lat/Long Caller Supplied: N 

Members Involved: DPCZ72F FMT20 TWJZ54 YEC42 YOR56

Map Link: (NEEDS DEVELOPEMNT)

ZZQ35 12 PUPS Remote 1/21/2004 3:35:03 PM 0401210001 Normal

Ticket Number: 0401210001
Old Ticket Number: 
Created By: RMD
Seq Number: 12

Created Date: 1/21/2004 12:45:11 AM
Work Date/Time: 1/26/2004 12:45:57 AM Hours Notice: 72
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: GREENVILLE
Place: GREENVILLE
Address Number: 12
Street: N HWY 25
Inters St: TIGERVILLE RD
Subd: 

Type of Work: GAS, INSTALL ANODES
Duration: 8 HOURS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: PIEDMONT NATURAL GAS,CO

Remarks/Instructions: PUTTING IN A ANODE BED // TEST TICKET THIS MAY NOT BE   
YOUR AREA                                                                     

Caller Information: 
Name: JOHN PLUMBLEE OR STANLEY NIX          PIEDMONT NATURAL GAS, CO              
Address: PO BOX 1905
City: GREENVILLE State: SC Zip: 29602
Phone: 864-234-6891 Ext: 3125 Type: Business
Fax: N/A Caller Email: 

Contact Information:
Contact:JOHN PLUMBLEE OR STANLEY NIX Email: N/A
Call Back:864-234-6891 Fax: N/A

Grids: 1048557; 1044993; 1041429; 1048558; 1044994; 1041430; 1044995
Lat/Long: 34.842611443662, -82.3704787605634
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CGR12 DPCZ02 PNGZ81 WOC22

Map Link: (NEEDS DEVELOPEMNT)


ZZQ35 14 PUPS Voice 1/21/2004 3:35:03 PM 0401210076 Normal

Ticket Number: 0401210076
Old Ticket Number: 
Created By: RLB
Seq Number: 14

Created Date: 1/21/2004 2:53:25 PM
Work Date/Time: 1/26/2004 3:00:00 PM Hours Notice: 71
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: PICKENS
Place: EASLEY
Address Number: 
Street: HWY 123
Inters St: EVELYN RD
Subd: 

Type of Work: SEPTIC SYSTEM, INSTALL
Duration: 6 MONTHS

Boring/Drilling: Y Blasting: Y White Lined: N Near Railroad: N 

Work Done By: KYLE MOOREHEAD

Remarks/Instructions: MARK BOTH SIDES OF HWY 123 FROM EVELYN RD TO EDENS RD   
                                                                              

Caller Information: 
Name: NANCY PAGE                            DON MOORHEAD CONSTRUCTION             
Address: 1513 ANDERSON STREET
City: BELTON State: SC Zip: 29627
Phone: (864) 934-9128 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:KYLE MOOREHEAD Email: 
Call Back:864-934-9128 Fax: 

Grids: 1076966; 1073402; 1076967; 1073403; 1076968; 1073404; 1076969; 1073405; 1076970; 1073406; 1076971; 1073407;
1076972; 1073408
Lat/Long: 34.8307465173502, -82.5129185977918
Secondary: 34.830304170347, -82.5079895883281
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 DPCZ25 FTH63 POW54

Map Link: (NEEDS DEVELOPEMNT)



ZZQ35 4 PUPS Remote 1/21/2004 3:35:01 PM 0401210052 Normal

Ticket Number: 0401210052
Old Ticket Number: 
Created By: R-RHO
Seq Number: 4

Created Date: 1/21/2004 1:50:58 PM
Work Date/Time: 1/26/2004 1:45:00 PM Hours Notice: 71
Update: 2/4/2004 Good Through: 2/9/2004

Excavation Information:
State: SC     County: GREENVILLE
Place: PIEDMONT
Address Number: 1111
Street: OLD GUNTER RD
Inters St: BESSIE ROAD
Subd: 

Type of Work: GAS, INSTALL SERVICE
Duration: 4 HOURS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: RHONDA ORR

Remarks/Instructions: MARK FRONT OF PROPERTY                                  

Caller Information: 
Name: RHONDA ORR                            FORT HILL NATURAL GAS AUTHORITY       
Address: PO BOX 189
City: EASLEY State: SC Zip: 29641
Phone: (864) 850-7124 Ext:  Type: Business
Fax: (864) 850-7265 Caller Email: 

Contact Information:
Contact:RHONDA ORR Email: 
Call Back: Fax: (864) 850-7265

Grids: 1408488; 1404924; 1401360; 1408489; 1404925; 1401361
Lat/Long: 34.701855002008, -82.416844439759
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CCOPZ90 DPCZ25 LAU27 PNGZ81

Map Link: (NEEDS DEVELOPEMNT)

ZZQ35 280 PUPS Voice 2/5/2004 12:00:54 PM 0402050945 Normal

Ticket Number: 0402050945
Old Ticket Number: 
Created By: KML
Seq Number: 280

Created Date: 2/5/2004 12:00:48 PM
Work Date/Time: 2/10/2004 11:45:00 AM Hours Notice: 71
Update: 2/19/2004 Good Through: 2/24/2004

Excavation Information:
State: SC     County: GREENVILLE
Place: GREER
Address Number: 
Street: WESTMORELAND DR
Inters St: HWY 14
Subd: 

Type of Work: ROAD CONSTRUCTION, PAVE/WIDEN
Duration: UNKNOWN

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N 

Work Done By: ASHMORE BROTHERS

Remarks/Instructions: UNSUSPENDED DUE TO CALLER CALLED BACK WITH INFO     
NEEDED TO PROCESS TICKET/ STARTING @ THE INTERSECTION OF HWY 14 // MARK   
BOTH SIDES OF WESTMORELAND RD TO THE INTERSECTION WITH ABNER CREEK RD //  
CALLER IS UNSURE OF THE FOOTAGE/                                          

Caller Information: 
Name: CARLETON ASHMORE                      ASHMORE BROTHERS              
Address: 1880 S HWY 14
City: GREER State: SC Zip: 29652
Phone: (864) 879-8294 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:CARLETON ASHMORE Email: 
Call Back: Fax: 

Grids: 995205; 991641; 988077; 998770; 995206; 991642; 988078; 998771;
995207; 991643; 998772; 995208
Lat/Long: 0, 0
Secondary: 0, 0
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CCSZ76 DPCZ08 GRR01                              


Map Link: (NEEDS DEVELOPEMNT)



ZZQ35 485 PUPS U.S. Mail 3/4/2004 1:19:56 PM 0403032355 Update

Ticket Number: 0403032355
Old Ticket Number: 0402182132
Created By: JDB
Seq Number: 485

Created Date: 3/3/2004 3:18:57 PM
Work Date/Time: 3/8/2004 3:30:53 PM Hours Notice: 72
Update: 3/17/2004 Good Through: 3/22/2004

Excavation Information:
State: SC     County: GREENVILLE
Place: GREER
Address Number: 11-19
Street: LAURELHURST COURT
Inters St: KINGSCREEK DR
Subd: CARMEN GLEN

Type of Work: CATV, INSTALL MAIN
Duration: 3 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N 

Work Done By: R-COMCON INCORPORTED

Remarks/Instructions: MARK THE FRONT OF THE PROPERTIES AND BOTH SIDES OF      
THE ROAD// THESE ARE SIDE BY SIDE//                                           

Caller Information: 
Name: TURK HENNES                           R-COMCON INCORPORTED                  
Address: PO BOX 1225
City: MAULDIN State: SC Zip: 29662
Phone: (864) 879-7510 Ext:  Type: Business
Fax: (864) 879-7510 Caller Email: 

Contact Information:
Contact:TURK HENNES Email: 
Call Back: Fax: (864) 879-7510

Grids: 
Lat/Long: 34.9200792339078, -82.2622699026729
Secondary: 34.911137318675, -82.2525242197787
Lat/Long Caller Supplied: N 

Members Involved: BSZT29 CCGZ85 DPCZ08 GRR01 QWC42                            


Map Link: (NEEDS DEVELOPEMNT)




