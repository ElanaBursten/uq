<?xml version="1.0"?>
<!-- 
Stylesheet : VA.xsl
Description: Transformation for Virginia XML tickets
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:output method="text" encoding="UTF-8" />
    <xsl:template match="/">
    <xsl:value-of select="//delivery/center"/>
    <xsl:text>                 </xsl:text>
    <xsl:value-of select="//Header"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>&#xA;Ticket No: </xsl:text>
    <xsl:value-of select="//tickets/ticket"/>
    <xsl:text>   Rev: </xsl:text>
    <xsl:value-of select="//tickets/revision"/>
    <xsl:text>  Type: </xsl:text>
    <xsl:value-of select="//tickets/priority"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="//tickets/type"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="//tickets/lookup"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="//tickets/category"/>
    <xsl:text>&#xA;  Send To: </xsl:text>
    <xsl:value-of select="//delivery/recipient"/>
    <xsl:text>    Seq No: </xsl:text>
    <xsl:value-of select="//delivery/seq_num"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>&#xA;Transmit      Time: </xsl:text>
    <xsl:value-of select="//delivery/transmitted"/>
    <xsl:text>    Op: </xsl:text>
    <xsl:value-of select="//Transmission/Operator"/>
    <xsl:text>&#xA;Original Call Time: </xsl:text>
    <xsl:value-of select="//tickets/started"/>
    <xsl:text>    Op: </xsl:text>
    <xsl:value-of select="//OrigCall/Operator"/>
    <xsl:text>&#xA;Work to Begin Time: </xsl:text>
    <xsl:value-of select="//tickets/replace_by_date"/>
    <xsl:text>&#xA;Due By:             </xsl:text>
    <xsl:value-of select="//tickets/response_due"/>
    <xsl:text>&#xA;Meet Date/Time:     </xsl:text>
    <xsl:value-of select="//tickets/meet_date"/>
    <xsl:text>&#xA;Old Tkt Number: </xsl:text>
    <xsl:value-of select="//tickets/original_ticket"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>&#xA;State: </xsl:text>
    <xsl:value-of select="//tickets/state"/>
    <xsl:text>            County: </xsl:text>
    <xsl:value-of select="//tickets/county"/>
    <xsl:text>     Place: </xsl:text>
    <xsl:value-of select="//tickets/place"/>
    <xsl:text>&#xA;Subdivision:  </xsl:text>
    <xsl:value-of select="//tickets/subdivision"/>
    <xsl:text>     Lots: </xsl:text>
    <xsl:value-of select="//tickets/lot"/>
    <xsl:text>&#xA;Address From: </xsl:text>
    <xsl:value-of select="//tickets/st_from_address"/>
    <xsl:text>  To: </xsl:text>
    <xsl:value-of select="//tickets/st_to_address"/>
    <xsl:text>  Street: </xsl:text>
    <xsl:value-of select="//tickets/street"/>
    <xsl:text>&#xA;Cross 1: </xsl:text>
    <xsl:value-of select="//tickets/cross1"/>
    <xsl:text>&#xA;Cross 2: </xsl:text>
    <xsl:value-of select="//tickets/cross2"/>
 	<xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates select="//Grid/TRSQ"/>
    <xsl:text>&#xA;Lat/Long: </xsl:text>
    <xsl:value-of select="//tickets/centroid/coordinate/latitude"/> 
    <xsl:text>  </xsl:text>
    <xsl:value-of select="//tickets/centroid/coordinate/longitude" />
    <xsl:text>&#xA;Work Done For: </xsl:text>
    <xsl:value-of select="//tickets/done_for"/>
    <xsl:text>&#xA;Type of Work: </xsl:text>
    <xsl:value-of select="//tickets/work_type"/>
    <xsl:text>&#xA;Whitelined: </xsl:text>
    <xsl:value-of select="//tickets/white_paint"/>
    <xsl:text>  Blasting: </xsl:text>
    <xsl:value-of select="//tickets/blasting"/>
    <xsl:text>  Boring: </xsl:text>
    <xsl:value-of select="//tickets/boring"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>&#xA;Location of Work: </xsl:text>
    <xsl:value-of select="//tickets/location"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>&#xA;Remarks: </xsl:text>
    <xsl:value-of select="//tickets/remarks"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>&#xA;Map Book: </xsl:text>
    <xsl:value-of select="//tickets/map_reference"/>
    <xsl:text>&#xA;Grids:</xsl:text> 
    <xsl:apply-templates select="//tickets/gridlist/grid"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>&#xA;Company       : </xsl:text>
    <xsl:value-of select="//tickets/name"/>
    <xsl:text>&#xA;Caller Address: </xsl:text>
    <xsl:value-of select="//tickets/address1"/>
    <xsl:text>&#xA;Caller City   : </xsl:text>
    <xsl:value-of select="//tickets/city"/>
    <xsl:text>  State: </xsl:text>
    <xsl:value-of select="//tickets/cstate"/>
    <xsl:text>  Zip: </xsl:text>
    <xsl:value-of select="//tickets/zip"/>
    <xsl:text>&#xA;Company Phone : </xsl:text>
    <xsl:value-of select="//tickets/phone"/>
    <xsl:text>&#xA;Contact Name: </xsl:text>
    <xsl:value-of select="//tickets/caller"/>
    <xsl:text>      Contact Phone: </xsl:text>
    <xsl:value-of select="//tickets/caller_phone"/>
    <xsl:text>&#xA;Alt. Contact: </xsl:text>
    <xsl:value-of select="//Caller/AltContact/Name"/>
    <xsl:text>      Phone: </xsl:text>
    <xsl:value-of select="//Caller/AltContact/Phone"/>
    <xsl:text>&#xA;Email:  </xsl:text>
    <xsl:value-of select="//tickets/email" />
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>&#xA;Notified Members:</xsl:text>
    <xsl:apply-templates select="//tickets/memberlist/memberitem"/>
    </xsl:template>

    <xsl:template match="//PolyPoint">
      <xsl:text>&#xA;  Lat : </xsl:text><xsl:value-of select="Lat"/>
      <xsl:text>&#x9;  Long: </xsl:text><xsl:value-of select="Lon"/>
    </xsl:template>

    <xsl:template match="//tickets/memberlist/memberitem">
      <xsl:text>&#xA;  </xsl:text><xsl:value-of select="member"/>
      <xsl:text> -- </xsl:text><xsl:value-of select="description"/>
    </xsl:template>

    <xsl:template match="//tickets/gridlist/grid">
      <xsl:text>&#xA;  </xsl:text>
      <xsl:value-of select="." />
    </xsl:template>

</xsl:stylesheet>  

