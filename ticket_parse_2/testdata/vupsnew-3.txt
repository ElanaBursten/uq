

WGL904 00257 VUPSa 01/09/07 10:21:49 A700900767-00A          UPDATE

Ticket No:  A700900767-00A                        UPDT GRID NORM LREQ
Transmit        Date: 01/09/07   Time: 10:21 AM   Op: 1MDW
Call            Date: 01/09/07   Time: 10:20 AM
Requested By    Date: 01/16/07   Time: 07:00 AM
Update By       Date: 01/30/07   Time: 11:59 PM
Due By          Date: 01/16/07   Time: 07:00 AM
Expires         Date: 02/02/07   Time: 07:00 AM
Old Tkt No: A635200285
Original Call   Date: 12/18/06   Time: 08:38 AM   Op: 1JOL

City/Co:FAIRFAX               Place:CLIFTON                             State:VA
Address:                      Street: TWIN LAKES DR
Cross 1:     UNION MILL RD                                     Intersection: N

Type of Work:   CLEARING EARTH AND ROCK/SANITARY, STORM, WATER MAIN-INSTALL
Work Done For:  WHITENER AND JACKSON
Location:       BEGINNING AT SOUTH SIDE OF TWIN LAKES DR AT ABOVE INTERSECTION,
                MARK SOUTHEAST ALONG TWIN LAKES DR 720 FT TO PROPERTY CORNER.
                THEN WEST 550 FT TO PROPERTY LINE THEN NORTH 450 FT TO STARTING
                POINT AND ALL PROPERTIES WITHIN THIS AREA
Instructions:   CALLER MAP REF: 19C2 C3 D3

Whitelined: N   Blasting: Y   Boring: N

Company:        PAUL O'MEARA CONSTRUCTION                     Type: CONT
Co. Address:    14955 DUMFRIES ROAD
City:           MANASSAS  State:VA  Zip:20112
Company Phone:  703-221-1882
Contact Name:   SUE DEVINE                  Contact Phone:703-221-1882
Call Back Time:                  Contact Fax  :703-730-5382
Field Contact:  ED REYNOLDS
Fld. Contact Phone:571-238-8754

Mapbook:  19C2
Grids:    3849D7724A     3849D7724B

Members:
COX901 = COX INET FIBER               COX902 = COX COMMUNICATIONS
DOM400 = DOMINION VIRGINIA POWER      FCU901 = FAIRFAX COUNTY DPW SEWER
FCW902 = FAIRFAX WATER                NVE901 = NORTHERN VIRGINIA ELEC COOP
VZN906 = VERIZON                      WGBLA1 = WASHINGTON GAS BLASTING
WGL904 = WASHINGTON GAS               WGLBLA = WASHINGTON GAS BLASTING

Seq No:   257 A
