
Ticket No:  4065047               48 HOUR NOTICE
Send To: TEST04     Seq No:    4  Map Ref:

Transmit      Date:  4/14/04   Time:  3:04 PM    Op: ethan
Original Call Date:  4/14/04   Time:  2:49 PM    Op: ethan
Work to Begin Date:  4/16/04   Time:  3:00 PM

State: OR            County: LANE                    Place: EUGENE
Address:    310      Street: FOXTAIL DR
Nearest Intersecting Street: HERITAGE OAKS DR

Twp: 18S   Rng: 3W    Sect-Qtr: 19-NE
Twp: 18S   Rng: 3W    Sect-Qtr: 19-NE
Ex. Coord NW Lat: 43.9948530Lon: -123.0915570SE Lat: 43.9941210Lon: -123.0896610

Type of Work: REPAIR ELEC SERVICE
Location of Work: ADD IS 4PLEX WITH 312,314,316. ABV ADD IS APX 50
: FT SE OF INTER. MARK ENTIRE LOT. CLLR REQUESTS LOCATORS CALL RES MANAGER
: BEFORE MARKING FOR MORE IN DEPTH INSTRUCTIONS

Remarks:
: CALLER GAVE TOWNSHIP, RANGE, SECTION AND 1/4 SCT INFO
Overhead Lines: N

Company     : CAMPBELL ELECTRIC                Best Time:
Contact Name: GLEN CAMPBELL                    Phone: (541)744-0705
Email Address:
Alt. Contact: RESIDENT MANAGER                 Phone: (541)687-1318
Contact Fax :
Work Being Done For: FOREST VILLAGE APT COMPLEX
Additional Members:
EUGENE01   EWEB01     LCEC01     NNG08      TCI02      USW01


Ticket No:  4065063               48 HOUR NOTICE
Send To: TEST04     Seq No:    5  Map Ref:

Transmit      Date:  4/14/04   Time:  3:04 PM    Op: tom
Original Call Date:  4/14/04   Time:  2:58 PM    Op: tom
Work to Begin Date:  4/16/04   Time:  3:00 PM

State: OR            County: LANE                    Place: EUGENE
Address:             Street: BERNTZEN ROAD
Nearest Intersecting Street: ROYAL AVE

Twp: 17S   Rng: 4W    Sect-Qtr: 22-SW-SE,27-NW-NE
Twp:       Rng:       Sect-Qtr:
Ex. Coord NW Lat: 44.0700510Lon: -123.1583750SE Lat: 44.0660840Lon: -123.1578100

Type of Work: REPL WATER MAIN
Location of Work: MARK FROM SW CORNER OF ABV INTER APX 1350FT S ON BOTH
: SIDES OF BURNTZEN ROAD TO NW CORNER OF BURNTZEN RD AND ELMIRA ROAD. AREA
: MARKED IN WHITE.

Remarks: CALLER GAVE TRSQ AS 17 04 22
:
Overhead Lines: N

Company     : E.W.E.B.                         Best Time:
Contact Name: RICK SITOWSKI                    Phone: (541)228-0551
Email Address:
Alt. Contact: TODD SIMMONS                     Phone: (541)228-0552
Contact Fax :?(541)371-4591
Work Being Done For: E.W.E.B.
Additional Members:
EUGENE01   EWEB01     NNG08      TCI02      USW01

Ticket No:  4065071               48 HOUR NOTICE            UPDATE
Send To: TEST04     Seq No:    6  Map Ref:
Update Of:  4040324
Transmit      Date:  4/14/04   Time:  3:04 PM    Op: tesa
Original Call Date:  4/14/04   Time:  3:01 PM    Op: tesa
Work to Begin Date:  4/16/04   Time:  3:15 PM

State: OR            County: CLACKAMAS               Place: MILWAUKIE
Address:   6790      Street: SE MONROE ST
Nearest Intersecting Street: 72ND ST

Twp: 1S    Rng: 2E    Sect-Qtr: 32-NW
Twp:       Rng:       Sect-Qtr:
Ex. Coord NW Lat: 45.4449720Lon: -122.5970520SE Lat: 45.4442120Lon: -122.5922720

Type of Work: INSTALL ALL UTILITY SERVICES
Location of Work: ADD IS APX 1/4MI W OF INTER.  MARK BOTH SIDES OF MONROE
: ST IN FRONT OF ABV ADD. MARKING NORTHSIDE OF MONROE  TO SOUTH SIDE OF
: MONROE AND ALL ROW AND EASEMENTS..

Remarks: 2ND UPDATE-NEEDS NEW MARKS
:
Overhead Lines: Y

Company     :                                  Best Time:
Contact Name: VITALLY BATRANCHUK               Phone: (503)740-0520
Email Address:
Alt. Contact: MARIN NITA                       Phone: (503)209-0941
Contact Fax :
Work Being Done For: VITALLY BATRANCHUK
Additional Members:
CCDOT01    CCDOT02    CWD01      JNS01      NNG01      PGE01      TCI26
 USW31



