
Ticket No:  7033524               48 HOUR NOTICE            UPDATE 
Send To: TEST03     Seq No:    2  Map Ref:  
Update Of:  7009374 
Transmit      Date:  2/26/07   Time:  5:52 AM    Op: mary 
Original Call Date:  2/24/07   Time:  4:16 PM    Op: webusr 
Work to Begin Date:  2/28/07   Time:  8:00 AM 

State: OR            County: LINCOLN                 Place: LINCOLN CITY 
Address:   3207      Street: NW KEEL AVE 
Nearest Intersecting Street: 31ST PL 

Twp: 07S   Rng: 11W   Sect-Qtr: 3-SE 
Twp: 7S    Rng: 11W   Sect-Qtr: 10-NE,3-SE 

Type of Work: INSTALL GAS SERVICE
Location of Work: ADDRESS IS APPROX 150 FT N OF ABOVE INTERS ON W SIDE OF NW
: KEEL AVE. LOCATE AREA MARKED IN WHITE. 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION AND 1/4 SCT INFO
: LOCATES DISTURBED PLEASE UPDATE 

Company     : NW NATURAL                       Best Time:   
Contact Name: SHERRY HARDING                   Phone: (541)994-2111  Ext.: 8540 
Alt. Contact: BRYAN COFFMAN                    Phone: (541)994-9612 
Work Being Done For: VINTAGE
Additional Members:  
COLC01     EMBOR01    FALCON07   NNG16      PPL18




Ticket No:  7033573               48 HOUR NOTICE            UPDATE 
Send To: TEST03     Seq No:    3  Map Ref:  
Update Of:  6234841 
Transmit      Date:  2/26/07   Time:  6:49 AM    Op: matthe 
Original Call Date:  2/26/07   Time:  6:47 AM    Op: matthe 
Work to Begin Date:  2/28/07   Time:  8:00 AM 

State: OR            County: HOOD RIVER              Place: HOOD RIVER 
Address:             Street: N 8TH ST 
Nearest Intersecting Street: RIVERSIDE DR 

Twp: 3N    Rng: 10E   Sect-Qtr: 26-NE,25 
Twp:       Rng:       Sect-Qtr:        

Type of Work: INSTALL WATER MAIN
Location of Work: MARK FROM ABV INTER APX 450FT W ALONG RAILROAD TRACKS BTWN
: SEWER PLANT FENCE AND I-84 INCL OTHER SIDE OF I-84 TO SPRINT BUILDING FENCE,
: APX 500FT S INTO PROP.  UPDT TICKET -- MARKS ARE GONE -- NEEDS REMARKS 

Remarks:  
: ANY QUESTIONS CONTACT CALLER 

Company     : R AND G EXCAVATING               Best Time:   
Contact Name: IAN SLOANE                       Phone: (503)394-2190 
Alt. Contact:                                  Phone:  
Work Being Done For: CITY OF HOOD RIVER 
Additional Members:  
COHR01     EMBOR01    FALCON03   HREC01     NNG06      ODOTRE01   PPL02




Ticket No:  7053416               2 FULL BUSINESS DAYS 
Send To: TEST03     Seq No:    4  Map Ref:  

Transmit      Date:  2/27/07   Time: 10:39 AM    Op: betsy 
Original Call Date:  2/27/07   Time: 10:31 AM    Op: betsy 
Work to Begin Date:  3/02/07   Time: 12:00 AM 

State: WA            County: BENTON                  Place: PROSSER 
Address: 167902      Street: W KINGTULL ROAD 
Nearest Intersecting Street: GRIFFIN 

Twp: 9N      Rng: 24E     Sect-Qtr: 20-SW,19-SE,30-NE,29-NW 
Twp:         Rng:         Sect-Qtr:        

Type of Work: LANDSCAPE/INSTALL SPRINKLER SYSTEM 
Location of Work: ADD APX 1/4MI FROM ABV INTER. ADD IS BTWN GRIFFIN AND
: YAKIMA VALLEY HWY. COMING FROM HWY, ADD ON RIGHT SIDE FO ROAD. MARK ALL LAWN
: AREAS INSIDE WHITE WOOD FENCE ON NE CORNER (SHOP SIDE) OF PROP AT ABV ADD 

Remarks:  
: BEST INFORMATION AVAILABLE 

Company     : TURFPRO TREE AND LAWN CARE       Best Time:   
Contact Name: DELANO JENNINGS                  Phone: (509)452-5296 
Alt. Contact: CHARLES -- CELL                  Phone: (509)728-3871 
Contact Fax :  
Work Being Done For: CHARLES JENNINGS
Additional Members:  
BENDPW01   BENPUD02   BENREA01   CHRTER01   EMBWA01    EMBWA02    WGPWA02 
 XVID01



