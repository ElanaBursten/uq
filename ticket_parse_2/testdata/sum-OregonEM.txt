
0
Date/Time :  2-24-07 at  0:22 

Oregon One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON 02/23/07

Date/Time: 02/24/2007 12:15:03 AM
Receiving Terminal: TEST03

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #

----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  7032795-48HR |   5  7033049-48HR |   9  7033277-48HR |  13  7033459-48HR |
   2  7032880-48HR |   6  7033083-48HR |  10  7033281-48HR |  14  7033479-48HR |
   3  7032990-48HR |   7  7033129-48HR |  11  7033415-48HR |  15  7033484-48HR |
   4  7032994-48HR |   8  7033141-SURV |  12  7033431-48HR |

* indicates ticket # is repeated

Total Tickets: 15

48HR - 48 HOUR NOTICE                 |   14

SURV - PRE-SURVEY                     |    1


Please call (503) 232-1987 if this data does not match the tickets you
received on 02/23/07




0
Date/Time :  2-17-07 at  0:25 

Washington One Call
SUMM
DAILY AUDIT OF TICKETS SENT ON 02/16/07

Date/Time: 02/17/2007 12:15:03 AM
Receiving Terminal: TEST03

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #

----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  7043629-48HR |   5  7043687-48HR |   8  7044136-48HR |  11  7044305-48HR |
   2  7043422-SURV |   6  7043669-48HR |   9  7044021-EMER |  12  7044324-48HR |
   3  7043643-48HR |   7  7043901-48HR |  10  7044303-SURV |  13  7044346-48HR |
   4  7043649-48HR |

* indicates ticket # is repeated

Total Tickets: 13

48HR - 2 FULL BUSINESS DAYS           |   10

SURV - PRE-SURVEY                     |    2

EMER - ++EMERGENCY++                  |    1


Please call (503) 232-1987 if this data does not match the tickets you
received on 02/16/07



