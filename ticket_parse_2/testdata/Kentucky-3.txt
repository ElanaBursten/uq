
--=====_SEE19571091_=====
Content-Type: text/plain
Content-Transfer-Encoding: quoted-printable

(KENTUCKY URG PRO INC - KY)   04/30/2003   11:57:15

     -0010            2-0224BELLTN       -0287             -0333
--------------------------------------------------------------------------
*** REMOTE BATCH ENTRY ***

 ***** ROUTINE *****

TIME..11:55  DATE..04/30/2003

REQUEST NO...20031802844

LATITUDE..36.6535549 LONGITUDE..-87.4401321

COUNTY..CHRISTIAN
CITY....HOPKINSVILLE (INSIDE)

ADDRESS..15938
STREET...FT CAMPBELL BLVD

SUBDIV. LOT#..
        NAME..

HWY 911
--------------------------------------------------------------------------
TYPE OF WORK....INSTALLING 1 POLE

EXPLOSIVE OR BLASTING..NO

EXTENT OF WORK.........DEPTH (FEET)..000

    -ROW     -EASEMENT   -PRIVATE PROPERTY   -UNDER ROADWAY   -DRIVEWAY
    -GRASS   -FRONT     X-REAR              X-RIGHT SIDE      -LEFT SIDE
     AREA

--------------------------------------------------------------------------
START DATE......05/05/2003  START TIME..08:30

CALLER..........TIM TUCKER
CALLER TITLE....
CALL BACK.......7:30-4:30PM
PHONE #.........270-886-2555
FAX #...........270-885-6469
ALT. PHONE #....
EMAIL ADDRESS...
CONTRACTOR......PENNYRILE RURAL ELECTRIC          03027
ADDRESS.........P O BOX 2900
CITY............HOPKINSVILLE
STATE...........KY
ZIP.............42241
--------------------------------------------------------------------------
COMMENTS:
LOCATE BACK RIGHT CORNER OF BLDG.  AMOCO/UNBANC                             
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                            
=
                                                                        
--=====_SEE19571091_=====--






