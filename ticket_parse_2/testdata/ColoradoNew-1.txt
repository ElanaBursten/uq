
TEST4  00001 UNCCa 01/27/05 12:04 PM A0710288-00A EMER NEW STRT LREQ RSND

Ticket Nbr: A0710288-00A
Original Call Date: 12/16/04  Time: 07:13 AM  Op: KJK
Locate By Date    : 12/16/04  Time: 08:12 AM  Meet: N  Extended job: N
State: CO     County: BOULDER          City: LONGMONT
Addr:      0  Street: BOSTON AVE
Near Intersection(s): S SUNSET ST
Grids: 02N069W09N*               :              :               Legal: Y
Lat/Long: 40.159485/-105.122235 40.159485/-105.120471
        : 40.158124/-105.122235 40.158124/-105.120471
Type of Work: REPR H2O MN                                Exp.: N  Boring: N
Location: LOC A 100FT RAD OF N/E CORN OF INTERSECT *ACCESS OPEN* *EMERG, OUT OF
        : SERV/CREW ON SITE*
Company : CITY OF LONGMONT                          Type: OTHR
Caller  : TRACY COLEMAN              Phone: (303)651-8468
Fax: (303)651-8702  Email: TRACY.COLEMAN@CI.LONGMONT.CO.US
Done for: CITY OF LONGMONT
Remarks:

Members :CMSNC00        :CMSNC18A       :ICGTL4 :LGMT01 :MCLD01 :PCNC00 :PCNG03
Members :PRPA01 :PSNC18A        :QLNC18A        :QLNCNC00

[Originally sent as sequence number 00036 at 12/27/04 09:38]


TEST4  00002 UNCCa 01/27/05 12:04 PM A0710206-01A NONC CNCL   LREQ RSND

Ticket Nbr: A0710206-01A
Original Call Date: 12/15/04  Time: 04:35 PM  Op: KLW
Locate By Date    : 12/17/04  Time: 11:59 PM  Meet: N  Extended job: N
State: CO     County: LARIMER          City: LOVELAND
Addr:   1250  Street: VANCE DR
Near Intersection(s): W COUNTY ROAD 18
Grids: 05N069W19SE  : 05N069W30NE               :               Legal: Y
Lat/Long: 40.380410/-105.160753 40.380410/-105.158944
        : 40.377850/-105.160753 40.377850/-105.158944
Type of Work: REPR H2O SERV LINE                         Exp.: N  Boring: N
Location: LOC ENTIRE LOT *ACCESS OPEN*        :
        : CNCLD DUE TO DID NOT GET JOB
Company : SHAFFER CUSTOM EXCAVATING, INC.           Type: OTHR
Caller  : MARY SHAFFER               Phone: (970)667-8196
Alt Cont: MARK - CEL                 Phone: (970)566-2974
Done for: DENNIS FRAHM H/O
Remarks:

Members :CHMPCBL2       :CMSNC00        :CMSNC10        :PCNC00 :PSNC10 :QLNCNC00
Members :QLNCNC10

[Originally sent as sequence number 00037 at 12/27/04 09:38]


TEST4  00004 UNCCa 01/27/05 12:04 PM A0708558-02A NONC ACMT   LREQ RSND

Ticket Nbr: A0708558-02A
Original Call Date: 12/15/04  Time: 08:51 AM  Op: JCL
Locate By Date    : 12/16/04  Time: 07:52 AM  Meet: N  Extended job: N
State: CO     County: ADAMS            City: WESTMINSTER
Addr:      0  Street: ZUNI ST
Near Intersection(s): W 128TH AVE
Grids: 01S068W28SW  : 01S068W29SE  : 01S068W32NE  : 01S068W33NW   Legal: Y
Lat/Long: 39.929213/-105.016491 39.929213/-105.014708
        : 39.926292/-105.016491 39.926292/-105.014708
Type of Work: REPR H20 MAIN                              Exp.: N  Boring: N
Location: LOC ENTIRE S/E CORNER OF INTERSEC *ACCESS OPEN* *EMERG, OUT OF
        : SERV/CREW ON SITE**RELOC/REFRESH-THE FOLLOWING UTILS PLS RESP TO LOC
        : ASAP OR CALL CUST:*ALL --*EMERG, OUT OF SERV/CREW ON SITE*
Company : LEVI CONTRACTORS INC.                     Type: OTHR
Caller  : SALLI MARTINEZ             Phone: (303)287-4900
Alt Cont: BILL KING                  Phone: (303)919-3126
Fax: (303)287-1120  Email:
Done for: MILE HIGH H20
Remarks:

Members :CMSND00        :CMSND22        :ICGTL3 :PCNDU0 :PSND22 :QLNCND00
Members :QLNCND22       :UNPR2A :WSTM02

[Originally sent as sequence number 00039 at 12/27/04 09:38]


TEST4  00003 UNCCa 01/27/05 12:04 PM A0710463-00A NORM UPDT STRT LREQ RSND

Ticket Nbr: A0710463-00A Update of: A0704083
Original Call Date: 12/13/04  Time: 08:36 AM  Op: KLW
Locate By Date    : 12/20/04  Time: 11:59 PM  Meet: N  Extended job: N
State: CO     County: BOULDER          City: LYONS
Addr:    340  Street: SEWARD ST
Near Intersection(s): 4TH AVE
Grids: 03N070W18NE               :              :               Legal: N
Lat/Long: 40.228304/-105.270531 40.228304/-105.266917
        : 40.226860/-105.270531 40.226860/-105.266917
Type of Work: REPR SEW MN                                Exp.: N  Boring: N
Location: LOC FRNT LOT TO INCLD ST *ACCESS OPEN* UPDATE DUE TO CORRECT ADDR
Company : TOWN OF LYONS                             Type: OTHR
Caller  : SCOTT DANIELS              Phone: (303)823-6622
Alt Cont: CASEY SMITH                Phone:
Fax: (303)823-8257  Email: SDANIELS@TOWNOFLYONS.COM
Done for: SELF
Remarks:

Members :CHMPCBL2       :LGMT01 :LYON01 :PCNC00 :PSNC17 :QLNCNC00       :QLNCNC17

[Originally sent as sequence number 00038 at 12/27/04 09:38]


TEST4  00005 UNCCa 01/27/05 12:04 PM A0710681-00A EMER UPDT STRT DAMG RSND

Ticket Nbr: A0710681-00A Update of: A0691471
Original Call Date: 12/03/04  Time: 11:08 AM  Op: TGD
Locate By Date    : 12/16/04  Time: 10:51 AM  Meet: N  Extended job: N
State: CO     County: ADAMS            City: THORNTON
Addr:      0  Street: LAFAYETTE ST
Near Intersection(s): E 128TH AVE
Grids: 01S068W26S*  : 01S068W35N*               :               Legal: Y
Lat/Long: 39.929364/-104.969703 39.929364/-104.967904
        : 39.927693/-104.969703 39.927693/-104.967904
Type of Work: TRAFFIC SIGNAL                             Exp.: N  Boring: N
Location: LOC THE N/E AND S/E CORNERS OF INTERSEC  *ACCESS OPEN* MT PLS AT
        : INTERSEC M*UPDATE/MARKS NEED REFRESHING* DAMAGE TO A CATV / UNKNOWN
        : ABOUT MARKINGS / UNKNOWN ABOUT SIZE / UNKNOWN ABOUT SERVICE OUTAGE /
        : CREW IS ON SITE / DAMAGE IS LOC @ THE NE/ CORNER INTERSEC / CATV
        : PLEASE RESPOND TO REPAIR / CC IS THORNTON
Company : COLORADO SIGNAL                           Type: OTHR
Caller  : JOYCE ASKEW                Phone: (303)289-3361
Alt Cont: BILL MARTINEZ              Phone: (303)289-3361
Fax: (303)288-5669  Email:
Done for: CITY OF THORNTON
Remarks:

Members :CMSND00        :CMSND18        :KNEB01 :NDP03  :NTHG01 :PCNDU0 :PSND18
Members :PSNG04 :QLNCND00       :QLNCND18       :THRN01

[Originally sent as sequence number 00040 at 12/27/04 09:38]


TEST4  00006 UNCCa 01/27/05 12:04 PM A0711085-00A EMER NEW STRT DAMG RSND

Ticket Nbr: A0711085-00A
Original Call Date: 12/16/04  Time: 11:56 AM  Op: RLB
Locate By Date    : 12/16/04  Time: 12:56 PM  Meet: N  Extended job: N
State: CO     County: ADAMS            City: BRIGHTON
Addr:      0  Street: E 124TH AVE
Near Intersection(s): BRIGHTON RD
Grids: 01S067W34*E  : 01S067W35*W               :               Legal: Y
Lat/Long: 39.922317/-104.865806 39.922317/-104.857598
        : 39.920945/-104.865806 39.920945/-104.857598
Type of Work: INSTL NEW STORMSEWER                       Exp.: N  Boring: N
Location: *DAMAGE TO A MKD GAS MN*HPND APX 1500FT E/ OF INTERS*CREW ON SITE*GAS
        : IS BLOWING*GAS NEED ONLY RSPND TO REPR* *BEST INFO*
Company : TETRA TECH                                Type: OTHR
Caller  : LEO VADER                  Phone: (303)961-8572
Done for: ADAMS COUNTY
Remarks:

Members :360NT2 :ATCT01 :CMSND00        :CMSND17        :PCNDU0 :PSND17 :QLNCND00
Members :QLNCND17       :SPRN01 :SPRNTA :UNPR04 :WCG01

[Originally sent as sequence number 00041 at 12/27/04 09:38]


TEST4  00007 UNCCa 01/27/05 12:04 PM A0702543-00A NORM NEW STRT LREQ RSND

Ticket Nbr: A0702543-00A
Original Call Date: 12/10/04  Time: 10:16 AM  Op: TGD
Locate By Date    : 12/14/04  Time: 08:30 AM  Meet: Y  Extended job: N
Non-scheduling members please meet at requested date and time or call customer.
State: CO     County: MESA             City: GRAND JCT
Addr:   3205  Street: N 12TH ST
Grids: 01S001W01*W  : 01S001W02*E               :               Legal: N
Lat/Long: 39.099801/-108.553227 39.099801/-108.551456
        : 39.095673/-108.553227 39.095673/-108.551456
Type of Work: SOIL SAMPLES                               Exp.: N  Boring: N
Location: LOC A 110FT RAD OF THE S/SD OF BUILDING TO INCLUDE THE SE/ CORNER AND
        : SW/  CORNER OF BUILDING *ACCESS OPEN* MEET ON TUESDAY, 12/14 @ 8:30AM
Company : GEOTECHNICAL ENGINEERING GROUP            Type: OTHR
Caller  : RANDY DEAN                 Phone: (970)245-4078
Alt Cont: GREG                       Phone:
Fax: (970)245-7115  Email:
Done for: DKB CONSTRUCTION
Remarks:

Members :BRSNG01        :GRJC01 :GVLH20 :PCGJ01 :PCNG01 :QLNCW01        :UTEH20

[Originally sent as sequence number 00042 at 12/27/04 09:38]


TEST4  00008 UNCCa 01/27/05 12:05 PM A0701136-00A EMER NEW STRT DAMG RSND

Ticket Nbr: A0701136-00A
Original Call Date: 12/09/04  Time: 01:43 PM  Op: TGD
Locate By Date    : 12/09/04  Time: 02:43 PM  Meet: N  Extended job: N
State: CO     County: EL PASO          City: CALHAN
Addr:  23830  Street: BLUE ROAN CIR
Near Intersection(s): ELLICOTT HWY
Grids: 12S062W07*W               :              :               Legal: Y
Lat/Long: 39.019639/-104.385637 39.019639/-104.383072
        : 39.017157/-104.385637 39.017157/-104.383072
Type of Work: DAMAGE TO AN UNMARKED TELCO SERVICE LINE   Exp.: N  Boring: N
Location: DAMAGE TO AN UNMARKED TELCO SERVICE LINE / UNKNOWN ABOUT SERVICE
        : OUTAGE / CREW IS ON SITE / UNKNOWN ABOUT PREVIOUS TICKET # / DAMAGE IS
        : LOC IN FRONT OF LOT / TELCO PLEASE RESPOND TO REPAIR / CC IS CALHAN
Company : ALAN WILLIS H/O                           Type: RESI
Caller  : ALAN WILLIS                Phone: (208)651-1158
Done for: SELF
Remarks:

Members :MVEL02 :QLNCC05

[Originally sent as sequence number 00043 at 12/27/04 09:38]


TEST4  00009 UNCCa 01/27/05 12:05 PM A0707799-00A NORM NEW STRT LREQ RSND

Ticket Nbr: A0707799-00A
Original Call Date: 12/14/04  Time: 03:24 PM  Op: JAL
Locate By Date    : 12/16/04  Time: 02:00 PM  Meet: Y  Extended job: N
Non-scheduling members please meet at requested date and time or call customer.
State: CO     County: ADAMS            City: THORNTON
Addr:      0  Street: PEARL ST
Near Intersection(s): E 85TH AVE
Grids: 02S068W27NE               :              :               Legal: N
Lat/Long: 39.851700/-104.984261 39.851700/-104.978737
        : 39.850410/-104.984261 39.850410/-104.978737
Type of Work: NEW SEWER MN                               Exp.: N  Boring: Y
Location: LOC ENTIRE INTERSECT CONT 250FT N/ ON PEARL FOR THE ENTIRE ST ALSO
        : FROM INTERSEC 600FT W/ ON 85TH AVE THE ENTIRE RD WAY THRU THE
        : GREENBELT TO GRANT ST *ACCESS OPEN* PLS MEET AT INTERSEC
Company : B.T. CONSTRUCTION                         Type: OTHR
Caller  : HERB JONES                 Phone: (303)469-0199
Alt Cont: CELL                       Phone: (303)591-3446
Fax: (303)466-8309  Email:
Done for: CITY OF THORNTON
Remarks:

Members :CMSND00        :CMSND19        :NWWS01 :PCNDU0 :PSND19 :QLNCND00
Members :QLNCND19       :THRN01

[Originally sent as sequence number 00056 at 12/20/04 15:26]


