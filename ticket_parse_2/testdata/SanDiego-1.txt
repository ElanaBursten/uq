
ICU Message 002 UQSDG  from SDGE                              07-30-02 06:43

SDG04  00051A USAS 07/29/02 10:42:22 A0009156-00A  GRID

Ticket : A0009156 Date: 07/29/02 Time: 10:38 Oper: TMC Chan: 100
Old Tkt: A0009156 Date: 07/29/02 Time: 10:42 Oper: TMC Revision: 00A
State: CA County: SAN DIEGO            Place: ENCINITAS
Locat: FROM CATV PED FRONT OF 14536 ORPHEUS AV CONT W/FOR APPROX 80FT TO S/SIDE
     : OF ADDRESS X/ST GLAUCUS ST
Address: ORPHEUS AVE
X/ST   : GLAUCUS ST
Grids: 1147B0324                                            Delineated: Y

Work : BORING TRENCH INSTALL CATV CONDUIT
Work date: 07/31/02 Time: 10:45 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                Permit   : NOT REQ
Done for : COX COMMUNICATIONS

Company: SUNSHINE COMMUNICATIONS INC    Caller: PETER ZAMORA
Address: 4145 AVENIDA DE LA PLATA
City&St: OCEANSIDE, CA                  Zip: 92056      Fax: 760-726-6415
Phone: 760-726-8415 Ext:      Call back: 7-5
Formn: PETER  ZAMORA        Phone: 760-533-2760
Mbrs : COX04  ENC01  ENC02  LEU01  PTT28  SDG04  SDW01  UCOX04 UPTT28 UTISD

Date Sent 07-29  Marking Crew UQ            Date Completed        Time      

                                  COMMENTS

38/388,390
BPL X2014

ICU Message 001 UQSDG  from SDGE                              07-30-02 06:43

SDG04  00038A USAS 07/29/02 09:12:59 A0008835-00A  GRID

Ticket : A0008835 Date: 07/29/02 Time: 09:10 Oper: KDB Chan: 100
Old Tkt: A0008835 Date: 07/29/02 Time: 09:12 Oper: KDB Revision: 00A
State: CA County: SAN DIEGO            Place: ENCINITAS
Locat: MOBILE HOME PARK: WORKING AT SPACE #18
Address: 523 N VULCAN AVE
X/ST   : UNION ST
Grids: 1147B05134                                           Delineated: Y

Work : REPLACE CATV
Work date: 07/31/02 Time: 09:11 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                Permit   : NOT REQUIRED
Done for : COX COMMUNICATIONS

Company: PACWEST                        Caller: RICK MAJOSKEY
Address: 1621 LA MIRADA
City&St: SAN MARCOS, CA                 Zip: 92069
Phone: 760-744-7608 Ext:      Call back: 7AM-4PM
Formn: RICK MAJOSKEY

                                  REMARKS

*CALLER REQ LOCATORS TO USE CHALK PAINT ONLY*

Mbrs : COX04  ENC01  ENC02  LEU01  MCIWLD PTT28  RSFCSD SCG28T SCG48T SDG04
SDW01  SEJ01  UCOX04 UPTT28 USEJ01 UTISD

Date Sent 07-29  Marking Crew UQ            Date Completed        Time      

                                  COMMENTS

38/243
BPL X2014

ICU Message 003 UQSDG  from SDGE                              07-30-02 06:43

SDG04  00065A USAS 07/29/02 11:32:50 A0009279-00A  GRID

Ticket : A0009279 Date: 07/29/02 Time: 11:28 Oper: JRB Chan: 100
Old Tkt: A0009279 Date: 07/29/02 Time: 11:32 Oper: JRB Revision: 00A
State: CA County: SAN DIEGO            Place: CARDIFF-BY-THE-SEA
Locat: WORKING ON WINDSOR RD IN FRONT OF ADA HARRIS ELEMENTRY SCHOOL
Address: WINDSOR RD
X/ST   : KINGS CROSS DR
Grids: 1167E021                                             Delineated: Y

Work : LANDSCAPING & IRRIGATION
Work date: 07/31/02 Time: 11:30 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                Permit   : NOT REQ
Done for : BILPO

Company: BROOKWOOD LANDSCAPE            Caller: JAVIER NUNEZ
Address: 6218 FAIRMOUNT AV
City&St: SAN DIEGO, CA                  Zip: 92120
Phone: 619-281-8887 Ext:      Call back: 24HRS
Formn: ROJILLIO
Mbrs : ADLCARL       COX04  ENC01  ENC02  PTT28  SDG04  SDW01  SEJ01  UCOX04
UPTT28 USEJ01 UTISD

Date Sent 07-29  Marking Crew UQ            Date Completed        Time      

                                  COMMENTS

59/510
BPL X2014

ICU Message 004 UQSDG  from SDGE                              07-30-02 06:43

SDG04  00083A USAS 07/29/02 13:44:17 A0009510-00A  GRID

Ticket : A0009510 Date: 07/29/02 Time: 13:35 Oper: DLW Chan: 100
Old Tkt: A0009510 Date: 07/29/02 Time: 13:43 Oper: DLW Revision: 00A
State: CA County: SAN DIEGO            Place: CARDIFF-BY-THE-SEA
Locat: ADDRESS NOT SHOWN LOCATED , APPROX 125FT N/OF LISZT AVE
Address: 1468 RUBENSTEIN DR
X/ST   : LISZT AVE
Grids: 1167D023                                             Delineated: N

Work : TRENCHING , FOR UNDERGROUND CONDUIT
Work date: 07/31/02 Time: 13:41 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                Permit   : PENDING
Done for : PROP OWN -PETERS

Company: H/O-PETERS                     Caller: JERRY PETERS
Address:
City&St:                                Zip:
Phone: 760-632-9292
Formn: JERRY PETERS         Phone: 760-420-3600
Mbrs : COX04  ENC01  ENC02  MCIWLD PTT28  SCG28T SCG48T SDG04  SDW01  UCOX04
UPTT28 UTISD

Date Sent 07-29  Marking Crew UQ            Date Completed        Time      

                                  COMMENTS

38/95
BPL X2014

ICU Message 006 UQSDG  from SDGE                              07-30-02 06:43

SDG04  00111A USAS 07/29/02 15:11:20 A0009780-01A  RXMIT GRID

Ticket : A0009780 Date: 07/29/02 Time: 15:09 Oper: PEG Chan: 100
Old Tkt: A0009780 Date: 07/29/02 Time: 15:07 Oper: PEG Revision: 01A
State: CA County: SAN DIEGO            Place: ENCINITAS
Locat: E/SIDE OF BRACERO RD APPROX 300FT S/OF REQUEZA ST ;  S/SIDE OF REQUEZA ST
     : APPROX 450FT E/OF BRACERO RD;  IN THE FIELD APPROX 450FT E/OF BRACERO RD
     : AND 300FT S/OF REQUEZA ST;   MEET AT INTER OF BRACERO RD AND REQUEZA
     : ST-CALL TO CONFIRM MEET
Address: BRACERO RD
X/ST   : REQUEZA ST
Grids: 1147E073                                             Delineated: Y

Work : INSTALL 3 POWER POLES & 3 ANCHORS
Work date: 08/01/02 Time: 08:00 Hrs notc: 065 Work hrs: 064 Priority: 2
Instruct : MEET AND MARK          Permit   : NOT REQUIRED
Done for : SDG&E

Company: SOUTHERN CONTR CO              Caller: LOUIS WHARTON
Address: 559 N TWIN OAKS VALLEY RD
City&St: SAN MARCOS, CA                 Zip: 92069
Phone: 760-744-0760 Ext:      Call back: 630-5
Formn: SAM CHERIMONTE

                                  REMARKS

**RESEND**CORRECTING TG FROM 1147E0734 TO 1147E073  --[PEG 07/29/02 15:11]

Mbrs : ADLCARL       COX04  ENC01  ENC02  PTT28  SDG04  SDW01  SEJ01  UCOX04
UPTT28 USEJ01 UTISD

Date Sent 07-29  Marking Crew UQ            Date Completed        Time      

                                  COMMENTS

RESENT TICKET
BPL X2014

ICU Message 002 UQSDG  from SDGE                              07-30-02 06:51

SDG05  00074A USAS 07/29/02 09:26:27 A0008885-00A  GRID

Ticket : A0008885 Date: 07/29/02 Time: 09:19 Oper: TMC Chan: 100
Old Tkt: A0008885 Date: 07/29/02 Time: 09:24 Oper: TMC Revision: 00A
State: CA County: SAN DIEGO            Place: ESCONDIDO
Locat: AREA WILL BE DELINEATED   *N/OF X/ST*
Address: 829 SOCIN CT
X/ST   : WANEK RD
Grids: 1110E0612                                            Delineated: N

Work : BORE LANDSCAPE,INSTALL DRAINS,IRRIGATION
Work date: 07/31/02 Time: 09:30 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                Permit   : NOT REQ
Done for : H/O MCCULLUM

Company: MR. Z`S LANDSCAPE              Caller: JUAN
Address: 1170 HARDING ST
City&St: ESCONDIDO, CA                  Zip: 92027
Phone: 760-802-3625 Ext:      Call back: 8AM-5PM
Formn: JUAN                 Phone: 760-802-3625
Mbrs : COX04  ESC01  PTT28  SDG05  UCOX04 UPTT28 UTISD

Date Sent 07-29  Marking Crew               Date Completed        Time      

                                  COMMENTS

55/627,620A

