
New Jersey One Call System        SEQUENCE NUMBER 0256   CDC = TST

Transmit:  Date: 11/30/04   At: 1621

*** E M E R G E N C Y     *** Request No.: 042910010

Operators Notified:
    LMU=/LACY MUA       / ADC=/COMCAST CATV   / OCU=/OCEAN COUNTY UA/ 
    AE1=/CONECTV PWR|UTQ/ NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON    |CLS/ 
    GPC=/JCP&L      |CLS/ 

Start Date/Time:    10/22/04   At 0001   Expiration Date: 

Location Information:
   County: OCEAN     Municipality: LACEY
   Subdivision/Community: 
   Street:               16 HOLLYWOOD BLVD S
   Nearest Intersection: ROUTE 9 
   Other Intersection:   
   Lat/Long:  
   Type of Work :        REPLACING POLE
   Extent of Work: 15FT RADIUS OF POLE # 50154/41525...     DEPTH: 6FT
     EXTENT OF WORK LINE 2
     EXTENT OF WORK LINE 3
     EXTENT OF WORK LINE 4
     EXTENT OF WORK LINE 5
     EXTENT OF WORK LINE 6
   Remarks:
     REMARKS LINE 1
     REMARKS LINE 2
     REMARKS LINE 3
     REMARKS LINE 4
     REMARKS LINE 5

   Working For:  JOE HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000

Excavator Information:
   Caller:       JACOB HOMEOWNER          
   Phone:        609-000-0000            

   Excavator:    JESSI HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000            Fax:  
   Cellular:     609-000-2900 WORK
   Email:        
End Request

~~~~~
New Jersey One Call System        SEQUENCE NUMBER 0255   CDC = TST

Transmit:  Date: 11/30/04   At: 1620

*** R O U T I N E         *** Request No.: 042910009

Operators Notified:
    LMU=/LACY MUA       / ADC=/COMCAST CATV   / OCU=/OCEAN COUNTY UA/ 
    AE1=/CONECTV PWR|UTQ/ NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON    |CLS/ 
    GPC=/JCP&L      |CLS/ 

Start Date/Time:    10/22/04   At 0001   Expiration Date: 12/01/04

Location Information:
   County: OCEAN     Municipality: LACEY
   Subdivision/Community: 
   Street:               16 HOLLYWOOD BLVD S
   Nearest Intersection: ROUTE 9 
   Other Intersection:   
   Lat/Long:  
   Type of Work :        ADDITION
   Extent of Work: M/O BEGINS AT 30FT BEHIND CURB,...       DEPTH: 3FT
     EXTENT OF WORK LINE 2
     EXTENT OF WORK LINE 3
     EXTENT OF WORK LINE 4
     EXTENT OF WORK LINE 5
     EXTENT OF WORK LINE 6
   Remarks:
     REMARKS LINE 1
     REMARKS LINE 2
     REMARKS LINE 3
     REMARKS LINE 4
     REMARKS LINE 5

   Working For:  JOHN HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000

Excavator Information:
   Caller:       JOHN HOMEOWNER          
   Phone:        609-000-0000            

   Excavator:    JOHN HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000            Fax:  
   Cellular:     609-000-2900 WORK
   Email:        
End Request

~~~~~
New Jersey One Call System        SEQUENCE NUMBER 0258   CDC = TST

Transmit:  Date: 11/30/04   At: 1621

*** R O U T I N E         *** Request No.: 042910012

Operators Notified:
    LMU=/LACY MUA       / ADC=/COMCAST CATV   / OCU=/OCEAN COUNTY UA/ 
    AE1=/CONECTV PWR|UTQ/ NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON    |CLS/ 
    GPC=/JCP&L      |CLS/ 

Start Date/Time:    10/22/04   At 0001   Expiration Date: 12/01/04

Location Information:
   County: OCEAN     Municipality: LACEY
   Subdivision/Community: 
   Street:               16 HOLLYWOOD BLVD S
   Nearest Intersection: ROUTE 9 
   Other Intersection:   
   Lat/Long:  
   Type of Work :        ADDITION
   Extent of Work: M/O BEGINS AT 30FT BEHIND CURB,...       DEPTH: 3FT
     EXTENT OF WORK LINE 2
   Remarks:
     REMARKS LINE 1
     REMARKS LINE 2
     REMARKS LINE 3
     REMARKS LINE 4
     REMARKS LINE 5

   Working For:  JOHN HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000

Excavator Information:
   Caller:       JOHN HOMEOWNER          
   Phone:        609-000-0000            

   Excavator:    JOHN HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000            Fax:  
   Cellular:     609-000-2900 WORK
   Email:        
End Request

~~~~~
New Jersey One Call System        SEQUENCE NUMBER 0259   CDC = TST

Transmit:  Date: 11/30/04   At: 1621

*** E M E R G E N C Y     *** Request No.: 042910013

Operators Notified:
    LMU=/LACY MUA       / ADC=/COMCAST CATV   / OCU=/OCEAN COUNTY UA/ 
    AE1=/CONECTV PWR|UTQ/ NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON    |CLS/ 
    GPC=/JCP&L      |CLS/ 

Start Date/Time:    10/22/04   At 0001   Expiration Date: 

Location Information:
   County: OCEAN     Municipality: LACEY
   Subdivision/Community: 
   Street:               16 HOLLYWOOD BLVD S
   Nearest Intersection: ROUTE 9 
   Other Intersection:   
   Lat/Long:  
   Type of Work :        REPLACING POLE
   Extent of Work: 15FT RADIUS OF POLE # 50154/41525...     DEPTH: 6FT
     EXTENT OF WORK LINE 2
     EXTENT OF WORK LINE 3
     EXTENT OF WORK LINE 4
     EXTENT OF WORK LINE 5
     EXTENT OF WORK LINE 6
   Remarks:
     REMARKS LINE 1
     REMARKS LINE 2
     REMARKS LINE 3
     REMARKS LINE 4
     REMARKS LINE 5

   Working For:  JOHN HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000

Excavator Information:
   Caller:       JOHN HOMEOWNER          
   Phone:        609-000-0000            

   Excavator:    JOHN HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000            Fax:  
   Cellular:     609-000-2900 WORK
   Email:        
End Request

~~~~~
New Jersey One Call System        SEQUENCE NUMBER 0257   CDC = TST

Transmit:  Date: 11/30/04   At: 1621

*** U P D A T E           *** Request No.: 042910011 Of Request No. 042781385

Operators Notified:
    LMU=/LACY MUA       / ADC=/COMCAST CATV   / OCU=/OCEAN COUNTY UA/ 
    AE1=/CONECTV PWR|UTQ/ NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON    |CLS/ 
    GPC=/JCP&L      |CLS/ 

Start Date/Time:    10/22/04   At 0001   Expiration Date: 12/01/04

Location Information:
   County: OCEAN     Municipality: LACEY
   Subdivision/Community: 
   Street:               16 HOLLYWOOD BLVD S
   Nearest Intersection: ROUTE 9 
   Other Intersection:   
   Lat/Long:  
   Type of Work :        REPAIR WATER LINE
   Extent of Work: CURB TO 20FT BEHIND CURB                 DEPTH: 3FT
     EXTENT OF WORK LINE 2
     EXTENT OF WORK LINE 3
     EXTENT OF WORK LINE 4
     EXTENT OF WORK LINE 5
     EXTENT OF WORK LINE 6
   Remarks:
     REMARKS LINE 1
     REMARKS LINE 2
     REMARKS LINE 3
     REMARKS LINE 4
     REMARKS LINE 5

   Working For:  JOHN HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000

Excavator Information:
   Caller:       JOHN HOMEOWNER          
   Phone:        609-000-0000            

   Excavator:    JOHN HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000            Fax:  
   Cellular:     609-000-2900 WORK
   Email:        
End Request

~~~~~
New Jersey One Call System        SEQUENCE NUMBER 0260   CDC = TST

Transmit:  Date: 11/30/04   At: 1621

*** U P D A T E           *** Request No.: 042910014 Of Request No. 042781385

Operators Notified:
    LMU=/LACY MUA       / ADC=/COMCAST CATV   / OCU=/OCEAN COUNTY UA/ 
    AE1=/CONECTV PWR|UTQ/ NJN=/NJ NATL GAS|UTQ/ BAN=/VERIZON    |CLS/ 
    GPC=/JCP&L      |CLS/ 

Start Date/Time:    10/22/04   At 0001   Expiration Date: 12/01/04

Location Information:
   County: OCEAN     Municipality: LACEY
   Subdivision/Community: 
   Street:               16 HOLLYWOOD BLVD S
   Nearest Intersection: ROUTE 9 
   Other Intersection:   
   Lat/Long:  
   Type of Work :        REPAIR WATER LINE
   Extent of Work: CURB TO 20FT BEHIND CURB                 DEPTH: 3FT
     EXTENT OF WORK LINE 2
     EXTENT OF WORK LINE 3
     EXTENT OF WORK LINE 4
     EXTENT OF WORK LINE 5
     EXTENT OF WORK LINE 6
   Remarks:
     REMARKS LINE 1
     REMARKS LINE 2
     REMARKS LINE 3
     REMARKS LINE 4
     REMARKS LINE 5

   Working For:  JOHN HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000

Excavator Information:
   Caller:       JOHN HOMEOWNER          
   Phone:        609-000-0000            

   Excavator:    JOHN HOMEOWNER
   Address:      16 HOLLYWOOD BLVD S
   City:         FORKED RIVER, NJ  08731
   Phone:        609-000-0000            Fax:  
   Cellular:     609-000-2900 WORK
   Email:        
End Request

~~~~~
