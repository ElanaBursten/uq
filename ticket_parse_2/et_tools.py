# et_tools.py
# Helper functions for ElementTree.

from __future__ import with_statement
import StringIO
import xml.etree.ElementTree as ET

XML_HEADER = '<?xml version="1.0" ?>'

def elem_as_dict(elem, attrs, default='', subelement_tags=[]):
    """ Extract a list of attributes from an element/tag and return them as a
        dictionary. """
    d = {}
    for attr in attrs:
        d[attr] = elem.get(attr, default)
    for st in subelement_tags:
        d[st] = []
        for subelement in elem.findall(st):
            d[st].append(subelement)
    return d

def elem_as_dictlist(elem, path, attrs, default='', subelement_tags=[]):
    items = []
    for e in elem.findall(path):
        d = elem_as_dict(e, attrs, default, subelement_tags)
        items.append(d)
    return items

def safe_find(elem, path):
    x = elem.find(path)
    if x is None:
        return {} # will support .get(); may be replaced with dummy node
    return x

def print_tree(node, indent=""):
    """ Simple tree printer for debugging purposes. Only displays the nodes,
        not text. """
    print "%s%s" % (indent, node)
    for child in node.getchildren():
        print_tree(child, indent + "  ")

def write_tree(tree, filename):
    with open(filename, 'wb') as f:
        f.write(XML_HEADER + '\n')
        f.write(ET.tostring(tree.getroot()))

def tree_as_string(tree, include_header=1):
    s = ET.tostring(tree.getroot())
    if include_header:
        s = XML_HEADER + "\n" + s
    return s

def findall(node, tag):
    """ Generator that finds all tags of the given name, starting at the given
        node. Namespaces are ignored (unlike Element.find() and friends).
        N.B. Does not accept XPath syntax. """
    for elem in node.getiterator():
        name = _remove_namespace(elem.tag)
        if name == tag:
            yield elem

def find(node, tag):
    """ Like findall(), but only returns the first matching node. """
    try:
        return findall(node, tag).next()
    except StopIteration:
        raise ValueError("Element not found: '%s'" % tag)

def _remove_namespace(tag):
    if tag.startswith('{'):
        idx = tag.find('}')
        return tag[idx+1:]
    return tag

class XMLValidateError(Exception):
    pass

def validate(xml):
    """ Do a crude validation of the given XML. <xml> can be a string, in
        which case it will be parsed first; or it can be a root node of
        ElementTree.
        Specifically, this function checks if text or tails contain certain
        strings, like "/>", that indicate invalid XML (even though ElementTree
        may not raise an error for them).
    """
    if isinstance(xml, basestring):
        xml = ET.fromstring(xml)
    if isinstance(xml, ET.ElementTree):
        xml = xml.getroot()

    def validate_node(node, acc=[]):
        for s in (node.text, node.tail):
            if s and "/>" in s:
                #print dir(node)
                raise XMLValidateError("Invalid XML detected: %r (%r)" %
                      (s, acc))
        for c in node.getchildren():
            validate_node(c, acc + [c])

    validate_node(xml)


class _FileWrapper:
    def __init__(self, source):
        self.source = source
        self.lineno = 0
        self.curr_line = ''
    def read(self, bytes):
        self.curr_line = s = self.source.readline()
        self.lineno += 1
        return s

# XXX obsolete
def validate2(data):
    f = _FileWrapper(StringIO.StringIO(data))
    for event, elem in ET.iterparse(f, events=['start', 'end']):
        if True or event == 'start':
            #print f.lineno, event, elem, (elem.text, elem.tail)
            for s in (elem.text, elem.tail):
                if s and '/>' in s:
                    raise XMLValidateError("Invalid XML detected; line %d: %r"
                          % (f.lineno, f.curr_line))

