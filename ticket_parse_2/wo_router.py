# wo_router.py

from __future__ import with_statement
import getopt
import os
import sys
#
import callcenters
import config
import datadir
import date
import date_special
import dbinterface_ado
import emailtools
import errorhandling2
import errorhandling_special
import mutex
import program_status
import ticket_db
import windows_tools
import wo_businesslogic
from check_python_version import check_python_version

class WorkOrderRouterOptions:
    verbose = 1
    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)

class WorkOrderRouter:

    def __init__(self, options=None, dbado=None):
        self.options = options or WorkOrderRouterOptions()
        self.tdb = ticket_db.TicketDB(dbado=dbado)
        self.config = config.getConfiguration()
        self.logic_cache = {} # instances of WorkOrderBusinessLogic

        self.holiday_list = date_special.read_holidays(self.tdb)
        self.emails = emailtools.Emails(self.tdb)

        # install logger
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtp_accs[0], me="WorkOrderRouter",
                   cc_emails=self.emails,
                   admin_emails=self.config.admins,
                   subject="WorkOrderRouter Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = self.options.verbose
        #self.ebc = email_by_client.EmailByClient(self.tdb)

    def get_pending_work_orders(self):
        # XXX probably move to stored proc later
        sql = """
         select top 500 * from work_order
         where status = '-P'
         and closed = 0
        """
        rows = self.tdb.runsql_result(sql)
        return rows

    def get_logic(self, call_center):
        try:
            return self.logic_cache[call_center]
        except KeyError:
            cc = call_center
            if call_center[0] in "0123456789":
                cc = 'c' + call_center
            logic_class = callcenters.get_wo_businesslogic(cc)
            logic = logic_class(self.tdb, self.holiday_list)
            self.logic_cache[call_center] = logic
            return logic

    def update_wo_row(self, wo_row):
        """ Update a dict with work_order fields to resemble a ticket field,
            so it can be passed to LocatorCatalog routing methods. """
        row = wo_row.copy()
        row['ticket_format'] = row['wo_source']
        for fn in ['map_page', 'work_city', 'work_county', 'work_state']:
            if row[fn] is None: row[fn] = ''
        return row

    def route_work_orders(self, wo_rows):
        for wo_row in wo_rows:
            try:
                self.route_work_order(wo_row)
            except:
                ep = errorhandling2.errorpacket()
                ep.add(info="could not route work order")
                self.log.log(ep, dump=1, write=1, send=1)
                # XXX what happens to the record in the db?

    def route_work_order(self, wo_row):
        # wo_row is a dictionary representing a work_order record.
        self.log.log_event("Routing work order id: %s" % wo_row['wo_id'])
        t_row = self.update_wo_row(wo_row)
        call_center = wo_row['wo_source']
        logic = self.get_logic(call_center)
        routing_result = logic.locator(t_row)
        self.log.log_event("Routing to: %s" % routing_result)
        locator = routing_result.locator or logic.DEFAULT_LOCATOR

        # assign employee, using stored proc
        self.assign_work_order(wo_row['wo_id'], locator)

        # update work order status
        self.update_work_order_status(wo_row['wo_id'], '-R', 0,
          date.Date().isodate(), locator, 'ROUTER')

    def assign_work_order(self, wo_id, emp_id):
        self.tdb.runsql("""
          exec assign_work_order %s, %s, 'ROUTER'
        """ % (wo_id, emp_id))

    def update_work_order_status(self, wo_id, status, closed, status_date,
                                 status_by, status_how):
        self.tdb.runsql("""
         exec update_work_order_status %(wo_id)s, '%(status)s', %(closed)s,
           '%(status_date)s', %(status_by)s, '%(status_how)s', NULL
        """ % locals())

    def run(self, max_times=1):
        for i in range(max_times):
            wo_rows = self.get_pending_work_orders()
            if wo_rows:
                self.route_work_orders(wo_rows)
            else:
                self.log.log_event("No pending work orders found")


if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "")
    configfile = ""

    # only one instance of this *program* is allowed
    biz_id = datadir.datadir.get_id()
    mutex_name = "wo_router:" + biz_id
    windows_tools.setconsoletitle("work_order_router")

    try:
        log = errorhandling_special.toplevel_logger(configfile,
              "WorkOrderRouter", "WokOrderRouter Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(configfile, "WorkOrderRouter")
        sys.exit(1)

    check_python_version(log)
    try:
        dbado = dbinterface_ado.DBInterfaceADO()
        with mutex.MutexLock(dbado, mutex_name):
            tr = WorkOrderRouter(dbado=dbado)
            program_status.write_status(mutex_name)
            tr.run()
    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)


