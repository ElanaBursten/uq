# parsers.py
# BaseParser class, plus several derived parsers for specific tickets.
# Created: 22 Jan 2002, Hans Nowak

import inspect
import os
import string
import types
#
import call_centers
import date
import formats
import termtranslator
import ticket as ticketmod
import ticketparser
import tools

# get the absolute path of translations.py since
# we may not be running in the same directory
import translations as trans
translations = os.path.join(os.path.dirname(trans.__file__), "translations.py")


STREET_NUMBER_MAXLEN = 10   # max. length of the work_address_number_2 field

class ParserError(Exception):
    pass

def get_parser(format):
    try:
        pclass = globals()[format+"Parser"]
    except KeyError:
        try:
            pclass = formats.get_parser(format)
        except:
            import traceback
            traceback.print_exc()
            raise ParserError, "No Parser class available for: %s" % (format,)
    return pclass

# XXX BaseParser: constructor requires that we pass in the format name.  Is
# this redundant?

class BaseParser:
    """ Base class that allows for easy-to-use parser subclasses. """

    MAX_LOCATES = 50
    # maximum number of locates that is allowed on a ticket

    # characters that are allowed in a term id
    VALID_LOCATE_CHARS = string.uppercase + string.digits + "*#-_/&"

    # Required fields. An exception will be raised if these are not found by
    # the regex matcher. (Note that this is *not* the same as matching an
    # empty string for an attribute.)
    required = ["ticket_number", "work_state", "transmit_date", "call_date",
                "ticket_type", "ticket_format", "kind", "work_date"]
    # con_name, company, ...?

    # if we find multiple values for these fields, keep them around so we can
    # issue a warning or error later
    only_one = ["ticket_number", "work_state", "transmit_date", "call_date",
                "ticket_type", "work_type"]

    def __init__(self, ticketdata, format, exclude=None, term_translator=None):
        self.parser_format = format

        if exclude is None:
            exclude = []
            
        if term_translator:
            self.term_translator = term_translator
        else:
            self.term_translator = termtranslator.TermTranslator(translations)                 
            
        self.data = ticketdata  # raw ticket data, as a long string
        self.ticket = ticketmod.Ticket() # "empty" Ticket instance
        self.ticket.image = self.data.lstrip()

        self.check_max_locates = 1  # do the "max locates" check

        self.exclude = exclude[:]
        # these locates are excluded and will be ignored if found in the
        # ticket image
        self.dup_fields = [] # names of fields that appeared more than once

        try:
            self.ticket.ticket_format = call_centers.format2cc[format]
            self.ticket.source = self.ticket.ticket_format # default
        except KeyError:
            errmsg = "No call center mapping found for %s" % (format,)
            raise ParserError, errmsg

    def _get_regexen(self):
        """ Return a list of pairs (regex_name, regex) for all regular
            expressions attributes (starting with "re_") for this class. """
        z = [(name, value)
             for (name, value) in inspect.getmembers(self)
             if name.startswith("re_") and name != "re_ticket_number"]
        z.sort()
        z[:0] = [("re_ticket_number", getattr(self, "re_ticket_number"))]
        # This guarantees that ticket number is the first regex we parse.
        # This is useful if errors occur when parsing any other attribute
        # than ticket_number; we can then display the number of the ticket
        # that failed to parse.
        return z

    def _get_findfunctions(self):
        return [(name, value)
                for (name, value) in tools.attributes(self.__class__).items()
                if name.startswith("find_")]

    @staticmethod
    def remove_duplicate_locates(locates):
        names = []
        ok = []
        for loc in locates:
            if loc.client_code not in names:
                ok.append(loc)
                names.append(loc.client_code)
        return ok

    def exclude_locate(self, client_code):
        return client_code in self.exclude
        
    def pre_process(self):
        """ Override in derived classess """

    def post_process(self):
        """ Does certain things to the Ticket's attributes after everything
            has been parsed. """

        # filter out any duplicate locates
        self.ticket.locates = self.remove_duplicate_locates(self.ticket.locates)

        def split_number(splitters):
            for splitter in splitters:
                if not self.ticket.work_address_number_2:
                    idx = self.ticket.work_address_number.find(splitter)
                    if idx >= 0:
                        first = self.ticket.work_address_number[:idx]
                        second = self.ticket.work_address_number[idx+1:]
                        self.ticket.work_address_number = first
                        self.ticket.work_address_number_2 = second
                        break # only split once

        # if work_address_number is like "xxx-yyy" (e.g. 16234-16245), and
        # work_address_number_2 is empty, then split the number
        split_number(["-"])

        # sometimes the second number may be too long... in that case,
        # prepend it to the street name
        if len(self.ticket.work_address_number_2) > STREET_NUMBER_MAXLEN:
            n = self.ticket.work_address_number_2
            self.ticket.work_address_number_2 = "" # n[:10]
            self.ticket.work_address_street = n + " " + \
             self.ticket.work_address_street
        # same for the first number
        if len(self.ticket.work_address_number) > STREET_NUMBER_MAXLEN:
            n = self.ticket.work_address_number
            self.ticket.work_address_number = "" # n[:10]
            self.ticket.work_address_street = n + " " + \
             self.ticket.work_address_street

        # long remarks are cut off after 1000 characters
        if len(self.ticket.work_remarks) > 1000:
            self.ticket.work_remarks = self.ticket.work_remarks[:1000] + \
             "   (MORE...)"

        # ticket_type needs to be single-spaced
        if self.ticket.ticket_type:
            self.ticket.ticket_type = tools.singlespaced(
             self.ticket.ticket_type)

        # email address can be cut off if it's too long
        if len(self.ticket.caller_email) > 40:
            self.ticket.caller_email = self.ticket.caller_email[:40]

        # no field value in ticket should start with a $, because this seems
        # to have special meaning in SQL Server
        for key, value in self.ticket._data.items():
            if type(value) in types.StringTypes and value.startswith("$"):
                setattr(self.ticket, key, " " + value)

        # if an exclude list is specified, remove those from the locates
        if self.exclude:
            for i in range(len(self.ticket.locates)-1, -1, -1):
                loc = self.ticket.locates[i]
                if self.exclude_locate(loc.client_code):
                    del self.ticket.locates[i]

        # insert duplicate fields into ticket, so other parts of the system can
        # see them
        self.ticket._dup_fields = self.dup_fields

        # 2005.02.16: FTTP rule
        # if "FTTP" is found in certain fields, add it to the ticket type.
        self.set_fttp()

        # Replace tabs with a single space from work_description field
        self.ticket.work_description = self.ticket.work_description.expandtabs(1)

    FTTP_FIELDS = ["work_type", "work_remarks", "company", "con_name"]

    # XXX this is really business logic and related to the call center rather
    # than the format... so eventually it should be moved somewhere else
    def set_fttp(self):
        fttp_found = fios_found = 0
        for fieldname in self.FTTP_FIELDS:
            value = self.ticket.get(fieldname, "")
            if value.upper().find("FTTP") >= 0:
                fttp_found = 1
            if value.upper().find("FIOS") >= 0:
                fios_found = 1

        if self.ticket.ticket_type.upper().find("FTTP") < 0:
            if (fttp_found and not fios_found) or self.is_fttp():
                self.ticket.ticket_type += " FTTP"
        else: # FTTP is in ticket_type already
            if fios_found:
                self.ticket.ticket_type = self.ticket.ticket_type.upper().replace("FTTP", "FIOS")

    def is_fttp(self):
        """ Override in subclasses.  Return True if the ticket should be
            considered FTTP. """
        return False

    def sanitycheck_before(self):
        """ Check that is done before we parse the raw ticket. Override this
            method and raise errors if the ticket doesn't meet the desired
            sanity checks, e.g. the presence of a certain line, etc.
        """

    def check_invalid_locates(self):
        invalid = []
        for loc in self.ticket.locates:
            if not tools.valid_locate_name(loc.client_code,
              self.VALID_LOCATE_CHARS):
                invalid.append(loc.client_code)
        if invalid:
            msg = "Invalid locate name(s): " + repr(invalid)
            raise ticketparser.TicketError, msg

    def check_max_number_of_locates(self):
        # more than MAX_LOCATES locates counts as an error
        if self.check_max_locates \
        and len(self.ticket.locates) > self.MAX_LOCATES:
            raise ticketparser.TicketError, \
             "More than %d locates on ticket" % (self.MAX_LOCATES,)

    def sanitycheck_after(self):
        self.check_invalid_locates()
        self.check_max_number_of_locates()

    def datecheck(self):
        """ Check that we don't have any invalid dates. Raise an exception if
            we do. Should not need to be overridden. """
        DATE_FIELDS = ["call_date", "transmit_date", "legal_date", "work_date",
                       "respond_date", "due_date", "legal_due_date"]
        for date_field in DATE_FIELDS:
            value = getattr(self.ticket, date_field)
            if value:
                try:
                    d = date.Date(value)
                    if d.correct():
                        setattr(self.ticket, date_field, d.isodate())
                except:
                    raise date.DateError, "Invalid date [%s]: %s" % (
                     date_field, value)
                else:
                    if not d.check():
                        raise date.DateError, "Invalid date [%s]: %s" % (
                         date_field, d.isodate())

    def parse(self, verbose=0):
        # Do the sanity check first... may raise errors
        self.sanitycheck_before()

        # Do any pre-processing
        self.pre_process()

        # Find all regular expressions
        regexen = self._get_regexen()

        # Walk through the list of regular expressions, processing them
        for rxname, rxvalue in regexen:
            attrname = rxname[3:]    # everything after the "re_"
            assert hasattr(self.ticket, attrname), \
                   "Unknown attribute: %s" % (attrname)

            # search the raw ticket data for this regular expression

            # todo(dan) Is this here for a reason, other than removing it
            # breaks a few tests?
            m = rxvalue.search(self.data)

            all = self.findall(rxvalue, self.data)
            if len(all) > 1 and attrname in self.only_one:
                self.dup_fields.append((attrname, all))
            m = all and all[0] or None
            if m:
                # do we have an f_ method for this attribute?
                if hasattr(self, "f_" + attrname):
                    f = getattr(self, "f_" + attrname)
                    try:
                        value = f(m)
                    except Exception, e:
                        text = string.join(map(str, e.args), " ")
                        newmsg = "Error when parsing field [%s]:\n" % (
                         attrname)
                        newmsg = newmsg + e.__class__.__name__ + ": "
                        newmsg = newmsg + text
                        raise ParserError, newmsg
                else:
                    # default value is m.group(1)
                    value = m.group(1)
                # set this attribute to the value we found
                setattr(self.ticket, attrname, value)
                if verbose:
                    print " ticket." + attrname + " = " + repr(getattr(
                     self.ticket, attrname))
            else:
                if verbose:
                    print "Not found:", attrname,
                    print "(ticket %s)" % (self.ticket.ticket_number
                     or "unknown")

        # Any special functions? (Starting with find_)
        funcs = self._get_findfunctions()
        for fname, fvalue in funcs:
            attrname = fname[5:]    # everything after the "find_"
            assert hasattr(self.ticket, attrname), \
             "Unknown attribute: %s" % (attrname)
            value = fvalue(self)
            setattr(self.ticket, attrname, value)
            if verbose:
                print " ticket." + attrname + " = " + repr(getattr(self.ticket,
                 attrname))

        # any last-minute changes?
        self.post_process()
        # check required fields again... no empty strings
        self.check_required_fields()
        # any dates invalid before we return it?
        self.datecheck()
        # do another sanity check on the parsed ticket
        self.sanitycheck_after()

        # mostly as a convenience:
        return self.ticket

    @staticmethod
    def findall(regex, data):
        all = regex.findall(data)   # must be a compiled regex
        all = [tools.PseudoMatch(m) for m in all]
        return all

    def check_required_fields(self):
        for name in self.required:
            value = getattr(self.ticket, name)
            if not value:
                raise AttributeError, "Field [%s] is empty" % (name)

    @staticmethod
    def isodate(date):
        """ Convert a date(time) string into an ISO date/time string.
            The date is assumed to be in the format MM/DD/YY, possibly with
            a time HH:MM. This should be converted to YYYY-MM-DD HH:MM:SS.
        """
        return tools.isodate(date)

    # _get_grids and _compute_longlats are moved to BaseParser. These will only
    # be called by parsers of tickets that have grids and want to compute
    # longitude and latitude.

    def _get_grids(self):
        """ Returns a list of grids (encoded long/lat coordinates). """
        lines = self.data.split("\n")
        grids = ""
        for line in lines:
            line = line.strip()
            if line.startswith("Grids"):
                grids = line[10:].strip()
                continue
            if grids:
                if line.startswith(": "):
                    grids = grids + line[1:].rstrip()
                else:
                    break
        grids = grids.split() # a list of grids
        # filter out strings with invalid length
        grids = [g for g in grids if len(g) >= 10]
        return grids

    def _compute_longlats(self):
        # compute longitude/latitude based on qtrmin-grids (1234A5678B, etc.)
        if hasattr(self, "_latlong"):
            return self._latlong
        else:
            grids = self._get_grids()
            grids = map(tools.grid2decimal, grids)
            latitudes = [t[0] for t in grids]
            longitudes = [-t[1] for t in grids]
            avg_latitude = tools.average(latitudes)
            avg_longitude = tools.average(longitudes)
            self._latlong = (avg_latitude, avg_longitude)
            return self._latlong

    # find_kind for all tickets
    def find_kind(self):
        ttype = self.ticket.ticket_type.upper()
        if ("EMER" in ttype or "RUSH" in ttype) and ("NON-EMER" not in ttype):
            return "EMERGENCY"
        else:
            return "NORMAL"

