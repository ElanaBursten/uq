# summary_fragment.py

import sqlmap
import static_tables

class SummaryFragment(sqlmap.SQLMap):
    __table__ = "summary_fragment"
    __key__ = "summary_fragment_id"
    __fields__ = static_tables.summary_fragment

