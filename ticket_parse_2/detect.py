# detect.py
# Detect Python type (CPython vs IronPython).

import sys

def is_ironpython():
    return 'IronPython' in sys.version

if __name__ == "__main__":

    print "sys.version:", sys.version
    print "sys.platform:", sys.platform
    print
    print "This is run under", is_ironpython() and "IronPython" or "CPython"

