# ticket_alert.py

import re
import call_centers
import parsers

class TicketAlertKeyword(object):
    def __init__(self, keyword, ticket_types):
        self.keyword = keyword
        self.keyword_regex = re.compile('\b%s\b' % keyword,
          re.MULTILINE|re.IGNORECASE) # Look for whole words
        self.ticket_type_regex = []
        for ticket_type in ticket_types:
            self.ticket_type_regex.append(re.compile(ticket_type))

class TicketAlertHandler:
    """ Determines if an alert needs to be sent for a given ticket.
        Works independent of call center or format. """

    def __init__(self, tdb, client_dict, hpgrid, hpaddress=None,
                 ticket_alert_keywords=None):
        self.tdb = tdb # in case we need to fetch UCCs
        self.client_dict = client_dict # as used by main.py
        self.hpgrid = hpgrid # HighProfileGrids instance
        self.hpaddress = hpaddress # HighProfileAddress instance

        # A dictionary whose keys are the call_centers and value is a dict
        # keyed by keyword and whose value is a list of ticket types
        self.ticket_alert_keywords = ticket_alert_keywords or {}

    def has_alert(self, ticket):
        """
        Determine if this ticket needs an "alert", by checking if it has
        clients that have alert=1.  If so, set ticket.alert.
        """
        alert = self._has_client_alert(ticket) or \
                self._has_hpgrid_alert(ticket) or \
                self._has_ticket_keyword(ticket) or \
                self._has_hpaddress_alert(ticket)
        return alert

    def _has_ticket_keyword(self, ticket):
        '''
        If the work_remarks contains a keyword and the ticket type matches,
        return True
        '''
        if self.ticket_alert_keywords.has_key(ticket.ticket_format):
            # Get the re pattern for the keyword section
            area = call_centers.cc2format[ticket.ticket_format]
            parser = parsers.get_parser(area[0])
            if hasattr(parser,"keyword_section"):
                m = parser.keyword_section.search(ticket.image)
                if m:
                    keyword_text = m.group(1).strip()
                    for ticket_alert_keyword, ticket_types in self.ticket_alert_keywords[ticket.ticket_format].iteritems():
                        m1 = re.search(ticket_alert_keyword, keyword_text, re.IGNORECASE)
                        m2 = None
                        if m1 is not None:
                            for ticket_type in ticket_types:
                                m2 = re.search(ticket_type.strip(), ticket.ticket_type, re.IGNORECASE)
                                if m2:
                                    break
                        if m1 and m2 is not None:
                            return True
        return False

    def _has_client_alert(self, ticket):
        """
        Return True if this ticket has any clients with alert=1.
        """
        if not self.client_dict:
            return False

        call_center = ticket.ticket_format

        # is this an update call center? if so, it should have a "master"
        cc = call_centers.get_call_centers(self.tdb)
        master_call_center = cc.update_call_center(ticket._update_call_center)

        if master_call_center:
            clients = self.client_dict.get(master_call_center, [])
            clients = [c for c in clients
                       if c.update_call_center == ticket._update_call_center]
        else:
            # regular call center
            clients = self.client_dict.get(call_center, [])

        # get names of clients on ticket
        client_names = [x.client_code for x in ticket.locates]

        # look if this ticket has a client that has alert=1
        alert = 0
        for client_name in client_names:
            for cl in clients:
                if client_name == cl.client_code and cl.alert == '1':
                    alert = 1
                    break

        return alert

    def _has_hpgrid_alert(self, ticket):
        """
        Return True if this ticket has any HP grids.
        """
        if not self.hpgrid:
            return False
        return bool(self.hpgrid.has_hp_grids(ticket, self.client_dict))

    def _has_hpaddress_alert(self, ticket):
        """
        Return True if this ticket has any HP addresses.
        """
        if not self.hpaddress:
            return False
        return bool(self.hpaddress.has_hp_address(ticket, self.client_dict))

    def get_hp_grid_clients(self, ticket):
        return self.hpgrid.get_hp_grid_clients(ticket, self.client_dict)

    def get_hp_address_clients(self, ticket):
        return self.hpaddress.get_hp_address_clients(ticket, self.client_dict)
