# soapresponder.py

# add path for lib/suds
import sys, os
whereami = os.path.dirname(os.path.abspath(__file__))
lib_path = os.path.join(whereami, 'lib')
sys.path = [p for p in sys.path if not 'suds-0.4' in p]
sys.path.append(lib_path)

import abstractresponder
import soapresponder_data as srd
import suds

class SOAPResponderError(abstractresponder.ResponderError):
    pass

class SOAPResponder(abstractresponder.AbstractResponder):
    name = 'SOAPResponder'
    responder_type = 'soap' # as used by ResponderConfigData

    def load_responder_data(self, call_center, raw_responderdata):
        self.responderdata = srd.getresponderdata(self.config, call_center,
                             raw_responderdata)
        self.responderdata.log = self.log

        self.log.log_event("Setting up SOAP client...")
        self.client = suds.client.Client(self.responderdata.url)

    def response_is_success(self, row, resp):
        return self.responderdata.response_is_success(row, resp)

    def make_response_string(self, ticket_number, membercode, responsecode,
                             explanation, version):
        return "DATA %s,%s,%s,%s%s," % (ticket_number, membercode,
                                        self.responderdata.sender,
                                        responsecode, explanation)
        # XXX trailing date for 3J?

    def send_response(self, ticket_number, membercode, responsecode,
                      explanation=None, locator_name='', version='0'):
        membercode = self.translate_term(membercode, {})

        rs = self.make_response_string(ticket_number, membercode,
             responsecode, explanation, version)
        self.log.log_event("Sending response string: %r" % rs)

        password = self._get_password(membercode)
        strs = self.client.factory.create("ns3:ArrayOfstring")
        strs.string.append(rs)
        result = self.client.service.PostStringResponse(membercode, password, strs)

        reply = self.responderdata.process_response(result)
        return str(reply[-1])

    def _get_password(self, membercode):
        for d in self.responderdata.logins:
            if d["login"] == membercode:
                return d["password"]
        raise SOAPResponderError("Unknown client code: %s" % membercode)

    def get_code_from_response(self, text):
        return text.lower().strip()

if __name__ == "__main__":

    abstractresponder.run_toplevel(SOAPResponder)


