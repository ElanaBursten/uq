# ticket_xml.py
# Generate XML for tickets, as per Mantis #2827.

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

import ticket

class TicketXMLGenerator:

    # fields that should not show up in the XML are listed here.
    TICKET_EXCLUDE = ["ticket_id"]
    LOCATE_FIELDS = ["client_code"]
    QM_FIELDS = ['source', 'due_date', 'work_lat', 'work_long']

    def __init__(self):
        pass # later: add config options here, if necessary

    def generate(self, aticket):
        """ Generate and return an ElementTree XML object from a ticket.
            Assumptions:
            - Ticket has locates that are already routed.
        """
        root = ET.Element('ticket')
        for fieldname, _ in ticket.Ticket.__fields__:
            if fieldname in self.TICKET_EXCLUDE:
                continue
            elem = ET.SubElement(root, fieldname)
            elem.text = str(getattr(aticket, fieldname, "") or "")
            elem.tail = "\n"

        for loc in aticket.locates:
            locnode = ET.SubElement(root, 'locate')
            locnode.tail = '\n'
            for fieldname in self.LOCATE_FIELDS:
                value = getattr(loc, fieldname)
                elem = ET.SubElement(locnode, fieldname)
                elem.text = str(value)
                elem.tail = '\n'

        qmnode = ET.SubElement(root, 'qmticket')
        qmnode.tail = '\n'
        for fieldname in self.QM_FIELDS:
            elem = ET.SubElement(qmnode, fieldname)
            elem.text = str(getattr(aticket, fieldname, "") or "")
            elem.tail = '\n'

        tree = ET.ElementTree(root)
        return tree

