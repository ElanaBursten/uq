# noclienthandler.py
# Created: 2002.11.04

import string
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

MAXLENGTH = 30

class NoClientHandler:
    """ Handles "no UQ client" warnings.
        Usage:

        1. check() :         check if a ticket has no UQ clients, and if so,
                             add its relevant data to the list
        2. send_warnings() : send warnings, if we have any
        3. flush() :         clear list
    """


    def __init__(self, tdb, clients, log, emails=None, verbose=1):
        self.tdb = tdb
        self.log = log
        self.clients = clients
        self.verbose = verbose
        self.emails = emails

        self.ticket_info = []

    def check(self, ticket, ticket_id, logic):
        call_center = ticket.ticket_format
        clients = [c.client_code for c in self.clients.get(call_center, [])]
        locates = [loc.client_code for loc in ticket.locates]

        if not self.has_uq_client(locates, logic, clients):
            self.log.log_event("No UQ clients found for %s ticket(s)" % call_center, dump=1 and self.verbose)
            self.log.log_event("Locates on ticket: %s" % (string.join(locates,',')), dump=1 and self.verbose)
            self.log.log_event("%s clients: %s" % (call_center, string.join(clients,',')), dump=1 and self.verbose)

            t = (call_center, ticket_id, ticket.ticket_number,
                 ticket.image)
            self.ticket_info.append(t)

    def flush(self):
        self.ticket_info = []

    def has_uq_client(self, locates_on_ticket, logic, clients):
        """ Return true (a value that evaluates to true) if the set of locates
            contains a UQ client.
            <logic> is the appropriate BusinessLogic instance for the given
            call center.
            <clients> is simply a list of client codes.
        """
        for locate in locates_on_ticket:
            if locate in clients:
                return 10   # client found
            elif not logic.do_no_client_check(locate):
                return 11   # don't do the "no client check" for this one
                            # IOW, it counts as a client

        if not logic.do_no_client_check(""):
            return 12   # don't do the check for _anything_
                        # IOW, pretend we have a client

        return 0    # all attempts failed

    def send_warnings(self):
        if not self.ticket_info:
            return

        # group by call center
        d = {}
        for call_center, ticket_id, ticket_number, image in self.ticket_info:
            if not d.has_key(call_center):
                d[call_center] = []
            d[call_center].append((ticket_id, ticket_number, image))

        # build and send a message for each call center
        for call_center, tlist in d.items():
            # build the message body
            f = StringIO()
            print >> f, "The following tickets have no locates"\
             " that are UQ clients:\n"
            warnings_sent = 0
            for ticket_id, ticket_number, image in tlist[:MAXLENGTH]:
                s = "** ticket_number: %s call_center: %s id: %s" % (
                 ticket_number, call_center, ticket_id)
                print >> f, s
                print >> f, "[image]"
                print >> f, image
                print >> f
                warnings_sent = warnings_sent + 1
            if self.emails:
                forward = self.emails[call_center].noclient
            else:
                forward = []
            self.log.log_warning(f.getvalue(), call_center=call_center,
             subject_hint="no UQ clients", forward=forward)
            self.log.log_event(
             "'No UQ clients' warning sent for %s %s ticket(s)" % (
             warnings_sent, call_center), dump=1 and self.verbose)


