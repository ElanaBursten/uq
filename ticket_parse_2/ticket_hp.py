# ticket_hp.py
# Placeholder for TicketHPInfo class.

import sqlmap
import static_tables

class TicketHPInfo(sqlmap.SQLMap):
    __table__ = "ticket_hp"
    __key__ = "ticket_hp_id"
    __fields__ = static_tables.ticket_hp

