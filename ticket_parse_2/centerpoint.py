# centerpoint.py

import centerpoint_grids
import date

CP_P_CODES = ('ALL_ELEC', 'ELEC_3PH', 'ELEC_MU', 'ELEC_S/L',
              'ELEC_URD', 'ELEC_ALL', 'ELEC-3PH', 'HLP-URD', 'HLP-3PH',
              'HLP-S/L', 'MU')
CP_G_CODES = ('ALL_GAS', 'GAS_MAIN', 'GAS_SERV', 'GAS_ALL', 'ENTX-MN', 'ENTX-SV',
              'IGSC', 'FOC')

def is_centerpoint(ticket):
    for loc in ticket.locates:
        if loc.client_code in ('CP-P', 'CP-G'):
            return 1
    return 0

def is_possibly_centerpoint(ticket):
    return centerpoint_grids.match(ticket.map_page) \
           and not is_centerpoint(ticket)

def reroute(tdb, ticket_id):
    locates = tdb.getrecords('locate', ticket_id=ticket_id)
    for loc in locates:
        if loc['status'] == '-R':
            locate_id = loc['locate_id']
            # set all/any assignments to inactive
            sql = "update assignment set active=0 where locate_id=%s" % (
                  locate_id,)
            tdb.runsql(sql)
    # set status to -P if status was -R
    sql = "update locate set status='-P' where status = '-R' and ticket_id=%s" % (ticket_id,)
    tdb.runsql(sql)

def reassign(tdb, log, ticket_id, locator):
    """ Reassign all locates with status -R to the given locator. """
    locates = tdb.getrecords('locate', ticket_id=ticket_id)
    for loc in locates:
        if loc['status'] == '-R':
            locate_id = loc['locate_id']
            assignments = tdb.getrecords('assignment', locate_id=locate_id, active=1)
            if (not assignments) or assignments[0]['locator_id'] != locator:
                # set all/any assignments to inactive
                sql = "exec assign_locate %s, %s" % (locate_id, locator)
                tdb.runsql(sql)
                log.log_event("[ULTRA] Locate %s assigned to %s" % (locate_id, locator))
            else:
                log.log_event("[ULTRA] Locate %s already assigned to %s"% (locate_id, locator))

def mark(ticket):
    """ Set a ticket's do_not_mark_before date to the next 11 PM. """
    assert ticket._filedate, "_filedate of Ticket must be set"
    d = date.Date(ticket._filedate)
    # go to 11 PM
    if d.hours >= 23:
        d.inc() # go to next day
    d.hours, d.minutes, d.seconds = 23, 0, 0
    ticket.do_not_mark_before = d.isodate()

def unmark(tdb, ticket_id):
    """ Unmark a ticket's do_not_mark_before date in the database. """
    sql = "update ticket set do_not_mark_before=null where ticket_id=%s" % (
          ticket_id,)
    tdb.runsql(sql)

