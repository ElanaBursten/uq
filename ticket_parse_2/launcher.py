# launcher.py

from __future__ import with_statement
import getopt
import os
import string
import sys
import time
import traceback
import win32api
import win32event
import xml.etree.ElementTree as ET
#
import config
import datadir
import dbinterface_ado
import errorhandling2
import et_tools
import listener
import mutex
import program_status
import version
import xmlconfigreader

WAIT = 5 # seconds
TOTAL_RUN_TIME = 9*60 # seconds

__usage__ = """\
launcher.py [options]

Attempt to launch a number of applications, configured in config_launcher.xml

Options:
    -C, --check    Check the XML configuration file, then quit.
    -t, --test     Do everything except actually launching the programs.
"""

def launcher_mutex_name():
    return "launcher:" + datadir.datadir.get_id()

class Launcher:

    def __init__(self, test=0, validate_xml=0, dbado=None):
        # default values
        self.wait = WAIT
        self.runtime = TOTAL_RUN_TIME
        self.pythonpath = "python" # override in config_launcher.xml
        self.test = test
        self.validate_xml = validate_xml
        if dbado:
            self.dbado = dbado
        else:
            self.dbado = dbinterface_ado.DBInterfaceADO()

        # Config needed for log dir and list of configured call centers
        self.cfg = config.getConfiguration()

        self.applications = self.read_configuration()

        smtpinfo = self.cfg.smtp_accs[0]
        log = errorhandling2.ErrorHandler(logdir=self.cfg.logdir,
              smtpinfo=smtpinfo,
              me="Launcher",
              admin_emails=self.cfg.admins,
              subject="Launcher Error Notification")
        self.log = log

        self._start()

    def _start(self):
        """ Start the launcher. Override this when testing etc. """
        self.log.log_event("Launcher started (version %s)" % (
         version.__version__))
        # only one instance of this *program* is allowed
        program_status.write_status(launcher_mutex_name())

    def read_configuration(self, filename="config_launcher.xml"):
        applications = []
        data = datadir.datadir.open(filename).read()
        dom = ET.fromstring(data)

        if self.validate_xml:
            et_tools.validate(dom)
            print "Configuration OK"; raise SystemExit

        ln = et_tools.find(dom, 'launcher')
        self.wait = int(ln.get('wait', 60))
        self.runtime = int(ln.get('runtime', 0))
        self.pythonpath = ln.get('pythonpath', 'python')

        applications = []
        for an in et_tools.findall(dom, 'application'):
            if an.get('name', '').lower().startswith('parser'):
                for process in self.cfg.processes:
                    if process['format']:
                        applications.append((an.get('name', '') + ' ' + process['format'],
                                             an.get('path', ''),
                                             an.get('mutex', ''),
                                             process['format']))
            else:
                applications.append((an.get('name', ''),
                                     an.get('path', ''),
                                     an.get('mutex', ''),
                                     ''))
        return applications

    def launch(self, test=0):
        self.flagged = [] # apps that don't run correctly won't be run a second time
        start = time.time()
        while (time.time() - start) < TOTAL_RUN_TIME:

            for (name, filepath, mutexname, call_center) in self.applications:

                # make sure mutex name ends in ':UQ' or equivalent
                biz_id = datadir.datadir.get_id()
                if mutexname.endswith(":"):
                    mutexname += biz_id
                elif not mutexname.endswith(biz_id):
                    mutexname = mutexname + ":" + biz_id

                if call_center:
                    mutexname = mutexname + ":" + call_center

                # if this program didn't start before, skip it
                if (name, filepath, mutexname, call_center) in self.flagged:
                    continue
                # check if mutex is available
                if mutex.mutex_is_locked(self.dbado, mutexname):
                    self.log.log_event("Mutex " + mutexname + " is already locked.")
                else:
                    self.log.log_event("Mutex " + mutexname + " is not locked.")
                    self.start(name, filepath, mutexname, call_center)

                # listen for 'quit' signal
                if 'quit' in listener.uqlistener.listen():
                    self.log.log_event("listener: 'quit' signal received, exiting...")
                    return

            time.sleep(WAIT)

        self.log.log_event("Ending launcher")

    def start(self, name, filepath, mutexname, call_center):
        self.log.log_event("Starting: %s (%s %s)" % (name, filepath, call_center))
        try:
            if not self.test:
                parts = filepath.split()
                # if it's a Python file, run it with Python, otherwise try
                # os.startfile (so we can still use batch files etc if
                # necessary)
                if parts and parts[0].endswith(".py"):
                    print repr(self.pythonpath), repr(parts), call_center
                    #os.spawnv(os.P_DETACH, self.pythonpath, parts)
                    os.system('start "%s" %s %s %s' % (name, self.pythonpath, filepath, call_center))
                elif call_center:
                    os.system('start "%s" "%s" %s' % (name, filepath, call_center))
                else:
                    os.startfile(filepath)
            self.log.log_event(" ...started")
            time.sleep(WAIT)
        except:
            info="An error occurred when trying to execute this application."
            ep = errorhandling2.errorpacket(info=info, name=name,
             filepath=filepath, call_center=call_center)
            self.log.log_error(ep, send=1, write=1, dump=1)
            self.flagged.append((name, filepath, mutexname, call_center))


()

if __name__ == "__main__":

    test = validate_xml = 0
    opts, args = getopt.getopt(sys.argv[1:], "Ct?", ["data=", "check", "test"])
    for o, a in opts:
        if o in ('-t', '--test'):
            test = 1
        if o == '-?':
            print >> sys.stderr, __usage__
            sys.exit(0)
        elif o == "--data":
            print "Data dir:", a
        elif o in ('-C', '--check'):
            validate_xml = 1

    mutex_name = launcher_mutex_name()
    try:
        dbado = dbinterface_ado.DBInterfaceADO()
        with mutex.MutexLock(dbado, mutex_name):
            launcher = Launcher(test=test, validate_xml=validate_xml, dbado=dbado)
            launcher.launch()
    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)
    except SystemExit:
        pass


