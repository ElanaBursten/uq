# Utiliquest Ticket Parser
# Last update: 13 Apr 2002, Hans Nowak

# todo: (TicketHandler) every time we parse, we create a TermTranslator...
#       it would probably better to create it once as an instance attribute

import os
import string
#
import call_centers
import formats
import parsers
import recognize
import summaryparsers
import termtranslator
import ticket_adapter
import ticketloader

controlL = chr(ord('L')-64)

# get the absolute path of translations.py since
# we may not be running in the same directory
import translations as trans
translations = os.path.join(os.path.dirname(trans.__file__), "translations.py")

class TicketError(Exception):
    """ Raised if there is a problem with parsing a ticket. """

def ticket_diff(original, update):
    """ Return a list of fields of ticket <original> that have changed in
        <update>. """
    IGNORE = ["ticket_id"]
    changed = []

    for key, value in original.__dict__.items():
        if key.startswith("_"):
            continue
        v1 = getattr(original, key) # value in original ticket
        v2 = getattr(update, key)   # value in update

        # if v2 is None, leave this field alone
        if v2 != None and v1 != v2:
            changed.append(key)

    # XXX what about locates??
    # XXX untested! write a test for this...

    return changed


class GMMessage:
    """ Dummy class to indicate "good morning" messages (should be ignored by
        most callers) """
    def __init__(self, _format="", image=""):
        self.format = _format
        self.image = image

#
# TicketBulkParser

class TicketBulkParser:
    """ Quick loading and parsing of bulk tickets. Replace or overload the
        action() method to do something with each ticket.
        Note that this class assumes that all tickets can be parsed
        correctly, so it doesn't do any exception handling to catch parsing
        errors.
    """
    def __init__(self, _format=""):
        self.format = _format
        self.handler = TicketHandler()

    def load(self, lst):
        # lst is a list of filenames that is used to load a bunch
        # of files with TicketLoader and parse the tickets.
        if self.format:
            suggestion = [self.format]
        else:
            suggestion = []
        for filename in lst:
            tl = ticketloader.TicketLoader(filename)
            tick = tl.getTicket()
            while tick:
                try:
                    ticket = self.handler.parse(tick, suggestion)
                except:
                    pass    # do nothing; ticket is probably invalid
                else:
                    self.action(ticket)
                tick = tl.getTicket()

    def action(self, ticket):
        # must be overloaded to do something useful. <ticket> is a parsed
        # ticket, an instance of the Ticket class.
        pass

###
### TicketHandler

class TicketHandlerError(Exception):
    pass

class TicketHandler:
    """ Generic ticket handler. Capable of handling both normal tickets and
        summaries. Also does the recognizing part.
    """

    def __init__(self):
        pass

    def parse(self, data, suggested_formats=(), exclude=None, call_center=None):
        """ Parse data. Return a Ticket instance or a TicketSummary
            instance. If data wasn't recognized as a valid ticket or summary,
            raise an exception.
            suggested_formats is a list or tuple of suggested format names. If
            empty, the normal recognize procedure is started. If it does
            contain formats, then the function checks if the ticket can be
            recognized as one of these. If that is not the case, an exception
            is raised.

            XXX Refactoring suggestion: move some of this code to methods
        """
        if exclude is None:
            exclude = []

        work_order = (call_center in call_centers.work_order_centers)

        if suggested_formats:
            for fmt in suggested_formats:
                z = recognize.recognize_as(data, fmt, work_order=work_order)
                if z:
                    _type, area = z, fmt
                    break
            else:
                # we're here if we didn't break, or, in other words, if we
                # didn't find anything
                s = "Ticket format not in %s" % (suggested_formats)
                raise TicketHandlerError, s
        else:
            _format = recognize.recognize_type(data)
            if not _format:
                raise TicketHandlerError("Unrecognized ticket")
            _type, area = _format

        if _type == "ticket":
            # return a Ticket instance, parsed and filled by the appropriate
            # Parser class

            # get Parser class for this format
            parser = parsers.get_parser(area)

            tt = termtranslator.TermTranslator(translations)

            # instantiate it and parse the ticket
            p = parser(data, area, exclude=exclude, term_translator=tt)
            ticket = p.parse()
            self.check_field_lengths(ticket)

            tt.translate(ticket)

            # make call center based adaptations, if appropriate
            try:
                adapter_class = ticket_adapter.get_adapter(ticket.ticket_format)
            except ticket_adapter.TicketAdapterError:
                pass # no adaptation is done for this ticket
            except:
                import traceback
                traceback.print_exc()
            else:
                adapter = adapter_class()
                adapter.adapt(ticket)

            return ticket

        elif _type == "summary":
            # return a parsed TicketSummary instance
            parser = summaryparsers.get_summaryparser(area)
            p = parser(area)
            summary = p.parse(data)

            tt = termtranslator.TermTranslator(translations)
            tt.translate_summary(summary)

            return summary

        elif _type == "gm_message":
            # messages are no longer ignored
            return GMMessage(_format=area, image=data)

        elif _type == "work_order":
            # get parser, parse work order and return it
            wop = formats.get_work_order_parser(area)()
            wo = wop.parse(data)
            return wo

        elif _type == "work_order_audit":
            woap = formats.get_work_order_audit_parser(area)()
            woa = woap.parse(data)
            return woa

        else:
            raise TicketError("Unknown type: %s" % _type)

    @staticmethod
    def check_field_lengths(ticket):
        """ Check the fields in the ticket. Collect any fields that are too
            long, truncate the field and add ellipses to indicate that the
            ticket image should be consulted.
        """

        def get_long_fields(obj, name):
            return [(name+"."+fname, a, b, c)
                    for (fname, a, b, c) in obj.check_field_lengths()]

        toolong = get_long_fields(ticket, "ticket")
        for loc in ticket.locates:
            toolong.extend(get_long_fields(loc, "locate"))

        if toolong:
            msg = "The following fields are too long:\n"
            for fieldname, maxlength, reallength, value in toolong:
                if fieldname.startswith('ticket') and maxlength > 4:
                    field = fieldname.split('.')[1]
                    ticket._data[field] = ticket._data[field][:maxlength-3]+'...'
                else:
                    s = " %s (allowed %s, real length %s)\n value: %s\n" % (
                        fieldname, maxlength, reallength, repr(value))
                    msg = msg + s
                    raise TicketHandlerError, msg

