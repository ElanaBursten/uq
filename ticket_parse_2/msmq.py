# msmq.py

import os
import sys
import win32com.client

# XXX not sure about performance; maybe we should have an object that reuses
# the MSMQDestination.
# XXX also, maybe the DIRECT and OS parts should be parameters.
def send_message(computer, queue, data, debug=0):
    """ Needs name of computer, then name of queue, qualified (e.g. a queue
        named Q1 in 'Private Queues' is private$\Q1.
        A pywintypes.com_error will be raised if the queue does not exist or
        is otherwise inaccessible.
    """
    dest = win32com.client.Dispatch("MSMQ.MSMQDestination")
    msg = win32com.client.Dispatch("MSMQ.MSMQMessage")

    dest.FormatName = "DIRECT=OS:%s\%s" % (computer, queue)
    msg.Label = "UQ Message"
    msg.Body = data

    if debug:
        print "dest.FormatName:", dest.FormatName
        print "msg.Body:", data

    msg.Send(dest)
    dest.Close()


if __name__ == "__main__":

    # simple test; assumes that a private queue named Q1 has been set up on
    # the local machine

    msg = sys.argv[1:] and sys.argv[1] or 'hello world'
    send_message(os.environ['COMPUTERNAME'], 'private$\\Q1', msg, debug=1)

