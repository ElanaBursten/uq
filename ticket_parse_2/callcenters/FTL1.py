# FTL1.py

import businesslogic
import date
import c1391
import FHL3
import duedates

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "2303"
    call_center = "FTL1"

    '''
    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, hours=2)
        else:
            cd = self.dcatalog.inc_n_days(cd, 3)
        return cd.isodate()
    '''
    legal_due_date = duedates.due_date_TN

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_grid_county_state_TN,
            self.lcatalog.munic,
        ]

    def allow_summary_updates(self):
        return 1

class XMLHTTPResponderData(c1391.XMLHTTPResponderData):
    pass

class FTPResponderData(FHL3.FTPResponderData):
    TEMPLATE2 = """\
    <Locate type="CUSTOM2" membercode="%(company)s" completionid="%(locate_id)s;%(resp_id)s">
        <customerid>ATMOS</customerid>
        <occid>TNOC</occid>
        <Jobid>%(ticket_number)s</Jobid>
        <Membercode>%(company)s</Membercode>
        <Operatorid>UTILIQUEST</Operatorid>
        <Completiondtdate>%(completion_date)s</Completiondtdate>
        <Completiondttime>%(completion_time)s</Completiondttime>
        <reasonid>%(status)s</reasonid>
        <faccode1>GAS</faccode1>
        <ismarked1>%(ismarked)s</ismarked1>
        <isclearexcavate1>%(isclearexcavate)s</isclearexcavate1>
        %(actcode)s
        <Remarks>%(remarks)s</Remarks>
    </Locate>
    """

    def extra_data(self, namespace):
        self.row = self.tdb.getrecords("locate", locate_id=namespace['locate_id'])[0]
        remarks = self.get_locate_notes()
        return {'remarks': remarks}

    def extra_names(self, namespace):
        # Use the closed_date, if available (should be)
        self.row = self.tdb.getrecords("locate", locate_id=namespace['locate_id'])[0]
        now = self.row['closed_date']
        if now is None:
            # No closed date, set to now
            now = date.Date().isodate()
        extra = {}
        extra['actcode'] = ''
        extra['completion_date'] = now[:10]
        extra['completion_time'] = now[11:] + ".000"
        status = namespace['status']
        extra['status'] = status
        explanation = namespace['explanation']
        if status == "CLEAR" and explanation == "NO GAS LINES":
            extra['ismarked'] = 0
            extra['isclearexcavate'] = 1
            extra['actcode'] = "<actcode1>NO GAS LINES</actcode1>"
        elif status == "LOCATED" and explanation == "MARKED":
            extra['ismarked'] = 1
            extra['isclearexcavate'] = 0
            extra['actcode'] = "<actcode1>MARKED</actcode1>"
        elif status == "LOCATE DELAYED":
            extra['ismarked'] = 0
            extra['isclearexcavate'] = 0
        elif status == "CANNOT LOCATE":
            extra['ismarked'] = 0
            extra['isclearexcavate'] = 0
        return extra

registry = {
    ('FTL1', 'xmlhttp', ''): XMLHTTPResponderData,
    ('FTL1', 'ftp', ''): FTPResponderData,
}

