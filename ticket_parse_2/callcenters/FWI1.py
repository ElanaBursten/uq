# FWI1.py

import businesslogic
import date

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = 'FWI1'
    DEFAULT_LOCATOR = "5337"

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_latlong_qtrmin,
            self.lcatalog.munic,
        ]

    def legal_due_date(self, row):
        if row.get('work_date'):
            return row['work_date']
        # in case work date isn't set, use the following
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, 1)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days(cd, 3)
        return cd.isodate()

registry = {}

