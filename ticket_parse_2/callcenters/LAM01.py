# LAM01.py
# LAM01 work order call center.
# (Mantis 2740..2745)

from __future__ import with_statement
import StringIO
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
#
import et_tools
from wo_businesslogic import WorkOrderBusinessLogic as WOBL
import wo_responder
import ticket_db
import tools

class WorkOrderBusinessLogic(WOBL):

    DEFAULT_LOCATOR = "15550"

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_latlong_qtrmin,
            #self.lcatalog.map_area,
            self.route_map_page,
            self.lcatalog.munic,
        ]

    #
    # custom routing methods, in progress

    def route_munic(self, row):
        locator = area_id = None
        locator_info = self.tdb.find_munic_area_locator_2(
                       row.get("work_state", ""), row.get("work_county", ""),
                       row.get("work_city", ""), self.call_center)
        if locator_info:
            locator = locator_info[0].get("emp_id", None)
            area_id = locator_info[0].get("area_id", None)
        return tools.RoutingResult(locator=locator, area_id=area_id)

    def route_map_page(self, row):
        locator = None
        map_page = row.get("map_page", "")
        if map_page:
            try:
                code, page, coords = map_page.split()
            except ValueError:
                return tools.RoutingResult()
                # invalid map_page, locator cannot be determined
            code = self.call_center + "-" + code
            routingresult = self.lcatalog.map_area_with_code(row, code)
            return routingresult

        return tools.RoutingResult()

#
# responder

# NOTE: Sample XML for the responses can be found at
# ticket_parse/testdata/responder/LAM01.

class WorkOrderResponderData(wo_responder.WorkOrderResponderData):

    RESPONSE_FIELDS = [
        (u'InternalJobID', 'client_wo_number'),
        (u'ExtentOfWork', 'work_description'),
        (u'CrossStreet', 'work_cross'),
        (u'NumRoadBores', 'road_bore_count'),
        (u'NumDriveWayBores', 'driveway_bore_count'),
        (u'StateRoadROW', 'state_hwy_row'),
        (u'SketchURL', '!attachments'),
        (u'DispatchCompleteDate', 'closed_date'),
        (u'Status', 'status'),
        (u'Notes', '!notes'),
        (u'Remarks', 'remarks'),
        (u'ResponseId', '!resp_id'),
    ]

    def extra_data(self, row):
        # these fields are numeric and must show up as '0' if None
        for fn in ['road_bore_count', 'driveway_bore_count', 'state_hwy_row']:
            row[fn] = row[fn] or 0

    def write_xml(self, rows, filename):
        root = ET.Element(u"WorkOrderResponses")
        root.tail = u'\n'
        for row in rows:
            r = ET.SubElement(root, u"Response")
            for xmlname, fieldname in self.RESPONSE_FIELDS:
                node = ET.SubElement(r, xmlname)
                node.text = str(row.get(fieldname, '')).decode('UTF-8') or u''
                node.tail = u'\n'
            r.tail = u'\n'

        # write XML, with correct header
        # XXX header stuff could be wrapped in et_tools.py
        tree = ET.ElementTree(root)
        c = StringIO.StringIO()
        tree.write(c, encoding='UTF-8') # or something
        with open(filename, 'wb') as f:
            f.write(c.getvalue())

registry = {
    ('LAM01', 'workorder', ''): WorkOrderResponderData,
}

