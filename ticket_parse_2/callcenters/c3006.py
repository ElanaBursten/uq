# c3006.py
# GA 811 ATT Translore

import c3003
import _GA811_responder as gr
import xmlhttpresponder_data as xhrd
from xml.sax.saxutils import escape

class BusinessLogic(c3003.BusinessLogic):
    call_center = '3006'

class SOAPResponderData(gr.GA811SOAPResponderData):
    pass

class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):

    acceptable_codes = ["250 ok", "ok"]
    codes_ok = ['successful', '250 ok', 'response posted', 'ok']

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="UTF-8" ?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema">
 <soap:Body>
   <UpdateTicket xmlns="http://translore.com/ResponseService/">
     <AuthID xmlns="">%(id)s</AuthID>
     <CallCenter xmlns="">3006</CallCenter>
     <TicketNumber xmlns="">%(ticket)s</TicketNumber>
     <TicketRevision xmlns="">0</TicketRevision>
     <MessagePayload xmlns="">
       <PayloadData>
         <Response>%(responsecode)s%(explanation)s</Response>
         <Notes>%(notes)s</Notes>
         <Status>%(responsecode)s</Status>
         <Who>UQ</Who>
         <Utility>%(termid)s</Utility>
         <WorkTime>%(locdatetime)s</WorkTime>
       </PayloadData>
     </MessagePayload>
   </UpdateTicket>
 </soap:Body>
</soap:Envelope>
"""

    def get_xml(self, names):
        notes = self.get_ticket_notes()
        closed_date = self.row['closed_date']
        if closed_date is None:
            # No closed date, set to now
            closed_date = date.Date().isodate()
        names.update({
            'id': escape(self.id),
            'locdatetime': closed_date.replace(" ", "T"),
            'notes': self.check_notes(notes),
            'termid': self.row['client_code'],
        })
        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

registry = {
    ('3006', 'soap', ''): SOAPResponderData,
    ('3006', 'xmlhttp', ''): XMLHTTPResponderData,
}

