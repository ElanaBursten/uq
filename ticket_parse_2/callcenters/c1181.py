# c1181.py

import businesslogic
import date
import NewJersey

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = '1181'
    DEFAULT_LOCATOR = '13645'

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
        ]

    def legal_due_date(self, ticketrow):
        if self.isemergency(ticketrow):
            dd = date.Date(ticketrow['call_date'])
            dd = self.dcatalog.inc_n_hours(dd, 2)
            return dd.isodate()
        else:
            cd = self.base_date(ticketrow['call_date'])
            # +48 hours, then set to midnight
            dd = date.Date(cd)
            dd = self.dcatalog.inc_n_days(dd, 2)
            self.dcatalog.set_midnight(dd)
            return dd.isodate()

class FTPResponderData(NewJersey.FTPResponderData):

    TEMPLATE2 = """\
    <Locate type="CUSTOM2" membercode="%(company)s"
            completionid="%(locate_id)s;%(resp_id)s">
        <customerid>SYSTEM</customerid>
        <occid>WVOC</occid>
        <Jobid>%(jobid)s</Jobid>
        <Membercode>%(company)s</Membercode>
        <Operatorid>UTILIQUEST</Operatorid>
        <Completiondtdate>%(completion_date)s</Completiondtdate>
        <Completiondttime>%(completion_time)s</Completiondttime>
        <reasonid>%(status)s</reasonid>
        <faccode1>ELECTRIC</faccode1>
        <ismarked1>%(ismarked)s</ismarked1>
        <isclearexcavate1>%(isclearexcavate)s</isclearexcavate1>
        <Remarks>%(remarks)s</Remarks>
    </Locate>
    """

    def extra_data(self, namespace):
        extra = NewJersey.FTPResponderData.extra_data(self, namespace)
        raw = namespace['raw']
        extra['jobid'] = raw['serial_number'] or raw['ticket_number']
        return extra

    # status mappings to determine 'ismarked' etc are different from
    # FJL1/NewJersey
    MARK_SETTINGS = {
        'CLEAR/NOT INVOLVED': (0, 1, '<actcode1>NO GAS LINES</actcode1>'),
        'CLEAR': (0, 1, '<actcode1>NO GAS LINES</actcode1>'),
        'MARKED': (1, 0, '<actcode1>MARKED</actcode1>'),
    }

registry = {
    ('1181', 'ftp', ''): FTPResponderData,
}

