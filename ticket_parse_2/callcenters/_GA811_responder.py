# _GA811_responder.py

import xmlhttpresponder_data as xhrd
import soapresponder_data as srd

class GA811SOAPResponderData(srd.SOAPResponderData):
    codes_ok = acceptable_codes = ["250", "251"]
    codes_error = ["450", "451", "452", "453", "454", "455", "456"]
    codes_reprocess = ["500", "530"]
    def response_is_success(self, row, resp):
        response, explanation_code, return_code = resp
        return return_code in self.codes_ok
    def process_response(self, reply):
        return reply[0]


