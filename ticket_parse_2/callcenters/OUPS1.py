# OUPS1.py

import businesslogic
import date
import duedatecatalog

class BusinessLogic(businesslogic.BusinessLogic):
    DEFAULT_LOCATOR = "20368"
    call_center = 'OUPS1'

    @staticmethod
    def non_emergency_due_date(isodate):
        return duedatecatalog.DueDateCatalog.dec_if_midnight(
          date.Date(isodate), minutes=1).isodate()

    def legal_due_date(self, ticketrow, is_update=0):
        transmit_date = ticketrow.get('transmit_date', None)
        respond_date = ticketrow.get('respond_date', None)
        assert transmit_date, 'transmit_date cannot be blank'

        if self.isemergency(ticketrow):
            due_date = self.dcatalog.inc_emergency(date.Date(transmit_date),
                                                   hours=2)
            return duedatecatalog.DueDateCatalog.dec_if_midnight(
              due_date, minutes=1).isodate()
        else:
            return self.non_emergency_due_date(respond_date or transmit_date)

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_latlong_qtrmin,
            self.lcatalog.munic
        ]

    def allow_summary_updates(self):
        return 1

registry = {}

