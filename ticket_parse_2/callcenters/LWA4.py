# LWA4.py

import LWA1
import _LocIncResponder as lir

class BusinessLogic(LWA1.BusinessLogic):
    call_center = 'LWA4'

class XMLHTTPResponderData(lir.XMLHTTPResponderData):
    ONE_CALL_CENTER = 'UULC'

registry = {
    ('LWA4', 'xmlhttp', ''): XMLHTTPResponderData,
}


