# FJT1.py

import businesslogic
import FTL1
import c1391

class BusinessLogic(FTL1.BusinessLogic):
    # same as FTL1, except for the default locator
    call_center = 'FJT1'
    DEFAULT_LOCATOR = '4060'

class XMLHTTPResponderData(c1391.XMLHTTPResponderData):
    pass

registry = {
    ('FJT1', 'xmlhttp', ''): XMLHTTPResponderData,
}

