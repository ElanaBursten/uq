# LOR1.py

import businesslogic
import date
import tools
import FOR1
import _LocIncResponder as lir

S_EMERGENCY = ['++EMERGENCY++',
               '++EMERGENCY++ CANCELLATION',
               '++EMERGENCY++ CORRECTION',
               '++EMERGENCY++ DUPLICATION',
               '++EMERGENCY++ UPDATE',
               'EMERGENCY']

S_MEET = ['+MEET TIME+',
          '+MEET TIME+ DUPLICATION',
          '+MEET TIME+ UPDATE',
          'MEET TIME']

S_PRE_SURVEY = ['PRE-SURVEY',
                'PRE-SURVEY CANCELLATION',
                'PRE-SURVEY CORRECTION',
                'PRE-SURVEY DUPLICATION',
                'PRE-SURVEY UPDATE']

class BusinessLogic(FOR1.BusinessLogic):
    call_center = 'LOR1'
    DEFAULT_LOCATOR = '271' # LocQM database

    def legal_due_date(self, row):
        # Mantis #3006
        wd = date.Date(row['work_date'])
        cd = date.Date(row['call_date'])

        tt = row['ticket_type']
        if tools.find_any(tt, S_EMERGENCY) and 'NON-EMERGENCY' not in tt:
            # greater of (call_date + 2h, work_date)
            cd2 = self.dcatalog.inc_n_hours(cd.copy(), 2)
            return max(cd2.isodate(), wd.isodate())
        elif tools.find_any(tt, S_PRE_SURVEY):
            return wd.isodate()
        elif tools.find_any(tt, S_MEET):
            # greater of (call_date + 2h, work_date)
            cd2 = self.dcatalog.inc_n_hours(cd.copy(), 2)
            return max(cd2.isodate(), wd.isodate())
        else:
            # greater of (call_date + 2d, work_date)
            dd1 = self.dcatalog.inc_n_days(cd.copy(), 2)
            return max(dd1.isodate(), wd.isodate())

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic, # has higher priority now than township
            self.township,  # special!
        ]

    def township(self, row):
        return self.lcatalog.township(row, (3, 3, 2))


class XMLHTTPResponderData(lir.XMLHTTPResponderData):
    ONE_CALL_CENTER = 'OUNC'

registry = {
    ('LOR1', 'xmlhttp', ''): XMLHTTPResponderData,
}


