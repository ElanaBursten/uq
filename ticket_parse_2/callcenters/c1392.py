# c1392.py

import businesslogic
import date

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = "1392"
    DEFAULT_LOCATOR = "6847"

    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, 2)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days(cd, 2) # 48 hours
        return cd.isodate()

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            #self.lcatalog.township,
            self.lcatalog.munic,
        ]

registry = {}

