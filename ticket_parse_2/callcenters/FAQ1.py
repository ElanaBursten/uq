# FAQ1.py

import businesslogic
import date

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = 'FAQ1'
    DEFAULT_LOCATOR = '4418'

    # for now, we use a 'default' due date computation
    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, 2)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days(cd, 2)
        return cd.isodate()

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.township,
            self.lcatalog.munic,
        ]

registry = {}

