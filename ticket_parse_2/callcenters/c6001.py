# c6001.py

import businesslogic
import date
import FDX1
import locate
import ticket_adapter
import SCA1

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "6010"
    call_center = "6001"

    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row) or self.isdigup(row):
            cd = self.dcatalog.inc_emergency(cd, 2)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days(cd, 2)
        return cd.isodate()

    # special routing is copied verbatim from FDX1:
    route_special = FDX1.BusinessLogic.route_special.im_func

    def locator_methods(self, row):
        return [
            self.route_special,
            self.route_by_routinglist,
            #self.lcatalog.map_grid_county_state_TX,
            self.route_map_area_with_code,
            self.lcatalog.map_qtrmin_long,
            self.lcatalog.munic,
        ]

    def isdigup(self, row):
        ticket_type = row['ticket_type'].lower()
        return ticket_type.find("dig-up") > -1

    def route_map_area_with_code(self, row):
        # map name (e.g. MAPSCO) is ignored; instead, we use map 'FHL2'
        return self.lcatalog.map_grid_county_state_TX(row, code='6001')


class TicketAdapter(ticket_adapter.TicketAdapter):

    # 'gas' rules are the same as FDX1 (although term ids aren't).

    # if these locates are present, then we also add <locate_name> + "G"
    G_LOCATES = ["CS3", "DFW", "FN3", "MKE", "TC2", "FS3"]

    def adapt(self, ticket):
        new_locates = []
        loc_names = [x.client_code for x in ticket.locates]
        for loc in ticket.locates:
            if loc.client_code in self.G_LOCATES \
            and (loc.client_code + "G") not in loc_names:
                new_locate = locate.Locate(loc.client_code + "G")
                new_locates.append(new_locate)

        ticket.locates.extend(new_locates)

class XMLHTTPResponderData(FDX1.XMLHTTPResponderData):
    pass

registry = {
    ('6001', 'xmlhttp', ''): XMLHTTPResponderData,
}

