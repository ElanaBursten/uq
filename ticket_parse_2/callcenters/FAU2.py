# FAU2.py

import businesslogic
import date
import Atlanta

class BusinessLogic(businesslogic.BusinessLogic):

    call_center = "FAU2"
    DEFAULT_LOCATOR = Atlanta.BusinessLogic.DEFAULT_LOCATOR

    def legal_due_date(self, row):
        # XXX this is a dummy method
        d = date.Date(row["transmit_date"])
        d = date.Date(self.base_date(d.isodate()))
        d.inc(2)
        return d.isodate()

registry = {}

