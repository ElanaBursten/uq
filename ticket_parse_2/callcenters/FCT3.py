# FCT3.py

import businesslogic
import FTL1
import date

class BusinessLogic(FTL1.BusinessLogic):
    call_center = 'FCT3'
    DEFAULT_LOCATOR = '4061'

    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, hours=2)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days_to_time(cd, 2, 0, minutes=1)
            '''
            After the recent change for the due dates (mantis 2071),
            the users no longer have the visual que of the "yellow" tickets.
            We need to adjust the due date so that it is due 2 minutes earlier
            at 11:59 PM preceeding the actual due date time. For example,
            instead of the due date/time = 12:01 on Tues, it should be 11:59
            on Monday.
            That way the techs know which tickets will become late the next
            morning using the visual colors already in the system.
            '''
            cd.dec_time(hours=0, minutes=2, seconds=0)

        return cd.isodate()

registry = {}

