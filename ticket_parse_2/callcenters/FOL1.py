# FOL1.py

import businesslogic
import date

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "675"
    call_center = "FOL1"

    def legal_due_date(self, ticketrow):
        # same as FCL1
        emergency = ticketrow["kind"] == "EMERGENCY"
        d = date.Date(ticketrow["transmit_date"])
        due_date = self._due_date(d, emergency).isodate()
        return due_date

    def _due_date(self, transmit_date, emergency=0):
        d = date.Date(transmit_date)
        if emergency:
            d = self.dcatalog.inc_n_hours(d, 2)
        else:
            # add 2 days, but ignore weekends
            d = date.Date(self.base_date(d.isodate()))
            d = self.dcatalog.inc_n_days(d, 2)
            if transmit_date.isodate() >= "2002-10-01":
                d = self.dcatalog.set_midnight(d)
        return d

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
        ]

registry = {}

