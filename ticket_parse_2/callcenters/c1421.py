# c1421.py

import date
import emailresponder_data as erd
import string
import FGV1
import FJL1
import _c1421
import xmlhttpresponder_data as xhrd
from xml.sax.saxutils import escape
import re

class BusinessLogic(FGV1.BusinessLogic):
    call_center = '1421'
    DEFAULT_LOCATOR = "10432"

    def _due_date(self, transmit_date, emergency=0):
        d = date.Date(transmit_date)
        if emergency:
            d = self.dcatalog.inc_n_hours(d, 2)
        else:
            # add 3 days, but ignore weekends
            d = date.Date(self.base_date(d.isodate()))
            d = self.dcatalog.inc_n_days(d, 3)
            d.set_time(23, 59, 59)
        return d

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
        ]

class EmailResponderData(_c1421.EmailResponderData):
    pass


class XMLHTTPResponderData(_c1421.XMLHTTPResponderData):
    queue = {'table': 'responder_multi_queue', 'respond_to': 'client'}


# alternative PUPS responder. the old responder (also PUPS) remains in use as
# well.
#
class XMLHTTPPUPSResponderData(xhrd.XMLHTTPResponderData):
    # this call center doesn't actually check the responses
    acceptable_codes = ["success"]
    codes_ok = ['success', 'noresponsesfound']
    codes_reprocess = ['databasefailure']
    codes_error = ['nocode', 'noorganization', 'noticket', 'invalidaction',
                   'validationfailure']

    ONE_CALL_CENTER = 'PUPS'
    XML_TEMPLATE = """
<locateResponses xmlns="http://schemas.progressivepartnering.com/geocall/v3/response/v1">
    <auth user="%(username)s" password="%(password)s" />
    <responses>
        <response>
            <ticket>%(ticket)s</ticket>
            <code>%(membercode)s</code>
            %(facility)s
            <action>%(responsecode)s</action>
            <result></result>
            <comment>%(explanation)s</comment>
        </response>
    </responses>
</locateResponses>
    """

    def get_xml(self, names):
        # note: we assume that 'initials' in config.xml is the username, and
        # 'id' is the password.
        names['username'] = escape(self.initials)
        names['password'] = escape(self.id)

        rec = self.tdb.getrecords('client', oc_code=self.row['client_code'])[0]
        fac_type = (rec['utility_type'] or "").lower()
        facility = ''
        # types used in client table: ugas, pipl, elec, phon, catv, watr
        if 'elec' in fac_type:
            names['facility'] = '<facilities>Electric</facilities>'
        elif ('gas' in fac_type) or ('pipl' in fac_type):
            names['facility'] = '<facilities>Gas</facilities>'
        elif 'phon' in fac_type:
            names['facility'] = '<facilities>Fiber</facilities>'
        else:
            names['facility'] = '<facilities>Electric</facilities>'

        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        code, _, s = resp
        return s.lower().strip() == 'success'

    def process_response(self, reply):
        # reply is a COM Object containing XML
        # XXX extract the relevant part only? <status> | <result>
        xmlblurb = reply.xml
        m = re.search("<status>(.*?)</status>", xmlblurb)
        if m is not None:
            return m.group(1)
        m = re.search("<result>(.*?)</result>", xmlblurb)
        if m is not None:
            return m.group(1)
        return 'unknown'


class FTPResponderData(FJL1.FTPResponderData):
    """ South Carolina Korterra responder. """

    # Template is much like FJL, but doesn't have a reason code or actcode1.

    TEMPLATE2 = """\
    <LocateCollection>
    <Locate type="CUSTOM2" membercode="%(company)s" completionid="%(locate_id)s;%(resp_id)s">
        <customerid>PROGRESS</customerid>
        <occid>SCOC</occid>
        <Jobid>%(ticket_number)s</Jobid>
        <Membercode>%(company)s</Membercode>
        <Operatorid>UTILIQUEST</Operatorid>
        <Completiondtdate>%(completion_date)s</Completiondtdate>
        <Completiondttime>%(completion_time)s</Completiondttime>
        <faccode1>ELECTRIC</faccode1>
        <ismarked1>%(ismarked)s</ismarked1>
        <isclearexcavate1>%(isclearexcavate)s</isclearexcavate1>
        <Remarks>%(remarks)s</Remarks>
    </Locate>
    </LocateCollection>
    """

    # we use the same extra_names() method; the 'actcode1' key is ignored when
    # creating the XML.

registry = {
    ('1421', 'xmlhttp', ''): XMLHTTPResponderData,
    ('1421', 'xmlhttp', 'pups'): XMLHTTPPUPSResponderData,
    ('1421', 'email', ''): EmailResponderData,
    ('1421', 'ftp', ''): FTPResponderData,
}

