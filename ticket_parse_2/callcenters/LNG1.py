# LNG1.py

import LOR1

class BusinessLogic(LOR1.BusinessLogic):
    call_center = 'LNG1'

    def legal_due_date(self, row):
        return row['work_date']
    due_date = legal_due_date

    def allow_summary_updates(self):
        """ Return true if summaries can be updated. """
        return 1

registry = {}

