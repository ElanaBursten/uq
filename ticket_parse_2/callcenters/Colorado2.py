# Colorado2.py
import string
import Colorado
from summaryparsers import BaseSummaryParser, SummaryError
import summarydetail
from re_shortcut import R

class Parser(Colorado.Parser):
    call_center = 'Colorado2'

class SummaryParser(BaseSummaryParser):

    re_header = R("^This is your End of Day Audit from IRTHNet for items sent")
    re_footer = R("^Total Items Sent: (\d+)")
    re_summary_date = R("^since ([0-9/]+) [0-9:]+ [AP]M")
    CLIENT_CODE = 'UNCC'

    def read2(self, data):
        tickets = []
        lines = data.split("\n")
        while lines and not lines[0].startswith("--------"):
            del lines[0]
        if lines:
            del lines[0] # remove "----"

        for line in lines:
            if line.strip():
                parts = line.split()
                seq_num, ticket_number = parts[:2] # this is all we need
                detail = summarydetail.SummaryDetail(
                         [seq_num, ticket_number, "00:00", "", ""])
                print detail
                tickets.append(detail)
            else:
                break # done!
            
    def has_header(self, data):
        return bool(self.re_header.search(data))

    def has_footer(self, data):
        return bool(self.re_footer.search(data))

    def get_expected_tickets(self, data):
        m = self.re_footer.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    def get_summary_date(self, data):
        m = self.re_summary_date.search(data)
        if m:
            m, d, y = map(int, m.group(1).split('/'))
            return "%04d-%02d-%02d 00:00:00" % (y, m, d)
        else:
            raise SummaryError, "No date found on audit"

    def get_client_code(self, data):
        return self.CLIENT_CODE

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        while lines and not lines[0].startswith("--------"):
            del lines[0]
        if lines:
            del lines[0] # remove "----"

        for line in lines:
            if line.strip():
                parts = line.split()
                seq_num, ticket_number = parts[:2] # this is all we need
                detail = summarydetail.SummaryDetail(
                         [seq_num, ticket_number, "00:00", "", ""])
                
                tickets.append(detail)
            else:
                break # done!

        return tickets
