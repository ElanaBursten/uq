# FCT1.py

import businesslogic
import FTL1
import c1391

class BusinessLogic(FTL1.BusinessLogic):
    # same as FTL1, except for default locator
    call_center = 'FCT1'
    DEFAULT_LOCATOR = '4061'

class XMLHTTPResponderData(c1391.XMLHTTPResponderData):
    pass

registry = {
    ('FCT1', 'xmlhttp', ''): XMLHTTPResponderData,
}

