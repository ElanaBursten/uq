# LCC1.py

import businesslogic
import tools
import LOR1

class BusinessLogic(LOR1.BusinessLogic):
    call_center = 'LCC1'

    def legal_due_date(self, row):
        return row['work_date']

    def allow_summary_updates(self):
        """ Return true if summaries can be updated. """
        return 1

registry = {}

