# NCA2.py

import businesslogic
import NCA1
import parser_tools

import update_call_centers as ucc

class BusinessLogic(NCA1.BusinessLogic):
    call_center = 'NCA2'

class UpdateData(ucc.UpdateData):
    def __init__(self):
        self.old_emergency = None

    def update_field(self, field_name, old_ticket, new_ticket):
        if self.old_emergency is None:
            # Save original value, in case it changes between field updates
            self.old_emergency = parser_tools.nca_isemergency(old_ticket)

        if field_name == 'ticket_type':
            if self.old_emergency and not parser_tools.nca_isemergency(new_ticket):
                # Once a ticket is an emergency, it stays an emergency
                return False
            return True

        return False

registry = {}

