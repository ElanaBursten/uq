# FMB1.py

import ftpresponder_data as frd
import call_centers
import date
import tools
import OCC1
import FDE1
import korterra_ftpresponder_data

class BusinessLogic(OCC1.BusinessLogic):

    call_center = "FMB1"
    DEFAULT_LOCATOR = "1007"

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.route_map_page,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
            self.route_munic,
        ]

    def route_munic(self, row):
        locator = area_id = None
        locator_info = self.tdb.find_munic_area_locator_2(
                       row.get("work_state", ""), row.get("work_county", ""),
                       row.get("work_city", ""), self.call_center)
        if locator_info:
            locator = locator_info[0].get("emp_id", None)
            area_id = locator_info[0].get("area_id", None)
        return tools.RoutingResult(locator=locator, area_id=area_id)

    def legal_due_date(self, ticketrow):
        call_date = ticketrow["call_date"]
        emergency = self.isemergency(ticketrow)
        return self._legal_due_date(date.Date(call_date),
               emergency=emergency).isodate()

    def _legal_due_date(self, call_date, emergency=0):
        # emergency tickets: +2 hours
        # others: +48 hours to midnight
        if emergency:
            return self.dcatalog.inc_emergency(call_date, hours=2)
        else:
            call_date = date.Date(self.base_date(call_date.isodate()))
            dd = self.dcatalog.inc_n_days(call_date, 2)
            return self.dcatalog.set_midnight(dd)

    def is_no_show(self, ticket):
        return OCC1.BusinessLogic.is_no_show(ticket) \
            or ticket["ticket_type"] == "NO RESPONSE"

    def allow_summary_updates(self):
        """ Return true if summaries can be updated. """
        return True

    def do_not_respond_before_date(self, ticket):
        ds = max(ticket.transmit_date, ticket.call_date)
        d = date.Date(ds)
        d.inc()
        d.hours, d.minutes, d.seconds = 8, 0, 0
        return d.isodate()


class FTPResponderData(FDE1.FTPResponderData):
    pass

class ExelonKorTerraFTPResponderData(
  korterra_ftpresponder_data.ExelonKorTerraFTPResponderData):
    pass

registry = {
    ('FMB1', 'ftp', ''): FTPResponderData,
    ('FMB1', 'ftp', 'exelon'): ExelonKorTerraFTPResponderData
}

