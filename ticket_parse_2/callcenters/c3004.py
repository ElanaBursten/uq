# c3004.py

import c3003
import _GA811_responder as gr

class BusinessLogic(c3003.BusinessLogic):
    call_center = '3004'

class SOAPResponderData(gr.GA811SOAPResponderData):
    pass

registry = {
    ('3004', 'soap', ''): SOAPResponderData,
}

