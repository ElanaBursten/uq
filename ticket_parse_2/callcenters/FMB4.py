# FMB4.py

import update_call_centers as ucc
import FMB1

class BusinessLogic(FMB1.BusinessLogic):
    call_center = 'FMB4'

class UpdateData(ucc.UpdateData):
    def __init__(self):
        self.old_emergency = None

    def update_field(self, field_name, old_ticket, new_ticket):
        if self.old_emergency is None:
            # Save original value, in case it changes between field updates
            self.old_emergency = BusinessLogic.isemergency(old_ticket)

        if field_name == 'ticket_type':
            if self.old_emergency and not BusinessLogic.isemergency(new_ticket):
                # Once a ticket is an emergency, it stays an emergency
                return False
            return True

        elif field_name == 'kind':
            if old_ticket.kind == 'NORMAL' and new_ticket.kind == 'EMERGENCY': 
                return True
            return False

        elif field_name == 'alert':
            if old_ticket.alert: 
                return False
            return True

        return False

registry = {}


