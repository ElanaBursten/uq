# FCT4.py

import businesslogic
import date
import FTL1

class BusinessLogic(FTL1.BusinessLogic):
    call_center = 'FCT4'
    DEFAULT_LOCATOR = '4061'

    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, 2)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days(cd, 3) # 72 hours
        return cd.isodate()

registry = {}

