# LCA1.py

import businesslogic
import LOR1

class BusinessLogic(LOR1.BusinessLogic):
    # emergency rules and due date are the same as LOR1!
    call_center = 'LCA1'
    DEFAULT_LOCATOR = '672'

    def legal_due_date(self, row):
        return row['work_date']

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic, # has higher priority now than township
        ]

registry = {}

