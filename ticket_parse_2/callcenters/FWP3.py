# FWP3.py

import businesslogic
import date

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "296"
    call_center = "FWP3"

    def isemergency(self, ticket):
        return businesslogic.BusinessLogic.isemergency(ticket) \
               or ticket["ticket_type"].find("SHORT NOTICE") > -1

    def legal_due_date(self, ticketrow):
        d = date.Date(ticketrow["call_date"])
        if self.isemergency(ticketrow):
            d = self.dcatalog.inc_emergency(d, 3)   # +3 hours
        else:
            d = date.Date(self.base_date(d.isodate()))
            d = self.dcatalog.inc_n_days(d, 2)  # +2 business days

        return d.isodate()

registry = {}

