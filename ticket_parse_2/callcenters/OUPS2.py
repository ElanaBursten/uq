# OUPS2.py

import OUPS1
import update_call_centers as ucc

class BusinessLogic(OUPS1.BusinessLogic):
    call_center = 'OUPS2'

    update_due_dates = True
    # Use the normal field update method, using UpdataData
    update_due_dates_manually = False

class UpdateData(ucc.UpdateData):
    def __init__(self):
        self.old_emergency = None
        self.new_due_date_pair_is_later = None

    def update_field(self, field_name, old_ticket, new_ticket):
        if self.old_emergency is None:
            # Save original values, since they could change

            # We can't use ticket.kind, since it doesn't get updated. So use
            # OUPS2.BusinessLogic.isemergency instead. OUPS2 is ok to use, since
            # it's currently the same as OUPS1 for this. But that could change.
            self.old_emergency = BusinessLogic.isemergency(old_ticket)

            if not new_ticket.due_date:
                raise ValueError('due_date cannot be blank') 
            if not new_ticket.legal_due_date:
                raise ValueError('legal_due_date cannot be blank') 
            new_date_later = (new_ticket.due_date > old_ticket.due_date) or (
              new_ticket.legal_due_date > old_ticket.legal_due_date)
            new_date_earlier = (new_ticket.due_date < old_ticket.due_date) or (
              new_ticket.legal_due_date < old_ticket.legal_due_date)
            self.new_due_date_pair_is_later = \
              new_date_later and not new_date_earlier

        new_emergency = BusinessLogic.isemergency(new_ticket)

        if field_name in ['due_date', 'legal_due_date']:
            if new_emergency <> self.old_emergency:
                if new_emergency:
                    return True # Emergency updates non-emergency
            else:
                if self.new_due_date_pair_is_later:
                    if new_emergency:
                        return True # Later emergency updates emergency 

                    if new_ticket.respond_date and (new_ticket.due_date ==
                      new_ticket.legal_due_date ==
                      BusinessLogic.non_emergency_due_date(
                        new_ticket.respond_date)):
                        # Later non-emergency, derived from respond_date,
                        # updates non-emergency
                        return True
            return False

        elif field_name == 'ticket_type':
            if self.old_emergency and not new_emergency:
                # Once a ticket is an emergency, it stays an emergency
                return False
            return True

        elif field_name == 'alert':
            return True

        return False

registry = {}

