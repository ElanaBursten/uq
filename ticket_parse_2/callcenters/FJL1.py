# FJL1.py

import businesslogic
import date
import FMS1
import FHL3
import c1391

class BusinessLogic(FMS1.BusinessLogic):
    DEFAULT_LOCATOR = "2598"
    call_center = "FJL1"

class FTPResponderData(FHL3.FTPResponderData):
    TEMPLATE2 = """\
    <Locate type="CUSTOM2" membercode="%(company)s" completionid="%(locate_id)s;%(resp_id)s">
        <customerid>ATMOSMS</customerid>
        <occid>MSOC</occid>
        <Jobid>%(ticket_number)s</Jobid>
        <Membercode>%(company)s</Membercode>
        <Operatorid>UTILIQUEST</Operatorid>
        <Completiondtdate>%(completion_date)s</Completiondtdate>
        <Completiondttime>%(completion_time)s</Completiondttime>
        <faccode1>GAS</faccode1>
        <ismarked1>%(ismarked)s</ismarked1>
        <isclearexcavate1>%(isclearexcavate)s</isclearexcavate1>
        %(actcode)s
        <Remarks>%(remarks)s</Remarks>
    </Locate>
    """

    # based on the status, we set <ismarked>, <isexcavated> and possibly
    # <actcode>.
    MARK_SETTINGS = {
        'CLEAR': (0, 1, '<actcode1>NO GAS LINES</actcode1>'),
        'LOCATED': (1, 0, '<actcode1>MARKED</actcode1>'),
    }

    def extra_names(self, namespace):
        # Use the closed_date, if available (should be)
        self.row = self.tdb.getrecords("locate",
                   locate_id=namespace['locate_id'])[0]
        now = self.row['closed_date']
        if now is None:
            # No closed date, set to now
            now = date.Date().isodate()

        extra = {}
        extra['remarks'] = self.get_locate_notes()
        extra['actcode'] = ''
        extra['completion_date'] = now[:10]
        extra['completion_time'] = now[11:] + ".000"
        status = namespace['status']
        extra['status'] = status

        ms = self.MARK_SETTINGS.get(status, (0, 0, ''))
        extra['ismarked'] = ms[0]
        extra['isclearexcavate'] = ms[1]
        extra['actcode'] = ms[2]

        return extra

class XMLHTTPResponderData(c1391.XMLHTTPResponderData):

    # the following namespace does matter:
    XMLNS = "http://tnresponse.korterraweb.com/"

    # NOTE: config should connect to msresponse.korterraweb.com; other
    # references, like the resp_url, should use tnresponse!

registry = {
    ('FJL1', 'ftp', ''): FTPResponderData,
    ('FJL1', 'xmlhttp', ''): XMLHTTPResponderData,
}

