# LQW1.py

import businesslogic
import date

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = 'LQW1'
    DEFAULT_LOCATOR = '274' # LocQM database

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.township,  # special!
            self.lcatalog.munic,
        ]

    def township(self, row):
        return self.lcatalog.township(row, (3, 3, 2))

    # for now, I'm assuming that it's the same as LWA1
    def legal_due_date(self, row):
        return row['work_date']
        '''
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            dd = self.dcatalog.inc_emergency(cd, 1)
        elif cd.hours >= 17 and cd.hours < 24:
            # 24 hours until 5 PM
            dd = self.dcatalog.inc_n_days_to_time(cd, 1, 17)
        else:
            # 48 hrs to 5 PM
            dd = self.dcatalog.inc_n_days_to_time(cd, 2, 17)
        return dd.isodate()
        '''

registry = {}

