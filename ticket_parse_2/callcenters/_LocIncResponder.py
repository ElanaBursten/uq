# _LocIncResponder.py

import string
#
import xmlhttpresponder_data as xhrd
import date
from xml.sax.saxutils import escape

class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):

    acceptable_codes = ["250 ok"]
    codes_ok = ['successful', '250 ok', 'response posted']
    codes_reprocess = []
    codes_error = []

    ONE_CALL_CENTER = 'UFPO' # override in subclasses
    # todo(dan) ONE_CALL_CENTERS may just be a temporary fix for item #3233
    ONE_CALL_CENTERS = {
     'QLNOR01': 'OUNC',
     'QLNOR02': 'OUNC',
     'QLNOR03': 'OUNC',
     'QLNOR04': 'OUNC',
     'QLNOR05': 'OUNC',
     'QLNOR06': 'OUNC',
     'QLNOR07': 'OUNC',
     'QLNOR08': 'OUNC',
     'QLNOR09': 'OUNC',
     'QLNOR10': 'OUNC',
     'QLNOR11': 'OUNC',
     'QLNOR12': 'OUNC',
     'QLNOR13': 'OUNC',
     'QLNOR14': 'OUNC',
     'QLNOR15': 'OUNC',
     'QLNOR16': 'OUNC',
     'QLNOR17': 'OUNC',
     'QLNOR18': 'OUNC',
     'QLNOR19': 'OUNC',
     'QLNOR20': 'OUNC',
     'QLNOR21': 'OUNC',
     'QLNOR22': 'OUNC',
     'QLNOR23': 'OUNC',
     'QLNOR24': 'OUNC',
     'QLNOR25': 'OUNC',
     'QLNOR26': 'OUNC',
     'QLNOR27': 'OUNC',
     'QLNOR28': 'OUNC',
     'QLNOR29': 'OUNC',
     'QLNWA01': 'WOC',
     'QLNWA02': 'WOC',
     'QLNWA03': 'WOC',
     'QLNWA04': 'WOC',
     'QLNWA05': 'UULC',
     'QLNWA06': 'WOC',
     'QLNWA07': 'WOC',
     'QLNWA08': 'WOC',
     'QLNWA09': 'WOC',
     'QLNWA10': 'UULC',
     'QLNWA11': 'UULC',
     'QLNWA12': 'WOC',
     'QLNWA13': 'UULC',
     'QLNWA14': 'WOC',
     'QLNWA15': 'UULC',
     'QLNWA16': 'UULC',
     'QLNWA17': 'UULC',
     'QLNWA18': 'UULC',
     'QLNWA19': 'WOC',
     'QLNWA20': 'UULC',
     'QLNWA21': 'WOC',
     'QLNWA22': 'UULC',
     'QLNWA23': 'UULC',
     'QLNWA24': 'UULC',
     'QLNWA25': 'UULC',
     'QLNWA26': 'UULC',
     'QLNWA27': 'IEUCC',
     'QLNWA28': 'WOC',
     'QLNWA29': 'WOC',
     'QLNWA30': 'UULC',
     'QLNWA31': 'UULC',
     'QLNWA32': 'IEUCC',
     'QLNWA33': 'UULC',
     'QLNWA34': 'IEUCC',
     'CENTEL04': 'IEUCC',
     'EMBOR01': 'OUNC',
     'PTI01': 'OUNC',
     'PTI02': 'OUNC',
     'PTI03': 'OUNC',
     'PTI04': 'OUNC',
     'PTI05': 'OUNC',
     'PTI06': 'OUNC',
     'PTI07': 'OUNC',
     'PTI08': 'OUNC',
     'PTI09': 'OUNC',
     'PTI13': 'OUNC',
     'PTI14': 'OUNC',
     'PTI15': 'OUNC',
     'PTI16': 'OUNC',
     'PTI17': 'OUNC',
     'PTI18': 'OUNC',
     'PTI19': 'OUNC',
     'PTI20': 'OUNC',
     'PTI21': 'OUNC',
     'PTI22': 'OUNC',
     'PTI23': 'OUNC',
     'PTI24': 'OUNC',
     'PTI25': 'OUNC',
     'PTI26': 'OUNC',
     'PTI27': 'OUNC',
     'PTI28': 'OUNC',
     'PTI29': 'OUNC',
     'PTI30': 'OUNC',
     'PTI31': 'OUNC',
     'PTI33': 'OUNC',
     'PTI37': 'OUNC',
     'TUO02': 'OUNC',
     'TUO03': 'OUNC',
     'TUO04': 'OUNC',
     'TUO05': 'OUNC',
     'TUO06': 'OUNC',
     'CENTEL01': 'UULC',
     'CENTEL02': 'UULC',
     'CENTEL03': 'UULC',
     'CENTEL05': 'UULC',
     'CENTEL06': 'UULC',
     'CENTEL07': 'UULC',
     'CENTEL08': 'UULC',
     'CENTEL09': 'UULC',
     'CENTEL11': 'UULC',
     'CENTEL12': 'UULC',
     'CENTEL13': 'UULC',
     'CENTEL16': 'UULC',
     'CENTEL17': 'UULC',
     'CENTEL18': 'UULC',
     'CENTRY01': 'UULC',
     'CENTRY02': 'UULC',
     'EMBWA02': 'UULC',
     'PTI43': 'UULC',
     'XTI10': 'UULC',
     'CENTEL10': 'WOC',
     'EMBWA01': 'WOC',
     'PGE01': 'WOC',
     'PGE03': 'WOC',
     'PGE04': 'WOC',
     'PTI11': 'WOC',
     'PTI10': 'WOC',
     'PTI12': 'WOC',
     'PTI32': 'WOC',
     'PTI34': 'WOC',
     'PTI35': 'WOC',
     'PTI36': 'WOC',
     'PTI38': 'WOC',
     'PTI39': 'WOC',
     'PTI40': 'WOC',
     'PGEF01': 'WOC',
     'PGEF03': 'WOC',
     'PGEF04': 'WOC',
     'XTI11': 'WOC',
     'CC7711': 'WOC',
     'CC7712': 'WOC',
     'CC7722': 'WOC',
     'CC7730': 'WOC',
     'CC7760': 'WOC'
    }

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="UTF-8" ?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema">
 <soap:Body>
  <AddContractLocatorResponses xmlns="http://tempuri.org/">
  <xmlContractLocatorResponses>
   <ContractLocatorResponseDataSet xmlns="http://tempuri.org/ContractLocatorResponseDataSet.xsd">
    <RESPONSE_INFO>
        <One_Call_Center>%(occ)s</One_Call_Center>
        <CDC>%(membercode)s</CDC>
        <Authentication_Code>%(id)s</Authentication_Code>
        <Ticket_Number>%(ticket)s</Ticket_Number>
        <Version_Number>%(revision)s</Version_Number>
        <Response_Code>%(responsecode)s</Response_Code>
        <Locate_DateTime>%(locdatetime)s</Locate_DateTime>
        <Note />
    </RESPONSE_INFO>
   </ContractLocatorResponseDataSet>
  </xmlContractLocatorResponses>
  </AddContractLocatorResponses>
 </soap:Body>
</soap:Envelope>
"""

    def get_xml(self, names):
        closed_date = self.row['closed_date']
        if closed_date is None:
            # No closed date, set to now
            closed_date = date.Date().isodate()
            self.log.log_event("Location date/time injected into xml based on current date/time")
        else:
            self.log.log_event("Location date/time injected into xml based on closed_date date/time")

        if names['membercode'].upper() in self.ONE_CALL_CENTERS:
            one_call = self.ONE_CALL_CENTERS[names['membercode'].upper()]
        else:
            one_call = self.ONE_CALL_CENTER

        names.update({
            'id': escape(self.id),
            'locdatetime': closed_date.replace("-", "/"),
            'occ': escape(one_call),
        })
        self.log.log_event("Current date/time injected into xml: %s"%(closed_date))
        revision = names['revision']
        names['revision'] = string.join([c for c in revision if c in string.digits], '')

        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

