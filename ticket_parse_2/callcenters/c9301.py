# c9301.py

import FCV3
import businesslogic
import date

class BusinessLogic(FCV3.BusinessLogic):
    DEFAULT_LOCATOR = '7432'
    call_center = '9301'

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic_with_dummy_city,
        ]

    def legal_due_date(self, ticketrow):
        transmit_date = ticketrow["transmit_date"]
        emergency = self.isemergency(ticketrow)
        return self._legal_due_date(transmit_date, emergency).isodate()

    def _legal_due_date(self, transmit_date, emergency=False):
        d = date.Date(transmit_date)
        if emergency:
            d = self.dcatalog.inc_n_hours(d, 3)
        else:
            # set to 7:00 of the next morning
            d.inc()
            d.hours, d.minutes, d.seconds = 7, 0, 0
            # make sure this doesn't fall in a weekend
            while not self.isbusinessday(d):
                d.inc()
            # fast-forward 48 hours
            d = self.dcatalog.inc_n_days(d, 2)
        return d

registry = {}

