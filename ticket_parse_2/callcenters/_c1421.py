# _c1421.py

from xml.sax.saxutils import escape
from re_shortcut import R
import emailresponder_data as erd
import string
import SCA1

class EmailResponderData(erd.EmailResponderData):

    SEPARATOR = ','
    LINE_FIELDS = [
        'ticket_number',
        'client_code',
        'disp_code',
        'due_date',
        'closed_date',
    ]

    def response_is_success(self, row, resp):
        # we don't get an acknowledgement, so we consider this to be always true
        return True

    def make_email_line(self, names, tdb, version):
        self.log.log_event("Closed date/time sent: %s"%(names.get('closed_date', 'N/A')))
        parts = [names.get(fieldname, "N/A") for fieldname in self.LINE_FIELDS]
        # add extra newline to separate blocks of data
        return string.join(parts, self.SEPARATOR) + "\n"

class XMLHTTPResponderData(SCA1.XMLHTTPResponderData):
    ONE_CALL_CENTER = 'PUPS'
    XML_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8" ?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <AddContractLocatorResponses xmlns="http://tempuri.org/">
      <xmlContractLocatorResponses>
        <ContractLocatorResponseDataSet xmlns="http://tempuri.org/ContractLocatorResponseDataSet.xsd">
        <RESPONSE_INFO>
        <One_Call_Center>%(occ)s</One_Call_Center>
        <CDC>%(membercode)s</CDC>
        <Authentication_Code>%(id)s</Authentication_Code>
        <Ticket_Number>%(ticket)s</Ticket_Number>
        <Version_Number>%(revision)s</Version_Number>
        %(facility)s
        <Response_Code>%(responsecode)s</Response_Code>
        <Locate_DateTime>%(locdatetime)s</Locate_DateTime>
        <Locator_Name>%(locatorname)s</Locator_Name>
        <Note>%(notes)s</Note>
        </RESPONSE_INFO>
        </ContractLocatorResponseDataSet>
      </xmlContractLocatorResponses>
    </AddContractLocatorResponses>
  </soap:Body>
</soap:Envelope>"""

    def get_xml(self, names):
        rec = self.tdb.getrecords('client', oc_code=self.row['client_code'])[0]
        fac_type = (rec['utility_type'] or "").lower()
        facility = ''
        # types used in client table: ugas, pipl, elec, phon, catv, watr
        if fac_type:
            if 'elec' in fac_type:
                facility = '<Facility_Type>electric primary</Facility_Type>'
            elif 'gas' in fac_type:
                facility = '<Facility_Type>gas distribution</Facility_Type>'
            elif 'phon' in fac_type:
                facility = '<Facility_Type>fiber optics</Facility_Type>'

        notes = self.get_ticket_notes()
        date_time = self.row.get('first_arrival_date',None)
        if date_time is None:
            date_time = self.row['closed_date']
            self.log.log_event("closed date injected into xml: %s"%(date_time))
        else:
            self.log.log_event("first_arrival_date injected into xml: %s"%(date_time))

        # Mantis 2821: add locator name to XML
        locator_name = self._get_employee_info(self.row['locate_id'])

        names.update({
            'id': escape(self.id),
            'locdatetime': date_time.replace("-", "/"),
            'occ': escape(self.ONE_CALL_CENTER),
            'notes': self.check_notes(notes),
            'facility': facility,
            'locatorname': escape(locator_name),
        })
        revision = names['revision']
        names['revision'] = escape(string.join(
         [c for c in revision if c in string.digits], ''))

        return self.XML_TEMPLATE % names

    DEFAULT_EMP_INFO = 'UTI'

    def _get_employee_info(self, locate_id):
        try:
            loc = self.tdb.getrecords('locate', locate_id=locate_id)[0]
            emp_id = loc['closed_by_id'] or loc['assigned_to']
            if emp_id:
                emp = self.tdb.getrecords('employee', emp_id=emp_id)[0]
                if emp['first_name']:
                    return '%s %s' % (emp['first_name'], emp['last_name'])
                else:
                    return emp['short_name']
            else:
                return self.DEFAULT_EMP_INFO
        except:
            self.log.log_event("Could not retrieve employee info")
            return self.DEFAULT_EMP_INFO

