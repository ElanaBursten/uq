# c9002.py

import c9001
import xmlhttpresponder_data as xhrd
import string
import date
from xml.sax.saxutils import escape

class BusinessLogic(c9001.BusinessLogic):
    call_center = '9003'

class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):

    acceptable_codes = ["250 ok"]
    codes_ok = ['successful', '250 ok', 'response posted']
    codes_reprocess = []
    codes_error = []

    ONE_CALL_CENTER = 'UFPO'

    XML_TEMPLATE = """\
<ContractLocatorResponseDataSet xmlns="http://tempuri.org/ContractLocatorResponseDataSet.xsd">
    <RESPONSE_INFO>
        <One_Call_Center>%(occ)s</One_Call_Center>
        <CDC>%(membercode)s</CDC>
        <Authentication_Code>%(id)s</Authentication_Code>
        <Ticket_Number>%(ticket)s</Ticket_Number>
        <Version_Number>%(revision)s</Version_Number>
        <Response_Code>%(responsecode)s</Response_Code>
        <Locate_DateTime>%(locdatetime)s</Locate_DateTime>
        <Locator_Name>UTI</Locator_Name>
        <Note>Response by Utiliquest</Note>
    </RESPONSE_INFO>
</ContractLocatorResponseDataSet>"""

    def get_xml(self, names):
        closed_date = self.row['closed_date']
        if closed_date is None:
            # No closed date, set to now
            closed_date = date.Date().isodate()
            self.log.log_event("Location date/time injected into xml based on current date/time")
        else:
            self.log.log_event("Location date/time injected into xml based on closed_date date/time")
        names.update({
            'id': escape(self.id),
            'locdatetime': closed_date.replace("-", "/"),
            'occ': escape(self.ONE_CALL_CENTER),
        })
        self.log.log_event("Current date/time injected into xml: %s"%(closed_date))
        revision = names['revision']
        names['revision'] = string.join([c for c in revision if c in string.digits], '')

        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        return True

registry = {
    ('9003', 'xmlhttp', ''): XMLHTTPResponderData,
}

