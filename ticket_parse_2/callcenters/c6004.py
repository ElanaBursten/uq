# c6004.py

import c6001
import xmlhttpresponder_data as xhrd
import SCA1

class BusinessLogic(c6001.BusinessLogic):
    call_center = '6004'

class XMLHTTPResponderData(SCA1.XMLHTTPResponderData):
    ONE_CALL_CENTER = 'TESS'

registry = {
    ('6004', 'xmlhttp', ''): XMLHTTPResponderData,
}

