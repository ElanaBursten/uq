# FDX3.py

import businesslogic
import FDX1

class BusinessLogic(FDX1.BusinessLogic):
    call_center = "FDX3"

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

registry = {}

