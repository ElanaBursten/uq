# FWP2.py

import businesslogic
import date
import string

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "296"
    call_center = "FWP2"

    def legal_due_date(self, ticketrow):
        call_date = ticketrow["call_date"]
        return self._due_date(call_date).isodate()

    def _due_date(self, call_date):
        d = date.Date(call_date)
        d = date.Date(self.base_date(d.isodate()))
        # add 48 hours
        d = self.dcatalog.inc_n_days(d, 2)
        d = self.dcatalog.set_midnight(d)
        return d

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.munic,
        ]

    def isrenotification(self, ticket):
        BANNER_FWP2 = "********** == R E N O T I F I C A T I O N == **********"
        return (
         ticket.ticket_type == "RENOTIFICATION"
         or string.find(ticket.ticket_type, "RXMIT") > -1
         or string.find(ticket.image, BANNER_FWP2) > -1)

registry = {}

