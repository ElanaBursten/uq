# OCC1.py

import businesslogic
import call_centers
import date
import tools

class BusinessLogic(businesslogic.BusinessLogic):

    call_center = "OCC1"
    DEFAULT_LOCATOR = "698"

    def legal_due_date(self, ticketrow):
        call_date = ticketrow["call_date"]
        emergency = self.isemergency(ticketrow)
        return self._legal_due_date(call_date, emergency).isodate()

    def _legal_due_date(self, call_date, emergency=0):
        d = date.Date(call_date)
        if emergency:
            d2 = self.dcatalog.inc_emergency(d) # +2 hours
        else:
            d = date.Date(self.base_date(d.isodate()))
            d2 = self.dcatalog.inc_48_hours_7am(d)
        return d2

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.route_map_page,
            self.lcatalog.munic,
        ]

    def route_map_page(self, row):
        locator = None
        map_page = row.get("map_page", "")
        if map_page:
            try:
                code, page, coords = map_page.split()
            except ValueError:
                return tools.RoutingResult()
                # invalid map_page, locator cannot be determined
            code = self.call_center + "-" + code
            routingresult = self.lcatalog.map_area_with_code(row, code)
            return routingresult

        return tools.RoutingResult()

    def is_ticket_ack(self, ticketrow):
        # emergency or cancellation tickets
        return (businesslogic.BusinessLogic.is_ticket_ack(self, ticketrow)
         #or ticketrow["ticket_type"].find("CANCELLATION") > -1
         #or ticketrow["ticket_type"].find("3 HOUR") > -1
         #or ticketrow['ticket_type'].find('3HRS') > -1
         or (self.call_center == "OCC1"
             and ticketrow["explosives"].startswith("Y")))

registry = {}

