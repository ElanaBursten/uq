# LWA1.py

import businesslogic
import date
import _LocIncResponder as lir

class BusinessLogic(businesslogic.BusinessLogic):
    call_center = 'LWA1'
    DEFAULT_LOCATOR = '272' # LocQM database

    # nostly the same as for LOR1
    def legal_due_date(self, row):
        # Mantis #2888, #2967.
        wd = date.Date(row['work_date'])
        cd = date.Date(row['call_date'])

        # TODO: 2 FULL BUSINESS (Mantis #3233)
        if 'EMERGENCY' in row['ticket_type'] \
        and 'NON-EMERGENCY' not in row['ticket_type']:
            # call date + 2h
            dd = self.dcatalog.inc_n_hours(cd.copy(), 2)
        elif 'PRE-SURVEY' in row['ticket_type']:
            # call date + 10 business days, to 23:59
            dd = self.dcatalog.inc_n_days(cd.copy(), 2)
            dd.set_time(23, 59, 59)
        elif 'MEET TIME' in row['ticket_type']:
            # work date + 2 business days
            dd = self.dcatalog.inc_n_days(wd.copy(), 2)
            dd.set_time(17, 0, 0)
        else:
            # call date + 2d + 11:59 PM
            dd = self.dcatalog.inc_n_days(cd.copy(), 2)
            dd.set_time(23, 59, 59)

        return dd.isodate()

    def township(self, row):
        return self.lcatalog.township(row, (3, 3, 2))

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.polygon,
            self.township,  # special!
            self.lcatalog.munic,
        ]

class XMLHTTPResponderData(lir.XMLHTTPResponderData):
    ONE_CALL_CENTER = 'UULC'

registry = {
    ('LWA1', 'xmlhttp', ''): XMLHTTPResponderData,
}


