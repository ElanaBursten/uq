# FNV4.py

import update_call_centers as ucc
import FNV1

class BusinessLogic(FNV1.BusinessLogic):
    call_center = 'FNV4'

class UpdateData(ucc.UpdateData):
    fields = ["ticket_type"]

registry = {}
