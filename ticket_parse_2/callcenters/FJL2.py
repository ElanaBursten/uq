# FJL2.py

import businesslogic
import date
import FJL1

class BusinessLogic(FJL1.BusinessLogic):
    call_center = "FJL2"

    def _use_96_hour_rule(self, ticketrow):
        counties = [
         "ST. TAMMANY", "ORLEANS", "ST. BERNARD", "JEFFERSON",
         "PLAQUEMINES", "LAFOURCHE", "TERREBONNE", "ST. MARY",
         "IBERIA", "VERMILION", "CAMERON", "CALCASIEU"
        ]
        county = ticketrow.get('work_county', '')
        ticket_type = ticketrow.get('ticket_type', '')

        return (county in counties
           and ticket_type.find("96 HOURS NOTICE") >= 0)

    # new due date rule, starting 2006-06-28
    def legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, 2)
        #elif self._use_96_hour_rule(row):
        #    cd = self.dcatalog.inc_n_days(cd, 4)
        else:
            cd = date.Date(self.base_date(cd.isodate()))
            cd = self.dcatalog.inc_n_days(cd, 2)
        return cd.isodate()

registry = {}

