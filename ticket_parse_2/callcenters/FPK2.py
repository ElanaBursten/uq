# FPK2.py

import FCL1

class BusinessLogic(FCL1.BusinessLogic):
    call_center = "FPK2"
    DEFAULT_LOCATOR = "2658"

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
        ]

registry = {}

