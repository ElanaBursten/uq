# FEF1.py

import xmlhttpresponder_data as xhrd
import date
import FOL1
import mapgrid
import tools
from xml.sax.saxutils import escape

class BusinessLogic(FOL1.BusinessLogic):

    DEFAULT_LOCATOR = "693"
    call_center = "FEF1"

    def do_no_client_check(self, client_code=""):
        return 1

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.route_lat_long,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
        ]

    def route_lat_long(self, row):
        locator = area_id = None
        lat = float(row.get("work_lat", 0))
        long = float(row.get("work_long", 0))
        if lat and long:
            try:
                celly, cellx = mapgrid.coords2map_JAX(lat, long)    # sic!
            except mapgrid.MapGridError:
                pass    # may raise out-of-bounds exception; ignore
            else:
                foo = self.tdb.find_map_area_locator("JACK", 1, cellx, celly)
                if foo:
                    locator = foo[0].get("emp_id", None)
                    area_id = foo[0].get("area_id", None)

        return tools.RoutingResult(locator=locator, area_id=area_id)

    # is_ticket_ack is derived from FOL1

    def allow_summary_updates(self):
        return 1


class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):

    acceptable_codes = ["250 ok", "ok"]
    codes_ok = ['successful', '250 ok', 'response posted', 'ok']

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="UTF-8" ?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema">
 <soap:Body>
   <TicketResponseStatus xmlns="http://translore.com/ResponseService/">
     <AuthID xmlns="">%(id)s</AuthID>
     <CallCenter xsi:nil="true" xmlns="">KOCC</CallCenter>
     <TicketNumber xmlns="">%(ticket)s</TicketNumber>
     <TicketRevision xmlns="">0</TicketRevision>
     <UtilityCode xmlns="">%(membercode)s</UtilityCode>
     <Response xmlns="">%(responsecode)s</Response>
     <Who xmlns="">UTI</Who>
     <Notes xmlns="">Response by Utiliquest</Notes>
     <WorkTime xmlns="">%(locdatetime)s</WorkTime>
   </TicketResponseStatus>
 </soap:Body>
</soap:Envelope>
"""

    def get_xml(self, names):
        closed_date = self.row['closed_date']
        if closed_date is None:
            # No closed date, set to now
            closed_date = date.Date().isodate()
            self.log.log_event("Location date/time injected into xml based on current date/time")
        else:
            self.log.log_event("Location date/time injected into xml based on closed_date date/time")
        names.update({
            'id': escape(self.id),
            'locdatetime': closed_date.replace(" ", "T")
        })
        self.log.log_event("Location date/time injected into xml: %s" %
         closed_date)
        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

registry = {
    ('FEF1', 'xmlhttp', ''): XMLHTTPResponderData,
}

