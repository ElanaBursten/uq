# SCA1.py

import re
import string
from xml.sax.saxutils import escape
#
import businesslogic
import date
import duedates
import korterra_ftpresponder_data
import xmlhttpresponder_data as xhrd
import NCA1

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "2087"
    call_center = "SCA1"

    def locator_methods(self, row):
        return [
            self.cc_routing,
            self.route_by_routinglist,
            self.lcatalog.map_grid_county_state,
            self.lcatalog.munic,
        ]

    legal_due_date = duedates.due_date_CA

    def allow_summary_updates(self):
        return True

    _re_re_mark = re.compile("^\s*Re-Mark:\s*(\S+)", re.MULTILINE)

    def screen(self, row, ticket_id, image):
        if 'UPDT' in row['ticket_type']:
            m = self._re_re_mark.search(image)
            return m and m.group(1) == 'N'
        return False


class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):

    # this call center doesn't actually check the responses
    acceptable_codes = ["250 ok"]
    codes_ok = ['successful', '250 ok', 'response posted']
    codes_reprocess = []
    codes_error = []

    ONE_CALL_CENTER = 'USAS'

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8" ?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <AddContractLocatorResponses xmlns="http://tempuri.org/">
      <xmlContractLocatorResponses>
        <ContractLocatorResponseDataSet xmlns="http://tempuri.org/ContractLocatorResponseDataSet.xsd">
        <RESPONSE_INFO>
        <One_Call_Center>%(occ)s</One_Call_Center>
        <CDC>%(membercode)s</CDC>
        <Authentication_Code>%(id)s</Authentication_Code>
        <Ticket_Number>%(ticket)s</Ticket_Number>
        <Version_Number>%(revision)s</Version_Number>
        <Response_Code>%(responsecode)s</Response_Code>
        <Locate_DateTime>%(locdatetime)s</Locate_DateTime>
        <Locator_Name>UTI</Locator_Name>
        <Note>%(notes)s</Note>
        </RESPONSE_INFO>
        </ContractLocatorResponseDataSet>
      </xmlContractLocatorResponses>
    </AddContractLocatorResponses>
  </soap:Body>
</soap:Envelope>"""

    def get_xml(self, names):
        date_time = self.row['closed_date']
        if date_time is None:
            # No closed date, set to now
            date_time = date.Date().isodate()
            self.log.log_event("Location date/time injected into xml based"\
                               " on current date/time")
        else:
            self.log.log_event("Location date/time injected into xml based"\
                               " on closed_date date/time")
        names.update({
            'id': escape(self.id),
            'locdatetime': date_time.replace("-", "/"),
            'occ': escape(self.ONE_CALL_CENTER),
            'notes': 'Response by Utiliquest'
        })
        self.log.log_event("Location date/time injected into xml: %s" %
         date_time)
        revision = names['revision']
        names['revision'] = escape(string.join([c for c in revision
                                                if c in string.digits], ''))

        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        return True


class EmailResponderData(NCA1.EmailResponderData):
    # same as NCA1
    update_call_center = ['SCA6', 'SCA8', 'SCA9']

class FTPResponderData(
  korterra_ftpresponder_data.CrownCastleKorTerraFTPResponderData2):
    OCCID = 'CSOC'
    def adapt_ticket_number(self, ticket_number):
        if ticket_number:
            return 'CS' + ticket_number + 'S'
        return ticket_number

registry = {
    ('SCA1', 'xmlhttp', ''): XMLHTTPResponderData,
    ('SCA1', 'email', ''): EmailResponderData,
    ('SCA1', 'ftp', ''): FTPResponderData
}

