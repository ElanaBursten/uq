# duedates.py
# Collect common due date calculations here.
#
# These can be injected into BusinessLogic classes like this:
#
# class BusinessLogic(...):
#     ...
#     legal_due_date = my_due_date_method

import date

def date_from_row(row, field_name):
    date_iso = row.get(field_name, None)
    if date_iso is None:
        return None
    return date.Date(date_iso)

def max_date(date1, date2):
    if date1 is None:
        return date2
    if date2 is None:
        return date1
    return max(date1, date2)

def due_date_TN(self, row):
    """ Due date rule used by TN call centers.
        Emergencies are call_date + 2 hours, except if the work_date is later,
        then that is used instead.
        Regular tickets are call_date + 3 business days.
    """
    cd = date.Date(row["call_date"])
    wd = row.get('work_date', None) and date.Date(row['work_date'])
    if self.isemergency(row):
        dd = self.dcatalog.inc_emergency(cd, hours=2)
        # use work_date if it's greater than the regular emergency d/t
        if (wd is not None) and (wd > dd):
            dd = wd
    else:
        cd = date.Date(self.base_date(cd.isodate()))
        dd = self.dcatalog.inc_n_days(cd, 3)
    return dd.isodate()

def due_date_CA(self, row, is_update=0):
    call_date = date_from_row(row, 'call_date')
    work_date = date_from_row(row, 'work_date')

    min_due_date = None
    if call_date:
        if self.isemergency(row):
            min_due_date = self.dcatalog.inc_n_hours(call_date, 2)
        else:
            # 2nd business day at 5pm
            min_due_date = self.dcatalog.inc_n_days(call_date, 2)
            min_due_date.set_time(17, 0, 0)

    due_date = max_date(work_date, min_due_date)
    if due_date:
        return due_date.isodate()
    return None

def due_date_CA_ATT(self, row, is_update=0):
    """ Due date calculation as used by NCA2/SCA6, Mantis 2828.
        It's possible for this method to return None, if the ticket is an
        update and if the work date < call date + 48h to 7PM. (Kind of a hairy
        rule...)
    """
    cd = date.Date(row['call_date'])

    # short notice: use work date (Mantis #3320) -- overrules emergency
    if 'SHORT NOTICE' in row['ticket_type'] \
    or 'SHRT' in row['ticket_type']:
        return date.Date(row['work_date']).isodate()

    # emergencies: +2 hours
    if self.isemergency(row):
        dd = self.dcatalog.inc_emergency(cd, hours=2)
        return dd.isodate()

    # otherwise, call date + 48 hours, but if work date is greater than call
    # date + 48 hours to 7PM (!), set to 48 hours to 7PM.
    cd = date.Date(self.base_date(cd.isodate()))
    wd = date.Date(row['work_date']) if row.get('work_date', None) else cd

    if 'MEET' in row['ticket_type']:
        if is_update: return None
        # work date should never be < call date
        if wd >= cd:
            return wd.isodate()

    dd = self.dcatalog.inc_n_days(cd, 2)
    ddplus = dd.clone(); ddplus.set_time(19, 0, 0) # +48h to 7PM
    if wd > ddplus:
        dd.set_time(19, 0, 0)
    elif is_update:
        # due date should not be updated
        return None

    return dd.isodate()

def correct_base_date(ddc, datestr):
    """ The due date is usually based on a call date or transmit date. As of
        Mantis #2710, we must make sure that that base date falls on a business
        day. If it's in the weekend or on a holiday, we'll use 7AM on the next
        business day as the base date.
    """
    d = date.Date(datestr)
    d = ddc.inc_to_business_day_7am(d)
    return d.isodate()

def correct_base_date_rev(ddc, datestr):
    """ Like correct_base_date, but MOVES BACK to the LAST business day. In
        practice, if a date falls in the weekend, this will return the
        previous Friday. Uncommon; only used by certain call centers. """
    d = date.Date(datestr)
    d = ddc.dec_to_business_day(d)
    return d.isodate()

