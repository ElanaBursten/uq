# FMW3.py

import date
import FMW1

class BusinessLogic(FMW1.BusinessLogic):
    call_center = 'FMW3'

    def allow_summary_updates(self):
        return 1

    # This due date computation used to be for FMW1 through FMW4. As of
    # 2010-10-01, FMW1/2/4 have a new due date rule, but FMW3 retains the
    # original one. (See Mantis #2661.)

    def legal_due_date(self, ticketrow):
        dd = ticketrow.get('due_date', None)
        if dd:
            return dd
        call_date = ticketrow["call_date"]
        emergency = self.isemergency(ticketrow)
        return self._legal_due_date(date.Date(call_date),
               emergency=emergency).isodate()

    def _legal_due_date(self, call_date, emergency=0):
        # emergency tickets: +3 hours (unaffected by QMAN-3400)
        # DC: +48 hours
        # late tickets: +24 hours to 5PM (new 2004.03.11)
        # others: +48 hours, then next 5PM
        if emergency:
            return self.dcatalog.inc_emergency(call_date, hours=3)
        call_date = date.Date(self.base_date(call_date.isodate()))
        if call_date.hours >= 17 and call_date.hours < 24:
            # 24 hours until 5 PM
            return self.dcatalog.inc_n_days_to_time(call_date, 1, 17)
        else:
            return self.dcatalog.inc_48_hours_5pm(call_date)

registry = {}

