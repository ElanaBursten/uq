
import update_call_centers as ucc
import SCA1
import ticket_adapter
import locate

class BusinessLogic(SCA1.BusinessLogic):
    call_center = 'SCA7'

class TicketAdapter(ticket_adapter.TicketAdapter):

    SDG_TERM_IDS = ['SDG0', 'NCU01', 'NEU01', 'BCU01', 'CMU01', 'EAU01']

    def adapt(self, ticket):
        """
        Mantis 2131:
        The SCA7 tickets will update SCA1 tickets. For each SDG0x term id
        received, we need to add a corresponding SDG0xG term id. For example,
        if we receive SDG04 on the ticket, the parser should add locates SDG04
        and SDG04G to the ticket for marking. The current SDG rule that add a
        term id only if SDG09 is on the ticket will not be used.

        See also 2275: Korterra-SDG ticket feed for CA
        """

        new_locates = []
        loc_names = [x.client_code for x in ticket.locates]
        for loc in ticket.locates:
            '''
            if (loc.client_code.startswith("SDG0") and (loc.client_code + "G") not in loc_names) or \
               (loc.client_code.startswith("NCU01") and (loc.client_code + "G") not in loc_names) or \
               (loc.client_code.startswith("NEU01") and (loc.client_code + "G") not in loc_names) or \
               (loc.client_code.startswith("BCU01") and (loc.client_code + "G") not in loc_names) or \
               (loc.client_code.startswith("CMU01") and (loc.client_code + "G") not in loc_names):
            '''
            for termid in self.SDG_TERM_IDS:
                if loc.client_code.startswith(termid) \
                and (loc.client_code + "G") not in loc_names:
                    new_locate = locate.Locate(loc.client_code + "G")
                    new_locate.added_by = "RULE"
                    new_locates.append(new_locate)
                    loc_names.append(new_locate.client_code)

        ticket.locates.extend(new_locates)

class UpdateData(ucc.UpdateData):
    pass

registry = {}

