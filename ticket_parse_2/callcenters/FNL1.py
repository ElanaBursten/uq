# FNL1.py

import businesslogic
import FBL1
import tools

class BusinessLogic(FBL1.BusinessLogic):

    DEFAULT_LOCATOR = "2396"
    call_center = "FNL1"

class FTPResponderData(FBL1.FTPResponderData):
    pass

registry = {
    ('FNL1', 'ftp', ''): FTPResponderData,
}

