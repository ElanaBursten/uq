# FHL3.py

import string
import time
import xml.etree.ElementTree as ET
from xml.sax.saxutils import escape
#
import FHL1
#
import businesslogic
import date
import et_tools
import ftpresponder_data as frd
import errorhandling2

class BusinessLogic(FHL1.BusinessLogic):
    call_center = 'FHL3'

class FTPResponderData(frd.FTPResponderData):

    TEMPLATE1 = """\
<?xml version="1.0" encoding="UTF-8"?>
<LocateCollection>
###BODY###
</LocateCollection>"""

    TEMPLATE2 = """\
    <Locate type="ELEC6" membercode="%(company)s" completionid="%(locate_id)s;%(resp_id)s">
        <Jobid>TS%(ticket_number)s</Jobid>
        <Membercode>%(company)s</Membercode>
        <Completiondtdate>%(completion_date)s</Completiondtdate>
        <Completiondttime>%(completion_time)s</Completiondttime>
        %(statustag)s
        <operatorid>UTILIQUEST</operatorid>
        <Remarks>%(remarks)s</Remarks>
    </Locate>
"""

    store_resp_context = False
    delete_ack = True
    is_xml = True

    def adapt_ticket_number(self, ticket_number):
        return ticket_number

    def extra_data(self, namespace):
        self.row = self.tdb.getrecords("locate", locate_id=namespace['locate_id'])[0]
        remarks = self.get_ticket_notes()
        return {'remarks': self.check_notes(remarks)}

    def extra_names(self, namespace):
        # Use the closed_date, if available (should be)
        self.row = self.tdb.getrecords("locate", locate_id=namespace['locate_id'])[0]
        now = self.row['closed_date']
        if now is None:
            # No closed date, set to now
            now = date.Date().isodate()
        status = namespace['status']
        return {
            'statustag': '<%s>1</%s>' % (status, status),
            'completion_date': now[:10],
            'completion_time': now[11:] + ".000",
        }

    def gen_filename(self):
        """ Generate a unique filename to be posted on the server. """
        t = time.time()
        t9 = time.localtime(t)
        year, month, day, hours, minutes, seconds = t9[:6]
        milliseconds = int((t - int(t)) * 1000)
        name = "%04d%02d%02d%02d%02d%02d%03d.xml" % (year, month, day, hours,
               minutes, seconds, milliseconds)
        return name

    def is_response_file(self, filename):
        return filename.endswith("_ack.xml")

    def get_acknowledgements(self, data, locates):
        dom = ET.fromstring(data)
        ok, errors = [], []
        for ackcoll in et_tools.findall(dom, 'ackcollection'):
            for ack in et_tools.findall(ackcoll, 'ack'):
                a = frd.FTPResponderAck()
                try:
                    for ackcode in et_tools.findall(ack, 'ackCode'):
                        a.result = ackcode.text.strip()
                        if str(a.result) == '19' or str(a.result) == '14':
                            raise ValueError('Result indicates a retry is needed')
                        break
                    for acktext in et_tools.findall(ack, 'ackText'):
                        a.raw = acktext.text.strip()
                        if str(a.raw).find('Operation Timed Out') != -1:
                            raise ValueError('Result indicates a retry is needed')
                        break
                    completion_id = ack.get('completionid')
                    a.locate_id, a.resp_id = completion_id.split(';')[:2]
                except:
                    ep = errorhandling2.errorpacket()
                    errors.append(ep)
                    continue
                else:
                    ok.append(a)
        # unlink so garbage collection can do it's job
        #dom.unlink()
        return ok, errors

    def is_success(self, result_code):
        return result_code == '0'

registry = {
    ('FHL3', 'ftp', ''): FTPResponderData,
}

