# c3003.py

import re
from xml.sax.saxutils import escape
#
import c300, FTL1
import date
import tools
import xmlhttpresponder_data as xhrd
import _GA811_responder as gr

re_meeting_respond_by = re.compile(
  "Meeting Respond By: +([0-9/]+) +Time: +([0-9:]+)")

class BusinessLogic(c300.BusinessLogic):
    call_center = '3003'

    def legal_due_date(self, ticketrow):
        emergency = ticketrow["kind"] == "EMERGENCY"
        if emergency:
            due_date = date.Date(date.Date(ticketrow["call_date"]))
            due_date = self.dcatalog.inc_n_hours(due_date, 2).isodate()

        ticket_type = ticketrow['ticket_type']
        if 'LPMEET' in ticket_type:
            try:
                m = re_meeting_respond_by.search(ticketrow.get('image'))
                if m:
                    return tools.isodate(m.group(1) + " " + m.group(2))
            except:
                import traceback; traceback.print_exc()
                pass # fall back to regular mechanism

        elif 'LPEXCA' in ticket_type:
            # "Legal Good Thru" with 23:59 as the time
            d = date.Date(ticketrow['legal_good_thru'])
            d.set_time(23, 59, 00)
            return d.isodate()

        # by default, fall back to the original due date mechanism
        return c300.BusinessLogic.legal_due_date(self, ticketrow)

class XMLHTTPResponderData(FTL1.XMLHTTPResponderData):
    pass

class TransloreXMLHTTPResponderData(xhrd.XMLHTTPResponderData):
    queue = {'table': 'responder_multi_queue', 'respond_to': 'client'}
    acceptable_codes = ["250 ok", "ok"]
    codes_ok = ['successful', '250 ok', 'response posted', 'ok']

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="UTF-8" ?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema">
 <soap:Body>
   <UpdateTicket xmlns="http://translore.com/ResponseService/">
     <AuthID xmlns="">%(id)s</AuthID>
     <CallCenter xmlns="">GAUPC</CallCenter>
     <TicketNumber xmlns="">%(ticket)s</TicketNumber>
     <TicketRevision xmlns="">0</TicketRevision>
     <MessageType xmlns="">Positive Response</MessageType>
     <MessagePayload xmlns="">
       <PayloadData>
         <Response>%(responsecode)s%(explanation)s</Response>
         <Notes>%(notes)s</Notes>
         <Status>%(responsecode)s</Status>
         <Who>%(locator_name)s</Who>
         <Utility>%(termid)s</Utility>
         <WorkTime>%(locdatetime)s</WorkTime>
       </PayloadData>
     </MessagePayload>
   </UpdateTicket>
 </soap:Body>
</soap:Envelope>
"""

    _fiber_refs = None # cache

    def _high_profile_info(self, locate_id):
        loc = self.tdb.getrecords('locate', locate_id=locate_id)[0]
        return bool(loc['high_profile']), int(loc['high_profile_reason'] or 0)

    def map_status(self, status):
        # load high profile reasons for Fiber and cache them
        if self._fiber_refs is None:
            self.__class__._fiber_refs = self.tdb.get_high_profile_reasons_fiber()
        # QMAN-3370: specific code mappings. if locate is high profile and the
        # high profile reason is Fiber, then use custom responses.
        if status in ['M', 'O']:
            # 1. check single reason HP
            ishp, hpreason = self._high_profile_info(self.row['locate_id'])
            # 2. check multi reason HP
            if ishp:
                hpmultirefs = []
                hpnote = self.tdb.find_hp_reason_note(self.row['ticket_id'],
                         self.row['client_code'])
                if hpnote is not None:
                    hpmultirefs = tools.get_hp_reasons(hpnote)
                multifiber = bool(set(self._fiber_refs[1]).intersection(hpmultirefs))
                if hpreason in self._fiber_refs[0] or multifiber: 
                    if self.row['status'] == 'M':
                        return 'Marked Fiber', ''
                    elif self.row['status'] == 'O':
                        return 'Ongoing Fiber', ''

        return None

    def get_xml(self, names):
        #print ">>names:", names
        #print ">>row:", self.row
        #print self._high_profile_info(self.row['locate_id'])
        notes = self.get_ticket_notes()
        closed_date = self.row['closed_date']
        if closed_date is None:
            # No closed date, set to now
            closed_date = date.Date().isodate()
        names.update({
            'id': escape(self.id),
            'locdatetime': closed_date.replace(" ", "T"),
            'notes': self.check_notes(notes),
            'termid': self.row['client_code'],
        })

        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

class SOAPResponderData(gr.GA811SOAPResponderData):
    sender = 'STS'

registry = {
    ('3003', 'xmlhttp', ''): XMLHTTPResponderData,
    ('3003', 'xmlhttp', 'translore'): TransloreXMLHTTPResponderData,
    ('3003', 'soap', ''): SOAPResponderData,
}

