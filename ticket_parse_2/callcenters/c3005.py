# c3005.py
# Mantis #2708: New call center for TN tickets that need to be sent to the GA
# office.

import businesslogic
import date
import c3003, c1391
import duedates

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = c3003.BusinessLogic.DEFAULT_LOCATOR
    call_center = "3005"

    def OLD_legal_due_date(self, row):
        cd = date.Date(row["call_date"])
        wd = date.Date(row['work_date'])
        if self.isemergency(row):
            cd = self.dcatalog.inc_emergency(cd, hours=2)
            # use work_date if it's greater than the regular emergency d/t
            if wd > cd:
                cd = wd
        else:
            cd = self.dcatalog.inc_n_days(cd, 3)
        return cd.isodate()

    legal_due_date = duedates.due_date_TN

    def locator_methods(self, row):
        return [
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            #self.lcatalog.map_grid_county_state_TN,
            self.lcatalog.munic,
        ]

    def allow_summary_updates(self):
        return 1

class XMLHTTPResponderData(c1391.XMLHTTPResponderData):
    pass

registry = {
    ('3005', 'xmlhttp', ''): XMLHTTPResponderData,
}

