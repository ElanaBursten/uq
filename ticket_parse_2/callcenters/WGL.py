# WGL.py

from __future__ import with_statement
import StringIO
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
#
import et_tools
from wo_businesslogic import WorkOrderBusinessLogic as WOBL
import ticket_db
import tools

class WorkOrderBusinessLogic(WOBL):

    DEFAULT_LOCATOR = "17291"

    def locator_methods(self, row):
        # everything goes to the default locator
        return []

registry = {}

