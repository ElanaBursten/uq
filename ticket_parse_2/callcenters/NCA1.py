# NCA1.py

from xml.sax.saxutils import escape

import businesslogic
import date
import duedates
import string
import emailresponder_data as erd
import korterra_ftpresponder_data
import parser_tools
import responder_ack
import re
import ticket_adapter
import xmlhttpresponder_data as xhrd
import locate

class BusinessLogic(businesslogic.BusinessLogic):

    DEFAULT_LOCATOR = "2086"
    call_center = "NCA1"

    def locator_methods(self, row):
        return [
            self.cc_routing,
            self.route_by_routinglist,
            self.lcatalog.map_qtrmin,
            self.lcatalog.munic,
        ]

    legal_due_date = duedates.due_date_CA

    def allow_summary_updates(self):
        return True

    def isemergency(self, ticket):
        return parser_tools.nca_isemergency(ticket)

    _re_ticket_type = [re.compile('EXTENSION'),
                       re.compile('FOLLOW UP'),
                       re.compile('RENEWAL'),
                       re.compile('EXTN'),
                       re.compile('FLUP'),
                       re.compile('RNEW')]
    _re_re_mark = re.compile(
      "Excavator Requests Operator\(s\) To Re-mark Their Facilities: *([YN])")

    def screen(self, ticket, ticket_id, image):
        """
        The screening process would only affect ticket types of
        EXTENSION, FOLLOW UP and RENEWAL. The tickets identify
        if we need to go to the site just before the comments section.
        "Excavator Requests Operator(s) To Re-mark Their Facilities:".
        If this is followed by a "Y" then it should be routed
        to the field, if it is followed by a "N", then all the
        locates should be screened.
        """
        for regex in BusinessLogic._re_ticket_type:
            m = regex.search(ticket['ticket_type'])
            if m:
                # Get the ticket image and see if we need to screen the locates
                m = self._re_re_mark.search(image)
                if m:
                    # Is it marked "N"?
                    return m.group(1) == 'N'
        return False

class TicketAdapter(ticket_adapter.TicketAdapter):
    # QMAN-3576

    def adapt(self, ticket):
        loc_names = [x.client_code for x in ticket.locates]
        if "CTYGSD" in loc_names:
            if "CTYGSD-SL" not in loc_names:
                ticket.locates.append(locate.Locate("CTYGSD-SL"))
            if "CTYGSD-TS" not in loc_names:
                ticket.locates.append(locate.Locate("CTYGSD-TS"))

class EmailResponderData(erd.EmailResponderData):

    # this is where the tickets are really from
    update_call_center = ['NCA2', 'NCA3']

    # for this responder, these fields all need to have the exact number of
    # characters, padded to the right with spaces if necessary.
    SEPARATOR = '|'
    LINE_FIELDS = [
        ('ticket_number', 17),
        ('disp_code', 11),
        ('cause_code', 3),
        ('remarks', 70), # min. 70 characters, but no upper limit; Mantis #2829
        ('date', 6),
        ('time', 5),
        ('tech_code', 3),
    ]

    def response_is_success(self, row, resp):
        # we don't get an acknowledgement, so we consider this to be always true
        return True

    def make_email_line(self, names, tdb, version):
        ticket_id = names['ticket_id']

        # get latest serial number from versions; don't rely on ticket_number
        curr_serial_number = tdb.get_latest_serial_number(ticket_id,
                             self.update_call_center)

        # get serial number of current version
        if version:
            serial_number = version.serial_number
            names['ticket_number'] = serial_number
        else:
            serial_number = curr_serial_number

        cd = date.Date(names['closed_date'])
        d = "%02d%02d%02d" % (cd.month, cd.day, (cd.year - 2000))
        if cd.hours < 12:
            ampm = 'A'
        else:
            ampm = 'P'
        t = "%02d%02d%s" % (cd.hours, cd.minutes, ampm)

        '''
        remarks = names['status_description']

        # put 'Reference ticket id' in remarks, but only for tickets with
        # serial numbers other than the current one:
        if serial_number and serial_number != curr_serial_number:
            remarks += ' / Reference ticket id ' + str(curr_serial_number)
        '''
        # Mantis #2829
        remarks = self.get_notes(tdb, ticket_id)

        names.update({
            'cause_code': '600',
            'remarks': remarks,
            'date': d,
            'time': t,
            'tech_code': self.tech_code, # set in config.xml
        })

        # create line
        parts = []
        for fieldname, length in self.LINE_FIELDS:
            part = self._sanitize_part(names[fieldname] or "")
            while len(part) < length:
                part += " "
            if len(part) > length and fieldname != 'remarks':
                part = part[:length]
            parts.append(part)

        return string.join(parts, self.SEPARATOR)

    def _sanitize_part(self, s):
        s = s.replace("|", "")
        s = s.replace("\n", "; ")
        s = s.replace("\r", "")
        return s

    def tweak_ticket_number(self, row):
        return row['serial_number'] or ""

    def filter_versions(self, versions):
        return [v for v in versions if v.source in self.update_call_center]

    #
    # responder acknowledgements

    def handle_acknowledgements(self, tdb, log):
        """ Handle acknowledgement files. """
        ra = responder_ack.ResponderAck(tdb, log, self.ack_dir, self.ack_proc_dir, self.name,
                                        self.config, self.accounts)
        ra.handle_acknowledgements()

    #
    # notes (Mantis #2829)

    def get_notes(self, tdb, ticket_id):
        sql = """
          select * from ticket_notes where ticket_id = %d
        """ % int(ticket_id)
        rows = tdb.runsql_result(sql)
        notes = [row['note'] or '' for row in rows]
        notes = string.join([note for note in notes if note], "; ")
        notes = notes.replace("\n", " ").replace("|", "")
        return notes

class XMLHTTPResponderData(xhrd.XMLHTTPResponderData):
    codes_ok = ['successful', '250 ok', 'response posted']
    codes_reprocess = []
    codes_error = []

    ONE_CALL_CENTER = 'USAN'

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8" ?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <AddContractLocatorResponses xmlns="http://tempuri.org/">
      <xmlContractLocatorResponses>
        <ContractLocatorResponseDataSet xmlns="http://tempuri.org/ContractLocatorResponseDataSet.xsd">
        <RESPONSE_INFO>
        <One_Call_Center>%(occ)s</One_Call_Center>
        <CDC>%(membercode)s</CDC>
        <Authentication_Code>%(id)s</Authentication_Code>
        <Ticket_Number>%(ticket)s</Ticket_Number>
        <Version_Number>%(revision)s</Version_Number>
        <Response_Code>%(responsecode)s</Response_Code>
        <Locate_DateTime>%(locdatetime)s</Locate_DateTime>
        <Locator_Name>UTI</Locator_Name>
        <Note>%(notes)s</Note>
        </RESPONSE_INFO>
        </ContractLocatorResponseDataSet>
      </xmlContractLocatorResponses>
    </AddContractLocatorResponses>
  </soap:Body>
</soap:Envelope>"""

    def get_xml(self, names):
        closed_date = self.row['closed_date']
        if closed_date is None:
            # No closed date, set to now
            closed_date = date.Date().isodate()
            self.log.log_event("Locate date/time injected into xml based"\
                               " on current date/time")
        else:
            self.log.log_event("Locate date/time injected into xml based"\
                               " on closed_date date/time")
        names.update({
            'id': escape(self.id),
            'locdatetime': closed_date.replace("-", "/"),
            'occ': escape(self.ONE_CALL_CENTER),
            'notes': 'Response by Utiliquest'
        })
        self.log.log_event("Locate date/time injected into xml: %s" %
         closed_date)
        revision = names['revision']
        names['revision'] = escape(string.join([c for c in revision
                                                if c in string.digits], ''))

        if names['ticket'] and names['ticket'].strip()[0] == '0':
            names['ticket'] = names['ticket'].strip()[1:]

        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

class FTPResponderData(
  korterra_ftpresponder_data.CrownCastleKorTerraFTPResponderData):
    def adapt_ticket_number(self, ticket_number):
        if ticket_number:
            return 'CN' + ticket_number + 'S'
        return ticket_number

registry = {
    ('NCA1', 'email', ''): EmailResponderData,
    ('NCA1', 'xmlhttp', ''): XMLHTTPResponderData,
    ('NCA1', 'ftp', ''): FTPResponderData
}

