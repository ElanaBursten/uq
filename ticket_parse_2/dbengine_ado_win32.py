# dbengine_ado_win32.py
# Requires: win32all

import string
#
import pywintypes
import win32com.client
#
import adoconstants
import adotools
import config
import dbengine
import tools

class DBEngineADOWin32(dbengine.DBEngine):

    def __init__(self, dbdata={}):
        dbengine.DBEngine.__init__(self)

        if not dbdata:
            self._config = config.getConfiguration()
            dbdata = self._config.ado_database

        if dbdata:
            self.config = None
            if dbdata.get('conn_string', ''):
                self._conn = self._connect(dbdata['conn_string'])
            else:
                self._conn = self._make_connection(
                  dbdata['host'], dbdata['database'],
                  dbdata['login'], dbdata['password'])
            self.timeout = dbdata.get('timeout_seconds', self.timeout)
        #else:
        #    self._config = config.getConfiguration()
        #    a = self._config.ado_database
        #    self._conn = self._make_connection(a["host"], a["database"],
        #                a["login"], a["password"])

    #
    # connection

    def _make_connection_string(self, host, database, login, password):
        s = ["Provider=sqloledb;",
             "Data Source=%s;" % host,
             "Initial Catalog=%s;" % database,
             "User Id=%s;" % login,
             "Password=%s;" % password]
        connstr = string.join(s)
        return connstr

    def _make_connection(self, host, database, login, password):
        """ Make a connection string, then use it to connect. """
        s = self._make_connection_string(host, database, login, password)
        return self._connect(s)

    def _connect(self, conn_string):
        """ Connect using a connection string. """
        conn = win32com.client.Dispatch("ADODB.Connection")
        conn.Open(conn_string)
        return conn

    def check_connection(self):
        if not self._conn:
            raise dbengine.DBException("No connection established")
            # FIXME: use specific db error

    #
    # runsql

    def runsql(self, sql, timeout=None):
        try:
            cmd = win32com.client.Dispatch("ADODB.Command")
            cmd.ActiveConnection = self._conn
            cmd.CommandText = sql
            cmd.CommandTimeOut = timeout or self.timeout
            spam, code = cmd.Execute()

        except pywintypes.com_error, e:
            text = e.args[2][2]
            raise dbengine.DBException(text, timeout=timeout or self.timeout,
                  sql=sql)

        return spam
        # XXX what's the result value? o.o

    def runsql_result(self, sql, timeout=None):
        """ More robust version of runsql, with timeout set.
            Returns result set (a list of dicts).
        """
        MAX_ATTEMPTS = 100
        try:
            cmd = win32com.client.Dispatch("ADODB.Command")
            cmd.ActiveConnection = self._conn
            cmd.CommandText = sql
            cmd.CommandTimeOut = timeout or self.timeout

            rs, code = cmd.Execute()

            # fixed this to work with ADO 2.8 and/or MSSQL2K SP3
            # 'rs' can be a chain of things, that aren't always valid result
            # sets; in that case, loop until we find one
            for i in range(MAX_ATTEMPTS):
                if adotools.is_result_set(rs):
                    break
                else:
                    rs, code = rs.NextRecordset()

            if rs.Eof:
                return []

            # create list of dictionaries (one dict per record)
            fieldnames = adotools.get_fieldnames(rs)
            rows = tools.transpose(rs.GetRows())
            records = adotools.list_of_record_dicts(rows, fieldnames)
            return records

        except pywintypes.com_error, e:
            text = e.args[2][2]
            raise dbengine.DBException(text, timeout=timeout or self.timeout,
                  sql=sql)

    def runsql_result_multiple(self, sql, timeout=None):
        """ Run SQL that returns multiple result sets. """
        MAX_ATTEMPTS = 100
        resultsets = []
        try:
            cmd = win32com.client.Dispatch("ADODB.Command")
            cmd.ActiveConnection = self._conn
            cmd.CommandText = sql
            cmd.CommandTimeOut = timeout or self.timeout

            rs, code = cmd.Execute()

            for i in range(MAX_ATTEMPTS): # limited, so it won't hang forever
                if adotools.is_result_set(rs):
                    if rs.Eof:
                        resultsets.append([])
                    else:
                        fieldnames = adotools.get_fieldnames(rs)
                        rows = tools.transpose(rs.GetRows())
                        recs = adotools.list_of_record_dicts(rows, fieldnames)
                        resultsets.append(recs)

                rs, code = rs.NextRecordset()
                if not rs:
                    break

            return resultsets

        except pywintypes.com_error, e:
            text = e.args[2][2]
            raise dbengine.DBException(text, timeout=timeout or self.timeout,
                  sql=sql)

    # NOTE: this should really be the default, since it's safer, unless there
    # are valid reasons to do otherwise (e.g. performance issues)
    # .
    def runsql_with_parameters(self, sql, params, timeout=None):
        """ Run a parametrized SQL command, e.g.
            "select * from client where client_id = ?"
            <params> is a list of 3-tuples (value, sqltype, fieldname).  (The
            fieldname is not strictly necessary, but useful for debugging.)
            In certain cases, it may also be a list of strings.
        """
        MAX_ATTEMPTS = 100
        try:
            cmd = win32com.client.Dispatch("ADODB.Command")
            cmd.ActiveConnection = self._conn
            cmd.CommandText = sql
            cmd.CommandTimeOut = timeout or self.timeout

            # add parameters
            if params and isinstance(params[0], tuple):
                # named parameters
                for (value, sqltype, name) in params:
                    # errors may occur here that produce unhelpful messages,
                    # so I'm catching them and reraising the exception with
                    # a more informative string:
                    try:
                        p = sqltype.make_ado_parameter(value, cmd)
                        p.Value = value
                        cmd.Parameters.Append(p)
                    except AttributeError, e:
                        raise Exception("Cannot set value for field %r (%r)" %
                              (name, value)) # FIXME
            else:
                # assume a list of values (probably strings)
                # we shouldn't use this a lot (or at all), because DBInterface
                # is supposed to create "real" params if at all possible.
                for param in params:
                    p = cmd.CreateParameter(param, adoconstants.adVariant,
                        adoconstants.adParamInput)
                    p.Value = param
                    cmd.Parameters.Append(p)
            cmd.Prepared = True

            rs, code = cmd.Execute()

            for i in range(MAX_ATTEMPTS):
                if adotools.is_result_set(rs):
                    break # continue as usual, with this recordset
                else:
                    try:
                        rs, code = rs.NextRecordset()
                    except AttributeError:
                        # if rs does not have an attribute NextRecordset,
                        # then apparently we don't have a valid recordset,
                        # and should return []
                        return []

            if rs.Eof:
                return []

            fieldnames = adotools.get_fieldnames(rs)
            rows = tools.transpose(rs.GetRows())
            records = adotools.list_of_record_dicts(rows, fieldnames)
            return records

        except pywintypes.com_error, e:
            text = e.args[2][2]
            raise dbengine.DBException(text, timeout=timeout or self.timeout,
                  sql=sql, params=params)

    #
    # transactions

    def begin_transaction(self):
        self._conn.BeginTrans()

    def rollback_transaction(self):
        self._conn.RollbackTrans()

    def commit_transaction(self):
        self._conn.CommitTrans()


