# xmlconfigreader.py
# Automated reading of (parts of) XML configuration file.
# Created: 2002.11.14
# XXX Relies on xml.dom.minidom, so we should NOT convert this to ElementTree

def U(x):
    if isinstance(x, unicode):
        return x.encode("utf-8")
    else:
        return x

def node_as_dict(node, tagname, attrnames):
    """ Read a node with attributes, e.g. <foo a="42" b="12" />, and return the
        attributes as a dict. If attributes aren't present, use an empty string
        as the default.
    """
    for subnode in node.getElementsByTagName(tagname)[:1]:
        d = {}
        for attrname in attrnames:
            d[attrname] = U(subnode.getAttribute(attrname))
        return d
    return {}

def node_as_tuple(node, tagname, attrnames):
    """ Like node_as_dict, but return a tuple of values. """
    d = node_as_dict(node, tagname, attrnames)
    lst = []
    for name in attrnames:
        lst.append(d[name])
    return tuple(lst)

def node_as_dictlist(node, tagname, attrnames):
    """ Like node_as_dict, but find multiple nodes with the same name, and
        return them in a list. """
    dicts = []
    for subnode in node.getElementsByTagName(tagname):
        d = {}
        for attrname in attrnames:
            d[attrname] = U(subnode.getAttribute(attrname))
        dicts.append(d)
    return dicts

def node_as_string(node, tagname, attrname):
    """ Get a node with a tag with one attribute, and return the value of that
        attribute as a string.
        Example: <logdir name="c:/foo/" />
    """
    for subnode in node.getElementsByTagName(tagname)[:1]:
        s = U(subnode.getAttribute(attrname))
        return s
    return ""

def get_attributes(node, attrnames):
    """ Get a bunch of attrbutes from a node. Return as a tuple. """
    lst = []
    for name in attrnames:
        value = U(node.getAttribute(name))
        lst.append(value)
    return tuple(lst)

def get_dict(node, attrnames):
    d = {}
    for name in attrnames:
        value = U(node.getAttribute(name))
        d[name] = value
    return d

def find_node(node, tagnames):
    if tagnames:
        for subnode in node.getElementsByTagName(tagnames[0]):
            if len(tagnames) == 1:
                return subnode
            else:
                return find_node(subnode, tagnames[1:])

    raise ValueError, "Node not found: '%s'" % (node,)

