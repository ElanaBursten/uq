# IRTHresponder.py
# Created: 13 Apr 2002, Hans Nowak
# (original name: AtlantaResponder.py; renamed 2002.06.25)

import socket
import time
#
import abstractresponder
import irthresponder_data
import telnetbot
import tools

fmt = tools.fmt

socket.setdefaulttimeout(40)

# these call centers used to use the modem responder
MODEM_RESPONDERS = ('OCC1', 'FCV1', 'FMW1', 'FMW2')

class IRTHresponder(abstractresponder.AbstractResponder):
    name = "IRTHresponder"
    responder_type = 'irth' # as used by ResponderConfigData

    # __init__ derives from AbstractResponder

    ###
    ### responding

    def login(self, call_center, login):
        server, port, password = self.get_login_data(call_center, login)
        self.connect(server, port)
        self.login_telnet(login, password, call_center)

    def logout(self):
        self.logout_telnet()

    def get_logins(self, call_center):
        """ Return a list of logins for this call center.  If there are none,
            return an empty list. """
        responders = self.config.responders[call_center]
        try:
            return responders['logins']
        except KeyError:
            return []

    def get_login_data(self, call_center, login):
        d = self.config.responders[call_center]
        server, port = d["server"], d["port"]
        try:
            port = int(port)
        except ValueError:
            port = 23   # Telnet default

        password = None
        for d in self.config.responders[call_center]["logins"]:
            if d["login"] == login:
                password = d["password"]
        if password is None:
            raise abstractresponder.ResponderError, \
                  "Unknown client code (%s)" % (login)

        return server, port, password

    ###
    ### helper functions. everything passes through respond_to_locates().

    def load_responder_data(self, call_center, raw_responderdata):
        self.responderdata = irthresponder_data.getresponderdata(
                             self.config, call_center, raw_responderdata)

    def respond_config(self):
        raise NotImplementedError

    #
    # responder log

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.startswith("2") or response.startswith("457")

    #
    # Telnet stuff

    def connect(self, site, port):
        self.bot = telnetbot.TelnetBot(site, port, timeout=40)

    def login_telnet(self, login, password, call_center):
        # we expect a line starting with "220"
        r = self.bot.read_until_regex(self.responderdata.login_regexen)
        idx, match, text = r

        self.bot.write("USER " + login + "\r\n")
        x = self.bot.read_until(self.responderdata.user_string)
        if x == '':
            raise abstractresponder.ResponderError, "Timed out"

        self.bot.write("PASS " + password + "\r\n")
        r = self.bot.read_until_regex(self.responderdata.password_regexen)
        idx, match, text = r
        if idx == 1:
            raise abstractresponder.ResponderError, \
                  "Could not log in: invalid password"

    def logout_telnet(self):
        self.bot.write("QUIT\r\n")
        self.bot.read_all()

    def wait_for_response(self):
        attempts = 0
        resp = ""
        while attempts < 10:
            resp = self.bot.read_until(self.responderdata.response_until)
            if resp:
                return resp
            attempts = attempts + 1
            print ":"
            time.sleep(0.2)
        # we did not get a response
        raise socket.error, "Failed to get a response from server"

    def make_response(self, ticket, membercode, responsecode, explanation=None):
        # delegate to *ResponderData.
        return self.responderdata.make_response(ticket, membercode,
               responsecode, explanation)

    def send_response(self, ticket_number, membercode, responsecode,
                      explanation=None, locator_name='', version=''):
        # version is unused at the moment but is necessary because some
        # other types of responders use it...
        s = self.make_response(ticket_number, membercode, responsecode,
                               explanation)
        self.bot.write(s)
        resp = self.wait_for_response()
        return resp

    def get_code_from_response(self, response):
        """ Get a code from the response from the server. """
        return response[:3] # first three characters


if __name__ == "__main__":

    abstractresponder.run_toplevel(IRTHresponder)

