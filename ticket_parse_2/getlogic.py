import businesslogic

geocoder = None

def getlogic(call_center, logic_dict, tdb, holiday_list):
    """ Get a BusinessLogic instance from the collection dict, or create
        and add one first if necessary. """
    global geocoder
    if logic_dict.has_key(call_center):
        return logic_dict[call_center]
    else:
        logic = businesslogic.getbusinesslogic(call_center, tdb, holiday_list,
                                               geocoder)
        logic_dict[call_center] = logic
        return logic
