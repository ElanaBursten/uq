# common_xmlhttpresponder_data.py

import string
import xml.etree.ElementTree as ET
from xml.sax.saxutils import escape

import date
import xmlhttpresponder_data

# todo(dan) this should work for newtin responders, and anything "similar enough"
class Newtin_XMLHTTPResponderData(xmlhttpresponder_data.XMLHTTPResponderData):
    # todo(dan) this is like all the others, but may want to review
    codes_ok = ['successful', '250 ok', 'response posted']

    codes_reprocess = []
    codes_error = []

    ONE_CALL_CENTER = ''

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <soap:Body>
    <AddContractLocatorResponses xmlns="http://tempuri.org/">
      <xmlContractLocatorResponses>
        <ContractLocatorResponseDataSet xmlns="http://tempuri.org/ContractLocatorResponseDataSet.xsd">
          <RESPONSE_INFO>
            <One_Call_Center>%(one_call_center)s</One_Call_Center>
            <CDC>%(membercode)s</CDC>
            <Authentication_Code>%(auth_code)s</Authentication_Code>
            <Ticket_Number>%(ticket)s</Ticket_Number>
            <Version_Number>%(revision)s</Version_Number>
            <Response_Code>%(responsecode)s</Response_Code>
            <Locate_DateTime>%(locate_datetime)s</Locate_DateTime>
            <Locator_Name>%(locator_name)s</Locator_Name>
            <Note>%(notes)s</Note>
          </RESPONSE_INFO>
        </ContractLocatorResponseDataSet>
      </xmlContractLocatorResponses>
    </AddContractLocatorResponses>
  </soap:Body>
</soap:Envelope>"""

    def get_xml(self, names):
        closed_date = self.row['closed_date']
        if closed_date is None:
            # No closed date, set to now
            closed_date = date.Date().isodate()
            self.log.log_event("Locate date/time injected into xml based"\
                               " on current date/time")
        else:
            self.log.log_event("Locate date/time injected into xml based"\
                               " on closed_date date/time")

        names.update({
            'one_call_center': escape(self.ONE_CALL_CENTER),
            'auth_code': escape(self.id),
            'locate_datetime': closed_date.replace("-", "/"),
            'locator_name': 'UTI',
            'notes': 'Response by Utiliquest'
        })

        self.log.log_event("Locate date/time injected into xml: %s" %
                           closed_date)

        revision = names['revision']
        names['revision'] = escape(
          string.join([c for c in revision if c in string.digits], ''))

        return self.XML_TEMPLATE % names

    # todo(dan) Review using codes_ok. Might want an 'always successful' option.
    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        return response.lower().strip() in self.codes_ok

class TicketCheck_XMLHTTPResponderData(xmlhttpresponder_data.XMLHTTPResponderData):
    pending_responses_query = '' # use the default

    codes_success = ['00'] # Success

    codes_ok = codes_success + [
      '02', # Error 02: District Code does not exist.
      '04', # Error 04: District Code is not valid for Ticket Number.
      '05', # Error 05: Current Status is already a 1.
      '06', # Error 06: Current Status is already a 2.
      '07', # Error 07: Current Status is already a 9.
      '08', # Error 08: Current Status is already a 10.
      '09', # Error 09: Invalid Status. '5' is Currently Not Valid.
      '10', # Error 10: Invalid Status. '9' is Currently Not Valid.
      '11', # Error 11: Invalid Status. '10' is Currently Not Valid.
      '12', # Error 12: Invalid Delay Request. '3' is Currently Not Valid.
      '13', # Error 13: Invalid Delay Request. '4' is Currently Not Valid.
      '15', # Error 15: The Ticket has been cancelled.
      '17', # Error 17: Invalid attempt to change from Marked (2)
      '18', # Error 18: Ticket is already locked.
      '20', # Error 20: Login ID does not have permission to open or close a ticket.
      '21', # Error 21: Login ID does not have permission to add a note.
      '23', # Error 23: Invalid Status. '11' is Currently Not Valid.
      '24', # Error 24: Invalid Status. '12' is Currently Not Valid.
      '25', # Error 25: Invalid Status. '13' is Currently Not Valid.
      '26', # Error 26: Invalid Status. '14' is Currently Not Valid.
      '27', # Error 27: Invalid Status. '15' is Currently Not Valid.
      '28', # Error 28: Current Status is already a 11.
      '29', # Error 29: Current Status is already a 13.
      '30', # Error 30: Current Status is already a 3.
      '31', # Error 31: Current Status is already a 4.
      '32', # Error 32: Current Status is already a 5.
      '33', # Error 33: Current Status is already a 6.
      '34', # Error 34: Current Status is already a 7.
      '35', # Error 35: Current Status is already a 8.
      '36'] # Error 36: Cannot post the same status as the current status.

    codes_reprocess = []
    codes_error = []

    codes_error_retry = [
      '01', # Error 01: Ticket does not exist.
      '03', # Error 03: Status is unknown value.
      '14', # Error 14: There has been a SYSTEM ERROR.
      '16', # Error 16: Login ID does not have permission to status District Code.
      '19', # Error 19: Operation Timed Out. Recommend Retry.
      '22', # Error 22: Ticket Header does not permit Ticket Check Status.
      'error']

    XML_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8"?>
<positiveresponsexmlobject>
  <userName>%(username)s</userName>
  <password>%(password)s</password>
  <state>%(state)s</state>
  <ticket>%(ticket)s</ticket>
  <district>%(membercode)s</district>
  <status>%(responsecode)s</status>
  <comments>%(notes)s</comments>
  <url>%(epr_link)s</url>
</positiveresponsexmlobject>"""

    CONTENT_TYPE_HEADER = 'application/xml; charset=utf-8'

    def get_xml(self, names):
        # todo(dan) do notes need truncating after escaping?
        names.update({
            'username': escape(self.initials),
            'password': escape(self.id),
            'state': escape(self.row['work_state']),
            'notes': escape(
              self.truncated_notes(self.get_public_locate_notes(), 200)),
            'epr_link': escape(self.get_epr_link())
        })
        return self.XML_TEMPLATE % names

    def response_is_success(self, row, resp):
        responsecode, explanationcode, response = resp
        if response:
            return response.lower().strip() in self.codes_success
        else:
            return False

    def process_response(self, reply):
        """
        Should be in one of these formats:

        <?xml version="1.0" ?>
        <string xmlns="http://services.occinc.com/wsexternal">
          <result>
            <Code>00</Code>
            <Message>SUCCESS</Message>
          </result>
        </string>

        <?xml version="1.0"?>
        <string xmlns="http://services.occinc.com/wsexternal">
          <result>ERROR</result>
        </string>
        """

        ns = {'x': 'http://services.occinc.com/wsexternal'}
        results = ET.fromstring(reply.xml).findall('x:result', ns)
        if len(results) == 1:
            codes = results[0].findall('x:Code', ns)
            messages = results[0].findall('x:Message', ns)
            if len(codes) == len(messages) == 1:
                self.log.log_event(
                  "Result message: %s" % messages[0].text)
                return codes[0].text
            elif (len(results[0]) == 0) and (
              results[0].text.lower().strip() == 'error'):
                return results[0].text

        self.log.log_event("Result XML:\n%s" % reply.xml)
        return 'unknown'

def get_responderdata(raw_responderdata):
    def param_value(params, param_code):
        for p in params:
            p_code, sep, p_value = p.partition('=')
            if p_code.strip() == param_code and sep == '=':
                return p_value.strip()
        return None

    if not raw_responderdata:
        return None

    responderdata = None

    kind = raw_responderdata.get('kind', '')

    if kind.startswith('newtin'): 
        responderdata = Newtin_XMLHTTPResponderData()
        params = kind.split()[1:]

        if 'multi_q' in params:
            responderdata.queue = {'table': 'responder_multi_queue',
                                   'respond_to': 'client'}

        one_call_center = param_value(params, 'one_call_center')
        if one_call_center:
            responderdata.ONE_CALL_CENTER = one_call_center

    elif kind.startswith('ticketcheck'):
        responderdata = TicketCheck_XMLHTTPResponderData()

    return responderdata