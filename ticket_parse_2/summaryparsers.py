# summaryparsers.py
# Created: 03 Mar 2002, Hans Nowak

import string
#
import call_centers
import formats
import summarydetail
import ticketsummary
from date import Date


class SummaryError(Exception):
    pass

def get_summaryparser(format):
    try:
        pclass = globals()[format+"SummaryParser"]
    except KeyError:
        try:
            pclass = formats.get_summaryparser(format)
        except:
            import traceback
            traceback.print_exc()
            raise SummaryError("No SummaryParser class available for: %s" %
                  format)
    return pclass


class BaseSummaryParser:
    """ Abstract class for parsing summary tickets. """

    def __init__(self, format):
        self.format = format # != call center

    def read(self, data):
        """ Read and return a list of ticket data. """
        return []

    def read_header(self, data):
        """ Read and return header info (client, date, expected_tickets). """
        #return "", "", 0
        client_code = self.get_client_code(data)
        date = self.get_summary_date(data)
        expected_tickets = self.get_expected_tickets(data)
        return client_code, date, expected_tickets
        # now, subclasses don't have to redefine read_header anymore.

    def get_client_code(self, data):
        raise NotImplementedError

    def get_summary_date(self, data):
        raise NotImplementedError

    def get_expected_tickets(self, data):
        raise NotImplementedError

    @classmethod
    def count_received(cls, summ_data):
        return len(summ_data)

    def parse(self, data):
        # get data
        data = self.pre_process(data)
        tickets = self.read(data)
        client, date, expected_tickets = self.read_header(data)
        # create and fill TicketSummary instance
        ts = ticketsummary.TicketSummary()
        ts.format = self.format
        ts.image = data
        ts.client_code = client
        ts.summary_date = date
        ts.expected_tickets = expected_tickets
        ts.data = tickets

        # try to determine format... there are currently two ways
        try:
            ts.call_center = call_centers.format2cc[self.format]
        except:
            print "No call center mapping for %s" % (self.format,)

        ts = self.post_process(ts)  # last-minute changes

        ts._has_header = self.has_header(data)
        ts._has_footer = self.has_footer(data)

        return ts

    # override these methods in subclasses
    def has_header(self, data):
        return 0
    def has_footer(self, data):
        return 0

    def pre_process(self, data):
        return data

    def post_process(self, ts):
        # correct times
        for item in ts.data:
            assert isinstance(item, summarydetail.SummaryDetail)
            #if not self._isvalidtime(item[2]):
            #    item[2] = "00:00"
            if not self._isvalidtime(item.ticket_time):
                item.ticket_time = "00:00"

        # set summary_date_end
        if ts.summary_date:
            ts.summary_date_end = self._nextmorning(ts.summary_date)

        return ts

    # XXX move to date tools
    @staticmethod
    def _isvalidtime(time):
        return (len(time) == 5
         and time[0] in string.digits
         and time[1] in string.digits
         and time[2] == ":"
         and time[3] in string.digits
         and time[4] in string.digits
         and 0 <= int(time[:2]) <= 23
         and 0 <= int(time[3:]) <= 59)

    # XXX move to date tools
    @staticmethod
    def _nextmorning(datestr):
        d = Date(datestr)
        d.inc() # next day
        d.resettime()   # make sure it's 00:00:00
        return d.isodate()
