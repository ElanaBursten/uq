# versions.py

import ftpresponder
import IRTHresponder
import main
import ticket_db
import ticketrouter
import watchdog

if __name__ == "__main__":

    print "** UQ **"
    print "main.py:", main.__version__
    print "ticketrouter.py:", ticketrouter.__version__
    print "watchdog.py:", watchdog.__version__
    print "IRTHresponder.py:", IRTHresponder.__version__
    print "FTPresponder.py:", ftpresponder.__version__

    print

    print "** Database **"
    tdb = ticket_db.TicketDB()
    conn = tdb.dbado.conn
    print "ADO version:", conn.Version
    print "DBMS name:", conn.Properties("DBMS Name")
    print "DBMS version:", conn.Properties("DBMS Version")
    print "OLE DB version:", conn.Properties("OLE DB Version")
    print "Provider name:", conn.Properties("Provider Name")
    print "Provider version:", conn.Properties("Provider Version")
