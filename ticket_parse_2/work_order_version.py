# work_order_version.py

import date
import sqlmap
import sqlmaptypes as smt
import static_tables

class WorkOrderVersion(sqlmap.SQLMap):

    __table__ = "work_order_version"
    __key__ = "wo_version_id"
    __fields__ = static_tables.work_order_version

    @classmethod
    def add_version(cls, tdb, wo_id, work_order, filename):
        wov = cls.new(wo_id=wo_id,
                      wo_number=work_order.wo_number,
                      wo_revision='',
                      work_type=work_order.work_type,
                      transmit_date=work_order.transmit_date,
                      processed_date=date.Date().isodate(),
                      arrival_date=work_order._filedate,
                      filename=filename,
                      image=work_order.image,
                      wo_source=work_order.wo_source)
        wov_id = wov.insert(tdb)
        return wov_id

