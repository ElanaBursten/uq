# ticketsummary.py
# XXX rename to summaryheader.py for consistency?

import date
import string
#
import sqlmap
import summarydetail # for load
import static_tables

class TicketSummary(sqlmap.SQLMap):
    __table__ = "summary_header"
    __key__ = "summary_header_id"
    __fields__ = static_tables.summary_header

    def __init__(self):
        sqlmap.SQLMap.__init__(self)

        self.data = []
        # self.data is supposed to be filled with 5-tuples (number,
        # ticket_number, time, revision, type_code)

        self.image = ""
        # the raw summary image.  will not be stored, but can be included in
        # error messages.

        self._has_header = 0
        self._has_footer = 0

        # these fields cannot be NULL, but the empty string works
        self.summary_date = ""
        self.summary_date_end = ""
        self.reset_changes()

    ### some simple methods for polymorphism

    def repr(self): # note: not __repr__
        """ Return a long string representation of the summary, showing all
            attributes. """
        z = []
        items = self.__dict__.items()
        items.sort()
        for key, value in items:
            if not key.startswith("_"):
                s = "%s = %s" % (key, repr(value))
                z.append(s)
        return string.join(z, "\n")

    def set_time(self, isodate):
        """ Convert the times in self.data list to full ISO-dates, using a
            given date (whose time will be ignored).
            (This is a special method because the summary parsers aren't
            always able to do this; sometimes an "external" method call is
            required.)
        """
        #print "## set_time called with:", isodate
        d = date.Date(isodate)
        for item in self.data:
            time = item[2]
            if len(time) == 5:  # make sure we have the "hh:mm" format
                hours, minutes = string.split(time, ":")
                d.hours = int(hours)
                d.minutes = int(minutes)
                d.seconds = 0   # no need to keep existing seconds
                item[2] = d.isodate()

    #
    # pickling

    def __getstate__(self):
        state = self.__dict__.copy()
        return state

    def __setstate__(self, adict):
        self.__dict__.update(adict)

    #
    # custom insert/update/etc

    def insert(self, tdb):
        """ Insert this summary into summary_header, and all SummaryDetail
            objects in self.data into summary_detail.
            Return a tuple (summary_header_id, summary_detail_ids).
        """
        summary_header_id = sqlmap.SQLMap.insert(self, tdb)
        summary_detail_ids = []
        for detail in self.data:
            detail.summary_header_id = summary_header_id
            did = detail.insert(tdb)
            summary_detail_ids.append(did)
        return (summary_header_id, summary_detail_ids)

    def update(self, tdb, force=0):
        # update header
        hid = super(TicketSummary, self).update(tdb, force=force)

        # update details
        # if detail is new: insert it
        # if it already exists: update it
        dids = []
        for detail in self.data:
            # make sure that summary_header_id is set correctly
            if detail.summary_header_id != hid:
                detail.summary_header_id = hid
            did = detail.save(tdb, force=force)
            dids.append(did)

        return hid, dids

        # Issues:
        # Do we update all fields, all records?
        # Or shall we somehow 'mark' fields and/or records that changed?
        # If we add details to an existing summary, those details need to be
        # *inserted* rather than updated.  (Also, can we assume that
        # summary_header_id is automatically set in such cases?)
        # Probably not, so the save() method should come in handy here.  Or
        # maybe something else that sets the appropriate field...

    @classmethod
    def load(cls, tdb, _id):
        # load summary header
        header = super(TicketSummary, cls).load(tdb, _id)

        # load summary detail records for this header (using bulk loading)
        details = summarydetail.SummaryDetail.load_multiple(tdb, _id, cls.__key__)
        # and inject them
        header.data = details

        return header


