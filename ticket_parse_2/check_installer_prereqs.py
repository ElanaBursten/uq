# check_installer_prereqs.py

import sys

__usage__ = """\
check_installer_prereqs.py PythonMajorVersion PythonMinorVersion
 MinPythonMicroVersion MinPyWin32Build

PythonMajorVersion      Python major version (e.g. 2)
PythonMinorVersion      Python minor version (e.g. 5)
MinPythonMicroVersion   Minimum Python micro version (e.g. 4)
MinPyWin32Build         Minimum PyWin32 build (e.g. 216)

Check backend installer prerequisites, and set an exit code as follows:
0   Ok. All prerequisites met.
1   Error running script
2   Invalid command line syntax
11  Incorrect Python version
12  Missing PyWin32
13  Incorrect PyWin32 build
"""

def check_installer_prereqs(python_major, python_minor, min_python_micro,
 min_pywin32_build):
    # Check Python version
    if (sys.version_info[0] != python_major) or (sys.version_info[1] !=
     python_minor) or (sys.version_info[2] < min_python_micro):
        return 11

    # Check if PyWin32 is installed
    try:
        import win32api
    except ImportError:
        return 12

    # Check PyWin32 build
    if (win32api.GetFileVersionInfo(win32api.__file__, chr(92))[
     'FileVersionLS'] >> 16) < min_pywin32_build:
        return 13

    return 0

if __name__ == "__main__":

    # todo(dan) Consider changing arguments to options, and setting defaults

    if len(sys.argv) <> 5:
        print __usage__
        sys.exit(2)

    try:
        python_major = int(sys.argv[1])
        python_minor = int(sys.argv[2])
        min_python_micro = int(sys.argv[3])
        min_pywin32_build = int(sys.argv[4])
    except:
        print __usage__
        sys.exit(2)

    # todo(dan) Print message instead of just the exit code?
    exit_code = check_installer_prereqs(python_major, python_minor,
     min_python_micro, min_pywin32_build)
    print exit_code
    sys.exit(exit_code)
