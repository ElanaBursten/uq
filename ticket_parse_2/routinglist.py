# routinglist.py
# Created: 2002.11.13 HN

import re
import string
import sys
#
import tools

# TODO:
# - errorhandling for this class... what if a regex doesn't match? We should
# then move on to the next one, but somehow the system should tell us what is
# going on. Add in a logger?

class RoutingListRow(tools.Struct):
    __fields__ = ["call_center", "field_name", "field_value", "area_id"]

class RoutingList:

    def __init__(self, tdb, verbose=1):
        self.tdb = tdb
        self.data = {}
        self.verbose = verbose

    def get_rules(self, call_center):
        if not self.data.has_key(call_center):
            rows = self._get_rules(call_center)
            self.data[call_center] = self._rows_to_rules(rows)
        return self.data[call_center]

    def _rows_to_rules(self, rows):
        rules = []
        for row in rows:
            rule = RoutingListRow(call_center=str(row["call_center"]),
                   field_name=str(row["field_name"]).lower(),
                   field_value=str(row["field_value"]),
                   area_id=str(row["area_id"]))
            if rule.field_value.startswith("/"):
                # this is a regular expression; compile it
                try:
                    regex = re.compile(rule.field_value[1:])
                except:
                    if self.verbose:
                        print >> sys.stderr, "Regex %s cannot be compiled" % (
                              repr(rule.field_value),)
                    continue
                rule.field_value = regex
            rules.append(rule)
        return rules

    def _get_rules(self, call_center):
        """ Get rules from the database. """
        sql = """
         select * from routing_list
         where call_center = '%s'
        """
        sql = sql % (call_center,)

        return self.tdb.runsql_result(sql)

    def route(self, row):
        """ Attempt to find a matching rule. If found, return the area_id. """
        call_center = row.get("ticket_format", "")
        if call_center:
            rules = self.get_rules(call_center)
            for rule in rules:
                value = row.get(rule.field_name, "") or ""
                if isinstance(rule.field_value, basestring):
                    if value.strip() == rule.field_value:
                        return (rule, rule.area_id)
                else:
                    m = rule.field_value.search(value)
                    if m:
                        return (rule, rule.area_id)

        return None # nothing matched

