# handle_damage.py
# Set the due date for damage records.
#
# 2002.12.27 HN Split off from ticketrouter.py.

from __future__ import with_statement
import getopt
import os
import sys
#
import config
import datadir
import date
import date_special
import dbinterface_ado
import dbinterface_old as dbinterface
import emailtools
import errorhandling2
import errorhandling_special
import mutex
import ticket_db
import ticketdb_decorator
import tools
import windows_tools

PROCESS_NAME = "DamageHandler"

class DamageHandlerOptions(tools.Bunch):
    configfile = ""
    verbose = 1
    wait = 0       # if set, wait for input before we end


class DamageHandler:

    def __init__(self, options=None, dbado=None):
        self.options = options or DamageHandlerOptions()

        if self.options.configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                      "Configuration already specified"
            self.config = config.Configuration(self.options.configfile)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()

        self.tdb = ticket_db.TicketDB(dbado=dbado)
        self.tdb = ticketdb_decorator.TicketDBDecorator(self.tdb,
                   ticketdb_decorator.TicketDBMethodDecorator)
        self.holiday_list = date_special.read_holidays(self.tdb)
        self.email = emailtools.Emails(self.tdb)

        # install logger
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtp_accs[0], me="DamageHandler",
                   cc_emails=self.email,
                   admin_emails=self.config.admins,
                   subject="DamageHandler Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = self.options.verbose

        self.rules = self.read_damage_rules()

        self.log.log_event("Start of damage handler")

    def read_damage_rules(self):
        return self.tdb.getrecords('damage_due_date_rule', active=1)

    def find_damage_rules(self, pc, company):
        """ Return all rules (if any) for the given profit center and
            company. """
        found = [rule for rule in self.rules
                 if rule['pc_code'] == pc
                 and rule['utility_co_damaged'] == company]
        if found: return found
        # if not found, try wildcards
        return [rule for rules in self.rules
                if rule['pc_code'] in (pc, '*')
                and rule['utility_co_damaged'] in (company, '*')]

    def run_damage(self, runonce=0):
        self.log.log_event("handle_damage.py started with pid %s" % os.getpid())
        while 1:
            damage_records = self.tdb.get_damage_without_due_date()
            if damage_records:
                if self.options.verbose:
                    print len(damage_records), \
                     "damage records found with no due date, updating..."
                try:
                    self.set_damage_due_date(damage_records)
                except:
                    ep = errorhandling2.errorpacket(
                     info="Error when setting damage due dates")
                    self.log.log(ep, send=1, dump=1, write=1)
                    self.log.force_send()
                    break
            else:
                if self.options.verbose:
                    print "No records found."
            if len(damage_records) < 50 or runonce:
                break

    def run(self, runonce=0):
        try:
            self.run_damage(runonce=runonce)
        except:
            ep = errorhandling2.errorpacket()
            ep.add(info="Error when trying to set damage due dates")
            self.log.log(ep, send=1, dump=1, write=1)
            self.log.force_send()
        self.log.log_event("End of damage handler")

        if self.options.wait:
            raw_input("[-w option specified] Press Enter to finish.")

    # XXX can't we just do an update statement??
    # maybe not... this does a bulk update
    def set_damage_due_date(self, damage_records):
        dates = {}  # id: due_date
        for row in damage_records:
            damage_id = row["damage_id"]
            due_date = row.get("uq_notified_date", None)
            if not due_date:
                continue    # ignore this one
            due_date = self.compute_damage_due_date(row, due_date)
            dates[damage_id] = due_date # ?
            s = "Setting due date of damage id %s to '%s'" % (
                 damage_id, due_date)
            self.log.log_event(s)

            # update damage record
            sql = """
             update damage
             set due_date = '%s'
             where damage_id = '%s'
            """ % (due_date, damage_id)
            self.tdb.runsql(sql)

    def compute_damage_due_date(self, row, date):
        # check damage due date rules
        rules = self.find_damage_rules(row['profit_center'],
                row['utility_co_damaged'])
        if rules:
            # use the first rule that matches profit center and company; use
            # +d where d is the number of days for that rule; if the value is
            # invalid, fall back to the default (+5 days)
            days = int(rules[0]['days'] or '5')
            return self.inc_due_date(date, days)
        else:
            # if no rules are specified, use the default (+5 days)
            return self.inc_due_date(date, days=5)

    def inc_due_date(self, datestr, days=5):
        d = date.Date(datestr)
        count = 0
        while count < days:
            d.inc()
            if not (d.isweekend()
             or date_special.isholiday(d, '*', self.holiday_list)):
                count += 1

        return d.isodate()


if __name__ == "__main__":

    options = DamageHandlerOptions()

    opts, args = getopt.getopt(sys.argv[1:], "c:w", ["data="])

    for o, a in opts:
        if o == "-c":
            options.configfile = a
        elif o == '-w':
            options.wait = 1

    try:
        log = errorhandling_special.toplevel_logger(options.configfile,
              "DamageHandler", "DamageHandler Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(options.configfile,
          "DamageHandler")
        sys.exit(1)

    mutex_name = PROCESS_NAME + ":" + datadir.datadir.get_id()
    try:
        dbado = dbinterface_ado.DBInterfaceADO()
        windows_tools.setconsoletitle("damagehandler")
        with mutex.MutexLock(dbado, mutex_name):
            dh = DamageHandler(options, dbado=dbado)
            dh.run()
    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)

