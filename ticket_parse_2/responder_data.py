# responder_data.py

# XXX the following query is DEPRECATED; should no longer be necessary, Mantis
# #1896 was resolved years ago

"""
The following is to prevent responding to certain VA call centers
until the responder queue insert date is more than 5 minutes old.
This is a (hopefully) temporary fix for db sync issues in which
the contact data has not been updated. Mantis #1896
"""
VA_SQL = """
        select top 800
         rq.locate_id, locate.status, ticket.ticket_format,
         ticket.ticket_id, ticket.ticket_number, locate.client_code,
         ticket.ticket_type, ticket.work_state, rq.insert_date,
         ticket.serial_number, locate.closed_date, ticket.work_county,
         ticket.work_city, ticket.work_date, ticket.service_area_code,
         ticket.due_date,
         coalesce((select info from ticket_info ti2 where ti2.ticket_info_id =
          (select max(ticket_info_id) from ticket_info ti
            where ti.ticket_id = locate.ticket_id
            and info_type='ONGOCONT')), '') as contact,
         (select min(arrival_date) from jobsite_arrival ja
            where ja.ticket_id=ticket.ticket_id) as first_arrival_date
         from responder_queue rq
         inner join locate on rq.locate_id=locate.locate_id
         inner join ticket on locate.ticket_id=ticket.ticket_id
        where ('%s' = rq.ticket_format)
         and (rq.do_not_respond_before is null
              or rq.do_not_respond_before <= getdate())
         and not exists (select * from response_log rl
            where rl.reply = '(ftp waiting)'
            and rl.locate_id = rq.locate_id
            and DateDiff(minute, rl.response_date, GetDate()) < 240)
         and (DateDiff(minute, rq.insert_date, GetDate()) > 5)
        order by insert_date
         """

# not sure if these are still used? at least not by FTP responder
special_queries = {
    "FMW1":  "exec get_pending_responses_client 'FMW1', 'PEP904'",
    "FMW3":  "exec get_pending_responses_client 'FMW1', 'PEP904'",
    "FCV3":  VA_SQL % "FCV3",
    "OCC2":  VA_SQL % "OCC2",
    "FPK1":  VA_SQL % "FPK1",
    "FDE2":  VA_SQL % "FDE2",
}

# special queries for FTP responder
special_queries_ftp = {
    "FMB2":  "exec get_pending_responses_ucc 'FMB2', @RetryPeriod = 120",
}
# XXX it's possible we need a special query for FMB1 as well... although the
# user can exclude clients in config.xml, so this may not be necessary.

