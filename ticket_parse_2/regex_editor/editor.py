versionInfo="0.0.1"
import logging
import platform

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='regex_editor.log',
                    filemode='w')

# define a Handler which writes WARN messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.WARN)

# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)

# create logger
logger = logging.getLogger("editor")

logger.debug("Logger set up")

import sys
import time
import re
import sre_constants
import site
site.addsitedir('.')

import os
import inspect
import cStringIO
import traceback
import imp
# The following imports are for py2exe (lazy evaluators in email module)
from email import Charset
from email import encoders
from email.mime import multipart
from email.mime import text
from email import generator
from email import iterators

from qteditorwindow import Ui_editorwindow as RegexEditorWindow
from PyQt4 import QtCore, \
                  QtGui
from PyQt4.QtGui import QTextCursor, \
                        QTextCharFormat, \
                        QBrush, \
                        QTextFormat, \
                        QColor, \
                        QErrorMessage, \
                        QMessageBox
import PyQt4.QtTest
import helpform

# determine default directory for sample tickets... may or may not be
# present, depending on whether we have this part of the svn source tree
whereami = os.path.dirname(os.path.abspath(__file__))
sample_dir = os.path.join(whereami, '..', 'testdata', 'sample_tickets')

def excepthook(excType, excValue, tracebackobj):
    """
    Global function to catch unhandled exceptions.

    @param excType exception type
    @param excValue exception value
    @param tracebackobj traceback object
    """
    separator = '-' * 80
    notice = \
        """An unhandled exception occurred. Please report the problem\n"""\
        """using the error reporting dialog or via email to <%s>.\n"""\
        """A log has been written to "%s".\n\nError information:\n""" % \
        ("charles.mccreary@oasisdigital.com", logger.root.handlers[0].baseFilename)

    timeString = time.strftime("%Y-%m-%d, %H:%M:%S")


    tbinfofile = cStringIO.StringIO()
    traceback.print_tb(tracebackobj, None, tbinfofile)
    tbinfofile.seek(0)
    tbinfo = tbinfofile.read()
    errmsg = '%s: \n%s' % (str(excType), str(excValue))
    sections = [separator, timeString, separator, errmsg, separator, tbinfo]
    msg = '\n'.join(sections)
    try:
        logger.critical(msg)
    except IOError:
        pass
    errorbox = QtGui.QMessageBox()
    errorbox.setWindowTitle("regex_editor unhandled exception")
    errorbox.setText(str(notice)+str(msg)+str(versionInfo))
    errorbox.exec_()

sys.excepthook = excepthook

import formats
import lateralregex
import findre

filedict = dict()
importeddict = dict()

'''
The regexdict contains the fields from the ticket table. This would
best be handled by hooking the regex_editor with the db. The values are
the regex for each field, either from the parser format or generated
'''
regexdict = {
    'caller_phone': None,
    'caller': None,
    'work_address_number': None,
    'ticket_type': None,
    'map_ref': None,
    'con_state': None,
    'update_of': None,
    'operator': None,
    'con_name': None,
    'duration': None,
    'installer_number': None,
    'priority': None,
    'call_date': None,
    'work_state': None,
    'work_subdivision': None,
    'transmit_date': None,
    'work_address_number_2': None,
    'work_lat': None,
    'caller_fax': None,
    'legal_good_thru': None,
    'do_not_respond_before': None,
    'serial_number': None,
    'start_date': None,
    'channel': None,
    'work_date': None,
    'status': None,
    'due_date': None,
    'recv_manager_id': None,
    'end_date': None,
    'work_description': None,
    'work_long': None,
    'company': None,
    'work_type': None,
    'respond_date': None,
    'alert': None,
    'service_area_code': None,
    'work_remarks': None,
    'work_city': None,
    'do_not_mark_before': None,
    'caller_cellular': None,
    'legal_restake': None,
    'ward': None,
    'explosives': None,
    'work_zip': None,
    'kind': None,
    'map_page': None,
    'work_notc': None,
    'con_type': None,
    'followup_type_id': None,
    'caller_contact': None,
    'points': None,
    'caller_altcontact': None,
    'work_county': None,
    'work_cross': None,
    'ticket_number': None,
    'con_city': None,
    'work_address_street': None,
    'caller_email': None,
    'con_zip': None,
    'con_address': None
    }

'''
Rudimentary format template
'''
filetemplate = """from parsers import BaseParser, R, ParserError
import datetime
import tools
import string
import re

class Parser(%%inherits%%):
%%regexstatements%%

class SummaryParser(object):
%%summary_parser%%
"""

class Matcher(object):
    def __init__(self,match,valid=False):
        self.match = match
        self.valid = valid

class EditorApplication(RegexEditorWindow):
    similarlines = [None]

    substringformat = QTextCharFormat()
    substringformat.setForeground(QBrush(QColor(255, 0, 0)))
    substringformat.setBackground(QBrush(QColor(255, 255, 255)))
    substringformat.setFontWeight(75) # bold

    fullmatchformat = QTextCharFormat()
    fullmatchformat.setForeground(QBrush(QColor(0, 0, 0)))
    fullmatchformat.setBackground(QBrush(QColor(255, 255, 0)))
    fullmatchformat.setFontWeight(75) # bold

    nothingformat = QTextCharFormat()
    nothingformat.setForeground(QBrush(QColor(0,0,0)))
    nothingformat.setBackground(QBrush(QColor(255, 255, 255)))
    nothingformat.setFontWeight(50) # normal

    workingdoc = ""
    editedautoregex = ""
    autoregexchanged = False

    def __init__(self, editorwindow):
        self.editorwindow = editorwindow
        self.tr = editorwindow.tr
        self.setupUi(editorwindow)
        self.dbfieldlist.setEnabled(False)
        self.userregex.setEnabled(False)

        QtCore.QObject.connect(self.menuFile, QtCore.SIGNAL("hovered(QAction *)"), self._actionHovered)

        # Connect the signals with the methods
        QtCore.QObject.connect(self.actionOpen_Ticket,
            QtCore.SIGNAL("triggered()"),
            self.openTicket)
        QtCore.QObject.connect(self.actionSave_Regex,
            QtCore.SIGNAL("triggered()"),
            self.saveRegexFile)
        QtCore.QObject.connect(self.copyautoregex,
            QtCore.SIGNAL("clicked()"),
            self.copyautoregexfunction)
        QtCore.QObject.connect(self.actionNew_Regex_File,
            QtCore.SIGNAL("triggered()"),
            self.newregexfile)
        QtCore.QObject.connect(self.actionExit,
            QtCore.SIGNAL("triggered()"),
            QtGui.qApp, QtCore.SLOT('quit()'))
        QtCore.QObject.connect(self.actionOpen_Regex_File,
            QtCore.SIGNAL("triggered()"),
            self.openRegexTemplate)
        QtCore.QObject.connect(self.dotall,
            QtCore.SIGNAL("clicked()"),
            self.highlightregex)
        QtCore.QObject.connect(self.ignorecase,
            QtCore.SIGNAL("clicked()"),
            self.highlightregex)

        QtCore.QObject.connect(self.restoreauto,
            QtCore.SIGNAL("clicked()"),
            self.restoreeditedautoregex)

        QtCore.QObject.connect(self.tickettext,
            QtCore.SIGNAL("cursorPositionChanged()"),
            self.getlineautoregex)

        QtCore.QObject.connect(self.highlightautoregex,
            QtCore.SIGNAL("clicked()"),
            self.highlightregex)

        QtCore.QObject.connect(self.userregex,
            QtCore.SIGNAL("textChanged(QString)"),
            self.highlightregex)
        QtCore.QObject.connect(self.autoregex,
            QtCore.SIGNAL("textChanged(QString)"),
            self.autoregexhighlight)

        QtCore.QObject.connect(self.saveregex,
            QtCore.SIGNAL("clicked()"),
            self.saveregexdict)
        QtCore.QObject.connect(self.dbfieldlist,
            QtCore.SIGNAL("itemSelectionChanged()"),
            self.loadregexfromdict)

        QtCore.QObject.connect(self.filecombobox,
            QtCore.SIGNAL("currentIndexChanged(int)"),
            self.updateworkingdoc)

        QtCore.QObject.connect(self.actionHelpAbout,
            QtCore.SIGNAL("triggered()"),
            self.helpAbout)

        QtCore.QObject.connect(self.actionHelp,
            QtCore.SIGNAL("triggered()"),
            self.helpHelp)

        #fix drag and move bug...
        for element in sorted(regexdict.keys()):
            self.dbfieldlist.addItem(element)
        self.matcher = None
        self.dbfieldlist.setCurrentRow(0)
        self.newregexfile()

    def _actionHovered(self, action):
        tip = action.toolTip()
        QtGui.QToolTip.showText(QtGui.QCursor.pos(), tip)

    def currentitem(self):
        return str(self.dbfieldlist.item(self.dbfieldlist.currentRow()).text())

    def copyautoregexfunction(self):
        self.userregex.setText(self.autoregex.text())

    def restoreeditedautoregex(self):
        self.autoregex.setText(self.editedautoregex)

    def newregexfile(self):
        self.dbfieldlist.setEnabled(True)
        self.userregex.setEnabled(True)
        importeddict = dict()
        self.inherits = "BaseParser"
        self.userregex.setText(str())
        self.saveregex.setEnabled(False)
        for key in regexdict.keys():
            regexdict[key] = None

    def loadregexfromdict(self):
        ci = self.currentitem()
        self.userregex.setText(str())
        self.regexlabel.setText("re_"+ci)
        self.highlightautoregex.setChecked(True)
        if regexdict[ci] is not None:
            self.userregex.setText(regexdict[ci].pattern)
            self.dotall.setChecked((regexdict[ci].flags & re.S) == re.S)
            self.ignorecase.setChecked((regexdict[ci].flags & re.I) == re.I)
            self.highlightregex()
        elif ci in importeddict.keys():
            self.userregex.setText(importeddict[ci].pattern)
            self.dotall.setChecked((importeddict[ci].flags & re.S) == re.S)
            self.ignorecase.setChecked((importeddict[ci].flags & re.I) == re.I)
            self.highlightregex()
        else:
            self.dotall.setChecked(False)
            self.ignorecase.setChecked(False)

    def saveregexdict(self):
        v = re.M
        if self.dotall.isChecked():
            v = v | re.S
        if self.ignorecase.isChecked():
            v = v | re.I
        regexdict[self.currentitem()] = re.compile(
            str(self.userregex.text()), v)

    def updateworkingdoc(self):
        self.workingdoc = filedict[str(self.filecombobox.currentText())]
        self.tickettext.setText(self.workingdoc)
        self.workingdoc = str(self.tickettext.toPlainText())
        self.highlightregex()

    def autoregexhighlight(self):
        if self.highlightautoregex.isChecked():
            self.highlightregex()

    def getlineautoregex(self):
        if self.similarlines == [None]:
            return False
        if len(filedict.keys()) > 1:
            self.editedautoregex = self.autoregex.text()
            cursor = self.tickettext.textCursor()
            cursor.select(QTextCursor.LineUnderCursor)
            start = int(cursor.selectionStart())
            fin = int(cursor.selectionEnd())
            selectedline = self.workingdoc[start:fin].strip()
            for element in self.similarlines:
                if selectedline in element:
                    dynreg = findre.dynamicregex(element)
                    if dynreg != None:
                        self.autoregex.setText(dynreg)
                        self.highlightregex()
                        return True
                    return False
            return False

    def highlightregex(self):
        self.matcher = Matcher(None,valid=False)
        fullmatches = list()
        substringmatches = list()
        if self.highlightautoregex.isChecked():
            pickedregex = self.autoregex.text()
        else:
            pickedregex = self.userregex.text()
        if pickedregex == '':
            self.erroroutput.setText("Please enter an expression.")
            return None
        #([\d]+).*([aeiou]+).*([aeiou^]+)
        #Ticket (Number):
        cursor = self.tickettext.textCursor()
        cursor.setPosition(0, QTextCursor.MoveAnchor)
        cursor.setPosition(len(self.workingdoc), QTextCursor.KeepAnchor)
        cursor.mergeCharFormat(self.nothingformat)
        try:
            v = re.M
            if self.ignorecase.isChecked():
                v = v | re.I
            if self.dotall.isChecked():
                v = v | re.S
            self.matcher.match = re.compile(str(pickedregex),v)
            for ma in self.matcher.match.finditer(self.workingdoc):
                fullmatches.append((ma.start(), ma.end()))
                for index in xrange(len(ma.groups())):
                    substringmatches.append((ma.start(index+1),
                        ma.end(index+1)))
            #Make it not show 1 on empty things
            self.erroroutput.setText("Matches found: %d" % (len(fullmatches),))
            for begin, end in fullmatches:
                cursor.setPosition(begin, QTextCursor.MoveAnchor)
                cursor.setPosition(end, QTextCursor.KeepAnchor)
                cursor.mergeCharFormat(self.fullmatchformat)

            for begin, end in substringmatches:
                cursor.setPosition(begin, QTextCursor.MoveAnchor)
                cursor.setPosition(end, QTextCursor.KeepAnchor)
                cursor.mergeCharFormat(self.substringformat)
        except sre_constants.error:
            self.erroroutput.setText("Mal-formed regular expression.")
        except:
            self.erroroutput.setText("A non-regex error has occured.")
        else:
            self.matcher.valid = True
        self.saveregex.setEnabled(self.matcher.valid)

    def openRegexTemplate(self):
        fn = QtGui.QFileDialog.getOpenFileName(self.centralwidget,
            "Open Regex Template", str(), "Python Files (*.py)")
        if not fn.isEmpty():
            QtGui.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
            f = str(fn)
            importdir = os.path.dirname(f)
            mod_name,file_ext = os.path.splitext(os.path.split(f)[-1])
            # Assume that any imports live in the same directory
            sys.path.append(importdir)
            pclass = imp.load_source(mod_name, f)
            if "Parser" not in dir(pclass):
                QMessageBox.critical(self.centralwidget, "Error",
                    "Selected parser does not have a Parser class defined.")
                QtGui.QApplication.restoreOverrideCursor()
                return

            for (name, value) in inspect.getmembers(pclass.Parser):
                if name.startswith("re_"):
                    importeddict[name[3:]] = value
            self.dbfieldlist.setEnabled(True)
            self.userregex.setEnabled(True)
            self.dbfieldlist.setCurrentRow(0)
            self.loadregexfromdict()
            QtGui.QApplication.restoreOverrideCursor()

    def clearOpenTickets(self):
        filedict = dict()
        self.filecombobox.clear()

    def openTicket(self):
        fn = QtGui.QFileDialog.getOpenFileNames(self.centralwidget,
            "Open Ticket File(s)", sample_dir, "Text files (*.txt)")
        if len(fn) > 0:
            QtGui.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
            self.clearOpenTickets()
            self.similarlines = [None]
            filedict.clear()
            fn = [str(x) for x in fn]
            for string in fn:
                fname = str(string)
                f = open(fname)
                filedict[fname]= f.read()
                f.close()
                self.filecombobox.addItem(fname)
            self.workingdoc = filedict[filedict.keys()[0]]
            self.tickettext.setText(self.workingdoc)
            self.similarlines = list(lateralregex.commonlines(fn))
            self.updateworkingdoc()
            QtGui.QApplication.restoreOverrideCursor()

    def humanflags(self, number):
        buffer = str()
        reflagnames = {
            re.M: "re.MULTILINE",
            re.I: "re.IGNORECASE",
            re.L: "re.LOCALE",
            re.S: "re.DOTALL",
            re.U: "re.UNICODE",
            re.X: "re.VERBOSE"
            }
        for element in reflagnames.keys():
            if number & element > 0:
                if buffer != str():
                    buffer += " | "
                buffer += reflagnames[element]
        return buffer

    def saveRegexFile(self, linefeed = '\r\n'):
        fn = QtGui.QFileDialog()
        #Y pq no funciona?
        fn.setConfirmOverwrite(False)
        fn = fn.getSaveFileName(self.centralwidget,
            "Save regular expressions", "*.py", "Python File (*.py)")
        L = lambda content, indent = 1, newline = linefeed, tab = 4: \
            indent * tab * ' ' + content + newline
        if fn.isEmpty():
            return False
        elif os.path.lexists(str(fn)):
            QMessageBox.warning(self.centralwidget, "Not permitted",
                "Overwriting of previous templates is not permitted.")
            return False
        fn = str(fn)
        if not(fn.lower().endswith('.py')):
            fn += '.py'
        f = open(str(fn),'w')
        buffer = str()
        for line in filetemplate.split('\n'):
            if "%%regexstatements%%" in line:
                for key in sorted(regexdict.keys()):
                    value = regexdict[key]
                    if value is not None:
                        expression = value.pattern
                        hrflags = self.humanflags(value.flags)
                        code = 're_%s = R("%s",%s)' % (key,expression,hrflags)
                        buffer += L(code)
            elif "%%summary_parser%%" in line:
                buffer += L("pass")
            elif "%%inherits%%" in line:
                buffer += L(line.replace("%%inherits%%",self.inherits), 0)
            else:
                buffer += L(line, 0)
        f.write(buffer + L(str(),0))
        f.close()
        return True

    def helpAbout(self):
        QtGui.QMessageBox().about(self.editorwindow,
                self.tr("About Regex Editor"),
                self.tr("""<b>Regex Editor</b> v %1
                <p>Copyright &copy; 2009 Oasis Digital
                All rights reserved.
                <p>This application can be used to generate
                regular expressions from multiple ticket files
                to assist in developing or modifying ticket parsers.
                <p>Python %2 - Qt %3 - PyQt %4
                on %5""").arg(versionInfo) \
                .arg(platform.python_version()).arg(QtCore.QT_VERSION_STR) \
                .arg(QtCore.PYQT_VERSION_STR).arg(platform.system()))

    def helpHelp(self):
        form = helpform.HelpForm("index.html", self.editorwindow)
        form.show()

def main(args):
    app=QtGui.QApplication(args)
    editorwindow = QtGui.QMainWindow()
    ui=EditorApplication(editorwindow)
    editorwindow.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main(sys.argv)
