import re

def regexescape(string, additional = ''):
    blacklist = "\\.^$*+?{}()[]" + additional
    for character in blacklist:
        string = string.replace(character, "\\"+character)
    return string

def splitplus(instr, additional):
    for character in additional:
        instr = instr.replace(character, ' ')
    return instr.split()

def dynamicregex(subjects, separate='', dontmatchsep = True):
    '''
    The heart of the dynamic regex generation
    Begin a list of segments that are common to each subject initially
    populating it with the words from the first list as a starting point and
    remove anything from that list not present in the other sets. We will
    assume that all universal elements can be used as field indicators.
    '''

    base = subjects[0]
    sharedelements = [regexescape(e) for e in splitplus(base.lower(),separate)]
    set = [sharedelements]
    for element in subjects[1:]:
        division = [regexescape(e) for e in splitplus(element.lower(),separate)]
        set.append(division)
        continuitycheck = 0
        poplist = list()
        for n in xrange(len(sharedelements)):
            if sharedelements[n] not in division:
                poplist.append(n)
            else:
                if continuitycheck > 0:
                    try:
                        pos = division.index(sharedelements[n],continuitycheck - 1)
                    except:
                        return False #This is a band aid patch; exception fail need not be here probably
                else:
                    pos = division.index(sharedelements[n])
                if pos >= continuitycheck:
                    continuitycheck = pos
                else:
                    '''
                    Elements are not continuous throughout the sets; all
                    elements must be in the same order throughout each of
                    the sets.
                    '''
                    return False
        poplist.sort()
        accumulator = 0
        for n in poplist:
            sharedelements.pop(n - accumulator)
            accumulator += 1

    '''
    Now that we have established what we can not ignore, we will use the
    first subject as a template for generating the regular expression.

    Dashes only matter when inside brackets [] which is why they are only
    escaped from this point on.
    '''
    template = splitplus(regexescape(subjects[0].lower()), separate)
    regex = ""
    if dontmatchsep and separate != str():
        variable = r"([^."+regexescape(separate, "-")+"]*)"
    else:
        variable = r"(.*)"
    capturingvariable = False
    templatelength = len(template)
    for element in xrange(templatelength):
        if template[element] in sharedelements:
            if element != 0:
                regex += r"[\s"+regexescape(separate, "-")+"]+"
            regex += template[element]
            capturingvariable = False
        elif not capturingvariable:
            regex += r"[\s"+regexescape(separate, "-" )+"]+"+variable
            capturingvariable = True

    return regex + ""
