'''
In GUI, make it so that when you click on a line, it brings up
generated regex for that line and then allows you to select from a
list of the regex and a list of the db fields to assign a regex to
that field.

In line with above, possibly include function that will accept a line
number as input then automatically diff that against other files to
generate a regex just for that line. In the GUI, maybe we could
set a database in which to automatically pull tickets from, maybe the
directory the ticket itself was in so that multiple files need not
be opened and accesible in order to generate a regular expression.

Could make a subisddiary by of common lines by precomputing all
common lines and caching them and force lateralregex to be a class
instead of a lose collection of functions. Say for example
line 91 was picked from file Japan42.txt. We would cycle through the
list of precomputed common lines using linesfromfile[91-1] in linelist
and when it's found, just use that set of lines to generate a
dynamic regular expression

Don't forget to implement multi ticket viewing in editor GUI
'''
import difflib
import findre
import cStringIO
import logging
logger = logging.getLogger("editor.lateralregex")

class MyDiffer(difflib.Differ):
    def __init__(self):
        difflib.Differ.__init__(self)
    def _fancy_replace(self, a, alo, ahi, b, blo, bhi):

        # don't synch up unless the lines have a similarity score of at
        # least cutoff; best_ratio tracks the best score seen so far
        best_ratio, cutoff = 0.72, 0.74
        cruncher = difflib.SequenceMatcher(self.charjunk)
        eqi, eqj = None, None   # 1st indices of equal lines (if any)

        # search for the pair that matches best without being identical
        # (identical lines must be junk lines, & we don't want to synch up
        # on junk -- unless we have to)
        for j in xrange(blo, bhi):
            bj = b[j]
            cruncher.set_seq2(bj)
            for i in xrange(alo, ahi):
                ai = a[i]
                if ai == bj:
                    if eqi is None:
                        eqi, eqj = i, j
                    continue
                cruncher.set_seq1(ai)
                # computing similarity is expensive, so use the quick
                # upper bounds first -- have seen this speed up messy
                # compares by a factor of 3.
                # note that ratio() is only expensive to compute the first
                # time it's called on a sequence pair; the expensive part
                # of the computation is cached by cruncher
                if cruncher.real_quick_ratio() > best_ratio and \
                      cruncher.quick_ratio() > best_ratio and \
                      cruncher.ratio() > best_ratio:
                    best_ratio, best_i, best_j = cruncher.ratio(), i, j
        if best_ratio < cutoff:
            # no non-identical "pretty close" pair
            if eqi is None:
                # no identical pair either -- treat it as a straight replace
                for line in self._plain_replace(a, alo, ahi, b, blo, bhi):
                    yield line
                return
            # no close pair, but an identical pair -- synch up on that
            best_i, best_j, best_ratio = eqi, eqj, 1.0
        else:
            # there's a close pair, so forget the identical pair (if any)
            eqi = None

        # a[best_i] very similar to b[best_j]; eqi is None iff they're not
        # identical

        # pump out diffs from before the synch point
        for line in self._fancy_helper(a, alo, best_i, b, blo, best_j):
            yield line

        # do intraline marking on the synch pair
        aelt, belt = a[best_i], b[best_j]
        if eqi is None:
            # pump out a '-', '?', '+', '?' quad for the synched lines
            atags = btags = ""
            cruncher.set_seqs(aelt, belt)
            for tag, ai1, ai2, bj1, bj2 in cruncher.get_opcodes():
                la, lb = ai2 - ai1, bj2 - bj1
                if tag == 'replace':
                    atags += '^' * la
                    btags += '^' * lb
                elif tag == 'delete':
                    atags += '-' * la
                elif tag == 'insert':
                    btags += '+' * lb
                elif tag == 'equal':
                    atags += ' ' * la
                    btags += ' ' * lb
                else:
                    raise ValueError, 'unknown tag %r' % (tag,)
            for line in self._qformat(aelt, belt, atags, btags):
                yield line
        else:
            # the synch pair is identical
            yield '  ' + aelt

        # pump out diffs from after the synch point
        for line in self._fancy_helper(a, best_i+1, ahi, b, best_j+1, bhi):
            yield line

def ndiff(a, b):
    return MyDiffer().compare(a, b)

def commonlines(filenamelist, areactuallyfiles = True):
    '''
    Have noticed that if template file is missing a field or matches
    incorrectly, ruins everything else. Opt to select n templates and
    then calculate which has most common regex of 3 maybe. or maybe
    offer max factorial comparison.
    '''
    fileset = list()

    for file in filenamelist:
        if areactuallyfiles:
            openfile = open(file, 'rb')
        else:
            openfile = cStringIO.StringIO(file)
        fileset.append(openfile.readlines())
        openfile.close()


    template = fileset[0]
    similardict = dict()
    for filedata in fileset[1:]:
        difflist = list(ndiff(template, filedata))
        logger.debug("contents of difflist")
        for diff in difflist:
            logger.debug(diff)
        lendifflist = len(difflist)
        linenumber = 0
        while linenumber < lendifflist:
            if difflist[linenumber].startswith('?'):
                similardict.setdefault(difflist[linenumber - 1], list())
                similardict[difflist[linenumber - 1]].append(difflist[linenumber+1][2:].strip())
                linenumber += 2
            linenumber += 1
    returnset = list()
    for k in similardict.keys():
        similardict[k].append(k[2:].strip())
        returnset.append(similardict[k])
    return returnset
