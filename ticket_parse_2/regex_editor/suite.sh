export QMDATA=~/programming/regexgen/trunk/editor/ticket_parse_trunk/testdata/datadir
export PYTHONPATH=~/programming/ticket_parse_trunk

echo "Generating python code for .ui files..."
pyuic4 interface.ui -o qteditorwindow.py

echo "Running tests..."
nosetests --with-figleaf

echo "Generating HTML..."
figleaf2html

echo "Cleaning up..."
find -iname "*.pyc" -delete
find -iname "*.figleaf*" -delete

echo "Done."
