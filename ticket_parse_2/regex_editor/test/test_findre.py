import os, site, unittest
site.addsitedir(os.path.join('..'))

#Import classes and methods to test after this point.
from findre import regexescape, splitplus, dynamicregex
import re

class test_findre(unittest.TestCase):
    def setUp(self):
        pass

    def test_regexescape(self):
        testdata = r"\.^$*+?{}()[]ABX"
        expected = r"\\\.\^\$\*\+\?\{\}\(\)\[\]\A\B\X"
        self.assertEqual(regexescape(testdata, 'ABX'), expected)

    def test_splitplus(self):
        testdata = "James:Eric Pruitt-I.Are"
        expected = ["James","Eric","Pruitt","I.Are"]
        self.assertEqual(splitplus(testdata,":-"),expected)

    def test_dynamicregex(self):
        testdata = [
            "Name--: John  DOB: Never",
            "Name--: Kathy DOB: Who cares?",
            "Name--: Bobby DOB: 19XX" ]
        failingtestdata = [
            "Name--: John  DOB: Never",
            "Name--: Kathy DOB: Who cares?",
            "DOB:    19XX     Name--: Bobby" ]
        expected = [
            [("John ", "Never")],
            [("Kathy", "Who cares?")],
            [("Bobby", "19XX")] ]
        ex = dynamicregex(testdata)
        compiledre = re.compile(ex, re.M | re.I | re.S)
        for n in range(3):
            exb = compiledre.findall(testdata[n])
            self.assertEqual(exb, expected[n])
        self.assertFalse(dynamicregex(failingtestdata))

        testdata = [
            "Name--[John]  DOB:Never",
            "Name--[Kathy] DOB:Who cares?",
            "Name--[Bobby] DOB:19XX" ]
        expected = [
            [("John", "Never")],
            [("Kathy", "Who cares?")],
            [("Bobby", "19XX")] ]

        ex = dynamicregex(testdata, '-[]:', True)
        compiledre = re.compile(ex, re.M | re.I | re.S)
        for n in range(3):
            exb = compiledre.findall(testdata[n])
            self.assertEqual(exb, expected[n])

        testdata = [
            "Name--[John]  DOB:Never",
            "Name--[Kathy] DOB:Who cares?",
            "Name--[Bobby] DOB:19XX" ]
        expected = [
            [("John] ", "Never")],
            [("Kathy]", "Who cares?")],
            [("Bobby]", "19XX")] ]

        ex = dynamicregex(testdata, '-[]:', False)
        compiledre = re.compile(ex, re.M | re.I | re.S)
        for n in range(3):
            exb = compiledre.findall(testdata[n])
            self.assertEqual(exb, expected[n])

    def tearDown(self):
        pass

def suite():
    s = unittest.makeSuite(test_findre)
    return unittest.TestSuite([s])

if __name__ == "__main__":

    unittest.main()
