ICEBOX_DEVEL = True

import os
import site
site.addsitedir(os.path.join('..'))

import sys
from minimock import mock,restore
import unittest

from PyQt4 import QtCore, QtGui#, Qt as QtB
from PyQt4.QtGui import QTextCursor, QTextCharFormat, QBrush, QTextFormat, QColor, QFont
from PyQt4.QtCore import QString, Qt, QPoint
from PyQt4 import QtTest
from PyQt4.QtTest import QTest
import editor
import HTMLParser

try:
    qmdata = os.environ['QMDATA']
    path_to_ticketparse = os.path.split(os.path.split(qmdata)[0])[0]
    sys.path.append(path_to_ticketparse)
except:
    print "The environment variable QMDATA must be defined"
    sys.exit(-1)

testpath = '.'
faketicket = os.path.join(testpath, 'testdata.txt')
fakesaveandload = os.path.join(testpath, 'test.txt')
fakesavefile = os.path.join(testpath, 'thisissaved.txt')
testsavefile = os.path.join(testpath, 'faketestingmoduleforimport')
somefile = os.path.join(testpath, 'alreadyhere')
filetoimport = os.path.join(path_to_ticketparse, 'formats', 'Richmond2.py')

class FontParser(HTMLParser.HTMLParser):
    def __init__(self):
        HTMLParser.HTMLParser.__init__(self)
        self.marriage = list()
    def handle_starttag(self, tag, attributes):
        if tag == 'font':
            for name, value in attributes:
                if name == 'color':
                    self.colorbuffer = value
    def handle_data(self, givendata):
        self.databuffer = givendata
    def handle_endtag(self, tag):
        if tag == 'font':
            self.marriage.append((self.colorbuffer, self.databuffer))


class testEditor(unittest.TestCase):
    def setUp(self):
        app=QtGui.QApplication(sys.argv)
        editorwindow = QtGui.QMainWindow()
        self.ui=editor.EditorApplication(editorwindow)
        #editorwindow.show()
        self.samplere = r'0+(\d+)a.'#'([\d]+).*([aeiou]+).*([aeiou^]+)'
        self.testsample = "00213a\n0123\n231a\n00333A\n"
        self.expectedmarriage = {
            (True, True): [
                ('#0000ff', '00'),
                ('#ff0000', '213'),
                ('#0000ff', 'a'),

                ('#0000ff', '00'),
                ('#ff0000', '333'),
                ('#0000ff', 'A') ],
            (True, False): [ ],
            (False, True): [
                ('#0000ff', '00'),
                ('#ff0000', '213'),
                ('#0000ff', 'a') ],
            (False, False): [ ]
            }

        f = open(faketicket, 'wb')
        f.write(self.testsample)
        f.close()

    def tearDown(self):
        self.ui = None
        deletelist = [ fakesaveandload,
            faketicket,
            fakesavefile,
            testsavefile+'.py',
            somefile
            ]

        for file in deletelist:
            try:
                os.remove(file)
            except:
                pass

    def toSimpleHtml(self, editor):
        html = QString()
        black = QColor(Qt.black)
        block = editor.document().begin()
        while block.isValid():
            iterator = block.begin()
            while iterator != block.end():
                fragment = iterator.fragment()
                if fragment.isValid():
                    format = fragment.charFormat()
                    family = format.fontFamily()
                    color = format.foreground().color()
                    text = Qt.escape(fragment.text())
                    if format.verticalAlignment() == \
                       QTextCharFormat.AlignSubScript:
                        text = QString("<sub>%1</sub>").arg(text)
                    elif format.verticalAlignment() == \
                        QTextCharFormat.AlignSuperScript:
                        text = QString("<sup>%1</sup>").arg(text)
                    if format.fontUnderline():
                        text = QString("<u>%1</u>").arg(text)
                    if format.fontItalic():
                        text = QString("<i>%1</i>").arg(text)
                    if format.fontWeight() > QFont.Normal:
                        text = QString("<b>%1</b>").arg(text)
                    if format.fontStrikeOut():
                        text = QString("<s>%1</s>").arg(text)
                    if color != black or not family.isEmpty():
                        attribs = ""
                        if color != black:
                            attribs += ' color="%s"' % color.name()
                        if not family.isEmpty():
                            attribs += ' face="%s"' % family
                        text = QString("<font%1>%2</font>")\
                                       .arg(attribs).arg(text)
                    html += text
                iterator += 1
            block = block.next()
        return html

    def test_openTicket(self):
        test_items = ["item1", "item2", "item3"]
        fd = open(fakesaveandload,"wb")
        for test_item in test_items:
            fd.write(test_item + "\n")
        fd.close()
        mock("editor.QtGui.QFileDialog.getOpenFileNames", returns = [QtCore.QString("test.txt")])
        self.ui.openTicket()
        for test_item, rcvd_item in zip(test_items,self.ui.workingdoc.split()):
            self.assertEquals(test_item, rcvd_item)
        for test_item, rcvd_item in zip(test_items,str(self.ui.tickettext.toPlainText()).split()):
            self.assertEquals(test_item, rcvd_item)

    def test_regexerror(self):
        self.ui.userregex.setText("This is (mal]formed")
        self.ui.highlightregex()
        self.assertEquals(self.ui.erroroutput.text(),
            "Mal-formed regular expression.")

    def test_humanreadable(self):
        from re import I, L, M, S, U, X
        expect = [ "re.MULTILINE",
            "re.IGNORECASE",
            "re.LOCALE",
            "re.DOTALL",
            "re.UNICODE",
            "re.VERBOSE" ].sort()
        got = [x.strip() for x in (self.ui.humanflags(I|L|M|S|U|X)).split('|')]
        self.assertEquals(got.sort(), expect)

    def test_saveregexdict(self):
        mock("editor.EditorApplication.currentitem", returns = "item1")
        self.ui.dotall.setChecked(True)
        self.ui.ignorecase.setChecked(True)
        self.ui.userregex.setText("text")
        self.ui.saveregexdict()
        self.assertEquals(editor.regexdict["item1"].pattern, "text")
        self.assertEquals(editor.regexdict["item1"].flags, 26)


    def test_loadregexdict(self):
        import re
        mock("editor.EditorApplication.currentitem", returns = "item1")
        self.ui.dotall.setChecked(True)
        self.ui.ignorecase.setChecked(False)
        self.ui.userregex.setText("text")
        self.ui.saveregexdict()
        self.ui.loadregexfromdict()
        self.assertEquals(editor.regexdict["item1"].pattern,
            str(self.ui.userregex.text()))
        self.assertEquals((editor.regexdict["item1"].flags | re.I == 0),
            self.ui.ignorecase.isChecked())
        self.assertEquals((editor.regexdict["item1"].flags | re.S != 0),
            self.ui.dotall.isChecked())

        mock("editor.EditorApplication.currentitem", returns = "item2")
        editor.regexdict["item2"] = None
        self.ui.loadregexfromdict()
        self.assertEquals(str(), str(self.ui.userregex.text()))
        self.assertFalse(self.ui.ignorecase.isChecked())
        self.assertFalse(self.ui.dotall.isChecked())

        #self.ui.newregexfile()


    def test_openRegexTemplate(self):
        mock("editor.QtGui.QFileDialog.getOpenFileName",
            returns = QtCore.QString(filetoimport))
        self.ui.openRegexTemplate()
        self.assertEqual(26, len(editor.importeddict.keys()))

        #re_caller_phone needs to be defined in the file.
        #I assume I should be able to use restore to fix this? How?
        mock("editor.EditorApplication.currentitem", returns = "caller_phone")
        self.ui.dbfieldlist.setCurrentRow(0)
        self.ui.loadregexfromdict()
        self.assertEqual(str(self.ui.userregex.text()),'^Caller.*Phone: (.*)$')

    def test_highlightregex(self):
        p = (True, False)
        self.ui.userregex.setText(self.samplere)
        mock("editor.QtGui.QFileDialog.getOpenFileNames",
            returns = [QtCore.QString(faketicket)])
        self.ui.openTicket()
        for ignoresetting in p:
            for dotsetting in p:
                self.ui.ignorecase.setChecked(ignoresetting)
                self.ui.dotall.setChecked(dotsetting)
                self.ui.highlightregex()
                html = self.toSimpleHtml(self.ui.tickettext)
                f = FontParser()
                f.feed(str(html))
                f.close()
                self.assertEquals(
                    self.expectedmarriage[(ignoresetting, dotsetting)],
                    f.marriage)

    def test_dbfieldclick(self):
        '''
        Tests that the regexlabel is set to the correct value
        '''
        self.ui.dbfieldlist.setEnabled(True)
        self.ui.dbfieldlist.setCurrentRow(1)
        self.assertEquals(str(self.ui.regexlabel.text()),'re_con_type')
        self.assertFalse(self.ui.dotall.isChecked())
        self.assertFalse(self.ui.ignorecase.isChecked())

    def test_saveregex_btn(self):
        '''
        Tests saveregex btn
        '''
        self.ui.newregexfile()
        self.ui.dbfieldlist.setCurrentRow(1)
        self.ui.userregex.setText("abc")
        QTest.mouseClick(self.ui.saveregex,Qt.LeftButton)
        re_expr = editor.regexdict[self.ui.currentitem()]
        self.assertEquals(re_expr.pattern, 'abc')

    def test_getlineautoregexsetone(self):
        import lateralregex

        editor.filedict["A"] = "Name = Kitty XXXXXXXXXXXX\n"
        editor.filedict["B"] = "Name = Lauren XXXXXXXXXXXX\n"
        self.ui.filecombobox.addItem("A")
        self.ui.filecombobox.addItem("B")

        #Can't figure out how to use PyQt testing properly so we will move
        #the cursor manually and assume click signal detection works

        self.ui.tickettext.moveCursor(QTextCursor.Start)

        self.ui.similarlines = lateralregex.commonlines([editor.filedict["A"], editor.filedict["B"]],False)

        expectedbool = True
        returnedbool = self.ui.getlineautoregex()

        expectedre = "name[\\s]+=[\\s]+(.*)[\\s]+xxxxxxxxxxxx"
        returnedre = str(self.ui.autoregex.text())

        self.assertEqual(expectedbool, returnedbool)
        self.assertEqual(expectedre, returnedre)

        editor.filedict["A"] = "AAA"
        editor.filedict["B"] = "BBB"
        editor.filedict["C"] = "CCC"
        editor.filedict["D"] = "DDD"
        self.ui.filecombobox.addItem("C")
        self.ui.filecombobox.addItem("D")

        #Can't figure out how to use PyQt testing properly so we will move
        #the cursor manually and assume click signal detection works

        self.ui.tickettext.moveCursor(QTextCursor.Start)

        self.ui.similarlines = lateralregex.commonlines([
            editor.filedict["A"],
            editor.filedict["B"],
            editor.filedict["C"],
            editor.filedict["D"] ],False)

        expectedbool = False
        returnedbool = self.ui.getlineautoregex()

        self.assertEqual(expectedbool, returnedbool)


    def test_saveRegexFile(self):
        f = open(somefile,'w')
        f.write("stuff")
        f.close()

        mock("editor.QtGui.QMessageBox.warning",
            returns = None)

        self.ui.newregexfile()
        self.ui.userregex.setText(".?(\d+).?")
        self.ui.saveregexdict()
        mock("editor.QtGui.QFileDialog.getSaveFileName",
            returns = QtCore.QString(testsavefile))
        self.assertTrue(self.ui.saveRegexFile())
        site.addsitedir(os.path.dirname(testsavefile))
        exec("import "+os.path.splitext(os.path.basename(testsavefile))[0])

        self.ui.newregexfile()
        self.ui.userregex.setText(".?(\d+).?")
        self.ui.saveregexdict()
        mock("editor.QtGui.QFileDialog.getSaveFileName",
            returns = QtCore.QString(somefile))
        self.assertFalse(self.ui.saveRegexFile())

        self.ui.newregexfile()
        self.ui.userregex.setText(".?(\d+).?")
        self.ui.saveregexdict()
        #?
        #mock("editor.QtGui.QFileDialog.getSaveFileName().isEmpty()",
        #    returns = True)
        #self.assertFalse(self.ui.saveRegexFile())



if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(testEditor)
    unittest.TextTestRunner(verbosity=2).run(suite)

