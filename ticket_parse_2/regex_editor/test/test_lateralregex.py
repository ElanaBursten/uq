import os, site, unittest
site.addsitedir(os.path.join('..'))

#Import classes and methods to test after this point.

from lateralregex import commonlines

class test_testedmodule(unittest.TestCase):
    def setUp(self):
        pass

    def test_commonlines(self):
        #Embedding then writing files at run time avoids pathing errors.
        #May write a program to make this easier.
        testfileA = """
First: James      Last: Pruitt    Middle: E
Nationality: Japanese               Age: 17
        """

        testfileB = """
First: Charles    Last: McCrea    Middle: R
Nationality: European               Age: ??
        """

        testfileC = """
First: Martin     Last: King      Middle: L
Nationality: American               Age: XX
        """
        # Cut of part of McCreary because line isn't matched with the
        # other ones otherwise.
        expected = [
            ["First: James      Last: Pruitt    Middle: E\n",
             "First: Charles    Last: McCrea    Middle: R\n",
             "First: Martin     Last: King      Middle: L\n" ],
            ["Nationality: Japanese               Age: 17\n",
             "Nationality: European               Age: ??\n",
             "Nationality: American               Age: XX\n" ]]

        f = open('sample1.txt', 'w')
        f.writelines(testfileA)
        f.close()

        f = open('sample2.txt', 'w')
        f.writelines(testfileB)
        f.close()

        f = open('sample3.txt', 'w')
        f.writelines(testfileC)
        f.close()

        self.deletelist = ["sample1.txt", "sample2.txt", "sample3.txt"]

        result = commonlines([
            "sample1.txt", "sample2.txt", "sample3.txt"])

        resultset = list()
        for top in result:
            for bottom in top:
                resultset.append(bottom)

        exset = list()
        for top in result:
            for bottom in top:
                exset.append(bottom)

        exset.sort()
        resultset.sort()

        self.assertEqual(exset, resultset)

        result = commonlines([
            testfileA, testfileB, testfileC], areactuallyfiles = False)

        resultset = list()
        for top in result:
            for bottom in top:
                resultset.append(bottom)

        exset = list()
        for top in result:
            for bottom in top:
                exset.append(bottom)

        exset.sort()
        resultset.sort()

        self.assertEqual(exset, resultset)

    def tearDown(self):
        for file in self.deletelist:
            try:
                os.remove(file)
            except:
                pass

def suite():
    s = unittest.makeSuite(test_testedmodule)
    return unittest.TestSuite([s])

if __name__ == "__main__":

    unittest.main()
