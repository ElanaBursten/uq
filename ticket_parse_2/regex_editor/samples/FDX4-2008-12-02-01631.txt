

DIG-TESS Locate Request For AMP
----------------------------------------------------------------------------
Ticket Number:    083371789         Old Ticket:                         
Priority:         Emergency         By:               Yesenia G         
Source:           Voice             Hours Notice:     0                 
Type:             Emergency         Date:             12/2/2008 10:32:11 AM
Sequence:         2                                                     
Map Reference:                                                          

Company Information
----------------------------------------------------------------------------
STOWE'S INDEPENDENT SERVICES        Type:             Contractor        
1951 E CONTINENTAL                  Contact:          RODNEY WHITFIELD  
SOUTHLAKE, TX 76092                 Caller:           MARY FLORES       
Phone:            (817) 329-0909    Caller Phone:     (214) 558-2548    
Fax:              (817) 329-2241    Callback:         0800 - 1700       
Alt Contact:                                                            
Caller Email:     CALLER DID NOT PROVIDE EMAIL                          

Work Information
----------------------------------------------------------------------------
State:            TX                Work Date:        12/02/08 at 1045  
County:           TARRANT           Type:             WATER             
Place:            HALTOM CITY       Done For:         HALTOM OAKS APARTM
Street:           5151 BROADWAY AVE                                     
Intersection:     PARKER                                                
Nature of Work:   EMER-WATER MAIN REPAIR                                
Explosives:       No                Deeper Than 16":  Yes               
White Lined:      No                Duration:         02 DAYS           
Mapsco:           MAPSCO 50,Y                                           

Remarks
----------------------------------------------------------------------------
EMER-WATER MAIN REPAIR-CREW ON SITE-CUST W\O SVC-WATER IS VISIBLE-WORK WILL 
BE 500' FROM THE ENTRANCE BEHIND BLDG 2 ON THE N SIDE-                      


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
AMP   ATMOS-MIDTX-UTILIQUEST (METROPLEX)                No                  
FN2   ONCOR ELECTRIC DISTRIBUTION-SMP (FORT WORTH NOR...No                  
MAR   CHARTER COMMUNICATIONS                            No                  
S90   ATT/D = DISTRIBUTION CABLE (FORMERLY SBC)         No                  


Location
----------------------------------------------------------------------------
Latitude:         32.8113540548028  Longitude:        -97.2719646112363 
Second Latitude:  32.8096113446817  Second Longitude: -97.2699062921957

