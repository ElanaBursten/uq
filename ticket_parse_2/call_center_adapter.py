# call_center_adapter.py
# Adapt tickets of certain call centers.
# Currently used to sort out LQW1 tickets into LOR1/LWA1/LID1.

import call_centers

# API:
# An Adapter class must derive from BaseAdapter.  Only one method is expected
# to be defined: adapt(ticket), which works kind of like Ticket.post_process;
# it takes a parsed Ticket object and changes it in-place.

class BaseAdapter:
    def adapt(self, ticket):
        """ Change a ticket in-place. """

_cache = {}

def get_adapter(call_center):
    """ Return an Adapter *instance* for the given call center. """
    if _cache.has_key(call_center):
        return _cache[call_center]
    try:
        name = call_center + "Adapter"
        adapter_class = globals()[name]
        adapter = adapter_class()
    except KeyError:
        adapter = BaseAdapter()
    _cache[call_center] = adapter
    return adapter

#
# implementations per call center

class LQW1Adapter(BaseAdapter):
    def adapt(self, ticket):
        sn = ticket.serial_number or ""
        if sn.startswith("WOC") or sn.startswith("UULC") \
        or sn.startswith("NUNC") or sn.startswith("IEUCC"):
            ticket.ticket_format = "LWA1"
        elif sn.startswith("OOC"):
            ticket.ticket_format = "LOR1"
        elif sn.startswith("IDL"):
            ticket.ticket_format = "LID1"
        else:
            raise ValueError, "Could not adapt ticket"
        # Q: what if it doesn't match?

class LNG1Adapter(BaseAdapter):
    def adapt(self, ticket):
        state = ticket.work_state or ""
        if state == 'OR':
            ticket.ticket_format = 'LOR1'
        elif state == 'WA':
            ticket.ticket_format = 'LWA1'
        # XXX EVIL hackery; not sure who originally put this here :-)
        # this is not supposed to change with every ticket... :-/
        if call_centers._cc:
            call_centers._cc.ucc['LNG1'] = ticket.ticket_format
        # otherwise, it remains LNG1 (but this shouldn't happen really)

class LEM1Adapter(BaseAdapter):
    def adapt(self, ticket):
        state = ticket.work_state or ""
        if state == 'OR':
            ticket.ticket_format = 'LOR1'
        elif state == 'WA':
            ticket.ticket_format = 'LWA1'
        else:
            # otherwise, it remains LEM1
            pass

class LCC1Adapter(BaseAdapter):
    def adapt(self, ticket):
        state = ticket.work_state or ""
        if state == 'OR':
            ticket.ticket_format = 'LOR1'
        elif state == 'WA':
            ticket.ticket_format = 'LWA1'
        # otherwise, it remains LCC1 (but this shouldn't happen really)

