# add_sample_tickets.py

import gc; gc.enable()
import getopt
import glob
import os
import shutil
import sys
import tempfile
import traceback

# add parent directory to sys.path
whereami = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(whereami, ".."))

import config
import main
import ticket_db

base_path = os.path.abspath(os.path.join(whereami, '..', 'testdata'))

GEOCODE_CALL_CENTERS = ['NewJersey', 'NewJersey2', 'NewJersey6', 'NewJersey8'] 

WORK_ORDERS = [
    ('LAM01', 'LAM01'),
]

TICKETS = [
    ('1102', '../AZ1102-1.txt'),
    ('1181', '1181-2011-03-23'),
    ('1393', '../AR1393-1.txt'),
    ('1421', '142x-2010-07-29'),
    ('1422', '1421-2012-03-09'),
    ('3003', '3003-2010-01-26'),
    ('3004', '3004-2010-01-26'),
    ('3004', '3004-2010-06-29'),
    ('3004', '3004-2011-08-agl'),
    ('3005', '3005-2011-01-19'),
    ('6001', '../TX6001-1.txt'),
    ('6002', '../TX6002-1.txt'),
    ('7501', '7501-2010-01-20'),
    ('9401', '../TN9401-1.txt'),
    ('Atlanta', '../atlanta-1.txt'),
    ('Atlanta', '../atlanta-2.txt'),
    ('Atlanta', '../atlanta-3.txt'),
    ('FAU4', 'FAU4-2010-01-26'),
    ('FCO1', '../Colorado-1.txt'),
    ('FCT3', 'FCT3-2010-05-19'),
    ('FCT3', 'FCT3-2010-07-01'),
    ('FDE2', 'FDE2-2011-02-25-due-date'),
    ('FDX1', 'FDX-2010-12-15-work-cross'),
    ('FMB1', 'FMB1-2011-06'),
    ('FMB2', 'FMB2-2011-04-22'),
    ('FMW1', 'FMW1-2011-04-22'),
    ('FMW1', 'FMW1-2011-06'),
    ('FMW1', 'FMW1-2011-changed-for-work-order-testing'),
    ('FMW3', 'FMW3-2011-02-25-due-date'),
    ('FPL2', 'FPL2-2011-04-29'),
    ('FTL1', '../Tennessee-1.txt'),
    ('FWP2', '../Pennsylvania-1.txt'),
    ('LAK1', '../Alaska-1.txt'),
    ('LNG1', 'LNG1-2010-12-16'),
    ('LOR1', '../Oregon2-1.txt'),
    ('LWA1', '../WashingtonState-1.txt'),
    ('NCA1', 'NCA1-2010-03-26'),
    ('NCA2', 'NCA2-2010-04-30'),
    ('NCA2', 'NCA2-2010-07-01-remarks'),
    ('NCA3', 'NCA3-2012-04-09'),
    ('NewJersey2', 'NewJersey2-2010-05-28'),
    ('NewJersey2', 'NewJersey2-2011-07-05-emer'),
    ('NewJersey2', 'NewJersey2-2012-02-24-dups'),
    ('NewJersey4', 'NewJersey4-2010-03-15'),
    ('NewJersey5', 'NewJersey5-2010-03-15'),
    ('NewJersey', 'NewJersey-2010-02-15'),
    ('NewJersey', 'NewJersey-2011-05-11-work-desc'),
    ('NewJersey', 'NewJersey-2011-07-expiration'),
    ('NewJersey', 'NewJersey-2012-02-24-dups'),
    ('OCC1', '../fairfax-1.txt'),
    ('OCC2', 'FairfaxXML-2010-05-06'),
    ('OCC2', 'OCC2-2011-02-25-due-date'),
    ('OCC2', 'OCC2-2010-01-13-url'),
    ('OCC3', 'OCC3-2010-05-18'),
    ('OCC4', 'OCC4-2011-09-30'),
    ('OCC4', 'OCC4-2011-12-21-xml'),
    ('SCA1', 'SCA1-2011-01-02'),
    ('SCA5', 'SCA5-2011-12-20-cox'),
    ('SCA6', 'SCA6-2010-07-01-remarks'),
    ('SCA8', 'SCA8-2012-04-09'),
    ('SCA9', 'SCA9-2012-04-09'),
]

AUDITS = [
    ('1102', '../sum-AZ1102-1.txt'),
    ('1181', '1181-2011-03-26'),
    ('1181', '1181-2011-04-01'),
    ('1182', '1192-2012-02-25-suffix'),
    ('1421', '142x-2010-07-29'),
    ('1421', '1421-2012-03-09'),
    ('1422', '1421-2012-03-09'),
    ('3004', '3004-2010-01-13'),
    ('3004', '3004-2010-06-29'),
    ('3005', '3005-2011-01-19'),
    ('7501', '7501-2012-02-19'),
    ('Atlanta', 'sum-Atlanta.txt'),
    ('FAU4', 'FAU4-2010-03-19'),
    ('FAU5', 'FAU5-2010-01-13'),
    ('FCL1', '../sum-NorthCarolina.txt'),
    ('FCO1', '../sum-Colorado.txt'),
    ('FCV1', '../sum-Richmond3.txt'),
    ('FJL4', 'FJL4-2010-12-15'),
    ('FMB1', '../sum-Baltimore.txt'),
    ('FMB2', 'FMB2-2011-04-22'),
    ('FMW1', '../sum-Washington.txt'),
    ('FMW4', 'FMW4-2009-12-15'),
    ('FPL2', 'FPL2-2011-04-29'),
    ('FTL1', '../sum-Tennessee.txt'),
    ('FTL3', 'FTL3-2010-07-01'),
    ('FWP1', '../sum-WestVirginia.txt'),
    ('FWP2', '../sum-Pennsylvania.txt'),
    ('LAK1', '../sum-Alaska.txt'),
    ('LWA1', '../sum-WashingtonState.txt'),
    ('LWA2', '../sum-WashingtonState2.txt'),
    ('NCA1', '../sum-NorthCalifornia.txt'),
    ('NCA2', 'NCA2-2010-03-18'),
    ('NewJersey', 'NewJersey-2010-02-15'),
    ('NewJersey2', 'NewJersey2-2010-05-28'),
    ('NewJersey2', 'NewJersey2-2010-06-18'),
    ('NewJersey4', 'NewJersey4-2010-03-15'),
    ('NewJersey4', 'NewJersey4-2011-11-06'),
    ('OCC2', 'OCC2-2010-06-11'),
    ('OCC4', 'OCC4-2011-09-30'),
    ('SCA1', '../sum-SouthCalifornia.txt'),
    ('SCA3', 'SCA3-2011-11-06'),
    ('SCA6', 'SCA6-2010-05-05'),
]

directories = ['incoming', 'processed', 'error']

def create_directories():
    temp_dir = tempfile.mkdtemp()
    for dir in directories:
        path = os.path.join(temp_dir, dir)
        try:
            os.mkdir(path)
        except:
            pass
    return temp_dir
    
def remove_directories(temp_data_dir):
    for dir in directories:
        path = os.path.join(temp_data_dir, dir)
        files = glob.glob(os.path.join(path, '*.*'))
        for f in files:
            try:
                os.remove(f)
            except:
                pass
        os.rmdir(path)
    os.rmdir(temp_data_dir)
            
def add_tickets_for(temp_data_dir, call_center, dir, max_batch, type='tickets'):
    # update configuration for 'GROUP'
    cfg.processes = [{
        'attachment_dir': '',
        'attachment_proc_dir': '',
        'attachment_user': '',
        'attachments': 0,
        'error': os.path.join(temp_data_dir, 'error'),
        'format': call_center,
        'incoming': os.path.join(temp_data_dir, 'incoming'),
        'processed': os.path.join(temp_data_dir, 'processed'),
        'upload_location': '',
        'geocode_latlong': 1 if call_center in GEOCODE_CALL_CENTERS else 0
    }]

    # find names of files in dir
    try:
        src_path = os.path.join(base_path, type, dir)
        if dir.endswith('.txt'):
            filenames = [src_path] # old-style audit files
        else:
            filenames = [os.path.join(src_path, fn)
                         for fn in os.listdir(src_path)
                         if not fn.startswith(".") and fn.endswith(".txt")]
    except:
        traceback.print_exc()
        return

    print dir, len(filenames)
    filenames = filenames[:max_batch]

    # copy files to incoming directory
    print "Copying", len(filenames), "files..."
    for fn in filenames:
        dest = os.path.join(temp_data_dir, 'incoming', os.path.split(fn)[1])
        try:
            shutil.copyfile(fn, dest)
        except:
            traceback.print_exc()
    print "OK"

    m = main.Main(call_center=call_center, debug=1)
    m.log.lock = 1 # don't send emails
    m.run(runonce=1)

    gc.collect()

def add_sample_tickets(temp_data_dir, max_batch, work_orders=1, tickets=1, audits=1):
    if work_orders:
        for call_center, dir in WORK_ORDERS:
            add_tickets_for(temp_data_dir, call_center, dir, max_batch, 'work-orders')
    if tickets:
        for call_center, dir in TICKETS:
            add_tickets_for(temp_data_dir, call_center, dir, max_batch, 'tickets')
    if audits:
        for call_center, dir in AUDITS:
            add_tickets_for(temp_data_dir, call_center, dir, max_batch, 'audits')

if __name__ == "__main__":

    max_batch = 100
    work_orders = tickets = audits = 1
    wait = 0
    opts, args = getopt.getopt(sys.argv[1:], "an:w")
    for o, a in opts:
        if o == '-n':
            max_batch = int(a)
        elif o == '-a':
            work_orders = tickets = 0 # audits only
        elif o == '-w':
            wait = 1

    temp_data_dir = create_directories()
    
    # this creates a Configuration instance which will be reused by main.Main;
    # any changes we make to it will be "seen" by main.Main when it's created
    cfg = config.getConfiguration()

    add_sample_tickets(temp_data_dir, max_batch, work_orders=work_orders, tickets=tickets,
     audits=audits)

    tdb = ticket_db.TicketDB()

    print "\nNumber of work orders in the database:", tdb.count('work_order')
    numcc = tdb.runsql_result("""
     select wo_source from work_order group by wo_source
    """)
    print "...in", len(numcc), "call centers:",
    print sorted([x['wo_source'] for x in numcc])

    print "\nNumber of tickets in the database:", tdb.count('ticket')
    numcc = tdb.runsql_result("""
     select ticket_format from ticket group by ticket_format
    """)
    print "...in", len(numcc), "call centers:",
    print sorted([x['ticket_format'] for x in numcc])

    print "\nNumber of audits in the database:", tdb.count('summary_header')
    numaudits = tdb.runsql_result("""
     select call_center from summary_header group by call_center
    """)
    print "...in", len(numaudits), "call centers:",
    print sorted([x['call_center'] for x in numaudits])

    remove_directories(temp_data_dir)
    
    if wait:
        raw_input("Press Enter to quit...")

