# show_routing_methods.py
# Q&D script to show each (active) call center's routing methods.

import sys; sys.path.insert(0, '..')
import call_centers
import callcenters # package

if __name__ == "__main__":

    # simple display for testing purposes.

    import pprint
    import ticket_db
    tdb = ticket_db.TicketDB()
    cc = call_centers.CallCenters(tdb)
    print "Call centers:"
    cc_names = cc.get_all_call_centers()
    #pprint.pprint(cc_names)
    #print
    #print "Update call centers:"
    #pprint.pprint(cc.ucc)

    for cc_name in cc_names:
        if cc_name[0] in '0123456789': cc_name = 'c' + cc_name
        try:
            logic_class = callcenters.get_businesslogic(cc_name)
        except:
            logic_class = None
        if logic_class is None:
            continue
        logic = logic_class(tdb, [])
        loc_methods = logic.locator_methods({})
        loc_method_names = [f.__name__ for f in loc_methods]
        print cc_name, loc_method_names

