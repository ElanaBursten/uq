# convert_status_translations.py
# Populate the status_translations table with data from config.xml.

# add parent directory to sys.path
import os
import sys
import datetime
import copy

whereami = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(whereami, ".."))

import ticket_db
import datadir
import config
import dbengine
from status_translation import StatusTranslation
from transaction import Transaction

cfg = config.get_XML_configuration()
tdb = ticket_db.TicketDB()

# put everything in one transaction. either we post all the statuses, or none
# at all (in case of errors).

with Transaction(tdb.dbado):
    for cc, resp_type, resp_data in cfg.responderconfigdata.data:
        print "-- Processing:", cc, resp_type
        kind = resp_data.get('kind') or None
        # print cc, resp_type, kind, len(resp_data['mappings'])
        for key, value in resp_data['mappings'].items():
            print "Inserting:", key, value[0], value[1]
            st = StatusTranslation()
            st.set(cc_code=cc, status=key, outgoing_status=value[0], active=1,
                   explanation_code=value[1], responder_kind=kind,
                   responder_type=resp_type)

            #print st, st._data
            try:
                st.insert(tdb)
            except dbengine.DBException as e:
                print "msg:", e.msg
                #print "data:", e.data
                #print "args:", e.args
                if "INSERT statement conflicted with the FOREIGN KEY" in e.msg \
                        and "status_translation_fk_status" in e.msg \
                        and "column 'status'" in e.msg:
                    print "ERROR: Unknown status:", key
                    sys.exit(1)
                elif "INSERT statement conflicted with the FOREIGN KEY" in e.msg \
                        and "status_translation_fk_cc_code" in e.msg \
                        and "dbo.call_center" in e.msg:
                    print "ERROR: Unknown call center:", cc
                    sys.exit(1)
                raise
    XMLConfig = cfg
    DBcfg = copy.deepcopy(cfg)
    DBcfg.db = tdb.dbado
    DBcfg = config.update_config_from_db(DBcfg)
    msgs = cfg.compare_config(DBcfg)

    if len(msgs) > 0:
        err_msg = 'Conversion errors found. Statuses not saved in the database.'
        msgs.append('[' + str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")) + '] ' + err_msg + '\n')
        msgs.reverse()

        msg_filename = os.path.join(datadir.datadir.path,
                                    'status_conversion_errors.txt')
        with open(msg_filename, 'a') as f:
            f.write('\n' + '\n'.join(msgs))

        print '\n'.join(msgs)
        print
        print 'Errors were also saved in %s.' % msg_filename
        print
        raise Exception('Database transaction rolled back.')

    print "Importing done"
