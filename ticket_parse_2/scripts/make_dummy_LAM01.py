# make_dummy_LAM01.py
# Parse LAM01 tickets from testdata/work-orders/LAM01 and store them in the
# database.
# FOR TESTING PURPOSES ONLY.

import os
import sys
import traceback

# add parent directory to the path
whereami = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(whereami, ".."))

import datadir
import ticket_db
import work_order
from formats import Lambert
import ticketloader

TICKET_PATH = os.path.join(whereami, "..", "testdata", "work-orders", "LAM01")
filenames = [os.path.join(TICKET_PATH, fn) for fn in os.listdir(TICKET_PATH)
             if fn.endswith(".txt")]

tdb = ticket_db.TicketDB()
wop = Lambert.WorkOrderParser()

for fn in filenames:
    print "Parsing:", os.path.split(fn)[1]
    tl = ticketloader.TicketLoader(fn)
    raw = tl.tickets[0]
    wo = wop.parse(raw)
    print "Work order number:", wo.wo_number

    try:
        wo_id = wo.insert(tdb)
    except:
        traceback.print_exc()
    else:
        print "Inserted with wo_id:", wo_id

