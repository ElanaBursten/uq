# generate_tables.py
# Generate static table definitions, for use by classes like Ticket, Locate,
# etc. Definitions are extracted from database.

import pprint
#
import datadir
import dbinterface
import tabledefs

TABLES = ['ticket', 'locate', 'assignment', 'summary_header', 'summary_detail',
          'ticket_version', 'attachment', 'client', 'response_log',
          'alert_ticket_keyword', 'alert_ticket_type',
          'work_order', 'wo_audit_header', 'wo_audit_detail', 'wo_assignment',
          'work_order_version', 'ticket_hp', 'response_ack_context',
          'summary_fragment', 'status_translation']
# XXX some of these can be handled by TableDefs; remove those

OUTPUT = "static_tables.py"

HEADER = """\
# static_tables.py
# Static table definitions.
# XXX This file is auto-generated! DO NOT EDIT; run generate_tables.py instead.

from sqlmaptypes import *

"""

if __name__ == "__main__":

    db = dbinterface.detect_interface()
    td = tabledefs.TableDefs(db)
    f = open(OUTPUT, 'w')
    f.write(HEADER)
    for table in TABLES:
        print "Generating:", table
        print >> f, table, "= \\"
        d = td[table]
        pprint.pprint(d, f)
        print >> f

    f.close()
    print "OK"
