# locate.py

import assignment
import locate_status
import sqlmap
import sqlmaptypes as smt
import static_tables
import errorhandling2
import errorhandling_special

log = errorhandling_special.toplevel_logger(None, name="Locate",
      subject="Locate Notification")

class Locate(sqlmap.SQLMap):
    """
    #
    # Questions:
    # What will happen with _locator and _assignment?
    # They should be rolled into the object somehow... (tricky)
    # I think _locator is obsolete, and _assignment stays...
    """

    __table__ = "locate"
    __key__ = "locate_id"
    __fields__ = static_tables.locate

    def __init__(self, client="", seq_number=None, added_by=None):
        sqlmap.SQLMap.__init__(self)
        self.client_code = client    # code
        self.status = locate_status.blank
        #self.client_id = None   # will be saved if present
        #self.closed = None
        #self.closed_date = None
        #self.closed_how = None
        #self.locate_id = None
        self.seq_number = seq_number
        if added_by:
            self.added_by = added_by
        # this will be set if the Locate was created by UltraFile

        # the locator field will not be saved, but may be used to add an
        # assignment record
        #self._locator = None
        self._assignment = None  # will hold a locator, and more

        self._existing = 0
        # if this is set, the updateticket method will not update this locate
        # (because it already exists and has a locator).
        # XXX _existing will become obsolete?

    def __repr__(self): # added for convenience when printing Tickets
        return "Locate(%s)" % (self.client_code,)
    def __len__(self):  # added for ticketstats
        return len(self.client_code)
    def __eq__(self, other):
        if isinstance(other, Locate):
            return self.client_code == other.client_code \
               and self.status == other.status
        else:
            return False
    def __cmp__(self, other):
        # necessary for sorting
        assert isinstance(other, Locate)
        if isinstance(other, Locate):
            return cmp(self.client_code, other.client_code)
        else:
            return False
    def __le__(self, other):
        assert isinstance(other, Locate)
        return self.client_code < other.client_code

    def insert(self, tdb):
        assert self.ticket_id, "Locate: ticket_id not set"
        # insert locate stuff as usual.
        locate_id = sqlmap.SQLMap.insert(self, tdb)
        # if there's an assignment, make sure it's hooked up to the locate,
        # then insert or update it as well.
        # also, make sure assignment_id is empty (we're *inserting*)
        if self._assignment and self._assignment.assignment_id is None:
            self._assignment.assignment_id = None
        self.save_assignment(tdb, locate_id)
        return locate_id

    def update(self, tdb, force=0):
        assert self.ticket_id, "Locate: ticket_id not set"
        locate_id = sqlmap.SQLMap.update(self, tdb, force=force)
        self.save_assignment(tdb, locate_id)
        return locate_id

    def save_assignment(self, tdb, locate_id):
        a = self._assignment
        if a is not None:
            if a.locate_id != locate_id:
                a.locate_id = locate_id
            a.save(tdb)
            return a.assignment_id
        return None

    def choose_and_assign(self, assignments):
        """ Choose a valid assignment (if any) from a list, and assign it.
            ('assignments' is a list of dicts, *not* a list of Assignments.)
            A status code is returned:
            0 = everything OK
            1 = no assignments were provided to choose from
            2 = multiple assignments were provided
        """

        if not assignments:
            return 1
            # no assignments is not an error, it just doesn't do anything

        if len(assignments) > 1:
            # there shouldn't really be multiple active assignments, but
            # if there are, choose the most recent one
            ep = errorhandling2.errorpacket()
            ep.add(info="Multiple active assignments for the same locate id")
            kwargs = {}
            for i, a in enumerate(assignments):
                kwargs['assignment %d' % i] = repr(a)
            ep.add(**kwargs)
            log.log(ep, send=1, write=1)
            # Assuming that both dates are isodates
            cmpfunc = lambda a, b: cmp(a["insert_date"], b["insert_date"])
            row = sorted(assignments, cmpfunc)[-1]
            self._assignment = assignment.Assignment.load_from_row(row)
            return 2
        else:
            self._assignment = assignment.Assignment.load_from_row(
                               assignments[0])
            return 0

    @classmethod
    def load(cls, tdb, id):
        locate = super(Locate, cls).load(tdb, id)
        assignments = tdb.getrecords("assignment", locate_id=id, active=1,
                      insert_date='IS NOT NULL')
        locate.choose_and_assign(assignments)
        return locate

    @classmethod
    def load_from_data(cls, locaterow, assignments):
        locate = super(Locate, cls).load_from_row(locaterow)
        assignments = [a for a in assignments
                       if a['locate_id'] == locate.locate_id]
        # note that assignments is a list of dicts
        locate.choose_and_assign(assignments)
        return locate

    def clear_id(self):
        sqlmap.SQLMap.clear_id(self)
        if self._assignment:
            self._assignment.locate_id = None
            self._assignment.clear_id()

