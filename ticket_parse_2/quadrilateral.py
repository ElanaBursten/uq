# Bilinear interpolant functions over a quadrilateral is
# given by the isoparametric formulation
def phi_1(p,q):
    return 0.25*(1.0+p)*(1+q)

def phi_2(p,q):
    return 0.25*(1.0-p)*(1+q)

def phi_3(p,q):
    return 0.25*(1.0-p)*(1-q)

def phi_4(p,q):
    return 0.25*(1.0+p)*(1-q)

phi = [phi_1,phi_2,phi_3,phi_4]

def _myDet(p, q, r):
    """
    Calculate the determinant of a special matrix with three 2D points.
    The sign, "-" or "+", determines the side, right or left,
    respectivly, on which the point r lies, when measured against
    a directed vector from p to q.
    """

    # We use Sarrus' Rule to calculate the determinant.
    # (could also use the Numeric package...)
    sum1 = q[0]*r[1] + p[0]*q[1] + r[0]*p[1]
    sum2 = q[0]*p[1] + r[0]*q[1] + p[0]*r[1]

    return sum1 - sum2


def _isRightTurn((p, q, r)):
    "Do the vectors pq:qr form a right turn, or not?"

    assert p != q and q != r and p != r

    if _myDet(p, q, r) < 0:
        return 1
    else:
        return 0


def _isPointInPolygon(r, P):
    """
    Determines whether a point r is inside a given polygon P
    """

    # We assume the polygon is a clockwise oriented list of points
    for i in xrange(len(P[:-1])):
        p, q = P[i], P[i+1]
        if not _isRightTurn((p, q, r)):
            return 0 # Outside

    return 1 # inside

def convexHull(P):
    """
    Calculate the convex hull of a set of points.
    """

    # Remove any duplicates
    # If the hull has a duplicate point, it will be deleted
    unique = {}
    for p in P:
        unique[p] = 1

    points = unique.keys()
    points.sort()

    # Build upper half of the hull.
    upper = [points[0], points[1]]
    for p in points[2:]:
        upper.append(p)
        while len(upper) > 2 and not _isRightTurn(upper[-3:]):
            del upper[-2]

    # Build lower half of the hull.
    points.reverse()
    lower = [points[0], points[1]]
    for p in points[2:]:
        lower.append(p)
        while len(lower) > 2 and not _isRightTurn(lower[-3:]):
            del lower[-2]

    # Remove duplicates.
    del lower[0]
    del lower[-1]

    # Concatenate both halfs and return.
    return tuple(upper + lower)

class Quadrilateral(object):
    """
    Class to calculate properties of a quadrilateral
    """
    def __init__(self,points):
        """
        Constructor for quadrilateral
        points - a list of tuples of length 4 containing the
        vertices of the quadrilateral.
           A      B      C      D
        [(0,0), (1,0), (1,1), (0,1)]
        """
        if len(points) != 4:
            raise ValueError, "The points vector must have length 4"
        # Create the convex hull such that the points are oriented correctly
        points = convexHull(points)
        self.points = points
        self.x = [p[0] for p in self.points]
        self.y = [p[1] for p in self.points]

    def centroid(self):
        """
        Determine the centroid of a quadrilateral
        x(p,q) = x_1*phi_1(p,q) + ...
        Another approach is to use the formula for a non-self-intersecting
        polygon (see http://local.wasp.uwa.edu.au/~pbourke/geometry/polyarea/)
        """
        x_c = reduce(lambda x,y:x+y, [x*y(0.0,0.0) for x,y in zip(self.x,phi)])
        y_c = reduce(lambda x,y:x+y, [x*y(0.0,0.0) for x,y in zip(self.y,phi)])
        return x_c,y_c

class Polygon(object):
    """
    Class to calculate properties of a polygon
    """
    def __init__(self,points):
        """
        Constructor for polygon
        points - a list of tuples containing the
        vertices of the polygon.
           A      B      C      D ...
        [(0,0), (1,0), (1,1), (0,1)] ...
        """
        # Create the convex hull such that the points are oriented correctly
        points = convexHull(points)
        self.points = points
        self.x = [p[0] for p in self.points]
        self.y = [p[1] for p in self.points]
        self.Properties()

    def Properties(self):
        x = self.x
        y = self.y
        # Close polygon
        x.append(self.x[0])
        y.append(self.y[0])
        np = len(x)
        nr = range(np-1)
        s = [ x[i]*y[i+1] - x[i+1]*y[i] for i in nr]
        area = abs(sum(s)/2.)
        scx = [ (x[i] + x[i+1]) * (x[i]*y[i+1] - x[i+1]*y[i]) for i in nr]
        scy = [ (y[i] + y[i+1]) * (x[i]*y[i+1] - x[i+1]*y[i]) for i in nr]
        try:
            cx = (1/(6.*area)) * sum(scx)
            cy = (1/(6.*area)) * sum(scy)
        except:
            cx = mean(x)
            cy = mean(y)
        bb = (min(x),min(y),max(x),max(y))
        self.bb = bb
        self.area = area
        # Flip since this algorithm assumes opposite orientation
        self.centroid = (-cx,-cy)

def centroid_from_vertices(vertices):
    """
    Uses a list of up to 2 vertices, like:
    [(40.141307, -83.126712), (40.140854, -83.121380)]
    """
    # todo(dan) Fix >2 vertices, with duplicate coordinates
    """
    if len(vertices) == 4:
        return Quadrilateral(vertices).centroid()
    elif len(vertices) == 3:
        # todo(dan) review/fix this
        x4 = vertices[2][0]
        y4 = vertices[2][1] + 0.0000001 # hack to create 4 distinct points
        return Quadrilateral(vertices + [(x4, y4)]).centroid()
    """
    if len(vertices) == 2:
        return (vertices[0][0] + vertices[1][0]) / 2.0,\
               (vertices[0][1] + vertices[1][1]) / 2.0
    elif len(vertices) == 1:
        return vertices[0][0], vertices[0][1] 
    else:
        return 0.0, 0.0
