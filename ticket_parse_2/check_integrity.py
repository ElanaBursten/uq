# check_integrity.py
# Several (SQL) integrity checks for the database.

import getopt
import string
import sys
import traceback
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
#
import config
import date
import dbinterface_ado
import mail2

__usage__ = """\
check_integrity.py [options]

Check integrity of database by doing several introspective queries.

Options:
    -e      Send output as email to "administrators".
"""


class Tee:
    """ Ties multiple files, or file-like objects, together. """
    def __init__(self, *files):
        self.files = files
    def write(self, text):
        for file in self.files:
            file.write(text)
# hey, this needs to be moved to tools; we already have a Tee somewhere

class IntegrityChecker:
    """ Does a database integrity check by executing some queries. """

    def __init__(self, config, send_email=0):
        self.config = config
        self.conn = None
        self.admin_emails = self.config.ic_emails
        self.send_email = send_email

        self.dbado = dbinterface_ado.DBInterfaceADO()

    ###
    ### Integrity checks

    # These queries return 0 upon "failure", i.e. an unexpected result is
    # found.

    def check_assigned_locates(self):
        sql = """
         select * from locate
         where closed = 1 and assigned_to is not NULL
        """
        rs = self.dbado.runsql_result(sql)
        print len(rs), "records found (expected: 0)"
        return len(rs) == 0

    def match_assigned_to(self):
        sql = """
         select count(*) as c from locate
         where assigned_to is not NULL
        """
        rs = self.dbado.runsql_result(sql)
        count1 = int(rs[0]["c"])

        sql = """
         select count(distinct assignment.locate_id) as c
         from assignment
         inner join locate on assignment.locate_id = locate.locate_id
         where locate.closed = 0
         and assignment.active = 1
         and locate.active = 1
        """
        rs = self.dbado.runsql_result(sql)
        count2 = int(rs[0]["c"])
        print count1, "vs", count2, "(expected: equal)"
        return count1 == count2

    def check_duplicate_assignments(self):
        # step 1: get a relatively recent assignment_id
        sql = """
          select max(assignment_id) - 100000 as asgid from assignment
        """
        rs = self.dbado.runsql_result(sql)
        asgid = rs[0]['asgid'] or 0

        # step 2: check records >= this assignment_id
        sql = """
         select a1.locate_id, a1.assignment_id, a1.locator_id, a2.assignment_id,
         a2.locator_id
         from assignment a1 inner join assignment a2
         on a1.locate_id=a2.locate_id
         where a1.active=1 and a2.active=1
         and a1.assignment_id<a2.assignment_id
         and a1.assignment_id >= %s
         and a2.assignment_id >= %s
        """ % (asgid, asgid)
        rs = self.dbado.runsql_result(sql)

        print len(rs), "records found (expected: 0)"
        return len(rs) == 0

    def check_billing_rate(self):
        sql = """
         select count(*) as c from billing_rate
         where status = 'NC' and rate > 0
        """
        rs = self.dbado.runsql_result(sql)
        count = int(rs[0]["c"])
        print count, "records found (expected: 0)"
        return count == 0

    def check_client_ids(self):
        sql = """
         select locate_id, locate.ticket_id, client_id, client_code,
          locate.status, added_by, ticket_format
         from locate with (index(locate_modified_date))
         inner join ticket on locate.ticket_id = ticket.ticket_id
         where client_id is null
         and client_code is not null
         and locate.status not in ('-N', '-P')
         %s
        """
        yesterday = date.Date()
        yesterday.dec(2)
        yesterday = "and locate.modified_date >= '%s'" % (yesterday.isodate(),)
        rs2 = self.dbado.runsql_result(sql % (yesterday,))
        count = len(rs2)
        print count, "records found with no client_id in the last 48"\
         " hours (expected: 0)"

        return count == 0

    def check_assigned_to_1(self):
        sql = """
         select top 100 * from locate
         where closed = 1
         and assigned_to is not NULL
         and modified_date > getdate() - 2
        """
        rs = self.dbado.runsql_result(sql)
        count = len(rs)
        print count, "closed locates with assigned_to still set"

        return count == 0

    def check_assigned_to_2(self):
        sql = """
         select top 100 * from locate
         where closed = 0
         and assigned_to is NULL
         and status not in ('-R', '-P', '-N')
         and modified_date > getdate() - 2
        """
        rs = self.dbado.runsql_result(sql)
        count = len(rs)
        print count, "open locates where assigned_to is not set properly"

        return count == 0

    ###
    ### main

    def run(self):
        """ Do a typical integrity check run. """
        methods = [
            self.check_assigned_locates,
            self.match_assigned_to,
            self.check_duplicate_assignments,
            self.check_billing_rate,
            self.check_client_ids,
            self.check_assigned_to_1,
            self.check_assigned_to_2,
        ]

        oldstdout = sys.stdout
        oldstderr = sys.stderr
        failure_found = 0
        try:
            # redirect sys.stdout so we "log" everything printed to a string
            f = StringIO()
            sys.stdout = sys.stderr = Tee(f, sys.stdout)

            for method in methods:
                print "Executing:", method.__name__
                try:
                    ok = method()
                    if not ok:
                        failure_found = 1
                        print " ...failure"
                    else:
                        print " ...ok"
                except:
                    traceback.print_exc()
                    failure_found = 1

            if self.send_email:
                self.send_report(f.getvalue(), failure_found)
        finally:
            sys.stdout = oldstdout
            sys.stderr = oldstderr

    def send_report(self, body, failure=1):
        print "Sending email...",
        if failure:
            result = "[failure]"
        else:
            result = "[success]"
        mail2.sendmail(
         smtpinfo=self.config.smtp_accs[0],
         toaddrs=self.admin_emails,
         subject="Integrity check results " + result,
         body=body)
        print "OK"


if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "e?", ["data="])

    send_email = 0
    for o, a in opts:
        if o == "-e":
            send_email = 1
        elif o == "-?":
            print >> sys.stderr, __usage__
            sys.exit(0)
        #elif o == "--data":
        #    print "Data dir:", a

    cfg = config.getConfiguration()

    ic = IntegrityChecker(cfg, send_email=send_email)

    ic.run()

