# fix_due_dates.py

# NOTE: For now, this script assumes that legal_due_date == due_date.

import getopt
import string
import sys
#
import businesslogic
import call_centers
import config
import date
import date_special
import dbinterface_old as dbinterface
import ticket_db
import ticketparser
import tools

__usage__ = """\
fix_due_dates.py [options] startdate enddate

Fixes due dates (and legal due dates) between the given dates.

Options:
    -1      Only update one record.
    -f C    Only inspect tickets of call center C.
    -t      Test mode; do everything except actually updating records.
    -c F    Use configuration file F.
    -n N    Change bulksize to N. (This is the number of records queried at
            once.)
    -l file Log changes to file.
    -i id   Only process records with ticket_id >= id.
    -s      "Smart" processing (i.e. don't look for tickets that are DONE).
    -o      Only process "old" due dates (< 2000-01-01).
    -d      Process tickets even if they are DONE.
"""

class TicketFixer:

    def __init__(self, startdate, enddate, call_center="", configfile="",
     bulksize=50, testmode=0, start_id=0, only_one=0, smart=0, old=0,
     process_done=0, logfile="", verbose=1):
        if configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                 "Configuration already specified"
            self.config = config.Configuration(configfile)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()

        self.bulksize = bulksize
        self.startdate = startdate
        self.enddate = enddate
        self.testmode = testmode
        self.call_center = call_center
        self.only_one = only_one
        self.process_done = process_done
        self.smart = smart
        self.old = old # only process "old" due dates (< 2000)
        self.verbose = verbose
        if start_id:
            self.id = start_id
        else:
            self.id = 0

        self.tdb = ticket_db.TicketDB()
        self.holidays = date_special.read_holidays(self.tdb)

        self.logic = {} # cache for businesslogic instances
        self.updates = []   # we keep a list around for testing purposes

        if logfile:
            if self.verbose:
                self.log = tools.Tee(open(logfile, "a+w"))
            else:
                self.log = open(logfile, "a+w")
        else:
            if self.verbose:
                self.log = sys.stdout
            else:
                self.log = tools.NullWriter()

    def getlogic(self, call_center):
        try:
            return self.logic[call_center]
        except KeyError:
            logic = businesslogic.getbusinesslogic(call_center, self.tdb,
                    self.holidays)
            self.logic[call_center] = logic
            return logic

    def getrecords(self):
        """ Get the next N ticket records, where N == self.bulksize. """
        sql = """
         select top %s * from ticket (nolock)
         where transmit_date >= '%s' and transmit_date <= '%s'
         and ticket_id > %d
         %s
         order by ticket_id
        """
        if self.call_center:
            clause = " and ticket_format = '%s' " % (self.call_center)
        else:
            clause = "" # can be used to filter by call center
        if self.smart:
            clause += " and kind <> 'DONE' "
        if self.old:
            clause += " and due_date < '2000-01-01' "
        sql = sql % (self.bulksize, self.startdate, self.enddate, self.id,
              clause)
        print >> self.log, "Getting records...",
        results = self.tdb.runsql_result(sql)
        print >> self.log, "OK (%d records found)" % (len(results),)
        if results:
            self.id = int(results[-1]["ticket_id"])
        return results

    def process(self, results):
        """ Process the given records, by computing the new due_date and
            comparing it to the old one.  If the due_date in the table differs
            from the one we just computed, then we update that record with the
            new value.
        """
        updates = {}
        for row in results:
            print >> self.log, row["ticket_id"], row["ticket_format"], \
             row["ticket_number"],

            call_center = row["ticket_format"]
            old_due_date = row.get("due_date", "")
            print >> self.log, "[%s]" % (old_due_date,),
            if old_due_date:
                old_due_date = date.Date(old_due_date).isodate()
            logic = self.getlogic(call_center)
            if logic:
                due_date = logic.due_date(row) # compute new due_date
                if old_due_date != due_date:
                    print >> self.log, "->", `due_date`,

                    if self.smart:
                        if old_due_date[:10] == due_date[:10]:
                            print >> self.log, "skipped"
                        else:
                            updates[row["ticket_id"]] = due_date
                    else:
                        if row["kind"] == "DONE":
                            if self.old or self.process_done:
                                updates[row["ticket_id"]] = due_date
                                print >> self.log, "DONE but update anyway"
                            else:
                                print >> self.log, "DONE"
                                # do not mark for updating; it's done
                        else:
                            print >> self.log
                            updates[row["ticket_id"]] = due_date

                    if self.only_one:
                        print >> self.log
                        break   # we got one, that's enough

            print >> self.log

        if updates and not self.testmode:
            self.updates.extend(updates.items())
            self.updatetickets(updates)

        return len(updates)

    def updatetickets(self, updates):
        """ Update the tickets specified in the dict. """
        if not updates:
            return
        before = []
        after = []
        if self.only_one:   # only update one ticket
            u_items = updates.items()[:1]
        else:
            u_items = updates.items()

        for ticket_id, due_date in updates.items():
            sql = """
             update ticket
             set due_date = '%s', legal_due_date = '%s'
             where ticket_id = '%s'
            """ % (due_date, due_date, ticket_id)
            self.tdb.runsql(sql)

    def run(self):
        count = 0
        while 1:
            results = self.getrecords()
            if results:
                count += self.process(results)
            if len(results) < self.bulksize:
                break   # no more tickets
            if self.only_one and count > 0:
                print >> self.log, "Stopping after one ticket."
                break

        print >> self.log, count, "records",
        if self.testmode: print >> self.log, "would be",
        print >> self.log, "changed."


if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "c:n:f:t1i:l:?sod")

    configfile = ""
    bulksize = 50
    callcenter = ""
    testmode = 0
    only_one = 0
    old = 0
    start_id = 0
    process_done = 0
    logfile = ""
    smart = 0
    # "smart" here means, that we:
    # - won't retrieve tickets that are done (kind == DONE)
    # - won't fix or log tickets where the date parts of the old and new due
    #   date are the same

    try:
        startdate, enddate = args[:2]
    except ValueError:  # unpack list of wrong size
        print >> sys.stderr, __usage__
        sys.exit(1)

    for o, a in opts:
        if o == "-c":
            configfile = a
        elif o == "-n":
            bulksize = int(a)
        elif o == "-f":
            callcenter = string.upper(a)
            # accept 'fcl1' as input, but convert to 'FCL1', etc; all call
            # center names are uppercase
        elif o == "-t":
            testmode = 1
        elif o == "-1":
            only_one = 1
        elif o == "-i":
            start_id = int(a)
        elif o == "-l":
            logfile = a
        elif o == "-s":
            smart = 1
        elif o == "-?":
            print >> sys.stderr, __usage__
            sys.exit(0)
        elif o == "-o":
            old = 1
        elif o == "-d":
            process_done = 1

    fixer = TicketFixer(startdate, enddate, call_center=callcenter,
            configfile=configfile, bulksize=bulksize, testmode=testmode,
            start_id=start_id, only_one=only_one, smart=smart, old=old,
            process_done=process_done, logfile=logfile)
    fixer.run()

