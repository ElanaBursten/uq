# locatorcatalog.py
# Created: 2002.08.14 (split off from businesslogic.py)

import re
import string
import logging
#
import polygon_routing
import tools

# logging for debugging purposes
log_debug = logging.getLogger("ticketrouter.locatorcatalog")

class LocatorCatalog:
    """ A catalog of possible ways to find a locator. "Clients" can call
        methods from this catalog at will.

        All top-level or "public" methods should return a 2-tuple (locator,
        mapinfo). (Mapinfo will often be ignored, but some call centers need
        it.)
    """

    re_township = re.compile("T?(\d+[A-Z]+?)\s*R?(\d+[A-Z]+)\s*(\d+)?",
                  re.MULTILINE)
    re_township_x = re.compile("T?(\d+[A-Z]+?)\s*R?(\d+[A-Z]+)\s*(\d+[A-Z*]+)?",
                    re.MULTILINE)
    re_grid = re.compile("(\d*)([A-Z]*)(\d*)", re.MULTILINE)
    re_map_area_with_code = re.compile("[A-Z0-9]+ +\d+ +[A-Z]+\d+",
                            re.MULTILINE)
    re_grid_TN = re.compile("(\d+)([A-Z]+)")
    re_grid_1391 = re.compile("(\d\d\d)(\d\d)")
    re_qtrmin_long = re.compile("\d{12,13}[A-D]?")

    def __init__(self, tdb, cc_routing_list=[]):
        self.tdb = tdb
        self.cc_routing_list = cc_routing_list
        self.polygoncache = polygon_routing.PolygonRoutingCache()

    def map_qtrmin(self, row):
        """ Use map_page and the find_qtrmin stored proc.
            NOTE: Make sure the parser fills this field!!
            Example: 2821A8124A.
        """
        routingresult = tools.RoutingResult()
        map_page = row.get("map_page", "")
        map_page = string.replace(map_page, "*", "A")
        if map_page:
            # todo(dan) Should longitude be expanded to 6 chars?
            lat, longitude = map_page[:5], map_page[5:10]
            locator_info = self.tdb.find_qtrmin_2(lat, longitude,
                           row['ticket_format'])
            if locator_info:
                routingresult.locator = locator_info[0].get("emp_id", None)
                routingresult.area_id = locator_info[0].get("area_id", None)
        return routingresult

    def map_qtrmin_long(self, row):
        """ Like map_qtrmin, but uses a "long" notation, causing map_page to
            be split differently. """
        routingresult = tools.RoutingResult()
        map_page = row.get("map_page", "")
        if map_page and self.re_qtrmin_long.search(map_page):
            longd, latd = tools.qtrminlong2decimal(map_page)
            qtrmin = tools.decimal2grid(latd, -longd)
            lat, long = qtrmin[:5], qtrmin[5:]
            locator_info = self.tdb.find_qtrmin_2(lat, long, row['ticket_format'])
            if locator_info:
                routingresult.locator = locator_info[0].get("emp_id", None)
                routingresult.area_id = locator_info[0].get("area_id", None)
        return routingresult

    def map_latlong_qtrmin(self, row):
        """ Use lat and long, but convert to qtrmin format. """
        routingresult = tools.RoutingResult()
        lat = row.get("work_lat", 0)
        long = row.get("work_long", 0)
        if long and lat:
            latcode = tools.convertcoord(float(lat))
            longcode = tools.convertcoord(abs(float(long)))
            locator_info = self.tdb.find_qtrmin_2(latcode, longcode,
                           row['ticket_format'])
            if locator_info:
                routingresult.locator = locator_info[0].get("emp_id", None)
                routingresult.area_id = locator_info[0].get("area_id", None)
        return routingresult

    def munic(self, row, return_mapinfo=1):
        """ Get locator using the state/county/city method. """
        locator = area_id = None
        mapinfo = None
        locator_info = self.tdb.find_munic_area_locator_2(
                       row.get("work_state", ""), row.get("work_county", ""),
                       row.get("work_city", ""), row['ticket_format'])
        if locator_info:
            locator = locator_info[0].get("emp_id", None)
            area_id = locator_info[0].get("area_id", None)
            if return_mapinfo:
                mapinfo = "%s %s" % (
                          locator_info[0].get("map_id", ""),
                          locator_info[0].get("area_name", ""))
        return tools.RoutingResult(locator=locator, mapinfo=mapinfo,
               area_id=area_id)

    def munic_with_dummy_city(self, row):
        if not row["work_city"]:
            row["work_city"] = "---"
        return self.munic(row)

    def township(self, row, prepend_zero_values=None):
        d = self.township_extended(row, prepend_zero_values)
        if d:
            locator = d.get("emp_id", None)
            area_id = d.get("area_id", None)
            return tools.RoutingResult(locator=locator, area_id=area_id)
        else:
            return tools.RoutingResult()

    def township_extended(self, row, prepend_zero_values=None):
        """ Returns a dict with fields as returned by the find_township stored
            procedure. Does NOT return RoutingResult. """
        mapinfo = row.get("map_page", "")
        mapinfo = string.replace(mapinfo, "*", "")  # remove *s
        baseline = row.get("work_state", "")
        call_center = row.get("ticket_format", "")
        if mapinfo:
            for regex in (self.re_township_x, self.re_township):
                m = regex.search(mapinfo)
                if m and len(m.groups()) == 3:
                    # if there is no section, then the third item of the
                    # groups() tuple will be None
                    township, range, section = m.groups()
                    # prepend zeroes if necessary
                    if prepend_zero_values:
                        if section is None:
                            continue    # need complete info for this
                        len_township, len_range, len_section = prepend_zero_values
                        while len(township) < len_township:
                            township = '0' + township
                        while len(range) < len_range:
                            range = '0' + range
                        while len(section) < len_section:
                            section = '0' + section
                    results = self.tdb.find_township_baseline_2(baseline,
                              township, range, section, call_center)
                    if results:
                        return results[0]   # a dict
        return {}

    def map_area(self, row, code):
        """ For map_page values like "014J02". """
        if not row["map_page"]:
            return tools.RoutingResult()

        m = self.re_grid.search(row["map_page"])
        # note that this doesn't extract the *code* from map_page
        if m and len(m.groups()) == 3:
            pagenum = int(m.group(1) or 0)
            x = m.group(2)
            ystring = m.group(3) or "0"
            y = int(ystring[0:2])
            grid_info = self.tdb.find_map_area_locator(code, pagenum, x, y)
            if grid_info:
                locator = grid_info[0].get("emp_id", None)
                area_id = grid_info[0].get("area_id", None)
                return tools.RoutingResult(locator=locator, area_id=area_id)
        return tools.RoutingResult()

    def map_area_with_code(self, row, code=None):
        """ For map_page values like "STAF 011 K01". Code can be overridden. """
        map_page = row.get("map_page", "")
        if map_page:
            m = self.re_map_area_with_code.search(map_page)
            if not m:
                # invalid format
                return self.empty()

            try:
                _code, page, coords = string.split(map_page)
            except ValueError:
                return None
                # invalid map_page, locator cannot be determined
            if not code:
                code = _code
            page = int(page)    # "026" -> 26
            s = ""
            while coords and coords[-1] in "0123456789":
                s = coords[-1] + s
                coords = coords[:-1]
            x = coords
            y = int(s)

            locator_info = self.tdb.find_map_area_locator(code, page, x, y)
            if locator_info:
                locator = locator_info[0].get("emp_id", None)
                area_id = locator_info[0].get("area_id", None)
                return tools.RoutingResult(locator=locator, area_id=area_id)

        return self.empty()

    def map_grid_county_state(self, row):
        """ Use this when we need to look up the state and county to choose
            a map, then look at the page/grid on that map. """
        if row["map_page"]:
            cnty = row.get("work_county", "")
            st = row.get("work_state", "")
            if st == 'VA':
                # HACK to deal with an ugly call center bug
                if cnty == 'FREDERICKSBURG':
                    cnty = 'SPOTSYLVANIA'
                elif cnty == "PORTSMOUTH":
                    cnty = "CHESAPEAKE"

            ticket_code = self.tdb.find_county_code(st, cnty)
            if ticket_code:
                return self.map_area(row, ticket_code)
                # returns a RoutingResult, is OK

        return self.empty()

    def map_grid_county_state_TN(self, row):
        if row["map_page"]:
            county = row.get("work_county", "")
            state = row.get("work_state", "")
            ticket_code = self.tdb.find_county_code(state, county)
            if ticket_code:
                # this is an adaptation of map_area()
                m = self.re_grid_TN.search(row["map_page"])
                if m and len(m.groups()) == 2:
                    page, x, y = m.group(1), "1", m.group(2)
                    grid_info = self.tdb.find_map_area_locator(ticket_code,
                                page, x, y)
                    if grid_info:
                        locator = grid_info[0].get("emp_id", None)
                        area_id = grid_info[0].get("area_id", None)
                        return tools.RoutingResult(locator=locator,
                               area_id=area_id)

        return self.empty()

    def map_grid_county_state_1391(self, row):
        if row["map_page"]:
            county = row.get("work_county", "")
            state = row.get("work_state", "")
            ticket_code = self.tdb.find_county_code(state, county)
            if ticket_code:
                # this is an adaptation of map_area()
                m = self.re_grid_1391.search(row["map_page"])
                if m and len(m.groups()) == 2:
                    page, x, y = m.group(1), "1", m.group(2)
                    # remove leading zeroes
                    page = page.lstrip('0 ')
                    y = y.lstrip('0 ')
                    grid_info = self.tdb.find_map_area_locator(ticket_code,
                                page, x, y)
                    if grid_info:
                        locator = grid_info[0].get("emp_id", None)
                        area_id = grid_info[0].get("area_id", None)
                        return tools.RoutingResult(locator=locator,
                               area_id=area_id)

        return self.empty()

    #
    # map_grid_county_state, Texas

    re_grid_TX = re.compile("(\S+) (\d+)([A-Z]*),(\S+)")
    # examples: "MAPSCO 123,U"
    #           "MAPSCO 123A,U"

    def map_grid_county_state_TX(self, row, code=None):
        """ Special flavor of map page routing as used by TX call centers
            (FDX, 6001, etc).
        """
        log_debug.debug("map_page = %s work_county = %s code = %s" % (row["map_page"], row["work_county"], code))
        # If there is no work_county, bail
        if row["map_page"] and row["work_county"]:
            # this is an adaptation of map_area()
            m = self.re_grid_TX.search(row["map_page"])
            log_debug.debug("Tuples found by regex:")
            for match in m.groups():
                log_debug.debug("%s" % (match,))
            if m:
                log_debug.debug("Number of groups found: %d" % (len(m.groups())),)
            if m and len(m.groups()) == 4:
                if code:
                    ticket_code = code # use custom map name, e.g. "FHL2"
                else:
                    ticket_code = m.group(1) # e.g. MAPSCO

                log_debug.debug("ticket_code = %s" % (ticket_code,))

                # MAPSCO 456,U -> MAPSCO/456/U/1
                # MAPSCO 456A,U -> MAPSCO/456/U/A
                page, x, y = m.group(2), m.group(4), m.group(3) or "1"
                log_debug.debug("page = %s, x = %s, y = %s" % (page, str(x), str(y)))

                # pages are numerical, so ignore any non-digits
                page = filter(lambda c: c in "0123456789", page)
                log_debug.debug("page = %s" % (page,))

                grid_info = self.tdb.find_map_area_locator(ticket_code,
                            page, x, y, row["work_county"])
                if grid_info:
                    locator = grid_info[0].get("emp_id", None)
                    area_id = grid_info[0].get("area_id", None)
                    log_debug.debug("locator = %s, area_id = %s" % (locator, area_id))
                    return tools.RoutingResult(locator=locator,
                           area_id=area_id)

        return self.empty()

    def map_grid_county_state_TX_2(self, row, code=None):
        """ Like map_grid_county_state_TX, but assumes that the page "number"
            may also contain letters, so map page values like '456A' are
            valid, without having to resort to hackery. """
        if row["map_page"]:
            # this is an adaptation of map_area()
            m = self.re_grid_TX.search(row["map_page"])
            if m and len(m.groups()) == 4:
                if code:
                    ticket_code = code # use custom map name, e.g. "FHL2"
                else:
                    ticket_code = m.group(1) # e.g. MAPSCO

                # MAPSCO 456,U -> MAPSCO/456/U/1
                # MAPSCO 456A,U -> MAPSCO/456A/U/1
                page, x, y = m.group(2) + m.group(3), m.group(4), "1"

                # pages are numerical, so ignore any non-digits
                grid_info = self.tdb.find_map_area_locator_2(ticket_code,
                            page, x, y)
                if grid_info:
                    locator = grid_info[0].get("emp_id", None)
                    area_id = grid_info[0].get("area_id", None)
                    return tools.RoutingResult(locator=locator,
                           area_id=area_id)

        return self.empty()

    def grid_area(self, row):
        """ Use find_grid_area_locator to find locator and mapinfo, if any, and
            return them. """
        locator = area_id = None
        mapinfo = None
        longitude = row.get("work_long", 0)
        lat = row.get("work_lat", 0)
        if longitude and lat:
            locator_info = self.tdb.find_grid_area_locator(lat, longitude)
            if locator_info:
                locator = locator_info[0].get("emp_id", None)
                area_id = locator_info[0].get("area_id", None)
                #print "#", long, lat, "lead to", locator_info[0]
                mapinfo = "%(map_page_num)s %(x_part)s %(y_part)s" \
                 % locator_info[0]
        return tools.RoutingResult(locator=locator, mapinfo=mapinfo,
               area_id=area_id)

    # XXX OBSOLETE -- DO NOT EDIT
    _CA_routing_data = [
        ("AMADOR", "*"),
        ("EL DORADO", "*"),
        ("MARIPOSA", "*"),
        ("MERCED", "*"),
        ("TUOLUMNE", "*"),
        ("SACRAMENTO", "GALT"),
        ("SACRAMENTO", "GALT, CO AREA"),
        ("SACRAMENTO", "HERALD, CO AREA"),
        ("KERN", "BEAR VALLEY SPRINGS"),
        ("KERN", "BEAR VALLEY SPRINGS, CO AREA"),
        ("KERN", "BEAR VALLEY, CO AREA"),
        ("KERN", "BORON, CO AREA"),
        ("KERN", "CALIENTE, CO AREA"),
        ("KERN", "CALIFORNIA CITY"),
        ("KERN", "CALIFORNIA CITY, CO AREA"),
        ("KERN", "CANTIL, CO AREA"),
        ("KERN", "CUMMINGS VALLEY SCHOOL, CO A"),
        ("KERN", "DUSTIN ACRES, CO AREA"),
        ("KERN", "EDWARDS AIR FORCE BASE, CO A"),
        ("KERN", "EDWARDS, CO AREA"),
        ("KERN", "GOLDEN HIILS"),
        ("KERN", "GOLDEN HILLS ELEMENTARY SCHO"),
        ("KERN", "HART FLAT, CO AREA"),
        ("KERN", "HAVILAH, CO AREA"),
        ("KERN", "KEENE, CO AREA"),
        ("KERN", "LORAINE, CO AREA"),
        ("KERN", "MOJAVE, CO AREA"),
        ("KERN", "MONOLITH, CO AREA"),
        ("KERN", "NORTH EDWARDS, CO AREA"),
        ("KERN", "RIDGECREST"),
        ("KERN", "ROSAMOND, CO AREA"),
        ("KERN", "STALLION SPRINGS, CO AREA"),
        ("KERN", "TEHACHAPI"),
        ("KERN", "TEHACHAPI, CO AREA"),
        ("KERN", "TWIN OAKS, CO AREA"),
        ("KERN", "WALKER BASIN, CO AREA"),
        ("KERN", "WOODFORD, CO AREA"),
        ("STANISLAUS", "CERES"),
        ("STANISLAUS", "CERES, CO AREA"),
        ("STANISLAUS", "CROWS LANDING, CO AREA"),
        ("STANISLAUS", "DENAIR, CO AREA"),
        ("STANISLAUS", "EMPIRE, CO AREA"),
        ("STANISLAUS", "GRAYSON, CO AREA"),
        ("STANISLAUS", "HICKMAN, CO AREA"),
        ("STANISLAUS", "HUGHSON"),
        ("STANISLAUS", "HUGHSON, CO AREA"),
        ("STANISLAUS", "KEYES, CO AREA"),
        ("STANISLAUS", "KNIGHTS FERRY, CO AREA"),
        ("STANISLAUS", "LA GRANGE, CO AREA"),
        ("STANISLAUS", "MODESTO"),
        ("STANISLAUS", "MODESTO, CO AREA"),
        ("STANISLAUS", "MOUNTAIN VIEW, CO AREA"),
        ("STANISLAUS", "NEWMAN"),
        ("STANISLAUS", "NEWMAN, CO AREA"),
        ("STANISLAUS", "OAKDALE"),
        ("STANISLAUS", "OAKDALE, CO AREA"),
        ("STANISLAUS", "PATTERSON"),
        ("STANISLAUS", "PATTERSON, CO AREA"),
        ("STANISLAUS", "RIVERBANK"),
        ("STANISLAUS", "RIVERBANK, CO AREA"),
        ("STANISLAUS", "SALIDA, CO AREA"),
        ("STANISLAUS", "TURLOCK"),
        ("STANISLAUS", "TURLOCK, CO AREA"),
        ("STANISLAUS", "VERNALIS, CO AREA"),
        ("STANISLAUS", "WESTLEY, CO AREA"),
        ("CALAVERAS", "ANGELS CAMP, CO AREA"),
        ("CALAVERAS", "ANGELS CITY"),
        ("CALAVERAS", "ARNOLD, CO AREA"),
        ("CALAVERAS", "AVERY, CO AREA"),
        ("CALAVERAS", "BURSON, CO AREA"),
        ("CALAVERAS", "COPPER COVE VILLAGE, CO AREA"),
        ("CALAVERAS", "COPPEROPOLIS, CO AREA"),
        ("CALAVERAS", "DORRINGTON, CO AREA"),
        ("CALAVERAS", "DOUGLAS FLAT, CO AREA"),
        ("CALAVERAS", "GLENCOE, CO AREA"),
        ("CALAVERAS", "HATHAWAY PINES, CO AREA"),
        ("CALAVERAS", "JENNY LIND, CO AREA"),
        ("CALAVERAS", "MOKELUMNE HILL, CO AREA"),
        ("CALAVERAS", "MOUNTAIN RANCH, CO AREA"),
        ("CALAVERAS", "MURPHYS, CO AREA"),
        ("CALAVERAS", "RAIL ROAD FLAT, CO AREA"),
        ("CALAVERAS", "SAN ANDREAS, CO AREA"),
        ("CALAVERAS", "SHEEP RANCH, CO AREA"),
        ("CALAVERAS", "VALLECITO, CO AREA"),
        ("CALAVERAS", "VALLEY SPRINGS, CO AREA"),
        ("CALAVERAS", "WALLACE, CO AREA"),
        ("CALAVERAS", "WEST POINT, CO AREA"),
        ("CALAVERAS", "WILSEYVILLE, CO AREA"),
        ("SAN JOAQUIN", "APP 4 MI W/O TRACY"),
        ("SAN JOAQUIN", "BANTA, CO AREA"),
        ("SAN JOAQUIN", "ESCALON"),
        ("SAN JOAQUIN", "ESCALON, CO AREA"),
        ("SAN JOAQUIN", "GALT, CO AREA"),
        ("SAN JOAQUIN", "LAMMERSVILLE ELEMENTARY SCHO"),
        ("SAN JOAQUIN", "LOCKEFORD, CO AREA"),
        ("SAN JOAQUIN", "LODI"),
        ("SAN JOAQUIN", "LODI, CO AREA"),
        ("SAN JOAQUIN", "OAKDALE, CO AREA"),
        ("SAN JOAQUIN", "RIPON"),
        ("SAN JOAQUIN", "RIPON, CO AREA"),
        ("SAN JOAQUIN", "THORNTON, CO AREA"),
        ("SAN JOAQUIN", "TRACY"),
        ("SAN JOAQUIN", "TRACY, CO AREA"),
        ("SAN JOAQUIN", "VERNALIS, CO AREA"),
        ("SAN JOAQUIN", "VICTOR, CO AREA"),
        ("SAN JOAQUIN", "WOODBRIDGE, CO AREA"),
    ]

    def _CA_special_routing(self, row):
        client_code = row.get("client_code", "")
        if client_code in ("PBTHAN", "PBTMDO"):
            for county, city in self._CA_routing_data:
                if row.get("work_county", "") == county \
                and (row.get("work_city", "") == city or city == "*"):
                    return client_code, county, city

        return None

    # XXX OBSOLETE
    def NCA_special_routing(self, row):
        result = self._CA_special_routing(row)
        if result:
            #client_code, county, city = result
            locator = self.tdb.get_locator_by_area("3637")
            return tools.RoutingResult(locator=locator, area_id="3637")
        return self.empty()

    # XXX OBSOLETE
    def SCA_special_routing(self, row):
        result = self._CA_special_routing(row)
        if result:
            #client_code, county, city = result
            locator = self.tdb.get_locator_by_area("3638")
            return tools.RoutingResult(locator=locator, area_id="3638")
        return self.empty()

    def empty(self):
        """ Return a new, empty RoutingResult. """
        return tools.RoutingResult()

    def polygon(self, row):
        """ Use "polygon routing": check what polygon the point (work_long,
            work_lat) falls in, then take the area associated with that
            polygon, and look up that area's locator.
        """
        routingresult = tools.RoutingResult()
        lat = row.get("work_lat", 0)
        long = row.get("work_long", 0)
        if long and lat:
            call_center = row['ticket_format']
            area_id = self.polygoncache.find_area(call_center, (long, lat))
            if area_id:
                locator = self.tdb.get_locator_by_area(area_id)
                if locator:
                    routingresult.area_id = area_id
                    routingresult.locator = locator
        return routingresult

    def installer(self, row):
        """ Get the installer_number from the ticket and look it up in the
            employee table.  That employee will be the locator. """
        rr = tools.RoutingResult()

        ins_num = row.get('installer_number', None)
        #print ">> installer_number:", `ins_num`
        if ins_num:
            emp_id = self.tdb.get_employee_with_installer_number(ins_num)
            if emp_id:
                rr.locator = emp_id

        return rr

