# ticketstats.py
# Created 24 Jan 2002, Hans Nowak
# Last update: 25 Mar 2002, Hans Nowak

import getopt
import glob
import string
import sys
import re
#
import datadir
import sqlmap
import sqlmaptypes as smt
import ticket as ticketmod
import ticketparser
import tools

__usage__ = """\
ticketstats.py [options] files...

Options:
    -s schema_file  Use a SQL schema file to check the field lengths.
    -f format       Assume that parsed tickets are of the given format.

Wildcards are permitted. If no files are specified, ticketstats.py processes
a fixed collection of files.
"""

# Modified TicketStats code so that it keeps track of discrete values, but
# only if they're hashable. This means that it won't keep track of lists
# (but those will most likely exceed 5 possibilities anyway).

class TicketStats:
    """ Keeps track of maximum length of fields. """

    def __init__(self):
        self.stats = {}
        self.tracker = {}   # keeps track of discrete values
        self.empty = {}     # number of times attributes were empty ("")
        self.none = {}      # number of times attributes were None

    def feed(self, ticket):
        """ Inspect the field length of a parsed ticket, and update the
            stats dictionary if necessary. """
        if isinstance(ticket, sqlmap.SQLMap):
            items = ticket._data.items()
        else:
            items = ticket.__dict__.items()
        for key, value in items:
            if not key.startswith("_"):
                # register empty or None
                if value == "":
                    self.empty[key] = self.empty.get(key, 0) + 1
                elif value is None:
                    self.none[key] = self.none.get(key, 0) + 1
                # update stats and tracker
                if self.stats.has_key(key):
                    if len(str(value)) > self.stats[key][0]:
                        self.stats[key] = self._getmax(value)
                    if tools.hashable(value):
                        self.tracker[key].add(value)
                else:
                    self.stats[key] = self._getmax(value)
                    if tools.hashable(value):
                        self.tracker[key] = tools.SimpleSet([value])
                    else:
                        self.tracker[key] = tools.SimpleSet()   # empty set

    def _getmax(self, value):
        if type(value) == type(""):
            return len(str(value)), value
        elif type(value) == type([]):
            # if it's a list, look for the longest item in the list
            maxlen, maxstr = 0, ""
            for item in value:
                length, string = self._getmax(item)
                if length > maxlen:
                    maxlen, maxstr = length, string
            return maxlen, maxstr
        elif value is None:
            return 0, value
        elif type(value) in (type(1), type(1L), type(1.0), type(1j)):
            return len(str(value)), value
        else:
            #raise TypeError, "Invalid type for _getmax: %s" % (type(value))
            # Do a valiant attempt to compute the length and use the
            # value... whether this works or not depends on the type of the
            # object!
            return len(value), value

    def report(self):
        items = self.stats.items()
        items.sort()
        for key, value in items:
            print key, "=>", value,
            # print empty/None results
            print "(%d times empty) (%d times None)" % (self.empty.get(key, 0),
            self.none.get(key, 0)),
            # print discrete values, but only if there are 5 or less
            if self.tracker.has_key(key):
                if len(self.tracker[key]) <= 5:
                    print self.tracker[key],
                else:
                    print "<%d discrete values>" % (len(self.tracker[key]))
            print

    def check_schema(self, filename=""):
        count = 0
        print "\nField length check:\n"
        stringfields = [(name, fielddef.width)
                        for (name, fielddef) in ticketmod.Ticket.__fields__
                        if isinstance(fielddef, smt.SQLString)]
        for fieldname, fieldlength in stringfields:
            reallength, value = self.stats.get(fieldname, (0, 0))
            if reallength > fieldlength:
                print "* %s: length in database is %d, found: %d" % (
                 fieldname, fieldlength, reallength)
                count = count + 1
        if not count:
            print "All field lengths are OK."


if __name__ == "__main__":

    schema_filename = ""
    format = ""

    opts, args = getopt.getopt(sys.argv[1:], "s:S:f:?")
    for o, a in opts:
        if o == "-?":
            print >> sys.stderr, __usage__
            sys.exit(0)
        if o in ("-s", "-S"):
            schema_filename = a
        elif o == "-f":
            format = a

    # get a list of files and glob them (wildcard expansion)
    files = []
    for arg in args:
        files.extend(glob.glob(arg))

    class BulkTicketStats(ticketparser.TicketBulkParser):
        def __init__(self, format=""):
            ticketparser.TicketBulkParser.__init__(self)
            self.count = 0
            self.stats = TicketStats()
        def action(self, ticket):
            self.stats.feed(ticket)
            self.count = self.count + 1
            #sys.stdout.write(".")
            if self.count % 100 == 0:
                print self.count, "tickets processed"

    bts = BulkTicketStats(format)

    if not files:
        # the default behavior: process a fixed collection of files
        bts.load([
            "../testdata/atlanta-1.txt",
            "../testdata/atlanta-2.txt",
            "../testdata/atlanta-3.txt",
            "../testdata/atlanta-4.txt",
            "../testdata/batonrouge1-1.txt",
            "../testdata/northcarolina-1.txt",
            "../testdata/lanham-1.txt",
            "../testdata/newjersey-1.txt",
            "../testdata/richmond1-1.txt",
            "../testdata/richmond2-1.txt",
            "../testdata/Pennsylvania-1.txt",
        ])
    else:
        bts.load(files)

    print bts.count, "tickets processed\n"
    bts.stats.report()

    bts.stats.check_schema()
