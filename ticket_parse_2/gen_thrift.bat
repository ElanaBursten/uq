@REM This runs the Thrift IDL compiler to create a QMLogic2ServiceLib package from QMLogic2ServiceLib.thrift.
@REM It should be run from the ticket_parse dir. 
@REM If you want to know what the params do, type thrift-0.9.1.exe --help.

REM todo(dan) add utf8strings?
..\bin\thrift-0.9.1.exe -out lib -verbose --gen py:new_style ..\server2\QMLogic2ServiceLib.thrift
