# csvreader.py

class CSVReaderError(Exception):
    pass

QUOTE_CHAR = '"'
SEPARATOR = ","

class CSVReader:

    def __init__(self, data, strict=1):
        self.strict = strict
        # if set, the number of fields found must match the number of
        # field names... so no excess or missing fields allowed.

        self.data = data # original data (one large string)
        self.lines = self.data.split("\n") # will be modified by parsing
        self.fieldnames = self._get_fieldnames()
        self._line = "" # line that was last parsed

    def _get_fieldnames(self):
        return self.parse_line(first=1)

    def get_next_line(self):
        """ Retrieve the next "ASCII" line from the file. """
        try:
            return self.lines.pop(0)
        except IndexError:
            return ""

    def parse_line(self, first=0):
        """ Parse the next line (may span multiple "ASCII" lines) and return
            the values found as a list of strings.
            If <first> is true, ignore restrictions like matching number of
            fields, etc.  (To be used internally.) """
        line = self.get_next_line()
        self._line = line # keep track of actual lines that we encounter
        if not line:
            return [] # nothing to return
        in_quotes = False
        tokens = []
        s = ""

        while True:
            if in_quotes:
                # continuing a quoted section, reappend newline
                s += "\n"
                line = self.get_next_line()
                self._line = self._line + "\n" + line
                if not line:
                    raise CSVReaderError, "Incomplete line: %r" % (self._line,)

            skip = 0
            for idx, c in enumerate(line):
                if skip:
                    skip = 0
                    continue
                if c == QUOTE_CHAR:
                    # is this a double quote?
                    if in_quotes \
                    and len(line) > idx+1 \
                    and line[idx+1] == QUOTE_CHAR:
                        s += line[idx+1]
                        skip = 1 # skip the next character (also a quote)
                    elif not in_quotes:
                        in_quotes = True
                        if not s.strip():
                            s = "" # ignore any leading whitespace
                    else:
                        in_quotes = not in_quotes
                elif c == SEPARATOR and not in_quotes:
                    tokens.append(s)
                    s = "" # start working on new "token"
                else:
                    s += c
            if not in_quotes:
                break

        tokens.append(s)

        if self.strict and not first:
            lt = len(tokens)
            lf = len(self.fieldnames)
            if lt > lf:
                raise CSVReaderError, "Too many fields: %d (expected %d)\nLine: %r" % (
                 lt, lf, self._line)
            elif lt < lf:
                raise CSVReaderError, "Not enough fields: %d (expected %d)\nLine: %r" % (
                 lt, lf, self._line)

        return tokens

    def parse_line_as_dict(self, strip_keys=1):
        """ Parse a line and return the results as a dictionary, with field
            names for keys. """
        values = self.parse_line()
        if not values:
            return {}
        d = {}
        for i, fieldname in enumerate(self.fieldnames):
            try:
                value = values[i]
            except IndexError:
                value = ""
            if strip_keys:
                fieldname = fieldname.strip()
            d[fieldname] = value
        return d

    def next(self):
        """ Get the next line, in the form of a dict.  To be used as an
            iterator or generator. """
        d = self.parse_line_as_dict()
        if not d:
            raise StopIteration
        return d

    def __iter__(self):
        return self


#
# The CSV reader can be run as a standalone script to test files.  This is
# non-destructive and doesn't affect the database in any way.

if __name__ == "__main__":

    import sys, traceback

    data = open(sys.argv[1]).read()
    csv = CSVReader(data)
    count = 0

    while True:
        count = count + 1
        try:
            d = csv.parse_line_as_dict()
            if not d:
                print "All lines parsed OK."
                break
            assert len(d) <= len(csv.fieldnames)
        except:
            traceback.print_exc()
            print
            print "Problem with line", count
            print "Line data collected so far:", csv._line
            break
        else:
            print "Correct line found: #%s, ticket number %s" % (
             count, d['Ticket Number'])
