# polygontester.py

import getopt
import os
import sets
import sys
#
import config
import datadir
import dbinterface_ado as dbado
import polygon_routing

class PolygonTester:

    def __init__(self, dbdata={}):
        self.cfg = config.getConfiguration()
        # if no dbdata are specified, use default data from configuration
        self.db = dbado.DBInterfaceADO(dbdata=dbdata)
        self.cache = polygon_routing.PolygonRoutingCache()

    def check(self, call_centers):
        for call_center in call_centers:
            self.check_call_center(call_center)

    def check_call_center(self, call_center):
        checked = sets.Set()
        foo = self.cache.get(call_center)
        if not foo:
            print "**WARNING**  Could not load", call_center
            return

        for rp in foo.routinglist:
            #print rp.coords, rp.area_id, rp.name
            if rp.area_id in checked:
                continue # we already checked this area
            self.check_area(rp.area_id)
            checked.add(rp.area_id)

    def check_area(self, area_id):
        sql = "select * from area where area_id = %s" % (area_id,)
        rows = self.db.runsql_result(sql)
        if not rows:
            print "Warning: area", area_id, "does not exist"
            return
        if not rows[0]['locator_id']:
            print "Warning: area", area_id, "has no locator"
            return

        locator_id = rows[0]['locator_id']
        sql2 = "select * from employee where emp_id = %s" % (locator_id,)
        rows2 = self.db.runsql_result(sql2)
        if not rows2:
            print "Warning: locator", locator_id, "does not exist"
            return
        if rows2[0]['active'] in ('0', 0, False):
            print "Warning: locator", locator_id, "is not active"
            return


if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "s:d:l:p:")
    call_centers = args[:]

    dbdata = {}
    for o, a in opts:
        if o == "-s":
            dbdata['host'] = a
        elif o == "-d":
            dbdata['database'] = a
        elif o == "-l":
            dbdata['login'] = a
        elif o == "-p":
            dbdata['password'] = a

    tester = PolygonTester(dbdata)
    tester.check(call_centers)

