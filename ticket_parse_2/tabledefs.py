# tabledefs.py
# Extract and cache field definitions of database tables.

import sqlmaptypes as smt

field_mappings = {
    'bit':      smt.sqlbool,
    'char':     smt.sqlchar,
    'datetime': smt.sqldatetime,
    'decimal':  smt.sqldecimal,
    'int':      smt.sqlint,
    'text':     smt.sqltext,
    'varchar':  smt.sqlstring,
    'tinyint':  smt.sqltinyint,
    'money':    smt.sqlcurrency,
}

class TableDefs(object):
    """ Holds field definitions of tables. Definitions are loaded on demand,
        then cached. """

    # all instances of TableDefs share the same cache
    _cache = {}

    def __init__(self, db):
        self.db = db

    def __getattr__(self, tablename):
        try:
            return self._cache[tablename]
        except KeyError:
            fields = self.extract_columns(tablename)
            self._cache[tablename] = fields
            return fields

    __getitem__ = __getattr__ # in case of ambiguity

    SQL_TEMPLATE = """\
        SELECT t.table_name,
            c.column_name 'name',
            c.data_type 'type',
            c.character_maximum_length 'length',
            c.numeric_precision,
            c.numeric_scale
        FROM INFORMATION_SCHEMA.TABLES t, INFORMATION_SCHEMA.COLUMNS c
        WHERE t.table_name = c.table_name AND t.table_name = '%s'
        ORDER BY c.ordinal_position
        """

    def extract_columns(self, table):
        """ Get schema data for the given table, and return a list of fields
            and their types (as defined in sqlmaptypes.py). """
        fields = []
        rows = self.db.runsql_result(self.SQL_TEMPLATE % table)
        for row in rows:
            if row['type'] == 'decimal':
                numprec = int(row['numeric_precision'])
                numscale = int(row['numeric_scale'])
                fields.append((row['name'], smt.sqldecimal(numprec, numscale)))
            elif row['type'] == 'varchar':
                fields.append((row['name'],
                  field_mappings[row['type']](int(row['length']))))
            else:
                fields.append((row['name'], field_mappings[row['type']]))

        return fields

    def reset(self):
        self.__class__._cache = {}

    def lookup(self, tablename, fieldname):
        fields = self.__getitem__(tablename)
        for name, type in fields:
            if name == fieldname:
                return type
        raise AttributeError("No definition found for %s.%s" % (
              tablename, fieldname))

    def fields_as_dict(self, tablename):
        fields = self.__getitem__(tablename)
        return dict([(k,v) for (k,v) in fields])

def table_exists(db, tablename):
    sql = """
      select * from information_schema.tables
      where table_name = '%s'
    """ % tablename
    rows = db.runsql_result(sql)
    return len(rows) > 0

