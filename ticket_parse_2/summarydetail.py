# summarydetail.py

import sqlmap
import sqlmaptypes as smt
import static_tables

class SummaryDetail(sqlmap.SQLMap):
    __table__ = "summary_detail"
    __key__ = "summary_detail_id"
    __fields__ = static_tables.summary_detail

    def __init__(self, data=()):
        sqlmap.SQLMap.__init__(self)
        if data:
            self.setdata(data)

    #
    # simulate list interface

    _list_fields = ["item_number", "ticket_number", "ticket_time", "revision",
                    "type_code"]

    def __getitem__(self, index):
        fieldname = self._list_fields[index]
        return self._data[fieldname]

    def __setitem__(self, index, value):
        fieldname = self._list_fields[index]
        self._data[fieldname] = value

    def setdata(self, data):
        for index, value in enumerate(data):
            fieldname = self._list_fields[index]
            self._data[fieldname] = value

    def getdata(self):
        return [self._data[fieldname] for fieldname in self._list_fields]

    #
    # pickling

    def __getstate__(self):
        state = self.__dict__.copy()
        return state

    def __setstate__(self, adict):
        self.__dict__.update(adict)

