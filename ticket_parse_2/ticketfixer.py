# ticketfixer.py
# Created: 16 Apr 2002, Hans Nowak
# Last update: 16 Apr 2002, Hans Nowak
# OBSOLETE.

# For now, a simple script to deal with the time problem.

import sys
import getopt
import ticket_db
import ticketparser
from date import Date

raise NotImplementedError, "This script is obsolete"

FIELDS = ["transmit_date", "call_date"]

# only fix if we specify the -f switch...
fix = "-f" in sys.argv[1:]

tdb = ticket_db.TicketDB()
tickets = tdb.gettickets()  # may be a lot of records
handler = ticketparser.TicketHandler()

for ticket in tickets:
    t = ticket["ticket"]
    id = ticket["ticket_id"]
    print "Inspecting ticket number", t.ticket_number, "id", id

    # for now, only check the dates
    raw = t.image
    tnow = handler.parse(raw)

    for fieldname in FIELDS:
        value1 = getattr(t, fieldname)
        value2 = getattr(tnow, fieldname)
        # what if certain dates are empty? I guess value1 would then be None
        # or ""...?
        time1 = value1[-8:]
        time2 = value2[-8:]
        # check if these values are correct, e.g. have length 8, etc.
        if time1 != time2:
            print "Different times for", fieldname, ":", time1, "vs", time2
            # correct time
            if fix:
                newdatetime = value1[:-8] + time2
                print "Fixing...",
                tdb.updateticket(id, {fieldname: value2}, whodunit="FIXER")
                print "OK"
