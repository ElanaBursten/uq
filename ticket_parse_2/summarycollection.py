# summarycollection.py
# Created: 12 Apr 2002, Hans Nowak
# Last update: 14 Apr 2002, Hans Nowak

import operator
import time
import types
#
import datadir
import date
import summary_fragment
import summaryparsers
import ticketparser
import ticketsummary

WAITING_TIME = 60*60*2
# the time we wait before flagging an incomplete summary as "done"

class SummaryCollectionError(Exception): pass

class SummaryCollection:
    """ A persistent collection of summary parts. """

    def __init__(self, tdb, call_center, formats, parse):
        self.data = {}
        self.tdb = tdb
        self.call_center = call_center
        self.last_client = {}   # last call center/client code added

        sql = """
         select * from summary_fragment
         where call_center = '%s' and parse_error <> 1
        """ % call_center
        for row in self.tdb.runsql_result(sql):
            try:
                # A parsing error here is unlikely, and would probably indicate
                # a programming error, since this was already successfully
                # parsed at least once
                summary = parse(row['image'], suggested_formats=formats,
                                call_center=self.call_center)
                summary._timestamp = date.Date(row['parse_date']).isodate()
                if isinstance(summary, ticketsummary.TicketSummary):
                    self._add_summary_to_dict(summary)
                else:
                    self._set_parse_error(self, row['summary_fragment_id'])
            except (TicketHandlerError, TicketError):
                self._set_parse_error(self, row['summary_fragment_id'])

    def _set_parse_error(self, summary_fragment_id):
        sql = """
         update summary_fragment
         set parse_error = 1
         where summary_fragment_id = %d
        """ % int(summary_fragment_id)
        self.tdb.runsql(sql)

    def _add_summary_to_dict(self, summary):
        key = (summary.call_center, summary.client_code)
        if not self.data.has_key(key):
            self.data[key] = []
        self.data[key].append(summary)

    def add(self, summary):
        """ Add a (parsed) summary to the collection, using the given call
            center and client code as a key, to the db and data dictionary. """

        parse_date = date.Date().isodate()

        # insert summary_fragment record
        sf = summary_fragment.SummaryFragment()
        sf.set(call_center=summary.call_center,
               client_code=summary.client_code,
               image=summary.image,
               parse_date=parse_date)
        sf.insert(self.tdb)

        summary._timestamp = parse_date
        self._add_summary_to_dict(summary)
        self.last_client[summary.call_center] = summary.client_code

    def get(self, call_center, client_code):
        """ Get the list of summary parts for the given call center and
            client code. """
        return self.data.get((call_center, client_code), [])

    def remove_summaries_from_db(self, call_center, client_code):
        """ Remove the list of summary parts for the given call center and
            client code from the database. """
        sql = """
         delete from summary_fragment
         where call_center = '%s' and client_code = '%s' and parse_error <> 1
        """ % (call_center, client_code)
        self.tdb.runsql(sql)

    def remove_summaries_from_dict(self, call_center, client_code):
        """ Remove the list of summary parts for the given call center and
            client code from the data dictionary. """
        del self.data[(call_center, client_code)]

    def iscomplete(self, call_center, client_code):
        """ Return true if the (multipart) summary is complete, i.e. has at
            least one part containing a header and one containing a footer.
        """
        h = f = ticks_found = expected = 0
        for summ in self.data[(call_center, client_code)]:
            # Get the ticket counter
            parser = summaryparsers.get_summaryparser(summ.format)

            expected = max(expected, summ.expected_tickets)
            ticks_found += parser.count_received(summ.data)
            h = h + summ._has_header
            f = f + summ._has_footer
        return (h > 0) and (f > 0) and (expected == ticks_found)

    def find_complete(self):
        """ Return a list of tuples (call_center, client_code) for summaries
            that are complete. """
        complete = []
        for (call_center, client_code) in self.data.keys():
            if self.iscomplete(call_center, client_code):
                complete.append((call_center, client_code))
        return complete

    def find_incomplete(self):
        incomplete = []
        for (call_center, client_code), summaries in self.data.items():
            if not self.iscomplete(call_center, client_code):
                timestamps = [date.Date(s._timestamp) for s in summaries]
                latest = max(timestamps)
                incomplete.append((call_center, client_code, latest))
        return incomplete

    def find_incomplete_and_done(self):
        """ Find summaries that are incomplete, but that need to be posted
            anyway because no more parts are coming in. (These summaries
            should be posted with a warning.) """
        # only take summaries whose client code differs from the latest
        # client code for that call center
        now = date.Date()
        incomplete = [i for i in self.find_incomplete()
                      if i[1] != self.last_client.get(i[0], "")
                      and now.diff(i[2]) > WAITING_TIME]
        return incomplete

    def tickets_complete(self, parts):
        """ Return true if the number of expected tickets is less than or equal
            to the actual number of ticket "records" found in .data. """
        if not parts:
            return 0    # undefined, really
        # which one has the footer? Assume the first footer found
        part_with_footer = filter(lambda p: p._has_footer, parts)[0]
        expected_tickets = part_with_footer.expected_tickets
        total = reduce(operator.add, [len(p.data) for p in parts])
        return total >= expected_tickets

    def mark_complete(self, parts):
        """ Mark a series of summary parts as complete or incomplete.
            Changes the summaries in place. """
        if not parts:
            return
        c = self.tickets_complete(parts)
        for part in parts:
            part.complete = c

