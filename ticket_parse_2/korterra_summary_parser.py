# korterra_summary_parser.py

import re
#
import date
import summarydetail
import summaryparsers
import tools

from re_tools2 import BLANKS_0_N, BLANKS_1_N

class BaseKorTerraSummaryParser(summaryparsers.BaseSummaryParser):
    r_summary_date = re.compile('FOR TICKETS SENT ON' + BLANKS_1_N + '([0-9/]+)')
    r_total = re.compile('Total Sent' + BLANKS_0_N + '=' + BLANKS_0_N + r'(\d+)')

    def ticket_number_from_jobid(self, jobid):
        return jobid

    def read(self, data):
        tickets = []
        collecting = 0
        lines = data.split("\n")
        for line in lines:
            if line.startswith("----") and not collecting:
                collecting = 1 # begin collecting
            elif (not line.strip()) and collecting:
                collecting = 0 # end collecting
                break
            elif ('No Tickets sent to this location' in line) and collecting:
                collecting = 0 # end collecting
                break
            elif collecting:
                try:
                    jobid, seq_number = line.split()[:2]
                except (IndexError, ValueError):
                    continue

                ticket_number = self.ticket_number_from_jobid(jobid)

                detail = summarydetail.SummaryDetail([
                         seq_number, ticket_number, "", "", ""])
                tickets.append(detail)

        return tickets

    def get_summary_date(self, data):
        m = self.r_summary_date.search(data)
        if m:
            d = tools.isodate(m.group(1))
            return date.Date(d).isodate()
        else:
            return date.Date().isodate()

    def get_expected_tickets(self, data):
        m = self.r_total.search(data)
        if m:
            try:
                total = int(m.group(1))
            except ValueError:
                return 0
            else:
                return total
        else:
            return 0

    def has_header(self, data):
        return bool(self.r_summary_date.search(data))

    def has_footer(self, data):
        return bool(self.r_total.search(data))
