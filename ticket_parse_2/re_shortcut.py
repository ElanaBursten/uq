import re
def R(regex, flags=0):
    # Shortcut function, provided for convenience. We're going to write
    # re.compile("something", re.MULTILINE) a *lot*.
    return re.compile(regex, re.MULTILINE | flags)
