# version.py

import os
import re

re_source_version = '([0-9]+)\.([0-9]+)\.([0-9]+).([0-9]+)-(.+)'

if os.path.exists('version.dat'):
    # Get the version from this file
    f = open('version.dat')
    __version__ = f.readline()
    m = re.search(re_source_version, __version__)
    if m:
        __version__ = '%s.%s.%s.%s-%s' % (m.group(1), m.group(2), m.group(3), m.group(4), m.group(5)[:7])
    f.close()
else:
    __version__ = 'N/A'

if __name__ == "__main__":

    print "Current version:", __version__

