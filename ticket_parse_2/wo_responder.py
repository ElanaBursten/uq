# wo_responder.py
# Relevant Mantis items: #2744, #2802
#
# NOTE: Currently we only have one work order center, LAM01, so much of this
# is actually an implementation of handling Lambert work orders. If/when other
# WO centers are added, the WorkOrderResponder will likely need to be made
# more general, with the center-specific code in subclasses.
#
# Depends on stored procs:
# - get_pending_wo_responses
# - get_wo_notes
# - get_wo_attachments

# TODO:
# - replace closed_date with status_date, but this will only work when the new
#   get_pending_wo_responses stored proc has been applied

from __future__ import with_statement
import date
import getopt
import os
import sys
import time
#
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
import callcenters
import config
import datadir
import dbinterface_ado
import errorhandling2
import errorhandling_special
import filetools
import mutex
import ticket_db
import tools
from check_python_version import check_python_version

PROCESS_NAME = 'wo_responder'

class WorkOrderResponderOptions:
    nodelete = 0
    sends_email = 1
    verbose = 1
    configfile = ""
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

class WorkOrderResponderData:
    def __init__(self, tdb):
        self.tdb = tdb
    def write_xml(self, rows, filename):
        pass # override in subclasses
    def extra_data(self, row):
        pass
        # override in subclass to make changes to row if necessary, e.g. to
        # add fields or change their format

class WorkOrderResponder:
    name = 'WorkOrderResponder'

    def __init__(self, options=None, dbado=None):
        self.options = options or WorkOrderResponderOptions()
        self.tdb = ticket_db.TicketDB(dbado=dbado)
        self.config = config.getConfiguration()
        self._setup_logger(self.options.verbose)
        self.wor_data = {} # cache of WorkOrderResponderData instances

    def _setup_logger(self, verbose):
        # create and configure the logger. requires self.config.
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtp_accs[0], me=self.__class__.name,
                   admin_emails=self.config.admins,
                   subject=self.__class__.name + " Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = verbose

    def get_pending_responses(self, call_center):
        sql = """
         exec get_pending_wo_responses '%s';
        """ % call_center
        rows = self.tdb.runsql_result(sql)
        return rows

    def get_responder_data(self, call_center):
        try:
            return self.wor_data[call_center]
        except KeyError:
            cc = call_center
            if call_center[0] in '0123456789':
                cc = 'c' + call_center
            word_class = callcenters.get_wo_responderdata(cc)
            word = word_class(self.tdb)
            self.wor_data[call_center] = word
            return word

    def run(self):
        for cc, resp_data in self.config.wo_responders.items():
            try:
                wor_data = self.get_responder_data(cc)
                self.process_responses(cc, resp_data)
                ack_dir = resp_data['ack_dir']
                if ack_dir: #and os.path.exists(ack_dir):
                    self.process_acks(ack_dir)
                else:
                    self.log.log_event("Acknowledgements not processed "\
                      "(directory not specified)")
            except:
                ep = errorhandling2.errorpacket()
                self.log.log(ep, send=self.options.sends_email, write=1,
                 dump=self.options.verbose)
            finally:
                if self.options.sends_email:            
                    self.log.force_send()
        
    def process_responses(self, call_center, resp_data):
        resp_dir = resp_data.get('resp_dir', '')
        self.log.log_event("Processing responses for: %s" % call_center)
        rows = self.get_pending_responses(call_center)
        self.log.log_event("Number of pending responses found: %d" % len(rows))
        for row in rows:
            try:
                self.process_response(call_center, resp_dir, row)
            except:
                ep = errorhandling2.errorpacket()
                ep.add(responderdata=row)
                ep.add(info="Could not process response")
                self.log.log(ep, send=self.options.sends_email, write=1,
                 dump=self.options.verbose)

    def process_response(self, call_center, resp_dir, row):
        # note: we create response log first, get ID, pass this in with the
        # response
        self.log.log_event("Processing response: %s" % row['client_wo_number'])
        wor_data = self.get_responder_data(call_center)
        filename = self.gen_filename(call_center, resp_dir, row)
        wor_data.extra_data(row)

        # get attachments and notes through separate queries
        row['!attachments'] = self.get_wo_attachments(row['wo_id'])
        row['!notes'] = self.get_wo_notes(row['wo_id'])
        row['!resp_id'] = resp_id = self.add_response_log(row)
        row['remarks'] = 'BSW WO#' + row['client_wo_number']

        # create XML file directly in resp_dir
        wor_data.write_xml([row], filename)

        # if all went well, remove from queue
        self.delete_from_queue(row)

    def process_acks(self, ack_dir):
        # check ack_dir; for now, only consider .xml and .txt files
        files = [fn for fn in os.listdir(ack_dir)
                 if fn.lower().endswith((".xml", ".txt"))]
        self.log.log_event("Processing acks: %d files found" % len(files))
        for fn in files:
            try:
                path = os.path.join(ack_dir, fn)
                self.log.log_event("Reading ack file: %s" % path)
                # read file as XML...
                xml = ET.parse(path)
                ok = xml.find('ACK').text
                num = xml.find('INTERNALNUMBER').text # irrelevant?
                resp_id = xml.find('RESPONSEID').text
                reason = xml.find('REASON').text
                # update response_log
                t = [(s or "").encode('utf-8') for s in [num, ok, reason]]
                self.log.log_event(tools.fmt("Processing ack for id:",
                  resp_id, "(%s/%s/%s)" % tuple(t)))
                self.update_response_log(resp_id, ok, reason)
                filetools.remove_file(path)
            except:
                ep = errorhandling2.errorpacket()
                ep.add(info="Could not process ack file")
                ep.add(ack_filename=path)
                self.log.log(ep, send=self.options.sends_email, write=1,
                 dump=self.options.verbose)

    def gen_filename(self, call_center, resp_dir, row):
        wo_number = row['client_wo_number']
        t9 = time.localtime(time.time())
        timestamp = '%04d%02d%02d-%02d%02d%02d' % tuple(t9[:6])
        filename = '%s-%s-%s.xml' % (call_center, wo_number, timestamp)
        return os.path.join(resp_dir, filename)

    def delete_from_queue(self, row):
        self.tdb.delete('wo_responder_queue', wo_id=row['wo_id'])

    def add_response_log(self, row):
        closed_date = row['closed_date'] or row['status_date'] \
                   or date.Date().isodate()
        sql = """
         insert wo_response_log
           (wo_id, response_date, wo_source, status, response_sent,
            success, reply)
         values (%s, '%s', '%s', '%s', '...', 0, '%s')
         select scope_identity() as id
        """ % (int(row['wo_id']), closed_date, row['wo_source'],
               row['status'], '(ack waiting)')
        rows = self.tdb.runsql_result(sql)
        return int(rows[0]['id'])

    def update_response_log(self, resp_id, ok, reason):
        resp_ok = ok.lower().strip() == 'ok'
        #reason = (reason or "").encode('utf-8')
        sql = """
         update wo_response_log
         set success=%d, reply='%s'
         where wo_response_id = %d
        """ % (int(resp_ok), reason, int(resp_id))
        self.tdb.runsql(sql)

    def get_wo_attachments(self, wo_id):
        rows = self.tdb.runsql_result("exec get_wo_attachments %s" % wo_id)
        return ' \n'.join([row.get('attachment_file', '') for row in rows])

    def get_wo_notes(self, wo_id):
        rows = self.tdb.runsql_result("exec get_wo_notes %s" % wo_id)
        return ' \n'.join([row.get('note', '') for row in rows])

if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "dew", ["data="])
    options = WorkOrderResponderOptions()

    for o, a in opts:
        if o == '-d':
            options.nodelete = 1
        elif o == '-e':
            options.sends_email = 0
        elif o == '-w':
            options.wait = 1
        elif o == "--data":
            print "Data dir:", a

    try:
        log = errorhandling_special.toplevel_logger(options.configfile,
              "WorkOrderResponder", "WorkOrderResponder Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(options.configfile,
         "WorkOrderResponder")
        sys.exit(1)

    check_python_version(log)

    try:
        # only once instance of this program is allowed to run at a time
        dbado = dbinterface_ado.DBInterfaceADO()
        mutex_name = PROCESS_NAME + ":" + datadir.datadir.get_id()
        with mutex.MutexLock(dbado, mutex_name):
            xlr = WorkOrderResponder(options, dbado=dbado)
            xlr.run()
    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)


