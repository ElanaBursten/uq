# assignment.py

import sqlmap
import sqlmaptypes as smt
import static_tables

class Assignment(sqlmap.SQLMap):

    __table__ = "assignment"
    __key__ = "assignment_id"
    __fields__ = static_tables.assignment

    def __init__(self, locator=""):
        sqlmap.SQLMap.__init__(self)
        self.locator_id = locator

    def __repr__(self):
        return "Assignment(%s)" % (self.locator_id,)

    def insert_sg(self, sg):
        pass

