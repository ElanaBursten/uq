; Use Inno Setup 5.4.3(a)+
#define Version GetEnv("VERSION")
#define GitCommitID GetEnv("GIT_COMMIT")
#define GitCommitIDShort copy(GitCommitID,1,7) 
#if GitCommitIDShort == ""
 #define GitCommitIDWithDash= "-DevTest"
#else
 #define GitCommitIDWithDash= "-" + GitCommitIDShort
#endif

; todo: Does switching between developer and production installations in the
; same installation directory work correctly?

[Setup]
AppCopyright=Copyright 2002-{#GetDateTimeString('yyyy', '#0', '#0')} by UtiliQuest, LLC
AppName=UtiliQuest Q Manager Backend
AppVerName=UtiliQuest Q Manager Backend {#Version}
AppID=UtiliQuestQManagerBackend
Compression=lzma/max
SolidCompression=yes
DefaultDirName={pf}\UtiliQuest\Q Manager Backend
DefaultGroupName=UtiliQuest
AppPublisher=UtiliQuest, LLC
AppPublisherURL=http://www.utiliquest.com/
AppMutex=UtiliQuestQManagerBackend
AppVersion={#Version}{#GitCommitIDWithDash}
OutputDir=..\build
OutputBaseFilename=QManagerBackendSetup-{#Version}{#GitCommitIDWithDash}
DisableProgramGroupPage=yes
PrivilegesRequired=admin
OutputManifestFile=QMBackendSetup-Manifest.txt
SetupLogging=yes
AllowUNCPath=False

[Types]
Name: "Production"; Description: "Production Installation"
Name: "Development"; Description: "Development and Test Installation"

[Components]
Name: "TestData"; Description: "Test data"; Types: Development

[InstallDelete]
Type: files; Name: "{app}\python.bat"
Type: files; Name: "{app}\zip-upload.bat"
Type: files; Name: "{app}\olddeploy.bat"
Type: files; Name: "{app}\deploy_config.bat"
Type: files; Name: "{app}\deploy.bat"
Type: filesandordirs; Name: "{app}\staging"

Type: files; Name: "{app}\*.py?"
Type: files; Name: "{app}\callcenters\*.py?"
Type: files; Name: "{app}\formats\*.py?"
Type: files; Name: "{app}\lib\*.py?"
Type: files; Name: "{app}\lib\pytz\*.py?"
Type: files; Name: "{app}\lib\pytz\tests\*.py?"
Type: files; Name: "{app}\lib\suds\*.py?"
Type: files; Name: "{app}\lib\suds\bindings\*.py?"
Type: files; Name: "{app}\lib\suds\mx\*.py?"
Type: files; Name: "{app}\lib\suds\sax\*.py?"
Type: files; Name: "{app}\lib\suds\transport\*.py?"
Type: files; Name: "{app}\lib\suds\umx\*.py?"
Type: files; Name: "{app}\lib\suds\xsd\*.py?"
Type: files; Name: "{app}\lib\requests\*.py?"
Type: files; Name: "{app}\lib\requests\packages\*.py?"
Type: files; Name: "{app}\lib\requests\packages\charade\*.py?"
Type: files; Name: "{app}\lib\requests\packages\chardet\*.py?"
Type: files; Name: "{app}\lib\requests\packages\urllib3\*.py?"
Type: files; Name: "{app}\lib\requests\packages\urllib3\contrib\*.py?"
Type: files; Name: "{app}\lib\requests\packages\urllib3\packages\*.py?"
Type: files; Name: "{app}\lib\requests\packages\urllib3\packages\ssl_match_hostname\*.py?"
Type: files; Name: "{app}\lib\pyproj\*.py?"
Type: files; Name: "{app}\lib\QMLogic2ServiceLib\*.py?"
Type: files; Name: "{app}\lib\thrift\*.py?"
Type: files; Name: "{app}\lib\thrift\protocol\*.py?"
Type: files; Name: "{app}\lib\thrift\server\*.py?"
Type: files; Name: "{app}\lib\thrift\transport\*.py?"
Type: files; Name: "{app}\scripts\*.py?"

Type: files; Name: "{app}\*.egg"

; Old performance profiling data
Type: files; Name: "{app}\profile-data\*_20050620_*.txt"
Type: dirifempty; Name: "{app}\profile-data"

; todo: Should we install the sample routing list? 
Type: files; Name: "{app}\cc_routing_list.tsv"

; todo: Can *.pyc and *.py be deleted from subdirs, without specifying each one?

[UninstallDelete]
Type: files; Name: "{app}\*.pyc"
Type: files; Name: "{app}\callcenters\*.pyc"
Type: files; Name: "{app}\formats\*.pyc"
Type: files; Name: "{app}\lib\*.pyc"
Type: files; Name: "{app}\lib\pytz\*.pyc"
Type: files; Name: "{app}\lib\pytz\tests\*.pyc"
Type: files; Name: "{app}\lib\suds\*.pyc"
Type: files; Name: "{app}\lib\suds\bindings\*.pyc"
Type: files; Name: "{app}\lib\suds\mx\*.pyc"
Type: files; Name: "{app}\lib\suds\sax\*.pyc"
Type: files; Name: "{app}\lib\suds\transport\*.pyc"
Type: files; Name: "{app}\lib\suds\umx\*.pyc"
Type: files; Name: "{app}\lib\suds\xsd\*.pyc"
Type: files; Name: "{app}\lib\requests\*.pyc"
Type: files; Name: "{app}\lib\requests\packages\*.pyc"
Type: files; Name: "{app}\lib\requests\packages\charade\*.pyc"
Type: files; Name: "{app}\lib\requests\packages\chardet\*.pyc"
Type: files; Name: "{app}\lib\requests\packages\urllib3\*.pyc"
Type: files; Name: "{app}\lib\requests\packages\urllib3\contrib\*.pyc"
Type: files; Name: "{app}\lib\requests\packages\urllib3\packages\*.pyc"
Type: files; Name: "{app}\lib\requests\packages\urllib3\packages\ssl_match_hostname\*.pyc"
Type: files; Name: "{app}\lib\pyproj\*.pyc"
Type: files; Name: "{app}\lib\QMLogic2ServiceLib\*.pyc"
Type: files; Name: "{app}\lib\thrift\*.pyc"
Type: files; Name: "{app}\lib\thrift\protocol\*.pyc"
Type: files; Name: "{app}\lib\thrift\server\*.pyc"
Type: files; Name: "{app}\lib\thrift\transport\*.pyc"
Type: files; Name: "{app}\scripts\*.pyc"

[Files]
; Config and data files typically have the onlyifdoesntexist and uninsneveruninstall flags
; Executable files (.py, exe, .bat, etc.) typically have the ignoreversion flag
; Obsolete files should typically be added to the InstallDelete section

; Production install files

Source: "*.py"; DestDir: "{app}"; Flags: ignoreversion
Source: "kill.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "tlist.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "clean.bat"; DestDir: "{app}"; Flags: ignoreversion
Source: "restart.bat"; DestDir: "{app}"; Flags: ignoreversion
Source: "run.bat"; DestDir: "{app}"; Flags: ignoreversion
Source: "version.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "callcenters\*.py"; DestDir: "{app}\callcenters"; Flags: ignoreversion
Source: "formats\*.py"; DestDir: "{app}\formats"; Flags: ignoreversion
Source: "lib\*"; DestDir: "{app}\lib"; Flags: createallsubdirs recursesubdirs ignoreversion; Excludes: "*.pyc"
Source: "scripts\convert_status_translations.py"; DestDir: "{app}\scripts"; Flags: ignoreversion

Source: "..\build\QMAttachmentImport.exe"; DestDir: "{app}\bin"; Flags: ignoreversion
Source: "..\build\QMCopyAttachments.exe"; DestDir: "{app}\bin"; Flags: ignoreversion

; Extra files for development install

Source: "add_test_data.bat"; DestDir: "{app}"; Flags: ignoreversion; Components: TestData
Source: "set_QMDATA.bat"; DestDir: "{app}"; Flags: ignoreversion; AfterInstall: Update_set_QMDATA; Components: TestData
Source: "scripts\add_sample_tickets.py"; DestDir: "{app}\scripts"; Flags: ignoreversion; Components: TestData
Source: "scripts\make_dummy_LAM01.py"; DestDir: "{app}\scripts"; Flags: ignoreversion; Components: TestData
Source: "testdata\datadir\rules\*cc_routing_list.tsv"; DestDir: "{code:DataDir}\rules"; Flags: ignoreversion; Components: TestData
Source: "testdata\datadir\routing_error_skip.txt"; DestDir: "{code:DataDir}"; Flags: ignoreversion; Components: TestData

; Test tickets used by add_sample_tickets.py
Source: "testdata\tickets\*.txt"; DestDir: "{app}\testdata\tickets"; Flags: recursesubdirs ignoreversion; Components: TestData

; Test audits used by add_sample_tickets.py
Source: "testdata\audits\*.txt"; DestDir: "{app}\testdata\audits"; Flags: recursesubdirs ignoreversion; Components: TestData

; Test work orders used by make_dummy_LAM01.py
Source: "testdata\work-orders\LAM01\*.txt"; DestDir: "{app}\testdata\work-orders\LAM01"; Flags: ignoreversion; Components: TestData

; todo: Review these. Should we overwrite sample_config when updating?
Source: "testdata\datadir\developer_config.xml"; DestDir: "{code:DataDir}"; DestName: "config.xml"; Flags: onlyifdoesntexist uninsneveruninstall; Components: TestData
Source: "sample_config.xml"; DestDir: "{code:DataDir}"; Flags: onlyifdoesntexist uninsneveruninstall; Components: TestData

[Run]
Filename: "{app}\restart.bat"; Flags: nowait; Tasks: RunRestart

[Tasks]
Name: "RunRestart"; Description: "Run restart.bat after a successful installation"; Check: IsProductionInstall

[Dirs]
Name: "{app}\logs"; Flags: uninsneveruninstall
Name: "{app}\rules"; Flags: uninsneveruninstall

[Code]

const
  PythonMajorVersion = 2;
  PythonMinorVersion = 7;
  MinPythonMicroVersion = 2;
  PythonDownload = 'http://python.org/download/releases/2.7.2/'; 
  MinPyWin32Build = 215; // Build released on 2/21/2011. 214 was from 7/8/2009.
  PyWin32Download = 'http://sourceforge.net/projects/pywin32/files/pywin32/';
  CRLF = #13#10;

var
  // todo: Change PrereqErrorPageID to PrereqErrorPage.ID
  PrereqErrorPageID: Integer;
  InputDataDirPage: TInputDirWizardPage;
  CurrentAppDir: string;
  CurrentDataDir: string;
  PostInstallWarning: string;
  
function DataDir(Param: string): string;
begin
  Result := CurrentDataDir;
end;

procedure Update_set_QMDATA;
begin
  if not SaveStringToFile(ExpandConstant('{app}\set_QMDATA.bat'),
    'set QMDATA=' + DataDir(''), False) then
    PostInstallWarning := ExpandConstant('Unable to update:' + CRLF +
      '{app}\set_QMDATA.bat. ' + CRLF +
      'You must update it manually, before running add_test_data.bat.'); 
end; 

function PythonVersionRequired: string;
begin
  Result := FmtMessage('%1.%2 (%1.%2.%3 or later)', [IntToStr(PythonMajorVersion),
    IntToStr(PythonMinorVersion), IntToStr(MinPythonMicroVersion)]);
end;

function IsProductionInstall: Boolean;
begin
  Result := WizardSetupType(False) = 'production';
end;

function IsDevelopmentInstall: Boolean;
begin
  Result := WizardSetupType(False) = 'development';
end;

procedure InitializeWizard();
var
  Page: TWizardPage;
begin
  Page := CreateOutputMsgMemoPage(wpWelcome, 'Verifying Prerequisites',
    'Setup is verifying prerequisites for installing.', 'Setup could not ' +
    'verify the installation prerequisites. Please read the following ' +
    'information. Then click Cancel if you need to exit.', '');
  PrereqErrorPageID := Page.ID;

  Page := CreateOutputMsgMemoPage(PrereqErrorPageID, 'Information', 
    'Please read the following important information before continuing.',
    'When you are ready to continue with Setup, click Next.',
    'This program will attempt to do the following:' + CRLF + CRLF +
    '1. Shut down all currently running Python processes.' + CRLF + CRLF +
    '2. Delete all Python scripts (*.py), including those not created by an ' +
    'installer, from the installation directories.' + CRLF + CRLF +
    '3. Delete other Python files (*.py?, e.g. *.pyc), including those not ' +
    'created by an installer, from the installation directories.');

  InputDataDirPage := CreateInputDirPage(wpSelectComponents,
    'Select Data Directory',
    'Where should writable data and configuration files be stored?',
    'Writable data and configuration files will be stored in the following ' +
    'folder.' + CRLF + CRLF +
    'To continue, click Next. If you would like to select a different folder' +
    ', click Browse.', False, 'New Folder');
  InputDataDirPage.Add('');
  
  CurrentAppDir := '?';
  CurrentDataDir := '?';
  PostInstallWarning := '';
end;

function CheckGeneralPrereqs: string;
var
  ResultCode: Integer;
  CommandLine: string;
begin
  Result := '';
  ExtractTemporaryFile('check_installer_prereqs.py');
  
  CommandLine := 'python.exe -h';
  if not Exec('>', CommandLine, '', SW_HIDE, ewWaitUntilTerminated, ResultCode)
    then begin
    Result := FmtMessage('Unable to run ''%1''. Python %2 is required, and ' +
      'should be in your path. Python is available here:' + CRLF + '%3', [
      CommandLine, PythonVersionRequired, PythonDownload]);
    Exit;
  end;
  
  CommandLine := ExpandConstant(Format('python.exe {tmp}\' +
    'check_installer_prereqs.py %d %d %d %d', [PythonMajorVersion,
    PythonMinorVersion, MinPythonMicroVersion, MinPyWin32Build]));
  if not Exec('>', CommandLine, '', SW_HIDE, ewWaitUntilTerminated, ResultCode)
    then begin
    Result := FmtMessage('Unable to run ''%1''. Python %2 is required, and ' +
      'may not be installed. Python is available here:' + CRLF + '%3', [
      CommandLine, PythonVersionRequired, PythonDownload]);
    Exit;
  end;
  
  if ResultCode <> 0 then begin  
    case ResultCode of
      11: Result := FmtMessage('Incorrect Python version found. Version %1 ' +
        'is required, and is available here:' + CRLF + '%2', [
        PythonVersionRequired, PythonDownload]);
      12: Result := FmtMessage('PyWin32 (build %1 or later) must be ' +
        'installed, but was not found. PyWin32 is available here:' + CRLF +
        '%2', [IntToStr(MinPyWin32Build), PyWin32Download]);
      13: Result := FmtMessage('Incorrect PyWin32 build found. Build %1 or ' +
        'later is required, and is available here:' + CRLF + '%2', [IntToStr(
        MinPyWin32Build), PyWin32Download]);
    else
      RaiseException(FmtMessage('Error running ''%1''.', [CommandLine]));
    end;
    Exit;
  end;
end;  

function ShouldSkipPage(PageID: Integer): Boolean;
var
  Msg: string;
begin
  Result := False;
  
  if PageID = PrereqErrorPageID then begin
    Msg := CheckGeneralPrereqs;
    if Msg = '' then
      Result := True
    else
      TOutputMsgMemoWizardPage(PageFromID(
        PrereqErrorPageID)).RichEditViewer.Lines.Text := Msg;
  end;

  if (PageID = InputDataDirPage.ID) and not IsDevelopmentInstall() then
    Result := True
end;

function BottomLevelDir(Dir: string): string;
var
  P: Integer;
begin
  repeat
    P := Pos('\', Dir);
    if P > 0 then
      Dir := Copy(Dir, P + 1, Length(Dir));
  until P = 0;
  Result := Dir;
end;

function NextButtonClick(CurPageID: Integer): Boolean;
var
  Dir: string;
begin
  Result := False;
  // todo: If the default app dir is selected, and it already exists, no warning
  // is given. This is inconsistent with data dir selection. Should one be
  // changed, to make them consistent?
  case CurPageID of
    wpSelectDir: begin
      Dir := ExpandConstant('{app}');
      if Dir <> CurrentAppDir then
        InputDataDirPage.Values[0] := ExpandConstant('{localappdata}') +
          '\UtiliQuest\Backend\' + BottomLevelDir(Dir);
      CurrentAppDir := Dir;
      Result := True;
    end;
  
    InputDataDirPage.ID:
      if InputDataDirPage.Values[0] = CurrentDataDir then
        Result := True
      else if (not DirExists(InputDataDirPage.Values[0])) or (MsgBox(
        'The folder:' + CRLF + CRLF + InputDataDirPage.Values[0] +
        CRLF + CRLF + 'already exists. Would you like to make this your data' +
        ' directory anyway?', mbConfirmation, MB_YESNO or MB_DEFBUTTON2) =
        IDYES) then begin
        CurrentDataDir := InputDataDirPage.Values[0];
        Result := True;
      end;
  else
    Result := True;
  end;
end;

procedure CurPageChanged(CurPageID: Integer);
begin
  case CurPageID of
    PrereqErrorPageID:
      WizardForm.NextButton.Enabled := False;
      
    wpReady:
      if IsDevelopmentInstall() then begin
        WizardForm.ReadyMemo.Lines.Insert(3, 'Data directory:');
        WizardForm.ReadyMemo.Lines.Insert(4, '      ' + CurrentDataDir);
        WizardForm.ReadyMemo.Lines.Insert(5, '');
      end;
    
    wpFinished:
      if PostInstallWarning <> '' then
        MsgBox('Warning:' + CRLF + CRLF + PostInstallWarning, mbError, MB_OK);
  end;
end;

function PrepareToInstall(var NeedsRestart: Boolean): string;
var
  ResultCode: Integer;
  CommandLine: string;
begin
  Result := '';

  ExtractTemporaryFile('tlist.exe');
  ExtractTemporaryFile('kill.exe');
  ExtractTemporaryFile('killall.py');
  ExtractTemporaryFile('listener.py');
  ExtractTemporaryFile('sendquitsignal.py');
  ExtractTemporaryFile('quitall.py');
    
  CommandLine := ExpandConstant('python.exe {tmp}\quitall.py -k');
  if not Exec('>', CommandLine, '', SW_HIDE, ewWaitUntilTerminated, ResultCode)
    then begin
    // todo: Should this raise an exception instead?
    Result := FmtMessage('Unable to run ''%1''.', [CommandLine]);
    Exit;
  end;

  if ResultCode <> 0 then begin    
    Result := 'Unable to shut down all Python processes.';
    Exit;
  end;
end;

