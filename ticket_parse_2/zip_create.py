# zip_create.py
# Create a zip file with all relevant Python files for distribution.  Must be
# run in the same directory where the Python files are.  The resulting zip file
# will have a name of the form "ticketparser-XYZ.zip", where XYZ is the
# current version number (as seen in version.py).

import os
import version
import zipfile

filename = "ticketparser-%s.zip" % (version.__version__,)

all_files = os.listdir(".")
files = [fn for fn in all_files
         if (fn.endswith(".py")
         or fn.endswith(".exe")
         or fn.endswith(".bat"))
         and not fn.startswith("test_")
         and not fn.startswith("demo_")]
files.extend([fn for fn in all_files
              if fn.startswith("sample")])

for subdir in ('callcenters', 'formats'):
    subfiles = os.listdir(subdir)
    subfiles = [fn for fn in subfiles
             if fn.endswith(".py")
             and not fn.startswith("test_")
             and not fn.startswith("demo_")]
    files.extend([os.path.join(subdir, fn) for fn in subfiles])

files.sort()

zf = zipfile.ZipFile(filename, 'w', zipfile.ZIP_DEFLATED)
for fn in files:
    print "Adding:", fn
    zf.write(fn)
zf.close()

print "Created:", filename
