# ticketupdateextractor.py
# Created: 2002.08.19, HN
#
# Special version of ticketextractor.py. In this case, we're scanning multiple
# log files for certain lines. These lines contain information about locates
# that need to be added to tickets. When/if found, the lines are stored
# verbatim in a file (with some "#" lines added to make sure the parser doesn't
# discard them as "invalid tickets").
#
# Everything else (interpreting the file, adding the locate) is done by the
# parser on the SW server. This script "merely" finds the relevant parts and
# uploads them.

import os
#
import ticketextractor

class TicketUpdateExtractor(ticketextractor.TicketExtractor):

    positions_file = "upd_positions.txt"    # different format!
    ids_file = "upd_ids.txt"

    prefix = "FCO1_merge"

    chunksize = 20

    def get_filenames(self, dir):
        """ Get the filenames we need to scan in a directory. Return fully
            qualified filenames (i.e., with pathname). """
        files = [f.lower() for f in os.listdir(dir)]
        files = [f for f in files if f.startswith("protocol")]
        return [os.path.join(dir, f) for f in files]

    def get_tickets(self, date):
        """ Get "tickets" from multiple files. """
        tickets = []
        dir = self.make_dirname(self.update_dir, date)
        filenames = self.get_filenames(dir)
        for filename in filenames:
            key = "%s:%s" % (date, os.path.split(filename)[1])
            t = ticketextractor.TicketExtractor.get_tickets(self, key,
             filename=filename)
            tickets.extend(t)
        return tickets

    def make_dirname(self, template, fdate):
        """ Return the name (with full path) of the file containing the tickets
            of a certain date. """
        d = fdate[:4] + fdate[5:7] + fdate[8:10]
        return template % d

    def filter_tickets(self, tickets):
        # add padding. this is necessary so the parser won't discard it as "not
        # a valid ticket".
        padding = "#\n" * 5
        for i in range(len(tickets)):
            tickets[i] = tickets[i] + padding

    # These methods can stay the same:
    # - __init__(beginmarker, endmarker)
    # - read_configuration(filename)
    # - get_id(date)
    # - write_tickets(tickets, date)
    # - run() -- maybe
    #   calls get_tickets, filter_tickets, write_tickets, upload_tickets
    # - upload_tickets()
    # - get_position(key)           # but use a different key
    # - set_position(key, value)    # ditto
    # - get_tickets(key)            # sort of... base method is called


if __name__ == "__main__":

    begin, end = "CALL up_MergeNotice(", "\n"

    te = TicketUpdateExtractor(begin, end)
    te.run()

