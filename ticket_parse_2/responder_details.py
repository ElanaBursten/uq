# responder_details.py
# Created: 14 Apr 2002, Hans Nowak
# Last update: 18 Apr 2002, Hans Nowak
#
# XXX XXX XXX NOW OBSOLETE!!

# NOTE:
# - These responder detail classes know nothing about configuration, except
#   what is passed to them by the caller.
# - They possibly don't need to know about querying the database either.

import telnetbot
import responder_mappings
import dbinterface_old as dbinterface

class ResponderError(Exception): pass

class BaseResponderHelper:
    """ Implements an abstract API for dealing with response sites. """

    def __init__(self, manual_mode=0):
        self.bot = None
        self.manual_mode = manual_mode

    def connect(self, site, port):
        self.bot = telnetbot.TelnetBot(site, port, timeout=5)  # timeout?

    def login(self, login, password): pass
    def logout(self): pass

    def process(self, locates):
        """ Main loop over all the locates. What happens with every locate,
            is left up to the subclass. """

    def do_manual(self):
        """ Called by subclasses for enforcing manual mode. """
        # XXX to be implemented

##
# Implementations

class AtlantaResponderHelper(BaseResponderHelper):

    def login(self, login, password):
        self.bot.read_until("220 GA response system ready - ver 2.0\r\n")
        self.bot.write("USER " + login + "\r\n")
        self.bot.read_until("password.\r\n")
        self.bot.write("PASS " + password + "\r\n")
        (idx, match, text) = self.bot.read_until_regex([
         "proceed.\r\n", "Not logged in.\r\n"])
        #self.bot.read_until("proceed.\r\n")
        if idx == 1:
            raise "Could not log in! Invalid password!"

        # XXX This should be replaced with read_until_regex calls... so we
        # can take appropriate action if a password isn't recognized.

    def logout(self):
        self.bot.write("QUIT\r\n")
        self.bot.read_all()

    def send_response(self, ticket, membercode, responsecode, explanation=None):
        s = "DATA %s,%s,UQ Responder,%s" % (ticket, membercode, responsecode)
        if explanation:
            s = s + ",%s" % (explanation)
        s = s + "\r\n"
        self.bot.write(s)
        resp = self.bot.read_until("\r\n") # read response
        # XXX somehow, we need to figure out whether the response was
        # accepted or not...!
        # that's easy... 220 is accepted... others are not...
        return resp # this'll do for now

    def send_response_status(self, ticket, clientcode, status):
        """ Send a response with a "status" (see responder_mappings.py). The
            status is mapped to a response code and explanation code. """
        try:
            z = responder_mappings.AtlantaResponderMappings[status]
        except KeyError:
            raise ResponderError, "Unknown status: %s" % (status)
        responsecode, explanation = z[1], z[2]
        return self.send_response(ticket, clientcode, responsecode, explanation)

    def process(self, locates):
        """ Process the whole 'locates' list. """
        for (ticket_number, client_code, status) in locates:
            self.send_response_status(ticket_number, client_code, status)

