# responder_ack.py
# Used for email responder (e.g. NCA1)

import filetools
import receiver

MAX_ROUNDS = 200
MAX_ACKS = 500


class ResponderAck:

    def __init__(self, tdb, log, ack_dir, ack_proc_dir, call_center,
                 configuration, accounts):
        self.tdb = tdb
        self.log = log
        self.ack_dir = ack_dir
        self.ack_proc_dir = ack_proc_dir
        self.call_center = call_center
        self.files_filenames = []
        self.receiver = receiver.Receiver(receiver.ReceiverConfiguration(configuration,
                                                                         self.call_center,
                                                                         accounts),
                                          receiver.ReceiverOptions())

    def get_ack_files(self):
        if self.ack_dir.strip():
            return filetools.get_filenames(self.ack_dir)
        else:
            # if self.ack_dir is empty, don't return anything
            return []

    def remove_ack_file(self, filename):
        self.log.log_event("Removing ack file: " + filename)
        filetools.remove_file(filename)

    def move_ack_file(self, filename):
        self.log.log_event("Moving ack file (%s) to %s" % (filename,
         self.ack_proc_dir))
        filetools.move_with_benefits(filename, self.ack_proc_dir)

    # get acknowledgement data (i.e., whatever the call center decides to
    # return to us)
    def get_ack_data(self, filename):
        lines = filetools.read_file_lines(filename)
        numbers = [line.split(",")[0].strip() for line in lines if line.strip()]
        return numbers

    # look up more data for ack records
    def find_resp_id(self, call_center, value):
        return self.tdb.find_response_ack_waiting(call_center, value)
        # raises KeyError if not found

    # update the responder log and whatever else we need to update
    def update_response_log(self, resp_id):
        self.tdb.update_response_log(resp_id, "acknowledged", 1)
        self.tdb.delete_response_ack_waiting(resp_id)

    def next_file_list(self):
        filenames = self.receiver.get_next_filename_list()

        if filenames or (filenames == 0):
            source = 'email'
        else:
            source = ''
            if not self.files_filenames:
                self.files_filenames = self.get_ack_files()
            if self.files_filenames:
                filenames = [self.files_filenames.pop()]
                source = 'file'

        return filenames, source

    def process_ack_entry(self, number, filename):
        try:
            resp_id = self.find_resp_id(self.call_center, str(int(number)))
        except KeyError:
            #Response not found in UQ system, probably processed already.
            self.log.log_event("No response_id found for: %s, %s " % (self.call_center, number))
        except ValueError:
            #Unable to process this entry, Indicator of a larger problem
            self.log.log_event("Invalid number for: %s, %s " % (self.call_center, number))
            self.log.log_event("Bailing out for file %s" % filename)
            return False
        else:
            self.update_response_log(resp_id)
            self.log.log_event("Response log updated for: %s, %s, %s" % (self.call_center,
                                                                         number, resp_id))
        return True

    def handle_acknowledgements(self):
        """ Top-level function to handle the retrieval, processing and
            deletion of acknowledgements. """

        rounds_completed = 0
        acks_processed = 0
        filenames, source = self.next_file_list()

        while (rounds_completed < MAX_ROUNDS) and (acks_processed < MAX_ACKS) and (filenames != []):
            # filenames = 0, an email was skipped. This is an unfortunate side effect
            # of next_file_list that should be ironed out.
            if filenames != 0:
                for filename in filenames:
                    self.log.log_event("Processing ack file: " + filename)

                    numbers = self.get_ack_data(filename)
                    for number in numbers:
                        # process_ack_entry can detect if the file is "corrupt" and will
                        # return false if this happens
                        if not self.process_ack_entry(number, filename):
                            break
                        acks_processed += 1

                    self.move_ack_file(filename)

                if source == 'email':
                    # Responsibility for all files has been accepted by the
                    # backend, so tell the receiver it's ok to delete the email
                    # that contained them.
                    self.receiver.confirm_filename_list()

            rounds_completed += 1

            filenames, source = self.next_file_list()