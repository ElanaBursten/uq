# configchecker.py
# Checks configuration issues.

import os
#
import call_centers
import errorhandling2

class ConfigurationError(Exception): pass

class ConfigChecker:

    def __init__(self, log=None):
        self.log = log

    def check(self, config):
        """ Check a Configuration object. """
        for process in config.processes:
            #incomingdir, format, processed, error, group = process
            incomingdir = process['incoming']
            format = process['format']
            processed = process['processed']
            error = process['error']

            for dir in incomingdir, processed, error:
                self._check(self.check_directory, dir)

            self._check(self.check_call_center, format)

            # TODO: test more...

        if self.log:
            self.log.force_send()

    def check_directory(self, dir):
        if not os.path.exists(dir):
            raise ConfigurationError("Directory %s doesn't exist" % dir)

    def check_call_center(self, call_center):
        if call_center and (call_center not in call_centers.cc2format.keys())\
        and (call_center not in call_centers.work_order_centers.keys()):
            raise ConfigurationError("Call center %s doesn't exist" %
             call_center)

    def _check(self, f, *args):
        try:
            f(*args)
        except ConfigurationError, e:
            # if we have a ErrorHandler object, we dump/send/write the error and move
            # on. otherwise, we raise an exception.
            ep = errorhandling2.errorpacket()
            if self.log:
                self.log.log(ep, send=1, write=1, dump=1)
            else:
                raise

