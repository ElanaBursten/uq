# update_call_centers.py
# Information about update call centers.
# Created: 2003.01.13 HN

import callcenters # package
#
import string

class UpdateDataError(Exception):
    """ Raised when no UpdateData class can be found for a given call center,
        or when there is a problem with said data. """

# XXX OBSOLETE?
def get_updatedata(call_center):
    """ Try to get and instantiate an UpdateData class for the given call
        center.  If not found, raise an UpdateDataError.  Looks for the given
        class in two ways: in the namespace of this module, and in the
        'callcenters' package.
        NOTE: The first lookup method is obsolete now, but I'll leave it in
        because it's useful for testing.
    """
    if call_center[0] not in string.letters:
        call_center = 'c' + call_center
    name = call_center + "UpdateData"
    try:
        klass = globals()[name]
    except KeyError:
        try:
            klass = callcenters.get_updatedata(call_center)
        except:
            raise UpdateDataError, "No update data for %s" % (call_center,)
        else:
            return klass()
    else:
        return klass()

class UpdateData:
    fields = []



