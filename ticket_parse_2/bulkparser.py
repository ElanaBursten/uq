# bulkparser.py
# Tool for parsing a lot of tickets and reporting any errors.  To be used for
# testing purposes, as a stand-alone program.
# Created: 05 Apr 2002, Hans Nowak

import getopt
import glob
import sys
#
import config
import datadir
import errorhandling2
import ticket as ticketmod
import ticketparser
import ticketloader
import ticketsummary

__usage__ = """\
bulkparser.py [options] files...

Parse a bulk of files containing tickets. Flag tickets that could not be
parsed.
File specifications may have wildcards.

Options:
    -f format   Assume that the tickets are of the specified format.
    -C center   Assume that the tickets are for the given call center.
"""

class BulkErrorHandler(errorhandling2.ErrorHandler):
    def __init__(self, logfilename):
        errorhandling2.ErrorHandler.__init__(self, logdir="logs", me="BulkParser")
        self.logger = errorhandling2.Logger("", "BulkParser")

class BulkParser:

    def __init__(self, logfilename="logs/bulkparser-log.txt", formats=[]):
        self.handler = ticketparser.TicketHandler()
        self.config = config.getConfiguration()
        self.log = BulkErrorHandler(logfilename)

        self.numprocessed = 0
        self.numerrors = 0

        self.numtickets = 0
        self.numsummaries = 0
        self.nummessages = 0
        self.numunknown = 0

        self.suggestion = formats

    def parse_all(self, filenames):
        for filename in filenames:
            self.parse(filename)

    def parse(self, filename):
        print "Processing:", filename
        loader = ticketloader.TicketLoader(filename)
        raw = loader.getTicket()
        count = 1
        while raw:
            try:
                ticket = self.handler.parse(raw, self.suggestion)
            except:
                print "Error: file %s, ticket #%d" % (filename, count)
                ep = errorhandling2.ErrorPacket()
                ep.set_traceback()
                ep.add(filename=filename, raw_ticket=raw, count=count)
                self.log.log(ep, write=1)
                self.numerrors += 1
            else:
                if isinstance(ticket, ticketsummary.TicketSummary):
                    self.numsummaries += 1
                elif isinstance(ticket, ticketmod.Ticket):
                    self.numtickets += 1
                elif isinstance(ticket, ticketparser.GMMessage):
                    self.nummessages += 1
                else:
                    self.numunknown += 1

            raw = loader.getTicket()
            count = count + 1
            self.numprocessed += 1


if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "C:f:?")

    format = []
    for o, a in opts:
        if o == "-f":
            formats = a.split(";")
        elif o == "-C":
            import call_centers
            formats = call_centers.cc2format[a]
        elif o == "-?":
            print >> sys.stderr, __usage__
            sys.exit(0)

    files = []
    for arg in args:
        files.extend(glob.glob(arg))

    bp = BulkParser(formats=formats)
    bp.parse_all(files)
    print bp.numprocessed, "tickets processed,", bp.numerrors, "errors"
    print bp.numtickets, "tickets,", bp.numsummaries, "summaries,", \
     bp.nummessages, "messages,", bp.numunknown, "unknown"

