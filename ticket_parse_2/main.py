# main.py
# Utiliquest Ticket Parser Main program

from __future__ import with_statement
import gc
import getopt
import os
import random
import socket
import smtplib
import string
import subprocess
import sys
import tempfile
import time
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
#
import attachment
import call_center_adapter
import call_centers
import centerpoint
import config
import datadir
import date
import date_special
import dbinterface_ado
import dbinterface_old as dbinterface
import duplicates
import emailtools
import errorhandling2
import errorhandling_special
import et_tools
import event_log
import feedhandler
import filetools
import geocoder
import getlogic as getlogic_mod # so getlogic_mod.geocoder can be set
import highprofilegrids
import highprofileaddress
import listener
import locate_status
import mail2
import msmq
import mutex
import noclienthandler
import program_status
import qminterface
import QMLogic2ServiceLib
import receiver
import router
import statuslist
import summarycollection
import ticket as ticketmod
import ticket_alert
import ticket_db
import ticket_grid
import ticket_xml
import ticketparser
import ticketsummary
import tools
import ticketloader
import ticketdb_decorator
from transaction import Transaction
import update_call_centers
import version
import windows_tools
import work_order
import work_order_audit
from sqlgatherer import SQLGathererError
from show_profile_report import show_profile_report
from getlogic import getlogic
from work_order_version import WorkOrderVersion
from check_python_version import check_python_version
from ticketfetcher import TicketFetcherFile, TicketFetcherEmail

# todo(dan) Change '0' to something useful?
__version__ = '0'

MAX_PER_CALL_CENTER = 500
MAX_PROCESSING_ATTEMPTS = 3 # max 2 immediate retries for certain exceptions

gc.enable()
gc.set_threshold(50000, 10, 10)

__usage__ = """\
main.py [options] call_center

Main ticket parser program.

Options:
    -1          Run once.
    -r N        Reparse tickets in error directories; ignore tickets in
                incoming directories. Only reparse tickets up to N days old.
    -c cfgfile  Specify an alternate configuration file. (default: config.xml)
    -p          Turn on profiling. Writes file main_profiler.txt.
    -m          Don't move files to processed directory.
    -q          Don't send any notification emails.
    -x          Route as we parse.
"""

WAIT = 60   # FIXME: Should be in config file?

class Error(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, args)
        self.data = kwargs # included in error reports

class NonrepeatedReadError(Error):
    pass

class UnroutedWorkOrderError(Error):
    pass

class Main:
    """ Main ticket processor. Check periodically for raw ticket files
        in the incoming directories, and attempt to process them.
    """

    _flag_non_clients = True and False
    # if true, non-UQ-clients are set to -N rather than -P when inserted
    # (update is currently unaffected)

    def __init__(self, configfile="", call_center=None, verbose=1, debug=0,
     dbado=None):
        self.runonce = 0
        self.reparse = 0    # are we in reparse mode?
        self.n = 0
        self.verbose = verbose
        self.call_center = call_center
        self.debug = debug

        if not self.call_center:
            print >> sys.stderr, "Cannot run Main without a valid call center"
            sys.exit(0)

        tools.check_python_version()

        if configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                      "Configuration already specified"
            self.config = config.Configuration(configfile)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()
            
        # these will use the same configuration as Main:
        if dbado:
            self.dbado = dbado
        else:
            self.dbado = dbinterface_ado.DBInterfaceADO()
        self.tdb = ticket_db.TicketDB(dbado=self.dbado)
        self.tdb = ticketdb_decorator.TicketDBDecorator(self.tdb,
                   ticketdb_decorator.TicketDBMethodDecorator)
        self.ids = []   # IDs of tickets that were stored
        self.holiday_list = date_special.read_holidays(self.tdb)
        self.emails = emailtools.Emails(self.tdb)

        # create and configure the logger
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                                               smtpinfo=self.config.smtp_accs[0],
                                               me="Ticketparser-" + self.call_center,
                                               cc_emails=self.emails,
                                               admin_emails=self.config.admins,
                                               subject="TicketParser Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = self.verbose
        if self.reparse:
            self.log.lock = 1   # do not send notification email

        email_log_file_name = "Ticketparser-Email-" + self.call_center
        self.email_error_handler = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                                                               me=email_log_file_name)
        self.email_error_handler.lock = 1

        self.handler = ticketparser.TicketHandler()
        self.feedhandler = feedhandler.FeedHandler(self.config, self.log,
         self.call_center, suggested_format='', reparse_days=0,
         verbose=verbose)
        self.ticketfetcher = TicketFetcherFile(self.feedhandler,
         self.call_center)
        self.formats = self.feedhandler.get_call_center_formats(self.call_center)
        self.sc = summarycollection.SummaryCollection(self.tdb,
         self.call_center, self.formats, self.handler.parse)
        self.logic = {} # collection of BusinessLogic instances
        self.call_center_process = self.config.call_center_process(
         self.call_center)
        self.geocoder = geocoder.Geocoder(self.config.geocoder, self.log)
        getlogic_mod.geocoder = self.geocoder
        self.event_log = event_log.EventLog(self.tdb, self.log,
         store_events=self.config.store_events)

        self.errcount = 0
        # number of tickets that could not be posted for the current file.
        # this is used to generate a unique filename.

        self.dont_move = 0  # default: do move files

        self.say("Retrieving clients...")
        self.clients = self.tdb.get_clients_dict()
        self.sayln("OK")
        self.noclienthandler = noclienthandler.NoClientHandler(self.tdb,
                               self.clients, self.log, emails=self.emails,
                               verbose=self.verbose)
        self.df = duplicates.DuplicateFinder(self.tdb)

        self.say("Loading call center info...")
        self.call_centers = call_centers.get_call_centers(self.tdb)
        self.sayln("OK")

        # verify that call center exists
        if not debug:
            assert self.call_center in self.call_centers.get_all_call_centers(), \
                   "Unknown call center: %s" % (self.call_center,)

        self.say("Retrieving high profile grids...")
        self.hpgrid = highprofilegrids.HighProfileGrids(self.tdb)
        self.hpaddress = highprofileaddress.HighProfileAddress(self.tdb)
        self.sayln("OK")

        self.say("Loading statuslist...")
        self.statuslist = statuslist.StatusList(self.tdb)
        self.sayln("OK")

        self.ticket_alert_keywords = self.tdb.get_ticket_alert_keywords()
        self.ticketalerthandler = ticket_alert.TicketAlertHandler(self.tdb,
          self.clients, self.hpgrid, self.hpaddress, self.ticket_alert_keywords)

        try:
            qminterface.write_utility_ini()
        except:
            ep = errorhandling2.ErrorPacket()
            ep.set_traceback()
            ep.add(info="Could not write custom utility.ini to data dir")
            self.log.log(ep, send=1, dump=1 and self.verbose, write=1)

        self.router = router.Router(self.tdb, self.log, self.holiday_list,
                      self.clients, self.verbose)

        receiver_options = receiver.ReceiverOptions()
        receiver_config = receiver.ReceiverConfiguration(
          self.config, self.call_center, self.call_center_process.get('accounts', []),
          self.call_center_process.get('db_accounts', []))
        self.receiver = receiver.Receiver(receiver_config, receiver_options)

    def say(self, *args):
        if self.verbose:
            for arg in args:
                print arg,

    def sayln(self, *args):
        if self.verbose:
            self.say(*args)
            print

    def check_connection(self):
        """ Test if we have a connection with the database. """
        try:
            self.tdb.check_connection()
        except:
            ep = errorhandling2.ErrorPacket()
            ep.set_traceback()
            ep.add(info="check_connection: Database connection failed")
            self.log.log(ep, send=1, dump=1 and self.verbose, write=1)
            return 0    # no, or invalid, connection
        else:
            return 1    # connection is OK

    # todo(dan) Review where this is getting called
    def dropticket(self, rawdata, errordir, oldname):
        """ Drop the current ticket (passed as raw data) in the error
            directory. The new file will get a unique filename, consisting
            of the old "ticket source" filename, plus an ID.
        """
        self.errcount = self.errcount + 1   # generate new "ID"

        # generate unique filename, based on original filename
        filename = os.path.split(oldname)[1]   # remove pathname
        newfilename = filename + ".%03d" % self.errcount
        newfilename = os.path.join(errordir, newfilename)
        with open(newfilename, "w") as f:
            f.write(rawdata)
        self.sayln(newfilename, "written.")

    def parseticket(self, raw_ticket, fileinfo):
        """ Try to parse a ticket. If this fails, catch the exception and
            return None to indicate failure. """
        ticket = None
        exclude = []

        if fileinfo.formats:
            exclude = sorted(self.get_exclude_list(self.call_center))
            self.log.log_event("Exclude list for %s: %s" % (
             self.call_center, exclude))

        try:
            # call TicketHandler instance to parse the ticket, and pass in a
            # format "suggestion" if we have any
            ticket = self.handler.parse(raw_ticket,
                     suggested_formats=fileinfo.formats,
                     exclude=exclude,
                     call_center=self.call_center)
        except:
            # There are apparently serious problems with the ticket.
            # Possible reasons include: error during parsing; not
            # recognized; no ticket_number; etc.
            is_linenoise = tools.is_linenoise(raw_ticket, 0.25)

            self.log.log_event('Problem parsing ticket:')
            ep = errorhandling2.errorpacket(rawticket=raw_ticket,
                 info="Problem parsing ticket", filename=fileinfo.fullname)
            if is_linenoise:
                ep.add(linenoise_info="Possibly line noise")
            send = 1
            if tools.is_spam(raw_ticket):
                ep.add(spam_info="Probable spam")
                send = 0
            self.log.log(ep, send=send, write=1, dump=1 and self.verbose)
            # XXX problem w/line noise handling: this probably *queues* the
            # message, rather than sending it directly.
            if not self.reparse:
                self.dropticket(raw_ticket, fileinfo.error, fileinfo.fullname)

        return ticket

    @staticmethod
    def setcallcenter(tick, call_center):
        if call_center:
            if isinstance(tick, ticketmod.Ticket):
                tick.ticket_format = call_center
                if not tick.source:
                    tick.source = call_center
            elif isinstance(tick, ticketsummary.TicketSummary):
                tick.call_center = call_center

    def process_file(self, raw_ticket, fileinfo):
        """ Process (parse) part of a file, which can contain a ticket, a
            summary, etc.
            <raw_ticket> is the raw ticket data (as a string); <fileinfo> is
            an instance of feedhandler.FileInfo.
        """
        # try to parse the "ticket". this may be an actual ticket, an audit, a
        # message, a work order, etc.
        ticket = self.parseticket(raw_ticket, fileinfo)
        if not ticket:
            return

        if isinstance(ticket, ticketmod.Ticket):
            try:
                self.process_ticket(ticket, fileinfo.call_center, raw_ticket,
                 fileinfo.fullname, fileinfo.error, fileinfo.filedate,
                 fileinfo.attachment_dir, fileinfo.attachment_proc_dir,
                 fileinfo.attachment_user)
            except Error as e:
                e.data['ticket'] = ticket.repr()
                raise

        elif isinstance(ticket, ticketsummary.TicketSummary):
            self.process_summary(ticket, fileinfo.call_center, raw_ticket,
             fileinfo.fullname, fileinfo.error)

        elif isinstance(ticket, ticketparser.GMMessage):
            self.process_message(ticket, fileinfo.call_center)

        elif isinstance(ticket, work_order.WorkOrder):
            self.process_work_order(ticket, fileinfo)

        elif isinstance(ticket, work_order_audit.WorkOrderAuditHeader):
            self.process_work_order_audit(ticket, fileinfo)

        else:
            raise ValueError("Unknown ticket type: %s" % ticket.__class__)

        # process_file ends here. This is all we should do in this
        # method. (KISS principle)

    def process(self, fileinfo):

        if not tools.can_rename(fileinfo.fullname):
            self.log.log_event("File '%s' cannot be opened for reading"\
             " -- deferred" % fileinfo.fullname, dump=1)
            return

        tl = ticketloader.TicketLoader(fileinfo.fullname)
        numids = len(self.ids)  # number of IDs before trying

        for raw in tl.tickets:
            attempt = 0
            while True:
                attempt += 1
                try:
                    self.process_file(raw, fileinfo)
                    break
                except Error as e:
                    e.data['raw_data'] = raw
                    e.data['filename'] = fileinfo.fullname
                    if isinstance(e, NonrepeatedReadError):
                        if attempt < MAX_PROCESSING_ATTEMPTS:
                            self.log.log_event(
                              'Attempt %s to process file, failed with: %s. '
                              'Will retry now.' % (attempt, repr(e)))
                        else:
                            self.log.log_exception(
                              'Final attempt to process file failed',
                              send_mail=1)
                            if self.reparse or (fileinfo.error is None):
                                raise # todo(dan) move logging to where this is handled
                            self.dropticket(raw, fileinfo.error, fileinfo.fullname)
                            break
                    else:
                        raise
                    
        self.files_accepted += 1

        # move the file to processed directory, except when reparsing
        if self.reparse:
            if len(self.ids) > numids:
                # apparently we could post something, success!
                os.remove(fileinfo.fullname)
                self.sayln(fileinfo.fullname, "deleted after successful processing.")
        elif not self.dont_move:
            # move to processed directory
            # XXX should be handled by TicketFetcher cleanup
            filetools.move_with_benefits(fileinfo.fullname, fileinfo.processed)

    def initialize_round_segment(self, round, segment):
        self.n = 0

        # before doing anything else, test database connection
        self.log.log_event("Start of ticketparser (svn:%s) run #%d(%s) (call center %s)"
          % (version.__version__, round, segment, self.call_center))
        self.tdb.dbado.timed_events = []
        if not self.check_connection():
            self.log.log_event("Problems with database connection.", dump=1)
            return 0

        self.round_start_time = time.time()
        self.round_db_time_elapsed = self.tdb.profiler.time_elapsed()
        self.round_db_accessed_times = len(self.tdb.profiler.data)
        return 1

    def finalize_round_segment(self, round, segment):
        elapsed = time.time() - self.round_start_time
        s = "%d tickets processed in %.1f seconds (%.1f in database)" % (
            self.n, elapsed, self.tdb.profiler.time_elapsed() - self.round_db_time_elapsed)
        self.log.log_event(s, self.verbose)
        s = "(database accessed %d times)" % (
         len(self.tdb.profiler.data) - self.round_db_accessed_times)
        self.log.log_event(s, self.verbose)

        self.log.force_send()
        self.log.log_event("End of ticketparser run #%d(%s)" % (round, segment))

    def process_all_files(self, files, _round):
        # <files> is a list of FileInfo objects
        for i, fileinfo in enumerate(files):
            age = (time.time() - time.mktime(fileinfo.time_t9)) / 60.0

            self.log.log_event(">>> %s (%d/%d, %.1f minutes old)" % (
             fileinfo.fullname, i+1, len(files), age))
            try:
                self.process(fileinfo)
                self.check_incomplete_summaries()
                self.send_no_uq_clients_warnings()

            except (dbinterface.DBFatalException, socket.error):
                ep = errorhandling2.errorpacket()
                ep.add(incoming_dir=fileinfo.incoming,
                       processed_dir=fileinfo.processed,
                       error_dir=fileinfo.error,
                       filename=fileinfo.filename)
                send = 1
                # Check that the value has a find method (string)
                if hasattr(ep.exception.args[0], "find"):
                    # if the db exception is a deadlock, don't send
                    if ep.exception.args[0].find('was deadlocked on lock resources') != -1 or \
                       ep.exception.args[0].find('Internal Server Error') != -1:
                        send = 0
                self.log.log(ep, send=send, write=1, dump=1 and self.verbose)
                break   # fatal error; do not continue loop

            except smtplib.SMTPException, e:
                # might happen when trying to send notification
                ep = errorhandling2.errorpacket()
                ep.add(incoming_dir=fileinfo.incoming,
                       processed_dir=fileinfo.processed,
                       error_dir=fileinfo.error,
                       filename=fileinfo.filename)
                self.log.log(ep, send=0, write=1, dump=1 and self.verbose)

            except Exception, e:
                ep = errorhandling2.errorpacket()
                ep.add(incoming_dir=fileinfo.incoming,
                       processed_dir=fileinfo.processed,
                       error_dir=fileinfo.error,
                       filename=fileinfo.filename)
                self.log.log(ep, send=1, write=1, dump=1 and self.verbose)

            # check for 'quit' signal
            if 'quit' in listener.uqlistener.listen():
                self.log.log_event("listener: 'quit' signal received, exiting")
                break

    def run(self, runonce=0):
        tools.log_info(self.log, "main.py")
        if not self.call_center:
            print >> sys.stderr, "No call_center specified"
            # this is not an error that should be logged, I think -- it's just
            # to make sure that we set a center when *testing* Main (as opposed
            # to normal usage)

        # parser runs <numrounds> times.  after every turn, it waits <wait>
        # seconds to allow new tickets to come in.
        numrounds = self.config.ticketparser['numrounds']
        wait = self.config.ticketparser['wait']

        for _round in range(numrounds):
            # XXX this should become: grab the next ticket; if not found, wait
            # a bit; otherwise process it & clean up, repeat
            
            check_file_source = True
            
            if not self.reparse:
                try:
                    if self.initialize_round_segment(_round, 'email source'):
                        for _ in range(MAX_PER_CALL_CENTER):
                            filenames = self.receiver.get_next_filename_list()

                            if filenames == 0:
                                check_file_source = False
                                continue

                            if not filenames:
                                break

                            # todo(dan) Refactor to include the relevant stuff
                            # in receiver.get_next_filename_list, and eliminate
                            # this step? We already know where the files are,
                            # so we don't need to find them again.
                            files = self.feedhandler.get_all_files(filenames)
                            if not files:
                                break

                            num_files = len(files)
                            self.files_accepted = 0
                            self.process_all_files(files, _round+1)
                            if self.files_accepted >= num_files:
                                self.receiver.confirm_filename_list()
                            check_file_source = False
                            
                        self.finalize_round_segment(_round, 'email source')
                except receiver.HeaderDelimeterError:
                    raise
                except:
                    ep = errorhandling2.errorpacket()
                    ep.add(info="Uncaught toplevel error")
                    self.log.log(ep, send=1, write=1, dump=1)
                    break

                try:
                    if self.initialize_round_segment(_round, 'database source'):
                        for _ in range(MAX_PER_CALL_CENTER):
                            item_id, filename = \
                             self.receiver.get_next_incoming_item(self.dbado)

                            if not filename:
                                break

                            files = self.feedhandler.get_all_files([filename])
                            if not files:
                                break

                            num_files = len(files)
                            self.files_accepted = 0
                            self.process_all_files(files, _round+1)
                            if self.files_accepted >= num_files:
                                self.receiver.delete_incoming_item(self.dbado,
                                 item_id)
                            check_file_source = False
                            
                        self.finalize_round_segment(_round, 'database source')
                except:
                    # todo(dan) Check for connection error in QMAN-3028
                    ep = errorhandling2.errorpacket()
                    ep.add(info="Uncaught toplevel error")
                    self.log.log(ep, send=1, write=1, dump=1)
                    break
                    
            if check_file_source:
                files = self.feedhandler.get_all_files()
                try:
                    if self.initialize_round_segment(_round, 'file source'):
                        self.files_accepted = 0
                        self.process_all_files(files, _round+1)
                        self.finalize_round_segment(_round, 'file source')
                except:
                    ep = errorhandling2.errorpacket()
                    ep.add(info="Uncaught toplevel error")
                    self.log.log(ep, send=1, write=1, dump=1)
                    break

            # check for 'quit' signal
            if 'quit' in listener.uqlistener.listen():
                self.log.log_event("listener: 'quit' signal received, exiting")
                break

            if runonce:
                break
            # wait <wait> seconds, unless this is the last round.
            # breaking out with Ctrl-C is possible here.
            if _round + 1 < numrounds:
                try:
                    if wait == -1:
                        interval = random.randint(30, 60)
                    else:
                        interval = wait
                    self.say("Waiting", interval, "seconds...")
                    start = time.time()
                    while (time.time() - start) < interval:
                        time.sleep(1)
                        # check for 'quit' signal
                        if 'quit' in listener.uqlistener.listen():
                            self.log.log_event("listener: 'quit' signal received, exiting")
                            raise SystemExit
                    self.sayln("OK")
                except IOError:
                    raise SystemExit
                except KeyboardInterrupt:
                    break

    def check_incomplete_summaries(self):
        """ Check for incomplete summaries that are "done" (i.e. won't receive
            any parts anymore) and post them. Always notify the admin of this
            event.
        """
        z = self.sc.find_incomplete_and_done()
        for call_center, client_code, timestamp in z:
            self.log.log_event("Incomplete summary for %s/%s found in "\
             "queue" % (call_center, client_code))
            parts = self.sc.get(call_center, client_code)
            parts_raw = string.join([p.image for p in parts], "\n---\n")
            try:
                with Transaction(self.tdb.dbado):
                    ids = self.tdb.storesummaries(parts, self.log)
                    self.sc.remove_summaries_from_db(call_center, client_code)
                self.sc.remove_summaries_from_dict(call_center, client_code)
                self.ids.extend(ids)
            except:
                ep = errorhandling2.errorpacket(summaries=parts_raw,
                 call_center=call_center, client_code=client_code)
                self.log.log(ep, send=1, write=1, dump=self.verbose)
            else:
                ep = errorhandling2.ErrorPacket()
                # do NOT fill traceback... no error occurred!
                ep.add(summaries=parts_raw, call_center=call_center,
                 client_code=client_code, info="Incomplete summary sent!",
                 id=ids[0])
                self.log.log(ep, send=1, write=1, dump=self.verbose)

    def isrenotification(self, ticket):
        logic = getlogic(ticket.ticket_format, self.logic, self.tdb,
                self.holiday_list)
        return logic.isrenotification(ticket)

    def is_no_show(self, ticket):
        logic = getlogic(ticket.ticket_format, self.logic, self.tdb,
                self.holiday_list)
        return logic.is_no_show(ticket)

    def send_message(self, message, call_center):
        """ Messages are no longer ignored, but are sent to a certain list of
            email addresses (depending on call center).
            If the call center cannot be determined, it's an error; also if no
            email addresses are defined.
        """
        if self.log.lock:
            return  # do not send anything

        # determine the call center, if not already known
        if not call_center:
            call_center = call_centers.format2cc.get(message.format, None)
            if not call_center:
                raise ValueError("Call center not known for this message")

        # get the email addresses for this call center
        #emails = self.message_emails.get(call_center, []) \
        # or self.admin_emails.get(call_center, [])
        emails = self.emails[call_center].message \
                 or self.emails[call_center].admin
        if not emails:
            raise ValueError("No message/admin emails for call center %s" %
             call_center)

        # create message and send it to all email addresses
        subject = "%s message" % (call_center)
        try:
            mail2.sendmail(self.config.smtp_accs[0], emails,
             subject, message.image)

            self.log.log_event("%s message sent to %s" % (
              call_center, string.join(emails, ", ")), dump=1 and self.verbose)
        except socket.timeout:
            self.log.log_event("Could not send email (timeout)")

    EXCLUDE_FOR_REQUEUEING = ["-R", "-N", "-P"]

    def add_locates_to_responder_queue(self, ticket_id):
        locates = self.tdb.get_locates(ticket_id)
        count = 0

        for row in locates:
            if row["status"] in self.EXCLUDE_FOR_REQUEUEING:
                continue
            locate_id = row["locate_id"]
            self.tdb.insert_responder_queue(locate_id)
            count = count + 1
            s = "Requeuing ticket_id %s, locate_id %s (%s) with status %r" % (
                ticket_id, locate_id, row['client_code'], row['status'])
            self.log.log_event(s)

        self.sayln(count, "locates added to responder queue for no show",
         "ticket", ticket_id)


    # for now, this only covers LAM01; code should be made more general
    # if/when we get more WO call centers
    def process_work_order(self, work_order, fileinfo):
        self.log.log_event("Processing work order: %s" % work_order.wo_number)

        # set client_id; there should be only one client for LAM01
        try:
            work_order.client_id = self.clients['LAM01'][0].client_id
        except:
            self.log.log_event("Could not set client_id for work order")
            raise

        if not work_order.transmit_date:
            work_order.transmit_date = (
              tools.extract_date_from_filename(fileinfo.filename)
              or work_order.call_date)

        # set due date; due date on work order is not correct
        import duedatecatalog
        ddc = duedatecatalog.DueDateCatalog(work_order.wo_source,
              self.holiday_list)
        td = date.Date(work_order.transmit_date)
        dd = ddc.inc_n_days(td, 1)
        work_order.due_date = td.isodate()
        work_order._filedate = fileinfo.filedate # used for versioning

        # check if a work order with this number already exists; if so, update
        # it; otherwise insert it
        try:
            existing_wo = work_order.__class__.find(self.tdb,
                          work_order.wo_source, work_order.wo_number)
        except ValueError:
            existing_wo = None

        if existing_wo:
            # work order with this number exists; update it
            work_order.wo_id = existing_wo.wo_id
            try:
                work_order.update(self.tdb, force=1)
            except:
                ep = errorhandling2.errorpacket()
                ep.add("Error processing work order")
                self.log.log(ep, send=1, dump=1 and self.verbose, write=1)
                raise
            else:
                self.log.log_event("Updated work order with id: %s" %
                  work_order.wo_id)
        else:
            # insert new work order
            try:
                work_order.insert(self.tdb)
            except:
                ep = errorhandling2.errorpacket()
                ep.add("Error processing work order")
                self.log.log(ep, send=1, dump=1 and self.verbose, write=1)
                raise
            else:
                self.log.log_event("Inserted work order with id: %s" %
                  work_order.wo_id)

        # add a record to work_order_version
        # NOTE: CONDITIONAL until Mantis #2841 has been deployed
        try:
            import tabledefs
            if tabledefs.table_exists(self.tdb, 'work_order_version'):
                WorkOrderVersion.add_version(self.tdb, work_order.wo_id,
                 work_order, fileinfo.filename)
        except:
            ep = errorhandling2.errorpacket()
            ep.add(work_order=work_order.long_repr(),
                   info="Problem storing work order version",
                   filename=fileinfo.filename)
            self.log.log(ep, send=1, dump=1 and self.verbose, write=1)
            raise

        # handle attachments that came with the email, if any
        if (fileinfo.attachment_dir and fileinfo.attachment_user):
            try:
                self.handle_attachments(work_order, fileinfo.filename,
                  fileinfo.attachment_dir, fileinfo.attachment_proc_dir,
                  fileinfo.attachment_user)
            except:
                ep = errorhandling2.errorpacket()
                ep.add(work_order=work_order.long_repr(),
                       info="Problem storing attachment",
                       filename=fileinfo.filename,
                       attachment_dir=fileinfo.attachment_dir,
                       attachment_user=fileinfo.attachment_user)
                self.log.log(ep, send=1, dump=1 and self.verbose, write=1)
                raise

    def process_work_order_audit(self, work_order_audit, fileinfo):
        self.log.log_event("Processing work order audit: %s / %s" %
          (work_order_audit.wo_source, work_order_audit.summary_date))
        try:
            work_order_audit.insert(self.tdb)
        except:
            ep = errorhandling2.errorpacket()
            ep.add("Error processing work order audit")
            self.log.log(ep, send=1, dump=1 and self.verbose, write=1)
        else:
            self.log.log_event("Inserted work order audit with id: %s" %
              work_order_audit.wo_audit_header_id)

    def process_message(self, tick, call_center):
        try:
            self.send_message(tick, call_center)
        except:
            ep = errorhandling2.errorpacket(raw_message=tick.image,
                 fixed_call_center=call_center, format=tick.format)
            self.log.log(ep, send=1, write=1, dump=1 and self.verbose)

    def process_summary(self, tick, call_center, raw_ticket, filename,
                        errordir):

        self.setcallcenter(tick, call_center)
        logic = getlogic(tick.call_center, self.logic, self.tdb,
                self.holiday_list)

        # send summary to certain emails
        self.send_summary(tick)

        # check for duplicate summaries
        if not logic.allow_summary_updates():
            s_dups = self.tdb.find_duplicate_summaries(tick)
            if s_dups:
                # only send an error message if the summary is non-empty
                if tick.data:
                    # do not post duplicate summaries; complain if we find one
                    try:
                        s = "Summary %s/%s/%s already exists" % (
                         tick.call_center, tick.client_code, tick.summary_date)
                        raise Exception, s
                    except:
                        ep = errorhandling2.errorpacket()
                        ep.add(info=s)
                        ep.add(filename=filename)
                        self.log.log_warning(ep, call_center=tick.call_center,
                         send=1, write=1, dump=1)
                        return
                else:
                    # summary is empty, don't send an error, but log it
                    s = "Summary %s/%s/%s already exists (duplicate empty)" % (
                     tick.call_center, tick.client_code, tick.summary_date)
                    self.log.log_event(s)
                    return

        self.log.log_event("Processing summary (%s, %s) (%s)" % (
         tick.call_center, tick.client_code,
         tick._has_header and "new" or "updated"))
        self.log.log_event("  (%d tickets found on summary)" % (
         tick.expected_tickets,))

        # add summary to collection, then possibly send if complete
        try:
            lenids = len(self.ids)
            self.sc.add(tick)
            ids = self.tdb.check_complete_summaries(self.sc, self.log)
            self.ids.extend(ids)
            # do not check incomplete summaries here!
            if len(self.ids) > lenids:
                return self.ids[-1]
            else:
                _id = -1 # mainly here so we have an id...
        except (dbinterface.DBException, dbinterface.RunSQLException):
            self.sayln('Problem processing summary:')
            ep = errorhandling2.errorpacket()
            ep.add(rawticket=raw_ticket, ticket=tick.repr(),
             info="Problem posting summary", filename=filename)
            self.log.log(ep, send=1, dump=1 and self.verbose, write=1)
            if not self.reparse:
                self.dropticket(raw_ticket, errordir, filename)
        else:
            self.n = self.n + 1
            self.ids.append(_id)
            
    def process_ticket(self, ticket, call_center, raw_ticket, filename,
                       errordir, filedate, attachment_dir="",
                       attachment_proc_dir="", attachment_user=0):

        self.setcallcenter(ticket, call_center)
        logic = getlogic(ticket.ticket_format, self.logic, self.tdb,
                self.holiday_list)

        self.set_source(ticket, logic)
        _id = "" # just in case, we don't want UnboundLocalError
        ticket._filedate = filedate
        shortfilename = os.path.split(filename)[1]

        # does this ticket have duplicate fields? send a warning
        if ticket._dup_fields:
            self.send_dup_fields_warning(ticket)

        # do we have an adapter for this call center? if so, adapt the ticket
        # this is responsible for sorting out LQW1 tickets to LOR1/LWA1/LID1
        try:
            adapter = call_center_adapter.get_adapter(call_center
                      or ticket.ticket_format)
            before = ticket.ticket_format
            adapter.adapt(ticket)
            after = ticket.ticket_format
            if before != after:
                self.log.log_event("Ticket adapted: %s -> %s" % (before, after))
                # if adapted, fetch the exclude list for this call center and
                # scrub the locates
                # The adapter will inject the original call center into the
                # update_call_centers dict
                exclude = sorted(self.get_exclude_list(before))
                self.log.log_event("Exclude list for %s based on %s: %s" % (
                 before, after, exclude))
                ticket.locates = [locate for locate in ticket.locates
                                  if locate.client_code not in exclude]
        except:
            ep = errorhandling2.errorpacket()
            ep.add(rawticket=raw_ticket, ticket=ticket.repr(),
                   info="Problem adapting ticket", filename=filename)
            self.log.log(ep, send=1, dump=1 and self.verbose, write=1)
            return

        # is this an update call center? if so, set the ticket_format to the
        # "master" call center
        master_cc = self.call_centers.update_call_center(ticket.ticket_format)
        if master_cc:
            ticket._update_call_center = ticket.ticket_format
            ticket.ticket_format = master_cc
            self.log.log_event("Updating " + ticket._update_call_center +
             " to " + ticket.ticket_format)

        # does this ticket have clients that have alert=1?  If so, set
        # ticket.alert:
        if self.ticketalerthandler.has_alert(ticket):
            ticket.alert = 'A'
            self.log.log_event("Ticket alert set for #%s" %
              ticket.ticket_number)

        alert_locates = self.ticketalerthandler.get_hp_grid_clients(ticket)
        alert_locates += self.ticketalerthandler.get_hp_address_clients(ticket)
        for loc in alert_locates:
            self.log.log_event("Locate alert set for: %s" % loc.client_code)
            loc.alert = 'A'
            # todo(dan) Review this section in QMAN-3360, in case there's still
            # a problem, and update or remove the comment. SQLXML is no longer
            # in use, so all references to it should be deleted.

            # NOTE: this works for inserts, and for updates with new locates.
            # There's still a problem if an existing locate (with no alert)
            # gets updated by a version that does have an alert.  We still
            # use SQLXML for this, and updateticket_sqlxml currently has no way
            # to handle this.  (in fact, it does not have a notion of "locate
            # that needs to be updated" beyond what is passed in via the
            # 'newlocates' and 'reassign' parameters.

        # todo(dan) Could searching by call date be a problem around jan 1?
        # todo(dan) Main and update center parsers could be working on the same
        # ticket. Probably should prevent this. Possibly with mutexes.

        # do we have duplicates of this ticket in the database?
        dfresult = self.df.getduplicates(ticket,
                   is_no_show=self.is_no_show(ticket),
                   is_renotification=self.isrenotification(ticket))

        closed_no_show = 0
        if dfresult.dups:
            # is this a no show ticket? if so, we won't post it
            if ticket.ticket_format in ("FCV1", "FCV3", "OCC1", "FMB1", "FMW1")\
            and logic.is_no_show(ticket):
                ticket_info = self.df._select_oldest_duplicate(dfresult.dups)
                ticket_id = ticket_info["ticket_id"]
                if self.tdb.isclosed(ticket_id):
                    #self.add_locates_to_responder_queue(ticket_id)
                    closed_no_show = 1
                    # instead of returning, we're going to update

        _id = ""
        self.log.log_event("Processing ticket %s (%s)" % (ticket.ticket_number,
         dfresult.dups and "updated" or "new"))
        self.log.log_event("  ...%s locate(s) on ticket: %s" % (
         len(ticket.locates), sorted([x.client_code for x in ticket.locates])))

        update = bool(dfresult.dups)
        self.prepare_ticket(ticket, update) # set client_id in locates, etc

        # FHL/Centerpoint stuff
        if ticket.ticket_format == 'FHL1' and ticket._filedate \
        and not logic.isemergency(ticket):
            if centerpoint.is_possibly_centerpoint(ticket):
                centerpoint.mark(ticket)

        # set do_not_respond_before date if appropriate
        # 2004.07.13: only for "new" tickets, not for updates!
        if not update:
            do_not_respond_before = logic.do_not_respond_before_date(ticket)
            if do_not_respond_before:
                ticket.do_not_respond_before = do_not_respond_before

        try:
            tv_id = None
            if update:
                # update an existing ticket, but only if the new ticket has a
                # transmit date >= that of the original ticket
                ld = date.Date(dfresult.latest["ticket"].transmit_date).isodate()
                locates_only = (
                 (ticket.transmit_date < ld
                  and ticket.ticket_format != 'FCO1'
                  and ticket._update_call_center != 'FMW2')
                 or closed_no_show)

                old_grids = dfresult.latest['ticket'].grids
                update_grids = ticket_grid.update_diff(old_grids, ticket.grids)

                # check for missing client_ids that should have been set
                newlocates = dfresult.newlocates
                self.set_locates_client_id_2(ticket.ticket_format, newlocates)
                reassign = dfresult.reassign
            else:
                # set non-UQ-clients to -N rather than -P, so the router
                # doesn't need to process these
                if self.__class__._flag_non_clients:
                    self.flag_non_clients(ticket)

                if self.call_center_process.get('geocode_latlong') and \
                 (float(ticket.work_lat) == float(ticket.work_long) == 0.0):
                    logic.geocode_new_ticket(ticket,
                     self.config.geocoder['min_precision'])

            assigned_work_order_locator = None
            copy_attachments_process = None
            self.tdb.dbado.begin_transaction()
            try:
                if update:
                    self.set_locates_client_id_3(ticket.ticket_format, reassign)
                    try:
                        self.tdb.updateticket(dfresult.latest_id, ticket,
                         newlocates=newlocates, reassign=reassign,
                         whodunit="PARSER", locates_only=locates_only,
                         update_grids=update_grids, post_image=True, log=self.log)
                    except SQLGathererError:
                        s = "Nothing to update on ticket. filename: %s" % filename
                        self.sayln('Problem updating ticket, nothing to update:')
                        self.log.log_event(s)
                    _id = dfresult.latest_id  # in case we need it later
                    if not ticket.ticket_id:
                        ticket.ticket_id = dfresult.latest_id
                        # XXX this should not be necessary, really >=(

                    # add ticket to ticket_version
                    tv_id = self.tdb.add_ticket_version(dfresult.latest_id, ticket,
                            shortfilename)
                    # add locates for responder
                    if closed_no_show:
                        self.add_locates_to_responder_queue(ticket_id)

                    # will this ticket be seen by the router? if not, and it
                    # should go to ticket_ack, then handle that here.
                    if logic.is_ticket_ack(ticket) \
                     and not newlocates \
                     and not reassign:
                        try:
                            # get statuses from latest duplicate
                            latest_dict = dfresult.get_latest_dict()
                            statuses = [d['status'] for d in latest_dict['locates'].values()]
                        except ValueError:
                            statuses = []
                        if statuses and ('-P' not in statuses):
                            self.log.log_event("#DEBUG INFO: statuses=%r" % (statuses,))
                            self.log.log_event("DEBUG INFO: dfresult.dups=%r" % (dfresult.dups,))
                            self.tdb.insert_ticket_ack(_id)
                            self.log.log_event("Added ticket_id %s to ticket_ack" % (_id,))
                            # Update the work priority if > 0
                            work_priority = logic.get_work_priority(ticket)
                            if work_priority is not None:
                                self.tdb.update_ticket_work_priority(_id, work_priority)
                                self.log.log_event("Changed work priority of ticket_id %s to %d" % (_id, work_priority))
                        elif not statuses:
                            self.log.log_event("Could not determine existing statuses, nothing inserted in ticket_ack")
                        else:
                            self.log.log_event("Some locates have -P, will be handled by router")

                else:
                    # insert a new ticket

                    # insert ticket as usual
                    _id = self.tdb.insertticket(ticket,
                         dfresult.parent_ticket_id, whodunit="PARSER")
                    tv_id = self.tdb.add_ticket_version(_id, ticket, shortfilename)

                self.tdb.add_ticket_hp_info(ticket)

                # if there is a work order: those locates that have -P must be
                # set to -R and assigned to the work order's assigned_to
                assigned_work_order_locator, copy_attachments_process = \
                  self.handle_work_orders(ticket)

                # check HP grids and addresses and (possibly) send emails
                self.hpgrid.check(ticket, _id, tv_id,
                     self.clients.get(ticket.ticket_format, []),
                     self.ticket_grid_callback)
                self.hpaddress.check(ticket, _id, tv_id,
                     self.clients.get(ticket.ticket_format, []),
                     self.ticket_hpaddress_callback)

                if ticket.ticket_format == 'FHL1' and ticket._filedate \
                and not logic.isemergency(ticket):
                    if centerpoint.is_centerpoint(ticket):
                        centerpoint.unmark(self.tdb, _id)

                # Mantis 2828: if the ticket is an update, and it's from an update
                # call center which has update_due_dates = True, then manually set
                # the due dates after regular updating process. This is done here
                # because it should happen regardless of the transmit dates of
                # the original/updated tickets.
                if update and ticket._update_call_center:
                    ulogic = getlogic(ticket._update_call_center, self.logic,
                             self.tdb, self.holiday_list)
                    if ulogic.update_due_dates and ticket.due_date and \
                      ulogic.update_due_dates_manually:
                        # set due date "manually"
                        self.tdb.set_due_date_manually(ticket.ticket_id,
                          ticket.due_date)

                if not update:
                    ttypes = QMLogic2ServiceLib.ttypes

                    event_locates = []
                    cc_clients = self.clients.get(ticket.ticket_format, [])
                    for loc in ticket.locates:
                        # get client_name and client_id for UQ clients
                        client_name = None
                        client_id = None
                        if loc.client_id:
                            # client_id set earlier, so it's a UQ client
                            client_id = int(loc.client_id)
                            for cc_client in cc_clients:
                                if cc_client.client_code == loc.client_code:
                                    client_name = cc_client.client_name
                                    break

                        event_locates.append(ttypes.LocateSummary(
                         ID=int(loc.locate_id), ClientID=client_id,
                         ClientCode=loc.client_code, ClientName=client_name))

                    self.event_log.log_ticket_event(int(ticket.ticket_id),
                     ttypes.TicketReceived(TicketNumber=ticket.ticket_number,
                     TicketFormat=ticket.ticket_format, Source=ticket.source,
                     Kind=ticket.kind, TicketType=ticket.ticket_type,
                     TransmitDate=ticket.transmit_date, DueDate=ticket.due_date,
                     LocateList=event_locates), default_version=1,
                     max_version=None)

                if assigned_work_order_locator:
                    locator_status = self.tdb.employee_status(
                      assigned_work_order_locator, hold_lock=True)
                    if locator_status <> ('1', '1'):
                        raise NonrepeatedReadError(
                          "Work order locator's status changed to disallow "
                          "assignments, immediately after being assigned to "
                          "ticket", locator_id=assigned_work_order_locator,
                          locator_active=locator_status[0],
                          locator_can_receive_tickets=locator_status[1])

                self.tdb.dbado.commit_transaction()

            except:
                # todo(dan) Improve handling of rollback failure in QMAN-3028.
                # The process should be terminated quickly, and there should be
                # no possibility of accidentally committing.  
                self.tdb.dbado.rollback_transaction()
                raise

            if copy_attachments_process:
                self.log.log_event(
                 "Running external program to copy attachments: %r" %
                 copy_attachments_process)
                # todo(dan) What should happen if this fails? Should this use a
                # batch file retry mechanism, like the other attachment utility? 
                # What would happen if the work order handling gets rerun?
                error_level = subprocess.call(copy_attachments_process, shell=False)

            # Mantis #2827: MSMQ queueing
            self.queue_ticket_msmq(ticket)

            if (attachment_dir and attachment_user):
                self.handle_attachments(ticket, filename, attachment_dir,
                  attachment_proc_dir, attachment_user)

        except (dbinterface.DBException, dbinterface.RunSQLException,
         dbinterface.DBFatalException, UnroutedWorkOrderError), e:
            self.sayln('Problem processing ticket:')
            ep = errorhandling2.errorpacket()
            ep.add(rawticket=raw_ticket, ticket=ticket.repr(),
                   info="Problem posting ticket", filename=filename)
            send = 1
            # Check that the value has a find method (string)
            # It should
            if hasattr(ep.exception.args[0],"find"):
                # if the db exception is a deadlock, don't send
                if ep.exception.args[0].find('was deadlocked on lock resources') != -1:
                    send = 0
            self.log.log(ep, send=send, dump=1 and self.verbose, write=1)
            if not self.reparse and errordir is not None:
                self.dropticket(raw_ticket, errordir, filename)
            else:
                # Reraise the exception, so the ticket file stays in incoming
                raise
        else:
            self.n = self.n + 1
            self.ids.append(_id)

            # send_no_uq_clients_warnings
            self.noclienthandler.check(ticket, _id, logic)

        try:
            return _id
        except NameError:
            return None

    def queue_ticket_msmq(self, ticket):
        if not self.config.msmq:
            return # no MSMQ configured

        try:
            m = self.config.msmq
            gen = ticket_xml.TicketXMLGenerator()
            xml = gen.generate(ticket)
            data = et_tools.tree_as_string(xml) # includes header
            #print data
            msmq.send_message(m['server'], m['queue'], data)
            self.log.log_event("Sending ticket to MSMQ queue") # XXX more info
        except:
            #import traceback; traceback.print_exc() # REMOVEME
            ep = errorhandling2.errorpacket()
            ep.add(msmq_data=m,
                   info="Could not send to MSMQ queue")
            self.log.log(ep, send=0, dump=1 and self.verbose, write=1)

    def handle_work_orders(self, ticket):
        """ Check if this call center has work orders, and if so, if the
            ticket has a work order associated with it. If such a work order
            is found, set statuses of locates to -R (rather than -P) and
            assign them to the work order's locator.
            NOTE: Must be called when ticket has already been inserted (so we
            have a ticket_id).
        """
        assigned_work_order_locator = None
        copy_attachments_process = None

        # does this call center have work orders? (Mantis #2743)
        wo_centers = self.call_centers.work_order_centers(ticket.ticket_format)
        if not wo_centers:
            return (assigned_work_order_locator, copy_attachments_process)
        #print "%s gets work order from %s" % (ticket.ticket_format, wo_centers)
        wo_number = ticket.get_wo_number()
        if not wo_number:
            # no WO number found, should be OK?
            print 'no WO number found'
            return (assigned_work_order_locator, copy_attachments_process)

        # todo(dan) Do we still need this hack?
        # HACK to make sure we have all the locates etc
        # better solution: make sure we pass in a valid, up-to-date Ticket
        # object with all of its locates having IDs, etc.
        ticket = ticketmod.Ticket.load(self.tdb, ticket.ticket_id)

        # find the work order with this WO number
        try:
            wo = work_order.WorkOrder.find(self.tdb, wo_centers[0],
                 wo_number)
        # todo(dan) Should this only handle ValueError's?
        except:
            ep = errorhandling2.errorpacket()
            ep.add(info="Error retrieving work order: %s" % wo_number)
            self.log.log(ep, send=1, write=1, dump=self.verbose)
        else:
            self.log.log_event("Work order found for ticket %s: %s" % (
              ticket.ticket_number, wo.wo_number))
            self.tdb.assoc_ticket_work_order(ticket.ticket_id, wo.wo_id)

            # assign ticket locates to this work order's locator
            for loc in ticket.locates:
                # if this is a client, assign it to the work order's
                # locator
                if not self.is_UQ_client(ticket.ticket_format, loc.client_code):
                    continue
                if not loc.locate_id:
                    continue

                if not wo.assigned_to_id:
                    raise UnroutedWorkOrderError(
                      'Cannot assign ticket, until associated work order is '
                      'routed', wo_number=wo.wo_number)

                if not assigned_work_order_locator:
                    locator_status = self.tdb.employee_status(
                      wo.assigned_to_id, hold_lock=False)
                    if locator_status <> ('1', '1'):
                        self.log.log_event(
                          'Cannot assign locates to work order locator %s, with'
                          ' status: active: %s, can_receive_tickets: %s' %
                          ((wo.assigned_to_id,) + locator_status))
                        break

                self.log.log_event(
                 "Assigning locate %s to locate id %s (via work order)"
                  % (loc.client_code, wo.assigned_to_id))
                # todo(dan) Log events here in QMAN-3508 and QMAN-3509
                self.tdb.assign_locate(loc.locate_id, wo.assigned_to_id)
                assigned_work_order_locator = wo.assigned_to_id
                if loc.status in ('-P', '-N'):
                    loc.status = '-R'
                    loc.update(self.tdb)

            # run external program to copy attachments
            utility_ini_path = datadir.datadir.get_filename("utility.ini")
            prog = self.config.utilities.get('copy_attachment')
            if prog:
                copy_attachments_process = '"%s" /ini "%s" %s %s' % (prog,
                  utility_ini_path, wo.wo_id, ticket.ticket_id)

        return (assigned_work_order_locator, copy_attachments_process)

    def create_attachment_files(self, att_filename, ticket, attachment_user,
                                att_batch_filename, attachment_proc_dir):
        """ Create an attachment file from the encoded attachment file, and
            create a batch file for uploading it. """

        # open attachment (giving attachment's filename, data)
        real_filename, att_data = attachment.read_attachment(att_filename)
        ext = os.path.splitext(real_filename)[1]
        if isinstance(ticket, work_order.WorkOrder):
            foreign_type = 7
            ticket_id = ticket.wo_id
        else:
            foreign_type = 1
            ticket_id = ticket.ticket_id

        try:
            # generate upload filename
            tempdir = tempfile.mkdtemp()
            upload_filename = os.path.join(tempfile.gettempdir(), tempdir,
                                           real_filename)

            # generate the temp file
            with open(upload_filename, 'wb') as g:
                g.write(att_data)
            self.log.log_event("Creating temporarily file: " + upload_filename)

            # quotes, and shell=False (below) are necessary to support path
            # names with spaces in them.
            command_line = '"%s" /ini "%s" %s %s "%s" "%s" "%s"' % (
             self.config.utilities.get('attachment_import'),
             datadir.datadir.get_filename("utility.ini"), foreign_type,
             ticket_id, upload_filename, attachment_user,
             "Uploaded by ticket parser")

            self.log.log_event("Creating attachment batch file: %s" % att_batch_filename)

            with open(att_batch_filename, 'w') as f:
                f.write(command_line + '\n')
                f.write('set QMAttachmentImport_ERRORLEVEL=%ERRORLEVEL%\n')
                f.write('rd "' + os.path.dirname(upload_filename) + '"\n')
                f.write('exit %QMAttachmentImport_ERRORLEVEL%\n')

            # move "source" file to attachment_proc_dir
            self.log.log_event("Moving %s to %s..." % (
             att_filename, attachment_proc_dir))
            filetools.move_file(att_filename, attachment_proc_dir)
        except:
            filetools.remove_file(att_batch_filename)
            filetools.remove_dir(tempdir) # includes temp file
            raise

    def handle_attachment(self, att_filename, ticket, attachment_user,
                          attachment_proc_dir):
        att_file_path, att_file_name = os.path.split(att_filename)
        att_batch_filename = os.path.join(att_file_path,
         attachment.ATTACHMENT_BATCH_FILENAME_PREFIX +
         os.path.splitext(att_file_name)[0] + '.bat')
        with mutex.MutexLock(self.dbado, attachment.upload_mutex_name(
         att_batch_filename)):
            self.create_attachment_files(att_filename, ticket, attachment_user,
              att_batch_filename, attachment_proc_dir)
            try:
                attachment.upload_attachment(att_batch_filename, self.log)
            except:
                ep = errorhandling2.errorpacket()
                ep.add(info="Error uploading attachment")
                ep.add(batchfile=att_batch_filename)
                self.log.log(ep, send=1, dump=1 and self.verbose, write=1)

    def handle_attachments(self, ticket, fullname, attachment_dir,
                           attachment_proc_dir, attachment_user):
        """ Handle the attachment (if any) that came in with the ticket or
            work order. Returns the number of attachments processed. """

        # look for files in attachment dir that start with the same number
        path, filename = os.path.split(fullname)
        shortname = os.path.splitext(filename)[0]
        att_filenames = attachment.find_attachments(attachment_dir, shortname)
        self.log.log_event("Attachment(s) found for this ticket: %s" %
         att_filenames)

        num_attachments = 0
        for att_filename in att_filenames:
            self.handle_attachment(att_filename, ticket, attachment_user,
                                   attachment_proc_dir)
            num_attachments += 1

        if num_attachments:
            self.log.log_event("%d attachments processed" % num_attachments)
        return num_attachments

    def is_UQ_client(self, call_center, client_code):
        clients = self.clients.get(call_center, [])
        for client in clients:
            if client.client_code == client_code:
                return True
        return False

    ###
    ### preparing a ticket for posting

    def flag_non_clients(self, ticket):
        _set = []
        for loc in ticket.locates:
            if loc.status == '-P':
                if not self.is_UQ_client(ticket.ticket_format, loc.client_code):
                    loc.status = '-N'
                    loc.closed = 1
                    loc.closed_how = 'parser'
                    _set.append(loc.client_code)
        if _set:
            self.log.log_event(" ...Set to -N: %s" % (_set,))

    def prepare_ticket(self, ticket, update):
        """ Prepare a ticket object for posting, by setting the due date
            (later), client_id of locates, etc. """
        self.set_locates_client_id(ticket)

        # get business logic for the *original* call center
        orig_cc = ticket._update_call_center or ticket.ticket_format
        logic = getlogic(orig_cc, self.logic, self.tdb, self.holiday_list)
        if not update:
            # by default, we don't set due_date and legal_due_date for updates
            self.set_due_date(ticket, is_update=False)
        elif logic.update_due_dates:
            self.set_due_date(ticket, use_ucc_rules=True, is_update=True)
        # later: set routing data as well

    def set_due_date(self, ticket, use_ucc_rules=False, is_update=False):
        # sometimes the due date is already extracted by the parser; use it
        if ticket.due_date and ticket.legal_due_date:
            return

        # NOTE: in the case of update call centers, this gets the business
        # logic for the *master* call center, because ticket.ticket_format has
        # already been updated at this point.
        # if use_ucc_rules is True, then we use the due date calculations for
        # the update call center.
        orig_cc = ticket.ticket_format
        if use_ucc_rules:
            orig_cc = ticket._update_call_center or ticket.ticket_format
        logic = getlogic(orig_cc, self.logic, self.tdb, self.holiday_list)

        try:
            if logic.update_due_dates:
                legal_due_date = logic.legal_due_date(ticket, is_update)
                due_date = legal_due_date #logic.due_date(ticket, is_update)
            else:
                legal_due_date = logic.legal_due_date(ticket)
                due_date = logic.due_date(ticket)
            if not is_update:
                assert legal_due_date, "Legal due date should be set"
                assert due_date, "Due date should be set"
        except:
            ep = errorhandling2.errorpacket()
            ep.add(info="Problem determining due date", ticket=ticket.repr())
            self.log.log(ep, send=1, write=1, dump=1)
        else:
            # we don't always set the due date for updates (see #2828)
            if due_date:
                ticket.due_date = due_date
                ticket.legal_due_date = legal_due_date

    def set_locates_client_id(self, ticket):
        """ Set the client_id field of the locates of the given Ticket. Only
            the client_id of UQ clients are set. """
        call_center = ticket.ticket_format
        clients = self.clients.get(call_center, [])
        for locate in ticket.locates:
            if not locate.client_id:
                for client in clients:
                    if client.client_code == locate.client_code:
                        locate.client_id = client.client_id
                        break

    def set_locates_client_id_2(self, call_center, locates):
        """ Like set_locates_client_id, but takes a list of locates. """
        clients = self.clients.get(call_center, [])
        for locate in locates:
            if not locate.client_id:
                for client in clients:
                    if client.client_code == locate.client_code:
                        locate.client_id = client.client_id
                        break

    def set_locates_client_id_3(self, call_center, reassigndict):
        clients = self.clients.get(call_center, [])
        for locate_id, data in reassigndict.items():
            if not data.get('client_id'):
                for client in clients:
                    if client.client_code == data.get('client_code'):
                        self.tdb.set_client_id(locate_id, client.client_id)
                        line = 'client code %s should have client_id %s; corrected' % (
                               client.client_code, client.client_id)
                        self.log.log_event(line)
                        break

    def _route(self, locate, ticket):
        # XXX a good basis, but not quite there yet
        # and it will probably be moved elsewhere!!
        """ Route a single locate. """
        # if there is already a locator present on another locate,
        # then use that
        locator = self.find_existing_locator(ticket)
        mapinfo = None

        # otherwise, use business logic to find a locator
        if not locator:
            # HACK to make this work with current code; will need to be done
            # better once this is the default way of routing
            ticket.client_code = locate.client_code

            logic = getlogic(ticket.ticket_format, self.logic, self.tdb, self.holiday_list)
            routingresult = logic.locator(ticket)
            locator = routingresult.locator
            mapinfo = routingresult.mapinfo
            # XXX remove useless ticket_id parameter from locator()

        # still not found? use the default
        if not locator:
            locator = logic.DEFAULT_LOCATOR

        # set locator, status, and possibly mapinfo
        locate.locator = locator
        locate.status = locate_status.assigned
        if mapinfo:
            ticket.map_page = mapinfo

    def route(self, ticket):
        self.router.prepare(ticket)
        self.router.route(ticket, whodunit="parser")

    @staticmethod
    def find_existing_locator(ticket):
        """ Try to find an existing locator on a ticket. """
        for locate in ticket.locates:
            if locate._assignment:
                return locate._assignment.locator_id
        return None

    def is_a_client(self, client_code, call_center):
        # XXX for now, we use TicketDB's method... but later we may want to
        # optimize by loading all the client names in memory once.
        return self.tdb.is_a_client(client_code, call_center)

    ###
    ### "no UQ clients" warning

    def send_summary(self, summ):
        """ Attempt to send a summary to certain emails. """
        summ_emails = self.emails[summ.call_center].summary
        #if not self.summary_emails.get(summ.call_center, []) \
        if (not summ_emails) or self.log.lock:
            return
        subject = "%s audit" % (summ.call_center,)
        body = summ.image
        toaddrs = summ_emails

        try:
            self.say("Sending email...")
            mail2.sendmail(self.config.smtp_accs[0], toaddrs, subject, body)
        except:
            ep = errorhandling2.errorpacket()
            ep.add(info="Could not send email", body=summ.image,
                   toaddrs=toaddrs)
            self.log.log(ep, send=0, dump=1, write=1)

        self.log.log_event(
         "%s summary sent to %s" % (summ.call_center, toaddrs),
         dump=1 and self.verbose)

    def send_no_uq_clients_warnings(self):
        self.noclienthandler.send_warnings()
        self.noclienthandler.flush()

    def set_source(self, ticket, logic):
        """ Set the source for the ticket, if appropriate.  (Default is the
            call center / feed.) """
        # NOTE: Currently only implemented as a simple rule; can be refactored
        # out to e.g. businesslogic, if more rules come up.
        # NCA1 and SCA1 have their own rules; default is call center.
        if ticket.ticket_format in ('SCA1', 'NCA1'):
            indicator = ticket.image.lstrip().split(' ', 1)[0]
            if indicator != 'UQSTSO':
                ticket.source = indicator[:12]
        else:
            ticket.source = self.call_center

    def get_exclude_list(self, call_center):
        """ Return a list of locates to be excluded for the given call center.
        """
        master = self.call_centers.update_call_center(call_center)
        if master:
            # update call centers: get clients from the "master" call center,
            # and exclude anything that isn't for this call center
            clients = self.clients.get(master, [])
            exclude = [c.client_code for c in clients
                       if c.update_call_center != call_center]
        else:
            # other call centers: exclude anything with the update_call_center
            # field filled
            clients = self.clients.get(call_center, [])
            exclude = [c.client_code for c in clients
                       if c.update_call_center
                       and c.update_call_center != call_center]
        return exclude
        # I suppose we could cache this somewhere...?

    def send_dup_fields_warning(self, ticket):
        c = StringIO()
        print >> c, "The following ticket has duplicate values:"
        print >> c
        for name, values in ticket._dup_fields:
            print >> c, "%s: %s" % (name, values)
        print >> c
        print >> c, "Ticket image:"
        print >> c, "-------------"
        print >> c
        print >> c, ticket.image
        self.log.log_warning(c.getvalue(), call_center=ticket.ticket_format,
         subject_hint="duplicate fields", send=1, write=1, dump=1)

    def send_email(self, smtpinfo, toaddrs, subject, body, email_type):
        try:
            mail2.sendmail(smtpinfo, toaddrs, subject, body, send_failure_warning=0)
            return True
        except (socket.error, smtplib.SMTPException) as e:
            ep = errorhandling2.errorpacket()
            self.log_email_error(ep, email_type, smtpinfo, toaddrs, subject, body, dump=1)

            if isinstance(e, (socket.timeout, socket.gaierror)):
                # todo(dan) Should we be sending a timeout warning for a gaierror?
                try:
                    mail2.send_timeout_warning()
                except (socket.error, smtplib.SMTPException) as e:
                    ep = errorhandling2.errorpacket()
                    ep.add(info="Could not send timeout warning email")
                    self.log.log(ep, send=0, dump=1, write=1)
        return False

    def log_email_error(self, ep, email_type, smtpinfo, toaddrs, subject, body, dump=0):
        ep.add(info="Could not send email", email_type=email_type, smtpinfo=smtpinfo,
               toaddrs=toaddrs, subject=subject)
        self.log.log(ep, send=0, dump=dump, write=1)
        ep.add(body=body)
        self.email_error_handler.log(ep, send=0, dump=dump, write=1)

    def ticket_grid_callback(self, ticket, ticket_id, client, relevant_grids, grid_email):
        # send email
        emails = tools.split_emails(grid_email)
        if emails:
            # did we already send an email for this ticket today?
            d = ticket_grid.get_date_sent(self.tdb, ticket.ticket_format,
                ticket.ticket_number, client.client_code)
            today = date.Date().isodate()
            if d[:10] == today[:10]:
                self.log.log_event("Grid warning email for %s/%s already sent today, skipped" % (
                 ticket.ticket_number, client.client_code))
                return 0

            self.log.log_event("Sending grid warning email for %s/%s to %s" % (
             ticket.ticket_format, client.client_code, emails))
            subject = "Ticketparser grid warning (%s/%s)" % (
                      ticket.ticket_format, client.client_code)

            # create message body
            c = StringIO()
            print >> c, "Ticket", ticket.ticket_number, "was received at", \
                        date.Date().isodate(), "for call center", \
                        ticket.ticket_format
            print >> c, "containing client", client.client_code + "."
            print >> c
            print >> c, "This ticket/client combination has the following", \
                        "high profile grids:"
            for grid in relevant_grids:
                print >> c, "-", grid
            print >> c
            print >> c, "Ticket image:"
            print >> c, "-------------"
            print >> c, ticket.image
            body = c.getvalue()

            if not self.log.lock and self.send_email(self.config.smtp_accs[0],
             emails, subject, body, 'high profile grid warning email'):
                # return 1 if mail successfully sent, otherwise 0
                return 1
            return 0
        else:
            self.log.log_event("No grid_email found for %s/%s" % (
             ticket.ticket_format, client.client_code))
            return 0

        # we don't log here; the ticket_grid.check() function does that

    def ticket_hpaddress_callback(self, ticket, ticket_id, client,
                                  relevant_address, grid_email):
        # send email
        emails = tools.split_emails(grid_email)
        if emails:
            # did we already send an email for this ticket today?
            d = ticket_grid.get_date_sent(self.tdb, ticket.ticket_format,
                ticket.ticket_number, client.client_code)
            today = date.Date().isodate()
            if d[:10] == today[:10]:
                self.log.log_event("HP Address warning email for %s/%s already sent today, skipped" % (
                 ticket.ticket_number, client.client_code))
                return 0

            self.log.log_event("Sending hp address warning email for %s/%s to %s" % (
             ticket.ticket_format, client.client_code, emails))
            subject = "Ticketparser hp address warning (%s/%s)" % (
                      ticket.ticket_format, client.client_code)

            # create message body
            c = StringIO()
            print >> c, "Ticket", ticket.ticket_number, "was received at", \
                        date.Date().isodate(), "for call center", \
                        ticket.ticket_format
            print >> c, "containing client", client.client_code + "."
            print >> c
            print >> c, "This ticket/client combination has the following", \
                        "high profile address:"
            print >> c, "Street - ", relevant_address["street"]
            print >> c, "City   - ", relevant_address["city"]
            print >> c, "County - ", relevant_address["county"]
            print >> c, "State  - ", relevant_address["state"]
            print >> c
            print >> c, "Ticket image:"
            print >> c, "-------------"
            print >> c, ticket.image
            body = c.getvalue()

            if not self.log.lock and self.send_email(self.config.smtp_accs[0],
             emails, subject, body, 'high profile address warning email'):
                # return 1 if mail successfully sent, otherwise 0
                return 1
            return 0
        else:
            self.log.log_event("No grid_email found for %s/%s" % (
             ticket.ticket_format, client.client_code))
            return 0

        # we don't log here; the ticket_grid.check() function does that


if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "1r:R:c:pf:?mqg:x")

    if not args:
        print >> sys.stderr, __usage__
        sys.exit(0)

    call_center = args[0]

    reparse = dont_move = 0
    runonce = 0
    profiler = 0
    configfile = ""
    format = ""
    reparse_days = 0
    quiet = 0   # if this is set, send no mail
    routing = 0

    for o, a in opts:
        if o == "-1":
            runonce = 1
        elif o == "-?":
            print >> sys.stderr, __usage__
            sys.exit(0)
        elif o in ("-r", "-R"):
            reparse = 1
            runonce = 1
            reparse_days = int(a) # XXX obsolete?
        elif o == "-c":
            configfile = a
        elif o == "-p":
            profiler = 1
            runonce = 1 # otherwise it doesn't make sense
        elif o == "-m":
            dont_move = 1
        elif o == "-q":
            quiet = 1
        elif o == "-x":
            routing = 1

    # only one instance of this *program* is allowed
    biz_id = datadir.datadir.get_id()
    mutex_name = "main.py:%s:%s" % (biz_id, call_center)
    # for example, "main.py:UQ:FDE1"
    windows_tools.setconsoletitle("main.py " + str(call_center))

    try:
        log = errorhandling_special.toplevel_logger(configfile, "Ticketparser",
         "Ticketparser Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(configfile, "Ticketparser")
        sys.exit(1)

    check_python_version(log)
    try:
        dbado = dbinterface_ado.DBInterfaceADO()
        with mutex.MutexLock(dbado, mutex_name):
            main = Main(configfile=configfile, call_center=call_center, dbado=dbado)
            program_status.write_status(mutex_name)
            if quiet:
                main.log.lock = 1
            main.reparse = reparse  # irrelevant?
            main.dont_move = dont_move
            main.run(runonce=runonce)
            if profiler:
                show_profile_report("main_profile_g_", call_center, main.tdb)

    except mutex.SingleInstanceError:
        print "Only one instance of this program can be run at a time!"
        print "Locked mutex:", repr(mutex_name)

    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)

