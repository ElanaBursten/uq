# lockmutex.py
# Simple script to test mutex locking.

from __future__ import with_statement
import dbinterface_ado
import sys
import mutex

name = sys.argv[1]
with mutex.MutexLock(dbinterface_ado.DBInterfaceADO(), name):
    raw_input("Press return to stop program and release mutex %r..." % name)

