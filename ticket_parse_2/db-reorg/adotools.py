# adotools.py
# Tools for working with ADO and Win32. (May or not work with .NET.)

import tools

def is_result_set(recordset):
    """ Sometimes stored procs return record sets for things that aren't
        actually result sets.  This function determines whether an
        ADO.RecordSet is valid or not. """
    try:
        recordset.Eof
    except:
        return 0
    else:
        return 1

def __ado_convert(self, resultset):
    """ Convert the values in a result set to strings. """
    for row in resultset:
        for key, value in row.items():
            row[key] = tools.ado_to_string(value)

# FIXME: this can be done better, in both interface and implementation
def __make_dict(self, rows, fieldnames):
    """ Make a list of dictionaries from a list of ADO-returned rows. """
    transposed_rows = zip(*rows)
    qrows = []
    for row in transposed_rows:
        d = {}
        for j in range(len(fieldnames)):
            d[fieldnames[j]] = row[j]
        qrows.append(d)
    return qrows

def list_of_record_dicts(rows, fieldnames):
    return [make_record_dict(row, fieldnames) for row in rows]

def make_record_dict(row, fieldnames):
    assert len(row) == len(fieldnames)
    row = [tools.ado_to_string(x) for x in row]
    return dict(zip(fieldnames, row))

def get_fieldnames(rs):
    """ Get a list of fieldnames, in order, from an ADO recordset. """
    return [field.Name for field in rs.Fields]


