# dbinterface.py
# Fairly low-level but platform-agnostic database interface.
# Should contain:
# - exception handling (w/ timeout, SQL attr, etc)
# - generic ways to get/insert/update/delete records
# - running SQL queries, stored procs (returning results or not)
# - things like counting the number of records, etc
# - optional profiling so we can measure performance
#
# All of the above should be done by talking to a DBEngine instance, not by
# accessing the database directly!

import string
#
import sqltools
import tools

class DBInterface(object):

    def __init__(self, dbengine):
        self._dbengine = dbengine

    # ideally, should be mostly compatible with existing dbinterface.py and
    # dbinterface_ado.py

    def runsql(self, sql, timeout=None):
        return self._dbengine.runsql(sql, timeout)

    def runsql_result(self, sql, timeout=None):
        return self._dbengine.runsql_result(sql, timeout)

    def runsql_result_multiple(self, sql, timeout=None):
        return self._dbengine.runsql_result_multiple(sql, timeout)

    def runsql_with_parameters(self, sql, params, timeout=None):
        return self._dbengine.runsql_with_parameters(sql, params, timeout)

    def call(self, name, *args):
        sql = "EXEC %s " % name
        sql += string.join(map(str, args), ", ")
        return self._dbengine.runsql_with_parameters(sql, args)
    call_result = call

    def get_max_id(self, tablename, id_fieldname):
        sql = "SELECT ISNULL(MAX(%s,0)) AS M FROM %s" % (id_fieldname,
              tablename)
        rows = self.runsql_result(sql)
        return rows[0]['M']

    def get_ids_after(self, tablename, id_fieldname, id):
        sql = "SELECT %s FROM %s WHERE %s > %s" % (id_fieldname, tablename,
              id_fieldname, tools.sqlify(id))
        rows = self.runsql_result(sql)
        return [row[id_fieldname] for row in rows]

    def count(self, tablename, **criteria):
        sql = "SELECT COUNT(*) AS C FROM %s" % tablename
        sql, params = sqltools.sql_with_conditions(sql, **criteria)
        rows = self._dbengine.runsql_with_parameters(sql, params)
        return rows[0]['C']

    def getrecords(self, tablename, **criteria):
        sql = "SELECT * FROM %s" % tablename
        sql, params = sqltools.sql_with_conditions(sql, **criteria)
        return self._dbengine.runsql_with_parameters(sql, params)

    def getrecords_ordered(self, tablename, orderfield, **criteria):
        sql = "SELECT * FROM %s" % tablename
        sql, params = sqltools.sql_with_conditions(sql, **criteria)
        sql += " ORDER BY %s" % orderfield
        return self._dbengine.runsql_with_parameters(sql, params)

    def deleterecords(self, tablename, **criteria):
        sql = "DELETE FROM %s" % tablename
        sql, params = sqltools.sql_with_conditions(sql, **criteria)
        return self._dbengine.runsql_with_parameters(sql, params)
    delete = deleterecords

    # FIXME: use params (this is complicated by the fact that we need to
    # specify the parameter type, apparently)
    def insertrecord(self, tablename, **criteria):
        sql = "INSERT %s " % tablename
        fieldnames, values = sqltools.fields_and_values(criteria)
        values = [tools.sqlify(x) for x in values]
        sql += ("(" + string.join(fieldnames, ", ") + ") ")
        sql += "VALUES " + ("(" + string.join(values, ", ") + ")")
        return self._dbengine.runsql(sql)
    insert = insertrecord

    def updaterecord(self, tablename, id_fieldname, id, timeout=None, **kwargs):
        sql = "UPDATE %s " % tablename
        fields = ["%s=%s" % (fieldname, tools.sqlify(value))
                  for fieldname, value in kwargs.items()]
        sql += "SET " + string.join(fields, ", ")
        sql += " WHERE %s = %s" % (id_fieldname, tools.sqlify(id))
        return self._dbengine.runsql(sql)
    update = updaterecord


def detect_interface():
    # for now, by default, assume ADO/Win32
    import dbengine_ado_win32
    dbengine = dbengine_ado_win32.DBEngineADOWin32() # from config file
    return DBInterface(dbengine)


