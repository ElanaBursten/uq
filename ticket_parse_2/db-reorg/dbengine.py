# dbengine.py
# Abstract database engine, to be subclassed to implement different ways of
# accessing databases: SQLXML, ADO/win32, IronPython, etc.
#
# A DBEngine needs to provide a minimum of methods to be used by DBInterface.

DEFAULT_TIMEOUT = 580

class DBException(Exception):
    def __init__(self, msg, **kwargs):
        Exception.__init__(self, msg)
        self.msg = msg
        self.data = kwargs
        self.args = (msg, self.data)

class DBFatalException(DBException):
    pass

class DBTimeout(DBException):
    pass
TimeoutError = DBTimeout

class DBEngine(object):

    def __init__(self):
        self._conn = None # low-level connection object
        self.timeout = DEFAULT_TIMEOUT

    # add abstract interface here...

    def runsql(self, sql, timeout=None):
        raise NotImplementedError

    def runsql_result(self, sql, timeout=None):
        raise NotImplementedError

    def runsql_result_multiple(self, sql, timeout=None):
        raise NotImplementedError

    def runsql_with_parameters(self, sql, params, timeout=None):
        raise NotImplementedError

