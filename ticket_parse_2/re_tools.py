# re_tools.py

import re
import string
#import sys

import quadrilateral
import tools

def compiled_regex(*args):
    #print >>sys.stderr, 're=', ''.join(args) # todo(dan) remove this
    return re.compile(''.join(args))
cr = compiled_regex

def group_re(re):
    return '(' + re + ')'
g = group_re

BL = r'(?:^|\n)'  # Beginning of line, including beginning of string
EL = r'(?:\n|$)'  # End of line, including end of string
N = r'\n'         # New line. Might not include beginning or end of string.
B = r'[ \t]*'     # 0-n spaces or tabs
B1 = r'[ \t]+'    # 1-n spaces or tabs
C = B + ':' + B   # ':' surrounded by 0-n spaces or tabs
A = r'(?:.|\n)*?' # 0-n of anything. Non-greedy. DOTALL not necessary.

# todo(dan) need separate constants for groups?
AN = '[A-Za-z0-9]+' # alphanumeric string, required
AN_GRP = g(AN) # alphanumeric string, required, grouped

DIGITS = r'\d+' # 1-n digits # todo(dan) be more consistent with \d vs [0-9] ?

DATE = '[0-9/]+' # todo(dan) be more specific?
TIME = '[0-9:]+' # todo(dan) be more specific?

DT12 = DATE + ' ' + TIME + ' ' + '(?:AM|PM)' # todo(dan) review
DT12_GRP = g(DT12)

DT24 = DATE + ' ' + TIME
DT24_GRP = g(DT24)

STATE = '..' #  todo(dan) be more specific? always 2 chars?
STATE_GRP = g(STATE)

CLIENT_CODE = r'\S+'
CLIENT_CODE_GRP = g(CLIENT_CODE)

# todo(dan) create a constant for '(.*?)' and '.*?' everywhere?

# todo(dan) rename to indicate it's a header at the beginning of line
def first_header_re(header):
    return ''.join((BL, B, header, C))
h1 = first_header_re

# todo(dan) rename to indicate it's a header after a B1 separator
def next_header_re(header):
    return ''.join((B1, header, C))
h2 = next_header_re

def only_field_re(value_header, value_re='(.*?)'):
    return cr(h1(value_header), value_re, EL) # todo(dan) need EL?

def only_or_first_field_re(value_header, next_header, value_re='(.*?)'):
    return cr(h1(value_header), value_re, '(?:', h2(next_header), '|', EL, ')')

def first_field_re(value_header, next_header, value_re='(.*?)'):
    # todo(dan) Change this to doc string tests?
    # Finds 'B606000313'
    # Ticket : B606000313 Rev: 00B Taken: 02/29/16 12:22 PM Channel: WEB
    return cr(h1(value_header), value_re, h2(next_header))

def first_field_then_optional_field_re(
  value_header, optional_header, next_header, value_re='(.*?)'):
    return cr(h1(value_header), value_re,
              '(?:', h2(optional_header), '|', h2(next_header), ')')

def next_field_re(first_header, value_header, next_header, value_re='(.*?)'):
    return cr(h1(first_header), '.*?', h2(value_header), value_re, h2(next_header))

# todo(dan) Test this. It hasn't been used yet.
def next_field_then_optional_field_re(
  first_header, value_header, optional_header, next_header, value_re='(.*?)'):
    return cr(h1(first_header), '.*?', h2(value_header), value_re,
              '(?:', h2(optional_header), '|', h2(next_header), ')')

def next_field_then_text_re(first_header, value_header, text, value_re='(.*?)'):
    return cr(h1(first_header), '.*?', h2(value_header), value_re, B1, text)

def next_or_last_field_re(first_header, value_header, next_header, value_re='(.*?)'):
    return cr(h1(first_header), '.*?', h2(value_header), value_re,
              '(?:', h2(next_header), '|', EL, ')')

def last_field_re(first_header, value_header, value_re='(.*?)'):
    # todo(dan) is EL necessary, since not using DOTALL?
    return cr(h1(first_header), '.*?', h2(value_header), value_re, EL)

# todo(dan) need constants for the param defaults?
# todo(dan) switch to multi-line, remove next_line_header, find another way to check for BOL before ticket?
def transmission_line_re(
  client_code_re=r'\S+', cc_code_re='OUPS[a-z]', transmit_date_re=DT24,
  ticket_type_re='.+?', next_line_header='Ticket'):
    # 'CGE    00106 OUPSb 02/29/16 12:23:08 B606000313-00B ROUT NEW POLY LREQ'
    return cr(
      BL, B, client_code_re, B1, DIGITS, B1, cc_code_re, B1, transmit_date_re,
      B1, AN, '-', AN, B1, ticket_type_re, N, A, next_line_header)

def usan_transmission_line_re(
  client_code_re=CLIENT_CODE, transmit_date_re=DT24, ticket_type_re='.+?'):
    return transmission_line_re(
      client_code_re=client_code_re, cc_code_re='USAN[A-Z]',
      transmit_date_re=transmit_date_re, ticket_type_re=ticket_type_re,
      next_line_header='Message Number')

def multiline_field_inline_header_re(header):
    return cr(h1(header), '.*?', EL, '(?:', C, '.*?', EL, ')*')

def multiline_field_inline_header_values(match):
    #print >>sys.stderr, 'match>>', match
    return [l.partition(':')[2].strip() for l in match.split('\n') if l.strip()]

def lat_long_from_polygon_coordinates(match):
    # Get list of strings like:
    # ['40.141307/-83.126712 40.140854/-83.121380', '40.139631/-83.126854']
    match_values = multiline_field_inline_header_values(match)

    # Get list of list of strings, like:
    # [['40.141307/-83.126712', '40.140854/-83.121380'], ['40.139631/-83.126854']]
    pair_lists = [l.split(' ') for l in match_values] # get list of 

    # Get list of coordinate pairs, like:
    # [(40.141307, -83.126712), (40.140854, -83.121380), (40.139631, -83.126854)]
    coord_pairs = []
    for pair_list in pair_lists:
        for pair in pair_list:
            if pair.strip():
                lat, sep, long = pair.partition('/')
                if sep <> '/': # todo(dan) don't change to assert, but handle differently
                    raise Exception('Invalid coordinate separator')
                coord_pairs.append((float(lat), float(long)))
                
    # todo(dan) Move this to a separate routine, and/or fix quadrilateral.Polygon
    if len(coord_pairs) == 4:
        return quadrilateral.Quadrilateral(coord_pairs).centroid()
    elif len(coord_pairs) == 3:
        # todo(dan) review/fix this
        lat4 = coord_pairs[2][0]
        long4 = coord_pairs[2][1] + 0.0000001 # hack to create 4 distinct points
        return quadrilateral.Quadrilateral(coord_pairs + [(lat4, long4)]).centroid()
    elif len(coord_pairs) == 2:
        return (coord_pairs[0][0] + coord_pairs[1][0]) / 2.0,\
               (coord_pairs[0][1] + coord_pairs[1][1]) / 2.0
    elif len(coord_pairs) == 1:
        return coord_pairs[0][0], coord_pairs[0][1] 
    else:
        return 0.0, 0.0

def multiline_field_inline_header_string_value(match):
    return ' '.join(multiline_field_inline_header_values(match))

def isodate_from_match(self, match):
    return tools.isodate(match.group(1))

def multiline_term_codes_field_re(header):
    return cr(h1(header), N, '((?:.*?=.*?', EL, ')+)')

def term_codes_from_multiline_term_codes_field(match):
    # match should include each line after the header line, that includes an
    # '=', up to the first line that doesn't include an '='.

    lines = [l for l in match.split('\n') if l.strip()]

    codes = []
    for line in lines:
        code1 = line[:39].partition('=')[0].strip()
        code2 = line[39:].partition('=')[0].strip()
        if code1:
            codes.append(code1)
        if code2:
            codes.append(code2)

    return codes
