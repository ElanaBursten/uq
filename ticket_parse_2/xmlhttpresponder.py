# xmlhttpresponder.py

import sys
import abstractresponder
import xmlhttpresponder_data
import win32com.client # FIXME: not valid in IronPython
import xml.etree.ElementTree as ET
from xml.sax.saxutils import escape # valid in CPython and IronPython

# NOTE: Don't use a BadResponseError here, since it bails out and
# doesn't respond to the rest of the locates either.  Makes sense in a telnet
# session, but not here.

class XMLHTTPResponder(abstractresponder.AbstractResponder):
    name = 'XMLHTTPResponder'
    responder_type = 'xmlhttp' # as used by ResponderConfigData

    def __init__(self, *args, **kwargs):
        abstractresponder.AbstractResponder.__init__(self, *args, **kwargs)
        self.xmlhttp = win32com.client.Dispatch("MSXML2.ServerXMLHTTP")

    def load_responder_data(self, call_center, raw_responderdata):
        self.responderdata = xmlhttpresponder_data.getresponderdata(
                             self.config, call_center, raw_responderdata)
        # Give the responder data the logger
        self.responderdata.log = self.log

    def response_is_success(self, row, resp):
        return self.responderdata.response_is_success(row, resp)

    def make_xml(self, ticket, membercode, responsecode, explanation,
                 locator_name, version):
        names = {'ticket': ticket,
                 'membercode': membercode,
                 'responsecode': responsecode,
                 'explanation': explanation,
                 'revision': version or '0',
                 'locator_name': locator_name or ''}
        for key, value in names.iteritems():
            try:
                names[key] = escape(value)
            except:
                s = 'Unable to escape special characters in %s. '\
                    'Value of %s is %s' % (key, key, repr(value))
                self.log.log_event(s)
                raise abstractresponder.ResponderError, s
        xmlblurb = self.responderdata.get_xml(names)
        if self.manual_mode:
            print >> sys.stderr, "[manual mode] XML sent:"
            print >> sys.stderr, xmlblurb
        return xmlblurb

    def send_response(self, ticket_number, membercode, responsecode,
                      explanation=None, locator_name='', version='0'):
        membercode = self.translate_term(membercode, {})
        xmlblurb = self.make_xml(ticket_number, membercode, responsecode,
                   explanation, locator_name, version)

        # validate xml
        try:
            ET.fromstring(xmlblurb)
        except:
            s = "XML response failed to parse.\n%s" % xmlblurb
            self.log.log_event(s)
            raise abstractresponder.ResponderError("Invalid XML")

        reply = self._send_response_xml(xmlblurb)

        reply_text = self.responderdata.process_response(reply)
        return reply_text

    def _send_response_xml(self, xmlblurb):
        self.xmlhttp.open("POST", self.responderdata.post_url, False)

        if hasattr(self.responderdata, 'CONTENT_TYPE_HEADER'):
            self.xmlhttp.setRequestHeader(
              'Content-Type', self.responderdata.CONTENT_TYPE_HEADER)
        else:
            self.xmlhttp.setRequestHeader("Content-Type", "text/xml")

        if self.responderdata.resp_url:
            self.xmlhttp.setRequestHeader("SOAPAction", self.responderdata.resp_url)
        self.xmlhttp.send(xmlblurb)

        reply = self.xmlhttp.responseXML
        return reply

    def get_code_from_response(self, text):
        return text.lower().strip()

if __name__ == "__main__":

    abstractresponder.run_toplevel(XMLHTTPResponder)

