# ftptools.py

import ftplib

class BetterFTP(ftplib.FTP):

    def cwd_create(self, dir):
        """ Try to CHDIR to the given directory.  If it does not exist, create
            it, and try to enter it again. """
        if "/" in dir:
            return self.cwd_create_many(dir)

        try:
            self.cwd(dir)
        except ftplib.error_perm:
            # try to create the directory, then enter it
            self.mkd(dir)
            self.cwd(dir)
        # FIXME: does not handle multiple directories, e.g. foo/bar/baz

    def cwd_create_many(self, path):
        """ Like cwd_create, but works with multiple paths, like foo/bar/baz.
        """
        dirs = filter(None, path.split("/"))
        for dir in dirs:
            # create, if necessary, and enter
            self.cwd_create(dir)

