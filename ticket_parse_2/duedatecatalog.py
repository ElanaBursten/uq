# duedatecatalog.py
# Created: 2002.08.14 (split off from businesslogic.py)

import date_special
import datetime

class DueDateCatalog:
    """ A catalog of possible ways to compute a due date.

        Rule: Methods both take and return Date instances. Methods also change
        the date in place.
    """
    def __init__(self, call_center, holiday_list):
        self.call_center = call_center
        self.holiday_list = holiday_list

    def isbusinessday(self, xdate):
        return not (xdate.isweekend() or date_special.isholiday(xdate,
         self.call_center, self.holiday_list))

    def inc_n_days(self, xdate, n=1):
        count = 0
        while count < n:
            xdate.inc()
            if self.isbusinessday(xdate):
                count = count + 1
        return xdate

    def inc_48_hours(self, xdate):
        """ Vanilla +48 hours (= 2 days) due date, discounting weekends and
            holidays as always. """
        return self.inc_n_days(xdate, 2)

    def inc_48_hours_5pm(self, xdate):
        """ Add 48 hours, then take the first 5PM.
            NOTE: May be generalized to use any time. """
        xdate = self.inc_48_hours(xdate)
        if xdate.hours < 17:
            # 5 PM is on the same day; just move forward
            xdate.hours, xdate.minutes, xdate.seconds = 17, 0, 0
        else:
            # 5 PM is on the next day
            xdate.hours, xdate.minutes, xdate.seconds = 17, 0, 0
            xdate.inc()
            while not self.isbusinessday(xdate):
                xdate.inc()
        return xdate

    def inc_n_days_to_time(self, xdate, days, hours, minutes=0):
        xdate = self.inc_n_days(xdate, days)
        d1 = datetime.datetime(xdate.year, xdate.month, xdate.day, xdate.hours,
             xdate.minutes)
        d2 = datetime.datetime(xdate.year, xdate.month, xdate.day, hours,
             minutes)
        if d1 < d2:
            xdate.hours, xdate.minutes, xdate.seconds = hours, minutes, 0
        else:
            # this is on the next day
            xdate.hours, xdate.minutes, xdate.seconds = hours, minutes, 0
            xdate.inc()
            while not self.isbusinessday(xdate):
                xdate.inc()
        return xdate

    def inc_emergency(self, xdate, hours=2):
        xdate.hours = xdate.hours + hours
        if xdate.hours >= 24:
            xdate.hours = xdate.hours - 24
            xdate.inc()
        # do we need the weekend/holiday check?
        return xdate

    def inc_48_hours_7am(self, xdate):
        """ 48 hours after next business 7AM. """
        # next business 7 AM
        xdate.inc()
        xdate.hours, xdate.minutes, xdate.seconds = 7, 0, 0
        while not self.isbusinessday(xdate):
            xdate.inc()
        # plus 48 hours
        xdate = self.inc_48_hours(xdate)
        return xdate

    def set_midnight(self, xdate):
        # "Midnight" is 23:59:59 here.
        xdate.hours, xdate.minutes, xdate.seconds = 23, 59, 59
        return xdate

    def inc_n_hours(self, xdate, n=1):
        """ Move forward by n hours. 'n' is supposed to be a small number; for
            moving forward days, use inc_n_days().
            This method does *not* take non-business days into account.
        """
        xdate.hours = xdate.hours + n
        if xdate.hours >= 24:
            xdate.hours = xdate.hours - 24
            xdate.inc()
        return xdate

    def inc_to_business_day(self, xdate):
        """ Move a date forward until we get a business day.  If it's already
            a business day, it will remain unchanged. """
        while not self.isbusinessday(xdate):
            xdate.inc()
        return xdate

    def dec_to_business_day(self, xdate):
        """ If the given date is not a business date, MOVE BACK to the LAST
            business date. Usually this means that if the date falls in a
            weekend, we move back to the previous Friday. """
        while not self.isbusinessday(xdate):
            xdate.dec()
        return xdate

    def inc_to_business_day_7am(self, xdate):
        """ If the date falls on a holiday/weekend, increase it until it falls
            on a business day; then set the time to 7 AM. If the date did fall
            on a business day, it remains unchanged. """
        if self.isbusinessday(xdate): return xdate # unchanged
        xdate = self.inc_to_business_day(xdate)
        xdate.set_time(7, 0, 0)
        return xdate

    @staticmethod
    def dec_if_midnight(xdate, minutes=1):
        if xdate.hours == xdate.minutes == xdate.seconds == 0:
            xdate.dec_time(minutes=1)           
        return xdate
