# baseresponder.py

class BaseResponder(object):

    def delete_from_queue(self, ident):
        s = "Removing locate from queue: %s" % (ident,)
        self.log.log_event(s)
        if getattr(self, 'responderdata', None) and \
         self.responderdata.queue['table'] == 'responder_multi_queue':
            self.dbado.deleterecords('responder_multi_queue', locate_id=ident,
             respond_to=self.responderdata.queue['respond_to'])
        else:
            self.dbado.deleterecords("responder_queue", locate_id=ident)

    def filter_responses(self, rows):
        for i in range(len(rows)-1, -1, -1):
            row = rows[i]

            if row['status'] in ['-R']:
                self.delete_from_queue(row['locate_id'])
                self.log.log_event("Record skipped: %s; reason: status %s" % (
                 row['locate_id'], row['status']))
                del rows[i]

            if not self.responderdata.send_emergencies:
                if row['ticket_type'] == 'EMERGENCY' \
                or row.get('kind') == 'EMERGENCY':
                    self.delete_from_queue(row['locate_id'])
                    self.log.log_event("Record skipped: %s; reason: emergency"
                     % (row['locate_id'],))
                    del rows[i]

