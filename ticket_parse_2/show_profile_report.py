import date

def show_profile_report(basename_prefix, group, tdb, verbose=1):
    if verbose: print "Writing profiler data...",
    d = date.Date().isodate()
    d = d.replace("-", "").replace(":", "").replace(" ", "_")
    basename = basename_prefix + group + "_" + d
    tdb.profiler.write_text(basename + ".txt")
    tdb.profiler.write_csv(basename + "_csv.txt")
    if verbose: print "OK"
