import string
import re
from parsers import BaseParser
from re_shortcut import R
import tools
import locate
from static_tables import ticket as ticket_table
from summaryparsers import BaseSummaryParser
import summarydetail

class Parser(BaseParser):

    re_ticket_number = R("Notice#: (\d+)")
    re_call_date = R("Create Date/Time: ([0-9/]+) ([0-9:]+)\s+(AM|PM)",
     re.DOTALL)
    re_transmit_date = re_call_date
    re_work_date = R("Work Date/Time: ([0-9/]+) ([0-9:]+)\s+(AM|PM)",
     re.DOTALL)

    re_ticket_type = R("Notice Type: (.*?) *Must Update")

    re_work_address_street = R("EXCAVATION ADDRESS.*?Address: (.*?) *$",
     re.DOTALL)
    re_work_cross = R("EXCAVATION ADDRESS.*?Intersecting Street: (.*?) *$",
     re.DOTALL)
    re_work_city = R("EXCAVATION ADDRESS.*?City: (.*?) *$", re.DOTALL)
    re_work_county = R("EXCAVATION ADDRESS.*?County: (.*?) *$", re.DOTALL)
    re_work_state = R("EXCAVATION ADDRESS.*?State: +(.*?) *$", re.DOTALL)

    re_work_description = R("DESCRIPTION OF WORK\s+=+\s+(.*?)\s+^Type of Work:",
     re.DOTALL)
    re_work_type = R("^Type of Work: (.*?) *$")
    re_duration = R("Work Duration: (.*?) +(Boring|Drilling|Blasting)")
    re_caller_contact = R("Site Contact: (.*?) +Site")

    re_work_remarks = R("REMARKS/HISTORY\s+=+\s+(.*?)(\s*)MEMBERS INVOLVED",
     re.DOTALL)

    f_call_date = lambda s, m: s.isodate(string.join(m.groups(), " "))
    f_transmit_date = f_call_date
    f_work_date = f_call_date

    def f_work_description(self,match):
        if len(match.group(1)) > ticket_table[10][1].width:
            return match.group(1)[:ticket_table[10][1].width - 12] + " (More ...)"
        else:
            return match.group(1)

    def find_locates(self):
        lines = string.split(self.data, "\n")
        locates = []
        found = False
        for line in lines:
            line = string.strip(line)
            if line.startswith("MEMBERS INVOLVED"):
                found = True
                continue
            if found:
                if line and not line.startswith("===="):
                    locates.extend(string.split(line))
        return map(locate.Locate, locates)

    def find_kind(self):
        type = self.ticket.ticket_type.lower()
        if type.find("emer") > -1 or type.find("rush") > -1:
            return "EMERGENCY"
        else:
            return "NORMAL"

    def post_process(self):
        # Used to extract caller information and some other data.

        # try to get address number
        s = self.ticket.work_address_street
        parts = string.split(s)
        if parts and parts[0][0] in "0123456789":
            self.ticket.work_address_number = parts[0]
            self.ticket.work_address_street = string.join(parts[1:], " ")

        # get the lines of the caller information block, but only the lines
        # that are non-empty
        lines = string.split(self.data, "\n")
        idx = -1
        for i in range(len(lines)):
            line = lines[i]
            if line.startswith("CALLER INFORMATION"):
                idx = i
                break
        if idx > -1:
            vlines = []
            for line in lines[idx+2:]:
                if not line.strip():
                    continue    # skip empty lines
                if line.startswith("REMARKS/HISTORY"):
                    break
                vlines.append(line[:40])    # note the :40 !!!
            while len(vlines) < 5:
                vlines.append("")

            # The caller information section looks kinda like this:
            #
            #  CALLER INFORMATION
            #  ==================
            #
            #  name
            #  company (con_name?)
            #  phone numbers
            #  address
            #
            #  city, state  zip
            self.ticket.caller = vlines[0].strip()
            self.ticket.con_name = vlines[1].strip()
            self.ticket.caller_phone = string.join(string.split(vlines[2])[:2], " ")
            self.ticket.caller_altphone = string.join(string.split(vlines[2])[2:4], " ")
            self.ticket.con_address = vlines[3].strip()
            parts = string.split(vlines[4], ",")
            self.ticket.con_city = parts and parts[0].strip() or ""
            if len(parts) > 1:
                parts2 = string.split(parts[1]) # state and zip
                self.ticket.con_state = parts2 and parts2[0].strip() or ""
                self.ticket.con_zip = parts2[1:] and parts2[1].strip() or ""

        # call parent's post_process
        BaseParser.post_process(self)

class SummaryParser(BaseSummaryParser):

    re_client_code = R("^PUPS DAILY AUDITS for (\S+)")
    re_summary_date = R("^PUPS DAILY AUDITS for \S+ on (\S+)")
    re_total = R("^TOTAL AUDITS for \S+ = (\d+)")

    def has_header(self, data):
        return string.find(data, "PUPS DAILY AUDITS for") > -1

    def has_footer(self, data):
        return string.find(data, "TOTAL AUDITS for") > -1

    def read(self, data):
        tickets = []
        lines = string.split(data, "\n")
        for i in range(len(lines)):
            line = lines[i]
            if line and line[0] in "0123456789":
                # we have a ticket number, sequence number, notice, status,
                # last action, and method sent
                ticket_number, line = line[:13], line[13:].strip()
                ticket_number = ticket_number[-7:]  # remove leading zeroes
                number, line = line[:3], line[3:].strip()
                token = line[:15].strip()
                detail = summarydetail.SummaryDetail(
                         [number, ticket_number, "00:00", "", token])
                tickets.append(detail)
        return tickets

    def get_client_code(self, data):
        m = self.re_client_code.search(data)
        client = m and m.group(1) or ""
        return client

    def get_summary_date(self, data):
        m = self.re_summary_date.search(data)
        d = m and m.group(1) or ""
        if d:
            d = "%s-%s-%s 00:00:00" % (d[-4:], d[:2], d[3:5])
        return d

    def get_expected_tickets(self, data):
        m = self.re_total.search(data)
        xt = m and int(m.group(1)) or 0
        return xt
