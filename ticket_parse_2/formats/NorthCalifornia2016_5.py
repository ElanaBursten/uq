# NorthCalifornia2016_5.py

import korterra_summary_parser
import NorthCalifornia2016

class Parser(NorthCalifornia2016.Parser):
    def pre_process(self):
        self.data = self.data.replace('\r\n', '\n') # fix line-endings

class SummaryParser(korterra_summary_parser.BaseKorTerraSummaryParser):
    def get_client_code(self, data):
        return 'NCA5'

    def ticket_number_from_jobid(self, jobid):
        if jobid.startswith('CN'):
            jobid = jobid[2:]
        if jobid.endswith('S'):
            jobid = jobid[:-1]
        return jobid
