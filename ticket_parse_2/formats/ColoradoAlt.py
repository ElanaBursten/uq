# ColoradoAlt.py

import Colorado

class Parser(Colorado.Parser):
    call_center = 'ColoradoAlt'
    # The ColoradoAlt format is really meant to process a different kind of
    # audit.  There is no separate format for regular tickets.
