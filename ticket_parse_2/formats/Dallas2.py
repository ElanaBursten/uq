# Dallas2.py

import Dallas1
from re_shortcut import R

class Parser(Dallas1.Parser):
    call_center = 'Dallas2'

class SummaryParser(Dallas1.SummaryParser):
    rheader = R("^(?:TESS|WELLSCO) audit for CDC of")
    rfooter = R("^Total number of Messages for \S+ +\d+")
    rclient = R("(?:TESS|WELLSCO) audit for CDC of (.*) +on")
    rsummarydate = R("(?:TESS|WELLSCO) audit for.*on (.*)$")
    rexpected = R("Total number of Messages for \S* +(.*)$")

    # XXX problem here is, that (TESS|WELLSCO) counts as a group... >=(
