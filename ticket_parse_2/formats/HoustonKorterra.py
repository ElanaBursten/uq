# HoustonKorterra.py

import parsers
import Delaware3
import Dallas1

class Parser(Dallas1.Parser):
    call_center = 'HoustonKorterra'

class SummaryParser(Delaware3.SummaryParser):
    def get_client_code(self, data):
        m = self.r_client_code.search(data)
        if m:
            cc = m.group(1)
            if cc.endswith('-SMTP-1'): cc = cc.split('-')[0]
            return cc
        else:
            return "FHL3"

