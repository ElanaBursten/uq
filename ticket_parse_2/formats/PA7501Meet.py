# PA7501Meet.py

import re
#
import PA7501
import re_tools2
from tools import R

class Parser(PA7501.Parser):
    re_ticket_number = R("Meeting Request Number--\[(.*?)\]")
    re_serial_number = re_ticket_number
    re_work_date = R("(?:Proposed Meeting Date and Time)--\[(.*?)\] \[(.*?)\]")
    re_work_type = R("Type of Work--\s*\[(.*?)\]", re.DOTALL)
    re_caller_email = R("Email--\[(.*?)\]")

    line_re = re_tools2.bracket_line_re
    ANY = re_tools2.ANY_BRACKETED
    ANY_CAP = re_tools2.ANY_CAPTURED_BRACKETED

    re_revision = line_re('', 'Meeting Request Number--', None,
                          value=(ANY + '-' + ANY_CAP))
    re_channel = line_re('Meeting Request Number--', 'Channel#--', '',
                         value=(ANY_CAP + ANY_CAP))
    re_company = line_re('', 'Owner--', '')

class SummaryParser(PA7501.SummaryParser):
    pass

