# NewJersey2.py

from re_shortcut import R
import NewJersey

class Parser(NewJersey.Parser):

    re_serial_number = R("(GSUPLS\d+)")
    re_service_area_code = R("GSUPLS\d+-\d+\s+(\S+)")

    def post_process(self):
        NewJersey.Parser.post_process(self)
        self._remove_disclaimer()

    def _remove_disclaimer(self):
        """ Remove disclaimer from ticket image. """
        END = "End Request"
        idx = self.ticket.image.find(END)
        if idx > -1:
            z = idx + len(END)
            self.ticket.image = self.ticket.image[:z]

class SummaryParser(NewJersey.SummaryParser):
    pass
