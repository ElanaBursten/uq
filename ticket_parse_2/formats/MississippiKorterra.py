# TennesseeKorterra.py

import re
import string
#
import HoustonKorterra
import Tennessee
import tools

def R(regex, flags=0):
    return re.compile(regex, re.MULTILINE | re.IGNORECASE | flags)

class Parser(Tennessee.Parser):
    re_transmit_date = R("\w+\s+\d+\s+MSOCS\s+([0-9/]+)\s+([0-9:]+)")
    re_caller_altphone = R("^CONTACT--.*?PHONE--\[(.*?)\]")
    re_caller_email = R("^CONTACT--.*?EMAIL--\[(.*?)\]")
    _re_work_long = R("LONGITUDE--\[(.*?)(\]|$)")
    # The sample tickets have a lot of unclosed []

    def find_work_description(self):    # or is this work_remarks?
        stub = []
        ok = 0
        for line in string.split(self.data, "\n"):
            line = string.strip(line)
            if ok:
                if line.startswith("["):
                    stub.append(line[1:-1])
                else:
                    break
            else:
                if line.startswith("LOCATION INFORMATION"):
                    ok = 1

        return string.join(map(string.strip, stub), " ")

    def find_work_long(self):
        longitude = 0.0
        longitudes = self._re_work_long.findall(self.data)
        if longitudes:
            try:
                longitude = tools.average([float(x) for x, _ in longitudes
                                           if x != '0'])
            except:
                longitude = 0.0
        return longitude

class SummaryParser(HoustonKorterra.SummaryParser):
    STRIP_NUMBER = 0  # don't strip any digits


