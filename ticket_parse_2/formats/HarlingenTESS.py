# HarlingenTESS.py

import Harlingen
import Washington2
from date import Date
import summarydetail
from re_shortcut import R

class Parser(Harlingen.Parser):
    call_center = 'HarlingenTESS'

class SummaryParser(Washington2.SummaryParser):
    re_ticket = R("(\*?)(\d+) TESS(\d+)-(\d+) (\d\d:\d\d)")
    DEFAULT_CLIENT_CODE = 'TESS'

    def get_summary_date(self, data):
        m = self.re_date.search(data)
        if m:
            d = Date(m.group(3) + " " + m.group(4))
        else:
            d = Date()
        if d.hours <= 7:
            d.dec()
        d.resettime()
        return d.isodate()

    def read(self, data):
        tickets = []
        results = self.re_ticket.findall(data)
        for r in results:
            indicator, seqnr, ticket_number, version, time = r
            t = [seqnr, ticket_number, time, version, indicator]
            detail = summarydetail.SummaryDetail(data=t)
            tickets.append(detail)
        return tickets
