import string
import re
import operator

from parsers import BaseParser
from re_shortcut import R
import tools
import locate
import date

from summaryparsers import BaseSummaryParser
import summarydetail

class Parser(BaseParser):

    re_ticket_number = R("^ ticket +(\S+) +xref")
    re_transmit_date = R("sent +(.*?) +(.*?) +(.*?) sequence")
    re_call_date = R("^ taken +(\S+) +(\S+) +(\S+)")

    re_work_state = R("^ county +(\S+) ")
    re_work_county = R("^ county +\S+ (.*?)$")
    re_work_city = R("^ place +(.*?) +class")

    _re_work_date = R("^ begin +(\S+) +(\S+) +(\S+)")
    re_work_type = R("^ work +(.*?)\s+contact", re.DOTALL)
    re_caller_contact = R("^ contact +(.*?) +for")
    re_company = R("^ contact.*?for (.*?) *$")
    re_caller_phone = R("^ +ph (\S+) ")
    re_caller_fax = R("^ +ph.*?fax ph +(.*?) *type")
    re_work_remarks = R("^ note +(.*?)\s+(within|eom)", re.DOTALL)
    re_work_description = R("^ where +(.*?)\s+begin", re.DOTALL)

    re_work_address_street = R("^ locn +.*?\"(.*?)\"")
    re_work_address_number = R("^ locn +(.*?) *\"")
    re_work_cross = R("^ locn +.*?\".*?\" *(.*?)\s*(where|county)", re.DOTALL)

    _re_ticket_type = R("^ *\*\*\* (.*?) \*\*\*.*?$")

    f_transmit_date = lambda s, m: \
     s.isodate(m.group(1) + " " + m.group(2) + " " + m.group(3))
    f_call_date = f_transmit_date

    #find_ticket_type = find_kind = lambda self: "NORMAL"
    # At this point, I don't know what the ticket type looks like on an Ohio
    # ticket!

    _re_locates = R("sequence \d+ to OUPS station # \d+ for (\S+)")

    def find_work_date(self):
        # if work_date isn't on the ticket, set it to "" and let post_process
        # handle the rest
        m = self._re_work_date.search(self.data)
        if m:
            return self.f_transmit_date(m)
        else:
            return ""

    def find_locates(self):
        locates = []
        m = self._re_locates.search(self.data)
        if m:
            name = m.group(1)
            locates.append(locate.Locate(name))
        return locates

    def find_ticket_type(self):
        m = self._re_ticket_type.search(self.data)
        if m:
            return m.group(1).strip()
        else:
            return "-"

    # same as FWP1's
    def post_process(self):
        # nasty bit of hackery... the parser should not have to know about due
        # dates, or even about call centers
        if not self.ticket.work_date:
            d = date.Date(self.ticket.call_date)
            import duedatecatalog
            dc = duedatecatalog.DueDateCatalog("FWP3", [])
            d = dc.inc_n_days(d, 2)
            self.ticket.work_date = d.isodate()

        self.ticket.kind = (
          "EMERGENCY" if self.ticket.ticket_type == "SHORT NOTICE"
          else "NORMAL")

        BaseParser.post_process(self)

class SummaryParser(BaseSummaryParser):

    def has_header(self, data):
        return string.find(data, "MESSAGE LIST BY STATION") > -1

    def has_footer(self, data):
        return string.find(data, "TOTALS FOR CDC") > -1

    def read(self, data):
        LENGTH = len("0013  0313-038-110-00 +")

        tickets = []
        for line in string.split(data, "\n"):
            if line and line[0] in "0123456789":
                while line:
                    begin, line = line[:LENGTH], string.strip(line[LENGTH:])
                    parts = string.split(begin)
                    number, ticket = parts[:2]
                    indicator = (len(parts) > 2) and "+" or ""
                    detail = summarydetail.SummaryDetail(
                             [number, ticket, "00:00", "", indicator])
                    tickets.append(detail)
        return tickets

    def get_client_code(self, data):
        m = R("STATION: \S+ +CDC: (\S+)").search(data)
        client = m and m.group(1) or ""
        return client

    def get_summary_date(self, data):
        m = R("MESSAGE LIST BY STATION.*?FOR: (\S+)", re.DOTALL).search(data)
        d = m and m.group(1) or ""
        if d:
            d = tools.isodate(d)
        return d

    def get_expected_tickets(self, data):
        m = R("^TOTALS FOR CDC \S+ +(\d+) +(\d+) +(\d+) +(\d+)").search(data)
        if m:
            numbers = [m.group(1), m.group(2), m.group(3), m.group(4)]
            numbers = reduce(operator.add, map(int, numbers))
        else:
            numbers = 0
        return numbers
