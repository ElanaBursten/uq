# Pennsylvania.py

import re
import string
#
from parsers import BaseParser, ParserError
from re_shortcut import R
from summaryparsers import BaseSummaryParser
import date
import locate
import summarydetail
import tools

class Parser(BaseParser):

    required = BaseParser.required[:]
    required.remove("work_date")

    re_transmit_date = R("POCS ([0-9/]+) ([0-9:]+)")

    re_ticket_number = R("Serial Number--\[(.*?)\]")
    re_ticket_type = R("\d+ +POCS +[0-9/]+ +[0-9:]+ \d+-\d+ *(.*?) *$")
    re_work_county = R("County--\[(.*?)\]")
    re_work_city = R("Municipality--\[(.*?)\]")
    re_work_address_street = R("Work Site--\[(.*?)\]")
    re_work_cross = R("Nearest Intersection--\[(.*?)\]")

    re_work_description = R("Location Information--\s\[(.*?)\]", re.DOTALL)
    re_work_type = R("Type of Work--\[(.*?)\]")
    re_company = R("Owner/Done for--\[(.*?)\]")

    re_work_date = R("(?:Proposed Dig|Scheduled Excavation) Date--\[(.*?)\] Dig Time--\[(.*?)\]")
    re_con_name = R("(?:Excavator|Contractor)--\[(.*?)\]")
    re_con_address = R("Address-+\[(.*?)\]")
    re_con_city = R("City-+\[(.*?)\]")
    re_con_state = R("City.*?State--\[(.*?)\]")
    re_con_zip = R("City.*?Zip--\[(.*?)\]")

    re_caller = R("Caller--\[(.*?)\]")
    re_caller_phone = R("Caller.*?Phone--\[(.*?)\]")
    re_caller_fax = R("FAX--\[(.*?)\]")
    re_caller_email = R("FAX.*?(?:E-mail address|Email)--\[(.*?)\]")
    re_caller_contact = R("Person to Contact--\[(.*?)\]")
    re_caller_altphone = R("Person to.*?Phone--\[(.*?)\]")
    re_work_extent = R("Extent of Excavation--\[(.*?)\]")

    re_call_date = R("Prepared--\[(.*?)\] +at +\[(.*?)\]")
    re_operator = R("Prepared.*?by +\[(.*?)\]")
    re_work_remarks = R("Remarks--\s\[(.*?)\]", re.DOTALL)
    re_legal_due_date = R("Lawful Dig Dates---\[(\S*?)\]\[(\d*?)\]")
    re_due_date = re_legal_due_date

    f_transmit_date = lambda s, m: s.isodate(m.group(1) + " " + m.group(2))

    def f_work_date(self, match):
        """ Date is in format like 19-MAR-02, and needs to be converted. """
        d, t = match.group(1), match.group(2)
        dd, dm, dy = d.split("-")
        dm = date.get_month_number(dm) # 1, 2, ..., 12
        if not dm:
            raise ParserError, "Unknown month '%s'" % (dm)
        newdate = "20%s-%02d-%s" % (dy, dm, dd)
        if len(t) == 0:
            # The time was left blank
            t = "0000"
        newtime = t[:2] + ":" + t[2:] + ":00"
        return newdate + " " + newtime
    f_call_date = f_work_date

    def f_legal_due_date(self, match):
        # due date is computed here, rather than in the router
        legal_due_date = self.f_work_date(match)
        d = date.Date(legal_due_date)
        # go back to the last non-weekend day
        d.dec()
        while d.isweekend():
            d.dec()
        return d.isodate()
    f_due_date = f_legal_due_date

    def find_locates(self):
        lines = []
        rloc = R("\S+ *=\S+")
        ok = 0
        # walking the ticket backwards...
        lines = self.data.split("\n")
        lines.reverse()

        locates = []
        for line in lines:
            line = line.strip()
            if not line:
                if locates:
                    break   # empty line before locates marks the end
                else:
                    continue    # ignore empty lines at the end
            elif "Copyright" in line:
                continue
            else:
                z = re.findall("(\S+) *=", line)
                locates[:0] = z

        return map(locate.Locate, locates)

    find_work_state = lambda s: "PA"

    def post_process(self):
        # Note: superclass's method is called last.
        # work_address_street often contains junk. Try to filter that out.
        a = self.ticket.work_address_street
        idx1 = a.find("**")
        idx2 = a.find("**", idx1+1)
        if idx2 > -1:
            left, right = a[:idx2], a[idx2:]
            left = left.replace("*", " ")
            left = string.join(left.split(), " ")
            self.ticket.work_address_number = left
            right = right.replace("*", " ")
            right = string.join(right.split(), " ")
            self.ticket.work_address_street = right
        else:
            a = a.replace("*", " ")
            a = string.join(a.split(), " ")
            self.ticket.work_address_street = a

        # do this *after* the other stuff, so the original post_process has
        # a chance to handle numbers like "1211-1213"
        BaseParser.post_process(self)

    def sanitycheck_after(self):
        BaseParser.sanitycheck_after(self)
        # if this is a design ticket ('DESIGN' or 'DSGN' in ticket_type),
        # then we don't require a work_date; otherwise no work_date is an error
        if (self.ticket.ticket_type.find("DESIGN") == -1) \
        and (self.ticket.ticket_type.find("DSGN") == -1):
            if not hasattr(self.ticket, "work_date") \
            or not self.ticket.work_date:
                raise AttributeError("Field [work_date] not found or empty")


class SummaryParser(BaseSummaryParser):
    re_page = R("PAGE 1 *$")
    re_footer = R("^Total: \d+ *$")
    re_client_code = R("^(\S+)\s+TICKET SUMMARY REPORT AS OF")
    re_summary_date = R(" *([0-9/]+) FROM PENNSYLVANIA ONE CALL")
    re_expected = R("^Total: (\S+)")
    re_ampm = R("[AP]M$")

    def has_header(self, data):
        C = "THIS IS THE END-OF-DAY SUMMARY AUDIT OF MESSAGES TRANSMITTED"
        return (string.find(data, C) > -1) and bool(self.re_page.search(data))
        # This header should be fool-proof, because it includes a "PAGE 1"
        # indicator!

    def has_footer(self, data):
        return not not self.re_footer.search(data)

    def get_client_code(self, data):
        m = self.re_client_code.search(data)
        client_code = m and m.group(1) or ""
        return client_code

    def get_summary_date(self, data):
        m = self.re_summary_date.search(data)
        if m:
            date = m.group(1)
            summary_date = tools.isodate(date)  # do not take time
        else:
            summary_date = ""
        return summary_date

    def get_expected_tickets(self, data):
        m = self.re_expected.search(data)
        expected_tickets = m and int(m.group(1)) or 0
        return expected_tickets

    def read(self, data):
        tickets = []
        for line in string.split(data, "\n"):
            line = string.strip(line)
            if line and line[0] in "0123456789":
                parts = string.split(line)
                assert len(parts) % 4 == 0
                while parts:
                    number, ticket_number, priority, time = parts[:4]
                    del parts[:4]
                    # prepend zeroes if necessary
                    m = self.re_ampm.search(time)
                    if m:
                        while len(time) < 7:
                            time = "0" + time
                        time = self._normalize_time(time)
                    else:
                        time = "%02d:%02d" % (int(time[0:2]), int(time[2:]))
                    detail = summarydetail.SummaryDetail(
                             [number, ticket_number, time, "", priority])
                    tickets.append(detail)
        return tickets

    def _normalize_time(self, time):
        """ Normalize a time like 09:12PM to ISO-time (21:12). """
        assert time.endswith("AM") or time.endswith("PM")
        time, ampm = time[:5], time[5:]
        hours, minutes = map(int, string.split(time, ":"))
        if hours == 12 and ampm == "AM":
            hours = 0
        elif hours == 12 and ampm == "PM":
            hours == 12
        elif ampm == "PM":
            hours = hours + 12
        return "%02d:%02d" % (hours, minutes)
