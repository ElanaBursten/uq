# SC1421PUPS.py

import re
import string
#
from parsers import BaseParser
from summaryparsers import BaseSummaryParser
import summarydetail
from tools import R
import tools
import locate

class Parser(BaseParser):
    re_ticket_number = R("^(?:Ticket|Notice) Number:\s+(\d+)")
    re_update_of = R("Old Notice:\s+(\d+)")
    re_call_date = R("^Created:\s*([0-9/]+) ([0-9:]+) (AM|PM)?")
    re_work_date = R("^Work Date:\s*([0-9/]+) ([0-9:]+) (AM|PM)?")
    re_transmit_date = R("([0-9/]+) ([0-9:]+) (AM|PM) \d+")
    re_work_state = R("^Excavation Information:\s+(\S+)", re.DOTALL)
    re_work_county = R("County:\s*(.*?)\s*Place:")
    re_work_city = R("Place:\s*(.*?)\s*$")
    re_caller = R("^Caller:\s*(.*?)\s*Phone:")
    re_caller_phone = R("Phone:\s*(.*?)\s*$")
    re_caller_email = R("Caller Email:\s*(\S+)")
    re_caller_fax = R("^Company Fax:\s*(.*?)\s*Type:")
    re_work_address_street = R("^Street:\s*(.*?)(?:\s*Address In Instructions.*|\s*)$") 
    re_work_cross = R("^Intersection:\s*(.*?)\s*$")
    re_work_type = R("Work Type:\s*(.*?)\s*$")
    re_explosives = R("^Explosives:\s*(\S+)")
    re_con_name = R("Caller Information:?\s*\n(.*?)\s*$")
    re_con_address = R("Caller Information:?\s*\n.*?\n(.*?)\s*$")
    re_con_city = R("Caller Information:?\s*\n.*?\n.*?\n(.*?)\s*,")
    re_con_state = R("Caller Information:?\s*\n.*?\n.*?\n.*?\s*,\s*(\S+)")
    re_con_zip = R("Caller Information:?\s*\n.*?\n.*?\n.*?\s*,\s*\S+\s+(\S+)")

    re_work_remarks = R("^Remarks:\s*(.*?)Member Utilities Notified:", re.S)
    re_work_description = R("^Instructions:\s*(.*?)Directions:", re.S)

    re_ticket_type = R("[0-9/]+ [0-9:]+ [AP]M \d+\s*(.*?)\s*$")

    # date/times may or may not have AM/PM indicator
    def f_transmit_date(self, match):
        has_am_pm = match.groups()[2].strip()
        if has_am_pm:
            return tools.isodate("%s %s %s" % match.groups()[:3])
        else:
            return tools.isodate("%s %s" % match.groups()[:2])
    f_work_date = f_call_date = f_transmit_date

    _re_company_old = R("Work Done By:\s*(.*?)\s*Duration:")
    _re_company_new = R("Work Done For:\s*(.*?)\s*$")
    def find_company(self):
        for r in [self._re_company_new, self._re_company_old]:
            s = tools.re_get(r, self.data)
            if s:
                return s

    LOCATES_END = ["Members are to contact",
                   "Design tickets are",
                   "Members are to respond"]

    def find_locates(self):
        lines = self.data.split('\n')
        locates = []
        # start of member section
        idx = tools.findfirst(lines,
              lambda s: s.startswith("Member Utilities Notified:"))
        # end of member section, if any
        idx2 = tools.findfirst(lines[idx:],
              lambda s: s.startswith(tuple(self.LOCATES_END)))

        if idx > -1:
            low = len(lines) if idx2 == -1 else idx2+idx
            locates = string.join(lines[idx+1:low], " ").split()
            # strip any trailing asterisks (M#3165)
            locates = [loc.strip('*') for loc in locates]
            # extra rule to filter out bogus locate names
            locates = [lc for lc in locates if not tools.contains_lowercase(lc)]
            locates = sorted(tools.unique(locates))
            locates = [locate.Locate(loc) for loc in locates]
        return locates

    _re_latlong_1 = R("^Lat/Lon?g:\s*([0-9.-]+)\s*,\s*([0-9.-]+)")
    _re_latlong_2 = R("^Second:\s*([0-9.-]+)\s*,\s*([0-9.-]+)")

    def post_process(self):
        self.check_max_locates = False

        lats, longs = [], []
        # parse lat, long, map_page
        for regex in (self._re_latlong_1, self._re_latlong_2):
            m = regex.search(self.data)
            if m:
                g1 = float(m.group(1))
                g2 = float(m.group(2))
                # Assume neither lat nor long can be 0
                if g1 and g2:
                    lats.append(g1)
                    longs.append(g2)
        self.ticket.work_lat = tools.average(lats) if lats else 0.0
        self.ticket.work_long = tools.average(longs) if longs else 0.0
        if self.ticket.work_lat and self.ticket.work_long:
            self.ticket.map_page = tools.decimal2grid(
             self.ticket.work_lat, self.ticket.work_long)

        # try to extract street number from address
        parts = self.ticket.work_address_street.split()
        if parts and parts[0][0] in "0123456789":
            self.ticket.work_address_street = string.join(parts[1:], " ")
            self.ticket.work_address_number = parts[0]

        # remove superfluous whitespace
        self.ticket.work_remarks = self.ticket.work_remarks.strip()
        self.ticket.work_description = self.ticket.work_description.strip()

        BaseParser.post_process(self)


class SummaryParser(BaseSummaryParser):

    S_HEADER = R("From (\S+)")
    S_FOOTER = R("^TOTAL\s+:\s+(\d+)", re.I)
    S_AUDIT_DATE = R("^Audit For ([0-9/]+)")

    def has_header(self, data):
        return bool(self.S_HEADER.search(data))

    def has_footer(self, data):
        return bool(self.S_FOOTER.search(data))

    def get_client_code(self, data):
        return "SC811" # dummy; audit contains multiple clients

    def get_summary_date(self, data):
        m = self.S_AUDIT_DATE.search(data)
        if m:
            d = m.group(1)
            summary_date = tools.isodate(d)
        else:
            summary_date = ""
        return summary_date

    def get_expected_tickets(self, data):
        m = self.S_FOOTER.search(data)
        xt = m and int(m.group(1)) or 0
        return xt

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        idx = tools.findfirst(lines, lambda s: "Type" in s and "Seq#" in s)
        if idx >= 0:
            for line in lines[idx+2:]: # skip line with "---"s
                if not line.strip(): break

                type_code = line[:6].strip()
                parts = line[6:].split()
                seq_no, ticket_number = parts[:2]
                detail = summarydetail.SummaryDetail([seq_no, ticket_number,
                 "00:00", type_code, ""])
                tickets.append(detail)

        return tickets


