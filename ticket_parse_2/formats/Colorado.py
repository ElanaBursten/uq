# Colorado.py

from parsers import BaseParser
from re_shortcut import R
import re
import string
import tools
import quadrilateral
import locate
from summaryparsers import BaseSummaryParser
import summarydetail

class Parser(BaseParser):
    call_center = 'Colorado'


    MEMBERS = []

    G_MEMBERS = [
     "PCGRE1", "PCND00", "PCNC00", "PCPB01", "PCPB02", "PCEP01",
     "PCIS01", "PCSD00", "PCNDU0",
     "PCDM01", "PCDM02", "PCDM03", "PCDM04", "PCGW01", "PCGW02", "PCNW01",
     "PCNW02", "PCNW03", "PCNW04", "PCNW05", "PCNW06", "PCNW07", "PCNW08",
     "PCSE01", "PCSE02", "PCSE03", "PCSE04", "PCSE05", "PCSE06", "PCSE07",
     "PCSE08", "PCSE09", "PCSW01", "PCSW02", "PCSW03", "PCSW04", "PCSW05",
     "PCSW06", "PCSW07", "PCSW08", "PCSW09", "PCSW10", "PCBR01", "PCBR02",
     "PCBR03"
    ]


    re_transmit_date = R("[A-Z0-9]+ +\d+ UNCC[a-z]? ([0-9/]+) ([0-9:]+) (AM|PM)")
    re_ticket_type = R("[A-Z0-9]+ +\d+ UNCC[a-z]? [0-9/]+ [0-9:]+ \S+ \S+ (.*?) *$")

    #re_ticket_number = R("Ticket Nbr: ([A-Z0-9]+)")
    re_ticket_number = R("^Ticket Nbr:\s+([A-Z0-9]+-0\S+)")
    re_revision = R("^Ticket Nbr:\s+[A-Z0-9]+-(0\S+)")
    re_call_date = R("^Original Call Date: ([0-9/]+) +Time: ([0-9:]+) (AM|PM)")
    re_operator = R("Original.*?Op: (\S+)")
    re_work_date = R("Locate By Date +: ([0-9/]+) +Time: ([0-9:]+) (AM|PM)")
    re_update_of = R("^Ticket Nbr:.*Update of: (\S+)")

    re_work_state = R("^State: (\S+)")
    re_work_county = R("County: (.*?) *City")
    re_work_city = R("City: (.*?) *$")
    re_work_address_number = R("^Addr: *(.*?) *Street")
    re_work_address_street = R("^Addr.*?Street: (.*?) *$")
    re_work_cross = R("Near Intersection\(s\): (.*?) *$")

    re_work_type = R("^Type of Work: (.*?) *Exp\.:")
    re_con_name = R("^Company : (.*?) *(Type:|$)")
    re_caller = R("^Caller  : (.*?) *Phone")
    re_caller_phone = R("^Caller.*?Phone: (.*?) *$")
    re_caller_fax = R("^Fax: (.*?) *Email")
    re_caller_email = R("^Fax.*?Email: (.*?) *$")
    re_company = R("^Done for: (.*?) *$")
    re_map_page = R("Grids: (\S+)")
    re_explosives = R("Exp.:\s+(\S+)")
    re_work_lat = R("^Lat/Long: *(\S+)/")
    re_work_long = R("^Lat/Long: *\S+/(\S+)")

    f_transmit_date = lambda s, m: s.isodate(string.join(m.groups(), " "))
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date

    keyword_section = R("Exp.:\s+\S+(.*)", re.DOTALL)

    # special:
    # grids

    def _find_something(self, text):
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith(text))
        idx2 = tools.findfirst(lines[idx1+1:],
               lambda s: not s.startswith(" " * (len(text)-1) + ":"))
        desc = ""
        for i in range(idx1, idx2+idx1+1):
            desc = desc + lines[i][len(text)+1:].strip() + " "
        return desc.strip()

    def find_work_description(self):
        return self._find_something("Location:")

    def find_work_remarks(self):
        return self._find_something("Remarks:")
        # haven't seen any remarks in sample tickets...

    # this is *NOT* how we assign locates for Colorado, but I leave it here in
    # case people change their minds:
    def __find_locates(self):
        locates = []
        lines = string.split(self.data, "\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Members"))
        idx2 = tools.findfirst(lines, lambda s: not s.startswith("Members"))
        for i in range(idx1, idx2+idx1+1):
            chunk = lines[i][9:]
            names = string.split(chunk)
            for name in names:
                if name.startswith(":"):
                    name = name[1:]
                locates.append(locate.Locate(name))

        return locates

    # we ignore the members section, and take the "term id" in the ticket
    # header as the (only) locate name.
    def find_locates(self):
        locates = []
        re_locates = R("([A-Z0-9]+) *\d+ *UNCC[a-z]? \d\d")
        m = re_locates.search(self.data)
        if m:
            name = m.group(1)
            locates.append(locate.Locate(name))

        # we also scan the members section for certain names:
        for line in self.data.split("\n"):
            if line.startswith("Members "):
                chunk = line[len("Members "):]
                parts = chunk.split()
                for part in parts:
                    if part.startswith(":"):
                        part = part[1:]
                    if part in self.MEMBERS:
                        locates.append(locate.Locate(part))

        # if locates in G_MEMBERS are present, then add name+"G"
        for loc in locates[:]:
            if loc.client_code in self.G_MEMBERS:
                gname = loc.client_code + "G"
                if gname not in [a.client_code for a in locates]:
                    locates.append(locate.Locate(gname))

        return locates

     #attempt here! 
    def work_lat_long(self):
        
        """
        Determine the work latitude and longitude
        Using the quad pairs given.
        First split line 1 with the spaces
        Second split line 1 with the slash.
        Lat/Long: 39.794571/-104.757816 39.794571/-104.755289
        : 39.792794/-104.757816 39.792794/-104.755289
        """
        lines = self.data.split("\n")
        idx = tools.findfirst(lines, lambda s: s.lstrip().startswith("Lat/Long"))
        if idx > -1:
            # Found it
            line = lines[idx]
            try:
                parts = line.split(':')[1]
                parts = parts.replace('/'," ")
                lat1, long1, lat2, long2 = [float(item) for item in parts.split()] # Split on whitespace
                # Get the next line
                line = lines[idx+1]
                line = line.replace('/'," ")
                line = line.replace(':'," ")
                lat3,long3,lat4,long4 = [float(item) for item in line.split()]
                # Extract the geometric centroid of the quadrilateral
                q = quadrilateral.Quadrilateral([(lat1,long1),
                                                 (lat2,long2),
                                                 (lat3,long3),
                                                 (lat4,long4)])

                return q.centroid()
            except:
                return 0.0, 0.0
        else:
            return 0.0, 0.0

    def find_work_long(self):
        """
        Find the work longitude
        """
        lat,long = self.work_lat_long()
        return long

    def find_work_lat(self):
        """
        Find the work latitude
        """
        lat,long = self.work_lat_long()
        return lat








    

    def post_process(self):
        # if this is a "meet" ticket, add MEET to the end of ticket_type
        re_meet = R("Meet: Y")
        m = re_meet.search(self.data)
        if m:
            #The line below adds MEET to the ticket.
            #CO mgmt has asked that we add it to the front and back of the ticket type.
            #self.ticket.ticket_type += " MEET"
            self.ticket.ticket_type = "MEET "  + self.ticket.ticket_type + " MEET"


        BaseParser.post_process(self)

class SummaryParser(BaseSummaryParser):
    RECOG_HEADER = "Summary of tickets sent to"
    re_line = R("^\d+\s+\d+\s+UNCC\d+")
    re_date = R("Summary of tickets sent.*at ([0-9/]+) ([0-9:]+)(AM|PM)")

    def has_header(self, data):
        return string.find(data, self.RECOG_HEADER) > -1

    def has_footer(self, data):
        return 1    # there is no footer, really

    def get_expected_tickets(self, data):
        # there no line that tells us how many tickets to expect, so we
        # have to improvise:
        z = self.re_line.findall(data)
        return len(z)

    def get_summary_date(self, data):
        m = self.re_date.search(data)
        if m:
            return m.group(1).replace("/", "-") + " 00:00:00"
        else:
            return date.Date().isodate()[:10] + " 00:00:00"

    def get_client_code(self, data):
        return "UNCC"   # there is no client code

    def read(self, data):
        tickets = []
        for line in data.split('\n'):
            if self.re_line.search(line):
                ticket_id = line[:15].strip()
                seqno = line[17:21].strip()
                serial_number = line[23:43].strip()
                priority = line[44:60].strip()
                detail = summarydetail.SummaryDetail(
                         [seqno, ticket_id, "00:00", "", ""])
                tickets.append(detail)

        return tickets
