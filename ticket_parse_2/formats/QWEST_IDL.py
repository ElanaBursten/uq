# QWEST_IDL.py

import Albuquerque
from re_shortcut import R

class Parser(Albuquerque.Parser):
    # Eliminated from a previous commit?
    #find_map_page = parser_mixins.FindSecondTownshipMixin.find_map_page
    #_re_map_page = R("Town:\s+(\S+)\s+Ran:\s+(\S+)\s+Sect 1/4:\s+(\S+ \S+)")

    re_serial_number = R("(IDL\d+)-?\s+(\d+|Internals)")
    re_caller_altcontact = R("^Alternate Contact:\s+(.*?)(\s+Alternate Phone|$)")
    re_caller_altphone = R("Alternate Phone:\s+(.*?)(\s+|$)")
    re_caller_fax = R("Fax No:\s+(.*?)(\s+|$)")
    re_work_cross = R("^Nearest Intersecting Street:\s+(.*?)\s*$")
    re_work_date = R(
     "^(?:Work to Begin|Locate By) Date: +([0-9/]+) +Time: +([0-9:]+) (AM|PM)")

class SummaryParser(Albuquerque.SummaryParser):
    re_ticket = R("(\*?)(\d+) (?:\S+/)?((?:IDL|OOC|UULC|WOC|IEUCC)\d+)\s+(\d\d:\d\d)")

    def get_client_code(self, data):
        # there is no client code on this type of summaries, so we make one up
        return "QWEST"

