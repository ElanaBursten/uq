# Harlingen.py

import Dallas1
import locate
from re_shortcut import R
import date

class Parser(Dallas1.Parser):
    call_center = 'Harlingen'

    re_map_ref = R("^MapRef : *(.*?) *Grid:")

    def post_process(self):
        if not self.ticket.transmit_date:
            self.ticket.transmit_date = self.ticket.call_date

        # don't update map_ref for tickets other than SSR/SSM
        if self.ticket.locates \
        and self.ticket.locates[0].client_code not in ('SSR', 'SSM'):
            self.ticket.map_ref = None

        # add other client codes if PUB is present
        names = [loc.client_code for loc in self.ticket.locates]
        if "PUB" in names:
            new = [locate.Locate(x) for x in ('PUBW', 'PUBS', 'PUBE')]
            self.ticket.locates.extend(new)

        Dallas1.Parser.post_process(self)

class SummaryParser(Dallas1.SummaryParser):
    r2_summary_date = R("Last Audit was on (.*?) at")
    r2_header = R("^Audit for \S+ of \S+ *(Last Audit was)?")

    def get_summary_date(self, data):
        d = Dallas1.SummaryParser.get_summary_date(self, data)
        if d:
            return d
        else:
            m = self.r2_summary_date.search(data)
            if m:
                s = m.group(1)
                return "20%s-%s-%s 00:00:00" % (s[:2], s[2:4], s[4:6])
            else:
                # if no date is found, use today's
                # XXX this may not be correct
                return date.Date().isodate()

    def has_header(self, data):
        hh = not not self.r2_header.search(data)
        return hh or Dallas1.SummaryParser.has_header(self, data)

    def read(self, data):
        tickets = Dallas1.SummaryParser.read(self, data)
        tickets = [t for t in tickets if not t[1].endswith("ds")]
        for t in tickets:
            if t[1].endswith("ET"):
                t[1] = t[1][:-2]    # strip that trailing 'ET'
        return tickets
