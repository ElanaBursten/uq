# Washington3.py

import Richmond3

class Parser(Richmond3.Parser):
    PARSE_GRIDS = False
    def post_process(self):
        Richmond3.Parser.post_process(self)
        if self.ticket.update_of and 'UPDATE' not in self.ticket.kind:
            self.ticket.kind += ' UPDATE'

class SummaryParser(Richmond3.SummaryParser):
    pass
