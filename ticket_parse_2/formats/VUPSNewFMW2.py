# VUPSNewFMW2.py

import _VUPSNew

class Parser(_VUPSNew.Parser):
    PARSE_GRIDS = True
    def post_process(self):
        _VUPSNew.Parser.post_process(self)
        if self.ticket.update_of and 'UPDATE' not in self.ticket.kind:
            self.ticket.kind += ' UPDATE'

import Richmond3
class SummaryParser(Richmond3.SummaryParser):
    pass
