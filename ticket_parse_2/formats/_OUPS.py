# _OUPS.py

import string
#import sys

import locate
import parsers
import re_tools
import tools

class Parser(parsers.BaseParser):
    """
    todo(dan) review these general assumptions:
    * User data doesn't repeat the headers we're looking for. Headers are
      mixed-case, and data seems to be all upper-case, so this is probably ok.
    * Top line comes before 'Ticket :' line. And there aren't any other lines
      that match the re's line format.
    * A line starting with 'Header :', will always start with the same header.
    * Review each common re function for more assumptions.

    todo(dan) Need to include 'Co addr2' if it exists?
    todo(dan) Test update_of
    """

    # todo(dan) Verify ok to use just first one.
    # There can be more than 1 line containing work_state
    only_one = parsers.BaseParser.only_one[:]
    if 'work_state' in only_one:
        only_one.remove('work_state')

    rt  = re_tools
    f0  = rt.only_field_re
    f01 = rt.only_or_first_field_re
    f1  = rt.first_field_re
    f2  = rt.next_field_re
    f23 = rt.next_or_last_field_re
    f3  = rt.last_field_re

    re_transmit_date = rt.transmission_line_re(transmit_date_re=rt.DT24_GRP)
    re_ticket_type   = rt.transmission_line_re(ticket_type_re='(.*?)')

    re_ticket_number = f1('Ticket', 'Rev')
    re_revision      = f2('Ticket', 'Rev', 'Taken')
    re_call_date     = f2('Ticket', 'Taken', 'Channel', rt.DT12_GRP)
    re_channel       = f3('Ticket', 'Channel')

    re_update_of = f1('Old Tkt', 'Taken')

    re_work_state  = f1('State', 'Cnty', rt.STATE_GRP)
    re_work_county = f2('State', 'Cnty', 'Place')
    re_work_city   = f3('State', 'Place')

    re_work_subdivision = f0('Subdivsn')

    re_work_address_number = f1('Address', 'Street')
    re_work_address_street = f3('Address', 'Street')

    re_work_cross = f1('Cross 1', 'Intersection')
    # todo(dan) How do we handle cross 2? #9 has cross 2.

    re_work_type = f0('WorkType')

    re_company = f0('Done for')

    re_explosives = f3('Whitelined', 'Blasting')

    re_work_date = f1('Work date', 'Meet', rt.DT12_GRP)

    re_respond_date = f3('Start by', 'Response Due', rt.DT12_GRP)
    # todo(dan) Need to check for duplicate respond_date's?

    re_caller       =  f1('Caller', 'Phone')
    re_caller_phone = f23('Caller', 'Phone', 'Ext')
    re_caller_contact = re_caller

    re_con_name = f1('Company', 'Type')
    re_con_type = f3('Company', 'Type')

    re_con_address = f0('Co addr')
    # todo(dan) Add 'Co addr2'?

    re_con_city  = f1('City', 'St')
    re_con_state = f2('City', 'St', 'Zip', rt.STATE_GRP)
    re_con_zip   = f3('City', 'Zip')

    re_caller_cellular = f01('Alt Tel#', 'Ext')

    re_caller_fax = f01('Fax', 'Ext')

    re_caller_altcontact =  f1('Alt cont', 'Phone')
    re_caller_altphone   = f23('Alt cont', 'Phone', 'Ext')

    re_caller_email = f0('Email')

    f_respond_date = f_work_date = f_call_date = f_transmit_date = rt.isodate_from_match

    def find_locates(self):
        locates = []
        m = re_tools.transmission_line_re(client_code_re='(\S*)').findall(self.data)
        if m:
            locates.append(locate.Locate(m[0]))
        # todo(dan) Check for >1 transmission line? It should already be found with other fields
        # todo(dan) When do duplicates get removed? Need to sort?

        # todo(dan) Could there be a 2nd UQ locate in Members, and would it be a problem?
        m = re_tools.multiline_term_codes_field_re('Members').findall(self.data)
        if m:
            # todo(dan) Check for >1 match
            codes = re_tools.term_codes_from_multiline_term_codes_field(m[0])
            for code in codes:
                locates.append(locate.Locate(code))
        return locates

    def find_work_description(self):
        # todo(dan) Replace with an re_tools function?
        m = re_tools.multiline_field_inline_header_re('Where').findall(self.data)
        if m:
            return re_tools.multiline_field_inline_header_string_value(m[0])
        return ''

    work_lat = None
    work_long = None

    # todo(dan) move this to re_tools?
    def work_lat_long(self):
        m = re_tools.multiline_field_inline_header_re('Best Fit').findall(self.data)
        if m:
            try:
                return re_tools.lat_long_from_polygon_coordinates(m[0])
            except:
                pass # todo(dan) Need to log, instead of just eating the exception?
        return 0.0, 0.0

    def find_work_lat(self):
        if self.work_lat is None:
            self.work_lat, self.work_long = self.work_lat_long()
        return self.work_lat

    def find_work_long(self):
        if self.work_long is None:
            self.work_lat, self.work_long = self.work_lat_long()
        return self.work_long

    def find_work_remarks(self):
        # todo(dan) Replace with an re_tools function?
        m = re_tools.multiline_field_inline_header_re('Comments').findall(self.data)
        if m:
            return re_tools.multiline_field_inline_header_string_value(m[0])
        return ''

    def post_process(self):
        if self.ticket.work_lat > 0 and self.ticket.work_long < 0:
            self.ticket.map_page = tools.decimal2grid(self.ticket.work_lat,
                                                      self.ticket.work_long)
        parsers.BaseParser.post_process(self)
