# WV1181.py

import re
#
from tools import R
import Arkansas
import DIGGTESS
import locate
import tools

class Parser(DIGGTESS.Parser):
    # differs from DIGGTESS in various subtle ways. :(

    re_work_date = R("Work Date:\s*([0-9/]+)\s+([0-9:]+)\s+(AM|PM)")
    re_operator = R("By:[ \t]+(.*?)\s*(Source|$)")
    re_channel = R("Source:[ \t]+(\S+)")
    re_caller = R("Caller:[ \t]*(.*?)\s*(Caller Phone|$)")
    re_caller_contact = R("Contact:[ \t]*(.*?)\s*(Contact Phone|$)")
    re_caller_email = R("^Company Email:\s*(\S+)")
    re_caller_fax = R("Company Fax:\s+(.*?)\s*$")
    re_work_county = R("^County:\s*(\S.*?)\s*Work Done For:")
    re_work_city = R("^Place:\s*(.*?)\s*$")
    re_explosives = R("Explosives:\s*(\S+)")

    def _parse_date(self, match):
        dpart, tpart, ampm = match.group(1), match.group(2), match.group(3)
        m, d, y = map(int, dpart.split('/'))
        h, mn, s = map(int, tpart.split(':'))
        if ampm.upper() == 'PM' and h < 12:
            h += 12
        return "%04d-%02d-%02d %02d:%02d:%02d" % (y, m, d, h, mn, s)

    f_call_date = f_transmit_date = f_work_date = _parse_date

    def find_locates(self):
        lines = self.data.split('\n')
        idx = tools.findfirst(lines, lambda s: s.startswith('Code'))
        if idx > -1:
            locates = []
            idx += 2
            while True:
                line = lines[idx]
                if line.strip():
                    locates.append(line.split()[0])
                    idx += 1
                else:
                    break
        return [locate.Locate(z) for z in locates]

    def post_process(self):
        lines = self.data.split('\n')
        idx = tools.findfirst(lines, lambda s: s.startswith("Company Informat"))
        if idx > -1:
            self.ticket.con_address = lines[idx+3].strip()

        DIGGTESS.Parser.post_process(self)

class SummaryParser(Arkansas.SummaryParser):

    def has_header(self, data):
        return "From Miss Utility of West Virginia" in data \
           and "Audit For" in data


