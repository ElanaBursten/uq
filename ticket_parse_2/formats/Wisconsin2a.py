# Wisconsin2a.py

import Wisconsin1
import Wisconsin2

class Parser(Wisconsin1.Parser):
    """ Just the same format as Wisconsin1 really, but for FWI2 """
    pass

class SummaryParser(Wisconsin2.SummaryParser):
    pass
