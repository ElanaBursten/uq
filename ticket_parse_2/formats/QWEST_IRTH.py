# QWEST_IRTH.py

from parsers import BaseParser
from re_shortcut import R
import tools
import string
import QWEST_IDL

class Parser(BaseParser):
    re_ticket_number = R("^Comment: +(\d+)")
    re_transmit_date = R("IRTH\d+\s+\d+\s+([0-9/]+) ([0-9:]+)")
    re_call_date = R("^Taken Date Time: ([0-9/]+) ([0-9:])")
    re_ticket_type = R("^Notice Type: (.*?) *$")
    re_caller = R("^Agent ID: (.*?) *$")
    re_caller_contact = R("^Contact: (.*?) *$")
    re_caller_phone = R("^Phone: (.*?) *$")
    re_caller_fax = R("^Fax: (.*?) *$")
    re_con_name = R("^Company: (.*?) *$")
    re_con_address = R("^Address: (.*?) *$")
    re_con_city = R("^Company City/State: (.*?) *$")
    re_con_zip = R("^Company Zip: (.*?) *$")
    re_company = R("^Work Done For: (.*?) *$")
    re_work_type = R("^Work Type: (.*?) *$")
    re_work_date = R("^Work Start Date Time: ([0-9/]+) ([0-9:]+)")
    re_work_state = R("^State: +(\S+)")
    re_work_county = R("^County: (.*?) *$")
    re_work_city = R("^Place: (.*?) *$")
    re_work_address_street = R("^Address1: (.*?) *$")
    re_work_cross = R("^Cross Street: (.*?) *$")
    re_work_remarks = R("^Comment: (.*?) *$")

    def f_transmit_date(self, match):
        return tools.isodate(string.join(match.groups(), " "))
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date

    def find_locates(self):
        raise NotImplementedError

class SummaryParser(QWEST_IDL.SummaryParser):
    pass
