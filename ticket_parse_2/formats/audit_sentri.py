# audit_sentri.py

import string
#
import summarydetail
import summaryparsers
from tools import R
import magicdate

class SummaryParser(summaryparsers.BaseSummaryParser):

    re_header = R("\*(?:SUM|EOD)\*\s+SENTRi\s+(.*?)\s*$")
    re_footer = R("TOTAL\s*-\s*(\d+)")
    DUMMY_CLIENT_CODE = "AGL"

    def has_header(self, data):
        return bool(self.re_header.search(data))

    def has_footer(self, data):
        return bool(self.re_footer.search(data))

    def get_client_code(self, data):
        return self.DUMMY_CLIENT_CODE

    def get_summary_date(self, data):
        # of the form "Tuesday, June 29, 2010"
        m = self.re_header.search(data)
        if m:
            date_raw = m.group(1)
            date_raw = string.join(date_raw.split()[1:], " ") # strip day name
            d = magicdate.magicdate(date_raw)
            if d:
                return "%s 00:00:00" % d
            else:
                raise ValueError("Could not extract audit date from '%s'" %
                      m.group(1))
        else:
            raise ValueError("Could not extract audit date")

    def get_expected_tickets(self, data):
        m = self.re_footer.search(data)
        if m:
            return int(m.group(1))
        else:
            raise ValueError("Could not extract expected number of tickets")

    def read(self, data):
        tickets = []
        collecting = False
        re_line = R("^\d{5}\s+\d+")

        for line in data.split('\n'):
            line = line.lstrip()
            m = re_line.search(line)
            if not m:
                continue # does not contain ticket info
            while line.strip():
                # some lines are slightly mis-formatted, they don't have the
                # timestamp; we can work around this in most cases by splitting
                # on the first occurrence of three spaces
                idx = line.find("   ")
                if idx > -1:
                    chunk, line = line[:idx+3], line[idx+3:].lstrip()
                else:
                    chunk, line = line, ""
                #print ">>", `chunk`, `line`
                #chunk, line = line[:33], line[33:]

                chunk = chunk.lstrip()
                if not re_line.search(chunk):
                    break # go to next line
                parts = chunk.split() + ["", "", "", ""]
                seqno, ticketno, time, y = parts[:4]
                if not time:
                    time = "00:00"

                # split ticket number and revision
                tp = ticketno.split('-')
                revision = tp[-1]
                ticketno = string.join(tp[:-1], "-")

                sd = summarydetail.SummaryDetail([seqno, ticketno, time,
                     revision, y])
                tickets.append(sd)

        return tickets


