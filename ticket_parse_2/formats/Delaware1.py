# Delaware1.py

# 2011-05-25 hn Updated call date, work date (Mantis #2806)

import re
import string
#
from parsers import BaseParser
from re_shortcut import R
import Lanham
import re_tools2
import tools

from re_tools2 import header_re as hdr
from re_tools2 import standard_line_re as line_re
from re_tools2 import MULTILINE, START, REST_OF_LINE, REST_OF_LINE_CAPTURED,\
  DATE_TIME_12_SPLIT_CAPTURED

class Parser(Lanham.Parser):
    # WashingtonParser and BaltimoreParser derive from this

    MAX_LOCATES = 55 # Changed so the test ticket will work, Any ramifications?

    _re_work_lat = R("Lat:\s+(\d+\.\d+)")
    _re_work_long = R("Lon:\s*(-\d+\.\d+)")
    _re_map_page = R("^(?:Caller +Provided: +|Ticket Mapping +: +)Map Name: +(\S+) +Map#: +(\S+) +Grid Cells: +(\S+?)(,|\s|$)", re.M)
    _re_map_page_old = R("^(?:Caller +Provided: +)?Map: +(\S+) +Page: +(\S+) +Grid Cells: +(\S+?)(,|\s|$)", re.M)

    re_transmit_date = R("^Transmit +Date: +([0-9/]+) +Time: +([0-9:APM ]+) +")
    re_call_date = R("^(?:Original Call|Release)\s+Date: +([0-9/]+) +Time: +([0-9:APM ]+)")
    re_work_date = R("^(?:Work to Begin Date|Response Due By): +([0-9/]+) +Time: +([0-9:APM ]+)")
    # Changes below for updated format for MD.
    re_caller = R("^Contact Name *: +(.*?) +(Con|Fax)")
    re_caller_contact = re_caller
    re_caller_altcontact = R("^Job Site Contact: *(.*?) +Job")
    re_work_city = R("^Place      : +(.*)$")
    re_con_state = R("^State      : ([A-Z][A-Z])")
    re_work_state = re_con_state
    re_revision = R("^Ticket No:.*Update No: (\S+)")



    #re_caller_fax = R("Fax\s+: +(.*)$")

    _new_re_work_subdivision = line_re('', hdr('Subdivision'), '')
    _new_re_caller_altphone = line_re(hdr('Job Site Contact'),
                                      hdr('Job Site Phone'), [hdr('Ext'), ''])
    _new_re_respond_date = line_re('', hdr('Response Due By'), '',
                                   value=DATE_TIME_12_SPLIT_CAPTURED)

    f_respond_date = re_tools2.isodate_from_3_groups

    def pre_process(self):
        Lanham.Parser.pre_process(self)

        if string.lower(self.parser_format) in ['delaware1', 'delaware3',
                                                'baltimore', 'baltimore4']:
            self.re_work_subdivision = self._new_re_work_subdivision
            self.re_caller_altphone = self._new_re_caller_altphone
            self.re_respond_date = self._new_re_respond_date

    _re_caller_address_line1 = line_re('', hdr('Caller Address'), '')

    def set_con_address(self):
        match = self._re_caller_address_line1.search(self.data)
        if match:
            self.ticket.con_address = match.group(1)

    _re_caller_address_line2 = re.compile(
      MULTILINE + START + hdr('Caller Address') + REST_OF_LINE + r'\n' +
      REST_OF_LINE_CAPTURED)

    def set_con_city_state_zip(self):
        match = self._re_caller_address_line2.search(self.data)
        if match:
            city, state, zip = re_tools2.city_state_zip_from_line(
              match.group(1))
            if city:
                self.ticket.con_city = city
            if state:
                self.ticket.con_state = state
            if zip:
                self.ticket.con_zip = zip

    def post_process(self):
        if string.lower(self.parser_format) in ['delaware1', 'delaware3',
                                                'baltimore', 'baltimore4']:
            self.set_con_address()
            self.set_con_city_state_zip()

        # get rid of any tabs in text fields
        for key, val in self.ticket._data.iteritems():
            if val is not None and key != 'image' and hasattr(val,'find'):
                self.ticket._data[key] = self.ticket._data[key].replace('\t','')

        if not self.ticket.map_page and self.ticket.work_lat > 0 and \
         self.ticket.work_long < 0:
            self.ticket.map_page = tools.decimal2grid(self.ticket.work_lat, 
             self.ticket.work_long)

        BaseParser.post_process(self)

    def find_work_lat(self):
        lat = 0
        lats = self._re_work_lat.findall(self.data)
        if lats:
            try:
                lats = map(float, lats)
                lat = sum(lats) / len(lats) #Average NW-SE
            except:
                lat = 0
        return lat

    def find_work_long(self):
        longitude = 0
        longitudes = self._re_work_long.findall(self.data)
        if longitudes:
            try:
                longitudes = map(float, longitudes)
                longitude = sum(longitudes) / len(longitudes) #Average NW-SE
            except:
                longitude = 0
        return longitude

    def find_map_page(self):
        map_page = ""
        m = self._re_map_page_old.search(self.data)
        if m:
            return string.join([m.group(1), m.group(2), m.group(3)])
        m = self._re_map_page.search(self.data)
        if m:
            return string.join([m.group(1), m.group(2), m.group(3)])
        return map_page

class SummaryParser(Lanham.SummaryParser):
    prepend_zeroes = 0

