# Alabama.py

from re_shortcut import R
import Richmond2
import locate
import string
import tools
import Atlanta
from date import Date

class Parser(Richmond2.Parser):

    re_transmit_date = R("[A-Z0-9]+ +\d+ +AOC ([0-9/]+) ([0-9:]+)")
    re_ticket_type = R("[A-Z0-9]+ +\d+ +AOC [0-9/]+ [0-9:]+ \S+ *(.*?) *$")
    re_ticket_number = R("^Ticket : +(\d+) +Rev")
    re_call_date = R("^Ticket.*Date: ([0-9/]+) Time: ([0-9:]+)")
    re_work_date = R("^Work date +: +([0-9/]+) Time: ([0-9:]+)")

    re_revision = R("Rev: (\S+)")
    re_work_city = R("^State.*Place: (.*?) *(In|$)")
    re_work_address_street = R("^Addr.*Street: (.*?) *(CPG:|$)")
    re_work_cross = R("^Xst +: (.*?) *(Int:|$)")

    re_company = R("^Done for: (.*?) *$")
    re_caller_email = R("^Email: (.*?) *$")
    re_caller_fax = R("^Fax: (.*?) *$")

    def find_work_description(self):
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Locat:"))
        if idx1 != -1:
            spam = [lines[idx1][len("Locat:"):]]
            for idx in range(idx1+1, len(lines)):
                if lines[idx].strip() == ":":
                    break
                spam.append(lines[idx])
            return string.join(spam, "").strip()
        else:
            return ""

    def find_work_remarks(self):
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Remarks :"))
        if idx1 > -1:
            spam = [lines[idx1][len("Remarks :"):]]
            for idx in range(idx1+1, len(lines)):
                if lines[idx].strip() == ":":
                    break
                spam.append(lines[idx])
            return string.join(spam, "").strip()
        else:
            return ""

    def find_locates(self):
        locates = []
        lines = self.data.split("\n")
        for line in lines:
            if line.startswith("Mbrs :"):
                names = line[len("Mbrs :"):].split()
                locates.extend(names)
        return [locate.Locate(n) for n in locates]

class SummaryParser(Atlanta.SummaryParser):
    LENGTH = len("00001 0268656-000 07:56 R ")
    RECOG_HEADER_2 = "NOW ENDING TRANSMISSION FOR TODAY"

    re_footer = R("^Total: (\d+) *$")
    re_recog_header_1 = R("\s+\*(SUM|EOD)\* AOC")

    '''
    def get_summary_date(self, data):
        ds = Atlanta.SummaryParser.get_summary_date(self, data)
        d = Date(ds)
        #d.inc()
        return d.isodate()
    '''

