# QWEST_WOC.py

from re_shortcut import R
import Oregon2
import QWEST_IDL

class Parser(Oregon2.Parser):
    re_serial_number = R("(WOC\d+)(-\d*)?\s+(\d+|Internals)")

class SummaryParser(QWEST_IDL.SummaryParser):
    pass
