# Kentucky.py

from parsers import BaseParser
from re_shortcut import R
from summaryparsers import BaseSummaryParser
import summarydetail
import re
import tools
import locate
import string

class Parser(BaseParser):

    re_transmit_date = R("KENTUCKY URG PRO INC - KY. +([0-9/]+) +([0-9:]+)")
    re_call_date = R("^TIME\.+([0-9:]+) +DATE\.+([0-9/]+)")
    re_ticket_type = R("(?:-----|ENTRY \*\*\*)\n\n(.*?)\n\nTIME", re.DOTALL)

    re_ticket_number = R("^REQUEST NO\.+(\S+)")
    re_work_county = R("^COUNTY\.+(.*?) *$")
    re_work_city = R("^CITY\.+(.*?) *(\(|$)")
    re_work_address_street = R("^STREET\.+(.*?) *$")
    re_work_type = R("^TYPE OF WORK\.+(.*?) *$")

    re_work_date = R("^START DATE\.+([0-9/]+) +START TIME\.+([0-9:]+)")

    re_caller = R("^CALLER\.+(.*?) *$")
    re_caller_phone = R("^PHONE #\.+(.*?) *$")
    re_caller_altphone = R("^ALT\. PHONE #\.+(.*?) *$")
    re_caller_email = R("^EMAIL ADDRESS\.+(.*?) *$")
    re_caller_fax = R("^FAX #\.+(.*?) *$")
    re_con_name = R("^CONTRACTOR\.+(.*?) *$")
    re_company = re_con_name    # ?
    re_con_address = R("^ADDRESS\.+(.*?) *$")
    re_con_city = R("ADDRESS.*CITY\.+(.*?) *$", re.DOTALL)
    re_con_state = R("^STATE\.+(\S+)")
    re_con_zip = R("^ZIP\.+(\S+)")

    f_transmit_date = lambda s, m: s.isodate(string.join(m.groups(), " "))
    f_call_date = lambda s, m: s.isodate(m.group(2) + " " + m.group(1))
    f_work_date = f_transmit_date

    _re_termid = R("\d+-\d+([A-Z][A-Z0-9]+)")

    def find_work_state(self):
        return "KY" # can we assume this?

    def find_locates(self):
        locates = []
        lines = self.data.split("\n")
        idx = tools.findfirst(lines,
              lambda s: s.find("KENTUCKY URG PRO INC") > -1)
        idx2 = tools.findfirst(lines, lambda s: s.startswith("---------"))
        if idx > -1 and idx2 > -1 and idx2 > idx:
            for i in range(idx+1, idx2):
                line = lines[i]
                parts = line.split()
                for part in parts:
                    m = self._re_termid.search(part)
                    if m:
                        termid = m.group(1)
                        locates.append(locate.Locate(termid))
                        break

        return locates

    def find_kind(self):
        is_emergency = self.ticket.ticket_type.find("EMERGENCY") > -1
        return ["NORMAL", "EMERGENCY"][is_emergency]

class SummaryParser(BaseSummaryParser):
    re_total = R("^TOTAL = (\d+)")
    re_station = R("STATION: (\S+)")

    def has_header(self, data):
        return data.find("FOLLOWING MESSAGE NUMBERS WERE TRANSMITTED") > -1

    def has_footer(self, data):
        return data.find("TOTAL =") > -1

    def get_summary_date(self, data):
        lines = string.split(data, "\n")
        idx = tools.findfirst(lines, lambda s: s.find("FOLLOWING MESSAGE") > -1)
        if idx > -1:
            parts = string.split(lines[idx+1])
            return tools.isodate(parts[0] + " " + parts[1])
        else:
            return ""

    def get_client_code(self, data):
        # we don't really have a client code, but we'll use the station code
        # instead
        m = self.re_station.search(data)
        if m:
            return m.group(1)
        return ""

    def get_expected_tickets(self, data):
        m = self.re_total.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    def read(self, data):
        tickets = []
        lines = string.split(data, "\n")
        idx = tools.findfirst(lines, lambda s: s.find("FOLLOWING MESSAGE") > -1)
        if idx > -1:
            for i in range(idx+2, len(lines)):
                line = lines[i]
                if not line.strip():
                    break   # stop at empty line
                parts = string.split(line)
                for part in parts:
                    # not much here -- just the ticket number
                    t = ["", part, "", "", ""]
                    detail = summarydetail.SummaryDetail(t)
                    tickets.append(detail)
        return tickets

