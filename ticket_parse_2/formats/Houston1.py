# Houston1.py

from re_shortcut import R
import Lanham
import string
import locate
import tools
import centerpoint

class Parser(Lanham.Parser):

    re_transmit_date = R(
     "^Transmit +Date: +([0-9/]+) +Time: +([0-9:]+)(AM|PM) +")
    re_call_date = R("^Original Call Date: +([0-9/]+) +Time: +([0-9:]+)(AM|PM)")
    re_work_date = R("^Work to Begin Date: +([0-9/]+) +Time: +([0-9:]+)(AM|PM)")

    f_transmit_date = lambda s, m: \
     tools.isodate(m.group(1) + " " + m.group(2) + " " + m.group(3))
    f_call_date = f_work_date = f_transmit_date

    def find_locates(self):
        # get the lines we want, using an unorthodox filtering mechanism :-)
        lines = self.data.split("\n")
        while lines and not lines[0].startswith("Send To:"):
            del lines[0]
        lines = [li for li in lines
                 if li.startswith("Send To:") or li.startswith(" ")]

        locates = []
        for line in lines:
            parts = line.split()
            if parts[0] == "Send" and len(parts) >= 4:
                locname = parts[2]
                locates.append(locname)
            elif len(parts) >= 2:
                locname = parts[0]
                locates.append(locname)

        # since we get locates from two places now, chances are there are
        # duplicates... weed them out
        locates = tools.remove_duplicates(locates)

        # special check for Centerpoint tickets
        idx1 = self.data.find('Explosives:')
        idx2 = self.data.find('Send To:')
        if idx1 > -1 and idx2 > -1:
            scandata = self.data[idx1:idx2]
            chunks = scandata.split()
            if 1 or ('RELIAN01' in locates):
                power = gas = 0
                for chunk in chunks:
                    # if any of these appear in this part of the ticket, CP-P
                    # or CP-G may be added (or both)
                    if chunk in centerpoint.CP_P_CODES:
                        power = 1
                    elif chunk in centerpoint.CP_G_CODES:
                        gas = 1
                if power:
                    locates.append('CP-P')
                if gas:
                    locates.append('CP-G')

        return map(locate.Locate, locates)

class SummaryParser(Lanham.SummaryParser):
    pass
