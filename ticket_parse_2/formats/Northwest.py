# Northwest.py

from parsers import BaseParser
from re_shortcut import R
from summaryparsers import BaseSummaryParser
from re_shortcut import R
import summarydetail
import re
import locate
import date
import magicdate

class Parser(BaseParser):
    only_one = [] # don't care about supposedly "duplicate" fields

    re_ticket_number = R("^Ticket No: (.*?) *$")
    re_ticket_type = R("^Notification Type: (.*?) *$")
    re_map_page = R("^Twp: (.*?) *Rng: (.*?) *Sect-Qtr: (.*?) *$")
    re_work_date = R("^Work Begin Date: ([0-9/]+).*Work Begin Time: ([0-9:]+)", re.DOTALL)
    re_call_date = R("^Original Call Date: ([0-9/]+) ([0-9:]+)")
    re_transmit_date = R("^Transmit Date: ([0-9/]+) ([0-9:]+)")
    re_work_state = R("^State: (\S+)")
    re_work_county = R("^County: (.*?) *$")
    re_work_city = R("^Place: (.*?) *$")
    re_work_address_number = R("^Address: (.*?) *$")
    re_work_address_street = R("^Street: (.*?) *$")
    re_work_cross = R("^Nearest Intersecting Street: (.*?) *$")
    re_work_type = R("^Type of Work: (.*?) *$")
    re_work_description = R("^Location of Work: (.*?) *$")
    re_work_remarks = R("^Remarks: (.*?) *$")
    re_con_name = R("^Company: (.*?) *$")
    re_caller_contact = R("^Contact Name: (.*?) *$")
    re_caller_phone = R("^Phone: (.*?) *$")
    re_company = R("^Work Being Done For: (.*?) *$")

    def f_work_date(self, match):
        d, t = match.group(1), match.group(2)
        m, d, y = d.split("/")
        return "%s-%s-%s %s" % (y, m, d, t)

    f_call_date = f_transmit_date = f_work_date

    _re_term_id = R("^Send To: (.*?) *$")

    def f_ticket_type(self, match):
        type = match.group(1)
        return type or "NORMAL"

    def f_map_page(self, match):
        z = match.group(1) + match.group(2) + match.group(3)
        z = z.replace('-', '')
        return z

    def find_locates(self):
        m = self._re_term_id.search(self.data)
        if m:
            return [locate.Locate(m.group(1))]
        else:
            return []

    def post_process(self):
        self.ticket.image = None # so it doesn't overwrite existing images
        BaseParser.post_process(self)


class SummaryParser(BaseSummaryParser):
    r_summary_date = R("^(\d\d)/(\d\d)/(\d\d\d\d) *$")
    r_total = R("^Total Tickets: (\d+)")

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        for line in lines:
            #if line.lstrip().startswith("|"):
            if "|" in line:
                parts = line.split("|")
                for part in parts:
                    part = part.strip() # remove empty strings
                    if part:
                        foo = ["", part, "00:00", "", ""]
                        t = summarydetail.SummaryDetail(foo)
                        tickets.append(t)

        return tickets

    def get_client_code(self, data):
        lines = data.split("\n")
        lines = [line for line in lines if line]
        # Client code may be on summary
        try:
            magicdate.magicdate(lines[0])
            # If a date, then old format
            return 'LNG'
        except:
            return lines[0].strip()

    def get_summary_date(self, data):
        m = self.r_summary_date.search(data)
        if m:
            s = "%s-%s-%s 00:00:00" % (m.group(3), m.group(1), m.group(2))
            return s
        else:
            return date.Date().isodate()    # take today, I guess

    def get_expected_tickets(self, data):
        m = self.r_total.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    def has_header(self, data):
        return bool(self.r_summary_date.search(data))

    def has_footer(self, data):
        return bool(self.r_total.search(data))
