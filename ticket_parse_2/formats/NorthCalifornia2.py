# NorthCalifornia2.py

import NorthCalifornia
from re_shortcut import R

class Parser(NorthCalifornia.Parser):
    SPECIAL_LOCATES = []
    re_caller_phone = R("^ *Business Tel: +(.*?) *Fax:")
    re_caller_email = R("^ *Email Address: +(\S+)")
    re_caller_fax = R("^ *Business Tel.*Fax: +(.*?) *$")
    re_company = R("^ *Done for: +(.*?) *Explosives:")

class SummaryParser(NorthCalifornia.SummaryParser):
    re_recog_header_1 = R("[A-Z0-9]+ +USA[NS] +[0-9/]+")
