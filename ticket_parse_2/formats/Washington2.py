# Washington2.py

import string
import Washington
from re_shortcut import R
import locate
import tools

from summaryparsers import BaseSummaryParser
import summarydetail
from date import Date

class Parser(Washington.Parser):
    re_service_area_code = R("MISUTIL\d+\s+(.*?)\s*\d\d\d\d/\d\d/\d\d")
    re_serial_number = R("(MISUTIL\d+)")

    # we're using a slightly different version to extract locates...
    def find_locates(self):
        # get the lines we want, using an unorthodox filtering mechanism :-)
        lines = string.split(self.data, "\n")
        while lines and not lines[0].startswith("Send To:"):
            del lines[0]
        lines = [li for li in lines
                 if li.startswith("Send To:") or li.startswith(" ")]

        locates = []
        for line in lines:
            parts = string.split(line)
            if parts[0] == "Send" and len(parts) >= 4:
                locname = parts[2]
                locates.append(locname)
            elif len(parts) >= 2:
                locname = parts[0]
                locates.append(locname)

        # but wait, there's more!
        lines = string.split(self.data, "\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Explosives:"))
        idx2 = tools.findfirst(lines, lambda s: s.startswith("Send To:"))
        if idx1 > -1 and idx2 > -1:
            for i in range(idx1+1, idx2):
                parts = string.split(lines[i])
                locates.extend(parts)

        # since we get locates from two places now, chances are there are
        # duplicates... weed them out
        locates = tools.remove_duplicates(locates)

        return map(locate.Locate, locates)

    def post_process(self):
        Washington.Parser.post_process(self)
        if self.ticket.map_page:
            self.ticket.grids = [self.ticket.map_page]

class SummaryParser(BaseSummaryParser):
    # replaces old Washington2SummaryParser, which was based on
    # WashingtonSummaryParser.

    re_header = R("AUDIT.*\d+/\d+/\d+ \d+:\d+:\d+\s+\d+")
    re_footer = R("Total number of notices sent during time period: (\d+)")
    re_ticket = R("(\*?)(\d+) (MISUTIL\d+|VUPS\d+) +(\d\d:\d\d)")
    re_date = R("for the period ([0-9/]+) ([0-9:]+) thru ([0-9/]+) ([0-9:]+)")
    DEFAULT_CLIENT_CODE = 'MISUTIL'

    def has_header(self, data):
        return not not self.re_header.search(data)

    def has_footer(self, data):
        return not not self.re_footer.search(data)

    def get_expected_tickets(self, data):
        m = self.re_footer.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    def get_summary_date(self, data):
        m = self.re_date.search(data)
        if m:
            d = Date(m.group(3) + " " + m.group(4))
            d.resettime()
            return d.isodate()
            # how well does this work for weekends? find out.
        else:
            d = Date()
            d.resettime()
            return d.isodate()

    def get_client_code(self, data):
        # there is no client code on this type of summaries, so we make one up
        return self.DEFAULT_CLIENT_CODE

    def read(self, data):
        tickets = []
        results = self.re_ticket.findall(data)
        for r in results:
            indicator, seqnr, ticket_number, time = r
            t = [seqnr, ticket_number, time, "", indicator]
            detail = summarydetail.SummaryDetail(data=t)
            tickets.append(detail)
        return tickets
