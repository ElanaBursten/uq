# NewJersey3.py

import date
import summarydetail
import summaryparsers
from re_shortcut import R
import NewJersey

class Parser(NewJersey.Parser):
    pass

class SummaryParser(summaryparsers.BaseSummaryParser):

    re_header = R('"Sequence","ID","Recipient"')
    re_line = R('^"(.*?)","(.*?)","(.*?)","(.*?)"')

    def get_summary_date(self, data):
        # there's no summary date on the audit, so we have to grok it from
        # the ticket dates
        dates = {}
        lines = data.split('\n')
        lines = [line for line in lines if line.strip()]
        lines = lines[1:] # skip header line

        for line in lines:
            m = self.re_line.search(line)
            if m:
                date_blurb = m.group(4).strip()
                date_parts = date_blurb.split()
                day = int(date_parts[2])
                year = int(date_parts[-1])
                month = date.get_month_number(date_parts[1])
                the_date = "%04d-%02d-%02d" % (year, month, day)

                dates[the_date] = dates.get(the_date, 0) + 1

        # which date is most common?
        a = [(t[1], t[0]) for t in dates.items()]
        a.sort()
        if a:
            the_date = a[-1][1]
            return date.Date(the_date).isodate()
        else:
            # not ideal, but we need to specify a date
            d = date.Date()
            d.resettime()
            return d.isodate()

    def has_header(self, data):
        m = self.re_header.search(data)
        return bool(m)

    def has_footer(self, data):
        # there is no footer; assume it's always there anyway
        return True

    def get_client_code(self, data):
        # there is no client code on the ticket; use a dummy value
        return "NJ3"

    def get_expected_tickets(self, data):
        # there's no "total" indicator on the ticket; count lines instead
        lines = data.split('\n')
        lines = [line for line in lines if line.strip()]
        return len(lines) - 1

    def read(self, data):
        tickets = []
        lines = data.split('\n')
        lines = [line for line in lines if line.strip()]
        lines = lines[1:] # skip header line

        for line in lines:
            m = self.re_line.search(line)
            if m:
                seq_number = m.group(1)
                ticket_number = m.group(2)
                date_blurb = m.group(4).strip()
                date_parts = date_blurb.split()
                time = date_parts[3][:5]
                detail = summarydetail.SummaryDetail(
                         [seq_number, ticket_number, time, "", ""])
                tickets.append(detail)

        return tickets

