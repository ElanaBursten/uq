import string
import re
import decimal
import date
from parsers import BaseParser
from re_shortcut import R
import tools
import locate
import Lanham

def extract_lat_long_1(latlong_re_1,latlong_re_2,line):
    m1 = latlong_re_1.search(line)
    if m1:
        long1 = decimal.Decimal(m1.group(1).strip()).quantize(decimal.Decimal("0.00001"))
        m2 = latlong_re_2.search(line)
        if m2:
            long2 = decimal.Decimal(m2.group(1).strip()).quantize(decimal.Decimal("0.00001"))
            avg_long = (long1 + long2) / 2
            return float(str(avg_long))
        else:
            return float(str(long1))
    raise ValueError

def extract_lat_long_2(latlong_re_1,latlong_re_2,line):
    m1 = latlong_re_1.search(line)
    if m1:
        parts = string.split(m1.group(1)) + [0, 0, 0]
        deg1, min1, sec1 = map(tools.safeint, parts[:3])
        m2 = latlong_re_2.search(line)
        if m2:
            parts = string.split(m2.group(1)) + [0, 0, 0]
            deg2, min2, sec2 = map(tools.safeint, parts[:3])
            if deg1 < 0: # Longitude
                factor = -1.0
            else:
                factor = 1.0
            latlong1 = deg1 + factor*min1 / 60.0 + factor*sec1 / (60.0*60.0)
            latlong2 = deg2 + factor*min2 / 60.0 + factor*sec2 / (60.0*60.0)
            avg_latlong = (latlong1 + latlong2) / 2
            return avg_latlong
    return 0


class Parser(BaseParser):
    """ Parser for Baton Rouge tickets. """

    MAX_LOCATES = 100

    re_ticket_number = R('^Ticket No: +([0-9]+)')
    re_call_date = R("^Original Call Date: +(.*?) +Time: +(.*?) +Op")
    re_operator = R("^Original Call Date.*Op: *(.*)\\.0$")  # strip .0

    re_con_name = R("Company +: *(.*)$")
    re_caller_contact = R("Contact Name: (.*?) +Contact")
    re_caller_phone = R("Contact Phone: *(.*)$")
    re_caller_altcontact = R("^Alt. Contact: (.*?) +Alt")
    re_caller_altphone = R("Alt. Phone +: *(.*)$")

    re_work_type = R("Type of Work : *(.*)$")
    re_company = R("Work Done For: *(.*)$")

    re_work_state = R("^State: (..)")
    re_work_county = R("^State.*Parish: *(.*)$")
    re_work_city = R("^Place: *(.*)$")
    re_work_address_number = R("^Address: *(.*?) *Street")
    re_work_address_street = R("^Address.*Street: *(.*)$")
    re_work_cross = R("^Nearest Intersecting Street: *(.*)$")

    re_work_date = R("^Work to Begin Date: +([0-9/]*) +Time: (.*)$")
    re_legal_due_date = R("^Mark By +Date: +([0-9/]*) +Time: (.*)$")
    re_due_date = re_legal_due_date
    # was: re_mark_by_date
    re_transmit_date = R("^Transmit +Date: +([0-9/]*) +Time: (.*) +Op")

    def f_company(self, match):
        s = match.group(1)
        return s.strip()

    def f_call_date(self, match):
        if not match.group(1):
            # Let business logic figure it out
            return ''
        s = match.group(1) + " " + match.group(2)
        s = s.strip()
        if s:
            s = tools.isodate(s)
        return s

    f_work_date = f_legal_due_date = f_due_date = f_transmit_date = f_call_date

    def find_kind(self):
        lines = string.split(self.data, "\n")
        lines = filter(string.strip, lines) # remove empty lines
        for i in range(len(lines)):
            line = lines[i]
            if line.startswith("NOTICE OF INTENT"):
                self.ticket.ticket_type = lines[i+1][:40].strip()
                #if lines[i+1].startswith("EMER"):
                if lines[i+1].find("EMER") > -1 \
                 or lines[i+1].find("RUSH") > -1:
                    return "EMERGENCY"
                else:
                    return "NORMAL"

    def find_work_remarks(self):
        lines = string.split(self.data, "\n")
        remarks = ""
        for line in lines:
            if line.startswith("Remarks:"):
                remarks = string.strip(line[len("Remarks:"):])
                continue
            if remarks and line:
                if line.startswith("Work to Begin Date"):
                    break
                else:
                    remarks = remarks + " " + string.strip(line)
        return remarks

    def find_work_description(self):
        # Not the same as find_work_remarks; more like the multiline fields
        # in AtlantaParser; XXX write generic routines for this!
        lines = string.split(self.data, "\n")
        location = ""
        for line in lines:
            if line.startswith("Location:"):
                location = string.strip(line[len("Location:"):])
                continue
            if location:
                if line.startswith(":"):
                    location = location + " " + string.strip(line[1:])
                else:
                    break
        return location

    def find_locates(self):
        # A bit trickier than AtlantaParser, because these members can be
        # followed by commas, which must be stripped. Also, we take both the
        # "Additional members" line(s) and the "Send To" section.
        lines = string.split(self.data, "\n")
        locates = []

        STR1 = "Additional Members:"
        idx = tools.findfirst(lines, lambda s: s.startswith(STR1))
        if idx:
            # get locates from this line
            locs = lines[idx][len(STR1):].strip()
            locs = string.split(locs, ",")
            for loc in filter(string.strip, locs):
                locates.append(loc.strip())
            # get the following lines, if any
            for i in range(idx+1, len(lines)):
                if not lines[i].strip():
                    break   # empty line stops it
                locs = lines[i].strip()
                locs = string.split(locs, ",")
                for loc in filter(string.strip, locs):
                    locates.append(loc.strip())

        for line in lines[idx+1:]:
            if line.startswith("Send To:"):
                parts = line.split()
                locates.append(parts[2])

        try:
            idx = locates.index("CLW02")
        except ValueError:
            pass    # CLW02 not in list
        else:
            del locates[idx]
            locates.extend(["CLG02", "CLS02", "CLW02"])

        # since we get this from two sources, we must remove any duplicates
        locates = tools.remove_duplicates(locates)

        return map(locate.Locate, locates)

    #
    # lat/long parsing

    _re_coord = R("^Ex\. Coord:(.*)$", re.MULTILINE)
    _re_long1 = R("NW.*?Lon: *([-0-9. ]+) *SE")
    _re_long2 = R("SE Lat.*?Lon: *([-0-9. ]+)")
    _re_lat1 = R("NW Lat: *([0-9. ]+)Lon:")
    _re_lat2 = R("SE Lat: *([0-9. ]+)Lon:")

    def _find_longlat(self):
        m = self._re_coord.search(self.data)
        if m:
            line = m.group(1).strip()
            return line
        return ""

    def find_work_long(self):
        line = self._find_longlat()
        if line:
            try:
                # Decimal degrees
                return extract_lat_long_1(self._re_long1,self._re_long2,line)
            except:
                # d/m/s
                return extract_lat_long_2(self._re_long1,self._re_long2,line)
        return 0

    def find_work_lat(self):
        line = self._find_longlat()
        if line:
            try:
                # Decimal degrees
                return extract_lat_long_1(self._re_lat1,self._re_lat2,line)
            except:
                # d/m/s
                return extract_lat_long_2(self._re_lat1,self._re_lat2,line)
        return 0

class SummaryParser(Lanham.SummaryParser):
    # Almost exactly the same as the Lanham summary format...
    def read(self, data):
        # Baton Rouge ticket number don't have leading zeroes
        tickets = Lanham.SummaryParser.read(self, data)
        for ticket in tickets:
            ticket_number = ticket[1]
            while ticket_number.startswith("0"):
                ticket_number = ticket_number[1:]
            ticket[1] = ticket_number
        return tickets
