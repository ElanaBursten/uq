# SouthCalifornia11.py

import korterra_summary_parser
import SouthCalifornia

class Parser(SouthCalifornia.Parser):
    pass

class SummaryParser(korterra_summary_parser.BaseKorTerraSummaryParser):
    def get_client_code(self, data):
        return 'SCA11'

    def ticket_number_from_jobid(self, jobid):
        if jobid.startswith('CS'):
            jobid = jobid[2:]
        if jobid.endswith('S'):
            jobid = jobid[:-1]
        return jobid
