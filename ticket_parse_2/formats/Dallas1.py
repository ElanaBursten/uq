# Dallas1.py

from parsers import BaseParser, ParserError
from re_shortcut import R
import date
import tools
import string
import locate
import NewJersey
import summarydetail
from static_tables import ticket

class Parser(BaseParser):
    call_center = 'Dallas1'

    re_ticket_number = R("^Locate Request No. (\S+)")
    re_call_date = R("^Prepared By.*On (\d\d-\S\S\S-\d\d)\s+At (\d+)")
    # Limit the width of map_page based on the width of the field
    # as defined by the schema
    for field, type in ticket:
        if field == 'map_page':
            max_length = type.width
    re_map_page = R("^Map Cross Reference : (.{,%d}?) *(,DALLAS|$)" % (max_length,))
    re_work_county = R("County: (.*?) *Town:")
    re_work_city = R("Town: (.*?) *$")
    re_ticket_type = R("^\* *(.*?) *\* MESSAGES Sent to")
    re_work_address_street = R("Address: (.*?) *$")
    re_work_date = R(
     "^Beginning Work Date ([0-9/]+) +Time of Day: ([0-9:]+) (AM|PM|am|pm)")
    re_work_type = R("^Nature of Work *: (.*?) *$")
    re_caller = R("^Person Calling : (.*?) *$")
    re_con_name = R("Company Name *: (.*?) *$")
    re_company = R("Work by.*For (.*?) *$")
    re_caller_contact = R("^Person to Contact : (.*?) *$")
    re_caller_phone = R("^Phone No. *(.*?) (OFFICE|/)")
    re_caller_fax = R("^Fax No. *(.*?) *$")
    re_caller_email = R("^Email: *(.*?) *$")
    re_work_remarks = R("^Remarks : (.*?) *$")
    re_work_cross = R("^Near Intersection: (.*?) *$")
    re_transmit_date = R("^\d+ to .*? at ([0-9:]+) on \S+, ([0-9/]+)")

    def f_call_date(self, match):
        cdate, time = match.group(1), match.group(2)
        day, month, year = cdate.split("-")
        month = date.get_month_number(month)
        if month:
            month = "%02d" % (month,)
            return "20%s-%s-%s %s:%s:00" % (
                   year, month, day, time[:2], time[2:])
        else:
            raise ParserError, "Invalid month: %s" % (
                  match.group(1) + " " + match.group(2))

    f_transmit_date = lambda s, m: \
     tools.isodate(m.group(2) + " " + m.group(1))
    f_work_date = lambda s, m: tools.isodate(string.join(m.groups(), " "))

    find_work_state = lambda s: "TX"

    _re_termid = R("SEQUENCE NUMBER \d+ +CDC = (\S+)")
    _re_mapinfo = R("(MAPSCO \d+,[A-Z0-9]+)")
    _re_mapref = R("^MapRef *: *(\S+)") # NOT for map_page

    def find_locates(self):
        locates = []
        m = self._re_termid.search(self.data)
        if m:
            locates.append(m.group(1))

        return [locate.Locate(z) for z in locates]

    def find_work_description(self):
        pieces = []
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines,
               lambda s: s.startswith("Near Intersection:"))
        if idx1 > -1:
            for i in range(idx1+1, len(lines)):
                line = lines[i]
                if not line.strip():
                    break
                pieces.append(line.strip())
        return string.join(pieces, "\n")

    def post_process(self):
        # try to extract street number from address
        parts = self.ticket.work_address_street.split()
        if parts and parts[0][0] in "0123456789":
            self.ticket.work_address_street = string.join(parts[1:], " ")
            self.ticket.work_address_number = parts[0]

        if not self.ticket.map_page or self.ticket.map_page == "NONE":
            m = self._re_mapinfo.search(self.data)
            if m:
                self.ticket.map_page = m.group(1)

        # do this for Dallas1 only, not Dallas2 or other subclasses
        #if (self.__class__ is Parser
        # or self.__class__ is TX6001.Parser) \
        z = getattr(self.__class__, 'call_center', '')
        if z in ("Dallas1", "TX6001") \
        and self.ticket.ticket_type.find('EMER') == -1:
            m = self._re_mapref.search(self.data)
            if m and string.strip(m.group(1)) == 'HP':
                #self.ticket.ticket_type = 'HIGH PROFILE'
                self.ticket.ticket_type = 'HP - ' + self.ticket.ticket_type

        BaseParser.post_process(self)

    def is_fttp(self):
        #if self.__class__.__name__.startswith("Dallas1"):
        if self.__class__ is Parser:
            return self.ticket.company.find('VERIZON OSP') >= 0
        else:
            return False

class SummaryParser(NewJersey.SummaryParser):
    LENGTH = len("023040836 0007  ")
    rheader = R("^TESS audit for CDC of")
    rfooter = R("^Total number of Messages for \S+ +\d+")
    rclient = R("TESS audit for CDC of (.*) +on")
    rsummarydate = R("TESS audit for.*on (.*)$")
    rexpected = R("Total number of Messages for \S* +(.*)$")

    # New Jersey summaries can be multipart; possibly Dallas1 summaries can be
    # as well.

    def get_summary_date(self, data):
        m = self.rsummarydate.search(data)
        sdate = m and m.group(1) or ""
        if sdate:
            # special rule for FDX1: if the summary date falls in the weekend,
            # make it Friday instead
            sdate = "20%s-%s-%s 00:00:00" % (sdate[:2], sdate[2:4], sdate[4:])
            #d = Date(sdate)
            #while d.isweekend():
            #    d.dec()
            #sdate = d.isodate()
        return sdate

    def read(self, data):
        tickets = []
        for line in string.split(data, "\n"):
            line = string.strip(line)
            if line and line[0] in "0123456789":
                while line:
                    l2 = line[:self.LENGTH]
                    parts = string.split(l2)
                    if len(parts) == 2:
                        ticket_number, number = parts[:2]
                        detail = summarydetail.SummaryDetail([number, ticket_number, "", "", ""])
                        tickets.append(detail)
                    # inspect rest of line
                    line = line[self.LENGTH:]   # do not strip

        return tickets

    # XXX not sure if we need this for Dallas1 as well.
    def post_process(self, ts):
        NewJersey.SummaryParser.post_process(self, ts)

        # New Jersey has special rules for setting the summary_date_end
        # field...
        if ts.summary_date:
            d = date.Date(ts.summary_date)
            if d.isweekend():
                while d.isweekend():
                    d.dec() # return to last Friday
                ts.summary_date = d.isodate()
                d.inc(3)    # move to next Monday
                ts.summary_date_end = d.isodate()
            else:
                ts.summary_date_end = self._nextmorning(ts.summary_date)

        return ts

