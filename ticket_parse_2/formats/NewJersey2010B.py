# NewJersey2010B.py
# Same as NewJersey2010, but for call center NewJersey2 rather than
# NewJersey.

import NewJersey2010
import HoustonKorterra

class Parser(NewJersey2010.Parser):
    pass

class SummaryParser(HoustonKorterra.SummaryParser):

    def get_client_code(self, data):
        return "GSU2" # dummy
