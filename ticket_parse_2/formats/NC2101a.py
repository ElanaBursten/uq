# NC2101a.py

import NC2101
from re_shortcut import R

class Parser(NC2101.Parser):
    re_ticket_type = R("Priority: (.*?) *$")
    re_transmit_date = R("^Submitted date: ([0-9/]+) Time: ([0-9:]+)")

class SummaryParser(NC2101.SummaryParser):
    pass
