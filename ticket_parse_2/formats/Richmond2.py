import string
import re
from parsers import BaseParser
from re_shortcut import R
import tools
import locate

import Pennsylvania

class Parser(BaseParser):
    """ Ticket parser for Richmond2. Much like Atlanta, but the differences
        are too great to derive from AtlantaParser.
    """
    re_ticket_number = R("^Ticket : (\d+) +Date")
    re_transmit_date = R("ONISI +([0-9/]+) +([0-9:]+) +")
    re_call_date = R("^Ticket.*?Date: ([0-9/]+) Time: ([0-9:]+) Oper")
    re_operator = R("^Ticket.*Oper: (.*) +Op")
    re_ticket_type = R("ONISI.* +\d+ (.*)$")

    re_work_state = R("^State: (..)")
    re_work_county = R("^State.*County: (.*?) +Place")
    re_work_city = R("^State.*Place: (.*)$")
    re_work_address_number = R("^Addr *: (.*?) +Street")
    re_work_address_street = R("^Addr.*Street: (.*)$")
    re_work_cross = R("^Near Intersection\(s\): (.*)$")

    re_work_type = R("^Work [tT]ype: (.*) *$")
    re_work_date = R("^Work date: +([0-9/]+) +Time: +([0-9:]+) +Hrs")
    re_work_notc = R("^Work date.*Hrs notc: (.*?) +Prio")
    re_priority = R("^Work date.*Priority: (.*)$")

    re_company = R("^Instructions.*Done for: (.*)$")
    re_con_name = R("^Company: (.*?) *(Call|$)")
    re_con_address = R("^Address: (.*)$")
    re_con_city = R("^City&St: (.*?),")
    re_con_state = R("^City&St:.*?, +(..)")
    re_con_zip = R("City&St.*Zip: (.*?)( +Fax|$)")
    re_caller_fax = R("City&St.*Fax: (.*)$")
    re_caller = R("^Caller : +(.*?) +Phone")
    re_caller_phone = R("^Caller.*Phone: (.*)$")

    re_respond_date = R("^Expire date: +([0-9/]+) +Time: +([0-9:]+)")

    re_map_page = R("^Grids: +(\S+)")   # for now

    f_call_date = lambda s, m: s.isodate(m.group(1) + " " + m.group(2))
    f_respond_date = f_transmit_date = f_work_date = f_call_date

    def find_work_description(self):
        desc = ""
        for line in string.split(self.data, "\n"):
            if desc:
                if string.strip(line).startswith(":"):
                    desc = desc + string.strip(line)[2:]
                else:
                    break
            else:
                if line.startswith("Locat:"):
                    desc = line[len("Locat: "):]
        return desc

    def find_work_remarks(self):
        remarks = ""
        for line in string.split(self.data, "\n"):
            if remarks:
                if string.strip(line):
                    remarks = remarks + string.strip(line) + " "
                else:
                    # empty line marks the end of the remarks
                    break
            else:
                if line.startswith("Remarks:"):
                    remarks = "!"
        return remarks[1:-1]  # remove '!' and trailing space

    def find_locates(self):
        lines = string.split(self.data, "\n")
        locates = ""
        for line in lines:
            line = string.strip(line)
            if line.startswith("Mbrs :"):
                locates = string.strip(line[7:]) + " "
                continue
            if locates:
                if line.startswith(": "):
                    # If the line started with ":", it would be an invalid locate
                    locates = locates + string.rstrip(line[1:]) + " "
                elif line:
                    locates = locates + line + " "
                else:
                    break   # empty line
        return map(locate.Locate, string.split(locates))

class SummaryParser(Pennsylvania.SummaryParser):

    def read(self, data):
        # almost the same as Pennsylvania's, only Richmond2 omits the leading
        # zeroes for the ticket numbers, leading to confusion
        tickets = Pennsylvania.SummaryParser.read(self, data)
        for ticket in tickets:
            ticket_number = ticket[1]
            while len(ticket_number) < 7:
                ticket_number = "0" + ticket_number
            ticket[1] = ticket_number
        return tickets

    def has_header(self, data):
        C = "THIS IS YOUR END OF DAY AUDIT FROM O N I S I"
        rpage = R("PAGE 1 *$")  # don't match PAGE 10, 11, etc
        return (string.find(data, C) > -1) and (not not rpage.search(data))

    def has_footer(self, data):
        rfooter = R("^ +GRAND TOTAL +\d+ +\d+ +\d+ +\d+")
        return not not rfooter.search(data)

    def get_client_code(self, data):
        return "-"
        # The "client" for this format is always UTIL11. No real client is
        # given. Therefore, we "blank" this field rather than storing a
        # pseudo-client code.

    def get_summary_date(self, data):
        m = R("^TICKET SUMMARY REPORT AT (\S+) (\S+) (\S+) FOR").search(data)
        if m:
            date, time, ampm = m.group(1), m.group(2), m.group(3)
            summary_date = tools.isodate(date)  # disregard time
        else:
            summary_date = ""
        return summary_date

    def get_expected_tickets(self, data):
        m = R("^ +GRAND TOTAL.* (\d+?) *$").search(data)
        expected_tickets = m and int(m.group(1)) or 0
        return expected_tickets

    # _has_header works different here, so we override parse()
    def parse(self, data):
        ts = Pennsylvania.SummaryParser.parse(self, data)
        rheader = R("END OF DAY AUDIT FROM O N I S I")
        ts._has_header = not not rheader.search(data)
        return ts
