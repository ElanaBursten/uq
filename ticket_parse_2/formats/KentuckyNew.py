# KentuckyNew.py

from parsers import BaseParser
from re_shortcut import R
import locate
import NorthCarolina
from date import Date

class Parser(BaseParser):
    re_transmit_date = R("^[0-9A-Z]+ +\d\d\d\d\d +KUPI[a-z] +([0-9/]+) ([0-9:]+) \d")
    re_ticket_type = R("^[0-9A-Z]+ +\d\d\d\d\d +KUPI[a-z] +[0-9/]+ [0-9:]+ \d+-\d+[A-Z]? (.*?) *$")
    re_ticket_number = R("^Ticket : (\S+)")
    re_call_date = R("^Ticket : \S+ Date: ([0-9/]+) Time: ([0-9:]+)")
    re_work_state = R("^State: (\S+)")
    re_work_county = R("^State.*Cnty: (.*?) City:")
    re_work_city = R("^State:.*City: (.*?) *$")
    re_work_address_number = R("^Address : (.*?) *$")
    re_work_address_street = R("^Street  : (.*?) *$")
    re_work_cross = R("^Cross 1 : (.*?) *$")
    re_work_type = R("^Work type : (.*?) *$")
    re_company = R("^Done for  : (.*?) *$")
    re_work_date = R("^Start date: ([0-9/]+) +Time: ([0-9:]+)")
    re_con_name = R("^Company : (.*?) +Type:")
    re_con_address = R("^Co addr : (.*?) *$")
    re_con_city = R("^City +: (.*?) *State:")
    re_con_state = R("^City.*State: (\S+)")
    re_con_zip = R("^City.*Zip: (\S+)")
    re_caller = R("^Caller  : (.*?) *Phone:")
    re_caller_phone = R("^Caller.*Phone: (\S+)")
    re_caller_contact = R("^Contact : (.*?) *Phone:")
    re_caller_fax = R("^Fax +: (\S+)")
    re_caller_email = R("^Email +: (\S+)")

    def f_transmit_date(self, match):
        d, t = match.group(1), match.group(2)
        m, d, y = d.split("/")
        return "%s-%s-%s %s" % (y, m, d, t)

    def f_call_date(self, match):
        d, t = match.group(1), match.group(2)
        m, d, y = d.split("/")
        return "%s-%s-%s %s:00" % (y, m, d, t)
    f_work_date = f_call_date

    def find_locates(self):
        return [locate.Locate("BELLTN")]

    def find_work_remarks(self):
        lines = self.data.split("\n")
        remarks = ""
        for line in lines:
            line = line.strip()
            if line.startswith("Remarks :"):
                remarks = line[10:].strip()
                continue
            if remarks:
                if line.startswith(": "):
                    remarks = remarks + line[1:].rstrip()
                else:
                    break
        return remarks

class SummaryParser(NorthCarolina.SummaryParser):
    LENGTH = len("00001 0607110006-01A 11:28   ")
    LENGTH2 = LENGTH
    re_header = R("^([A-Z0-9]+) \*EOD\* KUPI[a-z]")
    re_date = R("^[A-Z0-9]+ \*EOD\* KUPI[a-z] ([0-9/]+) ([0-9:]+)")

    def has_header(self, data):
        m = self.re_header.search(data)
        return bool(m)

    def get_client_code(self, data):
        return "BELLTN"
        #m = self.re_header.search(data)
        #client_code = m and m.group(1) or "FNV2"
        #return client_code

    def get_summary_date(self, data):
        m = self.re_date.search(data)
        if m:
            date_part, time_part = m.group(1), m.group(2)
            hours, minutes, seconds = time_part.split(":")
            m, d, y = date_part.split("/")
            d = Date("%s-%s-%s 00:00:00" % (y, m, d))
            if int(hours) < 8:
                d.dec()
            return d.isodate()
        else:
            import date
            return date.today() # all we can do
