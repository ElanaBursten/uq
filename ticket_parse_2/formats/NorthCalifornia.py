# NorthCalifornia.py

from parsers import BaseParser, ParserError
from re_shortcut import R
import parser_tools
import tools
import locate
import string
import Illinois
from date import Date
import SouthCalifornia

class Parser(BaseParser):

    # steal methods from SouthCalifornia
    SCA_mod_ticket_type = SouthCalifornia.Parser.mod_ticket_type.im_func

    re_ticket_number = R("^ *Message Number: (\S+)")
    re_transmit_date = R("[A-Z0-9]+\s+\d+\s+USAN +([0-9/]+) ([0-9:]+)+")
    re_call_date = R("Received by \S+ at ([0-9:]+) on ([0-9/]+)")
    re_ticket_type = R("[A-Z0-9]+\s+\d+\s+USAN [0-9/]+ [0-9:]+ \d+ (.*?) *$")

    re_work_date = R("^ *Work Begins: +([0-9/]+) at ([0-9:]+)")
    re_priority = R("Priority: (\S+)")
    re_caller = R("^ *Caller: +(.*?) *$")
    re_con_name = R("^ *Company: +(.*?) *$")
    re_con_address = R("^ *Address: +(.*?) *$")
    re_con_city = R("^ *City: +(.*?) *State")
    re_con_state = R("^ *City.*State: (\S+)")
    re_con_zip = R("^ *City.*Zip: (\S+)")
    re_caller_phone = R("^Business Tel: +(.*?) *Fax:")
    re_caller_fax = R("^Business Tel: +.* *Fax: *([\d-]+)")

    re_work_type = R("^ *Nature of Work: (.*?) *$")
    re_company = R("^Done for: +(.*?) *Explo")
    re_work_city = R("^ *Place: (.*?) *County")
    re_work_county = R("^ *Place.*County: (.*?) *State")
    re_work_state = R("^ *Place.*State: (\S+)")

    re_caller_altcontact = R("^Foreman: +(.*?) *$")
    re_caller_altphone = R("^Field Tel: *([\d-]+) *Cell Tel:")
    re_caller_cellular = R("^Field Tel: .* Cell Tel: *([\d-]+)")
    re_caller_email = R("^Email Address: *(\S+)")
    re_explosives = R("^Done for: +.*? *Explosives: (\w)")

    # work_address_street and work_cross are apparently optional.  the number
    # is extracted by post_process.
    re_work_address_street = R("^ *Street Address:\s+(.*?)\s*$")
    re_work_cross = R("^\s+Cross Street:\s+(.*?)\s*$")

    f_transmit_date = lambda s, m: s.isodate(string.join(m.groups(), " "))
    f_work_date = f_transmit_date

    # call_date is a special case, has order reversed
    f_call_date = lambda s, m: s.isodate(m.group(2) + " " + m.group(1))

    def find_kind(self):
        if self.ticket.priority == "1" and self.data.find("EMER") > -1:
            return "EMERGENCY"
        elif self.ticket.priority == "0":
            return "EMERGENCY"
        else:
            return "NORMAL"

    def find_work_description(self):
        lines = self.data.split("\n")
        lines = tools.find_block(lines,
                lambda s: (s.lstrip().startswith('Cross Street:')
                           or s.lstrip().startswith('Location:')),
                lambda s: s.lstrip().startswith('Place:'))
        if lines:
            return string.join([s.strip() for s in lines[1:]], '\n').strip()
        else:
            return ""

    def find_work_long(self):
        lines = self.data.split("\n")
        idx = tools.findfirst(lines, lambda s: s.lstrip().startswith("Long/Lat"))
        if idx > -1:
            line = lines[idx]
            parts = line.split()
            try:
                long1 = float(parts[2])
                long2 = float(parts[6])
                return (long1+long2) / 2
            except:
                return 0.0
        else:
            return 0.0

    def find_work_lat(self):
        lines = self.data.split("\n")
        idx = tools.findfirst(lines, lambda s: s.lstrip().startswith("Long/Lat"))
        if idx > -1:
            line = lines[idx]
            parts = line.split()
            try:
                lat1 = float(parts[4])
                lat2 = float(parts[8])
                return (lat1+lat2) / 2
            except:
                return 0.0
        else:
            return 0.0

    def find_work_remarks(self):
        lines = self.data.split("\n")
        lines = tools.find_block(lines,
                lambda s: s.lstrip().startswith("Comments:"),
                lambda s: s.lstrip().startswith("Sent to:"))

        if lines:
            return string.join([s.strip() for s in lines[1:]], '\n').strip()
        else:
            return ""

        #idx1 = tools.findfirst(lines, lambda s: s.lstrip().startswith("Comments:"))
        #if idx1 > -1:
        #    idx2 = tools.findfirst(lines[idx1+1:], lambda s: not s.strip())
        #    if idx2 > -1:
        #        lines2 = [line.strip() for line in lines[idx1+1:idx1+1+idx2]]
        #        return string.join(lines2, "\n")
        #return ""

    def _find_termid(self):
        """ Get locate from the first line of the ticket image.  May or may
            not be used, depending on the latest rules. """
        regex = R("([A-Z0-9]+)\s+\d+\s+USAN +\d\d")
        m = regex.search(self.data)
        if m:
            client_code = m.group(1)
            return client_code
        else:
            raise ParserError("Ticket has no term id")

    #
    # finding locates

    SPECIAL_LOCATES = ('SCERID', 'SCETUL', 'SCEVAL', 'SCEVIC', 'SCEALH')
    # rule to be implemented:
    # *REMOVE* them if they appear in the Send To section
    # but *ADD* them if the term id is one of these; ignore term id otherwise
    # XXX this rule is call center specific and should really be moved

    def _find_locates(self):
        """ Get locates in the "Send to" section.  May or may not be used,
            depending on the latest rules. """
        locates = []
        lines = string.split(self.data, "\n")
        lines.append("")    # add an empty line in case the locates block ends
        idx1 = tools.findfirst(lines, lambda s: s.lstrip().startswith("Sent to:"))
        if idx1 > -1:
            idx2 = tools.findfirst(lines[idx1+1:], lambda s: not s.strip())
            if idx2 > -1:
                for line in lines[idx1+1:idx1+1+idx2]:
                    while line.strip():
                        chunk = line[:38]   # first 38 characters
                        code, desc = string.split(chunk, "=")
                        loc = locate.Locate(string.strip(code))
                        locates.append(loc)
                        line = line[38:]    # get rest of line

        return locates

    def find_locates(self):
        # use the term id, not the members section
        locates = self._find_locates()
        locates = [loc for loc in locates
                   if not loc.client_code in self.SPECIAL_LOCATES]
        termid = self._find_termid()
        if termid in self.SPECIAL_LOCATES:
            locates.append(locate.Locate(termid))
        return locates

    def post_process(self):
        self.SCA_mod_ticket_type()

        # set map_page to qtrmin value
        if self.ticket.work_lat > 0 and self.ticket.work_long < 0:
            self.ticket.map_page = tools.decimal2grid(self.ticket.work_lat,
                                   self.ticket.work_long)
        # remove the number from work_address_street and put it in
        # work_address_number
        t = self.ticket
        parts = t.work_address_street.split()
        if parts:
            if tools.is_numeric(parts[0]):
                t.work_address_number = parts[0]
                t.work_address_street = string.join(parts[1:], " ")

        if ('EMER' not in t.ticket_type) and parser_tools.nca_isemergency(t):
            t.ticket_type += '-EMER'

        BaseParser.post_process(self)

class SummaryParser(Illinois.SummaryParser):

    RECOG_HEADER_2 = "DAILY TICKET RECAP"

    re_footer = R("^\s+TOTAL +- (\d+) *$")
    re_recog_header_1 = R("[A-Z0-9]+ +USAN +[0-9/]+")

    re_summary_date = R("THIS IS YOUR DAILY TICKET RECAP FOR ([0-9/]+)")

    def ALT_get_summary_date(self, data):
        date = ""  # default values, empty
        lines = string.split(data, "\n")
        for line in lines:
            m = self.re_recog_header_1.search(line)
            if m:
                parts = string.split(line)
                date1 = parts[2]
                date = tools.isodate(date1)
                date = Date(date).isodate()
                break
        return date

    def get_summary_date(self, data):
        m = self.re_summary_date.search(data)
        if m:
            d = m.group(1)
            return tools.isodate(d)
        else:
            return self.ALT_get_summary_date(data)
