# Idaho.py

import QWEST_IDL
from re_shortcut import R
import Alaska

class Parser(QWEST_IDL.Parser):
    required = QWEST_IDL.Parser.required[:]
    required.remove("serial_number")
    # the serial_number won't work (judging from the test tickets), but the
    # rest should work just fine

    _re_work_lat = R("^Latitude: (\S+)")
    _re_work_long = R("Longitude: (\S+)")

    def find_locates(self):
        # cut off long locate names
        locates = QWEST_IDL.Parser.find_locates(self)
        for loc in locates:
            if len(loc.client_code) > 10:
                loc.client_code = loc.client_code[:10]
        return locates

    def find_work_lat(self):
        lat = 0
        m = self._re_work_lat.search(self.data)
        if m:
            try:
                lat = float(m.group(1))
            except:
                lat = 0
        return lat

    def find_work_long(self):
        long = 0
        m = self._re_work_long.search(self.data)
        if m:
            try:
                long = float(m.group(1))
            except:
                long = 0
        return long

class SummaryParser(Alaska.SummaryParser):
    re_footer = R("^TOTAL = (\d+) *$")

