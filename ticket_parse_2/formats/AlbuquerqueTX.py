# AlbuquerqueTX.py

import Dallas1
from re_shortcut import R
import re

class Parser(Dallas1.Parser):
    call_center = 'AlbuquerqueTX'
    #G_LOCATES = []
    re_transmit_date = Dallas1.Parser.re_call_date
    f_transmit_date = Dallas1.Parser.f_call_date
    re_ticket_type = R("^\* +(.*?) +\*.*?MESSAGES Sent to", re.DOTALL)

class SummaryParser(Dallas1.SummaryParser):
    pass
