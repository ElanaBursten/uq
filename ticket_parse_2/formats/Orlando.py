import string
import re
from parsers import BaseParser
from re_shortcut import R
import tools
import locate
import NorthCarolina
from date import Date

class Parser(BaseParser):

    MAX_LOCATES = 90

    re_ticket_number = R("^Ticket : (\d+)")
    re_revision = R("^Ticket.*?Rev:(...)")
    re_call_date = R("^Ticket.*?Taken: ([0-9/]+) ([0-9:]+)")

    re_ticket_type = R("CALL SUNSHINE \S+ \S+ \S+ *(.*?) *$")

    re_work_state = R("^State: (\S+)")
    re_work_county = R("^State.*?Cnty: (.*?) *(?:Geo)?Place")
    re_work_city = R("^(?:State.*? Place:|CallerPlace:) (.*?) *$")
    re_work_subdivision = R("^Subdivision: (.*?) *($|Lot:)")

    re_work_address_number = R("^Address : (.*?) *$")
    re_work_address_street = R("^Street +: (.*?) *$")
    re_work_cross = R("^Cross 1 : (.*?)\s*Within 1", re.MULTILINE|re.DOTALL)

    re_work_date = R("^Work date: ([0-9/]+) Time: ([0-9:]+)")
    re_work_notc = R("^Work date.*?Hrs notc: (\S+)")
    re_duration = R("^Work date.*?Duration: (.*?) *$")
    re_due_date = R("^Due Date : ([0-9/]+) Time: ([0-9:]+)")
    re_legal_due_date = re_due_date
    re_work_type = R("^Work type: (.*?) *(Boring|$)")
    re_company = R("^Done for : (.*?) *$")

    re_con_name = R("^Company : (.*?) *Type:")
    re_con_address = R("^Co addr : (.*?) *$")
    re_con_city = R("^City +: (.*?) *State:")
    re_con_state = R("^City.*State: (..)")
    re_con_zip = R("^City.*Zip: (\S+)")

    re_caller = R("^Caller +: (.*?) *Phone:")
    re_caller_phone = R("^Caller.*?Phone: (.*?) *$")
    re_caller_contact = R("^Contact : (.*?) Phone:")
    re_caller_email = R("^Email +: (.*?) *$")
    re_caller_fax = R("^Fax +: (.*?) *$")

    #re_transmit_date = R("^Submitted: ([0-9/]+) ([0-9:]+)")
    re_transmit_date = R("\d+ CALL SUNSHINE ([0-9/]+) ([0-9:]+)")
    re_operator = R("^Submitted.*?Oper: (\S+)")
    re_channel = R("^Submitted.*Chan: (\S+)")

    re_map_page = R("^Grids +: *(\S+)") # only the first one counts

    f_call_date = lambda s, m: tools.isodate(m.group(1) + " " + m.group(2))
    f_transmit_date = f_call_date
    f_work_date = f_call_date
    f_due_date = f_call_date
    f_legal_due_date = f_call_date

    def find_work_remarks(self):
        # Orlando remarks *end* when encountering a line with a single ":"
        lines = string.split(self.data, "\n")
        remarks = ""
        for line in lines:
            if line.startswith("Remarks :"):
                remarks = remarks + line[10:].strip()
            else:
                if remarks:
                    if line.strip() == ":":
                        break
                    else:
                        remarks = remarks + " " + line.strip()
        return remarks

    def find_work_description(self):
        # Orlando remarks *end* when encountering a line with a single ":"
        lines = string.split(self.data, "\n")
        remarks = ""
        for line in lines:
            if line.startswith("Locat:"):
                remarks = remarks + line[7:].strip()
            else:
                if remarks:
                    if line.strip() == ":":
                        break
                    else:
                        remarks = remarks + " " + line.strip()
        return remarks

    def find_locates(self):
        locates = []
        for line in string.split(self.data, "\n"):
            if line.startswith("Mbrs :"):
                locates.extend(string.split(line[7:]))
        return map(locate.Locate, locates)

    def find_kind(self):
        type = self.ticket.ticket_type.lower()
        if type.find("emer") > -1 or type.find("rush") > -1:
            return "EMERGENCY"
        else:
            return "NORMAL"

    def post_process(self):
        # normalize map_page, we need it for routing
        if self.ticket.map_page:
            map_page = self.ticket.map_page
            map_page = string.replace(map_page, "-", "")
            map_page = string.replace(map_page, "*", "B")
            self.ticket.map_page = map_page

        # sometimes a range is specified in the number field
        re_range = R("(\S+) to (\S+)")
        m = re_range.search(self.ticket.work_address_number)
        if m and len(m.groups()) == 2:
            self.ticket.work_address_number = m.group(1)
            self.ticket.work_address_number_2 = m.group(2)

        # don't forget the post_process of the superclass
        BaseParser.post_process(self)

    ### Finding longitudes/latitudes in grids

    def find_work_lat(self):
        return self._compute_longlats()[0]

    def find_work_long(self):
        return self._compute_longlats()[1]

class SummaryParser(NorthCarolina.SummaryParser):

    def has_header(self, data):
        rheader = R("NOW ENDING TRANSMISSION FOR TODAY")
        return not not rheader.search(data)

    def get_client_code(self, data):
        m = R("^([A-Z0-9*]+) CALL SUNSHINE").search(data)
        client_code = m and m.group(1) or ""
        if client_code.endswith("*"):
            client_code = client_code[:-1]
        return client_code

    def get_summary_date(self, data):
        m = R("CALL SUNSHINE ([0-9/]+) ([0-9:]+)").search(data)
        if m:
            _m, _d, _y = map(int, string.split(m.group(1), "/"))
            ds = "20%02d-%02d-%02d" % (_y, _m, _d) + " " + m.group(2)
            sd = Date(ds)
            # if the time is early in the morning, take the previous date
            if sd.hours <= 10:   # 0 - 7 AM
                sd.dec()
            # set time to 00:00:00
            sd.hours = sd.minutes = sd.seconds = 0
            d = sd.isodate()
        else:
            d = ""
        return d

    def get_expected_tickets(self, data):
        m = R("^Total: (\d+)").search(data)
        expected_tickets = m and int(m.group(1)) or 0
        return expected_tickets
