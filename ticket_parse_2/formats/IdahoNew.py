# IdahoNew.py

import Idaho

class Parser(Idaho.Parser):
    only_one = Idaho.Parser.only_one[:]
    if "ticket_number" in only_one:
        only_one.remove("ticket_number")

class SummaryParser(Idaho.SummaryParser):
    pass
