# _ClickScreenerSummary.py

import re
from re_shortcut import R
from summaryparsers import BaseSummaryParser
import summarydetail
import tools

# TODO: integrate with Washington4.SummaryParser
# but note that cleared tickets are handled differently... =/

class SummaryParser(BaseSummaryParser):

    re_date = R("^\w+\s+\w+\s+ClickScreener\s+EOD\s+for\s+([0-9/]+)", re.I)

    def has_header(self, data):
        return "ClickScreener EOD" in data

    def has_footer(self, data):
        return "Total Tickets" in data

    def get_client_code(self, data):
        m = R("(^\w+)\s+\w+\s+ClickScreener EOD", re.IGNORECASE).search(data)
        client_code = m and m.group(1) or ""
        return client_code

    def get_summary_date(self, data):
        m = self.re_date.search(data)
        if m:
            d = m.group(1)
            summary_date = tools.isodate(d)
        else:
            summary_date = ""
        return summary_date

    def get_expected_tickets(self, data):
        m = R("([0-9,]+)\s+Total Tickets", re.IGNORECASE).search(data)
        xt = m and int(m.group(1).replace(',','')) or 0
        return xt

    def read(self, data):
        self.cleared = 0
        tickets = []
        lines = data.split("\n")
        idx = -1
        for i in range(len(lines)):
            if lines[i].lower().startswith("ticket         rev  seq"):
                idx = i
                break
        if idx >= 0:
            idx = idx + 1
            starts = [match.start() for match in re.finditer(re.escape(" ="), lines[idx])]
            idx = idx + 1
            while idx < len(lines):
                if not lines[idx].strip():
                    break   # empty line ends this session
                line = lines[idx]
                ticket_number = line[0:starts[0]].strip()
                revision = line[starts[0]:starts[1]].strip()
                number = line[starts[1]:starts[2]].strip()
                type_code = line[starts[3]:starts[4]].strip()[:20]
                ticket_time = "00:00" # No time, just date
                if 'tickets.utiliquest.com' in line[starts[4]:starts[5]].strip():
                    detail = summarydetail.SummaryDetail(
                             [number, ticket_number, ticket_time, revision, type_code])
                    tickets.append(detail)
                if 'Cleared' in line[starts[4]:starts[5]].strip():
                    self.cleared += 1

                idx = idx + 1

        return tickets

    def post_process(self, ts):
        '''
        Subtract the number of cleared tickets from the expected_tickets
        '''
        ts.expected_tickets -= self.cleared
        return BaseSummaryParser.post_process(self, ts)

