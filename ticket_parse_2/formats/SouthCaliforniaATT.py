# SouthCaliforniaATT.py

from summaryparsers import BaseSummaryParser
from re_shortcut import R
import parsers
import summarydetail
import date
import SouthCalifornia
import locate

class Parser(SouthCalifornia.Parser):
    re_work_cross = R("^ *X/ST 1 +: (.*?) *$")
    re_serial_number = R("^ *At&t Ticket Id: (\S+)")
    _re_term_id = R("^\s*(\S+)\s+\d+[A-Z]\s+USAS \d\d")

    def find_locates(self):
        m = self._re_term_id.search(self.data)
        if m:
            return [locate.Locate(m.group(1))]
        else:
            raise parsers.ParserError, "No term id found"

    _re_plat = R("clli code:\s+(\S+)")
    _re_hp_section = R("(ATTDSOUTH:\s*.*?)\s*$")

    def post_process(self):
        # add plat...
        m = self._re_plat.search(self.data)
        if m:
            for loc in self.ticket.locates:
                loc._plat = m.group(1)

        # add HP section, if any
        m = self._re_hp_section.search(self.data)
        if m:
            hp_info = m.group(1).strip()[:150]
            self.ticket._hp_info.append(hp_info)

        SouthCalifornia.Parser.post_process(self)


class SummaryParser(BaseSummaryParser):
    r_summary_date = R("Daily Summary Report (\d\d)(\d\d)(\d\d) *$")
    r_total = R("^TOTAL\s+(\S+)")

    def read(self, data):
        re_start = R("^-{41} *")
        start = re_start.search(data).end() + 1
        re_end = R("^ +-+")
        end = re_end.search(data).start() - 1

        # groups: 1: seq number, 2: ticket number; 3: version; 4: client code;
        #         5: time
        tickets = []
        lines = data[start:end].split("\n")
        for line in lines:
            if (not line.strip()) or line.startswith(" "):
                continue
            item_number = line[:5]
            ticket_number = line[7:16]
            revision = line[17:20]
            ticket_time = line[32:37]

            t = summarydetail.SummaryDetail([item_number, ticket_number,
                ticket_time, revision])
            tickets.append(t)

        return tickets

    def get_client_code(self, data):
        return "SCA6"

    def get_summary_date(self, data):
        m = self.r_summary_date.search(data)
        if m:
            s = "20%s-%s-%s 00:00:00" % (m.group(3), m.group(1), m.group(2))
            return s
        else:
            return date.Date().isodate()    # take today, I guess

    def get_expected_tickets(self, data):
        m = self.r_total.search(data)
        if m:
            try:
                total = int(m.group(1))
            except ValueError:
                return 0
            else:
                return total
        else:
            return 0

    def has_header(self, data):
        return bool(self.r_summary_date.search(data))

    def has_footer(self, data):
        return bool(self.r_total.search(data))

