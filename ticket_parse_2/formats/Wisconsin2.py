# Wisconsin2.py

from parsers import BaseParser
from re_shortcut import R
import locate
import re
import string
import tools
import Lanham

class Parser(BaseParser):

    re_ticket_number = R("^Ticket no.: *(\S+)")
    re_transmit_date = R("^Ticket no.*Date/Time: +([0-9/\-]+) at +([0-9:]+)")
    re_call_date = re_transmit_date
    re_ticket_type = R("^(.*?) *Operator.*?\nTicket no", re.DOTALL)
    re_work_type = R("^Type of Work: (.*?) *(\||$)")
    re_company = R("^Work for: (.*?) *Exp.:")
    re_work_date = R("^Start Date: +([0-9/\-]+) *Time: +([0-9:]+)(AM|PM)")
    re_work_county = R("^County: (.*?) *(\||$)")
    re_work_city = R("Place: (.*?) *(\||$)")
    #re_caller_contact = R("^Lats:.*?\n *(.*?) *(Phone:|$)", re.DOTALL)
    re_caller_phone = R("Phone: (.*?) *(\||$)", re.DOTALL)
    #re_con_name = R("^Lats:.*?\n.*?\n.*?\n.*? *(.*?) *$", re.DOTALL)
    re_work_description = R("^Marking Instructions: (.*?) *\n\n")
    re_work_remarks = R("^Remarks: (.*?) *(\n\n|$)")

    def f_transmit_date(self, match):
        spam = string.join(match.groups(), ' ')
        eggs = spam.replace('-', '/')
        return tools.isodate(eggs)
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date

    _re_work_lat = R("^Lats: +(\d+\.\d+),(\d+\.\d+)")
    _re_work_long = R("Lons: +(-\d+\.\d+),(-\d+\.\d+)")

    _re_addr1 = R("^At:")
    _re_addr2 = R("^Fire no.:")
    _re_addr3 = R("^Street:")
    _re_termid = R("\| ([A-Za-z0-9]+) *\d* *$")

    def find_work_state(self):
        return "WI"

    def find_locates(self):
        z = self._re_termid.findall(self.data)
        return [locate.Locate(x) for x in z]

    def post_process(self):
        # find fields that could not be retrieved by regexen
        lines = self.data.split("\n")
        for line in lines:
            if self._re_addr1.search(line):
                numchunk, streetchunk = line[4:27], line[27:]
                self.ticket.work_address_number = numchunk.strip()
                self.ticket.work_address_street = string.join(streetchunk.split(), " ")
                break
            elif self._re_addr2.search(line):
                numchunk, streetchunk = line[10:20], line[20:]
                self.ticket.work_address_number = numchunk.strip()
                self.ticket.work_address_street = string.join(streetchunk.split(), " ")
                break
            elif self._re_addr3.search(line):
                numchunk, streetchunk = line[7:20], line[20:]
                self.ticket.work_address_number = numchunk.strip()
                self.ticket.work_address_street = string.join(streetchunk.split(), " ")
                break

        # remove parts of locate section, if present
        if '|' in self.ticket.work_address_street:
            idx = self.ticket.work_address_street.find('|')
            self.ticket.work_address_street = self.ticket.work_address_street[:idx].rstrip()

        idx = tools.findfirst(lines, lambda l: l.startswith("Lats:"))
        if idx > -1:
            self.ticket.caller_contact = lines[idx+3][:40].strip()
            self.ticket.con_name = lines[idx+4][:40].strip()
            self.ticket.con_address = lines[idx+5][:40].strip()
            chunk = lines[idx+6][:40]
            first, last = chunk.split(",", 1)
            self.ticket.con_city = first.strip()
            foo = last.split()
            self.ticket.con_state = (len(foo) > 0) and foo[0] or ""
            self.ticket.con_zip = (len(foo) > 1) and foo[1] or ""

        # other post-processing and correction stuff
        if self.ticket.ticket_type.startswith("EMER"):
            self.ticket.kind = "EMERGENCY"
        else:
            self.ticket.kind = "NORMAL"

        # remove leading C/, V/, etc
        if len(self.ticket.work_city) >= 2 and self.ticket.work_city[1] == '/':
            self.ticket.work_city = self.ticket.work_city[2:]

        BaseParser.post_process(self)

    def find_work_long(self):
        """
        Find the work longitude
        """
        m = self._re_work_long.search(self.data)
        if m:
            return (float(m.group(1))+float(m.group(2)))/2.0
        else:
            return 0.0

    def find_work_lat(self):
        """
        Find the work latitude
        """
        m = self._re_work_lat.search(self.data)
        if m:
            return (float(m.group(1))+float(m.group(2)))/2.0
        else:
            return 0.0

class SummaryParser(Lanham.SummaryParser):
    prepend_zeroes = 7  # ticket number is 7 digits, no leading zeroes
