# Lanham.py

import decimal
import re
import string
#
from parsers import BaseParser
from re_shortcut import R
from summaryparsers import BaseSummaryParser
import locate
import summarydetail
import tools

class Parser(BaseParser):
    """ Ticket parser for Lanham, Maryland. """
    s_work_description = "Extent of Work:"

    required = BaseParser.required[:]
    required.remove("ticket_type")

    re_ticket_number = R("^Ticket No: +(\d+)( *|$)")
    re_transmit_date = R("^Transmit +Date: +([0-9/]+) +Time: +([0-9:]+) +")
    re_operator = R("^Transmit.*?Op: *(.*?) *$")
    re_call_date = R("^Original Call Date: +([0-9/]+) +Time: +([0-9:]+)")
    re_work_date = R("^Work to Begin Date: +([0-9/]+) +Time: +([0-9APM]+)")
    re_ticket_type = R("NOTICE.*EXCAVATE *(.*?) *$")
    re_update_of = R("^Ticket No:.*Update Of: (\S+)")

    re_work_city = R("^Place: +(.*)$")
    re_work_address_number = R("^Address: +(.*?) +Street")
    re_work_address_street = R("^Address.*?Street: *(.*)$")
    re_work_cross = R("^Nearest Intersecting Street: *(.*)$")

    re_work_type = R("^Type of Work: *(.*?) *$")
    re_caller_fax = R("Fax\s*: +(.*)$")

    re_con_name = R("^Company +: *(.*)$")
    re_caller = R("^Contact Name *?: +(.*?) +(Con|Fax)")
    re_caller_contact = re_caller
    re_caller_phone = R("Contact Phone: *(.*?) *(Ext|$)")
    re_caller_altcontact = R("^Alt. Contact *?: *(.*?) +Alt")
    re_caller_altphone = R("Alt. Phone +: *(.*?) *$")
    re_caller_email = R("^Email Address: *(.*?) *$")
    re_company = R("^Work Being Done For: *(.*?) *$")
    re_con_state = R("^State: ([A-Z][A-Z])")
    re_work_county = R("^State.*?County: *(.*?) *$")
    re_work_state = re_con_state
    # It may not be important to know what county the excavator is in...

    re_map_page = R("^(?:Caller +Provided: +)?Map: +(\S+) +Page: +(\S+) +Grid Cells: +(\S+?)(,|\s|$)")
    re_explosives = R("Explosives: (.*?) *$")

    f_transmit_date = lambda s, m: s.isodate(m.group(1) + " " + m.group(2))
    f_call_date = f_work_date = f_transmit_date

    f_ticket_type = lambda s, m: tools.singlespaced(m.group(1))
    f_map_page = lambda s, m: m.group(1) + " " + m.group(2) + " " + m.group(3)

    f_ticket_type = lambda s, m: m.group(1) or "-"

    def find_work_description(self):
        lines = string.split(self.data, "\n")
        desc = ""
        for line in lines:
            if line.startswith(self.s_work_description):
                desc = line[len(self.s_work_description):].strip()
                continue
            if desc:
                if line.startswith(":"):
                    desc = desc + " " + line[1:].strip()
                else:
                    break
        return desc

    def find_work_remarks(self):
        lines = self.data.split('\n')
        desc = ""
        for line in lines:
            if line.startswith(("Remarks:", "Comments:")):
                # ugly hack to remove first word ("Remarks:" or "Comments:")
                desc = (line.split(None, 1) + [''])[1]
                continue
            if desc:
                if line.startswith(":"):
                    # sometimes the last line hides a Fax number that should
                    # not be there, but shouldn't be left in work_remarks
                    # either:
                    #fi = string.find(line, "Fax:")
                    fi = line.find("Fax:")
                    if fi > -1:
                        line = line[:fi]
                    desc = desc + " " + line[1:].strip()
                else:
                    break
        return desc.strip()

    def find_locates(self):
        # get the lines we want, using an unorthodox filtering mechanism :-)
        lines = self.data.split("\n")
        while lines and not lines[0].startswith("Send To:"):
            del lines[0]
        lines = [li for li in lines
                 if li.startswith("Send To:") or li.startswith(" ")]

        locates = []
        for line in lines:
            wholeline = line
            line = line[:19]
            if line.startswith("Send"):
                line = line[len("Send To:"):]
            data = string.join(string.split(line), "")

            # find seq_number
            parts = wholeline.split()
            if wholeline.startswith("  "):
                seq_number = wholeline[19:40].strip() or None   # ken dit?
            else:
                try:
                    idx = parts.index("Seq")
                except ValueError:
                    seq_number = None
                else:
                    seq_number = parts[idx+2]

            locates.append((data, seq_number))

        # but wait, there's more!
        lines = string.split(self.data, "\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Explosives:"))
        idx2 = tools.findfirst(lines, lambda s: s.startswith("Send To:"))
        if idx1 > -1 and idx2 > -1:
            for i in range(idx1+1, idx2):
                parts = lines[i].split()
                parts = [(part, None) for part in parts]
                locates.extend(parts)

        # since we get locates from two places now, chances are there are
        # duplicates... weed them out
        locates = tools.remove_duplicates(locates)

        #return map(locate.Locate, locates)
        return [locate.Locate(name, seq_number=seq_number)
                for (name, seq_number) in locates
                if name.strip()]

    #
    # lat/long parsing

    _re_coord = R("^Ex\. Coord:(.*)$", re.MULTILINE)
    _re_long1 = R("NW.*?Lon: *([-0-9. ]+) *SE")
    _re_long2 = R("SE Lat.*?Lon: *([-0-9. ]+)")
    _re_lat1 = R("NW Lat: *([0-9. ]+)Lon:")
    _re_lat2 = R("SE Lat: *([0-9. ]+)Lon:")

    def _find_longlat(self):
        m = self._re_coord.search(self.data)
        if m:
            line = m.group(1).strip()
            return line
        return ""

    def find_work_long(self):
        line = self._find_longlat()
        if line:
            try:
                # Decimal degrees
                return extract_lat_long_1(self._re_long1,self._re_long2,line)
            except:
                # d/m/s
                return extract_lat_long_2(self._re_long1,self._re_long2,line)
        return 0

    def find_work_lat(self):
        line = self._find_longlat()
        if line:
            try:
                # Decimal degrees
                return extract_lat_long_1(self._re_lat1,self._re_lat2,line)
            except:
                # d/m/s
                return extract_lat_long_2(self._re_lat1,self._re_lat2,line)
        return 0

def extract_lat_long_1(latlong_re_1, latlong_re_2, line):
    m1 = latlong_re_1.search(line)
    if m1:
        long1 = decimal.Decimal(m1.group(1).strip()).quantize(
                decimal.Decimal("0.00001"))
        m2 = latlong_re_2.search(line)
        if m2:
            long2 = decimal.Decimal(m2.group(1).strip()).quantize(
                    decimal.Decimal("0.00001"))
            avg_long = (long1 + long2) / 2
            return float(str(avg_long))
        else:
            return float(str(long1))
    raise ValueError

def extract_lat_long_2(latlong_re_1,latlong_re_2,line):
    m1 = latlong_re_1.search(line)
    if m1:
        parts = m1.group(1).split() + [0, 0, 0]
        deg1, min1, sec1 = map(tools.safeint, parts[:3])
        m2 = latlong_re_2.search(line)
        if m2:
            parts = m2.group(1).split() + [0, 0, 0]
            deg2, min2, sec2 = map(tools.safeint, parts[:3])
            if deg1 < 0: # Longitude
                factor = -1.0
            else:
                factor = 1.0
            latlong1 = deg1 + factor*min1 / 60.0 + factor*sec1 / (60.0*60.0)
            latlong2 = deg2 + factor*min2 / 60.0 + factor*sec2 / (60.0*60.0)
            avg_latlong = (latlong1 + latlong2) / 2
            return avg_latlong
    return 0

class SummaryParser(BaseSummaryParser):

    prepend_zeroes = 8  # when > 0, prepend 0s to ticket number until length
                        # reached
    re_split = R("[ *]+")

    def has_header(self, data):
        return "DAILY AUDIT OF TICKETS SENT ON" in data

    def has_footer(self, data):
        rfooter = R("^Total Tickets: \d+ *$")
        return not not rfooter.search(data)

    def read(self, data):
        tickets = []
        re_start = R("-{5} -{12}")
        start = re_start.search(data).end() + 1
        re_end = R("\* indicates ticket # is repeated")
        end = re_end.search(data).start() - 1
        rline = R("^\s+\d+\**\s*\S+")
        for line in data[start:end].split("\n"):
            if rline.search(line):
                parts = line.split("|")
                parts = filter(string.strip, parts)
                for part in parts:
                    try:
                        number, ticket_number = part.split()
                    except ValueError:
                        spam = filter(None, self.re_split.split(part))
                        number, ticket_number = spam[:2]
                    if number.endswith("*"):
                        number = number[:-1]
                    ticket_number, indic = ticket_number.split("-")
                    while len(ticket_number) < self.__class__.prepend_zeroes:
                        ticket_number = "0" + ticket_number
                    # we don't have a time, so set 00:00
                    detail = summarydetail.SummaryDetail(
                             [number, ticket_number, "00:00", "", indic])
                    tickets.append(detail)
        return tickets

    def get_client_code(self, data):
        m = R("^Receiving Terminal: (.*)$").search(data)
        client_code = m and m.group(1) or ""
        if client_code:
            parts = client_code.split()
            client_code = string.join(parts, "")
        return client_code

    def get_summary_date(self, data):
        m = R("^DAILY AUDIT OF TICKETS SENT ON +(\S+)").search(data)
        d = m and m.group(1) or ""
        if d:
            nums = map(int, d.split("/"))
            nums = [("%02d" % n) for n in nums]
            d = string.join(nums, "/")
            d = tools.isodate(d)
        return d

    def get_expected_tickets(self, data):
        m = R("^Total Tickets: (\d+)").search(data)
        total = m and int(m.group(1)) or 0
        return total

