import string
import re
from parsers import BaseParser
from re_shortcut import R
import tools
import locate

import Ohio

class Parser(BaseParser):
    re_transmit_date = R("Dig.*xmit: *([0-9/]+) ([0-9:]+) (AM|PM|am|pm)")
    re_call_date = re_transmit_date
    re_ticket_number = R("^Ticket: +(\d+-\d+-\d+)-\d+")
    re_ticket_type = R("^Ticket.*Type: +(.*?) *(Previous|$)")
    re_work_state = R("^State : +(\S+)")
    re_work_county = R("^State.*County: +(.*?) *Place")
    re_work_city = R("^State.*Place: +(.*?) *$")
    #re_work_address_number_2 = R("^Addr : +(.*?) -")
    re_work_address_number = R("^Addr +: +(.*?) +Name:")
    re_work_address_street = R("^Addr.*Name: (.*?) *$")
    re_work_cross = R("^Cross : - Name: +(.*?) *$")
    re_work_date = R("^Begin: +([0-9/]+) ([0-9:]+) (AM|PM|am|pm)")
    re_work_type = R("^Work: +(.*?) *Pre Markings")
    re_caller_contact = R("^Contact: +(.*?) *Caller Type")
    re_con_name = R("^Company: +(.*?) *$")
    re_con_address = R("^Addr1: +(.*?) *$")
    re_con_city = R("^City: +(.*?) *State")
    re_con_state = R("^City.*State: +(..)")
    re_con_zip = R("^City.*Zip: +(.*?) *$")
    re_caller_phone = R("^Phone: +(.*?) *(Ext|Fax|$)")
    re_caller_fax = R("^Phone.*Fax: +(.*?) *(Cell|$)")
    re_caller_email = R("^Email: +(.*?) *$")
    re_company = R("^Working For: +(.*?) *$")

    f_transmit_date = lambda s, m: \
     s.isodate(m.group(1) + " " + m.group(2) + " " + m.group(3))
    f_work_date = f_transmit_date
    f_call_date = f_transmit_date

    _re_termid = R("Dig Request from OUPS for: +(\S+)")

    def find_locates(self):
        locates = []

        # find "Dig Request from OUPS for" section
        m = self._re_termid.search(self.data)
        if m:
            client_code = m.group(1)
            locates.append(locate.Locate(client_code))

        # find members section (not always available)
        lines = self.data.split('\n')
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Members:"))
        if idx1 == -1:
            return locates   # no member section?!
        idx2 = tools.findfirst(lines, lambda s: s.startswith("Lbps:"))
        if idx2 == -1 or idx2 < idx1:
            idx2 = tools.findfirst(lines, lambda s: s.startswith("eom")) # search until end of data

        mlines = lines[idx1:idx2]
        for line in mlines:
            if line.startswith("Members:"):
                line = line[8:].lstrip()
            else:
                line = line.lstrip()
            while line:
                chunk, line = line[:22], line[22:].lstrip()
                chunk = chunk.strip()
                parts = chunk.split('=')
                if parts:
                    termid = parts[0].strip()
                    locates.append(locate.Locate(termid))

        return locates

    def find_kind(self):
        if self.ticket.ticket_type.upper().find("SHORT NOTICE") > -1:
            return "EMERGENCY"
        else:
            return "NORMAL"

class SummaryParser(Ohio.SummaryParser):
    pass
