# GA3001.py

import Atlanta
import re

def R(regex, flags=0):
    return re.compile(regex, re.MULTILINE | re.IGNORECASE | flags)

class Parser(Atlanta.Parser):
    re_map_page = R("^Grids\s+:[ \t]+(\S+)")
    re_update_of = R("^Old Tkt: +(\S+)")

class SummaryParser(Atlanta.SummaryParser):
    pass
