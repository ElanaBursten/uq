# NashvilleKorterra2013.py

import string
import Nashville2013
from re_shortcut import R
import locate

from summaryparsers import BaseSummaryParser
import tools
import summarydetail
import date


class Parser(Nashville2013.Parser):
    pass

class SummaryParser(BaseSummaryParser):

    r_summary_date = R("FOR TICKETS SENT ON ([0-9/]+)")
    r_total = R("Total Sent\s*=\s*(\d+)")
    r_client_code = R("KORTERRA AUDIT ([^-]*)")

    DEFAULT_CLIENT_CODE = "FNV4"
    STRIP_NUMBER = 2  # number of characters we strip off the beginning of the
                      # ticket number

    def read(self, data):
        tickets = []
        collecting = 0
        lines = data.split("\n")
        for line in lines:
            if line.startswith("----") and not collecting:
                collecting = 1 # begin collecting
            elif (not line.strip()) and collecting:
                collecting = 0 # end collecting
                break
            elif ('No Tickets sent to this location' in line) and collecting:
                collecting = 0 # end collecting
                break
            elif collecting:
                try:
                    ticket_number, seq_number = line.split()[:2]
                except (IndexError, ValueError):
                    continue

                # strip digits if necessary
                ticket_number = ticket_number[self.STRIP_NUMBER:]

                detail = summarydetail.SummaryDetail([
                         seq_number, ticket_number, "", "", ""])
                tickets.append(detail)

        return tickets

    def get_client_code(self, data):
        m = self.r_client_code.search(data)
        if m:
            return m.group(1)
        else:
            return self.DEFAULT_CLIENT_CODE

    def get_summary_date(self, data):
        m = self.r_summary_date.search(data)
        if m:
            d = tools.isodate(m.group(1))
            return date.Date(d).isodate()
        else:
            return date.Date().isodate()    # take today, I guess

    def get_expected_tickets(self, data):
        m = self.r_total.search(data)
        if m:
            try:
                total = int(m.group(1))
            except ValueError:
                return 0
            else:
                return total
        else:
            return 0

    def has_header(self, data):
        return bool(self.r_summary_date.search(data))

    def has_footer(self, data):
        return bool(self.r_total.search(data))

