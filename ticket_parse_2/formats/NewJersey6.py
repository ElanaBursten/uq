# NewJersey6.py

import NewJersey2010
from summaryparsers import BaseSummaryParser, SummaryError
from tools import R
import re
import summarydetail

class Parser(NewJersey2010.Parser):
    pass

class SummaryParser(BaseSummaryParser):
    RECOG_HEADER = "THE FOLLOWING MESSAGE NUMBERS WERE TRANSMITTED"

    re_footer = R("^TOTAL = (\d+)")
    re_header_date = R("THE FOLLOWING MESSAGE NUMBERS WERE TRANSMITTED TO YOU TODAY.\s+([0-9/]+)", re.DOTALL|re.MULTILINE)
    re_client_code = R("STATION: (.*?) *$")
    prefix = "NJ"

    def has_header(self, data):
        return self.RECOG_HEADER in data

    def has_footer(self, data):
        return bool(self.re_footer.search(data))

    def get_expected_tickets(self, data):
        m = self.re_footer.search(data)
        if m is not None:
            return int(m.group(1))
        else:
            return 0

    def get_summary_date(self, data):
        m = self.re_header_date.search(data)
        if m:
            s = m.group(1)
            parts = map(int, s.split("/"))
            return "%04d-%02d-%02d 00:00:00" % (parts[2], parts[0], parts[1])
        else:
            raise SummaryError("No date found on audit")

    def get_client_code(self, data):
        m = self.re_client_code.search(data)
        if m:
            return m.group(1).strip()
        else:
            return "?"

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        status = 0
        for line in lines:
            if status == 0 and line.startswith("STATE"):
                status = 1
                continue
            if status == 1 and line.startswith("TOTAL ="):
                break
            if status:
                if line.strip():
                    parts = line.split()
                    raw_ticketno, seqno = parts[:2]
                    ticket_number = raw_ticketno[len(self.prefix):]
                    detail = summarydetail.SummaryDetail([seqno,
                             ticket_number, "00:00", "", ""])
                    tickets.append(detail)

        return tickets


