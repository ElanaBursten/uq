# Illinois.py
import string
from parsers import BaseParser
from re_shortcut import R
from summaryparsers import BaseSummaryParser
import summarydetail
import locate
import tools
import re

class Parser(BaseParser):

    re_transmit_date = R("[A-Z0-9]+ \d+ JULIE ([0-9/]+) ([0-9:]+)")
    re_ticket_type = R("[A-Z0-9]+ \d+ JULIE [0-9/]+ [0-9:]+ \d+ *(.*?) *$")
    re_ticket_number = R("^Dig No : (\d+)")
    re_priority = R("^Dig No.*?Priority: (\S+)")
    re_work_date = R("Digstart: ([0-9/]+) *Time: ([0-9:]+)")

    re_call_date = R("Rcvd +: ([0-9/]+) ([0-9:]+)")
    re_operator = R("Operator: (\S+)")
    re_revision = R("Rev : (\S+)")
    re_work_county = R("^County : *(.*?) *Place")
    re_work_city = R("^County.*?Place +: *(.*?) *$")
    re_work_address_number = R("^Address: (.*?) *Street")
    re_work_address_street = R("^Address.*?Street +: *(.*?) *$")
    re_work_cross = R("^Cross +: *(.*?) *Subdivision")
    re_work_subdivision = R("Subdivision: *(.*?) *$")

    re_work_type = R("Type +: *(.*?) *$")
    re_company = R("DoneFor: *(.*?) *$")
    re_con_name = R("^Firm +: *(.*?) *Caller")
    re_caller = R("Caller: (.*?) *$")
    re_con_address = R("Firm.*?Address: (.*?) *$", re.DOTALL)
    re_con_city = R("^City,St: (.*?),")
    re_con_state = R("^City,St: .*?, *(\S+)")
    re_caller_phone = R("^Phone +: (.*?) *Ext")
    re_caller_fax = R("Fax: (.*?) *$")
    re_con_zip = R("Zip +: (.*?) *$")
    re_map_page = R("Grids  : (\S+)")

    f_transmit_date = lambda s, m: s.isodate(m.group(1) + " " + m.group(2))
    f_work_date = f_transmit_date
    f_call_date = f_transmit_date

    find_work_state = lambda s: "IL"

    # special treatment for:
    # - grids

    def find_work_description(self):
        # find contents of 'Extent' section
        re_work_description = R("^Extent : (.*?) *$")
        m = re_work_description.search(self.data)
        if m:
            ext_desc = m.group(1)
        else:
            ext_desc = ""

        # find contents of 'Locat' section
        lines = self.data.split("\n")
        idx = tools.findfirst(lines, lambda s: s.startswith("Locat  :"))
        idx2 = tools.findfirst(lines[idx+1:],
         lambda s: not s.startswith("       :"))
        desc = ""
        for i in range(idx, idx+idx2+1):
            desc = desc + lines[i][8:].strip() + " "

        # glue them together
        return desc + ext_desc

    def find_work_remarks(self):
        lines = self.data.split("\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Remarks:"))
        idx2 = tools.findfirst(lines[idx1+1:], lambda s: not s.strip())
        remarks = ""
        for i in range(idx1, idx2+idx1+1):
            remarks = remarks + lines[i][9:] + " "
        return remarks.strip()

    def find_locates(self):
        lines = self.data.split("\n") + [""]
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Members:"))
        idx2 = tools.findfirst(lines[idx1+1:],
         lambda s: not s.startswith("Members:"))
        locates = []
        for i in range(idx1, idx2+idx1+1):
            chunk = lines[i][9:]
            names = chunk.split()
            locates.extend(names)

        locates = map(locate.Locate, locates)
        return locates

class SummaryParser(BaseSummaryParser):
    RECOG_HEADER_2 = "ATTENTION JULIE MEMBERS : END OF DAY AUDIT"

    re_footer = R("^\s+TOTAL +- (\d+) *$")
    re_recog_header_1 = R("\s+\*EOD\* JULIE")

    def has_header(self, data):
        return string.find(data, self.RECOG_HEADER_2) > -1

    def has_footer(self, data):
        return not not self.re_footer.search(data)

    def get_client_code(self, data):
        client = ""  # default value, empty
        lines = string.split(data, "\n")
        for line in lines:
            m = self.re_recog_header_1.search(line)
            if m:
                parts = string.split(line)
                client = parts[0]
                break
        return client

    def get_summary_date(self, data):
        date = ""  # default values, empty
        lines = string.split(data, "\n")
        for line in lines:
            m = self.re_recog_header_1.search(line)
            if m:
                parts = string.split(line)
                date1 = parts[3] # + " " + parts[4]
                date = tools.isodate(date1)
                date = (Date(date) - 1).isodate()
                break
        return date

    def get_expected_tickets(self, data):
        m = self.re_footer.search(data)
        if m:
            expected_tickets = int(m.group(1))
        else:
            expected_tickets = 0
        return expected_tickets

    def read(self, data):
        tickets = []
        for line in string.split(data, "\n"):
            if line and line[0] in "0123456789":
                parts = string.split(line, "/")
                for part in parts:
                    part = string.strip(part)
                    ticket_number = part[:7]
                    number = part[8:13]
                    info = part[7]
                    detail = summarydetail.SummaryDetail(
                             [number, ticket_number, "00:00", "", info])
                    tickets.append(detail)
                    # note: these summaries don't have a time!

        return tickets
