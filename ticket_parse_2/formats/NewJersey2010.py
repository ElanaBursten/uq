# NewJersey2010.py

import os
import re
import string
import sys
#

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lib'))

from parsers import BaseParser
import Lanham # for SummaryParser
from re_shortcut import R
import date
import locate
import pyproj
import summarydetail
import ticket
import tools

class Parser(BaseParser):
    only_one = BaseParser.only_one[:]; only_one.remove("ticket_number")

    re_serial_number = R("(GSUPLS\d+)")
    re_transmit_date = R("Transmit:\s+Date:\s+([0-9/]+)\s+At:\s+(\d\d:\d\d)")
    re_ticket_number = R("Request No.:\s+([0-9]+)")
    re_update_of = R("Request No.:\s*\S+\s*Of Request No.:?\s+([0-9]+)")
    re_ticket_type = R("^\*\*\*(.*?)\*\*\* Request")

    # call_date isn't known, so for now, I assume call_date == transmit_date
    re_call_date = re_transmit_date

    re_work_county = R("County:[ \t]*(.*?)[ \t]+Muni")
    re_work_city = R("Municipality: (.*?)[ \t]*$")
    re_work_subdivision = R("Subdivision/Community:[ \t]*(.*?)[ \t]*$")
    re_work_address_street = R("Street:[ \t]+(.*?)[ \t]*$")
    re_work_cross = R("Nearest Intersection:[ \t]+(.*?)[ \t]*$")
    re_work_type = R("Type of Work:\s+(.*?)\s*$")
    re_work_description = R("Extent of Work:\s+(.*?)\s*Remarks:",
     re.DOTALL|re.M) # may span multiple lines

    re_work_date = R("Start Date/Time:\s+([0-9/]+)\s+A[tT]\s+(\d\d:\d\d)")

    re_company = R("Working For:\s+(.*?)\s*$")
    # other fields in this block are ignored (for now, anyway)

    re_caller = R("Caller:\s+(.*?)\s*(Title|$)")
    re_caller_phone = R("Caller.*?\n\s*Phone:\s+([0-9()-]+)", re.DOTALL)

    re_con_name = R("Excavator:\s+(.*?)\s*$")
    re_con_address = R("Excavator.*Address:\s+(.*?)\s*$", re.DOTALL)
    re_con_city = R("Excavator.*City:\s+(.*?),", re.DOTALL)
    re_con_state = R("Excavator.*City:\s+.*?,\s+(..)\s+", re.DOTALL)
    re_con_zip = R("Excavator.*City:.*?,\s+..\s+(.*?)\s*$", re.DOTALL)
    re_caller_fax = R("Excavator.*?Fax: +([0-9-()]+)", re.DOTALL)
    re_caller_cellular = R("Excavator.*?Cellular: +([0-9-()]+)", re.DOTALL)
    re_caller_email = R("Excavator.*?Email:\s+(.*?)\s*$", re.DOTALL)

    re_respond_date = re_work_date
    
    _re_lat = R("^Lat/Lon:.*?Lat:[ \t]*([\d\. \t]*)")
    _re_long = R("^Lat/Lon:.*?Lon:[ \t]*(-?[\d\. \t]*)")
    _re_utm_datum = R("^Lat/Lon:[ \t]*Nad:[ \t]*(27|83)[ \t]*Lat:")
    _re_utm_zone = R("^Lat/Lon:.*?Zone:[ \t]*(\d+)")

    def f_transmit_date(self, match):
        return self.isodate(match.group(1) + " " + match.group(2))
    f_call_date = f_respond_date = f_work_date = f_transmit_date

    # Mantis 2832: parse expiration date as legal_good_thru
    re_legal_good_thru = R("Expiration Date: *(\S+)")
    def f_legal_good_thru(self, match):
        return self.isodate(match.group(1))

    f_ticket_type = lambda s, m: \
     string.join(string.split(m.group(1)), "")

    def find_work_state(self): return "NJ"

    def find_work_remarks(self):
        lines = self.data.split("\n")
        remarks = ""
        for line in lines:
            if line.strip().startswith("Remarks:"):
                remarks = "!"
                continue
            if remarks:
                if not line.strip():
                    break
                if line.startswith(" "):    # indentation
                    remarks = remarks + line.strip() + " "
                else:
                    break   # end of it
        return remarks[1:]  # remove "!"

    def find_locates(self):
        """ Get locates (term ids) on ticket. Normally we use only one
            term id, in the header "CDC = ...". However, if "CDC = XXX",
            then we extract term ids from the "Operators Notified" section. """
        re_loc = R("^\s*New Jersey One Call.*?CDC\s+=\s+(\S+)", re.DOTALL)
        m = re_loc.search(self.data)
        if m:
            loc = m.group(1)
            if loc == "XXX":
                # get complete locates list, a bit like Atlanta
                reloc2 = R("\S+=/")
                ok = 0
                locates = ["XXX"]
                for line in self.data.split("\n"):
                    if ok:
                        if (not line.strip()) or ('=' not in line):
                            break # done processing this section
                        for i in range(2):
                            s = line[:40].strip()
                            if '=' in s:
                                loc_name = s.split()[0]
                                locates.append(loc_name)
                            line = line[40:]
                    else:
                        if line.startswith("Operators Notified:"):
                            ok = 1
                return map(locate.Locate, locates)
            else:
                # return only one locate
                return [locate.Locate(loc)]
        else:
            return []

    def sanitycheck_before(self):
        # the 'End Request' line must always be present, or the ticket is
        # probably incomplete
        BaseParser.sanitycheck_before(self)
        if self.data.find("End Request") == -1:
            raise ticketparser.TicketError, "No 'End Request' found"

    def _work_coordinates(self):
        ellipsoids = {'NAD83': 'GRS80', 'NAD27': 'clrk66'}

        def decimal_coord(s_coord):
            if not s_coord:
                return ticket.LAT_LONG_DEFAULT
            parts = s_coord.split() + ["0", "0", "0"]
            deg, min, sec = [float(s) for s in parts[:3]]
            coord = abs(deg) + min/60.0 + sec/3600.0 
            if deg < 0: coord = -coord
            return coord

        m_xcoord = self._re_lat.search(self.data)
        if m_xcoord is not None:
            s_xcoord = m_xcoord.group(1).strip()
        else:
            s_xcoord = ''

        m_ycoord = self._re_long.search(self.data)
        if m_ycoord is not None:
            s_ycoord = m_ycoord.group(1).strip()
        else:
            s_ycoord = ''

        m_datum = self._re_utm_datum.search(self.data)
        if m_datum is not None:
            datum = 'NAD' + m_datum.group(1)
        else:
            datum = 'NAD83' # todo(dan) change to WGS84?

        m_zone = self._re_utm_zone.search(self.data)
        if m_zone is not None:
            zone = int(m_zone.group(1))

        if m_zone and zone:
            if s_xcoord and s_ycoord:
                p = pyproj.Proj(proj='utm', zone=zone, ellps=ellipsoids[datum],
                 datum=datum)
                long, lat = p(float(s_ycoord), float(s_xcoord), inverse=True)
                return lat, long
            else:
                return ticket.LAT_LONG_DEFAULT, ticket.LAT_LONG_DEFAULT                
        else:
            return decimal_coord(s_xcoord), decimal_coord(s_ycoord)

    def post_process(self):
        # try to extract street number from address
        parts = self.ticket.work_address_street.split()
        if parts and parts[0][0] in "0123456789":
            self.ticket.work_address_street = string.join(parts[1:], " ")
            self.ticket.work_address_number = parts[0]

        self.ticket.work_lat, self.ticket.work_long = \
         self._work_coordinates()

        # set map_page to qtrmin value
        try:
            lat = float(self.ticket.work_lat)
            long = float(self.ticket.work_long)
        except:
            pass
        else:
            if 90 > lat > 0 and -180 < long < 0:
                self.ticket.map_page = tools.decimal2grid(lat, long)

        BaseParser.post_process(self)

class SummaryParser(Lanham.SummaryParser):
    # supposedly these will not be multipart anymore.

    prepend_zeroes = 9

