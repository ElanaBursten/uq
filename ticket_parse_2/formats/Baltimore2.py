# Baltimore2.py

import Baltimore, Delaware3
from tools import R
import string

class Parser(Baltimore.Parser):

    # necessary for Mantis #2893?
    re_serial_number = R("KORTERRA JOB UTILIQUEST\S+\s+(\S+)")

    def post_process(self):
        # remove 'MD' prefix from serial number
        while self.ticket.serial_number \
        and self.ticket.serial_number[0] in string.uppercase:
            self.ticket.serial_number = self.ticket.serial_number[1:]

        Baltimore.Parser.post_process(self)

class SummaryParser(Delaware3.SummaryParser):
    def get_client_code(self, data):
        return "FMB2"


