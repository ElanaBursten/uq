# __init__.py

#
# a few auxiliary functions to get specific classes

def get_parser(format):
    """ Get a Parser class for the given format in the 'formats' package.
        <format> is a string like "Atlanta", "Dallas1", etc.  It should not
        end with "Parser". """
    mod = __import__(format, globals(), locals())
    # NOTE: globals() and locals() are necessary here, otherwise it won't
    # find modules in this package!  E.g. __import__('LID1') would not work.
    return mod.Parser

def get_summaryparser(name):
    mod = __import__(name, globals(), locals())
    return mod.SummaryParser

def get_work_order_parser(name):
    mod = __import__(name, globals(), locals())
    return mod.WorkOrderParser

def get_work_order_audit_parser(name):
    mod = __import__(name, globals(), locals())
    return mod.WorkOrderAuditParser

