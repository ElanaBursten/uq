# GA3004b.py
# Tickets are like GA3004 and GA3004a, but audits have a special type
# ("SENTRi").

import GA3004
import audit_sentri

class Parser(GA3004.Parser):
    call_center = 'GA3004b'
    # derives post-processing from GA3004 as well (Mantis #2845)

class SummaryParser(audit_sentri.SummaryParser):
    pass


