# AlaskaNew.py

from parsers import BaseParser
from re_shortcut import R
import Alaska
import tools
import string
import locate
import ticketparser

class Parser(BaseParser):

    # allow spaces in term ids
    VALID_LOCATE_CHARS = BaseParser.VALID_LOCATE_CHARS + " "

    re_ticket_number = R("NOTICE OF INTENT TO EXCAVATE\s+Ticket No:\s+(\S+)")
    re_ticket_type = R("Header Code:\s+(.*?) *$")
    re_transmit_date = R(
     "^Transmit Date: +([0-9/]+) +Time: +([0-9:]+) (AM|PM)")
    re_call_date = R(
     "^Original Call Date: +([0-9/]+) +Time: +([0-9:]+) (AM|PM)")
    re_work_date = R(
     "^Locate By Date: +([0-9/]+) +Time: +([0-9:]+) (AM|PM)")
    re_con_name = R("Company:\s+(.*?) *$")
    re_caller_contact = R("^Site Contact:\s+(.*?)\s+Site Ph")
    re_caller_phone = R("Phone:\s+(\S+)")
    re_caller_fax = R("Fax:\s+(\S+)")
    re_caller_email = R("Email:\s+(\S+)")
    re_work_city = R("Locate City:\s+(.*?) *$")
    re_work_address_number = R("^Address: *(\S+) *Unit:")
    re_work_address_street = R("^Street:\s+(.*?) *$")
    re_work_cross = R("^Closest Intx:\s+(.*?) *$")
    re_map_page = R("^MAP PAGE:\s+(.*?) *(MAP PAGE|$)")
    re_work_type = R("^Type of Work:\s+(.*?) *$")

    re_company = re_con_name
    # this field doesn't appear on the ticket, so I duplicate con_name

    def f_transmit_date(self, match):
        return tools.isodate(string.join(match.groups(), " "))
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date

    def find_work_state(self):
        return "AK"

    def find_locates(self):
        locates = []
        lines = string.split(self.data, "\n")
        idx1 = tools.findfirst(lines, lambda s: s.startswith("Members Notified:"))
        if idx1 > -1:
            for i in range(idx1+1, len(lines)):
                line = lines[i]
                if not line.strip():
                    break
                while line.strip() and not line.startswith("  "):
                    chunk, line = line[:17], line[17:]
                    chunk = chunk.replace("*", "").strip() # no asterisks
                    if chunk:
                        locates.append(chunk)
                else:
                    continue

        # fix name longer than 10 characters (not a LocInc client)
        for i, name in enumerate(locates):
            if name == "GCI - FIBER":
                locates[i] = "GCI-FIBER"

        return [locate.Locate(x) for x in locates]

    s_work_description = "Dig Info:"
    s_work_remarks = "Remarks:"

    def find_work_description(self):
        lines = self.data.split("\n")
        desc = ""
        for line in lines:
            if line.startswith(self.s_work_description):
                desc = line[len(self.s_work_description):].strip()
                continue
            if desc:
                if line.startswith("   "):
                    desc = desc + " " + line[1:].strip()
                else:
                    break
        return desc.strip()

    def find_work_remarks(self):
        lines = self.data.split("\n")
        desc = ""
        for line in lines:
            if line.startswith(self.s_work_remarks):
                desc = line[len(self.s_work_remarks):].strip()
                continue
            if desc:
                if line.startswith("   "):
                    desc = desc + " " + line[1:].strip()
                else:
                    break
        return desc.strip()

    def post_process(self):
        self.ticket.work_cross = tools.singlespaced(self.ticket.work_cross)
        BaseParser.post_process(self)

class SummaryParser(Alaska.SummaryParser):
    pass
