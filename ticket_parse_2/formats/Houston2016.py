# Houston2016.py

import re
#
import Lanham
import locate
import parsers
import quadrilateral
import re_tools2
import tools

from re_tools2 import header_re as hdr
from re_tools2 import standard_line_re as line_re
from re_tools2 import ALL_0_N, BLANKS_0_N, BLANKS_1_N,\
  DATE_TIME_12_SPLIT_CAPTURED, END, FLOAT_CAPTURED, MULTILINE, REST_OF_LINE,\
  REST_OF_LINE_CAPTURED, START

class Parser(parsers.BaseParser):
    re_ticket_type = line_re('', 'NOTICE OF INTENT TO EXCAVATE', '')
    re_ticket_number = line_re('', hdr('Ticket No'), [hdr('Orig. Tkt'), ''])
    re_update_of = line_re(hdr('Ticket No'), hdr('Orig. Tkt'), '')
    re_transmit_date = line_re('', hdr('Transmit +Date'), hdr('Type'),
                               value=DATE_TIME_12_SPLIT_CAPTURED)
    re_channel = line_re(hdr('Transmit +Date'), hdr('Type'), '')
    re_call_date = line_re('', hdr('Original Call Date'), '',
                           value=DATE_TIME_12_SPLIT_CAPTURED)
    re_work_date = line_re('', hdr('Work to Begin Date'), '',
                           value=DATE_TIME_12_SPLIT_CAPTURED)
    re_work_county = line_re('', hdr('County'), hdr('State'))
    re_work_state = line_re(hdr('County'), hdr('State'), '')
    re_work_city = line_re('', hdr('Place'), '')
    re_work_address_number = line_re('', hdr('Address'), hdr('Street'))
    re_work_address_street = line_re(hdr('Address'), hdr('Street'), '')
    re_work_cross = line_re('', hdr('Nearest Intersecting Street'), '')
    re_explosives = line_re('', hdr('Explosives'), hdr('Duration'))
    re_duration = line_re(hdr('Explosives'), hdr('Duration'), '')
    re_work_type = line_re('', hdr('Type of Work'), '')
    re_company = line_re('', hdr('Work Done For'), '')
    re_con_name = line_re('', hdr('Company'), hdr('Type'))
    re_con_type = line_re(hdr('Company'), hdr('Type'), '')
    re_caller = line_re('', hdr('Contact Name'), hdr('Phone'))
    re_caller_contact = re_caller
    re_caller_phone = line_re(hdr('Contact Name'), hdr('Phone'),
                              [hdr('Ext'), ''])
    re_caller_altcontact = line_re('', hdr('Alt. Contact'), hdr('Phone'))
    re_caller_altphone = line_re(hdr('Alt. Contact'), hdr('Phone'),
                                 [hdr('Ext'), ''])
    re_caller_fax = line_re(hdr('Best Time'), hdr('Fax'), [hdr('Ext'), ''])
    re_caller_email = line_re('', hdr('Email'), '')

    f_transmit_date = re_tools2.isodate_from_3_groups
    f_call_date = f_work_date = f_transmit_date

    def find_locates(self):
        locates = []
        match = line_re('', hdr('Send To'), hdr('Seq No')).search(self.data)
        if match:
            locates.append(locate.Locate(match.group(1)))
        return locates

    def find_work_description(self):
        return re_tools2.find_standard_multiline('Extent of Work', self.data)

    def find_work_remarks(self):
        return re_tools2.find_standard_multiline('Remarks', self.data)

    def set_con_address(self, data):
        match = line_re('', hdr('Address'), '').search(data)
        if match:
            self.ticket.con_address = match.group(1)

    _re_address_line2 = re.compile(MULTILINE + START + hdr('Address') +
                                   REST_OF_LINE + r'\n' + REST_OF_LINE_CAPTURED)

    def set_con_city_state_zip(self, data):
        match = self._re_address_line2.search(data)
        if match:
            city, state, zip = re_tools2.city_state_zip_from_line(
              match.group(1))
            if city:
                self.ticket.con_city = city
            if state:
                self.ticket.con_state = state
            if zip:
                self.ticket.con_zip = zip

    _re_poly_1 = line_re('', hdr('Poly 1'), '')
    _re_coordinates = re.compile(
      MULTILINE + hdr('Lat') + BLANKS_0_N + FLOAT_CAPTURED + BLANKS_1_N +
      hdr('Lon') + BLANKS_0_N + FLOAT_CAPTURED)

    def set_work_lat_long(self):
        match = self._re_poly_1.search(self.data)
        if not match:
            return

        matches = self._re_coordinates.findall(match.group(1))

        coordinates = []
        for match in matches:
            try:
                lat = float(match[0])
                lon = float(match[1])
            except ValueError:
                continue
            coordinates.append((lat, lon))

        self.ticket.work_lat, self.ticket.work_long = \
          quadrilateral.centroid_from_vertices(coordinates)

    def post_process(self):
        match = re_tools2.lines_from_re(hdr('Company')).search(self.data)
        if match:
            self.set_con_address(match.group())
            self.set_con_city_state_zip(match.group())

        self.set_work_lat_long()
        if self.ticket.work_lat > 0 and self.ticket.work_long < 0:
            self.ticket.map_page = tools.decimal2grid(self.ticket.work_lat,
                                                      self.ticket.work_long)
        parsers.BaseParser.post_process(self)

class SummaryParser(Lanham.SummaryParser):
    pass
