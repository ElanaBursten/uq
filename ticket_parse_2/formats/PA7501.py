# PA7501.py

import re
import string
#
from re_shortcut import R
from static_tables import ticket as ticket_table
import date
import locate
import quadrilateral
import re_tools2
import tools
#
import Pennsylvania

class Parser(Pennsylvania.Parser):

    re_company = R("(?:Project Owner|Owner/Work Being Done for|Work Being Done For|Work For)--\[(.*?)\]")
    re_ward = R("Ward--\[(\S+)\]")
    re_work_remarks = R("Remarks--\s*\[(.*?)\]", re.DOTALL)
    re_serial_number = R("Serial Number--\[(.*?)\]")
    re_respond_date = R("Response Due Date--\[(.*?)\]")
    re_work_description = R(
      re_tools2.START + "Location Information--\s*\[(.*?)\]\s*$",
      re.DOTALL|re.MULTILINE)

    line_re = re_tools2.bracket_line_re
    ANY = re_tools2.ANY_BRACKETED
    ANY_CAP = re_tools2.ANY_CAPTURED_BRACKETED

    re_revision = line_re('', 'Serial Number--', None,
                          value=(ANY + '-' + ANY_CAP))
    re_channel = line_re('Serial Number--', 'Channel#--', '',
                         value=(ANY_CAP + ANY_CAP))  
    re_work_subdivision = line_re('', 'Subdivision--',
                                  ['Site Marked in White--', ''])
    re_duration = line_re('Scheduled Excavation Date--', 'Duration--', '')

    # todo(dan) Should these be fixed for older formats (see 2010 test tickets)?
    re_caller_email = line_re('', 'Email--', '') # todo(dan) can after be None?
    re_caller_altphone = line_re('', 'Contact Phone--', 'Contact Ext--')

    # todo(dan) Change this to use line_re?
    re_work_date = R("(?:Proposed Dig|Scheduled Excavation) Date--\[(.*?)\](?: Dig Time--\[(.*?)\])?")

    f_channel = lambda s, m: m.group(1) + " " + m.group(2)

    def f_work_date(self, match):
        """ Date is in format like 19-MAR-02, and needs to be converted. """
        d, t = match.group(1), match.group(2)
        date_parts = d.split("-")
        if len(date_parts) <> 3:
            return None
        dd, dm, dy = date_parts
        dm = date.get_month_number(dm) # 1, 2, ..., 12
        if not dm:
            raise ParserError, "Unknown month '%s'" % (dm)
        newdate = "20%s-%02d-%s" % (dy, dm, dd)
        if len(t) == 0:
            # The time was left blank
            t = "0000"
        newtime = t[:2] + ":" + t[2:] + ":00"
        return newdate + " " + newtime

    def f_respond_date(self, match):
        """ Date is in format like 19-MAR-02, and needs to be converted. """
        d = match.group(1)
        dd, dm, dy = d.split("-")
        dm = date.get_month_number(dm) # 1, 2, ..., 12
        if not dm:
            raise ParserError, "Unknown month '%s'" % (dm)
        newdate = "20%s-%02d-%s" % (dy, dm, dd)
        t = "0000"
        newtime = t[:2] + ":" + t[2:] + ":00"
        return newdate + " " + newtime

    def f_work_address_street(self,match):
        # XXX get rid of direct ticket_table access >=(
        if len(match.group(1)) > ticket_table[16][1].width:
            return match.group(1)[:ticket_table[16][1].width]
        else:
            return match.group(1)

    def find_explosives(self):
        found = self.data.upper().find("BLAST") > -1
        return found and "Y" or "N"

    G_LOCATES = ['KC', 'KD', 'KE', 'KF', 'KG']

    def post_process(self):
        Pennsylvania.Parser.post_process(self)
        # ticket number may appear twice on this format, it's not an error
        self.dup_fields = [x for x in self.dup_fields
                           if x[0] != 'ticket_number']
        self.ticket._dup_fields = self.dup_fields

        self.ticket.work_description = tools.singlespaced(self.ticket.work_description)

        # add 'G' locates
        locate_names = [loc.client_code for loc in self.ticket.locates]
        for glocname in self.G_LOCATES:
            if glocname in locate_names \
            and glocname + 'G' not in locate_names:
                newloc = locate.Locate(glocname + 'G')
                self.ticket.locates.append(newloc)

    def work_lat_long(self):
        """
        Determine the work latitude and longitude
        Expecting something like this:
        Mapped Type--[P] Mapped Lat/Lon--
        [40.256476/-75.601436,40.253921/-75.597203,40.256808/-75.594354,
         40.258554/-75.599335]

        or
        Mapped Type--[L] Mapped Lat/Lon--
        [40.540487/-79.773561,40.538909/-79.775242]
        or (new in Mantis #2808):

        Location Information--
             []
             Caller Lat/Lon--[40.123400/-79.123400,40.432100/-79.432100]
        """

        re_latlongs = [
          R("Mapped Type--\[\S\] Mapped Lat/Lon--\n\s*\[(\S*)\n\s*(\S*)\]"),
          R("Mapped Type--\[\S\] Mapped Lat/Lon--\n\s*\[(\S*)\]"),
          R("Caller Lat/Lon--\[(.*?)\]"),
        ]

        for re_latlong in re_latlongs:
            m = re_latlong.search(self.data)
            if m is None:
                continue
            try:
                pairs = string.join(m.groups(), ',').split(',')
                pairs = [item for item in pairs if len(item) != 0]
                lat_longs = [(float(x), float(y))
                             for x, y in [pair.split('/') for pair in pairs]]
                if len(lat_longs) == 1:
                    return lat_longs[0]
                elif len(lat_longs) == 2:
                    return ((lat_longs[0][0] + lat_longs[1][0]) / 2.0,
                            (lat_longs[0][1] + lat_longs[1][1]) / 2.0)
                else:
                    p = quadrilateral.Polygon(lat_longs)
                    return p.centroid
            except:
                continue
        return (0.0, 0.0)

    def find_work_long(self):
        lat, long = self.work_lat_long()
        return long

    def find_work_lat(self):
        lat, long = self.work_lat_long()
        return lat

class SummaryParser(Pennsylvania.SummaryParser):
    pass

