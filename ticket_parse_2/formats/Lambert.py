# Lambert.py
# Work order format.

import string
#
import et_tools
import sqlmaptypes as smt
import tools
import work_order as work_order_mod
import work_order_audit_parser
import work_order_parser
from work_order_audit import WorkOrderAuditDetail

class WorkOrderParser(work_order_parser.WorkOrderXMLParser):
    """ XML parser for Lambert work orders. """

    REQUIRED = ['client_wo_number', 'kind', 'job_number', 'client_order_num',
                'call_date']

    x_client_wo_number = 'INTERNALNUMBER'
    x_kind = 'TYPE'
    x_job_number = 'JOBID'
    x_client_order_num = 'ORDERNUMBER'
    x_due_date = 'DUEDATE'
    x_client_master_order_num = 'MON'
    x_call_date = 'ISSUEDATE'
    x_transmit_date = 'TRANSMITDATE'
    x_work_type = 'DROPTYPE'
    x_caller_name = 'CUSTOMERNAME'
    x_work_city = 'CUSTOMERCITY'
    x_work_state = 'CUSTOMERSTATE'
    x_work_county = 'CUSTOMERCOUNTY'
    x_work_zip = 'CUSTOMERZIP'
    x_caller_phone = 'CUSTOMERCONTACTNUMBER'
    x_caller_altphone = 'CUSTOMERBTN'
    x_wire_center = 'WIRECENTER'
    x_work_center = 'WORKCENTER'
    x_central_office = 'CO'
    x_serving_terminal = 'SERVINGTERMINAL'
    x_map_page = 'MAP'
    x_map_ref = 'GRID'
    x_circuit_number = 'CIRCUITID'
    x_f2_cable = 'F2CABLE'
    x_terminal_port = 'TERMINALPORT'
    x_f2_pair = 'F2PAIR'
    x_work_description = 'DEFAULTEXTENTOFWORK'
    x_source_sent_attachment = 'ATTACHMENTINCLUDED'

    # these fields will be converted to ISO format:
    DATES = ['due_date', 'transmit_date', 'call_date']

    def post_process(self, work_order):

        work_order.wo_number = 'LAM' + work_order.client_wo_number

        # handle tickets w/o the new TRANSMITDATE field, introduced in Mantis
        # #2786; if it doesn't exist, use ISSUEDATE; this should not occur in
        # production anymore, but we do have a bunch of test tickets without
        # <TRANSMITDATE>
        if not work_order.transmit_date:
            work_order.transmit_date = work_order.call_date

        # these are the same fields
        work_order.caller_contact = work_order.caller_name

        # a few fields are hardcoded for now
        work_order.wo_source = 'LAM01'
        work_order.status = '-P'
        work_order.closed = 0

        # lat/long
        coords = et_tools.find(self._root, 'TERMINALGPS').text
        if coords:
            slat, slong = coords.split('/')[:2]
            work_order.work_lat = float(slat)
            work_order.work_long = float(slong)

        # address; try to split off a number if available
        address = et_tools.find(self._root, 'CUSTOMERADDRESS').text
        parts = address.split()
        if parts and parts[0][0] in string.digits:
            work_order.work_address_number = parts[0]
            address = string.join(parts[1:], ' ')
        work_order.work_address_street = address

        for fn in self.DATES:
            d = tools.isodate(getattr(work_order, fn))
            work_order.set(**{fn: d})

        # cut off strings that are too long
        # XXX this should really be done for all SQLMap subclasses
        for fname, ftype in work_order_mod.WorkOrder.__fields__:
            if isinstance(ftype, smt.SQLString):
                maxlen = ftype.width
                value = getattr(work_order, fname)
                if value and isinstance(value, basestring) \
                and len(value) > maxlen:
                    work_order.set(**{fname: value[:maxlen]})

        # if it all went OK, set parsed_ok to 1
        work_order.parsed_ok = True

class WorkOrderAuditParser(work_order_audit_parser.WorkOrderAuditXMLParser):

    x_summary_date = 'AUDITDATE'

    def find_entries(self):
        entries = []
        for node in et_tools.findall(self._root, 'INTERNALNUMBER'):
            number = node.text
            woad = WorkOrderAuditDetail()
            woad.set(client_wo_number=number)
            entries.append(woad)
        return entries

    def post_process(self, wo_audit):
        wo_audit.wo_source = 'LAM01' # XXX hardcoded for now
        wo_audit.entries = self.find_entries()
        wo_audit.summary_date = tools.isodate(wo_audit.summary_date)

        work_order_audit_parser.WorkOrderAuditXMLParser.post_process(self,
          wo_audit)


