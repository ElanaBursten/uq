# AL811.py

import AlabamaNew
from summaryparsers import BaseSummaryParser
from tools import R
import re
import tools
import summarydetail

class Parser(AlabamaNew.Parser):
    pass

class SummaryParser(BaseSummaryParser):

    S_HEADER = R("^From (\S+)")
    S_FOOTER = R("^TOTAL\s+:\s+(\d+)", re.I)
    S_AUDIT_DATE = R("^Audit For ([0-9/]+)")

    def has_header(self, data):
        return bool(self.S_HEADER.search(data))

    def has_footer(self, data):
        return bool(self.S_FOOTER.search(data))

    def get_client_code(self, data):
        return "AL811" # dummy; audit contains multiple clients

    def get_summary_date(self, data):
        m = self.S_AUDIT_DATE.search(data)
        if m:
            d = m.group(1)
            summary_date = tools.isodate(d)
        else:
            summary_date = ""
        return summary_date

    def get_expected_tickets(self, data):
        m = self.S_FOOTER.search(data)
        xt = m and int(m.group(1)) or 0
        return xt

    def read(self, data):
        tickets = []
        lines = data.split("\n")
        idx = tools.findfirst(lines, lambda s: "Type" in s and "Seq#" in s)
        if idx >= 0:
            for line in lines[idx+2:]: # skip line with "---"s
                if not line.strip(): break

                type_code = line[:6].strip()
                parts = line[6:].split()
                seq_no, ticket_number = parts[:2]
                detail = summarydetail.SummaryDetail([seq_no, ticket_number,
                 "00:00", "", type_code])
                tickets.append(detail)

        return tickets



