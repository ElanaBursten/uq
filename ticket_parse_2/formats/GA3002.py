# GA3002.py

import GA3001
from summaryparsers import BaseSummaryParser
from re_shortcut import R
import string
import tools
import locate
import summarydetail
from date import Date

class Parser(GA3001.Parser):
    pass

class SummaryParser(BaseSummaryParser):
    LENGTH = len("00002 01302-106-039-001 00:09 L")
    RECOG_HEADER_2 = "-----------------------------------------------"

    re_footer = R("^\s+Total Tickets Sent: *(\d+) *$")
    # "Total Tickets Sent: 2307"
    re_recog_header_1 = R("\s+\*SUM\* GAUPC")
    re_date = R("^(\d\d\/\d\d\/\d\d\d\d) ")
    # "02/15/2005"

    def has_header(self, data):
        #return string.find(data, self.RECOG_HEADER_2) > -1
        return True
        # 2005-10-14: some audits have no header, not even the dashed line.
        # To prevent the system from considering these audits "incomplete",
        # we're just going to pretend it does have a header, always.

    def has_footer(self, data):
        return not not self.re_footer.search(data)

    def get_expected_tickets(self, data):
        m = self.re_footer.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    def get_summary_date(self, data):
        # there's no summary date on the audit, so we have to grok it from
        # the ticket dates
        dates = {}
        lines = string.split(data, "\n")
        for line in lines:
            m = self.re_date.search(line)
            if m:
                thedate = line.split()[0]
                dates[thedate] = dates.get(thedate, 0) + 1

        # which date is most common?
        a = [(t[1], t[0]) for t in dates.items()]
        a.sort()
        if a:
            thedate = a[-1][1]
            month, day, year = map(int, thedate.split("/", 3))
            thedate = "%04d-%02d-%02d" % (year, month, day)
            return Date(thedate).isodate()
        else:
            return Date().isodate()
            # not ideal, but we need to specify a date

    def get_client_code(self, data):
        return "3002" # dummy client code

    def read(self, data):
        tickets = []
        for line in string.split(data, "\n"):
            if self.re_date.search(line):
                parts = line.split()
                adate, atime, ticket_number = parts[:3]
                ticket_number, revision = ticket_number.split(":", 1)
                detail = summarydetail.SummaryDetail()
                detail.setdata(["", ticket_number, atime, revision, ""])
                tickets.append(detail)
        return tickets
