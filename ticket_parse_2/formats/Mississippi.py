# Mississippi.py

import string
import Tennessee
from re_shortcut import R

class Parser(Tennessee.Parser):
    re_transmit_date = R("\d+ +MS?OCS +([0-9/]+) +([0-9:]+)")

class SummaryParser(Tennessee.SummaryParser):
    def has_header(self, data):
        return string.find(data, "FROM MSOCS") > -1
