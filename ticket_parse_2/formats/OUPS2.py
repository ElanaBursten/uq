# OUPS2.py

import string
#
import _OUPS
import Dallas4
import emailtools

class Parser(_OUPS.Parser):
    def pre_process(self):
        _OUPS.Parser.pre_process(self)
        if ('quoted-printable' in self.data.lower()) and ('=3D' in self.data):
            # decode quoted-printable text, that wasn't decoded by the receiver
            decoded_data = self.data.decode('quoted-printable')
            decoded_data = decoded_data.replace('\r', '') # fix newline problem
            lines = decoded_data.split('\n')
            lines = emailtools.remove_mark_lines(lines)
            self.data = string.join(lines, '\n')
            self.ticket.image = self.data.lstrip()

    def post_process(self):
        _OUPS.Parser.post_process(self) # todo(dan) make sure this is doing what I think it is
        for x in self.ticket.locates:
            if x.client_code in ['CBE0', 'CBR0', 'CPOP']:
                x.alert = self.ticket.alert = 'A' # todo(dan) Should this be 'A'?

class SummaryParser(Dallas4.SummaryParser):
    CLIENT_CODE = 'OUPS'
