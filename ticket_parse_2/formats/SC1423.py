# SC1423.py

import SC1421
import SC1422
import Dallas4

class Parser(SC1421.Parser):
    # steal methods from SC1422
    _re_work_lat1 = SC1422.Parser._re_work_lat1
    _re_work_lat2 = SC1422.Parser._re_work_lat2
    _re_work_long1 = SC1422.Parser._re_work_long1
    _re_work_long2 = SC1422.Parser._re_work_long2
    find_work_lat = SC1422.Parser.find_work_lat.im_func
    find_work_long = SC1422.Parser.find_work_long.im_func
    post_process = SC1422.Parser.post_process.im_func

class SummaryParser(Dallas4.SummaryParser):
    CLIENT_CODE = 'CPLZ05'

