import Orlando

class Parser(Orlando.Parser):
    # rules for these terms have been moved to translations.py
    W_TERMS = ['GCE560', 'GCE561']

class SummaryParser(Orlando.SummaryParser):
    pass

