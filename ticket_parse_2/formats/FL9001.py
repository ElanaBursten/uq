import Jacksonville
import Orlando

class Parser(Jacksonville.Parser):
    def post_process(self):
        Jacksonville.Parser.post_process(self)
        # due date is taken from ticket, except for emergencies (will be
        # computed by business logic)
        if self.ticket.kind.startswith('EMER'):
            self.ticket.due_date = self.ticket.legal_due_date = None

class SummaryParser(Orlando.SummaryParser):
    pass
