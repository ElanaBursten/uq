# VUPSNewFDE2.py

import Richmond3
import VUPSNewMeetSpecial
from tools import R

class Parser(VUPSNewMeetSpecial.Parser):
    PARSE_GRIDS = True
    re_due_date = R("^Due By\s+Date:\s+([0-9/]+)\s+Time:\s+([0-9:]+) (AM|PM)")
    re_legal_due_date = re_due_date
    f_due_date = VUPSNewMeetSpecial.Parser.f_transmit_date
    f_legal_due_date = f_due_date

class SummaryParser(Richmond3.SummaryParser):
    pass

