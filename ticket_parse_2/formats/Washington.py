# Washington.py

import locate
import string
#
import Delaware1
import Lanham
import tools

R = tools.R

class Parser(Delaware1.Parser): # was: Lanham

    _APPEND_UPDATE = False # if True, append 'UPDATE' to ticket.kind
    
    re_caller = R("^Contact Name *: +(.*?) +(Con|Fax)")
    re_caller_contact = re_caller
    re_caller_altcontact = R("^Job Site Contact: *(.*?) +Job")
    re_work_city = R("^Place      : +(.*)$")
    re_con_state = R("^State      : ([A-Z][A-Z])")
    re_work_state = re_con_state
    re_revision = R("^Ticket No:.*Update No: (\S+)")

    # Mantis #2773: only the term ids in the "Send To:" section must be
    # included; the other ones must be ignored
    def find_locates(self):
        lines = self.data.split("\n")
        while lines and not lines[0].startswith("Send To:"):
            del lines[0]
        lines = [li for li in lines
                 if li.startswith("Send To:") or li.startswith(" ")]

        locates = []
        for line in lines:
            wholeline = line
            line = line[:19]
            if line.startswith("Send"):
                line = line[len("Send To:"):]
            data = string.join(line.split(), "")

            # find seq_number
            parts = wholeline.split()
            if wholeline.startswith("  "):
                seq_number = wholeline[19:40].strip() or None
            else:
                try:
                    idx = parts.index("Seq")
                except ValueError:
                    seq_number = None
                else:
                    seq_number = parts[idx+2]

            locates.append((data, seq_number))

        locates = [locate.Locate(name, seq_number=seq_number)
                   for (name, seq_number) in locates if name.strip()]

        return locates

    _re_map_page_1 = R("^(?:Caller +Provided: +)?Map: +(\S+) +Page: +(\S+) +Grid Cells: +(\S+?)(,|\s|$)")
    _re_map_page_2 = R("^(?:Caller +Provided: +|Ticket Mapping\s+:\s+)Map Name: +(\S+)\s+Map#: (\S+)\s+Grid Cells: (\S+?)(,|\s|$)")

    def find_map_page(self):
        for r in [self._re_map_page_1, self._re_map_page_2]:
            m = r.search(self.data)
            if m:
                return m.group(1) + " " + m.group(2) + " " + m.group(3)
        return ''

    def post_process(self):
        Delaware1.Parser.post_process(self)
        
        if not self.ticket.map_page and self.ticket.work_lat > 0 and \
         self.ticket.work_long < 0:
            self.ticket.map_page = tools.decimal2grid(self.ticket.work_lat, 
             self.ticket.work_long)
        
        if self.ticket.map_page:
            self.ticket.grids = [self.ticket.map_page]

        # add UPDATE to ticket.kind, but only for old-style tickets
        if self._APPEND_UPDATE:
            if self.ticket.update_of and 'UPDATE' not in self.ticket.kind \
            and 'Ticket Mapping' not in self.ticket.image:
                self.ticket.kind += ' UPDATE'

class SummaryParser(Lanham.SummaryParser):
    prepend_zeroes = 0
