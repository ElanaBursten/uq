# OregonXML.py

import string
#
import xml_parser
import locate
import tools
from quadrilateral import Polygon
import Oregon

F = xml_parser.Finder  # shorthand

class Parser(xml_parser.XmlParser):
    def __init__(self, *args, **kwargs):
        xml_parser.XmlParser.__init__(self, *args, **kwargs)

        self.check_max_locates = False

        # Calculate lat/lon
        self.lat_long()

        self.re_ticket_number = F(self.root,"./Ticket", attribute="TicketNum")
        self.re_work_state = F(self.root,"./Ticket/TickAddress/State")
        self.re_work_county = F(self.root,"./Ticket/TickAddress/County")
        self.re_work_city = F(self.root,"./Ticket/TickAddress/City")
        self.re_work_address_number = F(self.root,"./Ticket/TickAddress/Number")
        self.re_work_address_street = F(self.root,"./Ticket/TickAddress/Street")
        self.re_work_cross = F(self.root,"./Ticket/TickAddress/NearInt")
        self.re_work_type = F(self.root,"./Ticket/TickInfo/TypeOfWork")
        self.re_work_remarks = F(self.root,"./Ticket/TickInfo/Remarks")
        self.re_work_description = F(self.root,"./Ticket/TickInfo/LocOfWork")
        self.re_transmit_date = F(self.root,"./Ticket/Transmission/Time")
        self.re_call_date = F(self.root,"./Ticket/OrigCall/Time")
        self.re_operator = F(self.root,"./Ticket/OrigCall/Operator")
        self.re_kind = F(self.root,"./Ticket/Header")
        self.re_ticket_type = self.re_kind
        self.re_work_date = F(self.root,"./Ticket/Dates/WorkBegin")
        self.re_company = F(self.root,"./Ticket/TickInfo/WorkDoneFor")
        self.re_caller = F(self.root,"./Ticket/Caller/Company")
        self.re_caller_contact = F(self.root,"./Ticket/Caller/Contact/Name")
        self.re_caller_phone = F(self.root,"./Ticket/Caller/Contact/Phone")
        self.re_caller_email = F(self.root,"./Ticket/Caller/Email")
        self.re_con_name = F(self.root,"./Ticket/Caller/Company")
        self.re_con_city = F(self.root,"./Ticket/Caller/CAddress/City")
        self.re_con_state = F(self.root,"./Ticket/Caller/CAddress/State")
        self.re_con_zip = F(self.root,"./Ticket/Caller/CAddress/Zip")

    def lat_long(self):
        PolyPoint_elements = self.root.findall("./Ticket/Polygons/Polygon/ExCoord/PolyPoint")
        lats = []
        lons = []
        for PolyPoint_element in PolyPoint_elements:
            lats.append(float(PolyPoint_element.find("./Lat").text))
            lons.append(float(PolyPoint_element.find("./Lon").text))
        poly = Polygon(zip(lats,lons))
        self.lat = poly.centroid[0]
        self.lon = poly.centroid[1]

    def f_call_date(self, match):
        return tools.isodate(match.group(0))

    def f_work_date(self, match):
        return tools.isodate(match.group(0))

    def f_transmit_date(self, match):
        return tools.isodate(match.group(0))

    def find_map_page(self):
        # Try to find a "Township" section for map_page. If the XML has no
        # such node, return "".

        elems = xml_parser.find_all_with_attribute(self.root,
                "./Ticket/Grid/TRSQ/Township", "CallerProvided", "N")

        if elems:
            Township = elems[0].attrib["Value"]
            # Just use the first
            Range = elems[0].find("./Range").attrib["Value"]
            Section = elems[0].find("./Range/Section").attrib["Value"]
        else:
            # Use the first one found, if possible
            township_element = self.root.find("./Ticket/Grid/TRSQ/Township")
            if township_element is None:
                return ""
            Township = township_element.attrib["Value"]
            # Just use the first
            Range = township_element.find("./Range").attrib["Value"]
            Section = township_element.find("./Range/Section").attrib["Value"]

        return "%s%s%s" % (Township, Range, Section)


    def find_work_lat(self):
        return self.lat

    def find_work_long(self):
        return self.lon

    def find_con_address(self):
        elements = []
        elements.append(self.root.find("./Ticket/Caller/CAddress/Number"))
        elements.append(self.root.find("./Ticket/Caller/CAddress/Street"))
        return string.join([element.text for element in elements
                            if element is not None], ' ')

    def find_locates(self):
        locate_elems = self.root.findall("./Ticket/Notification/District/DCode")
        return [locate.Locate(locate_elem.text) for locate_elem in locate_elems]


class SummaryParser(Oregon.SummaryParser):
    pass


