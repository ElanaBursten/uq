# Arizona.py

from parsers import BaseParser
from re_shortcut import R
import locate
import re
import string
import tools
import quadrilateral
import summaryparsers
import summarydetail

class Parser(BaseParser):

    re_ticket_number = R("^Ticket No: *(\d+)\.")
    re_ticket_type = R("^Ticket No: *\S+ *(.*?) *$")
    re_transmit_date = R("^Transmission Date: *([0-9/]+) *Time: *([0-9:]+) (AM|PM)")
    re_call_date = re_transmit_date
    re_work_date = R("^Work Start Date: *([0-9/]+) *Time: *([0-9:]+) (AM|PM)")
    re_due_date = R("^Due Date: *([0-9/]+) *Time: *([0-9:]+) (AM|PM)")
    re_legal_due_date = re_due_date
    re_work_description = R("^Location of Work: (.*?)Type of Work:", re.DOTALL)
    re_work_type = R("^Type of Work: (.*?) *$")
    re_work_remarks = R("^Remarks: (.*?)Company:", re.DOTALL)
    re_con_name = R("^Company: (.*?) *Best Time:")
    re_caller_contact = R("^Contact Name: (.*?) *Phone:")
    re_caller_phone = R("^Contact Name.*Phone: (.*?) *$")
    re_work_state = R("^State: (\S+)")
    re_work_county = R("^State.*County: (.*?) *City:")
    re_work_city = R("^State.*City: (.*?) *$")
    re_work_address_number = R("^Address: *(.*?) *Street:")
    re_work_address_street = R("^Address.*Street: (.*?) *$")
    re_map_page = R("^Twp: *(\S+) *Rng: *(\S+) *Sect-Qtr: *(\S+)")
    re_work_lat = R("^Lat/Lon: *(\S+)")
    re_work_long = R("^Lat/Lon: *\S+ *(\S+)")

    _re_locates = R("^Send To: (\S+)")

    f_transmit_date = lambda s, m: tools.isodate(string.join(m.groups(), ' '))
    f_call_date = f_transmit_date
    f_work_date = f_transmit_date
    f_due_date = f_transmit_date
    f_legal_due_date = f_transmit_date

    f_work_description = lambda s, m: m and m.group(1).replace('\n:', '').strip() or ''
    f_work_remarks = f_work_description

    f_map_page = lambda s, m: string.join(m.groups(), "")[:20]

    def find_locates(self):
        m = self._re_locates.search(self.data)
        if m:
            return [locate.Locate(m.group(1))]
        else:
            return []

    def work_lat_long(self):
        """
        Determine the work latitude and longitude
        Expecting something like this:
        Lat/Lon: 32.573448/-117.058623 32.573448/-117.057489
                32.567181/-117.058623 32.567181/-117.057489
        """
        lines = self.data.split("\n")
        idx = tools.findfirst(lines, lambda s: s.lstrip().startswith("Lat/Lon"))
        if idx > -1:
            # Found it
            line = lines[idx]
            try:
                parts = line.split(':')[1]
                lat1, long1, lat2, long2 = [float(item) for item in parts.split()] # Split on whitespace
                # Get the next line
                line = lines[idx+1]
                lat3,long3,lat4,long4 = [float(item) for item in line.split()]
                # Extract the geometric centroid of the quadrilateral
                q = quadrilateral.Quadrilateral([(lat1,long1),
                                                 (lat2,long2),
                                                 (lat3,long3),
                                                 (lat4,long4)])
                return q.centroid()
            except:
                return 0.0, 0.0
        else:
            return 0.0, 0.0

    def find_work_long(self):
        """
        Find the work longitude
        """
        lat,long = self.work_lat_long()
        return long

    def find_work_lat(self):
        """
        Find the work latitude
        """
        lat,long = self.work_lat_long()
        return lat

class SummaryParser(summaryparsers.BaseSummaryParser):
    # Arizona Blue Stake

    re_term_id = R("^DESTINATION AUDIT FOR (\S+)")
    re_date = R("^LIST OF ALL TICKETS SENT BETWEEN ([0-9/]+) ")
    # ^^ time is assumed to be shortly after midnight
    re_total = R("^Total Tickets:\s+(\d+)")

    def has_header(self, data):
        return bool(self.re_date.search(data))

    def has_footer(self, data):
        return bool(self.re_total.search(data))

    def get_expected_tickets(self, data):
        m = self.re_total.search(data)
        if m:
            return int(m.group(1))
        else:
            return 0

    def get_summary_date(self, data):
        m = self.re_date.search(data)
        if m:
            ds = m.group(1)
            parts = [int(s) for s in ds.split('/')]
            return "%04d-%02d-%02d 00:00:00" % (parts[2], parts[0], parts[1])
        else:
            return date.Date().isodate()

    def get_client_code(self, data):
        m = self.re_term_id.search(data)
        return m and m.group(1) or "SCA3"

    def read(self, data):
        tickets = []
        lines = data.split('\n')
        idx = tools.findfirst(lines, lambda s: s.startswith('----'))

        for line in lines[idx+1:]:
            if not line.strip():
                break
            parts = line.split('|')
            seq_no = parts[0].strip()
            ticket_no = parts[1].strip() # XXX minus '.000'
            if '.' in ticket_no:
                ticket_no = ticket_no.split('.')[0]
            d = summarydetail.SummaryDetail()
            d.setdata([seq_no, ticket_no, "00:00", "", ""])
            tickets.append(d)

        return tickets

