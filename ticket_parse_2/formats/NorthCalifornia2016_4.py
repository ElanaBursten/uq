# NorthCalifornia2016_4.py

import NorthCalifornia2016
import parsers

class Parser(NorthCalifornia2016.Parser):

    def find_locates(self):
        try:
            return NorthCalifornia2016.Parser.find_locates(self)
        except parsers.ParserError as e:
            if e.message <> 'Ticket has no term id':
                raise
        return self._find_locates()

    def post_process(self):
        if not self.ticket.ticket_type:
            if self.ticket.priority:
                self.ticket.ticket_type = 'PRIORITY=' + self.ticket.priority 

        if not self.ticket.transmit_date:
            if self.ticket.call_date:
                self.ticket.transmit_date = self.ticket.call_date

        NorthCalifornia2016.Parser.post_process(self)

class SummaryParser(NorthCalifornia2016.SummaryParser):
    pass

