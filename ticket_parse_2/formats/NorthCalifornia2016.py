# NorthCalifornia2016.py

import parsers
import re
import re_tools
import string
import tools
from date import Date

import NorthCalifornia
import summarydetail

class Parser(NorthCalifornia.Parser):
    rt  = re_tools
    f0  = rt.only_field_re
    f01 = rt.only_or_first_field_re
    f1  = rt.first_field_re
    f1o = rt.first_field_then_optional_field_re
    f2  = rt.next_field_re
    f2o = rt.next_field_then_optional_field_re
    f2t = rt.next_field_then_text_re
    f23 = rt.next_or_last_field_re
    f3  = rt.last_field_re

    re_transmit_date = rt.usan_transmission_line_re(
      transmit_date_re=rt.DT24_GRP)
    re_ticket_type = rt.usan_transmission_line_re(ticket_type_re='(.*?)')
    re_revision = f2t('Message Number', 'Rev', 'Received by')
    re_work_notc = f2('Work Begins', 'Notice', 'Priority')
    re_caller_phone = f1o('Business Tel', 'Ext', 'Fax')
    re_caller_fax = f3('Business Tel', 'Fax')
    re_caller_email = f0('Email Address')
    re_company = f1('Done for', 'Explosives')
    re_explosives = f3('Done for', 'Explosives')
    re_caller_altcontact = f0('Foreman')
    re_caller_cellular = f01('Cell Tel', 'Ext')
    re_work_address_street = f01('Street Address', 'Side')
    re_work_cross = f01('Cross Street', 'Corner')

    def _find_termid(self):
        m = re_tools.usan_transmission_line_re(
          client_code_re=re_tools.CLIENT_CODE_GRP).search(self.data)
        if m:
            client_code = m.group(1)
            return client_code
        else:
            raise parsers.ParserError("Ticket has no term id")

    def post_process(self):
        NorthCalifornia.Parser.post_process(self)
        if ('-MEET' not in self.ticket.ticket_type) and (
          'Field Meet Requested' in self.data):
            self.ticket.ticket_type += '-MEET'

class SummaryParser(NorthCalifornia.SummaryParser):
    re_recog_header_1 = re.compile(r'[A-Z0-9]+ +\*(?:EOD|SUM)\* +USAN[A-Z] +[0-9/]+')

    def ALT_get_summary_date(self, data):
        date = ""  # default values, empty
        lines = string.split(data, "\n")
        for line in lines:
            m = self.re_recog_header_1.search(line)
            if m:
                parts = string.split(line)
                date1 = parts[3]
                date = tools.isodate(date1)
                date = Date(date).isodate()
                break
        return date

    def read(self, data):
        tickets = []
        for line in string.split(data, '\n'):
            if line[:1].isalpha() and line[1:2].isdigit():
                parts = string.split(line, '/')
                for part in parts:
                    part = part.strip()
                    ticket_number = part[:10]
                    info = part[10]
                    number = part[11:16]
                    detail = summarydetail.SummaryDetail(
                      [number, ticket_number, "00:00", "", info])
                    tickets.append(detail)
        return tickets
