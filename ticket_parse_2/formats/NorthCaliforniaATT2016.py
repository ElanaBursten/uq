# NorthCaliforniaATT2016.py

import re_tools
import NorthCalifornia2016
import NorthCaliforniaATT

from re_shortcut import R

class Parser(NorthCalifornia2016.Parser):
    rt  = re_tools
    f1  = rt.first_field_re
    f23 = rt.next_or_last_field_re

    re_serial_number = f1('At&t Ticket Id', 'clli code')

    find_locates = NorthCaliforniaATT.Parser.find_locates.__func__

    _re_plat = f23('At&t Ticket Id', 'clli code', 'OCC Tkt No')

    def add_plat_to_locates(self):
        m = self._re_plat.search(self.data)
        if m:
            for loc in self.ticket.locates:
                loc._plat = m.group(1)

    _re_hp_section = R("(PACBEL:[ \t]*.*?)[ \t]*$")

    def add_hp_info(self):
        # add HP section, if any
        m = self._re_hp_section.search(self.data)
        if m:
            hp_info = m.group(1).strip()[:150]
            self.ticket._hp_info.append(hp_info)
    
    def post_process(self):
        self.add_plat_to_locates()
        self.add_hp_info()
        NorthCalifornia2016.Parser.post_process(self)

class SummaryParser(NorthCaliforniaATT.SummaryParser):
    pass
