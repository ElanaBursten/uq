# xcelresponder.py
# Related Mantis items: #783, #835, #860

from __future__ import with_statement
import csv
import getopt
import os
import string
import sys
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
#
import baseresponder
import config
import datadir
import date
import dbinterface_ado
import errorhandling2
import errorhandling_special
import mail2
import mutex
import program_status
import ticket_db
import tools
import version
from check_python_version import check_python_version

__usage__ = """\
xcelresponder.py [options]

Options:
    -d      Do not delete records from responder queue.
    -e      Do not send email.
    -w      Wait for keypress when done [for testing purposes].
"""

PROCESS_NAME = "XCELresponder"

class XCelResponderOptions:
    verbose = 0
    sends_email = 1
    nodelete = 0
    wait = 0
    configfile = ""

    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)

class XCelDialect(csv.excel):
    """ csv dialect class that causes all fields to be quoted. """
    quoting = csv.QUOTE_ALL

# dummy responder data to keep BaseResponder happy.
class DummyResponderData:
    send_emergencies = 0
    queue = {'table': 'responder_queue'}

class XCelResponder(baseresponder.BaseResponder):

    responder_type = 'xcel'

    def __init__(self, options=None):
        if options is None:
            options = XCelResponderOptions()
        self.options = options

        # set configuration
        if options.configfile:
            if config._configuration:
                raise config.ConfigurationError, \
                 "Configuration already specified"
            self.config = config.Configuration(options.configfile)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()

        # create and configure the logger
        self.log = errorhandling2.ErrorHandler(logdir=self.config.logdir,
                   smtpinfo=self.config.smtp_accs[0], me="XCelresponder",
                   admin_emails=self.config.admins,
                   subject="XCelresponder Error Notification")
        self.log.defer_mail = 1
        self.log.verbose = options.verbose

        self.dbado = dbinterface_ado.DBInterfaceADO()
        self.termids = []   # will be filled later, per call center
        self.responderdata = DummyResponderData()

        # responder is starting; log and write program status
        self.log.log_event("Responder started (version %s)" %
         version.__version__)
        program_status.write_status(PROCESS_NAME + ":" +
         datadir.datadir.get_id())

        self.max_number = 0
        self.use_fieldname_header = 0

    def run(self):
        responders = self.config.responderconfigdata.get_responders(
                     type=self.responder_type)
        for call_center, _, raw_responderdata in responders:
            mutex_name = PROCESS_NAME + ':' + datadir.datadir.get_id() + \
             ':' + call_center
            try:
                with mutex.MutexLock(self.dbado, mutex_name):
                    self.run_call_center(call_center, raw_responderdata)
            except mutex.SingleInstanceError:
                ep = errorhandling2.errorpacket()
                ep.add(info="Locked mutex: %s" % mutex_name)
                self.log.log(ep, send=1, write=1, dump=1)
            except:
                ep = errorhandling2.errorpacket()
                ep.add(info='Error running XCEL responder',
                       responderdata=raw_responderdata)
                self.log.log(ep, send=1, write=1, dump=1)

        if self.options.wait:
            raw_input("[-w option specified] Press Enter to finish.")

    def run_call_center(self, call_center, configdata):
        # 1. get data
        self.termids = configdata['termids']
        self.log.log_event("Using termids: %s" % (self.termids,))
        data = self.get_pending_responses(call_center)

        # 2. write file in CSV-format
        target_date = date.current()[:10]
        filename = self.gen_filename(call_center)
        outputdir = configdata.get('output_dir', '')
        fullname = os.path.join(outputdir, filename)
        dicts = []
        for row in data:
            try:
                d = self.make_csv_dict(row)
                dicts.append(d)
            except:
                ep = errorhandling2.errorpacket()
                ep.add(info='Could not gather all data for this record',
                       row=row)
                self.log.log(ep, send=1, write=1, dump=1)
        body = self.write_file(fullname, dicts)

        # 3. send email
        if self.options.sends_email:
            self.send_email(body, configdata['recipients'], filename)
        else:
            self.log.log_event("No email sent (-e switch)")

        # 4. log responses
        for row in data:
            self.log_response(row)

        # 5. if everything went well, clean up
        if not self.options.nodelete:
            for row in data:
                locate_id = row['locate_id']
                self.log.log_event("Deleting from queue: locate_id %s" % (locate_id,))
                self.delete_from_queue(locate_id)
        else:
            self.log.log_event("No records deleted from queue (-d switch)")

    def send_email(self, emailbody, recipients, filename):
        """ Send emails to the given recipients. """
        host = self.config.smtp
        fromaddr = self.config.from_address
        toaddrs = [d['email'] for d in recipients]
        subject = "XCEL report"
        #body = "XCEL Report in CSV format:\n\n--begin--\n" + emailbody + \
        #       "--end--\n"
        body = "Attached is the daily XCEL report in CSV format."
        self.log.log_event("Preparing to send report email to: %s" % (toaddrs,))

        multiparts= [(filename, emailbody, 'text/plain', '')]

        try:
            mail2.sendmail_multipart(self.log.smtpinfo, toaddrs, subject,
             body, multiparts)
        except:
            ep = errorhandling2.errorpacket()
            ep.add(info='Error when sending email')
            self.log.log(ep, send=1, write=1, dump=1)
        self.log.log_event("Report email sent")

    def _get_terms(self):
        return self.termids

    def get_pending_responses(self, call_center):
        self.log.log_event("Getting pending responses for " + call_center + "...")

        sql = "exec get_pending_responses_unrestricted '%s'" % (call_center,)
        rows = self.dbado.runsql_result(sql)
        if self.max_number:
            rows = rows[:self.max_number]
        self.filter_responses(rows) # remove unwanted records
        if rows:
            self.add_more_data(rows)    # add more fields

        # remove more records, based on fields added by add_more_data
        self.filter_more_responses(rows)

        self.log.log_event("OK (%d records found)" % (len(rows),))
        self.log.log_event("%d records found for %s" % (
         len(rows), call_center), dump=0)
        return rows

    def is_watch_and_protect(self, row):
        return row.get('ticket_type', '').lower() == 'watch and protect'

    def filter_responses(self, rows):
        baseresponder.BaseResponder.filter_responses(self, rows)

        termids = self._get_terms()
        for i in range(len(rows)-1,-1,-1):
            row = rows[i]
            if not row['client_code'] in termids \
            or self.is_watch_and_protect(row):
                self.delete_from_queue(row['locate_id'])
                self.log.log_event("Record skipped: %s" % (row['locate_id'],))
                del rows[i]

    def filter_more_responses(self, rows):
        for i in range(len(rows)-1,-1,-1):
            row = rows[i]
            # if the locate was added by a tech, then the added_by field will
            # contain a number.  we don't want to include those records.
            # 2010-08-13: Now probably redundant, per Mantis #2645
            try:
                int(row['added_by'])
            except (ValueError, TypeError):
                pass
            else:
                self.delete_from_queue(row['locate_id'])
                self.log.log_event("Record was added by tech %s, skipped: %s" % (
                 row['added_by'], row['locate_id'],))
                del rows[i]

    def add_more_data(self, rows):
        """ Add more data to the dataset returned by the get_pending_responses
            stored proc.  Changes the rows list in place. """
        sql = """
         select ticket.*, locate.*
         from ticket, locate
         where ticket.ticket_id = locate.ticket_id
         and locate.locate_id = %s
        """
        for index, row in enumerate(rows):
            locate_id = row['locate_id']
            self.log.log_event("[%d/%d] Gathering more data for locate_id %s..." % (
             index+1, len(rows), locate_id,))
            mysql = sql % (locate_id,)
            moredata = self.dbado.runsql_result(mysql)
            if moredata:
                row.update(moredata[0])
            else:
                self.log.log_event("Could not find data for locate_id %s" % (locate_id,))

        """
>Reqd Field Name Type Max Size Description
>==== ============ ====== ======== ========================================
> * Order Number integer 10 As provided from XCEL (same as one-call)
> * Member Code char 20
> * Facility Type char 1 Valid values: 'G'=Gas, 'E'=Electric, 'TELE', 'FIBER' (any >others?)
> * Tech ID char 6 Identifying person that did actual locate
> * Locate DateTime datetime n/a Date/Time locate complete (YYYY-MM-DD HH:MM:SS)
> * Record TimeStamp datetime n/a Date/Time record sent (YYYY-MM-DD HH:MM:SS)
> * Billing Type char 1 T=Time&Materials, U=Unit, N=No Charge
> * Billing Qty single Format: 9999999.99 (use 0 if no charge)
> * Marked integer 1 1 = Marked, 0 = Not Marked
> * Cleared integer 1 1 = Cleared (NLR), 0 = Not Cleared
> * Cancelled integer 1 1 = Cancelled (no visit necessary), 0 = Not Cancelled
> * Completed integer 1 1 = Completed, 0 = Not yet completed (still ongoing)
> *** Must receive one and only one 'Completed' record per order - this will close order ***
> * Remarks char 500 Free text - include why not marked, why cancelled, any other >comments
"""

    FIELDNAMES = ['Order Number', 'Member Code', 'Facility Type', 'Tech ID',
                  'Locate DateTime', 'Record TimeStamp', 'Billing Type',
                  'Billing Qty', 'Marked', 'Cleared', 'Cancelled', 'Completed',
                  'Remarks']

    def make_csv_dict(self, row):
        billing_type = 'U'
        if row['status'] == 'H':
            billing_type = 'T'
        elif row['status'] == 'NC':
            billing_type = 'N'

        if billing_type == 'T':
            total_hours = int(row.get('regular_hours') or 0) + \
                          int(row.get('overtime_hours') or 0)
            billing_qty = "%.2f" % (total_hours,)
        else:
            billing_qty = row['qty_marked']
            if billing_qty is None:
                billing_qty = ""

        member_code = row['client_code']
        facility_type = 'E'
        if member_code.endswith('G'):
            facility_type = 'G'
            member_code = member_code[:-1]

        tech_id = ""
        if int(row.get('closed', 0)):
            emp_id = row['closed_by_id']
            # try to find user number for this emp_id
        else:
            emp_id = row['assigned_to']
        if emp_id:
            tech_id = self.find_user_number(emp_id)

        marked = (row['status'] == 'M') and 1 or 0
        cleared = (row['status'] not in ('M', 'NC')) and 1 or 0
        #cancelled = (row['status'] == 'NC') and 1 or 0
        cancelled = 0   # new rule 2004-06-11
        completed = int(row.get('closed', 0))
        remarks = ""    # we usually don't provide remarks

        if row['status'] == 'O':
            remarks = "Ongoing"
            cleared = 0

        d = {
            'Order Number': row['ticket_number'],
            'Member Code': member_code,
            'Facility Type': facility_type,
            'Tech ID': tech_id,
            'Locate DateTime': row['closed_date'],  # "complete date"
            'Record TimeStamp': row['transmit_date'],
            'Billing Type': billing_type,
            'Billing Qty': billing_qty,
            'Marked': marked,
            'Cleared': cleared,
            'Cancelled': cancelled,
            'Completed': completed,
            'Remarks': remarks,
        }

        return d

    def find_user_number(self, emp_id):
        sql = "select emp_number from employee where emp_id = %s" % (emp_id,)
        rows = self.dbado.runsql_result(sql)
        if rows:
            return rows[0]['emp_number'] or ""
        else:
            return ""

    def gen_filename(self, call_center):
        target_date = date.current()[:10]
        return "XCEL-report-%s-%s.cmp" % (call_center, target_date)

    def write_file(self, filename, dicts):
        self.log.log_event("Writing %s..." % (filename,))
        f = open(filename, 'w') # NO datadir here
        c = StringIO()
        tee = tools.BetterTee(f, c)
        dw = csv.DictWriter(tee, self.FIELDNAMES, dialect=XCelDialect)

        # write line with field names? not sure
        if self.use_fieldname_header:
            dw.writerow(dict([(x, x) for x in self.FIELDNAMES]))
        else:
            # no, write different header line instead:
            print >> tee, "ReplyTo: XcelResponseErrors@utiliquest.com"

        # write a line for every dict/record
        for d in dicts:
            dw.writerow(d)
        f.close()
        self.log.log_event("File written (%s records)" % len(dicts))

        return c.getvalue()

    def log_response(self, row):
        self.dbado.insertrecord("response_log",
         locate_id=row['locate_id'],
         response_date=date.Date().isodate(),
         call_center=row['ticket_format'],
         status=row['status'],
         response_sent="",
         success=1,
         reply="",
        )

()

if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "dew", ["data="])
    options = XCelResponderOptions()

    for o, a in opts:
        if o == '-d':
            options.nodelete = 1
        elif o == '-e':
            options.sends_email = 0
        elif o == '-w':
            options.wait = 1
        elif o == "--data":
            print "Data dir:", a

    try:
        log = errorhandling_special.toplevel_logger(options.configfile,
              "XCelResponder", "XCelResponder Error Notification")
    except config.ConfigurationError:
        errorhandling_special.log_config_error(options.configfile,
         "XCelResponder")
        sys.exit(1)

    check_python_version(log)

    try:
        xlr = XCelResponder(options)
        xlr.run()
    except SystemExit:
        pass
    except:
        ep = errorhandling2.errorpacket()
        ep.add(info="Uncaught toplevel error")
        log.log(ep, send=1, write=1, dump=1)
