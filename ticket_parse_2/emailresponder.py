# emailresponder.py

import string
#
import abstractresponder
import emailresponder_data
import errorhandling2
import mail2
import statuslist

# We really shouldn't use a BadResponseError here, since it bails out and
# doesn't respond to the rest of the locates either.  Makes sense in a telnet
# session, but not here.

class EmailResponder(abstractresponder.AbstractResponder):
    name = 'EmailResponder'
    responder_type = 'email' # as used by ResponderConfigData

    def __init__(self, *args, **kwargs):
        abstractresponder.AbstractResponder.__init__(self, *args, **kwargs)
        self.statuslist = statuslist.StatusList(self.tdb)

    def load_responder_data(self, call_center, raw_responderdata):
        self.responderdata = emailresponder_data.getresponderdata(
                             self.config, call_center, raw_responderdata)
        # Give the responder data the logger
        self.responderdata.log = self.log

    def response_is_success(self, row, resp):
        return self.responderdata.response_is_success(row, resp)

    def send_response(self, ticket_number, membercode, responsecode,
                      explanation=None, locator_name='', version='0'):

        line = self.make_email_line(ticket_number, membercode, responsecode,
               explanation, version)
        self.lines.append(line)

        return "ok"

        # NOTE: we send a number of tickets together in one email,
        # rather than one by one; therefore, collecting lines is done here,
        # while the actual creating and sending of the email is done in
        # post_process_locates()

    def make_email_line(self, ticket_number, membercode, responsecode,
                        explanation, version):

        status = self._row['status']
        try:
            status_desc = self.statuslist.lookup_description(status)
        except KeyError:
            status_desc = 'unknown status'

        names = {
            'ticket_id': self._row['ticket_id'],
            'ticket_number': ticket_number,
            'disp_code': responsecode, # a translated status
            'closed_date': self._row['closed_date'],
            'client_code': membercode,
            'tech_code': self.responderdata.tech_code,
            'status_description': status_desc,
            'work_date': self._row.get('work_date', ""),
            'work_city': self._row.get('work_city', ""),
            'work_county': self._row.get('work_county', ""),
            'due_date': self._row.get('due_date', ""),
        }

        line = self.responderdata.make_email_line(names, self.tdb, self._version)
        return line

    @staticmethod
    def make_email_body(lines):
        return string.join(lines, "\n")
        # may be moved to EmailResponderData if the need arises

    @staticmethod
    def get_code_from_response(text):
        return text.lower().strip()

    def pre_process_locates(self, call_center):
        self.lines = []

    def post_process_locates(self, call_center):
        if self.lines:
            body = self.make_email_body(self.lines)
            self.send_email(body)
        else:
            self.log.log_event("Nothing to send for " + call_center)

    def send_email(self, body):
        self.log.log_event("Sending email with responses to %s..." % (
         self.responderdata.resp_email,))
        try:
            #host = self.config.smtp
            #fromaddr = self.responderdata.sender
            smtpinfo = self.config.smtp_accs[0].clone(
                       from_address=self.responderdata.sender)
            toaddrs = [s.strip() for s in
                       self.responderdata.resp_email.split(';')]
            subject = self.responderdata.subject or 'Email responses'
            mail2.sendmail(smtpinfo, toaddrs, subject, body)
        except:
            ep = errorhandling2.errorpacket(info="Could not send email")
            self.log.log(ep, send=0, write=1, dump=self.verbose)
        else:
            self.log.log_event("Email sent to %s "% string.join(toaddrs, ';'))

if __name__ == "__main__":

    abstractresponder.run_toplevel(EmailResponder)
