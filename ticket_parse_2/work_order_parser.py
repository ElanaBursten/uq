# work_order_parser.py

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

import et_tools
import work_order

class WorkOrderParser(object):
    """ Base class for work order parsers. """
    REQUIRED = []

class WorkOrderXMLParser(WorkOrderParser):

    REQUIRED = []

    def parse(self, data):
        self._root = ET.fromstring(data)
        self._data = data

        # prepare WorkOrder record
        wo = work_order.WorkOrder()
        wo.image = data
        fields = {}

        # attributes starting with 'x_' map to WorkOrder field names (without
        # the leading 'x_'). post-processing and more complex extractions can
        # be handled in post_process().

        fieldnames = [name[2:] for name in dir(self) if name.startswith("x_")]
        for fn in fieldnames:
            xml_fieldname = getattr(self, 'x_' + fn, None)
            if xml_fieldname is None:
                continue
            try:
                fields[fn] = et_tools.find(self._root, xml_fieldname).text
            except:
                # field not found; if it's required, raise an error;
                # otherwise, substitute a value of None
                if fn in self.REQUIRED:
                    raise
                else:
                    fields[fn] = None

        wo.set(**fields)

        self.post_process(wo)

        return wo

    def post_process(self, work_order):
        pass  # override in subclasses

