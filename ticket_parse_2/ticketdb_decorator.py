# ticketdb_decorator.py
# Decorator for TicketDB, that uses Profiler for timing method calls.
# Created: 2002.07.09 HN

import decorator
import profiler

class TicketDBDecorator(decorator.Decorator):
    def __init__(self, *args, **kwargs):
        decorator.Decorator.__init__(self, *args, **kwargs)
        self.profiler = profiler.Profiler()

class TicketDBMethodDecorator(decorator.MethodDecorator):

    def before(self, *args, **kwargs):
        self.decorator.profiler.start()

    def after(self, *args, **kwargs):
        self.decorator.profiler.stop()
        self.decorator.profiler.add(self.f.__name__, args, kwargs)



if __name__ == "__main__":

    import ticket_db

    tdb = ticket_db.TicketDB()
    print tdb.getticketcount()

    tdb_deco = TicketDBDecorator(tdb, TicketDBMethodDecorator)
    print tdb_deco.getticketcount()
    print tdb_deco.profiler.data[0]
