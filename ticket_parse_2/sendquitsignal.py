# sendquitsignal.py
# Send the 'quit' signal to apps that are listening for it.

import os
import listener

def send_quit_signal():
    f = open(listener.QUIT_FILENAME, "w")
    f.write("1")
    f.close()

def release_quit_signal():
    # reset file to 0 bytes
    f = open(listener.QUIT_FILENAME, "w")
    f.close()

    # then remove it
    try:
        os.remove(listener.QUIT_FILENAME)
    except:
        import traceback; traceback.print_exc()
        print "Could not remove signal file."


if __name__ == "__main__":

    send_quit_signal()
    raw_input("Press any key to stop the signal... ")
    release_quit_signal()

