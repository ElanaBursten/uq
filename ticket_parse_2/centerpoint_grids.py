# centerpoint_grids.py

# NOTE: data has embedded tab characters!
data = '''\
91
92
93
121
122
123
151
152
153
211
212
213
242
243
244
245
246
247
249
250
282
283
284
285
286
287
288
289
290
291
292
322
323
324
325
326
327
328
329
330
331
332
362
363
364
365
366
367
368
369
370
371
372
402
403
404
405
406
407
408
409
410
411
412
437
438
439
440
441
442
443
444
445
446
447
448
449
450
451
452
'''

# fill grids dictionary
grids = {}
for line in data.split("\n"):
    if line.strip():
        try:
            page, rest = line.split("\t", 1)
        except ValueError:
            try:
                page, rest = line.split(" ", 1)
            except ValueError:
                page, rest = line.strip(), ""
        if rest:
            parts = [x.strip() for x in rest.split(",")]
        else:
            parts = []
        grids[page] = parts

def match(map_page):
    # map_page has a format like 'TX 485 N'
    try:
        parts = map_page.split()
        page, letter = parts[1], parts[2]
    except (ValueError, IndexError):
        return 0    # not enough information could be extracted
    try:
        p = grids[page]
        return (not p) or (letter in p)
    except KeyError:
        return 0

if __name__ == "__main__":

    import pprint
    pprint.pprint(grids)

