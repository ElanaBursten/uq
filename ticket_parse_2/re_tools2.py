# re_tools2.py

# todo(dan) What general ways can parsing with re's produce incorrect results?
# And how can they be fixed, or at least minimized? Add min and/or max length to
# value re's?

# todo(dan) Improve naming

# todo(dan) Review returning None vs '' for matches. Could vary for 'find' vs
# other types of functions.

import re
#
import tools

def options_re(before, options, after, blank=''):
    if (options is None) or (options == []) or (options == [None]):
        return ''

    if not isinstance(options, list):
        options = [options]

    if len(options) == 1:
        if options[0]:
            return before + options[0] + after
        else:
            return blank

    options2 = []
    for option in options:
        if option:
            options2.append(before + option + after)
        else:
            options2.append(blank)
    return '(?:' + '|'.join(options2) + ')'

# todo(dan) be careful if preceding re matches an empty string
def lookbehind_re(options):
    return '(?<=' + options_re('', options, '') + ')'

BLANK = r'[ \t]'
BLANKS_0_N = BLANK + '*'
BLANKS_1_N = BLANK + '+'
BLANKS_1_N_RE = re.compile(BLANKS_1_N)

# todo(dan) Rename these?
START = '^' + BLANKS_0_N
END = BLANKS_0_N + '$'

MULTILINE = '(?m)'

def header_re(header):
    return header + BLANKS_0_N + ':'

def bracketed_re(value):
    return r'\[' + BLANKS_0_N + value + BLANKS_0_N + r'\]'

# todo(dan) Rename?
ANY = '.*?'
ANY_BRACKETED = bracketed_re(ANY)
ANY_CAPTURED = '(.*?)'
ANY_CAPTURED_BRACKETED = bracketed_re(ANY_CAPTURED)

ALL = r'(?:.|\n)' # same as '.' with DOTALL, but doesn't require DOTALL
ALL_0_N = ALL + '*'
REST_OF_LINE = '.*?$' # assumes multi-line
REST_OF_LINE_CAPTURED = '(' + REST_OF_LINE + ')' # assumes multi-line

# these are useful for excluding inf and NaN
FLOAT = r'[-+.\d]*'
FLOAT_CAPTURED = '(' + FLOAT + ')'

DATE = '[0-9/]+'
DATE_CAPTURED = '(' + DATE + ')'
TIME = '[0-9:]+'
TIME_CAPTURED = '(' + TIME + ')'
AMPM = '(?:AM|PM|am|pm)'
AMPM_CAPTURED = '(' + AMPM + ')'

DATE_TIME_12_SPLIT_CAPTURED = DATE_CAPTURED + BLANKS_1_N + header_re('Time') + \
  BLANKS_0_N + TIME_CAPTURED + BLANKS_0_N + AMPM_CAPTURED

def isodate_from_3_groups(self, match):
    return tools.isodate(match.group(1) + ' ' + match.group(2) + ' ' +
                         match.group(3))

def header_value_re(header, value):
    return header_re(header) + BLANKS_0_N + value

# todo(dan) Document the params
def standard_line_re(first_options, before_options, after_options,
                     value=ANY_CAPTURED, compiled=True):
    first = options_re('', first_options, '.*' + lookbehind_re([BLANK, ':']))
    before = options_re('', before_options,
                        BLANKS_0_N + lookbehind_re([BLANK, ':']),
                        blank=(START if first else ''))
    after = options_re(BLANKS_1_N, after_options, '', blank=END)
    regex = MULTILINE + START + first + before + value + after
    if compiled:
        return re.compile(regex)
    return regex

# todo(dan) Document the params
def bracket_line_re(first_options, before_options, after_options,
                    value=ANY_CAPTURED_BRACKETED, compiled=True):
    first = options_re('', first_options, '.*' + lookbehind_re([BLANK, r'\]']))
    # todo(dan) Should before consume trailing blanks?
    before = options_re('', before_options, '', blank=(START if first else ''))
    after = options_re(BLANKS_0_N, after_options, '', blank=END)
    regex = MULTILINE + START + first + before + value + after
    if compiled:
        return re.compile(regex)
    return regex

# todo(dan) rename?
def lines_from_re(first, compiled=True):
    regex = MULTILINE + START + first + ALL_0_N
    if compiled:
        return re.compile(regex)
    return regex

# todo(dan) rename?
# todo(dan) Add an option to check for >1 match?
# todo(dan) Revise so compiled regex can be saved?
def find_standard_multiline(header, data):
    regex = MULTILINE + START + header_re(header) + REST_OF_LINE +\
      r'(?:\n' + BLANKS_0_N + ':' + REST_OF_LINE + ')*'
    match = re.compile(regex).search(data)
    if match:
        return ' '.join([l.partition(':')[2].strip()
                        for l in match.group().split('\n')])
    return ''

# todo(dan) rename?
def city_state_zip_from_line(line):
    city = state = zip = None

    part1, sep, part2 = line.rpartition(',')
    if sep:
        city = part1.strip()

        parts = BLANKS_1_N_RE.split(part2.strip(), 1)
        if parts and (len(parts) > 0) and parts[0] and (len(parts[0]) == 2):
            state = parts[0]
            if (len(parts) > 1) and parts[1]:
                zip = parts[1]

    return city, state, zip
