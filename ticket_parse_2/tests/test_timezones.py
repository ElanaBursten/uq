# test_timezones.py

import datetime
import site; site.addsitedir('.')
import unittest
#
import timezone # add pytz egg to path
import pytz

class TestTimezones(unittest.TestCase):

    def test_simple_conversion(self):
        # get some time zones, using the names used by pytz
        est = pytz.timezone("EST5EDT")
        pst = pytz.timezone("PST8PDT")

        # create a timezone-agnostic date/time
        d = datetime.datetime(2011, 11, 17, 18, 52, 00)

        # and a format for display
        fmt = "%Y-%m-%d %H:%M:%S %Z%z"

        # take the original date and give it a timezone (EST/EDT)
        d_est = est.localize(d)
        self.assertEquals(d_est.strftime(fmt), "2011-11-17 18:52:00 EST-0500")

        # now convert it to PST/PDT
        d_pst = d_est.astimezone(pst)
        self.assertEquals(d_pst.strftime(fmt), "2011-11-17 15:52:00 PST-0800")

    def test_dst(self):
        """ Test conversions between time zones with and without DST. """

        ESTEDT = pytz.timezone("EST5EDT")
        EST = pytz.timezone("EST")
        MST = pytz.timezone("MST")
        MSTMDT = pytz.timezone("MST7MDT")
        d = datetime.datetime(2011, 8, 1, 15, 30, 00)
        fmt = "%Y-%m-%d %H:%M:%S %Z%z"

        # give the date a time zone (EST); no notion of DST here
        d1 = EST.localize(d)
        self.assertEquals(d1.strftime(fmt), "2011-08-01 15:30:00 EST-0500")

        # convert that time to EST5EDT (thus, taking DST into account, which
        # applies in this case, since the date is in August)
        d2 = d1.astimezone(ESTEDT)
        self.assertEquals(d2.strftime(fmt), "2011-08-01 16:30:00 EDT-0400")


        # convert the EST date to MST (again, no DST; like Arizona)
        d3 = d1.astimezone(MST)
        self.assertEquals(d3.strftime(fmt), "2011-08-01 13:30:00 MST-0700")

        # convert the EST date to MST7MDT, taking DST into account
        d4 = d1.astimezone(MSTMDT)
        self.assertEquals(d4.strftime(fmt), "2011-08-01 14:30:00 MDT-0600")

    def test_timezones(self):
        """ Check if the timezones in pytz_timezones exist. """
        for key, value in timezone.pytz_timezones.items():
            tz = pytz.timezone(value)

    def test_convert(self):
        self.assertEquals(
          timezone.convert("2011-11-19 22:25:00", "EST/EDT", "PST/PDT"),
          "2011-11-19 19:25:00")
        self.assertEquals(
          timezone.convert("2011-11-19 22:25:00", "EST/EDT", "MST"),
          "2011-11-19 20:25:00")

        # use DST
        self.assertEquals(
          timezone.convert("2011-07-19 22:25:00", "EST/EDT", "EST"),
          "2011-07-19 21:25:00")

        # use pytz internal names
        self.assertEquals(
          timezone.convert("2011-11-19 22:25:00", "EST5EDT", "PST8PDT"),
          "2011-11-19 19:25:00")

        # unknown timezone names should raise an exception
        try:
            self.assertEquals(
              timezone.convert("2011-11-19 22:25:00", "EST/EDT", "bogus"),
              "2011-11-19 19:25:00")
        except pytz.UnknownTimeZoneError:
            pass
        except:
            raise
        else:
            raise

