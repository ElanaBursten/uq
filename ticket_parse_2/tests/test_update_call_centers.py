# test_update_call_centers.py

import site; site.addsitedir('.')
import unittest
#
import _testprep
import call_centers
import ticket_db
import update_call_centers

class TestUpdateCallCenters(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.cc = call_centers.get_call_centers(self.tdb)
        self.cc.reload()

    def test_get_updatedata_1(self):
        '''
        Get the update call centers and check for UpdateData
        '''
        missing = []
        uccs = self.cc.ucc.keys()
        for cc in uccs:
            try:
                ud = update_call_centers.get_updatedata(cc)
            except update_call_centers.UpdateDataError:
                missing.append(cc)

        self.assertEquals(missing, [], "No UpdateData found for: %s" % (missing,))

    def test_get_updatedata_2(self):
        '''
        Get all call centers and check for UpdateData
        '''
        missing = []
        for cc in call_centers.cc2format.keys():
            if cc in ["TEST01", "TEST02", "XYZ"]: continue
            if not _testprep.RUN_BROKEN_FDE7 and cc == 'FDE7': continue
            if not _testprep.RUN_BROKEN_FMW6 and cc == 'FMW6': continue
            try:
                ud = update_call_centers.get_updatedata(cc)
            except update_call_centers.UpdateDataError:
                missing.append(cc)

        self.assertEquals(missing, [], "No UpdateData found for: %s" % (missing,))


if __name__ == "__main__":

    unittest.main()

