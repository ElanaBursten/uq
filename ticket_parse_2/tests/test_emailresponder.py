# test_emailresponder.py

from __future__ import with_statement
from StringIO import StringIO
import os
import shutil
import site; site.addsitedir('.')
import string
import unittest
#
import _testprep
import abstractresponder
import businesslogic
import config
import datadir
import date
import emailresponder
import mail2
import ticket_db
import ticketloader
import ticketparser
import ticketversion
import tools
from mockery import *
from test_main import drop

def create_mock_responder(rows, *args, **kwargs):
    """ Create a mock EmailResponder with a number of rows (a list of dicts)
        to respond to. """

    class MockEmailResponder(emailresponder.EmailResponder):
        sent = []
        def send_email(self, body):
            self.__class__.sent.append(body)
        def get_pending_responses_raw(self, call_center):
            return [row for row in rows
                    if row['ticket_format'] == call_center]

    return MockEmailResponder(*args, **kwargs)


class TestEmailResponder(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

        try:
            os.mkdir("../testdata/temp_NCA1_ack")
        except:
            pass
        try:
            os.mkdir("../testdata/temp_NCA1_ack_proc")
        except:
            pass

    def tearDown(self):
        try:
            shutil.rmtree("../testdata/temp_NCA1_ack")
        except:
            pass
        try:
            shutil.rmtree("../testdata/temp_NCA1_ack_proc")
        except:
            pass

    def test_NCA1(self):
        _testprep.add_test_client(self.tdb, 'COX01', 'NCA1')
        _testprep.add_test_client(self.tdb, 'COX04', 'NCA1')

        cfg = config.getConfiguration()
        self.assert_(cfg.emailresponders.get('NCA1')), \
         "NCA1 email responder block required in config.xml"
        # use custom mappings so we're not dependent on what happens to be in
        # config.xml
        z = cfg.emailresponders['NCA1']
        z['all_versions'] = 0
        z['mappings']['M'] = ('3100', '')
        z['mappings']['N'] = ('3101', '')
        z['clients'] = ['COX01', 'COX04']
        z['ack_dir'] = 'zigazig-ah'
        # make sure we don't have 'all_versions' set
        self.assertEquals(z.get('all_versions', 0), 0)

        rows = [
          {'serial_number': '123', 'ticket_id': 1, 'locate_id': 100,
           'client_code': 'ABC01',  'status': 'C'},
          {'serial_number': '456', 'ticket_id': 1, 'locate_id': 101,
           'client_code': 'COX01', 'status': 'M'},
          {'serial_number': '789', 'ticket_id': 1, 'locate_id': 102,
           'client_code': 'BAH05',  'status': 'P'},
          {'serial_number': '666', 'ticket_id': 2, 'locate_id': 103,
           'client_code': 'COX04',  'status': 'N'},
        ]
        for row in rows:
            row['ticket_number'] = 'dummy'
            row['ticket_format'] = 'NCA1'
            row['ticket_type'] = 'NORMAL'
            row['closed_date'] = '2006-12-31 09:10:11'

        def mock_getticket(*args, **kwargs):
            return {'kind': "NORMAL", 'ticket_type': "test"}

        e = create_mock_responder(rows, verbose=0)
        with Mockery(e.tdb, 'getticket', mock_getticket):
            e.log.lock = True
            e.load_responder_data('NCA1', None)
            self.assertEquals(e.responderdata.name, 'NCA1')

            e.respond_all()

        self.assertEquals(len(e.sent), 1)
        self.assertEquals(len(e.sent[0]), 114*2 + 1)
        self.assertEquals(e.sent[0].count('666'), 1)
        self.assertEquals(e.sent[0].count('123106'), 2)
        # tech code 'UTI' should be defined in config.xml
        self.assertEquals(e.sent[0].count('UTI'), 2)

        # NCA1 does acknowledgements.  therefore, at this point, we expect
        # success=0 and a 'waiting' message:
        resp_rows = self.tdb.getrecords('response_log')
        self.assertEquals(len(resp_rows), 2) # 2 responses sent
        self.assertEquals(resp_rows[0]['success'], '0')
        self.assertEquals(resp_rows[0]['reply'], '(email waiting)')

        # what are contents of response_ack_waiting?
        raw_rows = self.tdb.getrecords('response_ack_waiting')
        self.assertEquals(len(raw_rows), 2)
        # reference_numbers should be serial numbers, not ticket numbers
        self.assertEquals(raw_rows[0]['reference_number'], '456')
        self.assertEquals(raw_rows[1]['reference_number'], '666')

        # put ack file somewhere and have responder process it.
        # then test response_log again.

        # point ack_dir to temp dir
        z['ack_dir'] = '../testdata/temp_NCA1_ack'
        z['ack_proc_dir'] = '../testdata/temp_NCA1_ack_proc'
        # put files in it with acks
        with open('../testdata/temp_NCA1_ack/ack1.txt', 'w') as f:
            print >> f, "456,ACK"
            print >> f, "666,ACK"
            print >> f, "777888999,ACK" # does not exist

        # run responder again, this time with no responses
        del e
        e = create_mock_responder([], verbose=0)
        e.log.lock = 1
        e.load_responder_data('NCA1', None)
        e.respond_all()

        # the response_log records should now have success=1
        resp_rows = self.tdb.getrecords('response_log')
        self.assertEquals(len(resp_rows), 2) # 2 responses sent
        self.assertEquals(resp_rows[0]['success'], '1')
        self.assertEquals(resp_rows[0]['reply'], 'acknowledged')

        # ack directory should be empty
        files = os.listdir("../testdata/temp_NCA1_ack")
        self.assertEquals(files, [])

        # what are contents of response_ack_waiting?
        raw_rows = self.tdb.getrecords('response_ack_waiting')
        self.assertEquals(len(raw_rows), 0)

        # put files in it with spam
        spam_tickets = tools.safeglob(os.path.join(_testprep.TICKET_PATH,
                       "sample_tickets", "NCA2", "nca2-*.txt"))
        for ticket_file in spam_tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.tickets[0]
            drop(os.path.basename(ticket_file), raw, dir = z['ack_dir'])

        # run responder again, this time with no responses
        e = create_mock_responder([], verbose=0)
        e.log.lock = True
        e.log.logger.logfile = StringIO()
        e.load_responder_data('NCA1', None)
        e.respond_all()
        log = e.log.logger.logfile.getvalue()
        e.log.logger.logfile.close()
        self.assertEquals(log.count('Invalid number for'), len(spam_tickets))
        self.assertEquals(log.count('Bailing out for file'), len(spam_tickets))

    test_NCA1.fails=1

    def _test_NCA1_2(self, all_versions=0, with_notes=0):
        # test NCA1 again, this time to check ticket_version and remarks.

        _testprep.add_test_client(self.tdb, 'COX01', 'NCA1')
        _testprep.add_test_client(self.tdb, 'COX04', 'NCA1')

        cfg = config.getConfiguration()
        self.assert_(cfg.emailresponders.get('NCA1')), \
         "NCA1 email responder block required in config.xml"
        # use custom mappings so we're not dependent on what happens to be in
        # config.xml
        z = cfg.emailresponders['NCA1']
        z['all_versions'] = 0
        z['mappings']['M'] = ('3100', '')
        z['mappings']['N'] = ('3101', '')
        z['clients'] = ['COX01', 'COX04']
        z['all_versions'] = all_versions
        # make sure we don't have 'all_versions' set
        self.assertEquals(z.get('all_versions', 0), all_versions)

        tl = ticketloader.TicketLoader("../testdata/NorthCaliforniaATT-1.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['NorthCaliforniaATT'])
        t.locates[0].client_code = 'COX01'
        t.serial_number = '1234'
        t.source = 'NCA2'
        ticket_id, locate_ids = t.insert(self.tdb)

        if with_notes:
            # insert a few ticket notes; these should show up in the text of
            # the response
            for i in range(3):
                note = str(i+1) * 50
                sql = """
                  insert notes (foreign_id, foreign_type, uid, note)
                  values (%d, 1, %d, '%s');
                """ % (int(ticket_id), 500, note)
                self.tdb.runsql(sql)
            # verify that the notes were inserted
            tnotes = self.tdb.getrecords('ticket_notes', ticket_id=ticket_id)
            self.assertEquals(len(tnotes), 3)

        # there are three versions of this ticket; sns are 123, 456, and 789
        # 789 is the most recent one
        self.tdb.add_ticket_version(ticket_id, t, 'one.txt')
        t.serial_number = '4567'
        self.tdb.add_ticket_version(ticket_id, t, 'two.txt')
        t.serial_number = '7890'
        self.tdb.add_ticket_version(ticket_id, t, 'three.txt')

        # add an NCA1 version as well; this should be ignored
        t.serial_number = '666'
        t.source = 'NCA1'
        self.tdb.add_ticket_version(ticket_id, t, 'four.txt')

        rows = [
          {'serial_number': '7890', 'ticket_id': ticket_id,
           'locate_id': locate_ids[0], 'client_code': 'COX01',
           'status': 'C'}
        ]
        for row in rows:
            row['ticket_number'] = t.ticket_number
            row['ticket_format'] = 'NCA1'
            row['ticket_type'] = 'NORMAL'
            row['closed_date'] = '2006-12-31 09:10:11'

        e = create_mock_responder(rows, verbose=0)
        e.log.lock = 1
        e.load_responder_data('NCA1', None)
        self.assertEquals(e.responderdata.name, 'NCA1')

        e.respond_all()

        # only one ticket has been responded to
        self.assertEquals(len(e.sent), 1)
        if all_versions:
            if with_notes:
                # longer, because notes are part of the response now
                self.assertEquals(len(e.sent[0]), 594 + 2)
            else:
                self.assertEquals(len(e.sent[0]), 114*3 + 2) # 3 lines
            #self.assertEquals(e.sent[0].count('Cleared / Aerial'), 3)
            # remarks must refer to latest serial_number
            #self.assertEquals(e.sent[0].count('Reference ticket id 7890'), 2)
            self.assertEquals(e.sent[0].count('7890'), 1) # 3)
            self.assertEquals(e.sent[0].count('1234 '), 1)
            self.assertEquals(e.sent[0].count('4567 '), 1)
            self.assertEquals(e.sent[0].count('123106'), 3)
        else:
            self.assertEquals(len(e.sent[0]), 114)
            # remarks must refer to latest serial_number
            #self.assertEquals(e.sent[0].count('Cleared / Aerial'), 1)
            self.assertEquals(e.sent[0].count('Reference ticket id 789'), 0)
            self.assertEquals(e.sent[0].count('7890'), 1)
            self.assertEquals(e.sent[0].count('123106'), 1)

    def test_NCA1_2_single_version(self):
        self._test_NCA1_2(all_versions=0)

    test_NCA1_2_single_version.fails=1

    def test_NCA1_2_all_versions(self):
        self._test_NCA1_2(all_versions=1)

    test_NCA1_2_all_versions.fails=1

    def test_NCA1_with_notes(self):
        self._test_NCA1_2(all_versions=1, with_notes=1)

    test_NCA1_with_notes.fails=1

    def test_FWP1(self):
        _testprep.add_test_client(self.tdb, 'FWP01', 'FWP1')
        _testprep.add_test_client(self.tdb, 'FWP02', 'FWP1')

        cfg = config.getConfiguration()
        self.assert_(cfg.emailresponders.get('FWP1')), \
         "FWP1 email responder block required in config.xml"
        # use custom mappings so we're not dependent on what happens to be in
        # config.xml
        z = cfg.emailresponders['FWP1']
        z['all_versions'] = 0
        z['mappings']['M'] = ('M', '') # actual value doesn't matter
        z['mappings']['XD'] = ('XD', '')
        z['clients'] = ['FWP01', 'FWP02']
        # make sure we don't have 'all_versions' set
        self.assertEquals(z.get('all_versions', 0), 0)

        rows = [
          # first locate; should be responded to
          {'ticket_id': 1,
           'locate_id': 100,
           'client_code': 'FWP01',
           'status': 'C',
           'ticket_number': '123456',
           'work_date': '2007-02-01 12:00:00',
           'work_city': 'FOO',
           'work_county': 'BAR',
          },
          # second locate; should be skipped (status XD)
          {'ticket_id': 2,
           'locate_id': 101,
           'client_code': 'FWP01',
           'status': 'XD',
           'ticket_number': '789789',
           'work_date': '2007-03-01 12:00:00',
           'work_city': 'THIS',
           'work_county': 'THAT',
          },
        ]
        for row in rows:
            row['ticket_format'] = 'FWP1'
            row['ticket_type'] = 'NORMAL'
            row['closed_date'] = '2007-01-31 09:10:11'

        def mock_getticket(*args, **kwargs):
            return {'kind': "NORMAL", 'ticket_type': "test"}

        e = create_mock_responder(rows, verbose=0)
        with Mockery(e.tdb, 'getticket', mock_getticket):
            e.log.lock = 1
            e.load_responder_data('FWP1', None)
            self.assertEquals(e.responderdata.name, 'FWP1')

            e.respond_all()

        self.assertEquals(len(e.sent), 1)
        self.assertEquals(len(e.sent[0]), 80)
        self.assertEquals(e.sent[0].count('FWP01'), 1)
        self.assertEquals(e.sent[0].count('FOO'), 1)
        self.assertEquals(e.sent[0].count('BAR'), 1)
        self.assertEquals(e.sent[0].count('2007-02-01'), 1)
        self.assertEquals(e.sent[0].count('123456'), 1)

        # FWP does not do acknowledgements.  therefore we expect success=1:
        rows = self.tdb.getrecords('response_log')
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['success'], '1')
        self.assertEquals(rows[0]['reply'], 'ok')

    def test_1421(self):
        _testprep.add_test_client(self.tdb, 'Z1421A01', '1421')
        _testprep.add_test_client(self.tdb, 'Z1421A02', '1421')

        cfg = config.getConfiguration()
        self.assert_(cfg.emailresponders.get('1421')), \
         "1421 email responder block required in config.xml"
        # use custom mappings so we're not dependent on what happens to be in
        # config.xml
        z = cfg.emailresponders['1421']
        z['all_versions'] = 0
        z['mappings']['M'] = ('3020', '') # actual value doesn't matter
        z['mappings']['XD'] = ('3030', '')
        z['clients'] = ['Z1421A01', 'Z1421A02']
        # make sure we don't have 'all_versions' set
        self.assertEquals(z.get('all_versions', 0), 0)

        # fields here must occur in result set returned by get_pending_responses
        rows = [
          # first locate; should be responded to
          {'ticket_id': 1,
           'locate_id': 100,
           'client_code': 'Z1421A01',
           'status': 'C',
           'ticket_number': '123456',
           'work_date': '2007-02-01 12:00:00',
           'work_city': 'FOO',
           'work_county': 'BAR',
           'due_date': '2007-03-01 11:00:00',
          },
          # second locate; should be responded to as well
          {'ticket_id': 2,
           'locate_id': 101,
           'client_code': 'Z1421A01',
           'status': 'XD',
           'ticket_number': '789789',
           'work_date': '2007-03-01 12:00:00',
           'work_city': 'THIS',
           'work_county': 'THAT',
           'due_date': '2007-03-01 11:00:00',
          },
        ]
        for row in rows:
            row['ticket_format'] = '1421'
            row['ticket_type'] = 'NORMAL'
            row['closed_date'] = '2007-01-31 09:10:11'

        def mock_getticket(*args, **kwargs):
            return {'kind': "NORMAL", 'ticket_type': "test"}

        e = create_mock_responder(rows, verbose=0)
        with Mockery(e.tdb, 'getticket', mock_getticket):
            e.log.lock = 1
            e.load_responder_data('1421', None)
            self.assertEquals(e.responderdata.name, '1421')

            e.respond_all()

        self.assertEquals(len(e.sent), 1)
        lines = e.sent[0].split('\n')
        self.assertEquals(len(lines), 4) # including two blank lines
        self.assertEquals(lines[1].strip(), "")
        self.assertEquals(lines[0], "123456,Z1421A01,3010,2007-03-01 11:00:00,2007-01-31 09:10:11")
        self.assertEquals(lines[2], "789789,Z1421A01,3030,2007-03-01 11:00:00,2007-01-31 09:10:11")

        # 1421 does not do acknowledgements.  therefore we expect success=1:
        rows = self.tdb.getrecords('response_log')
        self.assertEquals(len(rows), 2)
        self.assertEquals(rows[0]['success'], '1')
        self.assertEquals(rows[0]['reply'], 'ok')

    def test_1421_with_responder_queue(self):
        _testprep.add_test_client(self.tdb, 'Y1421A01', '1421')
        _testprep.add_test_client(self.tdb, 'Y1421A02', '1421')

        cfg = config.getConfiguration()
        self.assert_(cfg.emailresponders.get('1421')), \
         "1421 email responder block required in config.xml"
        # use custom mappings so we're not dependent on what happens to be in
        # config.xml
        z = cfg.emailresponders['1421']
        z['all_versions'] = 0
        z['mappings']['M'] = ('3020', '') # actual value doesn't matter
        z['mappings']['XD'] = ('3030', '')
        z['clients'] = ['Y1421A01', 'Y1421A02']
        # make sure we don't have 'all_versions' set
        self.assertEquals(z.get('all_versions', 0), 0)

        # store a ticket
        tl = ticketloader.TicketLoader("../testdata/SC1421-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("BSZB45", "Y1421A01")
        raw = raw.replace("COC82", "Y1421A02")
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['SC1421'])
        t.due_date = t.legal_due_date = '2007-02-02 12:13:14'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # set status and stuff
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        for locrow in locates:
            if locrow['client_code'] in z['clients']:

                # set status to M (this will apparently insert the locate into
                # the responder queue)
                sql = """
                 update locate
                 set status = 'M', closed_date = '2007-02-03 13:14:15'
                 where locate_id = %s
                """ % (locrow['locate_id'],)
                self.tdb.runsql(sql)

        # make sure we have a due date
        ticketrow = self.tdb.getrecords("ticket", ticket_id=ticket_id)[0]
        self.assertEquals(ticketrow['due_date'], '2007-02-02 12:13:14')

        # get records from responder_queue
        q = self.tdb.getrecords("responder_queue")
        self.assertEquals(len(q), 2)

        sql = "exec get_pending_responses '1421'"
        rows = self.tdb.runsql_result(sql)
        self.assertEquals(len(rows), 2)

        e = create_mock_responder(rows, verbose=0)
        e.log.lock = 1
        e.load_responder_data('1421', None)
        self.assertEquals(e.responderdata.name, '1421')
        # Send the log to a string
        e.log.logger.logfile = StringIO()
        e.respond_all()

        log = e.log.logger.logfile.getvalue()
        e.log.logger.logfile.close()
        self.assertEquals(log.count("Closed date/time sent: 2007-02-03 13:14:15"),2)

        self.assertEquals(len(e.sent), 1)
        lines = e.sent[0].split('\n')
        self.assertEquals(len(lines), 4) # includes two newlines
        self.assertEquals(lines[0], "0609211920,Y1421A01,3020,2007-02-02 12:13:14,2007-02-03 13:14:15")
        self.assertEquals(lines[1], "")

        # 1421 does not do acknowledgements.  therefore we expect success=1:
        rows = self.tdb.getrecords('response_log')
        self.assertEquals(len(rows), 2)
        self.assertEquals(rows[0]['success'], '1')
        self.assertEquals(rows[0]['reply'], 'ok')

    def test_1421_with_multiple_email_addr(self):
        _testprep.add_test_client(self.tdb, 'Y1421A01', '1421')
        _testprep.add_test_client(self.tdb, 'Y1421A02', '1421')

        cfg = config.getConfiguration()
        self.assert_(cfg.emailresponders.get('1421')), \
         "1421 email responder block required in config.xml"
        # use custom mappings so we're not dependent on what happens to be in
        # config.xml
        z = cfg.emailresponders['1421']
        z['all_versions'] = 0
        z['mappings']['M'] = ('3020', '') # actual value doesn't matter
        z['mappings']['XD'] = ('3030', '')
        z['clients'] = ['Y1421A01', 'Y1421A02']

        # Multiple email addresses should be separated by a semicolon
        in_addr1 = z['resp_email']
        in_addr2 = 'user@company.com'
        z['resp_email'] = string.join([z['resp_email'],'user@company.com'],';')

        # make sure we don't have 'all_versions' set
        self.assertEquals(z.get('all_versions', 0), 0)

        # store a ticket
        tl = ticketloader.TicketLoader("../testdata/SC1421-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("BSZB45", "Y1421A01")
        raw = raw.replace("COC82", "Y1421A02")
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['SC1421'])
        t.due_date = t.legal_due_date = '2007-02-02 12:13:14'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # set status and stuff
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        for locrow in locates:
            if locrow['client_code'] in z['clients']:

                # set status to M (this will apparently insert the locate into
                # the responder queue)
                sql = """
                 update locate
                 set status = 'M', closed_date = '2007-02-03 13:14:15'
                 where locate_id = %s
                """ % (locrow['locate_id'],)
                self.tdb.runsql(sql)

        # make sure we have a due date
        ticketrow = self.tdb.getrecords("ticket", ticket_id=ticket_id)[0]
        self.assertEquals(ticketrow['due_date'], '2007-02-02 12:13:14')

        # get records from responder_queue
        q = self.tdb.getrecords("responder_queue")
        self.assertEquals(len(q), 2)

        sql = "exec get_pending_responses '1421'"
        rows = self.tdb.runsql_result(sql)
        self.assertEquals(len(rows), 2)

        # Mock the sendmail method
        sendmail_log = []
        def mock_sendmail(*args, **kwargs):
            sendmail_log.append((args, kwargs))

        with Mockery(mail2, 'sendmail', mock_sendmail):

            def create_mock_responder(rows, *args, **kwargs):
                """ Create a mock EmailResponder with a number of rows (a list
                    of dicts) to respond to. """

                class MockEmailResponder(emailresponder.EmailResponder):
                    def get_pending_responses_raw(self, call_center):
                        return [row for row in rows
                                if row['ticket_format'] == call_center]

                return MockEmailResponder(*args, **kwargs)

            e = create_mock_responder(rows, verbose=0)
            e.log.lock = 1
            e.load_responder_data('1421', None)
            self.assertEquals(e.responderdata.name, '1421')
            # Send the log to a string
            e.log.logger.logfile = StringIO()

            e.respond_all()

            # verify fake email
            some_args = sendmail_log[0][0]
            self.assertTrue(in_addr2 in some_args[1])
            self.assertEquals(some_args[2], '1421 Response')

        # What was logged?
        log = e.log.logger.logfile.getvalue()
        e.log.logger.logfile.close()
        self.assertEquals(log.count('Email sent to'), 1)

    def test_1421_with_status_code_skip(self):
        _testprep.add_test_client(self.tdb, 'Y1421A01', '1421')
        _testprep.add_test_client(self.tdb, 'Y1421A02', '1421')

        cfg = config.getConfiguration()
        self.assert_(cfg.emailresponders.get('1421')), \
         "1421 email responder block required in config.xml"
        # use custom mappings so we're not dependent on what happens to be in
        # config.xml
        z = cfg.emailresponders['1421']
        z['all_versions'] = 0
        z['mappings']['M'] = ('3020', '') # actual value doesn't matter
        z['mappings']['XD'] = ('3030', '')
        z['clients'] = ['Y1421A01', 'Y1421A02']
        z['status_code_skip'] = ['ZZZ',]

        # Multiple email addresses should be separated by a semicolon
        in_addr1 = z['resp_email']
        in_addr2 = 'user@company.com'
        z['resp_email'] = string.join([z['resp_email'],'user@company.com'],';')

        # make sure we don't have 'all_versions' set
        self.assertEquals(z.get('all_versions', 0), 0)

        # store a ticket
        tl = ticketloader.TicketLoader("../testdata/SC1421-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("BSZB45", "Y1421A01")
        raw = raw.replace("COC82", "Y1421A02")
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['SC1421'])
        t.due_date = t.legal_due_date = '2007-02-02 12:13:14'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # set status and stuff
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        for locrow in locates:
            if locrow['client_code'] in z['clients']:

                # set status to ZZZ
                sql = """
                 update locate
                 set status = 'ZZZ', closed_date = '2007-02-03 13:14:15'
                 where locate_id = %s
                """ % (locrow['locate_id'],)
                self.tdb.runsql(sql)

        # make sure we have a due date
        ticketrow = self.tdb.getrecords("ticket", ticket_id=ticket_id)[0]
        self.assertEquals(ticketrow['due_date'], '2007-02-02 12:13:14')

        # get records from responder_queue
        q = self.tdb.getrecords("responder_queue")
        self.assertEquals(len(q), 2)

        sql = "exec get_pending_responses '1421'"
        rows = self.tdb.runsql_result(sql)
        self.assertEquals(len(rows), 2)

        # Mock the sendmail method
        sendmail_log = []
        def mock_sendmail(*args, **kwargs):
            sendmail_log.append((args, kwargs))

        with Mockery(mail2, 'sendmail', mock_sendmail):

            def create_mock_responder(rows, *args, **kwargs):
                """ Create a mock EmailResponder with a number of rows (a list
                    of dicts) to respond to. """

                class MockEmailResponder(emailresponder.EmailResponder):
                    def get_pending_responses_raw(self, call_center):
                        return [row for row in rows
                                if row['ticket_format'] == call_center]

                return MockEmailResponder(*args, **kwargs)

            e = create_mock_responder(rows, verbose=0)
            e.log.lock = 0
            e.load_responder_data('1421', None)
            self.assertEquals(e.responderdata.name, '1421')
            e.responderdata.status_code_skip[:] = ['ZZZ'] # No Response
            # Send the log to a string
            e.log.logger.logfile = StringIO()

            e.respond_all()

            # What was logged
            log = e.log.logger.logfile.getvalue()
            e.log.logger.logfile.close()

            self.assertEquals(log.count('Skipping: ZZZ'),2)
            self.assertEquals(log.count('Removing locate from queue'),2)
            self.assertNotEquals(log.find('--- Warning at'),-1)
            self.assertNotEquals(log.find('ticket number:  0609211920 client_code:  Y1421A01 work_date:  2006-09-26 13:30:24 status:  ZZZ'),-1)
            self.assertNotEquals(log.find('ticket number:  0609211920 client_code:  Y1421A02 work_date:  2006-09-26 13:30:24 status:  ZZZ'),-1)
            self.assertNotEquals(log.find('Mail notification sent to'),-1)
            self.assertNotEquals(log.find('0 responses sent'),-1)

            # Verify warning email
            some_args = sendmail_log[0][0]
            self.assertEquals(some_args[2],
              'EmailResponder warning (Skipped status codes)')
            self.assertTrue(
              'ticket number:  0609211920' in some_args[3])
            self.assertTrue(
              'client_code:  Y1421A01' in some_args[3])
            self.assertTrue(
              'work_date:  2006-09-26 13:30:24' in some_args[3])
            self.assertTrue('status:  ZZZ' in some_args[3])
            self.assertTrue('ticket number:  0609211920 client_code:  Y1421A01 work_date:  2006-09-26 13:30:24 status:  ZZZ' in some_args[3])

    def test_NewJersey(self):
        call_center = 'NewJersey'
        client_code = 'ADC'
        # Create test client
        _testprep.add_test_client(self.tdb, client_code, call_center)

        cfg = config.getConfiguration()
        # use custom mappings so we're not dependent on what happens to be in
        # config.xml
        z = {}
        z['name'] = call_center
        z['all_versions'] = 0
        z['translations'] = {}
        z['send_emergencies'] = 1
        z['ack_dir'] = '../testdata/temp_NewJersey_ack'
        z['ack_proc_dir'] = '../testdata/temp_NewJersey_ack_proc'
        z['accounts'] = []
        z['mappings'] = {}
        z['mappings']['N'] = ('No Conflict', '') # actual value doesn't matter
        z['mappings']['NP'] = ('No Plant', '')
        z['mappings']['C'] = ('Cleared/Aerial', '')
        z['mappings']['M'] = ('Marked', '')
        z['mappings']['NC'] = ('No Charge', '')
        z['mappings']['O'] = ('Ongoing', '')
        z['clients'] = []
        z['skip'] = ["ADC", "AE1", "C10", "C11", "CAM", "CC1", "CC2",
                     "CC3", "CC4", "CC5", "CC6", "CC7", "CC8", "CC9",
                     "CCC", "CCF", "CTC", "EG1", "GSC", "GSF", "NJN",
                     "SCA", "SCV", "SJG", "SUJ", "TK1", "TK3", "TK8",
                     "UNI", "VIN"]

        z['resp_email'] = 'nobody@nowhere.com'
        z['sender'] = "UQ Email Responder <responder@utiliquest.com>"
        z['subject'] = "No Responses"
        z['tech_code'] = "UQ"

        cfg.emailresponders[call_center] = z
        cfg.responderconfigdata.add(z['name'], 'email', z)
        cfg.responderconfigdata.gather_skip_data()

        # store a ticket
        tl = ticketloader.TicketLoader('../testdata/newjersey-1.txt')
        raw = tl.tickets[0]
        raw = raw.replace("UW4", "ADC")
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['NewJersey'])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # set status and stuff
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        for locrow in locates:
            # set status to M
            sql = """
             update locate
             set status = 'M', closed_date = '2007-02-03 13:14:15'
             where locate_id = %s
            """ % (locrow['locate_id'],)
            self.tdb.runsql(sql)

        # get records from responder_queue
        q = self.tdb.getrecords("responder_queue")
        self.assertEquals(len(q), 1)

        sql = "exec get_pending_responses '%s'" % (call_center,)
        rows = self.tdb.runsql_result(sql)
        self.assertEquals(len(rows), 1)

        # Mock the sendmail method
        sendmail_log = []
        def mock_sendmail(*args, **kwargs):
            sendmail_log.append((args, kwargs))

        with Mockery(mail2, 'sendmail', mock_sendmail):

            def create_mock_responder(rows, *args, **kwargs):
                """ Create a mock EmailResponder with a number of rows (a list
                    of dicts) to respond to. """

                class MockEmailResponder(emailresponder.EmailResponder):
                    def get_pending_responses_raw(self, call_center):
                        return [row for row in rows
                                if row['ticket_format'] == call_center]

                return MockEmailResponder(*args, **kwargs)

            e = create_mock_responder(rows, verbose=0)
            e.log.lock = 0
            e.load_responder_data(call_center, None)
            self.assertEquals(e.responderdata.name, call_center)
            # Send the log to a string
            e.log.logger.logfile = StringIO()

            e.respond_all()

            # What was logged
            log = e.log.logger.logfile.getvalue()
            e.log.logger.logfile.close()

            self.assertEquals(log.count('Skipping: ADC'), 1)
            self.assertEquals(log.count('Removing locate from queue'), 1)



if __name__ == "__main__":

    unittest.main()

