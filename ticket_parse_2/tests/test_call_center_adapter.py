# test_call_center_adapter.py

import httplib
import os
import site; site.addsitedir('.')
import unittest
#
import call_center_adapter as cca
import call_centers
import config
import dbinterface_old as dbinterface
import mail2
import main
import ticket_db
import ticketloader
import ticketparser
import _testprep

class TestCallCenterAdapter(unittest.TestCase):

    def drop(self, filename, data):
        """ Write a file (containing one or more images) to the
            test_incoming directory. """
        fullname = os.path.join("test_incoming", filename)
        with open(fullname, "wb") as f:
           f.write(data)

    def send(self,xml):
        self.xml = xml

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()

    def tearDown(self):
        _testprep.clear_database(self.tdb)

    def test_LQW1_1(self):
        adapter = cca.get_adapter('LQW1')
        self.assert_(isinstance(adapter, cca.LQW1Adapter))

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","qwest-all.txt"))
        formats = call_centers.cc2format["LQW1"]
        self.assertEquals(len(formats), 5, "This test needs to be updated")
        values = [
            ("OOC", "LOR1"),
            ("UULC", "LWA1"),
            ("IEUCC", "LWA1"),
            ("WOC", "LWA1"),
            ("IDL", "LID1"),
        ]
        for index, raw in enumerate(tl.tickets):
            t = self.handler.parse(raw, formats)
            adapter.adapt(t)
            source, call_center = values[index]
            self.assert_(t.serial_number.startswith(source),
             "Source expected for ticket #%d: %s" % (index, source))
            self.assertEquals(t.ticket_format, call_center)

        # invoking an empty adapter should have no effect, but not raise an
        # error either
        before = t.ticket_format
        empty_adapter = cca.BaseAdapter()
        empty_adapter.adapt(t)
        self.assertEquals(before, t.ticket_format)

    def test_LQW1_2(self):
        adapter = cca.get_adapter('LQW1')
        self.assert_(isinstance(adapter, cca.LQW1Adapter))

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","qwest-all.txt"))
        formats = call_centers.cc2format["LQW1"]
        self.assertEquals(len(formats), 5, "This test needs to be updated")
        raw = tl.getTicket()
        t = self.handler.parse(raw, formats)
        t.serial_number = 'ABC2004050300258'
        self.assertRaises(ValueError,adapter.adapt,t)

    def test_LNG1(self):
        adapter = cca.get_adapter('LNG1')
        self.assert_(isinstance(adapter, cca.LNG1Adapter))

        values = ['LOR1', 'LOR1', 'LOR1', 'LWA1', 'LOR1']

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","Northwest-1.txt"))
        for index, raw in enumerate(tl.tickets):
            t = self.handler.parse(raw, ['Northwest'])
            adapter.adapt(t)
            self.assertEquals(t.ticket_format, values[index])

    def test_LEM1(self):
        adapter = cca.get_adapter('LEM1')
        self.assert_(isinstance(adapter, cca.LEM1Adapter))

        values = ['LOR1', 'LOR1', 'LWA1']

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","OregonEM-1.txt"))
        for index, raw in enumerate(tl.tickets):
            t = self.handler.parse(raw, ['OregonEM'])
            adapter.adapt(t)
            self.assertEquals(t.ticket_format, values[index])

    def test_LCC1_adapters(self):
        # test how LCC1 call_center_adapter works with main.py.
        self.config = config.getConfiguration()
        # replace the real configuration with our test data
        self.config.processes = [
         # one set for general testing...
         {'incoming': 'test_incoming',
          'format': '',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
        ]
        mail2.TESTING = 1
        mail2._test_mail_sent = []
        _testprep.create_test_dirs()
        _testprep.set_cc_routing_file()
        for process in self.config.processes:
            #incoming, name, processed, error, group = process
            _testprep.clear(process['incoming'])
            _testprep.clear(process['processed'])
            _testprep.clear(process['error'])

        self.config.processes[0]['format'] = 'LCC1'

        formats = call_centers.cc2format["LCC1"]
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","LCC1-2007-12-11-00045.txt"))
        for index, raw in enumerate(tl.tickets):
            t = self.handler.parse(raw, formats)
            self.drop("LCC1-WA-%d.txt" % (index,), raw)
        # Assume an OR ticket will do
        tl = ticketloader.TicketLoader(os.path.join("../testdata","Oregon2-1.txt"))
        for index, raw in enumerate(tl.tickets):
            t = self.handler.parse(raw, formats)
            self.drop("LCC1-OR-%d.txt" % (index,), raw)

        m = main.Main(verbose=0, call_center='LCC1')
        # Truncate the log file
        m.log.logger.logfile.truncate(0)
        m.log.lock = 1
        m.run(runonce=1)

        tickets = self.tdb.getrecords("ticket")
        def get_number(call_center):
            return len([t for t in tickets if t['ticket_format'] == call_center])
        self.assertEquals(get_number("LOR1"), 92)
        self.assertEquals(get_number("LWA1"), 1)
        # Count how many times ticket adapted
        num_found = 0
        # rewind the log file
        m.log.logger.logfile.seek(0)
        for line in m.log.logger.logfile.readlines():
            if "Ticket adapted: LCC1 ->" in line:
                num_found += 1
        self.assertEquals(num_found, 93)


if __name__ == "__main__":
    unittest.main()
