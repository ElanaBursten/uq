# test_ticketsummary.py
# Test TicketSummary class and related functions.

import site; site.addsitedir('.')
import unittest
#
import datadir
import date
import summarydetail
import ticket_db
import ticketloader
import ticketparser
import ticketsummary
import _testprep

class TestTicketSummary(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

    def test_summary_insert_and_load(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        summ = handler.parse(raw, ['Atlanta'])
        self.assertEquals(len(summ.data), 50)

        hid, dids = summ.insert(self.tdb)
        self.assertEquals(len(dids), 50)
        self.assertEquals(summ.summary_header_id, hid)
        self.assertEquals(summ.data[0].summary_header_id, hid)
        self.assertEquals(summ.data[0].summary_detail_id, dids[0])

        # now test loading.  TicketSummary.load() should also load the
        # details
        summ2 = ticketsummary.TicketSummary.load(self.tdb, hid)
        self.assert_(isinstance(summ2, ticketsummary.TicketSummary))
        self.assertEquals(len(summ2.data), 50)
        self.assertEquals(summ2.data[0].ticket_number, summ.data[0].ticket_number)
        self.assert_(summ2.data[0].summary_detail_id)
        self.assert_(summ2.data[0].summary_header_id)

        # test load_multiple
        details = summarydetail.SummaryDetail.load_multiple(self.tdb, hid,
                  'summary_header_id')
        self.assertEquals(len(details), 50)
        detail1 = details[0]
        self.assertEquals(detail1.ticket_number, summ.data[0].ticket_number)

    def test_update(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        summ = handler.parse(raw, ['Atlanta'])
        self.assertEquals(len(summ.data), 50)

        hid, dids = summ.insert(self.tdb)
        self.assertEquals(len(dids), 50)
        self.assertEquals(summ.summary_header_id, hid)
        self.assertEquals(summ.data[0].summary_header_id, hid)
        self.assertEquals(summ.data[0].summary_detail_id, dids[0])

        # now update this summary with a new detail
        sd = summarydetail.SummaryDetail.new(summary_header_id=hid,
             item_number="1000", ticket_number="123-456-789",
             ticket_time="00:00", type_code="X", revision=0)
        summ.data.append(sd)

        # update, and check if the new detail was stored
        hid2, dids2 = summ.update(self.tdb)
        self.assertEquals(hid, hid2)
        self.assertEquals(len(dids)+1, len(dids2))
        self.assertEquals(sd.summary_detail_id, dids2[-1])

        # do a "clean" load from the db and check if the detail is there as well
        summ2 = ticketsummary.TicketSummary.load(self.tdb, hid)
        self.assertEquals(len(summ2.data), len(dids2))

    def test_storesummaries_update(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        summ = handler.parse(raw, ['Atlanta'])
        self.assertEquals(len(summ.data), 50)
        hid, dids = summ.insert(self.tdb)

        # fake a list of "updates" by sticking in the same summary
        fakesumm1 = ticketsummary.TicketSummary()
        fakesumm1.format = summ.format
        fakesumm1.data = summ.data
        for d in fakesumm1.data:
            # remove ids, pretend we have "pristine" details
            d.summary_detail_id = None # will be handled by insert
            d.summary_header_id = None # will by handle by TicketSummary.update
        self.tdb.storesummaries_update([fakesumm1], hid, None)

        summ2 = ticketsummary.TicketSummary.load(self.tdb, hid)
        self.assertEquals(len(summ2.data), 100)

    def test_find_latest_summary(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        summ = handler.parse(raw, ['Atlanta'])
        self.assertEquals(len(summ.data), 50)
        hid, dids = summ.insert(self.tdb)

        summ = self.tdb.find_latest_summary('Atlanta', 'ABS01')
        self.assert_(summ)
        self.assert_(isinstance(summ, ticketsummary.TicketSummary))

    def test_updatesummary(self):
        # post a multipart summary
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt")
        # parse and post part 1
        raw1 = tl.tickets[1]
        handler = ticketparser.TicketHandler()
        summpart1 = handler.parse(raw1, ['Atlanta'])
        hid, dids = summpart1.insert(self.tdb)

        # let's check some data
        foo = ticketsummary.TicketSummary.load(self.tdb, hid)
        self.assertEquals(foo.expected_tickets, '0') # should not be set yet
        self.assertEquals(foo.call_center, summpart1.call_center)
        self.assertEquals(len(foo.data), 122-6)
        self.assertEquals(foo.complete, None) # should not be set yet

        raw2 = tl.tickets[2]
        summpart2 = handler.parse(raw2, ['Atlanta'])
        hid2, dids2 = self.tdb.updatesummary(summpart2)

        # summary should be complete now
        foo = ticketsummary.TicketSummary.load(self.tdb, hid)
        self.assertEquals(foo.expected_tickets, str(summpart2.expected_tickets))
        self.assertEquals(len(foo.data), int(foo.expected_tickets))
        self.assertEquals(foo.expected_tickets, '122')
        # Note: SummaryCollection sets the 'complete' field, so that isn't
        # done here

    def test_storesummaries(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt")
        handler = ticketparser.TicketHandler()
        raw1 = tl.tickets[1]
        summ1 = handler.parse(raw1, ['Atlanta'])
        raw2 = tl.tickets[2]
        summ2 = handler.parse(raw2, ['Atlanta'])

        ids = self.tdb.storesummaries([summ1, summ2], None)
        # ids is a list of 2-tuples (hid, dids)
        hid, dids1 = ids[0]

        # let's see if it was stored correctly...
        foo = ticketsummary.TicketSummary.load(self.tdb, hid)
        self.assertEquals(foo.expected_tickets, '122')
        self.assertEquals(len(foo.data), 122)

        # nothing should be marked as changed, we just loaded this stuff
        self.assertEquals(foo.changed(), False)
        for detail in foo.data:
            self.assertEquals(detail.changed(), False)

        # let's store without changing anything
        foo.save(self.tdb)
        self.assertEquals(foo.stats['skipped'], [foo.get_id()])
        self.assertEquals(foo.stats['updated'], [])
        for detail in foo.data:
            self.assertEquals(detail.stats['updated'], [])
            self.assertEquals(detail.stats['skipped'], [detail.get_id()])

        # let's change a thing or two
        foo.summary_date_end = date.Date().isodate()
        foo.data[10].revision = 'C'
        foo.save(self.tdb)
        self.assertEquals(foo.stats['updated'], [foo.get_id()])
        for idx, detail in enumerate(foo.data):
            if idx == 10:
                self.assertEquals(detail.stats['skipped'], [])
                self.assertEquals(detail.stats['updated'], [detail.get_id()])
            else:
                self.assertEquals(detail.stats['updated'], [])
                self.assertEquals(detail.stats['skipped'], [detail.get_id()])



if __name__ == "__main__":

    unittest.main()
