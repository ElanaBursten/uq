# test_testprep.py

import os
import site; site.addsitedir('.')
import unittest
#
import _testprep

class TestTestPrep(unittest.TestCase):

    def test_create_and_delete(self):
        _testprep.create_test_dirs()
        for dir in _testprep.TEST_DIRECTORIES:
            self.assert_(os.path.exists(dir), "Directory %s not found" % (dir,))

        _testprep.delete_test_dirs()
        for dir in _testprep.TEST_DIRECTORIES:
            self.assert_(not os.path.exists(dir),
                 "Directory %s still exists" % (dir,))


if __name__ == "__main__":

    unittest.main()

