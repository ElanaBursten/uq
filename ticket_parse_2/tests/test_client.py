# test_client.py

import site; site.addsitedir('.')
import unittest
#
import _testprep
import client
import ticket_db

class TestClient(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

    def test_001(self):
        self.tdb.delete("client", oc_code="Test001")
        cid1 = _testprep.add_test_client(self.tdb, "Test001", "Atlanta")
        c1 = client.Client.load(self.tdb, cid1)
        self.assert_(c1.alert in ("0", "1")) # a string

        # client_code is a synonym for oc_code, for both getters and setters
        self.assertEquals(c1.oc_code, "Test001")
        self.assertEquals(c1.client_code, "Test001")
        c1.client_code = "Doorag"
        self.assertEquals(c1.oc_code, "Doorag")
        self.assertEquals(c1.client_code, "Doorag")


if __name__ == "__main__":

    unittest.main()
