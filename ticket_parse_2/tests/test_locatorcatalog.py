# test_locatorcatalog.py
# Test methods in LocatorCatalog.

import os
import unittest
import site; site.addsitedir('.')
#
import dbinterface # new
import locatorcatalog
import ticket_db
import ticketloader
import ticketparser
import tools
import _testprep

class TestLocatorCatalog(unittest.TestCase):

    # For nose
    will_fail = False

    def __init__(self, *args, **kwargs):
        self.tdb = ticket_db.TicketDB()
        self.db = dbinterface.detect_interface() # new-style
        unittest.TestCase.__init__(self, *args, **kwargs)

    def test_township_regex(self):
        """ Test LocatorCatalog.re_township regex """
        r = locatorcatalog.LocatorCatalog.re_township
        m = r.search("04S069W12SE")
        self.assertEquals(m.group(1), "04S")
        self.assertEquals(m.group(2), "069W")
        self.assertEquals(m.group(3), "12")

    def test_township_x_regex(self):
        """ Test LocatorCatalog.re_township_x regex """
        r = locatorcatalog.LocatorCatalog.re_township_x
        m = r.search("04S069W12SE")
        self.assertEquals(m.group(1), "04S")
        self.assertEquals(m.group(2), "069W")
        self.assertEquals(m.group(3), "12SE")

    def test_township(self):
        # preparation...
        emp_id = _testprep.add_test_employee(self.tdb, 'test_township')
        area_id = _testprep.add_test_area(self.tdb, 'test_township', emp_id)
        _testprep.add_test_township(self.tdb, '03S', '068W', '15', area_id, 'XX')
        # use a state that doesn't so exist, so we won't clash with any
        # existing records in map_trs

        lc = locatorcatalog.LocatorCatalog(self.tdb)
        row = {"map_page": "03S068W15NE", "work_state": "XX"}
        rr = lc.township(row)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # this should work too...
        row = {"map_page": "03S 068W 15NE", "work_state": "XX"}
        rr = lc.township(row)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # Clean up
        _testprep.delete_test_township(self.tdb, '03S', '068W', '15', area_id, 'XX')
        _testprep.delete_test_area(self.tdb, 'test_township', emp_id)
        _testprep.delete_test_employee(self.tdb, 'test_township')

    def test_township_baseline_1(self):
        """ Test LocatorCatalog.township_extended() [1] """
        self.tdb.delete("map_trs", township='02S', range='068W', section='19',
         basecode='CO')
        emp_id = _testprep.add_test_employee(self.tdb, 'FCO1-001')
        map_id = _testprep.add_test_map(self.tdb, 'FCO1-TESTMAP', 'CO',
                 'NONE', 'CO')
        area_id = _testprep.add_test_area(self.tdb, 'ND-FCO1-TEST', emp_id, map_id)
        _testprep.add_test_township(self.tdb, '02S', '068W', '19', area_id,
         'CO')

        t = {"ticket_number": "123", "ticket_type": "BOZO",
             "work_type": "BURY DROP", "con_name": "THE HOLE BUSINESS",
             "kind": "NORMAL", "map_page": "02S068W19SW", "company": "AZAZEL",
             "work_state": "CO"}
        lc = locatorcatalog.LocatorCatalog(self.tdb)
        d = lc.township_extended(t)
        self.assert_(d, "Result set expected")
        self.assertEquals(d["area_name"], "ND-FCO1-TEST")

        # Cleanup
        _testprep.delete_test_township(self.tdb, '02S', '068W', '19', area_id, 'CO')
        _testprep.delete_test_area(self.tdb, 'ND-FCO1-TEST', emp_id, map_id)
        _testprep.delete_test_map(self.tdb, 'FCO1-TESTMAP', 'CO', 'NONE', 'CO')
        _testprep.delete_test_employee(self.tdb, 'FCO1-001')

    def test_township_baseline_2(self):
        self.tdb.delete('map_trs', township='04S', range='01W', section='24',
                        basecode='AL')
        self.tdb.delete('map_trs', township='04S', range='01W', section='22',
                        basecode='AL')
        emp_id = _testprep.add_test_employee(self.tdb, 'test_tb2')
        area_id = _testprep.add_test_area(self.tdb, 'test_tb2', emp_id)
        _testprep.add_test_township(self.tdb, '04S', '01W', '24', area_id, 'AL')
        _testprep.add_test_township(self.tdb, '04S', '01W', '22', area_id, 'AL')

        # grab a ticket and make sure the map page is what we said above

        tl = ticketloader.TicketLoader("../testdata/AlabamaMobile-1.txt")
        raw = tl.getTicket()    # get any old ticket, as long as it's real
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["AlabamaMobile"])
        self.assertEquals(t.work_state, "AL")
        t.map_page = "T04SR01W24** "

        # test regular expression
        m = locatorcatalog.LocatorCatalog.re_township.search(t.map_page)
        if m:
            self.assertEquals(m.groups(), ("04S", "01W", "24"))
        else:
            self.fail("Regex does not match")

        lc = locatorcatalog.LocatorCatalog(self.tdb)
        d = lc.township_extended(t)

        self.assertEquals(d["area_id"], area_id)

        # try another one; should be routed to the same area/person
        t.map_page = "04SR01W22*E"
        d = lc.township_extended(t)
        self.assertEquals(d["area_id"], area_id)

    def test_township_baseline_short(self):
        # preparation...
        emp_id = _testprep.add_test_employee(self.tdb, 'test_trsbshort')
        area_id = _testprep.add_test_area(self.tdb, 'test_trsbshort', emp_id)
        _testprep.add_test_township(self.tdb, '01N', '072W', '*', area_id, 'XX')

        tl = ticketloader.TicketLoader("../testdata/AlabamaMobile-1.txt")
        raw = tl.getTicket()    # get any old ticket, as long as it's real
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["AlabamaMobile"])
        lc = locatorcatalog.LocatorCatalog(self.tdb)
        t.work_state = "XX"

        # try the "short" version as seen in the Albuquerque format
        # we're looking up 'CO', '01N', '072W', anything
        t.map_page = "01N072W"
        d = lc.township_extended(t)
        self.assertEquals(d["area_id"], area_id)

        # Clean up
        _testprep.delete_test_township(self.tdb, '01N', '072W', '*', area_id, 'XX')
        _testprep.delete_test_area(self.tdb, 'test_trsbshort', emp_id)
        _testprep.delete_test_employee(self.tdb, 'test_trsbshort')

    def test_township_with_zeroes(self):
        emp_id = _testprep.add_test_employee(self.tdb, 'test_zeroes')
        area_id = _testprep.add_test_area(self.tdb, 'test_zeroes', emp_id)
        _testprep.add_test_township(self.tdb, '18S', '03W', '19NE', area_id, 'XX')

        tl = ticketloader.TicketLoader("../testdata/Oregon2-1.txt")
        raw = tl.getTicket()    # get any old ticket, as long as it's real
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["Oregon2"])
        lc = locatorcatalog.LocatorCatalog(self.tdb)
        t.work_state = "XX"

        # try the "short" version as seen in the Albuquerque format
        # we're looking up 'CO', '01N', '072W', anything
        t.map_page = "18S3W19NE"
        d = lc.township_extended(t, (3, 3, 2))
        self.assertEquals(d["area_id"], area_id)

        # Clean up
        _testprep.delete_test_township(self.tdb, '18S', '03W', '19NE', area_id, 'XX')
        _testprep.delete_test_area(self.tdb, 'test_zeroes', emp_id)
        _testprep.delete_test_employee(self.tdb, 'test_zeroes')

    def test_munic(self):
        emp_id, area_id, county_area_id = self.before_munic()

        t = {"ticket_number": "123", "ticket_type": "BOZO",
             "work_type": "BURY DROP", "con_name": "THE HOLE BUSINESS",
             "kind": "NORMAL", "map_page": "DORKS", "company": "AZAZEL",
             "work_state": "XX", "work_county": "test_munic",
             "ticket_format": "FCO1",
             "work_city": "BAR"}
        lc = locatorcatalog.LocatorCatalog(self.tdb)
        rr = lc.munic(t)
        self.assertEquals(rr.area_id, str(area_id))
        self.assertEquals(rr.locator, str(emp_id))

        # Clean up
        self.after_munic(emp_id, area_id)

    def before_munic(self):
        # add test records
        emp_id = _testprep.add_test_employee(self.tdb, 'test_munic')
        new_area_id = _testprep.add_test_area(self.tdb, 'test_munic', emp_id)
        new_county_area_id = _testprep.add_test_county_area(self.tdb,
                             'test_munic', new_area_id)

        return (emp_id, new_area_id, new_county_area_id)

    def after_munic(self,emp_id, new_area_id):
        # delete test records
        _testprep.delete_test_county_area(self.tdb, 'test_munic', new_area_id)
        _testprep.delete_test_area(self.tdb, 'test_munic', emp_id)
        _testprep.add_test_employee(self.tdb, 'test_munic')

    def _test_map_grid_county_state_TX(self):
        # assumes map_page_num is an int

        # preparation...
        NAME = 'test_mgcsTX'
        emp_id = _testprep.add_test_employee(self.tdb, NAME)
        emp_id_2 = _testprep.add_test_employee(self.tdb, NAME+'2')
        area_id = _testprep.add_test_area(self.tdb, NAME, emp_id)
        area_id_2 = _testprep.add_test_area(self.tdb, NAME+'2', emp_id_2)
        map_id = _testprep.add_test_map(self.tdb, "FOOFUR", 'XX', 'BALDWIN', 'FOOFUR')
        _testprep.add_test_map_page(self.tdb, map_id, 456)
        self.tdb.delete("map_cell", map_page_num=456)
        _testprep.add_test_map_cell(self.tdb, map_id, 456, 'A', '1', area_id)
        _testprep.add_test_map_cell(self.tdb, map_id, 456, 'A', 'W', area_id_2)

        t = {"ticket_number": "123", "ticket_type": "BOZO",
             "work_type": "BURY DROP", "con_name": "THE HOLE BUSINESS",
             "kind": "NORMAL", "map_page": "FOOFUR 456,A", "company": "AZAZEL",
             "work_state": "XX", "work_county": "BALDWIN",
             "work_city": "FAIRHOPE"}

        # Here's the idea:
        # MAPSCO 404,J means: ticket_code = MAPSCO, page = 404, x = J, y = 1
        # MAPSCO has map_id 767 (in map table)
        # look for relevant record in map_cell, this has area and locator

        lc = locatorcatalog.LocatorCatalog(self.tdb)
        rr = lc.map_grid_county_state_TX(t)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # any letters after the page number are now put in "Y"
        t["map_page"] = "FOOFUR 456W,A"
        rr = lc.map_grid_county_state_TX(t)
        self.assertEquals(rr.area_id, area_id_2)
        self.assertEquals(rr.locator, emp_id_2)

        # test regexen
        m = lc.re_grid_TX.search("MAPSCO 59,A")
        self.assertEquals(m.groups(), ("MAPSCO", "59", "", "A"))
        m = lc.re_grid_TX.search("MAPSCO 59W,A")
        self.assertEquals(m.groups(), ("MAPSCO", "59", "W", "A"))

        # Clean up
        _testprep.delete_test_map_cell(self.tdb, map_id, 456, 'A', '1', area_id)
        _testprep.delete_test_map_cell(self.tdb, map_id, 456, 'A', 'W', area_id_2)
        _testprep.delete_test_map_page(self.tdb, map_id, 456)
        _testprep.delete_test_map(self.tdb, "FOOFUR", 'XX', 'BALDWIN', 'FOOFUR')
        _testprep.delete_test_area(self.tdb, NAME, emp_id)
        _testprep.delete_test_area(self.tdb, NAME+'2', emp_id_2)
        _testprep.delete_test_employee(self.tdb, NAME)
        _testprep.delete_test_employee(self.tdb, NAME+'2')

    def _test_map_grid_county_state_TX_2(self):
        # assumes map_page_num is a varchar

        # preparation...
        NAME = 'test_mgcsTX2'
        emp_id = _testprep.add_test_employee(self.tdb, NAME)
        emp_id_2 = _testprep.add_test_employee(self.tdb, NAME+'2')
        area_id = _testprep.add_test_area(self.tdb, NAME, emp_id)
        area_id_2 = _testprep.add_test_area(self.tdb, NAME+'2', emp_id_2)
        map_id = _testprep.add_test_map(self.tdb, "ZAXBYS", 'XY', 'BALDWIN', 'ZAXBYS')
        _testprep.add_test_map_page(self.tdb, map_id, '457')
        _testprep.add_test_map_cell(self.tdb, map_id, '457', 'B', '1', area_id)
        _testprep.add_test_map_cell(self.tdb, map_id, '457A', 'C', '1', area_id_2)

        t = {"ticket_number": "123", "ticket_type": "BOZO",
             "work_type": "BURY DROP", "con_name": "THE HOLE BUSINESS",
             "kind": "NORMAL", "map_page": "ZAXBYS 457,B", "company": "AZAZEL",
             "work_state": "XY", "work_county": "BALDWIN",
             "work_city": "FAIRHOPE"}

        # Here's the idea:
        # MAPSCO 404,J means: ticket_code = MAPSCO, page = 404, x = J, y = 1
        # look for relevant record in map_cell, this has area and locator

        lc = locatorcatalog.LocatorCatalog(self.tdb)
        rr = lc.map_grid_county_state_TX_2(t)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # any letters after the page number are now part of the page "number"
        t["map_page"] = "ZAXBYS 457A,C"
        rr = lc.map_grid_county_state_TX_2(t)
        self.assertEquals(rr.area_id, area_id_2)
        self.assertEquals(rr.locator, emp_id_2)

        # Clean up
        _testprep.delete_test_map_cell(self.tdb, map_id, '457', 'B', '1', area_id)
        _testprep.delete_test_map_cell(self.tdb, map_id, '457A', 'C', '1', area_id_2)
        _testprep.delete_test_map_page(self.tdb, map_id, '457')
        _testprep.delete_test_map(self.tdb, "ZAXBYS", 'XY', 'BALDWIN', 'ZAXBYS')
        _testprep.delete_test_area(self.tdb, NAME, emp_id)
        _testprep.delete_test_area(self.tdb, NAME+'2', emp_id_2)
        _testprep.delete_test_employee(self.tdb, NAME)
        _testprep.delete_test_employee(self.tdb, NAME+'2')

    def test_map_grid_county_state_TX(self):
        # run one of these two tests depending on the type of map_page_num.
        if self.tdb.pagenum_is_varchar():
            self._test_map_grid_county_state_TX_2()
        else:
            self._test_map_grid_county_state_TX()
            self.map_grid_county_state_TX_with_cnty()

    def map_grid_county_state_TX_with_cnty(self):
        # assumes map_page_num is an int
        # preparation...
        NAME = 'test_mgcsTX'
        emp_id_1 = _testprep.add_test_employee(self.tdb, NAME+'1')
        emp_id_2 = _testprep.add_test_employee(self.tdb, NAME+'2')
        emp_id_3 = _testprep.add_test_employee(self.tdb, NAME+'3')
        emp_id_4 = _testprep.add_test_employee(self.tdb, NAME+'4')
        map_id_1 = _testprep.add_test_map(self.tdb, "FOOFUR", 'XX', 'BALDWIN', 'FOOFUR')
        map_id_2 = _testprep.add_test_map(self.tdb, "FOOFUR", 'XX', 'GEORGE', 'FOOFUR')
        area_id_1 = _testprep.add_test_area(self.tdb, NAME+'1', emp_id_1,map_id_1)
        area_id_2 = _testprep.add_test_area(self.tdb, NAME+'2', emp_id_2,map_id_1)
        area_id_3 = _testprep.add_test_area(self.tdb, NAME+'3', emp_id_3,map_id_2)
        area_id_4 = _testprep.add_test_area(self.tdb, NAME+'4', emp_id_4,map_id_2)
        _testprep.add_test_map_page(self.tdb, map_id_1, 456)
        _testprep.add_test_map_page(self.tdb, map_id_2, 456)
        _testprep.add_test_map_cell(self.tdb, map_id_1, 456, 'A', '1', area_id_1)
        _testprep.add_test_map_cell(self.tdb, map_id_1, 456, 'A', 'W', area_id_2)
        _testprep.add_test_map_cell(self.tdb, map_id_2, 456, 'A', '1', area_id_3)
        _testprep.add_test_map_cell(self.tdb, map_id_2, 456, 'A', 'W', area_id_4)

        t = {"ticket_number": "123", "ticket_type": "BOZO",
             "work_type": "BURY DROP", "con_name": "THE HOLE BUSINESS",
             "kind": "NORMAL", "map_page": "FOOFUR 456,A", "company": "AZAZEL",
             "work_state": "XX", "work_county": "BALDWIN",
             "work_city": "FAIRHOPE"}

        # Here's the idea:
        # MAPSCO 404,J means: ticket_code = MAPSCO, page = 404, x = J, y = 1
        # MAPSCO has map_id 767 (in map table)
        # look for relevant record in map_cell, this has area and locator

        lc = locatorcatalog.LocatorCatalog(self.tdb)
        rr = lc.map_grid_county_state_TX(t)
        self.assertEquals(rr.area_id, area_id_1)
        self.assertEquals(rr.locator, emp_id_1)

        # any letters after the page number are now put in "Y"
        t["map_page"] = "FOOFUR 456W,A"
        rr = lc.map_grid_county_state_TX(t)
        self.assertEquals(rr.area_id, area_id_2)
        self.assertEquals(rr.locator, emp_id_2)

        t["work_county"] = "GEORGE"
        t["map_page"] = "FOOFUR 456,A"

        lc = locatorcatalog.LocatorCatalog(self.tdb)
        rr = lc.map_grid_county_state_TX(t)
        self.assertEquals(rr.area_id, area_id_3)
        self.assertEquals(rr.locator, emp_id_3)

        t["map_page"] = "FOOFUR 456W,A"

        # any letters after the page number are now put in "Y"
        t["map_page"] = "FOOFUR 456W,A"
        rr = lc.map_grid_county_state_TX(t)
        self.assertEquals(rr.area_id, area_id_4)
        self.assertEquals(rr.locator, emp_id_4)

        # No county
        t["map_page"] = "FOOFUR 456,A"
        t["work_county"] = ""
        rr = lc.map_grid_county_state_TX(t)
        self.assertEquals(rr.area_id, None)
        self.assertEquals(rr.locator, None)

        # No county
        t["map_page"] = "FOOFUR 456,A"
        del t["work_county"]
        self.assertRaises(KeyError,lc.map_grid_county_state_TX,t)

        # Clean up
        _testprep.delete_test_area(self.tdb, NAME+'1', emp_id_1,map_id_1)
        _testprep.delete_test_area(self.tdb, NAME+'2', emp_id_2,map_id_1)
        _testprep.delete_test_area(self.tdb, NAME+'3', emp_id_3,map_id_2)
        _testprep.delete_test_area(self.tdb, NAME+'4', emp_id_4,map_id_2)

        _testprep.delete_test_map_cell(self.tdb, map_id_1, 456, 'A', '1', area_id_1)
        _testprep.delete_test_map_cell(self.tdb, map_id_1, 456, 'A', 'W', area_id_2)
        _testprep.delete_test_map_cell(self.tdb, map_id_2, 456, 'A', '1', area_id_3)
        _testprep.delete_test_map_cell(self.tdb, map_id_2, 456, 'A', 'W', area_id_4)

        _testprep.delete_test_map_page(self.tdb, map_id_1, 456)
        _testprep.delete_test_map_page(self.tdb, map_id_2, 456)

        _testprep.delete_test_map(self.tdb, "FOOFUR", 'XX', 'BALDWIN', 'FOOFUR')
        _testprep.delete_test_map(self.tdb, "FOOFUR", 'XX', 'GEORGE', 'FOOFUR')

        _testprep.delete_test_employee(self.tdb, NAME+'1')
        _testprep.delete_test_employee(self.tdb, NAME+'2')
        _testprep.delete_test_employee(self.tdb, NAME+'3')
        _testprep.delete_test_employee(self.tdb, NAME+'4')


    def test_locatorcatalog_map_area_with_code(self):
        """ Test locatorcatalog.map_area_with_code """

        # preparation
        emp_id = _testprep.add_test_employee(self.tdb, 'test_map_area')
        area_id = _testprep.add_test_area(self.tdb, 'test_map_area', emp_id)
        map_id = _testprep.add_test_map(self.tdb, 'WAHEY', 'XX', 'BLAH', 'WAHEY')
        _testprep.add_test_map_page(self.tdb, map_id, 11)
        _testprep.add_test_map_cell(self.tdb, map_id, 11, 'K', 1, area_id)

        lc = locatorcatalog.LocatorCatalog(self.tdb)
        row = {"map_page": "WAHEY 011 K01"}
        rr = lc.map_area_with_code(row)
        #self.assert_(spam is not None)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        row["map_page"] = "some nonsense here"
        routingresult = lc.map_area_with_code(row)
        self.assertEquals(routingresult.locator, None)

        # Clean up
        _testprep.delete_test_map_cell(self.tdb, map_id, 11, 'K', 1, area_id)
        _testprep.delete_test_map_page(self.tdb, map_id, 11)
        _testprep.delete_test_map(self.tdb, 'WAHEY', 'XX', 'BLAH', 'WAHEY')
        _testprep.delete_test_area(self.tdb, 'test_map_area', emp_id)
        _testprep.delete_test_employee(self.tdb, 'test_map_area')

    def test_locatorcatalog_map_area(self):
        """ Test locator.map_area """

        # preparation
        emp_id = _testprep.add_test_employee(self.tdb, 'test_map_area')
        area_id = _testprep.add_test_area(self.tdb, 'test_map_area', emp_id)
        map_id = _testprep.add_test_map(self.tdb, 'ZOINKS', 'XX', 'BLAH', 'ZOINKS')
        _testprep.add_test_map_page(self.tdb, map_id, 14)
        _testprep.add_test_map_cell(self.tdb, map_id, 14, 'J', 1, area_id)

        lc = locatorcatalog.LocatorCatalog(self.tdb)
        row = {"map_page": "014J01"}
        routingresult = lc.map_area(row, "ZOINKS")
        self.assertEquals(routingresult.area_id, area_id)
        self.assertEquals(routingresult.locator, emp_id)

        # Clean up
        _testprep.delete_test_map_cell(self.tdb, map_id, 14, 'J', 1, area_id)
        _testprep.delete_test_map_page(self.tdb, map_id, 14)
        _testprep.delete_test_map(self.tdb, 'ZOINKS', 'XX', 'BLAH', 'ZOINKS')
        _testprep.delete_test_area(self.tdb, 'test_map_area', emp_id)
        _testprep.delete_test_employee(self.tdb, 'test_map_area')

    def test_map_area_1391(self):
        # preparation
        emp_id = _testprep.add_test_employee(self.tdb, 'test_map_area')
        area_id = _testprep.add_test_area(self.tdb, 'test_map_area', emp_id)
        map_id = _testprep.add_test_map(self.tdb, 'ZOINKS', 'TN', 'DIXIE', 'TN1391')
        _testprep.add_test_map_page(self.tdb, map_id, 14)
        _testprep.add_test_map_cell(self.tdb, map_id, 14, 1, 4, area_id)

        lc = locatorcatalog.LocatorCatalog(self.tdb)
        row = {"map_page": "01404",
               "work_county": "DIXIE",
               "work_state": "TN",
               "ticket_format": "1391",
        }
        routingresult = lc.map_grid_county_state_1391(row)
        self.assertEquals(routingresult.area_id, area_id)
        self.assertEquals(routingresult.locator, emp_id)

        # check if businesslogic actually finds it
        import businesslogic
        logic = businesslogic.getbusinesslogic('1391', self.tdb, None)
        rr = logic.locator(row)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # Clean up
        _testprep.delete_test_map_cell(self.tdb, map_id, 14, 1, 4, area_id)
        _testprep.delete_test_map_page(self.tdb, map_id, 14)
        _testprep.delete_test_map(self.tdb, 'ZOINKS', 'TN', 'DIXIE', 'TN1391')
        _testprep.delete_test_area(self.tdb, 'test_map_area', emp_id)
        _testprep.delete_test_employee(self.tdb, 'test_map_area')

    def test_qtrmin_area_id(self):
        """ Test locatorcatalog.map_qtrmin area_id """

        # preparation
        emp_id = _testprep.add_test_employee(self.tdb, 'test_qtrmin')
        area_id = _testprep.add_test_area(self.tdb, 'test_qtrmin', emp_id)
        _testprep.add_test_qtrmin(self.tdb, '9999A', '9999A', area_id)

        # Test if LocatorCatalog.map_qtrmin returns the area id.
        cat = locatorcatalog.LocatorCatalog(self.tdb)
        row = {'map_page': '9999A9999A', 'ticket_format': 'FCO1'}
        rr = cat.map_qtrmin(row)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # do the "gridlets" have any effect?
        row = {'map_page': '9999A9999A-42', 'ticket_format': 'FCO1'}
        rr = cat.map_qtrmin(row)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # *temp*
        '''
        row = {'map_page': '3847D7710C-42'}
        rr = cat.map_qtrmin(row)
        print ">>", rr
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)
        '''

        # Clean up
        _testprep.delete_test_qtrmin(self.tdb, '9999A', '9999A', area_id)
        _testprep.delete_test_area(self.tdb, 'test_qtrmin', emp_id)
        _testprep.delete_test_employee(self.tdb, 'test_qtrmin')

    def test_qtrmin_latlong(self):
        """ Test locatorcatalog.map_qtrmin area_id """

        # preparation
        emp_id = _testprep.add_test_employee(self.tdb, 'test_qtrmin')
        area_id = _testprep.add_test_area(self.tdb, 'test_qtrmin', emp_id)
        _testprep.add_test_qtrmin(self.tdb, '4305C', '8800A', area_id)

        # Test if LocatorCatalog.map_qtrmin returns the area id.
        cat = locatorcatalog.LocatorCatalog(self.tdb)
        row = {'work_lat': 43.088, 'work_long': -88.0143, 'ticket_format': 'FCO1'}
        rr = cat.map_latlong_qtrmin(row)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # Clean up
        _testprep.delete_test_qtrmin(self.tdb, '4305C', '8800A', area_id)
        _testprep.delete_test_area(self.tdb, 'test_qtrmin', emp_id)
        _testprep.delete_test_employee(self.tdb, 'test_qtrmin')

    def test_NCA_special_routing(self):
        cat = locatorcatalog.LocatorCatalog(self.tdb)
        row = {
         "ticket_number": "123", "ticket_type": "BOZO",
         "work_type": "BURY DROP", "con_name": "THE HOLE BUSINESS",
         "kind": "NORMAL", "map_page": "MAPSCO 1,A", "company": "AZAZEL",
         "work_state": "TX", "work_county": "SAN JOAQUIN",
         "work_city": "TRACY", "client_code": "PBTHAN",
        }

        rows = self.db.getrecords('employee', emp_id="2108")
        name = ''
        if not int(rows[0]['active']) and not int(rows[0]['can_receive_tickets']):
            name = rows[0]['short_name']
            _testprep.set_employee_active(self.db, name)

        rr = cat.NCA_special_routing(row)
        self.assertEquals(rr.area_id, "3637")
        self.assertEquals(rr.locator, "2108")

        row["work_county"] = "AMADOR"
        row["work_city"] = "POOPSIEDOODLE"
        rr = cat.NCA_special_routing(row)
        self.assertEquals(rr.area_id, "3637")
        self.assertEquals(rr.locator, "2108")

        rr = cat.NCA_special_routing({})
        self.assert_(isinstance(rr, tools.RoutingResult))
        self.assert_(rr.locator is None)

        if name:
            _testprep.set_employee_inactive(self.tdb,name)

    def test_SCA_special_routing(self):
        cat = locatorcatalog.LocatorCatalog(self.tdb)
        row = {
         "ticket_number": "123", "ticket_type": "BOZO",
         "work_type": "BURY DROP", "con_name": "THE HOLE BUSINESS",
         "kind": "NORMAL", "map_page": "MAPSCO 1,A", "company": "AZAZEL",
         "work_state": "TX", "work_county": "SAN JOAQUIN",
         "work_city": "TRACY", "client_code": "PBTHAN",
        }

        rr = cat.SCA_special_routing(row)
        self.assertEquals(rr.area_id, "3638")
        self.assertEquals(rr.locator, "1869")

        row["work_county"] = "AMADOR"
        row["work_city"] = "POOPSIEDOODLE"
        rr = cat.SCA_special_routing(row)
        self.assertEquals(rr.area_id, "3638")
        self.assertEquals(rr.locator, "1869")

    def test_polygon(self):
        import config
        cfg = config.getConfiguration()
        cfg.polygon_dir = os.path.abspath("../testdata/maps")

        cat = locatorcatalog.LocatorCatalog(self.tdb)

        row = {
            'work_long': -77.052209,
            'work_lat': 38.909123,
            'ticket_format': 'FMW1',
        }
        rr = cat.polygon(row)
        self.assertEquals(rr.area_id, '11862')

    def test_qtrmin_long(self):
        """ Test locatorcatalog.qtrmin_long """

        # preparation
        emp_id = _testprep.add_test_employee(self.tdb, 'test_qtrmin_long')
        area_id = _testprep.add_test_area(self.tdb, 'test_qtrmin_long', emp_id)
        _testprep.add_test_qtrmin(self.tdb, '3225D', '10149D', area_id, call_center='XYZ')

        # test if find_qtrmin actually works here
        rows = self.tdb.find_qtrmin("3225D", "10149D")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['emp_id'], emp_id)
        self.assertEquals(rows[0]['area_id'], area_id)

        # Test if LocatorCatalog.map_qtrmin returns the area id.
        cat = locatorcatalog.LocatorCatalog(self.tdb)
        row = {'ticket_format': 'XYZ', 'map_page': '3225001014900'}
        rr = cat.map_qtrmin_long(row)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # Clean up
        _testprep.delete_test_qtrmin(self.tdb, '3225D', '10149D', area_id, call_center='XYZ')
        _testprep.delete_test_area(self.tdb, 'test_qtrmin_long', emp_id)
        _testprep.delete_test_employee(self.tdb, 'test_qtrmin_long')

    def test_find_township_baseline_2(self):
        emp_id = _testprep.add_test_employee(self.tdb, "ttb2")
        area_id = _testprep.add_test_area(self.tdb, 'ttb2', emp_id)
        # delete record from map_trs
        self.tdb.delete("map_trs", call_center='XYZ')
        _testprep.add_test_township(self.tdb, '10N', '06E', '23', area_id,
          'NM', 'XYZ')

        rows = self.tdb.find_township_baseline_2('NM', '10N', '06E', '23', 'XYZ')
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['area_id'], area_id)
        self.assertEquals(rows[0]['emp_id'], emp_id)

        cat = locatorcatalog.LocatorCatalog(self.tdb)
        row = {'ticket_format': 'XYZ', 'map_page': '10N06E23W', 'work_state': 'NM'}
        rr = cat.township(row)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # add record to map_trs
        _testprep.add_test_township(self.tdb, '10N', '06E', '23', area_id, 'NM', 'XYZ')
        rows = self.tdb.find_township_baseline_2('NM', '10N', '06E', '23', 'XYZ')
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['area_id'], area_id)
        self.assertEquals(rows[0]['emp_id'], emp_id)

        rr = cat.township(row)
        self.assertEquals(rr.area_id, area_id)
        self.assertEquals(rr.locator, emp_id)

        # Clean up
        _testprep.delete_test_township(self.tdb, '10N', '06E', '23', area_id,
         'NM', 'XYZ')
        _testprep.delete_test_area(self.tdb, 'ttb2', emp_id)
        _testprep.delete_test_employee(self.tdb, "ttb2")


if __name__ == "__main__":

    unittest.main()
