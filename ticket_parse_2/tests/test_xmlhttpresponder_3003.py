# test_xmlhttpresponder_3003.py

from __future__ import with_statement
import os
import unittest
#
from mockery import *
import _testprep
import config
import dbinterface_ado
import xmlhttpresponder
import ticket_db
import ticketloader
import ticketparser
import callcenters.c3003 as c3003

class MockXMLHTTPResponder(xmlhttpresponder.XMLHTTPResponder):
    sent = []
    def _send_response_xml(self, *args, **kwargs):
        self.sent.append((args, kwargs))
        # responder expects a COM object, so we mock one
        class Reply:
            Text = "250 ok"
        return Reply()

class TestXMLHTTPResponder(unittest.TestCase):

    def setUp(self):
        self.dbado = dbinterface_ado.DBInterfaceADO()
        self.tdb = ticket_db.TicketDB(dbado=self.dbado)
        _testprep.clear_database(self.tdb)
        self.test_emp_id = _testprep.add_test_employee(self.tdb, "TestXMLHTTP")
        self.cfg = config.getConfiguration(reload=True)

    def tearDown(self):
        config.getConfiguration(reload=True)

    def test_3003_translore(self, high_profile=0):
        # set up config
        z = {'ack_dir': '',
             'ack_proc_dir': '',
             'mappings': {'M': ('Marked', ''), 
                          'N': ('Cleared', '')},
             'logins': [{'login': 'BSNE', 'password': '123'}],
             'clients': ['BSCA', 'BSEA', 'BSEG', 'BSES', 'BSNE', 'BSNW',
                         'BGAWA', 'BGAWC', 'BGAWM', 'BGAWR', 'BGAWS'],
             'all_versions': 0,
             'id': 'spam',
             'initials': 'UQ',
             'name': '3003',
             'post_url': 'http://example.com/whatgoeshere',
             'resp_url': 'http://example.com/whatgoeshere',
             'send_3hour': 1,
             'send_emergencies': 1,
             'skip': [],
             'status_code_skip': [],
             'translations': {},
             'kind': 'translore'}

        try:
            del self.cfg.xmlhttpresponders['3003']
        except:
            pass
        self.cfg.xmlhttpresponders['3003'] = z
        self.cfg.responderconfigdata.add(z['name'], 'xmlhttp', z, unique=True)

        # add tickets
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "3003-2010-01-26",
             "3003-2010-01-26-01518.txt"))
        raw = tl.tickets[0]

        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['GA3003'])
        t.locates[0].status = 'M'
        assert t.locates[0].client_code == 'BSNE'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        if high_profile == 1:
            # set locate to high profile with Fiber reason
            ref_ids = self.tdb.get_high_profile_reasons_fiber()[0]
            self.tdb.runsql("""
             update locate
             set high_profile = 1, high_profile_reason = %s
             where locate_id = %s
            """ % (ref_ids[0], t.locates[0].locate_id))
        elif high_profile == 2:
            # set locate.high_profile to 1, don't set reason
            self.tdb.runsql("""
              update locate
              set high_profile = 1
              where locate_id = %s
            """ % t.locates[0].locate_id)
            # make sure we have records in call_center_hp?
            # add ticket note containing HP Fiber refs
            emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
            uid = _testprep.add_test_user(self.tdb, emp_id)
            self.tdb.insert_ticket_note(ticket_id, emp_id, 
             '2013-01-01 12:13:14', 'HPR-1,42,1217 BSNE')

        # add a BSNE locate to responder_multi_queue
        sql = """
         select * from locate
         where client_code = 'BSNE' and ticket_id = %s""" % ticket_id
        locate = self.dbado.runsql_result(sql)
        sql = "exec insert_responder_multi_queue 'client', %s" % \
         locate[0]['locate_id']
        self.dbado.runsql(sql)

        # make sure we have one ticket in the database
        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # check responder queues
        rmq = self.tdb.getrecords('responder_multi_queue', ticket_format='3003')
        rq = self.tdb.getrecords('responder_queue', ticket_format='3003')
        self.assertEquals(len(rmq), 1)
        self.assertEquals(len(rq), 1)

        # run mock responder
        resp = MockXMLHTTPResponder(verbose=0)
        resp.sent = []
        resp.respond_all()

        # check output, response log
        self.assertEquals(len(resp.sent), 1)

        # responder_multi_queue should be empty now
        rmq = self.tdb.getrecords('responder_multi_queue', ticket_format='3003')
        rq = self.tdb.getrecords('responder_queue', ticket_format='3003')
        self.assertEquals(len(rmq), 0)
        self.assertEquals(len(rq), 1)

        rl = self.tdb.getrecords('response_log')
        if high_profile > 0:
            self.assertTrue('Marked Fiber' in resp.sent[0][0][0])
            self.assertEquals(rl[0]['response_sent'], 'Marked Fiber')
        else:
            self.assertEquals(rl[0]['response_sent'], 'Marked')
        self.assertEquals(rl[0]['success'], '1')

    def test_3003_translore_hp_single(self):
        self.test_3003_translore(high_profile=1)
    def test_3003_translore_hp_multi(self):
        self.test_3003_translore(high_profile=2)

if __name__ == "__main__":
    unittest.main()

