# test_date.py
# Test cases solely for the date module.

import site; site.addsitedir('.')
import unittest
#
import date
import time
import tools

class TestDate(unittest.TestCase):

    def date(self):
        # a default test date.
        return date.Date("2000-03-03 12:00:00")

    def test_inc_time(self):
        """ Test Date.inc_time() """
        d = self.date()
        d.inc_time(seconds=30)
        self.assertEquals(d.isodate(), "2000-03-03 12:00:30")
        d.inc_time(seconds=40)
        self.assertEquals(d.isodate(), "2000-03-03 12:01:10")
        d.inc_time(minutes=10)
        self.assertEquals(d.isodate(), "2000-03-03 12:11:10")
        d.inc_time(minutes=60)
        self.assertEquals(d.isodate(), "2000-03-03 13:11:10")
        d.inc_time(hours=18)
        self.assertEquals(d.isodate(), "2000-03-04 07:11:10")

    def test_dec_time(self):
        """ Test date.dec_time() """
        d = self.date()
        d.dec_time(seconds=30)
        self.assertEquals(d.isodate(), "2000-03-03 11:59:30")
        d.dec_time(minutes=2)
        self.assertEquals(d.isodate(), "2000-03-03 11:57:30")
        d.dec_time(minutes=60)
        self.assertEquals(d.isodate(), "2000-03-03 10:57:30")
        d.dec_time(hours=5)
        self.assertEquals(d.isodate(), "2000-03-03 05:57:30")
        d.dec_time(hours=9)
        self.assertEquals(d.isodate(), "2000-03-02 20:57:30")
        d.dec_time(hours=19, minutes=56, seconds=29)
        self.assertEquals(d.isodate(), "2000-03-02 01:01:01")

        d = date.Date("2012-02-24 00:00:00")
        d.dec_time(seconds=1)
        self.assertEquals(d.isodate(), "2012-02-23 23:59:59")

    ### moved from test_date_special:

    def test_contents(self):
        """ Test Date contents """
        d = date.Date("2000-03-02 15:09:03")
        self.assertEquals(d.year, 2000)
        self.assertEquals(d.month, 3)
        self.assertEquals(d.day, 2)
        self.assertEquals(d.hours, 15)
        self.assertEquals(d.minutes, 9)
        self.assertEquals(d.seconds, 3)

        #d = date.Date("2012-02-24 24:00:00")
        #self.assertEquals((d.hours, d.minutes, d.seconds), (0, 0, 0))

    def test_leapyears(self):
        """ Test leapyears """
        self.assert_(date.Date("2000-01-01").isleapyear())
        self.assert_(not date.Date("2001-01-01").isleapyear())
        self.assert_(not date.Date("1900-01-01").isleapyear())
        self.assert_(date.Date("2004-01-01").isleapyear())

    def test_inc(self):
        """ Test Date.inc() """
        d1 = date.Date("2000-01-01 00:00:00")
        d2 = d1 + 10
        self.assertEquals(d2.isodate(), "2000-01-11 00:00:00")
        d3 = date.Date("2000-01-31 00:00:00")
        d4 = d3 + 1
        self.assertEquals(d4.isodate(), "2000-02-01 00:00:00")

    def test_selfcheck(self):
        """ Test Date self check """
        s = "2000-01-02 15:16:17"
        d1 = date.Date(s)
        self.assertEquals(d1.isodate(), s)

    def test_check(self):
        """ Test the Date.check() method. """
        values = [
            ("2002-01-01", 1),
            ("2002-01-01 10:24:56", 1),
            ("2002-13-01", 0),
            ("2002-02-29", 0),
            ("2002-04-31", 0),
            ("2002-03-04 24:17:00", 0),
            ("2002-03-04 09:77:66", 0),
        ]
        for v, x in values:
            d = date.Date(v)
            self.assertEquals(d.check(), x)

    def test_isodate(self):
        i = tools.isodate
        self.assertEquals(i("04/08/02 12:05"), "2002-04-08 12:05:00")
        self.assertEquals(i("04/08/02 08AM"), "2002-04-08 08:00:00")
        self.assertEquals(i("04/08/02 08PM"), "2002-04-08 20:00:00")
        self.assertEquals(i("04/08/02 12:05 PM"), "2002-04-08 12:05:00")
        self.assertEquals(i("04/08/02 12:05 AM"), "2002-04-08 00:05:00")
        self.assertEquals(i("04/08/02 12:00 AM"), "2002-04-08 00:00:00")
        self.assertEquals(i("04/08/02"), "2002-04-08 00:00:00")

        # Albuquerque time format
        self.assertEquals(i("04/25/2003 03:17:04 PM"), "2003-04-25 15:17:04")

    def test_get_month_number(self):
        g = date.get_month_number
        self.assertEquals(g("march"), 3)
        self.assertEquals(g("March"), 3)
        self.assertEquals(g("Mar"), 3)
        self.assertEquals(g("MARCH"), 3)
        self.assertEquals(g("January"), 1)
        self.assertEquals(g("foobar"), 0)

    def test_date_t9(self):
        """ Test Date <-> time.struct_time conversion """
        t = time.localtime(time.mktime((2002, 12, 24, 9, 24, 35, 0, 0, 0)))
        self.assert_(isinstance(t, time.struct_time))
        d = date.Date(t)
        self.assertEquals(d.isodate(), "2002-12-24 09:24:35")



if __name__ == "__main__":

    unittest.main()
