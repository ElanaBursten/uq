# test_work_order_audit_parser.py

import os
import site; site.addsitedir('.')
import unittest
#
import ticketloader
import work_order
import work_order_audit_parser
from formats import Lambert
import _testprep

class TestWorkOrderAuditParser(unittest.TestCase):

    def test_Lambert(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             'work-order-audits', 'LAM01', 'LAM01-2011-03-07-00129-A01.txt'))
        raw = tl.tickets[0]

        woap = Lambert.WorkOrderAuditParser()
        woa = woap.parse(raw)

        self.assertEquals(woa.wo_source, 'LAM01')
        self.assertEquals(woa.summary_date, '2011-02-16 00:00:00')
        self.assertEquals(len(woa.entries), 40)
        self.assertEquals(woa.entries[0].client_wo_number, 'V0002678')
        self.assertEquals(woa.entries[-1].client_wo_number, 'V0002921')

        # test another ticket, updated format
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             'work-order-audits', 'LAM01', 'LAM01-2011-03-18-00005-A01.txt'))
        raw = tl.tickets[0]

        woa = woap.parse(raw)

        self.assertEquals(woa.wo_source, 'LAM01')
        self.assertEquals(woa.summary_date, '2011-03-18 00:00:00')
        self.assertEquals(len(woa.entries), 10)
        self.assertEquals(woa.entries[0].client_wo_number, 'V0005210')
        self.assertEquals(woa.entries[-1].client_wo_number, 'V0005238')

        # and another one...
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             'work-order-audits', 'LAM01', 'LAM01-2011-03-23-00051-A01.txt'))
        raw = tl.tickets[0]

        woa = woap.parse(raw)

        self.assertEquals(woa.wo_source, 'LAM01')
        self.assertEquals(woa.summary_date, '2011-03-23 00:00:00')
        self.assertEquals(len(woa.entries), 50)
        self.assertEquals(woa.entries[0].client_wo_number, 'V0002698')
        self.assertEquals(woa.entries[-1].client_wo_number, 'V0002955')

if __name__ == "__main__":
    unittest.main()
