# test_errorhandling2.py
# Created: 2002.11.05 HN

import site; site.addsitedir('.')
import StringIO
import unittest
#
import dbinterface_old as dbinterface
import dbinterface_ado
import emailtools
import errorhandling2
import ticket_db
import version

class TestErrorHandling2(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

    def make_logger(self):
        import config
        cfg = config.getConfiguration()
        cc_emails = emailtools.Emails(self.tdb)
        logger = errorhandling2.ErrorHandler(logdir='logs',
                 smtpinfo=cfg.smtp_accs[0],
                 me="test_ErrorHandler",
                 cc_emails=cc_emails,
                 admin_emails=cfg.admins)
        logger.subject = "TicketParser Error Notification"
        logger.defer_mail = 1
        logger.verbose = 0
        logger.lock = 0
        # setting lock = 0 here will queue the mails rather than bailing out
        # of the _queuemail() method

        return logger

    def test_sql_attribute(self):
        """ Test if 'sql' attribute is present in database exceptions """
        db = dbinterface_ado.DBInterfaceADO()
        # let's get some invalid SQL
        sql = "select blah from fnorb"
        try:
            db.runsql_result(sql)
        except Exception, e:
            #self.assert_(hasattr(e, "sql"))
            self.assertTrue(hasattr(e, 'data'))
            self.assertTrue(e.data.has_key('sql'))

    def test_ErrorHandler(self):
        logger = self.make_logger()
        logger.lock = 0
        # setting lock = 0 here will queue the mails rather than bailing out
        # of the _queuemail() method

        packet = errorhandling2.errorpacket()
        logger._queuemail(packet)
        logger._queuemail(packet, 'Atlanta')
        self.assert_(logger.cc_bags['Atlanta'])
        logger.lock = 1
        # setting lock = 1 here will leave the mails in the queue rather than
        # sending them... or so I hope ;-)

        e = errorhandling2.ErrorHandler()
        line = e._version_line()
        self.assertEquals(line, "(Version: %s)" % (version.__version__,))

    def test_messages(self):
        MESSAGE = "Timeout expired, dude"
        try:
            raise Exception(MESSAGE)
        except:
            msg = errorhandling2.get_exception_message()
            self.assertEquals(msg, MESSAGE)
            self.assertEquals(errorhandling2.is_database_glitch(msg), True)
        else:
            self.fail("Failed to flag exception as database glitch")

    def test_dumptofile(self):
        logger = self.make_logger()
        try:
            raise dbinterface.DBException("bwahaha", timeout=3, sql="bogus")
        except:
            ep = errorhandling2.errorpacket(extra="31337")
            s = StringIO.StringIO()
            logger._dumptofile(ep, s)
            lines = [line.rstrip() for line in s.getvalue().split('\n')]
            self.assertTrue('traceback:' in lines)
            self.assertTrue('data.extra:' in lines)
            self.assertTrue('sql:' in lines)



if __name__ == "__main__":

    unittest.main()

