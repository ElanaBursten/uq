# test_archiver.py

from __future__ import with_statement
import copy
import StringIO
import os
import shutil
import site; site.addsitedir('.')
import sys
import tarfile
import tempfile
import time
import traceback
import unittest
#
import archiver
import config
import date
import filetools
import tools
from mockery import *

def create_test_dirs(directories):
    for dir in directories:
        if not os.path.exists(dir):
            os.mkdir(dir)
        clear(dir)

create_test_dirs.will_fail = True

def clear(dir, strict=0):
    """ Empty a directory.  (For now, this assumes that there are no
        subdirectories. """
    if os.path.exists(dir) or strict:
        try:
            filenames = os.listdir(dir)
        except WindowsError:
            import time; time.sleep(0.5)
            filenames = os.listdir(dir) # try again
        for filename in filenames:
            fullname = os.path.join(dir, filename)
            try:
                os.remove(fullname)
            except:
                pass # may not have permission

def delete_test_dirs(directories):
    for dir in directories:
        shutil.rmtree(dir)

delete_test_dirs.will_fail = True

def drop_file(directory, filename, timestamp):
    fullname = os.path.join(directory, filename)
    f = open(fullname, 'w')
    f.write("blah") # dummy contents
    f.close()
    # set date/time
    d = date.Date(timestamp)
    t = d.as_tuple()
    n = time.mktime(t)
    os.utime(fullname, (n, n))

def with_config_copy(f):
    """ Decorator to run a test with a custom configuration (which is assumed
        to be in self.conf_copy). """
    def w(self):
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            f(self)
    w.__name__ = f.__name__
    return w

class TestArchiver(unittest.TestCase):

    def setUp(self):
        self.configuration = config.getConfiguration()
        self.conf_copy = copy.deepcopy(self.configuration)
        self.cfg = copy.deepcopy(self.configuration)
        clear(os.path.join('c:\\temp'))
        self.conf_copy.archiver['archive_dir'] = os.path.join('c:\\temp')

        self.TEST_DIRECTORIES = [os.path.join("..","testdata","arch")]
        tmpdir = tempfile.mkdtemp()
        self.conf_copy.archiver['additional_dirs'] = [tmpdir]
        self.TEST_DIRECTORIES.append(tmpdir)

        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'format': 'ABC',
          'processed': 'processed'},
         {'format': 'ABC',
          'processed': 'test_processed_ABC'},
         {'format': 'DEF',
          'processed': 'test_processed_DEF'}]

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['format', 'processed']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)

    @with_config_copy
    def test_find_files_to_be_archived(self):
        ar = archiver.Archiver(current_date="2006-04-07", verbose=0)
        ar.prepare()

        # drop files in testdata/arch
        p = self.conf_copy.processes[0]['processed']
        drop_file(p, "one", "2006-03-01")
        drop_file(p, "two", "2006-03-02")
        drop_file(p, "three", "2006-04-07")

        # set some dates
        ar.find_files_to_be_archived(self.conf_copy.processes[0]['processed'])
        self.assertEquals(len(ar.to_be_archived), 2)

        # check uniform slashes; we don't want "testdata/arch\one"
        self.assertEquals(ar.to_be_archived[0],
          os.path.join(self.conf_copy.processes[0]['processed'], 'one'))

    @with_config_copy
    def test_archive_all_1(self):
        # drop files in testdata/arch
        p = self.conf_copy.processes[0]['processed']
        drop_file(p, "one", "2006-03-01")
        drop_file(p, "two", "2006-03-02")
        drop_file(p, "three", "2006-04-07")

        with filetools.Stdout(StringIO.StringIO()) as out:
            ar = archiver.Archiver(current_date="2006-04-07", verbose=0)
            ar.make = True
            ar.delete = True
            ar.archive_all(call_centers_to_archive='ABC')
            targetdir = self.conf_copy.archiver['archive_dir']
            tf = tarfile.open(
                 os.path.join(targetdir, 'ABC-2006-04-02.tar.bz2'), 'r|bz2')
            files_in_archive = [os.path.normpath(name)
                                for name in tf.getnames()]
            self.assertEquals(len(files_in_archive),2)
            self.assertEquals(files_in_archive[0],
              os.path.join('processed', 'one'))
            self.assertEquals(files_in_archive[1],
              os.path.join('processed', 'two'))
            output = out.getvalue()
            self.assertNotEquals(output.find('Skipping: DEF'), -1)

            out.close()

    @with_config_copy
    def test_archive_none(self):
        # What happens when there is nothing to archive?

        with filetools.Stdout(StringIO.StringIO()) as out:
            p = self.conf_copy.processes[0]['processed']
            drop_file(p, "one", "2006-04-07")
            drop_file(p, "two", "2006-04-07")
            drop_file(p, "three", "2006-04-07")
            ar = archiver.Archiver(current_date="2006-04-07", verbose=0)
            ar.make = True
            ar.delete = True
            ar.archive_all(call_centers_to_archive='ABC')
            self.assertNotEquals(
              out.getvalue().find('No files found for archive'), -1)
            out.close()

    @with_config_copy
    def test_archive_empty(self):
        # What happens when the directory is empty?

        with filetools.Stdout(StringIO.StringIO()) as out:
            ar = archiver.Archiver(current_date="2006-04-07", verbose=0)
            ar.make = True
            ar.delete = True
            ar.archive_all(call_centers_to_archive='ABC')
            self.assertNotEquals(
              out.getvalue().find('No files found for archive'), -1)
            out.close()

    @with_config_copy
    def test_archive_no_call_centers_1(self):
        # What happens when a call_center is not specified?

        with filetools.Stdout(StringIO.StringIO()) as out:
            p = self.conf_copy.processes[0]['processed']
            drop_file(p, "one", "2006-03-01")
            drop_file(p, "two", "2006-03-02")
            drop_file(p, "three", "2006-04-07")
            ar = archiver.Archiver(current_date="2006-04-07", verbose=0)
            ar.make = True
            ar.delete = True
            ar.archive_all(call_centers_to_archive=None)
            targetdir = self.conf_copy.archiver['archive_dir']
            tf = tarfile.open(
              os.path.join(targetdir, 'ABC-2006-04-02.tar.bz2'), 'r|bz2')
            files_in_archive = [os.path.normpath(name)
                                for name in tf.getnames()]
            self.assertEquals(len(files_in_archive),2)
            self.assertEquals(files_in_archive[0],
              os.path.join('processed', 'one'))
            self.assertEquals(files_in_archive[1],
              os.path.join('processed', 'two'))
            output = out.getvalue()
            self.assertEquals(output.count('No files found for archive.'), 3)
            out.close()

    @with_config_copy
    def test_archive_main(self):
        # drop files in testdata/arch
        p = self.conf_copy.processes[0]['processed']
        drop_file(p, "one", "2006-03-01")
        drop_file(p, "two", "2006-03-02")
        drop_file(p, "three", "2006-04-07")

        targetdir = self.conf_copy.archiver['archive_dir']
        output_path = os.path.join(targetdir, 'FWP2-2006-04-02.tar.bz2')

        whereami = os.path.dirname(os.path.abspath(__file__))
        cmd = tools.fmt(sys.executable,
                        os.path.join(whereami, "..", "archiver.py"),
                        " -d 2006-04-07 d FWP2")
        cin, cout, cerr = os.popen3(cmd)
        proc = cout
        output = proc.read()  # effectively executes the pipe

        if len(output) < 80 \
        or not os.path.exists(output_path):
            errmsg = tools.fmt("popen() command did not execute properly\n",
                     "**command: %r\n" % cmd,
                     "**stdout:\n%s\n" % output,
                     "**stderr:\n%s\n" % cerr.read(),
                     sep="")
            self.fail(errmsg)

        tf = tarfile.open(output_path, 'r|bz2')

        files_in_archive = [os.path.normpath(name) for name in tf.getnames()]
        self.assertEquals(len(files_in_archive),2)
        self.assertEquals(files_in_archive[0],os.path.join('processed','one'))
        self.assertEquals(files_in_archive[1],os.path.join('processed','two'))

        self.assertTrue('Archive date is 2006-04-02' in output)
        self.assertTrue('3 files found' in output)
        self.assertTrue('File marked for archiving: processed\\one' in output)
        self.assertTrue('File marked for archiving: processed\\two' in output)
        self.assertTrue('Archiving: FWP2' in output)


if __name__ == '__main__':

    unittest.main()

