# test_summaryparser_bulk.py

import os
import unittest
#
import _testprep
import datadir
import ticketloader
import ticketparser
import tools # safeglob

ROOT_PATH = os.path.join(_testprep.TICKET_PATH, "audits")

DATA = [
    # (path in testdata/audits; formats)
    ("3004-2010-01-13", ["GA3004", "GA3004a"]),
    ("FAU4-2010-03-19", ["Atlanta4", "Atlanta4a"]),
    ("FAU5-2010-01-13", ["Atlanta5", "Atlanta5a"]),
    ("FMW4-2009-12-15", ["Washington4"]),
    ("NCA2-2010-03-18", ["NorthCaliforniaATT"]),
    ("NewJersey4-2010-03-15", ["NewJersey4"]),
    ("NewJersey-2010-02-15", ["NewJersey", "NewJersey2010"]),
    ("NewJersey2-2010-05-28", ["NewJersey2a"]),
    ("NewJersey2-2010-06-18", ["NewJersey2010B"]),
    ("OCC2-2010-06-11", ["FairfaxXML"]),
    ("OCC2-2010-06-11", ["DelawareXML"]),
    ("OCC2-2010-06-11", ["WashingtonXML"]),
    ("3004-2010-06-29", ["GA3004b"]),    # SENTRi
    ("3004-2010-06-29", ["Atlanta5b"]),  # SENTRi
    ("FTL3-2010-07-01", ["TennesseeNC"]),
    ("142x-2010-07-29", ["GreenvilleNew"]),
    ("FJL4-2010-12-15", ["MississippiKorterra"]),
    ('3005-2011-01-19', ['TN3005']),
    ('1181-2011-03-26', ['WV1181']),
    ('1181-2011-04-01', ['WV1181Alt']),
    ('SCA3-2011-11-06', ['Arizona']),
    ('NewJersey4-2011-11-06', ['NewJersey4']),
    ('1182-2012-02-25-suffix', ["WV1181Korterra"]),
    ('1182-2012-02-25-suffix', ["WV1181Alt"]),
    ('1421-2012-03-09', ["SC1421A"]),
    ('1421-2012-03-09', ["SC1425A"]), # !
    ('1421-2012-03-09', ["SC1422A"]),
    ("3006-2012-11-01", ['GA3006']),
    ('FMW1-2012-11-03-merged', ['Washington']),
    ('FNV1-2013-01-08', ['Nashville2013']),
    ('LOR3-2013-03-09', ['Oregon4']),
    ('LOR4-2013-03-09', ['Oregon5']),
    ('LWA2-2013-03-09', ['WashingtonState2']),
    ('LWA3-2013-03-09', ['WashingtonState3']),
    ('SCA10-2013-05-04', ['SouthCalifornia10']),
    ('NewJersey4-2013-06-06', ['NewJersey4']),
    ('FAU2-2013-06-12', ['AL811']),
    ('NewJersey6-2013-11-01', ['NewJersey6']),
    ('NewJersey7-2013-11-01', ['NewJersey7']),
    ('NewJersey9-2014-08', ['NewJersey9']),
]

class TestSummaryParserBulk(unittest.TestCase):
    def setUp(self):
        self.handler = ticketparser.TicketHandler()

def inject_method(id, path, formats):

    def m(self):
        filenames = tools.safeglob(os.path.join(ROOT_PATH, path, "*.txt"))
        for filename in filenames:
            tl = ticketloader.TicketLoader(filename)
            for idx, raw in enumerate(tl):
                error_info = "%r %r #%d" % (filename, formats, idx+1)
                t = self.handler.parse(raw, formats)

                # additional sanity tests
                self.assertTrue(t.client_code > "", error_info)
                self.assertTrue(t.summary_date > "", error_info)
                self.assertTrue(t._has_header, error_info)
                self.assertTrue(t._has_footer, error_info)
                self.assertEquals(t.expected_tickets, len(t.data), error_info)

    m.__name__ = method_name = "test_%03d_%s" % (id, formats[0])
    setattr(TestSummaryParserBulk, method_name, m)

# inject test cases dynamically
for id, (path, formats) in enumerate(DATA):
    inject_method(id+1, path, formats)

if __name__ == "__main__":
    unittest.main()

