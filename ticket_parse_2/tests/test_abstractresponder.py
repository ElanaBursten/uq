# test_abstractresponder.py

import os
import site; site.addsitedir('.')
import unittest
#
import abstractresponder
import businesslogic
import ticket_db

consolidated_response_groups = [
    ['KC', 'KCG'],
    ['KD', 'KDG'],
    ['KE', 'KEG'],
    ['KF', 'KFG'],
    ['KG', 'KGG'],
]

def consolidate_response_status(self, rows):
    assert rows, "consolidate_response_status cannot be called with 0 rows"
    statuses = [row['status'] for row in rows]
    if 'M' in statuses:
        return 'M'
    elif 'C' in statuses:
        return 'C'
    else:
        return rows[0]['status']

class TestAbstractResponder(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

    #
    # tests for consolidating responses

    def test_consolidate_status(self):
        # really a businesslogic test, but we need it here
        import callcenters.c7501
        b = callcenters.c7501.BusinessLogic
        b.consolidated_response_groups = consolidated_response_groups
        b.consolidate_response_status = consolidate_response_status

        logic = businesslogic.getbusinesslogic("7501", self.tdb, [])
        rows = [
          {'ticket_id': 1, 'client_code': 'KC',  'status': 'X'},
          {'ticket_id': 1, 'client_code': 'KCG', 'status': 'Y'},
        ]
        status = logic.consolidate_response_status(rows)
        self.assertEquals(status, 'X')

        rows[0]['status'] = 'C'
        status = logic.consolidate_response_status(rows)
        self.assertEquals(status, 'C')

        rows[1]['status'] = 'M'
        status = logic.consolidate_response_status(rows)
        self.assertEquals(status, 'M')

    def test_consolidate_responses(self):
        import callcenters.c7501
        b = callcenters.c7501.BusinessLogic
        b.consolidated_response_groups = consolidated_response_groups
        b.consolidate_response_status = consolidate_response_status
        logic = businesslogic.getbusinesslogic("7501", self.tdb, [])
        rows = [
          {'ticket_id': 1, 'client_code': 'KC',  'status': 'X',},
          {'ticket_id': 1, 'client_code': 'KCG', 'status': 'Y',},
          {'ticket_id': 1, 'client_code': 'KA',  'status': 'P',},
          {'ticket_id': 2, 'client_code': 'XYZ',  'status': 'P',},
        ]

        class MockResponder(abstractresponder.AbstractResponder):
            pass

        r = MockResponder(verbose=0)
        crows = r.consolidate_responses("7501", rows)
        self.assertEquals(len(crows), 3)
        self.assertEquals(crows[0]['client_code'], 'KA')
        self.assertEquals(crows[1]['client_code'], 'XYZ')
        self.assertEquals(crows[2]['client_code'], 'KC')
        self.assertEquals(crows[2]['status'], 'X')

        rows[0]['status'] = 'C'
        crows = r.consolidate_responses("7501", rows)
        self.assertEquals(len(crows), 3)
        self.assertEquals(crows[2]['client_code'], 'KC')
        self.assertEquals(crows[2]['status'], 'C')

        rows[1]['status'] = 'M'
        crows = r.consolidate_responses("7501", rows)
        self.assertEquals(len(crows), 3)
        self.assertEquals(crows[2]['client_code'], 'KC')
        self.assertEquals(crows[2]['status'], 'M')

    def test_response_log(self):
        # Based on actual error received 2005-10-17.  The error message returned
        # by the XMLHTTPServer had apostrophes in it, *and* was too long.

        class MockResponder(abstractresponder.AbstractResponder):
            def response_is_success(self, row, resp):
                return False
        mr = MockResponder(verbose=0)
        self.tdb.delete("response_log")

        row = {u'status': 'M', u'work_state': 'PA',
               u'insert_date': '2005-10-16 22:19:39', u'ticket_format': '7501',
               u'ticket_type': 'NEW XCAV RTN', u'locate_id': '146445249',
               u'client_code': 'KA', u'ticket_id': '23944481',
               u'ticket_number': '2856371'}
        reply = "soap:Server Server was unable to process request. --> "\
                + "Cannot open database requested in login ''Phoenix''. "\
                + "Login fails.\nLogin failed for user ''brresvc''."
        resp = ('003', '0', reply)
        mr.log_response(row, resp)

        rows = self.tdb.getrecords("response_log")
        self.assertEquals(rows[0]['locate_id'], '146445249')
        self.assert_(rows[0]['reply'].startswith("soap:Server Server"))

    def test_send_response_for_all_versions(self):

        class MockResponder(abstractresponder.AbstractResponder):
            simple_log = []
            def send_response(self, *args, **kwargs):
                self.simple_log.append((args, kwargs))

        # get a SCA1 ticket
        import ticketparser, ticketloader
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","SouthCalifornia-1.txt"))
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["SouthCalifornia"])
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        # add another version or two for it
        t.revision = '01A'
        self.tdb.add_ticket_version(ticket_id, t, "one.txt")
        t.revision = '02A'
        self.tdb.add_ticket_version(ticket_id, t, "two.txt")
        # set some variables
        client_code = t.locates[0].client_code
        response_code = 'X'
        explanation = 'boo'

        # then run responder
        mr = MockResponder(verbose=0)
        mr.send_response_for_all_versions(ticket_id, t.ticket_number, client_code,
         response_code, explanation, '')

        # check if we sent a response for each version
        self.assertEquals(len(mr.simple_log), 2)
        self.assertEquals(mr.simple_log[0][0][5], '01A')
        self.assertEquals(mr.simple_log[1][0][5], '02A')

if __name__ == "__main__":
    unittest.main()

