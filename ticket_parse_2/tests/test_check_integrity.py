# test_check_integrity.py

import site; site.addsitedir('.')
import sys
import unittest
#
import check_integrity
import config
import tools

class TestCheckIntegrity(unittest.TestCase):

    # XXX
    # There is room for a bunch of tests here, that set certain conditions and
    # then verify if the IntegrityChecker class correctly finds and flags them.

    def test_001(self):
        """ Simple object creation and run. """
        cfg = config.getConfiguration()
        ic = check_integrity.IntegrityChecker(cfg, send_email=0)

        # suppress output to stdout
        oldstdout = sys.stdout
        sys.stdout = tools.NullWriter()
        try:
            ic.run()
        finally:
            sys.stdout = oldstdout

if __name__ == "__main__":
    unittest.main()
