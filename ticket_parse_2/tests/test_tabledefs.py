# test_tabledefs.py

import unittest
#
import dbinterface # new
import sqlmaptypes as smt
import tabledefs

class TestTableDefs(unittest.TestCase):

    def setUp(self):
        self.db = dbinterface.detect_interface()
        self.td = tabledefs.TableDefs(self.db)

    def test_simple(self):
        client_fields = self.td.client # looked up on demand
        f1 = client_fields[0]
        self.assertEquals(f1[0], 'client_id')
        self.assertTrue(isinstance(f1[1], smt.SQLInt))

    def test_lookup(self):
        t = self.td.lookup('client', 'client_id')
        self.assertTrue(isinstance(t, smt.SQLInt))

    def test_fields_as_dict(self):
        d = self.td.fields_as_dict('ticket')
        self.assertTrue(d.has_key('ticket_number'))

    def test_table_exists(self):
        self.assertEquals(tabledefs.table_exists(self.db, 'ticket'), True)
        self.assertEquals(tabledefs.table_exists(self.db, 'bogus123'), False)

if __name__ == "__main__":
    unittest.main()

