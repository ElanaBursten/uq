# mock_objects.py
# Various mock objects used for testing.

import sys
#
import mockery

attempt = 0 # used in test_main_2.py for testing geocoder

class MockSMTP:
    _sent = [] # class attribute, so it persists even after a MockSMTP
               # instance has been destroyed
    def __init__(self, *args, **kwargs):
        self._mock_args = (args, kwargs)
        self.__class__._sent = [] # reset at each invocation
        self.sock = mockery.MockObject()
    def sendmail(self, *args, **kwargs):
        self._sent.append((args, kwargs))
    def quit(self, *args, **kwargs):
        pass
    def __getattr__(self, name):
        print >> sys.stderr, "Unknown attribute:", name
        return mockery.MockObject()


