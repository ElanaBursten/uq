# test_polygon.py

import unittest
import site; site.addsitedir('.')
#
import polygon

class TestPolygon(unittest.TestCase):

    def test01(self):
        "A simple rectangle"

        points = [(0,0), (1,0), (1,1), (0,1), (0,0)]
        i = polygon.inside((0.5, 0.5), points)
        self.assertEquals(i, True)

        points = [(0,0), (1,0), (1,1), (0,1), (0,0)]
        i = polygon.inside((1.1, 0.5), points)
        self.assertEquals(i, False)

    def test02(self):
        "A more complex polygon"

        points = [(1,-1), (2,2), (5,3), (7,2), (6,-1), (5,1), (4,-1), (3,0)]
        data = [
            ((2,0), True),
            ((0,0), False),
            ((4,0), True),
            ((5,2), True),
            ((6,0), True),
        ]
        for point, expected in data:
            z = polygon.inside(point, points)
            self.assertEquals(z, expected, "Unexpected result for %s" % (point,))


if __name__ == "__main__":

    unittest.main()
