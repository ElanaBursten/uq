# test_ticketparser_bulk.py

import os
import string
import unittest
#
import _testprep
import datadir
import ticketloader
import ticketparser
import tools # safeglob

# check :-
# The default is to just check if tickets in a given directory parse (using
# the specified format).  Additionally, we can pass a list of functions,
# each of which need to return True for each ticket.  This way, we can
# easily test if certain fields are filled properly, etc.

def check(format, path, conditions=None):
    return {'format': format,
            'path': os.path.join(_testprep.TICKET_PATH, *path),
            'conditions': conditions or []}

DATA = [
    check("NewJersey2010", ("tickets", "NewJersey-2010-02-15")),
    check("NewJersey2010B", ("tickets", "NewJersey-2010-02-15")),
    check("NewJersey4", ("tickets", "NewJersey4-2010-03-15")),
    check("PA7501", ("tickets", "7501-2010-01-20"), [
        lambda t: t.work_description > ""]),
    check("FairfaxXML", ("tickets", "FairfaxXML-2010-05-06")),
    check("DelawareXML", ("tickets", "FairfaxXML-2010-05-06")), # same format
    check("WashingtonXML", ("tickets", "FairfaxXML-2010-05-06")), # ditto
    check("VUPSNewOCC3", ("tickets", "OCC3-2010-05-18")),
    check("NC811", ("tickets", "FCT3-2010-05-19")),
    check("TN811", ("tickets", "FCT3-2010-05-19")),
    check("GA3004", ("tickets", "3004-2010-06-29")),
    check("Atlanta5", ("tickets", "3004-2010-06-29")),
    check("TennesseeNC", ("tickets", "FCT3-2010-07-01")),
    check("SouthCaliforniaATT", ("tickets", "SCA6-2010-07-01-remarks")),
    check("NorthCaliforniaATT", ("tickets", "NCA2-2010-07-01-remarks")),
    check("NewJersey2010", ("tickets", "NewJersey-2011-07-expiration")),

    # Mantis #2624
    check("SC1421", ("tickets", "142x-2010-07-29")),
    check("SC1423", ("tickets", "142x-2010-07-29")),
    check("GreenvilleNew", ("tickets", "142x-2010-07-29")),
    check("Greenville3", ("tickets", "142x-2010-07-29")),

    check("SouthCalifornia", ("tickets", "SCA1-2011-01-02")),
    check("TN3005", ("tickets", "3005-2011-01-19")),
    check("WV1181", ("tickets", "1181-2011-03-23")),

    check("VUPSNewFDE2", ("tickets", "FDE2-2011-02-25-due-date")),
    check("VUPSNewFMW3", ("tickets", "FMW3-2011-02-25-due-date")),
    check("FairfaxXML", ("tickets", "OCC2-2011-02-25-due-date")),
    check("Baltimore2", ("tickets", "FMB2-2011-04-22")),
    check("Washington", ("tickets", "FMW1-2011-04-22")),
    check("PensacolaAL", ("tickets", "FPL2-2011-04-29")),
    check("SC1422", ("tickets", "1422-2011-06-01")),
    check("Baltimore", ("tickets", "FMB1-2011-06")),
    check("Washington", ("tickets", "FMW1-2011-06")),
    check("Washington", ("tickets", "FMW1-2011-06-01-original")),
    check("PA7501", ("tickets", "7501-2011-07")),
    check("GA3004", ("tickets", "3004-2011-08-agl")),
    check("VUPSNewOCC4", ("tickets", "OCC4-2011-09-30")),
    check("WV1181Korterra", ("tickets", "1181-2011-11-28")),
    check("SouthCaliforniaCOX", ("tickets", "SCA5-2011-12-20-cox")),
    check("OCC4XML", ("tickets", "OCC4-2011-12-21-xml")),
    check("FairfaxXML", ("tickets", "OCC2-2012-01-13-url")),
    check(["PA7501", "PA7501Meet"], ("tickets", "7501-2012-02-19")),
    check("WV1181Alt", ("tickets", "1181-2012-02-15")),
    check("NewJersey2010", ("tickets", "NewJersey-2012-02-24-dups")),
    check("NewJersey2010B", ("tickets", "NewJersey2-2012-02-24-dups")),
    check("SC1421A", ("tickets", "1421-2012-03-09")),
    check("NorthCalifornia3", ("tickets", "NCA3-2012-04-09")),
    check("SouthCalifornia9", ("tickets", "SCA9-2012-04-09")),
    check("SouthCaliforniaNV", ("tickets", "SCA8-2012-04-09")),
    check("AlabamaNew", ("tickets", "FAU2-2012-08-29")),
    check("SC1424", ("tickets", "1424-2012-10-05-ansco")),
    check("SC1421A", ("tickets", "1421-2012-10-06")),
    check("SC1422A", ("tickets", "1422-2012-10-12")),
    check("GA3006", ("tickets", "3006-2012-11-01")),
    check("SC2051", ("tickets", "2051-2012-11-07")),
    check("Nashville2013", ("tickets", "FNV1-2013-01-08")),
    check("Nashville2013", ("tickets", "FNV1-2014-09-notime")),
    check("Oregon4", ("tickets", "LOR3-2013-02")),
    check("WashingtonState2", ("tickets", "LWA2-2013-02")),
    check("Oregon5", ("tickets", "LOR3-2013-02")),
    check("WashingtonState3", ("tickets", "LWA2-2013-02")),
    check("NC2101", ("tickets", "2101-2013-03-01")),
    check("Oregon6", ("tickets", "LOR5-2013-03-14")),
    check("WashingtonState4", ("tickets", "LWA4-2013-03-14")),
    check("WashingtonState5", ("tickets", "LWA5-2013-03-28")),
    check("SouthCalifornia10", ("tickets", "SCA10-2013-05-04")),
    check("NewJersey2010", ("tickets", "NewJersey-2013-07-16-bogus-date")),
    check("NorthCalifornia", ("tickets", "NCA1-2013-08-28-qtrmin")),
    check("NorthCalifornia2", ("tickets", "NCA2-2013-08-28-qtrmin")),
    check("SouthCalifornia", ("tickets", "SCA1-2013-10-17-corona")),
    check("NewJersey7", ("tickets", "NewJersey7-2013-11-01")),
    check("NewJersey6", ("tickets", "NewJersey6-2013-11-01")),
    check("OregonPortland", ("tickets", "LOR6-2013-11-09")),
    check("GeorgiaXML", ("tickets", "GeorgiaXML-2013-11")),
    check("SC1425", ("tickets", "142x-2010-07-29")),
    check("SC1425A", ("tickets", "1421-2012-03-09")), # !
    check("SC1425A", ("tickets", "1421-2012-10-06")), # !
    check("SC1421PUPS", ("tickets", "1421-2014-02-pups")),
    check("NewJersey8", ("tickets", "NewJersey8-2014-02")),
    check("NewJersey2010", ("tickets", "NewJersey-2014-03-latlong")),
    check("NorthCalifornia4", ("tickets", "NCA4-2014-03")),
    check("SouthCalifornia11", ("tickets", "SCA11-2014-04")),
    check("NorthCalifornia5", ("tickets", "NCA5-2014-04")),
    check("NewJersey9", ("tickets", "NewJersey9-2014-08")),

    # Mantis #3048
    check("NorthCaliforniaATT", ("tickets", "NCA-2012-06-15-revision"),
     [lambda t: t.revision]),
    check("SouthCaliforniaATT", ("tickets", "SCA-2012-06-15-revision"),
     [lambda t: t.revision]),

    # Mantis #3009
    check("GA3003", ("tickets", "3003-2012-04-28-no-grids"), [
      lambda t: t.map_page == ""]),

]

class TestTicketParserBulk(unittest.TestCase):
    def setUp(self):
        self.handler = ticketparser.TicketHandler()

def inject_method(id, d):

    def m(self):
        try:
            filenames = tools.safeglob(os.path.join(d['path'], "*.txt"))
        except ValueError:
            self.fail("No tickets found in directory: %r" % d['path'])

        for filename in filenames:
            tl = ticketloader.TicketLoader(filename)
            for idx, raw in enumerate(tl):
                fmt = d['format']
                if not isinstance(fmt, list):
                    fmt = [fmt]
                try:
                    t = self.handler.parse(raw, fmt)
                except Exception, e:
                    err_msg = "format: %s file: %s ticket #: %s" % (
                              d['format'], filename, idx+1)
                    self.fail(string.join([
                      "Ticket could not be parsed",
                      "Original error: %s " % (e.args,),
                      err_msg], "\n"))

                for j, f in enumerate(d['conditions']):
                    self.assertTrue(f(t), "Ticket %s (%s) fails condition #%s"
                     % (t.ticket_number, t.ticket_format, j+1))
                    # XXX add more info

    m.__name__ = method_name = "test_%03d_%s" % (id, d['format'])
    setattr(TestTicketParserBulk, method_name, m)

# inject test cases dynamically
for id, d in enumerate(DATA):
    inject_method(id+1, d)

TestTicketParserBulk.test_027_Baltimore2.im_func.fails=1
TestTicketParserBulk.test_028_Washington.im_func.fails=1
TestTicketParserBulk.test_031_Baltimore.im_func.fails=1
TestTicketParserBulk.test_032_Washington.im_func.fails=1
TestTicketParserBulk.test_033_Washington.im_func.fails=1

if __name__ == "__main__":
    unittest.main()

