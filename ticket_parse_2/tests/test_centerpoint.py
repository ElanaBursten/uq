# test_centerpoint.py
# XXX We should not depend on the values in centerpoint_grids.py. Tests should
# define their own.

import site; site.addsitedir('.')
import unittest
#
import centerpoint
import centerpoint_grids

class TestCenterpoint(unittest.TestCase):

    def test_match(self):
        # depends on data in centerpoint_grids.py
        self.assertEquals(centerpoint_grids.match('TX 151 A'), 1)
        self.assertEquals(centerpoint_grids.match('TX 292 C'), 1)
        self.assertEquals(centerpoint_grids.match('TX 298 S'), 0)

if __name__ == "__main__":
    unittest.main()


