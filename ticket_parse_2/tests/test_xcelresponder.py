# test_xcelresponder.py

import csv
import os
import site; site.addsitedir('.')
import unittest
#
import date
import ticket_db
import ticketloader
import ticketparser
import xcelresponder
import _testprep

TEST_TERMIDS = ['CMSDC00', 'CMSDN00', 'CMSDC00G', 'CMSDN00G']

class TestXCelResponder(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()

        # make sure we have no tickets, locates, etc
        _testprep.clear_database(self.tdb)
        self.tdb.deleterecords("response_log")

        # post some tickets, eh?
        ids = []
        tl = ticketloader.TicketLoader("../testdata/Colorado2-1.txt")
        for raw in tl.tickets:
            t = self.handler.parse(raw, ["Colorado2"])
            t.ticket_format = 'FCO1'
            id = self.tdb.insertticket(t, whodunit='parser')
            ids.append(id)

        # insert a test employee
        emp_id_1 = _testprep.add_test_employee(self.tdb, 'xcel_000a')
        sql = "update employee set emp_number = '666999' where emp_id = %s" % (
              emp_id_1,)
        self.tdb.runsql(sql)
        emp_id_2 = _testprep.add_test_employee(self.tdb, 'xcel_000b')
        sql = "update employee set emp_number = '666888' where emp_id = %s" % (
              emp_id_2,)
        self.tdb.runsql(sql)

        # tweak data
        sql = "update locate set client_code='CMSDC00G' where ticket_id = %s" % (
              ids[0],)
        self.tdb.runsql(sql)
        sql = "update locate set qty_marked=0 where ticket_id=%s" % (ids[0],)
        self.tdb.runsql(sql)
        sql = "update locate set qty_marked=42 where ticket_id=%s" % (ids[1],)
        self.tdb.runsql(sql)
        # set some statuses
        sql = "update locate set status='M' where ticket_id=%s" % (ids[0],)
        self.tdb.runsql(sql)
        sql = """
          update locate set status='H', regular_hours=20, overtime_hours=10
          where ticket_id=%s""" % ids[1]
        self.tdb.runsql(sql)
        sql = "update locate set status='NC' where ticket_id=%s" % (ids[2],)
        self.tdb.runsql(sql)
        sql = "update locate set status='O' where ticket_id=%s" % (ids[3],)
        self.tdb.runsql(sql)

        # set assigned_to for all of them
        sql = "update locate set assigned_to = %s" % (emp_id_1,)
        self.tdb.runsql(sql)
        # set the closed date for some of them
        sql = """
          update locate set closed_date='%s', closed=1, closed_by_id=%s
          where ticket_id = %s""" % (date.Date().isodate(), emp_id_2, ids[0])
        self.tdb.runsql(sql)
        # set added_by for some of them (should be filtered out)
        #sql = "update locate set added_by = '200' where ticket_id = %s" % (
        #      ids[-1],)
        #self.tdb.runsql(sql)

        # insert in responder queue
        self.tdb.deleterecords('responder_queue')
        locate_ids = [x['locate_id'] for x in self.tdb.getrecords('locate')]
        for locate_id in locate_ids:
            self.tdb.insert_responder_queue(locate_id)

        rows = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(rows), len(locate_ids))

        rows = self.tdb.runsql_result("exec get_pending_responses 'FCO1'")
        self.assertEquals(len(rows), len(locate_ids))

        # install _get_terms hook so code matches test data
        # XXX better done using mocking
        def _get_terms(self):
            return TEST_TERMIDS
        xcelresponder.XCelResponder._get_terms = _get_terms

    def test_001_get_data(self):
        xlr = xcelresponder.XCelResponder()
        xlr.log.lock = 1
        data = xlr.get_pending_responses('FCO1')
        for row in data:
            self.assertEquals(row['client_code'] in TEST_TERMIDS, True)
        self.assertEquals(len(data), 8)

    test_001_get_data.has_error=1

    def test_002_write_file(self):
        xlr = xcelresponder.XCelResponder()
        xlr.log.lock = 1
        xlr.FIELDNAMES = ['Name', 'Age', 'Best Subject']
        dicts = [
            {'Name': 'Harry Potter', 'Age': 15, 'Best Subject': 'DADA'},
            {'Name': 'Hermione Granger', 'Age': 15,
             'Best Subject': 'Pretty much everything'},
            {'Name': 'Ron Weasley', 'Age': 16,
             'Best Subject': 'nothing, really'},
            {'Name': 'Severus Snape', 'Best Subject': 'Potions', }
        ]
        data = xlr.write_file("test002.csv", dicts)

        # test some stuff...
        f = open("test002.csv", "r")
        lines = f.readlines()
        line = lines[1] # change this if we don't write the 'header' line
        self.assertEquals(line.rstrip(), '"Harry Potter","15","DADA"')
        # Note: This is the format returned by the responder, with no spaces
        # after commas

    test_002_write_file.has_error=1

    def test_003(self):
        def gen_filename(target_date):
            return 'test003.csv'

        xlr = xcelresponder.XCelResponder()
        xlr.log.lock = 1
        xlr.gen_filename = gen_filename
        xlr.run_call_center('FCO1',
          {'termids': [],
           'recipients':[{'email':'bugs@bunny.org'},
                         {'email':'daffy@duck.org'}]})

        # get the results from the CSV file
        f = open('test003.csv', 'r')
        dr = csv.DictReader(f, xlr.FIELDNAMES,
             dialect=xcelresponder.XCelDialect)
        dicts = []
        for d in dr:
            dicts.append(d)
        self.assertEquals(len(dicts), 9)
        # may change if we change the test data...
        f.close()

        # status M
        self.assertEquals(dicts[1]['Order Number'], '0654878')
        self.assertEquals(dicts[1]['Billing Type'], 'U')
        self.assertEquals(dicts[1]['Marked'], '1')
        self.assertEquals(dicts[1]['Cleared'], '0')
        self.assertEquals(dicts[1]['Cancelled'], '0')
        self.assertEquals(dicts[1]['Completed'], '1')
        self.assertEquals(dicts[1]['Tech ID'], '666888')
        self.assertEquals(dicts[1]['Facility Type'], 'G')

        # status H
        self.assertEquals(dicts[2]['Billing Type'], 'T')
        self.assertEquals(dicts[2]['Marked'], '0')
        self.assertEquals(dicts[2]['Cleared'], '1')
        self.assertEquals(dicts[2]['Cancelled'], '0')
        self.assertEquals(dicts[2]['Completed'], '0')
        self.assertEquals(dicts[2]['Tech ID'], '666999')
        self.assertEquals(dicts[2]['Facility Type'], 'E')

        # status NC
        self.assertEquals(dicts[3]['Billing Type'], 'N')
        self.assertEquals(dicts[3]['Marked'], '0')
        self.assertEquals(dicts[3]['Cleared'], '0')
        self.assertEquals(dicts[3]['Cancelled'], '0')   # was: '1'

        # status O
        self.assertEquals(dicts[4]['Billing Type'], 'U')
        self.assertEquals(dicts[4]['Marked'], '0')
        self.assertEquals(dicts[4]['Cleared'], '0')
        self.assertEquals(dicts[4]['Cancelled'], '0')
        self.assertEquals(dicts[4]['Remarks'], 'Ongoing')

        # check responder log
        rows = self.tdb.getrecords("response_log")
        self.assertEquals(len(rows), 8)

    test_003.has_error=1

    def tearDown(self):
        _testprep.clear_database(self.tdb)
        try:
            os.remove("test002.csv")
        except:
            pass


if __name__ == "__main__":

    unittest.main()
