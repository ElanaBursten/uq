@echo off
rem Replace this with the correct location on your computer:
rem (or, make the path of the data dir absolute)
cd c:
cd \work\uq\ticket_parse\

%1 --data testdata\datadir
rem You may have to prepend something like "c:\Python23\python.exe"
