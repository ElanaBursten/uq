# test_duedatecatalog.py
# Test the methods in DueDateCatalog; do *NOT* test call center specific due
# date calculations.

import unittest
import site; site.addsitedir('.')
#
import date
import duedatecatalog

class TestDueDateCatalog(unittest.TestCase):

    def test_inc_to_business_day(self):
        cat = duedatecatalog.DueDateCatalog("OCC1", [])  # any cc will do
        d1 = date.Date("2007-06-22 12:13:14") # Friday
        d2 = cat.inc_to_business_day(d1)
        self.assertEquals(d2.isodate(), "2007-06-22 12:13:14") # unchanged

        e1 = date.Date("2007-06-23 12:13:14") # Saturday
        e2 = cat.inc_to_business_day(e1)
        self.assertEquals(e2.isodate(), "2007-06-25 12:13:14") # Monday

        f1 = date.Date("2007-06-24 12:13:14") # Sunday
        f2 = cat.inc_to_business_day(f1)
        self.assertEquals(f2.isodate(), "2007-06-25 12:13:14") # Monday

    def test_inc_n_days_to_time(self):
        """ Test inc_n_days_to_time() """
        ddc = duedatecatalog.DueDateCatalog("FMB1", [])
        d1 = date.Date("2002-11-04 18:15:00")   # Monday, 6:15 PM
        d2 = ddc.inc_n_days_to_time(d1, 1, 17)  # 24 hours to 5 PM
        self.assertEquals(d2.isodate(), "2002-11-06 17:00:00")

    def test_inc_n_days_to_time_2(self):
        """ Test inc_n_days_to_time() """
        ddc = duedatecatalog.DueDateCatalog("FMB1", [])
        d1 = date.Date("2002-11-04 18:15:00")   # Monday, 6:15 PM
        d2 = ddc.inc_n_days_to_time(d1, 2, 19, minutes=1)  # 48 hours to 7 PM
        self.assertEquals(d2.isodate(), '2002-11-06 19:01:00')

    def test_inc_n_days_to_time_3(self):
        """ Test inc_n_days_to_time() """
        ddc = duedatecatalog.DueDateCatalog("FMB1", [])
        d1 = date.Date("2002-11-04 18:15:00")   # Monday, 6:15 PM
        d2 = ddc.inc_n_days_to_time(d1, 1, 17, minutes=1)  # 24 hours to 5:01 PM
        self.assertEquals(d2.isodate(), "2002-11-06 17:01:00")

    def test_inc_48_hours_7am(self):
        """ Test 48 hours + time mechanism """
        cat = duedatecatalog.DueDateCatalog("OCC1", [])  # any cc will do
        din = date.Date("2002-07-09 10:36:00")
        dout = cat.inc_48_hours_7am(din)
        self.assertEquals(dout.isodate(), "2002-07-12 07:00:00")

        din = date.Date("2002-07-12 10:36:00")
        dout = cat.inc_48_hours_7am(din)
        self.assertEquals(dout.isodate(), "2002-07-17 07:00:00")



if __name__ == "__main__":

    unittest.main()
