# test_main.py
# Created: 03 Mar 2002, Hans Nowak

from __future__ import with_statement
from StringIO import StringIO
import copy
import datetime
import httplib
import os
import re
import shutil
import site; site.addsitedir('.')
import string
import sys
import time
import traceback
import unittest
import xml.etree.ElementTree as ET
#
from tools import glob
import _testprep
import businesslogic
import call_centers
import centerpoint
import client
import config
import datadir
import date
import dbinterface_ado
import dbinterface_old as dbinterface
import locate
import locate_status
import mail2
import main
import static_tables
import ticket
import ticket_db
import ticketloader
import ticketparser
import ticketrouter
import ticketsummary
import tools
#
from mockery import *
from search_log_file import SearchLogFile

TICKET_PATH = _testprep.TICKET_PATH
glob = tools.safeglob

# TODO: move to _testprep?
def drop(filename, data, dir="test_incoming"):
    """ Write a file (containing one or more images) to the
        test_incoming directory. """
    fullname = os.path.join(dir, filename)
    f = open(fullname, "wb")
    f.write(data)
    f.close()

def dump_log(m):
    m.log.logger.logfile.seek(0)
    for line in m.log.logger.logfile.readlines():
        sys.stdout.write(line)


class TestMain(unittest.TestCase):

    def setUp(self):

        self.config = config.getConfiguration()
        # replace the real configuration with our test data

        CENTERS = ['FCL2', 'FDX1', 'FAM1', 'FMW2', 'FMW3', 'LWA1']

        self.config.processes = []
        for center in CENTERS:
            p = {'incoming': 'test_incoming_' + center,
                 'format': center,
                 'processed': 'test_processed_' + center,
                 'error': 'test_error_' + center,
                 'attachments': '',
                }
            self.config.processes.append(p)

        # these centers use the regular incoming (etc) dirs:
        for center in ['FMB1', 'OCC2', 'FHL1', 'LOR1', 'LQW1', 'OCC1', 'OCC3']:
            p = {'incoming': 'test_incoming',
                 'format': center,
                 'processed': 'test_processed',
                 'error': 'test_error',
                 'attachments': '',
                }
            self.config.processes.append(p)

        # and one for LAK1
        self.config.processes.append(
          {'incoming': 'test_incoming_LAK1',
           'format': 'LAK1',
           'processed': 'test_processed_LAK1',
           'error': 'test_error_LAK1',
           'attachments': 1,
           'attachment_dir': 'test_attachments_LAK1',
           'attachment_proc_dir': 'test_attachment_proc_dir_LAK1'})

        self.handler = ticketparser.TicketHandler()
        self.tdb = ticket_db.TicketDB()

        mail2.TESTING = 1
        mail2._test_mail_sent = []

        # Make sure the right directories are created
        for process in self.config.processes:
            for key in ['incoming','processed','error','attachment_dir','attachment_proc_dir']:
                dir = process.get(key,'')
                if dir and not dir in _testprep.TEST_DIRECTORIES:
                    _testprep.TEST_DIRECTORIES.append(dir)

        _testprep.create_test_dirs()
        _testprep.set_cc_routing_file()
        _testprep.clear_database(self.tdb)

        for process in self.config.processes:
            #incoming, name, processed, error, group = process
            incoming = process['incoming']
            processed = process['processed']
            error = process['error']
            _testprep.clear(incoming)
            _testprep.clear(processed)
            _testprep.clear(error)

        # the USW01 client is no longer active in the database, make it active
        sql = "select call_center from client where oc_code = 'USW01'"
        self.result_USW01 = self.tdb.runsql_result(sql)[0]

        _testprep.set_client_active(self.tdb, self.result_USW01['call_center'],
         "USW01")

        # Remember the configured responders
        configuration = config.getConfiguration()
        self.responders = configuration.responders
        self.conf_copy = copy.deepcopy(self.config)

    def tearDown(self):
        # Reset the configured responders
        configuration = config.getConfiguration()
        configuration.responders = self.responders

        # Set it back inactive
        _testprep.set_client_inactive(self.tdb,
          self.result_USW01['call_center'], "USW01")
        _testprep.clear_database(self.tdb)

    def prepare(self, *centers):
        self.config.processes = [p for p in self.config.processes
                                 if p['format'] not in centers]
        for center in centers:
            p = {'incoming': 'test_incoming',
                 'format': center,
                 'processed': 'test_processed',
                 'error': 'test_error',
                 'attachments': ''}
            self.config.processes.append(p)
            _testprep.set_call_center_active(self.tdb, center)
        call_centers.get_call_centers(self.tdb).reload()

    def clear_dir(self, dirname):
        """ Empty a directory. """
        filenames = os.listdir(dirname)
        for filename in filenames:
            fullname = os.path.join(dirname, filename)
            os.remove(fullname)

    def add_fake_client(self, call_center, name):
        self.remove_fake_client(call_center, name)
        sql = """
         insert client
         (client_name, oc_code, office_id, call_center, active)
         values ('%s', '%s', 100, '%s', 1)
        """ % (name, name, call_center)
        self.tdb.runsql(sql)

    def remove_fake_client(self, call_center, name):
        self.tdb.deleterecords("client", call_center=call_center,
         oc_code=name)

    def empty_summary_fragment(self):
        self.tdb.runsql('delete from summary_fragment')

    ###
    ### Test code

    def test_goodticket(self):
        """ Make sure a perfectly valid ticket is parsed and posted
            correctly. """
        self.prepare('Atlanta')
        # create a ticket file
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        drop("test1.txt", raw)

        m = main.Main(verbose=0, call_center="Atlanta")
        #m.log.lock = 1
        m.run(runonce=1)
        id = m.ids[0]   # id of ticket that was just posted

        # get ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(t2.parsed_ok, "1")
        self.assertEquals(t2.parse_errors, None)
        self.assert_(t2.due_date)

        self.assert_(os.path.exists("test_processed/test1.txt"),
         "test_processed/test1.txt does not exist")

    def test_badticket1(self):
        """ Test what happens to a ticket that cannot be parsed. """
        self.prepare('Atlanta')
        # (It should be posted anyway, with an error message and parsed_ok
        #  == 0, and the original file should be in the processed
        #  directory.)
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        # mutilate the raw data so we cannot parse it correctly
        raw = string.replace(raw, "12311-109-056", "")
        raw = string.replace(raw, "Ticket :", "")
        drop("test2.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.run(runonce=1)

        # make sure that nothing was posted
        self.assertEquals(m.ids, [])

        self.assert_(os.path.exists("test_processed/test2.txt"),
         "test_processed/test2.txt does not exist")
        self.assert_(os.path.exists("test_error/test2.txt.001"))

    def test_badticket2(self):
        """ Test what happens to a ticket that cannot be posted.
            (The original file should end up in the processed directory
            anyway, but the image of the ticket should be saved as a
            separate file in the error directory.) Nothing is posted,
            obviously (because we can't).
        """
        self.prepare('Atlanta')
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        # meddle with the ticket so the state field will be too long
        raw = string.replace(raw, "GA", "Georgia")
        drop("test3.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.run(runonce=1)

        self.assert_(os.path.exists("test_error/test3.txt.001"))
        self.assert_(os.path.exists("test_processed/test3.txt"))

    def test_naughtyticket1(self):
        """ What happens if a ticket cannot be recognized? """
        self.prepare('Atlanta')
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        # mutilate data so it won't be recognized as Atlanta ticket
        raw = string.replace(raw, "GAUPC", "BOGUS")
        drop("test4.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.run(runonce=1)

        self.assertEquals(m.ids, [])
        self.assert_(os.path.exists("test_processed/test4.txt"))
        self.assert_(os.path.exists("test_error/test4.txt.001"))

    def test_6(self):
        # Adapted from demo_ticket_db.py.
        self.prepare('Atlanta')
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        tp = ticketparser.TicketHandler()
        ticket = tp.parse(raw, ["Atlanta"])

        ticket.ticket_number = "test6-" + str(long(1000 * time.time()))
        del ticket.locates[4:]  # only keep 4 locates
        id = self.tdb.insertticket(ticket)

        # now add some locates...
        ticket.locates.append(locate.Locate('ZZZ01'))
        ticket.locates.append(locate.Locate('ABC01'))
        self.assertEquals(len(ticket.locates), 6)

        m = main.Main(verbose=0, call_center='Atlanta')

        # let's see if we find a duplicate for this ticket... (we should,
        # because we just posted one)
        tixinfo = m.df.find_duplicate_tickets(ticket)
        self.assertEquals(len(tixinfo), 1)
        # tixinfo is a list of dicts {ticket_id, ticket, locates}

        #m.log.lock = 1
        newlocates = m.df._find_new_locates(ticket, tixinfo[0])
        self.assertEquals(len(newlocates), 2)

        self.tdb.updateticket(id, ticket, newlocates=newlocates)

        # how many locates are there for this ticket?
        sql = "select locate_id, client_code "\
         "from locate where ticket_id = '%s'" % (id)
        results = self.tdb.runsql_result(sql)
        self.assertEquals(len(results), 6)

    def test_reassigning(self):
        """ Test reassigning of locates """
        # Test if, after encountering a dup ticket, the locates of that
        # ticket with status -N (not a customer) are reset to status -P
        # (unassigned) and 'closed'.
        self.prepare('Atlanta')

        # create a ticket and post it
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        tp = ticketparser.TicketHandler()
        ticket = tp.parse(raw, ["Atlanta"])
        id = self.tdb.insertticket(ticket)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        # make sure there are locates with status -N; we'll look at them later
        z = self.tdb.getrecords("locate", ticket_id=id,
            status=locate_status.not_a_customer)
        self.assert_(len(z) > 0)

        # then drop a file with that same ticket in incoming
        drop("test-r.txt", raw)

        # let main pick it up
        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.run(runonce=1)

        # it should be in processed afterwards
        self.assert_(os.path.exists("test_processed/test-r.txt"))

        # check if those locates have status -P now... or in other words,
        # there should be no "not a customer" records left
        foo = self.tdb.getrecords("locate", ticket_id=id,
         status=locate_status.not_a_customer)
        self.assert_(len(foo) == 0)

    def test_NewJerseyTickets(self):
        """ Test New Jersey tickets """
        # Post two New Jersey tickets, with the same number but with
        # different locates, and see if they're stored correctly.
        self.prepare('NewJersey')
        tl = ticketloader.TicketLoader("../testdata/newjersey-1.txt", chr(12))
        raw = tl.getTicket()

        drop("nj1.txt", raw)
        time.sleep(1)

        # change the locate
        raw = string.replace(raw, "CDC = UW4", "CDC = HANS1")
        drop("nj2.txt", raw)

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='NewJersey')
        #m.log.lock = 1
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 2)
        self.assertEquals(results[0]["client_code"], "UW4")
        self.assertEquals(results[1]["client_code"], "HANS1")
        self.assertEquals(results[1]['added_by'], "PARSER")

    def test_NewJerseyTickets_2(self):
        """
        Test NewJersey2 tickets and verify client
        """
        self.prepare('NewJersey2')

        # drop all of the NewJersey2 tickets
        GPP_tickets = glob(os.path.join(_testprep.TICKET_PATH, "sample_tickets",
                      "GPP", "GSU2-2008-02-01-*.txt"))
        for ticket_file in GPP_tickets:
            tl = ticketloader.TicketLoader(ticket_file,chr(12))
            raw = tl.tickets[0]
            drop(os.path.basename(ticket_file), raw)

        m = main.Main(verbose=0, call_center='NewJersey2')
        m.run(runonce=1)

        for id in m.ids:
            results = self.tdb.getrecords("locate", ticket_id=id)
            self.assertEquals(len(results), 1)
            self.assertEquals(results[0]["client_code"], "GPP")
            self.assertEquals(results[0]['added_by'], "PARSER")

    ### There should be a way to test the reparsing...
    def test_yyy_reparsing(self):
        """ Test reparsing """
        self.prepare('Atlanta')
        # 1. Make a ticket that cannot be posted or parsed.
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.tickets[0]
        raw = string.replace(raw, "GAUPC", "BOGUS")
        drop("test5.txt", raw)

        # 2. Let main.py run. It should end up in the error directory.
        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.run(runonce=1)

        # 3. Verify that it's in the error directory.
        self.assert_(os.path.exists("test_error/test5.txt.001"))

        # 4. Hack the file so it will parse OK now.
        f = open("test_error/test5.txt.001", "r")
        raw = f.read()
        f.close()
        raw = string.replace(raw, "BOGUS", "GAUPC")
        g = open("test_error/test5.txt.1", "w")
        g.write(raw)
        g.close()

        tp = ticketparser.TicketHandler()
        t1 = tp.parse(raw, ["Atlanta"])  # this should be OK now

        # 5. Run Main in reparse-mode. Of course, the ticket is still not
        # going to parse/post, so that doesn't change anything. So we'll
        # have to change that file somehow, to make it valid...
        del m
        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.reparse = 1
        m.ticketfetcher.feedhandler.reparse_days = 100000
        m.run(runonce=1)

        # 6. Assert that ticket is posted & gone from error directory
        self.assert_(len(m.ids) >= 1)
        id = m.ids[-1]
        self.assert_(not os.path.exists("test_error/test5.txt"))

        t2 = self.tdb.getticket(id)
        self.assertEquals(t1.ticket_number, t2.ticket_number)


    def test_renotification_1(self):
        """ Test renotifications (1) """

        """ As seen in the specs:
            1. The original Ticket # 1234567, sent on 3/4/2002
            2. An update to Ticket # 1234567, sent on 3/5/2002, which would
            update the existing ticket
            3. A RENOTIFICATION Ticket # 1234567, sent on 3/8/2002, which would
            become a new ticket (with the same number) in Q Manager.
            4. Another copy of the same RENOTIFICATION sent on 3/8/2002 could
            be processed later; this would update the second ticket, it would
            not create a third ticket.
        """
        self.prepare('FWP2')

        # NOTE: Rules have changed on 2002.07.20, and isrenotification() now
        # returns true for _all_ tickets. This test still works though,
        # because:
        # - the update [2] is done when the ticket isn't closed, so the ticket
        #   is updated rather than inserted again;
        # - the renotification [3] is inserted, because the original ticket is
        #   closed and it's still recognized as a renotification;
        # - renotification update rules apply to [4].

        tl = ticketloader.TicketLoader("../testdata/Pennsylvania-1.txt", chr(12))
        raw = tl.getTicket()
        raw = string.replace(raw, "03/14/02 08:52:23", "03/04/02 12:30:00")
        ticket = self.handler.parse(raw, ["Pennsylvania"])

        self.assertEquals(ticket.ticket_format, "FWP2")

        # 1. post the ticket
        id = self.tdb.insertticket(ticket)

        # 2. post an update
        raw = string.replace(raw, "03/04/02 12:30:00", "03/05/02 12:30:00")
        drop("FWP2_2.txt", raw)
        m = main.Main(verbose=0, call_center='FWP2')
        #m.log.lock = 1
        m.run(runonce=1)

        id2 = m.ids[-1]
        self.assertEquals(id2, id)

        # 3. close the ticket
        sql = """
         update locate
         set closed = 1
         where ticket_id = '%s'
        """
        sql = sql % (id)
        self.tdb.runsql(sql)

        self.assertEquals(self.tdb.isclosed(id), 1)

        # 4. post a renotification
        raw = string.replace(raw, "03/05/02 12:30:00", "03/08/02 14:30:00")
        raw = string.replace(raw, "[ROUTINE]", "[RENOTIFICATION]")
        raw = string.replace(raw, "000 ROUTINE PLACE", "000 RENOTIFICATION")
        t3 = self.handler.parse(raw, ["Pennsylvania"])
        self.assertEquals(t3.ticket_type, "RENOTIFICATION")
        self.assertEquals(t3.ticket_format, "FWP2")
        drop("FWP2_3.txt", raw)
        m.run(runonce=1)

        id3 = m.ids[-1]
        # this should have been posted as a new ticket
        self.assertNotEqual(id, id3)
        # test parent_ticket_id
        results = self.tdb.getrecords("ticket", ticket_id=id3)
        self.assertEquals(len(results), 1)
        parent_ticket_id = results[0].get("parent_ticket_id", None)
        self.assertEquals(parent_ticket_id, id)

        # 5. post another renotification
        drop("FWP2_4.txt", raw)
        m.run(runonce=1)

        id4 = m.ids[-1]
        self.assertEquals(id3, id4)
        self.assertEquals(m.ids[-2], m.ids[-1])

    def test_renotification_2(self):
        """ Test renotifications (2) """
        # Like test_renotification_1, but in this case we close the ticket
        # before sending the renotification.
        self.prepare('FWP2')
        self.tdb.deleterecords("assignment")
        self.tdb.deleterecords("locate")
        self.tdb.deleterecords("ticket")

        tl = ticketloader.TicketLoader("../testdata/Pennsylvania-1.txt", chr(12))
        raw = tl.getTicket()
        raw = string.replace(raw, "03/14/02 08:52:23", "03/04/02 12:30:00")
        ticket = self.handler.parse(raw, ["Pennsylvania"])

        self.assertEquals(ticket.ticket_format, "FWP2")

        # 1. post the ticket
        id = self.tdb.insertticket(ticket)

        # 2. post an update
        raw = string.replace(raw, "03/04/02 12:30:00", "03/05/02 12:30:00")
        drop("FWP2_2.txt", raw)
        m = main.Main(verbose=0, call_center='FWP2')
        #m.log.lock = 1
        m.run(runonce=1)

        id2 = m.ids[-1]
        self.assertEquals(id2, id)

        # 3. post a renotification -- ticket is open though
        raw = string.replace(raw, "03/05/02 12:30:00", "03/08/02 14:30:00")
        raw = string.replace(raw, "[ROUTINE]", "[RENOTIFICATION]")
        raw = string.replace(raw, "000 ROUTINE PLACE", "000 RENOTIFICATION")
        t3 = self.handler.parse(raw, ["Pennsylvania"])
        self.assertEquals(t3.ticket_type, "RENOTIFICATION")
        self.assertEquals(t3.ticket_format, "FWP2")
        drop("FWP2_3.txt", raw)
        m.run(runonce=1)

        id3 = m.ids[-1]
        # this should *not* have been posted as a new ticket
        self.assertEquals(id, id3)

    def test_renotification_FCV3(self):
        """ Test FCV3 renotification """
        self.prepare('FCV3')
        tl = ticketloader.TicketLoader("../testdata/Richmond3-1.txt")
        raw = tl.getTicket()    # get any old ticket
        t = self.handler.parse(raw, ["Richmond3"])
        t.ticket_format = t.source = 'FCV3'
        # mangle transmit date a bit so we set it back a day
        d = date.Date(t.transmit_date)
        d.dec()
        t.transmit_date = d.isodate()
        id = self.tdb.insertticket(t)

        # close the ticket or it won't work
        sql = """
         update locate
         set closed = 1
         where ticket_id = '%s'
        """
        sql = sql % (id)
        self.tdb.runsql(sql)

        self.assertEquals(self.tdb.isclosed(id), 1)

        # this doesn't make any sense... here we find the duplicate ticket,
        # but when called in Main, we don't find it...?!?!
        import duplicates
        t2 = self.handler.parse(raw, ["Richmond3"])
        t2.ticket_format = t2.source = 'FCV3'
        df = duplicates.DuplicateFinder(self.tdb)
        dfresult = df.getduplicates(t2, is_renotification=1)
        self.assertEquals(dfresult.parent_ticket_id, id)

        r = df.find_duplicate_tickets(t2)
        self.assertEquals(len(r), 1)

        # now drop this same ticket for main.py to pick up
        drop("Richmond3-renotif-test-1.txt", raw)
        m = main.Main(verbose=0, call_center='FCV3')
        #m.log.lock = 1
        m.run(runonce=1)

        # how many tickets are there with this number? (should be 2)
        sql = """
         select * from ticket
         where ticket_number = '%s'
         order by ticket_id
        """
        sql = sql % (t.ticket_number,)
        results = self.tdb.runsql_result(sql)

        self.assertEquals(len(results), 2)
        # the original ticket should not have a parent_ticket_id
        parent_ticket_id = results[0].get("parent_ticket_id", None)
        self.assertEquals(parent_ticket_id, None)
        # but the "renotification" should
        parent_ticket_id = results[1].get("parent_ticket_id", None)
        self.assertEquals(parent_ticket_id, id)

    def test_isnotification(self):
        """ Test if a ticket is a renotification """
        self.prepare('FWP2')
        tl = ticketloader.TicketLoader("../testdata/renotif.txt")
        raw = tl.tickets[1] # the second ticket
        t = self.handler.parse(raw, ["Pennsylvania"])
        m = main.Main(verbose=0, call_center='FWP2')

        self.assertEquals(m.isrenotification(t), 1)

    def test_update(self):
        """ Test ticket updates and transmit_date """
        # tickets with a transmit_date < that of the original ticket will not
        # be updated.
        self.prepare('Atlanta')
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        d = date.Date(t.transmit_date)
        d.inc() # bump transmit date
        t.transmit_date = d.isodate()

        id = self.tdb.insertticket(t)

        # now write the (unchanged) raw ticket to a file
        drop("noupdate.txt", raw)

        # and let main have its way, picking up the ticket
        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.run(runonce=1)

        t2 = self.tdb.getticket(id)
        self.assertEquals(date.Date(t2.transmit_date).isodate(),
         t.transmit_date)

    def test_update_with_grids(self):
        """ Test updates with grids. """
        self.prepare('FCL1')
        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-1.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["NorthCarolina"])
        old_grids = t.grids
        t.grids = ["Groucho", "Harpo", "Zeppo"]

        id = self.tdb.insertticket(t)

        # now write the (unchanged) raw ticket to a file
        drop("fcl1-update.txt", raw)

        # and let main have its way, picking up the ticket
        m = main.Main(verbose=0, call_center='FCL1')
        #m.log.lock = 1
        m.run(runonce=1)

        t2 = self.tdb.getticket(id)
        self.assertEquals(date.Date(t2.transmit_date).isodate(),
                          t.transmit_date)
        self.assertEquals(len(t2.grids), 7)
        self.assertEquals(t2.grids, old_grids + t.grids)

    def test_renotification_3(self):
        """ Test renotifications (3) """
        self.prepare('FCL1')
        tl = ticketloader.TicketLoader("../testdata/renotif-2.txt")
        raw = tl.getTicket()
        drop("renotif3-1.txt", raw)

        m = main.Main(verbose=0, call_center='FCL1')
        #m.log.lock = 1
        m.run(runonce=1)

        id = m.ids[-1]

        # now close all locates for this ticket
        sql = """
         update locate
         set closed = 1
         where ticket_id = '%s'
        """
        sql = sql % (id)
        m.tdb.runsql(sql)

        raw = tl.getTicket()
        drop("renotif3-2.txt", raw)
        m.run(runonce=1)

        # this ticket should *not* have been an update...
        id2 = m.ids[-1]
        self.assertNotEqual(id, id2)

        t1 = m.tdb.getrecords("ticket", ticket_id=id)[0]
        t2 = m.tdb.getrecords("ticket", ticket_id=id2)[0]
        self.assertEquals(t1["ticket_number"], t2["ticket_number"])

    def test_duplicate_summaries(self):
        self.prepare('NewJersey')

        # NOTE: make sure to use a call center that doesn't allow updating of
        # summaries!
        tl = ticketloader.TicketLoader("../testdata/sum-NewJersey.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["NewJersey"])
        self.assertEquals(isinstance(summ, ticketsummary.TicketSummary), True)

        # post this summary
        hid, dids = self.tdb.storesummary(summ)

        # how many files are in summary_header now?
        result = self.tdb.getrecords("summary_header")
        self.assertEquals(len(result), 1)

        # now drop this same file in an incoming directory
        drop("dupsum.txt", raw)

        # let main.py run
        m = main.Main(verbose=0, call_center='NewJersey')
        #m.log.lock = 1
        m.run(runonce=1)

        # assert that we still have only one summary_header record
        result = self.tdb.getrecords("summary_header")
        self.assertEquals(len(result), 1)

    def test_main_ids(self):
        """ Test if main.ids has the correct number of values """
        self.prepare('Atlanta')
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")

        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        self.assertEquals(len(m.ids), 0)
        for i in range(3):
            raw = tl.getTicket()
            drop("m" + str(i), raw)
            m.run(runonce=1)
            self.assertEquals(len(m.ids), i+1)

        # drop it again, causing an update; the id should be added though
        drop("m3again.txt", raw)
        m.run(runonce=1)
        self.assertEquals(len(m.ids), 4)

    def test_ticket_version(self):
        """ ticket_version is updated when processing duplicate tickets """
        self.prepare('Atlanta')
        # Test if ticket_version is updated when processing duplicate tickets.

        # grab any old ticket and drop it for main.py to find
        tl = ticketloader.TicketLoader("../testdata/atlanta-3.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])

        drop("tv1.txt", raw)
        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.run(runonce=1)

        id = m.ids[-1]  # hopefully this works...

        # ticket_version should now be updated
        results = self.tdb.getrecords("ticket_version", ticket_id=id)
        self.assertEquals(len(results), 1)
        rec = results[0]
        self.assertEquals(rec["ticket_number"], t.ticket_number)
        # ticket_type should be the _old_ value ("FOOFOO")
        self.assertEquals(rec["ticket_type"], t.ticket_type)
        self.assertEquals(rec["transmit_date"], "2001-11-01 00:08:32")
        self.assertEquals(rec["filename"], "tv1.txt")
        self.assertEquals(rec["ticket_revision"], "001")

        # drop that same ticket again, and let main update it... but change
        # transmit_date first
        raw = string.replace(raw, "11/01/01 00:08:32", "11/02/01 00:07:00")
        drop("tv2.txt", raw)
        m.run(runonce=1)

        # there should now be two ticket_version records for this ticket
        results = self.tdb.getrecords("ticket_version", ticket_id=id)
        self.assertEquals(len(results), 2)
        self.assertEquals(results[1]["transmit_date"], "2001-11-02 00:07:00")
        self.assert_(results[1].get("arrival_date"))
        self.assertEquals(results[1]["filename"], "tv2.txt")

    def test_no_show(self):
        """ Test no show tickets (1) """
        # old rule:
        # no show tickets should not be posted. at least, not for Richmond(1|3)
        # and Fairfax.
        # new rule: we do post them, but we do a "locates only" update rather
        # than a regular one.
        self.prepare('FCV1')

        tl = ticketloader.TicketLoader("../testdata/Richmond1-1.txt")
        raw = tl.tickets[1] # not the first one
        oldraw = raw

        # make sure you post this ticket first, or it won't work
        t = self.handler.parse(raw, ["Richmond1"])
        self.assertEquals(len(t.locates), 5)
        self.assertEquals(t.ticket_format, "FCV1")
        id = self.tdb.insertticket(t)
        # and close it
        sql = """
         update locate
         set closed = 1, status = 'NC'
         where ticket_id = '%s'
        """
        sql = sql % (id)
        self.tdb.runsql(sql)
        self.assertEquals(self.tdb.isclosed(id), 1)

        # how many locates on this ticket? there should be 5
        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 5)

        # now make a NO SHOW for this ticket
        raw = string.replace(raw, "INSUFFICIENT NOTICE", "NO SHOW")
        raw = string.replace(raw, "FCS01", "FCS01    CTR02") # add a locate
        t = self.handler.parse(raw, ["Richmond1"])
        self.assertEquals(t.ticket_type, "NO SHOW")
        self.assertEquals(len(t.locates), 6)

        q_len_before = len(self.tdb.getrecords("responder_queue"))

        drop("noshow.txt", raw)

        m = main.Main(verbose=0, call_center='FCV1')
        #m.log.lock = 1
        m.EXCLUDE_FOR_REQUEUEING = []   # so -P will be requeued
        m.run(runonce=1)

        #self.assertEquals(m.ids, [])
        self.assert_(m.ids, "Something should have been posted")
        # should be empty; we didn't post anything

        # check number of tickets in database (should be 1)
        results = self.tdb.getrecords("ticket")
        self.assertEquals(len(results), 1)

        t2 = self.tdb.getticket(id)
        self.assertEquals(t2.ticket_type, "INSUFFICIENT NOTICE")
        # and not NO SHOW; ticket shouldn't have been updated

        # there should be *6* locates now
        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 6)

        # check if records have been added to the responder queue
        q_len_after = len(self.tdb.getrecords("responder_queue"))
        self.assert_(q_len_after > q_len_before)
        self.assertEquals(q_len_after - q_len_before, len(t.locates))

        ###
        ### test exclusion mechanism

        # close ticket again
        sql = """
         update locate
         set closed = 1, status = 'NC'
         where ticket_id = '%s'
        """
        sql = sql % (id)
        self.tdb.runsql(sql)
        self.assertEquals(self.tdb.isclosed(id), 1)


        # make another NO SHOW for this ticket
        raw = string.replace(oldraw, "INSUFFICIENT NOTICE", "NO SHOW")
        raw = string.replace(raw, "FCS01", "FCS01   CTR02   SNARL02") # add a locate
        t = self.handler.parse(raw, ["Richmond1"])
        self.assertEquals(t.ticket_type, "NO SHOW")
        self.assertEquals(len(t.locates), 7)

        q_len_before = len(self.tdb.getrecords("responder_queue"))

        drop("noshow2.txt", raw)

        m.EXCLUDE_FOR_REQUEUEING = ["-P", "-N", "-R"]
        m.run(runonce=1)

        #self.assertEquals(m.ids, [])
        self.assert_(m.ids, "Something should have been posted")
        # should be empty; we didn't post anything

        # check number of tickets in database (should be 1)
        results = self.tdb.getrecords("ticket")
        self.assertEquals(len(results), 1)

        t2 = self.tdb.getticket(id)
        self.assertEquals(t2.ticket_type, "INSUFFICIENT NOTICE")
        # and not NO SHOW; ticket shouldn't have been updated

        # there should be *7* locates now
        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 7)

        # check how many records have been added to the responder queue
        q_len_after = len(self.tdb.getrecords("responder_queue"))
        self.assertEquals(q_len_after - q_len_before, 6)
        # 6 and not 7, because one of them has status -P ...?

    def test_no_show_2(self):
        """ Test no show tickets (2) """
        self.prepare('OCC1')
        self.assertEquals(len(self.tdb.getrecords("responder_queue")), 0)

        tl = ticketloader.TicketLoader("../testdata/test-noshow.txt")
        raw = tl.getTicket()
        drop("noshow-1.txt", raw)

        m = main.Main(verbose=0, call_center='OCC1')
        #m.log.lock = 1
        m.run(runonce=1)

        id = m.ids[-1]  # get the id of the ticket that shoulda been posted
        # close that ticket
        sql = """
         update locate
         set closed = 1, status = 'NC'
         where ticket_id = '%s'
        """
        sql = sql % (id)
        self.tdb.runsql(sql)
        self.assertEquals(self.tdb.isclosed(id), 1)
        # closing of locates apparently inserts records in the responder queue,
        # so make sure it's empty before we move on
        self.tdb.deleterecords("responder_queue")

        # post the second one... but inspect it first
        raw2 = tl.getTicket()
        t = self.handler.parse(raw2, ["Fairfax"])
        self.assertEquals(t.ticket_type, "NO SHOW")
        self.assertEquals(t.ticket_format, "OCC1")
        dups = m.df.find_duplicate_tickets(t)
        self.assertEquals(len(dups), 1)
        #dups, latest, latest_id, newlocates, reassign, parent_ticket_id = \
        dfresult = m.df.getduplicates(t, is_no_show=1)    # !!
        self.assertEquals(len(dfresult.dups), 1)

        logic = businesslogic.getbusinesslogic("OCC1", self.tdb, [])
        self.assertEquals(logic.is_no_show(t), 1)

        drop("noshow-2.txt", raw2)

        m.run(runonce=1)

        # there should only be one record with this ticket number
        sql = """
         select ticket_id
         from ticket
         where ticket_number = '02048441'
        """
        results = self.tdb.runsql_result(sql)
        self.assertEquals(len(results), 1)
        # there should be records in the responder queue now
        self.assertEquals(len(self.tdb.getrecords("responder_queue")),
         len(t.locates))

    def test_message(self):
        """ Test messages """
        # test what happens if we drop a message in the incoming dir.
        self.prepare('FCV2')
        tl = ticketloader.TicketLoader("../testdata/goodmorning.txt")
        raw = tl.getTicket()
        msg = self.handler.parse(raw, ["Richmond2"])
        self.assert_(isinstance(msg, ticketparser.GMMessage))

        drop("msg-1.txt", raw)

        m = main.Main(verbose=0, call_center='FCV2')
        #m.log.lock = 1
        m.run(runonce=1)

        # we're merely testing that no exception should be raised here; nothing
        # in the database changes

    def test_summary_updates(self):
        """ Test summary updates """

        self.prepare('SCA1', 'OCC1')

        # before we begin, make sure summary tables are empty
        self.assertEquals(len(self.tdb.getrecords("summary_header")), 0)
        self.assertEquals(len(self.tdb.getrecords("summary_detail")), 0)

        tl = ticketloader.TicketLoader("../testdata/sum-SouthCalifornia.txt")
        raw = tl.getTicket()
        drop("summ-1.txt", raw)
        drop("summ-2.txt", raw)

        m = main.Main(verbose=0, call_center='SCA1')
        m.run(runonce=1)

        headers = self.tdb.getrecords("summary_header")
        self.assertEquals(len(headers), 1)

        details = self.tdb.getrecords("summary_detail")
        self.assertEquals(len(details), 92*2)
        # there should be twice as many detail records, since we updated it

        # now try the same with OCC1 tickets, which don't allow "updates"
        tl = ticketloader.TicketLoader("../testdata/sum-Fairfax.txt")
        raw = tl.getTicket()
        drop("summ-3.txt", raw)
        drop("summ-4.txt", raw)

        m = main.Main(verbose=0, call_center='OCC1')
        m.log.lock = 1
        m.run(runonce=1)

        headers = self.tdb.getrecords("summary_header",
                  call_center="OCC1")
        self.assertEquals(len(headers), 1)
        hid = headers[0]["summary_header_id"]

        details = self.tdb.getrecords("summary_detail",
         summary_header_id=hid)
        self.assertEquals(len(details), 147)
        # *NOT* twice the number, just the number on the summary, the second
        # one isn't posted

    # TODO (possibly):
    # test that adds a ticket with locates, which checks if client_id is set
    # for all locates.
    # Also, make sure that pre-routing does this correctly. (Shouldn't affect
    # this really since pre-routing doesn't _post_ any locates...)

    def test_locates_client_id(self):
        """ Test if Main sets client_id of locates """
        self.prepare('Atlanta')

        # make sure USW01 is a client
        _testprep.make_client(self.tdb, 'USW01', 'Atlanta')

        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        drop("atl01.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.run(runonce=1)

        atl_clients = [c.client_code for c in m.clients["Atlanta"]]
        self.assert_("USW01" in atl_clients)

        results = self.tdb.getrecords("locate")
        self.assert_("USW01" in [row["client_code"] for row in results])
        self.assert_(results)
        for row in results:
            client_id = row.get("client_id", None)
            client_code = row.get("client_code")
            if client_code in atl_clients:
                self.assert_(client_id, "%s has no client_id" % (client_code))
            else:
                self.assertEquals(client_id, None)

    def test_due_date_updates(self):
        """ Updates should not change due date """
        # updates should not change the due date, but they can change other
        # fields...
        self.prepare('FCO1')
        tl = ticketloader.TicketLoader("../testdata/Colorado-1.txt")
        raw = tl.getTicket()
        raw = string.replace(raw, "EMER NEW STRT LREQ", "NORMAL")
        drop("col1new.txt", raw)

        # let Main have its way
        m = main.Main(verbose=0, call_center='FCO1')
        #m.log.lock = 1
        m.run(runonce=1)

        # a record should have been posted
        results = self.tdb.getrecords("ticket", ticket_format="FCO1")
        self.assertEquals(len(results), 1)
        due_date = results[0]["due_date"]
        self.assertEquals(due_date, "2002-08-01 23:59:59")

        # now update that raw ticket to create a new one
        raw2 = string.replace(raw, "ATDW00", "D00K13")
        raw2 = string.replace(raw2, "07/30/02 05:48 AM", "07/31/02 06:12 AM")
        drop("col1update.txt", raw2)

        m.run(runonce=1)

        # there should still be *one* record, with the *same* due date, but
        # other fields should be different
        results2 = self.tdb.getrecords("ticket", ticket_format="FCO1")
        self.assertEquals(len(results2), 1)
        self.assertEquals(results2[0]["due_date"], due_date)
        self.assertNotEquals(results[0]["transmit_date"],
         results2[0]["transmit_date"])

    test_due_date_updates.fails=1

    def test_client_id_with_routing(self):
        """ Test if client_id is set after routing """
        self.prepare('Atlanta')
        # get any old ticket
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        raw = raw.replace("TCG01", "NEW2002")
        drop("atl01.txt", raw)

        self.add_fake_client("Atlanta", "NEW2002")

        # store the ticket using main.py
        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.run(runonce=1)

        # assert the ticket is in the database
        id = m.ids[-1]
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_id"], id)

        if m._flag_non_clients:
            # make sure NEW2002 is set to -P, not -N
            sql = """
             update locate
             set status = '-P', closed=0
             where ticket_id = %s and client_code = '%s'
            """ % (id, 'NEW2002')
            self.tdb.runsql(sql)

        # add one of the non-client locates as a new client
        try:
            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.clients = self.tdb.get_clients_dict()
            #tr.log.lock = 1
            # assert that NEW2002 is found by router
            found = 0
            for row in tr.clients["Atlanta"]:
                if row.client_code == "NEW2002":
                    found = 1
            self.assert_(found, "NEW2002 not found by router")

            # then run the router
            tr.run(runonce=1)
            # see if the new client has a client_id

            # inspect some locates
            r1 = self.tdb.getrecords("locate", ticket_id=id,
                 client_code="NEW2002")[0]
            self.assertEquals(r1["status"], locate_status.assigned)
            self.assert_(r1["client_id"])

            r2 = self.tdb.getrecords("locate", ticket_id=id,
             client_code="USW01")[0]
            self.assertEquals(r2["status"], locate_status.assigned)
            self.assert_(r2["client_id"])

            # check if all client_ids are set
            locates = self.tdb.getrecords("locate", ticket_id=id)
            self.assert_(locates)
            for row in locates:
                if row["status"] == locate_status.assigned:
                    self.assert_(row["client_id"],
                     "locate %s should have a client_id" % (
                     row["client_code"],))
                else:
                    self.assertEquals(row.get("client_id", None), None,
                     "locate %s shouldn't have a client_id" % (
                     row["client_code"],))
        finally:
            self.tearDown()    # delete just about everything
            self.remove_fake_client("Atlanta", "NEW2002")

    def test_FAM1_two_formats(self):
        # FAM1 (and FAU2, for that matter) changes formats.  Allowing two
        # formats (for now) allows for an intermediary period where both the
        # old and the new format are allowed. """

        self.prepare('FAM1', 'FCL2')

        '''
        # just to be certain...
        self.config.processes = [
         # one set for general testing...
         #["test_incoming", "", "test_processed", "test_error", "test"],
         {'incoming': 'test_incoming',
          'format': '',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},

         # and one for FCL2 specifically
         #["test_incoming_FCL2", "FCL2", "test_processed_FCL2",
         # "test_error_FCL2", "test"],
         {'incoming': 'test_incoming_FCL2',
          'format': 'FCL2',
          'processed': 'test_processed_FCL2',
          'error': 'test_error_FCL2',
          'attachments': ''},

         # and one for FAM1
         #["test_incoming_FAM1", "FAM1", "test_processed_FAM1",
         # "test_error_FAM1", "test"],
         {'incoming': 'test_incoming_FAM1',
          'format': 'FAM1',
          'processed': 'test_processed_FAM1',
          'error': 'test_error_FAM1',
          'attachments': ''},

        ]
        '''

        # copy old AlabamaMobile tickets to directory
        shutil.copyfile("../testdata/AlabamaMobile-1.txt",
                        "test_incoming\AlabamaMobile-1.txt")

        m = main.Main(verbose=0, call_center='FAM1')
        #m.log.lock = 1
        m.run(runonce=1)

        tickets = self.tdb.getrecords("ticket", ticket_format="FAM1")
        self.assertEquals(len(tickets), 6)

        # copy new AlabamaMobileNew tickets to directory
        shutil.copyfile("../testdata/AlabamaMobileNew-1.txt",
                        "test_incoming\AlabamaMobileNew-1.txt")

        m.run(runonce=1)

        tickets = self.tdb.getrecords("ticket", ticket_format="FAM1")
        self.assertEquals(len(tickets), 6+5)

    def test_update_seq_number(self):
        tl = ticketloader.TicketLoader("../testdata/Baltimore-1.txt")
        raw = tl.getTicket()

        drop("bal01.txt", raw)
        m = main.Main(verbose=0, call_center='FMB1')
        #m.log.lock = 1
        m.run(runonce=1)

        # are tickets and locates really in database?
        ticket_id = m.ids[-1]
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        self.assert_(locates)
        found = 0
        for locdict in locates:
            if locdict['client_code'] == 'BGE09':
                self.assertEquals(locdict['seq_number'], '0121')
                found = 1
        self.assert_(found, "BGE09 not found")

        # set status to -N
        sql = "update locate set status = '-N'"
        self.tdb.runsql(sql)

        # now, put in an updated ticket
        raw = raw.replace("09:01", "09:02")
        raw = raw.replace("Seq No: 0121", "Seq No: 0122")

        drop("bal02.txt", raw)
        m.run(runonce=1)

        # did the ticket get updated?
        tickets = self.tdb.getrecords("ticket")
        self.assertEquals(len(tickets), 1)
        self.assertEquals(tickets[0]['transmit_date'], '2002-05-14 09:02:00')
        ticket_id = tickets[0]['ticket_id']

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        self.assert_(locates)
        found = 0
        for locdict in locates:
            if locdict['client_code'] == 'BGE09':
                self.assertEquals(locdict['seq_number'], '0122')
                found = 1
        self.assert_(found, "BGE09 not found")

    test_update_seq_number.has_error=1

    def test_Harlingen_map_ref(self):
        # map_ref should only be stored for tickets from SSR/SSM.  We ignore
        # other term ids.

        tl = ticketloader.TicketLoader("../testdata/Harlingen-1.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ['Harlingen'])
        self.assertEquals(t.ticket_number, "022940580")

        id = self.tdb.insertticket(t)

        raw2 = raw.replace('CSD', 'SSR')
        raw2 = raw2.replace('HIDALGO', 'ARISTOTELES')
        t2 = self.handler.parse(raw2, ['Harlingen'])
        self.tdb.updateticket(id, t2)

        rows = self.tdb.getrecords('ticket', ticket_id=id)
        self.assertEquals(rows[0]['map_ref'], 'ARISTOTELES')

        self.tdb.updateticket(id, t)  # store CSD ticket again
        # map_ref should be unchanged
        rows = self.tdb.getrecords('ticket', ticket_id=id)
        self.assertEquals(rows[0]['map_ref'], 'ARISTOTELES')

    def test_centerpoint(self):
        # first, post a "possibly Centerpoint" ticket
        tl = ticketloader.TicketLoader('../testdata/Houston1-1.txt')
        raw = tl.tickets[0]
        raw = raw.replace("485", "450") # fix centerpoint value
        t = self.handler.parse(raw, ['Houston1'])
        self.assertEquals(centerpoint.is_centerpoint(t), 0)
        self.assertEquals(centerpoint.is_possibly_centerpoint(t), 1)

        # make sure FHL1 is active
        self.tdb.runsql("""
          update call_center
          set active = 1
          where cc_code = 'FHL1'
        """)

        drop('cp1.txt', raw)

        m = main.Main(verbose=0, call_center='FHL1')
        #m.log.lock = 1
        m.run(runonce=1)
        ticket_id = m.ids[-1]

        rows = self.tdb.dbado.getrecords('ticket', ticket_id=ticket_id)
        self.assertEquals(len(rows), 1)
        self.assertNotEqual(rows[0].get('do_not_mark_before'), None)

        # take this same ticket, turn it into a CP ticket, and change the time
        raw = raw.replace('09:57AM', '10:01PM')
        raw = raw.replace('TXUC01', 'ELEC_3PH')
        t = self.handler.parse(raw, ['Houston1'])
        self.assertEquals(centerpoint.is_centerpoint(t), 1)
        self.assertEquals(centerpoint.is_possibly_centerpoint(t), 0)
        self.tdb.dbado.runsql("update locate set status='-R' where ticket_id=%s" % (ticket_id,))

        drop('cp2.txt', raw)

        m.run(runonce=1)

        rows = self.tdb.dbado.getrecords('ticket', ticket_id=ticket_id)
        self.assertEquals(rows[0]['transmit_date'], '2002-10-21 22:01:00')
        self.assertEquals(rows[0].get('do_not_mark_before'), None)

        locates = self.tdb.dbado.getrecords('locate', ticket_id=ticket_id)
        statuses = [loc['status'] for loc in locates]
        self.assertEquals(statuses, ['-R', '-P'])

    def test_incomplete_summaries_1(self):
        """ Test what main.py does with incomplete summaries. """
        self.prepare('NewJersey')
        tl = ticketloader.TicketLoader("../testdata/sum-NewJersey.txt")
        raw = tl.tickets[3]
        drop("sumnj-inc.txt", raw)

        m = main.Main(verbose=0, call_center='NewJersey')
        #m.log.lock = 1
        m.run(runonce=1)

        for summ in m.sc.data['NewJersey', 'NJN']:
            # 24h earlier
            d = date.Date(summ._timestamp)
            d.dec_time(seconds=60*60*24)
            summ._timestamp = d.isodate()

        raw = tl.tickets[0]
        drop("sumnj2.txt", raw)
        m.run(runonce=1)

        # IMPORTANT: call this method explicitly here... for some reason it
        # doesn't work without dropping and processing another summary first.
        # If there are problems with this method, we'll find out here.
        try:
            #m.log.lock = 1
            m.check_incomplete_summaries()
        finally:
            try:
                self.empty_summary_fragment()
            except:
                pass

    def test_incomplete_summaries_2(self):
        """ Test what main.py does with multipart summaries. """
        self.prepare('NewJersey')

        try:
            self.empty_summary_fragment()
        except:
            pass

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","NewJersey2","GSU1-2009-05-13-00031.txt"))
        raw = tl.tickets[0]
        drop("sum_page_1.txt", raw)

        m = main.Main(verbose=0, call_center='NewJersey')

        m.log.logger.logfile = StringIO()
        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","NewJersey2","GSU1-2009-05-13-00032.txt"))
        raw = tl.tickets[0]
        drop("sum_page_2.txt", raw)
        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","NewJersey2","GSU1-2009-05-13-00033.txt"))
        raw = tl.tickets[0]
        drop("sum_page_3.txt", raw)
        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","NewJersey2","GSU1-2009-05-13-00034.txt"))
        raw = tl.tickets[0]
        drop("sum_page_5.txt", raw)
        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","NewJersey2","GSU1-2009-05-13-00035.txt"))
        raw = tl.tickets[0]
        drop("sum_page_4.txt", raw)
        m.run(runonce=1)

        header = self.tdb.find_latest_summary("NewJersey","NJN")
        self.assertTrue(int(header.complete))

        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

    def test_is_UQ_client(self):
        self.prepare('XYZ')
        _testprep.add_test_call_center(self.tdb, 'XYZ')
        _testprep.add_test_client(self.tdb, 'XYZ', 'XYZ')
        call_centers.cc2format['XYZ'] = []
        self.prepare('XYZ')
        m = main.Main(verbose=0, call_center='XYZ')
        self.assertEquals(m.is_UQ_client('Atlanta', 'USW01'), True)
        self.assertEquals(m.is_UQ_client('Atlanta', 'ABS01'), False)
        self.assertEquals(m.is_UQ_client('Zigzag', 'USW01'), False)
        self.assertEquals(m.is_UQ_client('Atlanta', 'R2D2'), False)
        self.assertEquals(m.is_UQ_client('XYZ', 'XYZ'), True)

    def test_flag_non_clients(self):
        self.prepare('Atlanta')

        self.tdb.deleterecords('client', oc_code='AGL18', call_center='Atlanta')
        _testprep.add_test_client(self.tdb, 'AGL18', 'Atlanta', False, False)

        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])

        m = main.Main(verbose=0, call_center='Atlanta')
        m.flag_non_clients(t)
        for loc in t.locates:
            if loc.client_code == 'USW01':
                self.assertEquals(loc.status, '-P',
                 loc.client_code + " should have status -P")
            else:
                self.assertEquals(loc.status, '-N',
                 loc.client_code + " should have status -N")

    def test_7501_emergency(self):
        self.prepare('7501')
        tl = ticketloader.TicketLoader("../testdata/7501-emer-1.txt")
        raw = tl.tickets[0]
        drop("7501-emer-1.txt", raw)

        # make sure we have at least one client
        _testprep.add_test_client(self.tdb, 'TOF', '7501')

        m = main.Main(verbose=0, call_center='7501')
        m.log.lock = 1
        m.run(runonce=1)

        tickets = self.tdb.getrecords("ticket")
        self.assertEquals(len(tickets), 1)
        row = tickets[0]
        self.assertEquals(row['ticket_number'], "0455865")
        self.assertEquals(row['ticket_type'], "NEW XCAV EMER")
        self.assertEquals(row['due_date'], "2005-02-14 13:54:00")
        self.assertEquals(row['kind'], "EMERGENCY")

    def test_LOR1_serial(self):
        tl = ticketloader.TicketLoader("../testdata/lor1-serial.txt")
        raw = tl.tickets[0]
        drop("lor1a.txt", raw)

        m = main.Main(verbose=0, call_center='LOR1')
        m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords("ticket", ticket_format="LOR1")
        self.assertEquals(len(rows), 1)
        id = rows[0]['ticket_id']
        self.assertEquals(rows[0]['serial_number'], None)

        raw = tl.tickets[1]
        drop("lqw1a.txt", raw)

        del m
        m = main.Main(verbose=0, call_center='LQW1')
        m.log.lock = 1
        m.run(runonce=1)
        # it should be adapting

        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['serial_number'], "OOC2005030900065")
        self.assertEquals(rows[0]['source'], 'LOR1')

    def test_SCA1_G_locates(self):
        self.prepare('SCA1')
        tl = ticketloader.TicketLoader("../testdata/SouthCalifornia-1.txt")
        raw = tl.getTicket()
        raw = string.replace(raw, "CAW03", "SDG09")
        raw = string.replace(raw, "CMW52", "SDG08")
        ae = self.assertEquals

        drop("sca1a.txt", raw)

        m = main.Main(verbose=0, call_center='SCA1')
        m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        ticket_id = rows[0]['ticket_id']

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        self.assertEquals(len(locates), 12)
        for locrow in locates:
            if locrow['client_code'].endswith('G'):
                ae(locrow['added_by'], 'RULE')
            else:
                ae(locrow['added_by'], 'PARSER')

    def test_source(self):
        self.prepare('SCA1')
        tl = ticketloader.TicketLoader("../testdata/SouthCalifornia-1.txt")
        raw = tl.tickets[0]
        drop("scasource.txt", raw)
        raw2 = tl.tickets[1]
        raw2 = raw2.replace('UTI50', 'UQSTSO')
        drop("scasource2.txt", raw2)

        m = main.Main(verbose=0, call_center='SCA1')
        m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 2)
        self.assertEquals(rows[0]['source'], 'UTI50')
        self.assertEquals(rows[1]['source'], 'SCA1')

        tvrows = self.tdb.getrecords("ticket_version")
        self.assertEquals(len(tvrows), 2)
        self.assertEquals(tvrows[0]['source'], 'UTI50')
        self.assertEquals(tvrows[1]['source'], 'SCA1')

    def test_LAK1_attachments(self):
        # get ticket and drop it
        tl = ticketloader.TicketLoader("../testdata/Alaska-1.txt")
        raw = tl.tickets[0]
        drop("LAK1-2005-07-12-0081.txt", raw, 'test_incoming_LAK1')

        # drop attachment
        f = open(os.path.join(_testprep.TICKET_PATH, "receiver", "attachments",
            "LAK1-2005-07-12-0081-A01.txt"), "rb")
        data = f.read()
        f.close()
        drop("LAK1-2005-07-12-0081-A01.txt", data, 'test_attachments_LAK1')

        m = main.Main(verbose=0, call_center='LAK1')
        m.log.lock = 1
        m.run(runonce=1)

    def test_alert_1(self):
        # Test client-based alerts in main.py.
        self.prepare('Atlanta')

        # create test client that has alert=1
        self.tdb.delete('client', oc_code='ALERT0')
        client_id = _testprep.add_test_client(self.tdb, 'ALERT0', 'Atlanta')
        self.tdb.runsql("""
          update client
          set alert=1
          where client_id = %s
        """ % (client_id,))

        # drop a ticket that has ALERT0 as a client
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("OMN01", "ALERT0")
        drop("Atlanta-alert.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        m.log.lock = 1
        m.run(runonce=1)

        ticket_id = m.ids[-1]
        rows = self.tdb.getrecords("ticket", ticket_id=ticket_id)
        self.assertEquals(rows[0]['ticket_number'], '12311-109-056')
        self.assertEquals(rows[0]['alert'], 'A')

        # now test what happens to a ticket that's guaranteed to have
        # "non-alert" clients only (but it does have clients)
        self.tdb.delete('client', oc_code='NOALERT')
        _testprep.add_test_client(self.tdb, "NOALERT", "Atlanta")
        raw = tl.tickets[0]
        raw = raw.replace('12311-109-056', '12311-109-057')
        raw = raw.replace("AGL18", "NOALERT")
        r = re.compile("ATT02.*?USW01", re.DOTALL|re.MULTILINE)
        raw = r.sub("", raw) # now we only have one term id, NOALERT
        drop("Atlanta-alert-2.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        m.log.lock = 1
        m.run(runonce=1)

        ticket_id = m.ids[-1]
        rows = self.tdb.getrecords("ticket", ticket_id=ticket_id)
        self.assertEquals(rows[0]['ticket_number'], '12311-109-057')
        self.assertEquals(rows[0]['alert'], None)

    def test_alert_4(self):
        # Test client-based alerts in main.py.
        self.prepare('6001', '6004')

        # drop all of the 6001/6004 tickets from 2007-10-03
        cc = call_centers.get_call_centers(self.tdb) # CallCenters object
        try:
            update_call_centers_orig = copy.deepcopy(cc.ucc)
            if not cc.update_call_center('6004'):
                cc.ucc['6004'] = '6001'
            test_client_id = _testprep.add_test_client(self.tdb, 'FN2', '6001',
                             active=1, alert=1)
            c6001_tickets = glob(os.path.join(_testprep.TICKET_PATH,
              "sample_tickets", "6001-2007-10-03-*.txt"))
            c6004_tickets = glob(os.path.join(_testprep.TICKET_PATH,
              "sample_tickets", "6004-2007-10-03-*.txt"))
            for ticket_file in c6001_tickets:
                tl = ticketloader.TicketLoader(ticket_file)
                raw = tl.tickets[0]
                drop(os.path.basename(ticket_file), raw)

            m = main.Main(verbose=0, call_center='6001')
            m.log.lock = 1
            m.run(runonce=1)

            for ticket_file in c6004_tickets:
                tl = ticketloader.TicketLoader(ticket_file)
                raw = tl.tickets[0]
                drop(os.path.basename(ticket_file), raw)

            m = main.Main(verbose=0, call_center='6004')
            m.log.lock = 1
            m.run(runonce=1)

            for ticket_id in m.ids:
                rows = self.tdb.getrecords("ticket", ticket_id=ticket_id)
                self.assertEquals(rows[0]['alert'], 'A')
        finally:
            _testprep.clear_database(self.tdb)
            _testprep.delete_test_client(self.tdb, test_client_id)
            cc.ucc = update_call_centers_orig

    def test_alert_5(self):
        # Test ticket alert based on keyword
        self.prepare('SCA1')

        if static_tables.alert_ticket_keyword and static_tables.alert_ticket_type:
            try:
                self.tdb.insertrecord('alert_ticket_type', call_center = 'SCA1', ticket_type = 'REMK REMK GRID')
                rec_id = self.tdb.dbado.get_max_id('alert_ticket_type', 'alert_ticket_type_id')
                self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'Exposed')
                self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'RESPOND')
                self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'UTI')
                self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'Further Marks')
                self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'ASAP')

                # parse tickets with that term id on it
                tickets = glob(os.path.join(_testprep.TICKET_PATH,"sample_tickets","SCA1","sca1-2009-08-21-*.txt"))
                for ticket_file in tickets:
                    tl = ticketloader.TicketLoader(ticket_file)
                    raw = tl.getTicket()
                    drop(os.path.basename(ticket_file), raw)

                m = main.Main(verbose=0, call_center='SCA1')
                m.log.lock = 1
                m.run(runonce=1)

                for ticket_id in m.ids:
                    rows = self.tdb.getrecords("ticket", ticket_id=ticket_id)
                    self.assertEquals(rows[0]['alert'], 'A')
            finally:
                _testprep.clear_database(self.tdb)
                self.tdb.deleterecords('alert_ticket_keyword', alert_ticket_type_id = rec_id)
                self.tdb.deleterecords('alert_ticket_type', alert_ticket_type_id = rec_id)

    def test_alert_2(self):
        # Test HP-grid based alerts in main.py.
        self.prepare('FCL1')

        # add some test records
        self.tdb.delete('hp_grid', call_center='FCL1')
        _testprep.add_test_hp_grid(self.tdb, 'FCL1', 'ABC', 'Harry')
        _testprep.add_test_hp_grid(self.tdb, 'FCL1', 'ABC', 'Ron')
        _testprep.add_test_hp_grid(self.tdb, 'FCL1', 'ABC', 'Hermione')
        _testprep.add_test_hp_grid(self.tdb, 'FCL1', 'DEF', 'Snape')
        _testprep.add_test_hp_grid(self.tdb, 'FCL1', 'PQR', 'Dumbledore')

        # make sure these are clients:
        _testprep.add_test_client(self.tdb, 'ABC', 'FCL1')
        _testprep.add_test_client(self.tdb, 'DEF', 'FCL1')
        _testprep.add_test_client(self.tdb, 'PQR', 'FCL1')

        # drop a ticket that has ABC as a client
        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-2.txt")
        raw = tl.tickets[1]
        raw = raw.replace("BEC02", "ABC")
        raw = raw.replace("3517C8113B   3517C8113C   3517D8113B   3517D8113C",
                          "Harry Ron Seamus Ginny Hermione")
        t = self.handler.parse(raw, ['NorthCarolina'])
        drop("NC-alert.txt", raw)

        m = main.Main(verbose=0, call_center='FCL1')
        m.log.lock = 1
        m.run(runonce=1)

        ticket_id = m.ids[-1]

        # check if the grids were really saved
        grids = self.tdb.getrecords("ticket_grid", ticket_id=ticket_id)
        self.assertEquals(len(grids), 5)

        rows = self.tdb.getrecords("ticket", ticket_id=ticket_id)
        self.assertEquals(rows[0]['ticket_number'], 'A0062729')
        self.assertEquals(rows[0]['ticket_format'], 'FCL1')
        self.assertEquals(rows[0]['alert'], 'A')

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        for locrow in locates:
            if locrow['client_code'] == 'ABC':
                self.assertEquals(locrow['alert'], 'A')
            else:
                self.assertEquals(locrow['alert'], None)

    def test_alert_3(self):
        """
        Test HP-address based alerts in main.py.
        """
        self.prepare('FCL1')

        # add some test records
        self.tdb.delete('hp_address', call_center='XYZ')
        self.tdb.delete('hp_address', call_center='ZYZ')
        _testprep.add_test_hp_address(self.tdb, 'FCL1', 'ABC', 'Linden Ln.', 'GASTONIA OUT', 'GASTON', 'NC')
        _testprep.add_test_hp_address(self.tdb, 'FCL1', 'ABC', 'Norwich Dr.', 'Fort Worth', 'MECKLENBURG', 'NC')
        _testprep.add_test_hp_address(self.tdb, 'FCL1', 'ABC', 'Hwy 322', 'Kilgore', 'Rusk', 'NC')
        _testprep.add_test_hp_address(self.tdb, 'FCL1', 'DEF', 'Maumelle', 'Plano', 'Collin', 'NC')
        _testprep.add_test_hp_address(self.tdb, 'FCL1', 'PQR', 'Alma', 'Dallas', 'Dallas', 'NC')

        # make sure these are clients:
        client_id = _testprep.add_test_client(self.tdb, 'ABC', 'FCL1')
        _testprep.add_test_client(self.tdb, 'DEF', 'FCL1')
        _testprep.add_test_client(self.tdb, 'PQR', 'FCL1')

        sql = """
         update client
         set grid_email = 'bogus@bogus.com'
         where client_id = %s
        """ % (client_id,)
        self.tdb.runsql(sql)

        # drop a ticket that has ABC as a client
        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-2.txt")
        raw = tl.tickets[1]
        # Replace the real client with the fake client
        raw = raw.replace("BEC02", "ABC")
        # Replace the real address with the fake address
        raw = raw.replace("Street  : PACOLET CT   Intersection: N",
                          "Street  : LINDEN LN.   Intersection: N")
        t = self.handler.parse(raw, ['NorthCarolina'])
        drop("NC-alert.txt", raw)

        mock_sendmail = make_logger()
        with Mockery(mail2, 'sendmail', mock_sendmail):

            m = main.Main(verbose=0, call_center='FCL1')
            #m.log.lock = 1
            m.log.logger.logfile.truncate(0)
            m.run(runonce=1)

        # Verify alert logged
        match, next_log_entry = SearchLogFile(m.log.logger.logfile.name,"Ticket alert set for #A0062729")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(m.log.logger.logfile.name,"Locate alert set for: ABC")
        self.assertNotEqual(match,None)

        # Verify fake email
        logged = mock_sendmail.log[0][0]
        body = logged[3]
        self.assertNotEquals(body.find('Street -  LINDEN LN.'), -1)
        self.assertNotEquals(body.find('City   -  GASTONIA OUT'), -1)
        self.assertNotEquals(body.find('County -  GASTON'), -1)
        self.assertNotEquals(body.find('State  -  NC'), -1)

        ticket_id = m.ids[-1]

        rows = self.tdb.getrecords("ticket", ticket_id=ticket_id)
        self.assertEquals(rows[0]['ticket_number'], 'A0062729')
        self.assertEquals(rows[0]['ticket_format'], 'FCL1')
        self.assertEquals(rows[0]['alert'], 'A')

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        for locrow in locates:
            if locrow['client_code'] == 'ABC':
                self.assertEquals(locrow['alert'], 'A')
            else:
                self.assertEquals(locrow['alert'], None)

        self.tdb.delete('hp_address', call_center='XYZ')
        self.tdb.delete('hp_address', call_center='ZYZ')

    def test_LNG1_image(self):
        # when LNG1 updates an existing LOR1/LWA1 ticket, the original image
        # should not be overwritten.
        self.prepare('LNG1', 'LOR1', 'LWA1')

        tl = ticketloader.TicketLoader("../testdata/Oregon2-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("2004", "2006")
        raw = raw.replace("/04", "/06")
        drop("a-LOR1.txt", raw)

        m = main.Main(verbose=0, call_center='LOR1')
        m.log.lock = 1
        m.run(runonce=1)

        tl = ticketloader.TicketLoader("../testdata/Northwest-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("6159514", "4065047")
        drop("b-LNG1.txt", raw)

        m = main.Main(verbose=0, call_center='LNG1')
        m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords("ticket", ticket_number="4065047")
        self.assertEquals(len(rows), 1)
        self.assert_(rows[0]['image'].find("48 HOUR NOTICE") >= 0)
        # this works because images must always be inserted, but for updates
        # they don't necessarily need to be present

    def test_batonrouge_nomark(self, format='BatonRouge1', call_center='FBL1'):
        # these tickets have no "mark by" date, which means the due date is
        # empty and must be computed separately.  make sure that this indeed
        # happens, especially for updates.

        self.prepare(call_center)
        tl = ticketloader.TicketLoader("../testdata/batonrouge-nomark.txt")
        raw = tl.tickets[0]
        drop("batonrouge-nomark.txt", raw)
        drop("batonrouge-nomark-2.txt", raw)

        m = main.Main(verbose=0, call_center=call_center)
        m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords("ticket", ticket_number="70192639")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['ticket_format'], call_center)
        self.assertEquals(rows[0]['due_date'], '2007-05-10 12:44:00')
        self.assertEquals(rows[0]['legal_due_date'], '2007-05-10 12:44:00')

    def test_batonrouge_nomark_1411(self):
        # test this for 1411 as well
        self.prepare('1411', 'Atlanta')
        self.test_batonrouge_nomark(format='LA1411', call_center='1411')

    def test_get_ticket_notes(self):
        """Test whether the notes associated with the ticket and locates is returned"""

        # create a ticket and post it
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        tp = ticketparser.TicketHandler()
        ticket = tp.parse(raw, ["Atlanta"])
        ticket_id = self.tdb.insertticket(ticket)
        # insert some notes for it
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        results = self.tdb.getrecords("locate")
        client_id = results[12]['client_id']
        locate_id = results[12]['locate_id']
        for row in results:
            # insert a locate note
            sql = """
             insert notes
             (foreign_type, foreign_id, entry_date,modified_date,uid,active,note)
             values (5, '%s', getdate(),getdate(),%s,1,'A locate note')
            """ % (row['locate_id'],uid)
            self.tdb.runsql(sql)
        notes = self.tdb.get_all_notes(ticket_id,locate_id)
        # There should be two notes
        self.assertEqual(len(notes),2)
        self.assertEqual(notes[0],'foo')
        records = self.tdb.getrecords("client",client_id=client_id)
        self.assertEqual(notes[1],records[0]['client_name']+': A locate note')

    def test_LPMEET_ticket(self):
        """ Test LPMEET ticket processing"""
        self.prepare('FAU4')

        # make sure we have certain clients and they are active
        term_ids = ['FLI70', 'MID70', 'GP420']
        for term_id in term_ids:
            self.tdb.runsql("""
              update client
              set active = 1
              where oc_code = '%s'
              and call_center = 'FAU4'
            """ % term_id)

        tl = ticketloader.TicketLoader("../testdata/sample_tickets/FAU4-2007-12-04-00001.txt")
        raw = tl.getTicket()

        drop("LPMEET.txt", raw)

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='FAU4')
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        handler = ticketparser.TicketHandler()
        raw = tl.tickets[0]
        t = handler.parse(raw, ['Atlanta4', 'Atlanta4a'])
        clients_to_expect = 0

        clients = self.tdb.get_clients_dict().get(t.ticket_format, [])
        for locate in t.locates:
            for client in clients:
                if client.client_code == locate.client_code:
                    clients_to_expect += 1
                    break

        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 15)
        clients_rcvd = 0
        for res in results:
            if res["client_id"] is not None:
                clients_rcvd += 1
        self.assertEquals(clients_to_expect, clients_rcvd)

    def test_FL9003_ticket(self):
        """ Test FL9003 ticket processing"""
        # Add call center
        _testprep.add_test_call_center(self.tdb, '9003')
        self.prepare('9003', '9001')
        # Add client
        _testprep.add_test_client(self.tdb, 'BOGUS', 9001, active=1,
                                  update_call_center=9003)
        _testprep.set_client_inactive(self.tdb, '9001', 'SBF20')

        tl = ticketloader.TicketLoader(os.path.join("../testdata",
             "sample_tickets", "9003-2007-12-11-00054.txt"))
        raw = tl.getTicket()
        # Add the test client to the ticket
        raw = raw.replace('Mbrs :  COO600 CVCFTV FPC322 MCIU01 OUC553 OUC582 PGSORL SBF20  LS1104 ST1259\n',\
                          'Mbrs :  COO600 CVCFTV FPC322 MCIU01 OUC553 OUC582 PGSORL SBF20  LS1104 ST1259\nMbrs :  BOGUS')

        drop("FL9003.txt", raw)

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='9003')
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        t = self.tdb.getticket(id)
        clients_to_expect = 0

        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 11)

        clients = self.tdb.get_clients_dict().get(t.ticket_format, [])
        for locate in t.locates:
            for client in clients:
                if client.client_code == locate.client_code:
                    clients_to_expect += 1
                    break

        clients_rcvd = 0
        for res in results:
            if res["client_id"] is not None:
                clients_rcvd += 1
        self.assertEquals(clients_to_expect, clients_rcvd)

    def test_LCC1_Summary(self):
        """
        Test LCC1 Summary.
        """
        _testprep.add_test_call_center(self.tdb, 'LCC1')
        self.prepare('LCC1')
        # create a ticket file
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LCC1_audit.txt"))
        raw = tl.getTicket()
        drop("test1.txt", raw)

        m = main.Main(verbose=0, call_center='LCC1')
        m.log.logger.logfile.truncate(0)
        m.run(runonce=1)
        id = m.ids[0]   # id of ticket that was just posted
        # Verify that summary ticket was parsed
        match, next_log_entry = SearchLogFile(m.log.logger.logfile.name,"Processing summary (LCC1, LCC1) (new)")
        self.assertNotEqual(match,None)
        _testprep.delete_test_call_center(self.tdb, 'LCC1')

    def execute_raise_1(self, *args, **kwargs):
        raise dbinterface.DBException(self.err_msg)

    def execute_raise_2(self, *args, **kwargs):
        raise dbinterface.DBFatalException(self.err_msg)

    def test_ep_1(self):
        """
        Mock the dbinterface_ado.DBInterfaceADO._runsql_with_parameters method so it raises a DBException.
        Verify that no email is sent if the exception is from a deadlock.
        This test raises the DBException exception while updating.
        """
        self.prepare('Atlanta')
        self.err_msg = \
          "Transaction (Process ID 94) was deadlocked on lock"+\
          " resources with another process and has been chosen as the"+\
          " deadlock victim. Rerun the transaction."

        # create a ticket and post it
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        tp = ticketparser.TicketHandler()
        ticket = tp.parse(raw, ["Atlanta"])
        id = self.tdb.insertticket(ticket)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        # make sure there are locates with status -N; we'll look at them later
        z = self.tdb.getrecords("locate", ticket_id=id,
            status=locate_status.not_a_customer)
        self.assert_(len(z) > 0)

        # then drop a file with that same ticket in incoming
        drop("test-r.txt", raw)

        # Mock dbinterface_ado.DBInterfaceADO._runsql_with_parameters
        # XXX Not IronPython-compatible; either make test CPython-only or make
        # more generic.
        with Mockery(dbinterface_ado.DBInterfaceADO, '_runsql_with_parameters',
             self.execute_raise_1):

            # let main pick it up
            m = main.Main(verbose=0, call_center='Atlanta')
            m.log.lock = 1
            m.log.logger.logfile.truncate(0)
            m.run(runonce=1)

        logfile_name = m.log.logger.logfile.name
        # Verify that no email was sent
        match, next_log_entry = SearchLogFile(logfile_name,
          "Mail notification sent to")

        # debugging info
        if next_log_entry is not None:
            m.log.logger.logfile.seek(0)
            for line in m.log.logger.logfile.readlines():
                print line
        self.assertEquals(next_log_entry, None)

        # Verify that there was the assigned exception
        match, next_log_entry = SearchLogFile(logfile_name, "DBException")
        if next_log_entry is None:
            m.log.logger.logfile.seek(0)
            for line in m.log.logger.logfile.readlines():
                print line
        self.assertNotEquals(next_log_entry, None)

    def test_ep_2(self):
        """
        Mock the dbinterface_ado.DBInterfaceADO._runsql_with_parameters method
        so it raises a DBFatalException.
        Verify that no email is sent if the exception is from a deadlock.
        This test raises the DBFatalException exception while updating.
        """
        self.prepare('Atlanta')
        self.err_msg = "Transaction (Process ID 94) was deadlocked on lock"\
                     + " resources with another process and has been chosen"\
                     + " as the deadlock victim. Rerun the transaction."

        # create a ticket and post it
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        tp = ticketparser.TicketHandler()
        ticket = tp.parse(raw, ["Atlanta"])
        id = self.tdb.insertticket(ticket)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        # make sure there are locates with status -N; we'll look at them later
        z = self.tdb.getrecords("locate", ticket_id=id,
            status=locate_status.not_a_customer)
        self.assertTrue(len(z) > 0)

        # then drop a file with that same ticket in incoming
        drop("test-r.txt", raw)

        # Mock dbinterface_ado.DBInterfaceADO._runsql_with_parameters
        with Mockery(dbinterface_ado.DBInterfaceADO, '_runsql_with_parameters',
             self.execute_raise_2):

            # let main pick it up
            m = main.Main(verbose=0, call_center='Atlanta')
            m.log.lock = 1
            m.log.logger.logfile.truncate(0)
            m.run(runonce=1)

        logfile_name = m.log.logger.logfile.name
        # Verify that no email was sent
        match, next_log_entry = SearchLogFile(
          logfile_name, "Mail notification sent to")
        if next_log_entry is not None:
            m.log.logger.logfile.seek(0)
            for line in m.log.logger.logfile.readlines():
                print line
        self.assertEquals(next_log_entry,None)
        # Verify that there was the assigned exception
        match, next_log_entry = SearchLogFile(logfile_name, "DBFatalException")
        self.assertNotEquals(next_log_entry,None)

    def test_ep_3(self):
        """ Mock the dbinterface_ado.DBInterfaceADO._runsql_with_parameters
            method so it raises a DBException.
            Verify that no email is sent if the exception is from a deadlock.
            This test raises the DBException exception while inserting.
        """
        self.prepare('Atlanta')
        self.err_msg = "Transaction (Process ID 94) was deadlocked on lock"\
                     + " resources with another process and has been chosen"\
                     + " as the deadlock victim. Rerun the transaction."

        # create a ticket file
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        drop("test1.txt", raw)

        with Mockery(dbinterface_ado.DBInterfaceADO, '_runsql_with_parameters',
             self.execute_raise_1):

            m = main.Main(verbose=0, call_center='Atlanta')
            m.log.logger.logfile.truncate(0)
            m.run(runonce=1)

        logfile_name = m.log.logger.logfile.name
        # Verify that no email was sent
        match, next_log_entry = SearchLogFile(logfile_name,
          "Mail notification sent to")
        if next_log_entry is not None:
            dump_log(m)
        self.assertEquals(next_log_entry,None)
        # Verify that there was the assigned exception
        match, next_log_entry = SearchLogFile(logfile_name, "DBException")
        self.assertNotEquals(next_log_entry, None)
        match, next_log_entry = SearchLogFile(logfile_name,
          "DBException: ('Transaction (Process ID 94) was deadlocked")
        if next_log_entry is None:
            dump_log(m)
        self.assertNotEquals(next_log_entry, None)

    def test_ep_4(self):
        """
        Mock the dbinterface_ado.DBInterfaceADO._runsql_with_parameters method so it raises a DBException.
        Verify that no email is sent if the exception is from a deadlock.
        This test raises the DBFatalException exception while inserting.
        """
        self.prepare('Atlanta')
        self.err_msg = "Transaction (Process ID 94) was deadlocked on lock"\
                     + " resources with another process and has been chosen"\
                     + " as the deadlock victim. Rerun the transaction."

        # create a ticket file
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        drop("test1.txt", raw)

        with Mockery(dbinterface_ado.DBInterfaceADO, '_runsql_with_parameters',
             self.execute_raise_2):

            m = main.Main(verbose=0, call_center='Atlanta')
            m.log.logger.logfile.truncate(0)
            m.run(runonce=1)

        logfile_name = m.log.logger.logfile.name
        # Verify that no email was sent
        match, next_log_entry = SearchLogFile(
          logfile_name, "Mail notification sent to")
        if match is not None:
            m.log.logger.logfile.seek(0)
            for line in m.log.logger.logfile.readlines():
                print line
        self.assertEquals(match, None)
        # Verify that there was the assigned exception
        match, next_log_entry = SearchLogFile(logfile_name, "DBFatalException")
        self.assertNotEquals(match,None)
        match, next_log_entry = SearchLogFile(logfile_name,
          "DBFatalException: ('Transaction (Process ID 94) was deadlocked")
        self.assertNotEquals(match,None)

    def test_FDX1_with_routing_1(self):
        """
        Test if FDX1 is loaded and routed properly
        """
        self.prepare('FDX1')
        NAME = 'test_routing_FDX1_1'
        emp_id = _testprep.add_test_employee(self.tdb, NAME)
        area_id = _testprep.add_test_area(self.tdb, NAME, emp_id)
        map_id = _testprep.add_test_map(self.tdb, "FOOFUR", 'TX', 'DALLAS', 'FDX1')
        _testprep.add_test_map_page(self.tdb, map_id, 39)
        # Add map_cell row for A to Z
        for letter in string.ascii_uppercase:
            _testprep.delete_test_map_cell(self.tdb, map_id, 39, letter, 'A', area_id)
            _testprep.add_test_map_cell(self.tdb, map_id, 39, letter, 'A', area_id)

        _testprep.set_client_active(self.tdb, 'FDX1', 'MQ4')

        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,
             "sample_tickets", "FDX1", "FDX1-2007-12-23-00044.txt"))
        raw = tl.tickets[0]
        drop("test_FDX1_with_routing.txt", raw)

        # store the ticket using main.py
        m = main.Main(verbose=0, call_center='FDX1')
        m.log.logger.logfile.truncate(0)
        m.run(runonce=1)

        # assert the ticket is in the database
        id = m.ids[-1]
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_id"], id)

        # add one of the non-client locates as a new client
        try:
            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.clients = self.tdb.get_clients_dict()
            tr.log.logger.logfile.truncate(0)
            found = 0
            for row in tr.clients["FDX1"]:
                if row.client_code == "MQ4":
                    found = 1
            self.assert_(found, "MQ4 not found by router")

            # then run the router
            tr.run(runonce=1)
            # see if the new client has a client_id

            # inspect some locates
            r1 = self.tdb.getrecords("locate", ticket_id=id,
                 client_code="MQ4")[0]
            self.assertEquals(r1["status"], locate_status.assigned)
            self.assert_(r1["client_id"])

            # Verify it is assigned to the right locator
            self.assertEquals(r1['assigned_to'],emp_id)
            # Verify that munic locator was used
            match_str = "locator %s found (BusinessLogic.route_map_area_with_code)" % (emp_id,)
            match, next_log_entry = SearchLogFile(tr.log.logger.logfile.name,match_str)
            self.assertNotEquals(match,None)

        finally:
            # Clean up
            for letter in string.ascii_uppercase:
                _testprep.delete_test_map_cell(self.tdb, map_id, 39, letter, 'A', area_id)
            _testprep.delete_test_map_page(self.tdb, map_id, 39)
            _testprep.delete_test_map(self.tdb, "FOOFUR", 'TX', 'DALLAS', 'FDX1')
            _testprep.delete_test_area(self.tdb, NAME, emp_id)
            _testprep.delete_test_employee(self.tdb, NAME)
            _testprep.clear_database(self.tdb)
            self.tearDown()    # delete just about everything

    def test_FDX1_with_routing_2(self):
        """
        Test if FDX1 is loaded and routed properly via munic routing
        """
        self.prepare('FDX1')
        NAME = 'test_routing_FDX1_1'
        # Set up for mapsco routing in Tarrant county
        emp_id = _testprep.add_test_employee(self.tdb, NAME)
        area_id = _testprep.add_test_area(self.tdb, NAME, emp_id)
        map_id = _testprep.add_test_map(self.tdb, "FOOFUR", 'TX', 'TARRANT', 'FDX1')
        _testprep.add_test_map_page(self.tdb, map_id, 39)
        # Add map_cell row for A to Z
        for letter in string.ascii_uppercase:
            _testprep.delete_test_map_cell(self.tdb, map_id, 39, letter, 'A', area_id)
            _testprep.add_test_map_cell(self.tdb, map_id, 39, letter, 'A', area_id)

        # Set up for munic_area routing in Dallas county
        emp_id_2 = _testprep.add_test_employee(self.tdb, NAME+"_DALLAS")
        area_id_2 = _testprep.add_test_area(self.tdb, NAME+"_DALLAS", emp_id_2)
        county_area_id = _testprep.add_test_county_area(self.tdb, 'DALLAS',
                         area_id_2, state='TX', call_center='FDX1')

        _testprep.set_client_active(self.tdb, 'FDX1', 'MQ4')

        datadir_path = datadir.datadir.path
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,
             "sample_tickets", "FDX1", "FDX1-2007-12-23-00044.txt"))
        raw = tl.tickets[0]
        # Change the town from Mesquite to BAR (to match what is set up in _testprep.add_test_county_area)
        raw = raw.replace("Town: MESQUITE","Town: BAR")
        drop("test_FDX1_with_munic_routing.txt", raw)

        # store the ticket using main.py
        m = main.Main(verbose=0, call_center='FDX1')
        m.log.logger.logfile.truncate(0)
        m.run(runonce=1)

        # assert the ticket is in the database
        id = m.ids[-1]
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_id"], id)

        # add one of the non-client locates as a new client
        try:
            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.clients = self.tdb.get_clients_dict()
            tr.log.logger.logfile.truncate(0)
            found = 0
            for row in tr.clients["FDX1"]:
                if row.client_code == "MQ4":
                    found = 1
            self.assert_(found, "MQ4 not found by router")

            # then run the router
            tr.run(runonce=1)
            # see if the new client has a client_id

            # inspect some locates
            r1 = self.tdb.getrecords("locate", ticket_id=id,
                 client_code="MQ4")[0]
            self.assertEquals(r1["status"], locate_status.assigned)
            self.assert_(r1["client_id"])

            # Verify it is assigned to the right locator
            self.assertEquals(r1['assigned_to'],emp_id_2)
            # Verify that munic locator was used
            match_str = "locator %s found (LocatorCatalog.munic)" % (emp_id_2,)
            match, next_log_entry = SearchLogFile(tr.log.logger.logfile.name,match_str)
            self.assertNotEquals(match,None)

        finally:
            # Clean up
            for letter in string.ascii_uppercase:
                _testprep.delete_test_map_cell(self.tdb, map_id, 39, letter, 'A', area_id)
            _testprep.delete_test_map_page(self.tdb, map_id, 39)
            _testprep.delete_test_map(self.tdb, "FOOFUR", 'TX', 'TARRANT', 'FDX1')
            _testprep.delete_test_area(self.tdb, NAME, emp_id)
            _testprep.delete_test_employee(self.tdb, NAME)
            _testprep.clear_database(self.tdb)
            _testprep.delete_test_county_area(self.tdb, 'DALLAS', area_id_2, state='TX', call_center='FDX1')
            _testprep.delete_test_area(self.tdb, NAME+"_DALLAS", emp_id_2)
            _testprep.delete_test_employee(self.tdb, NAME+"_DALLAS")
            self.tearDown()    # delete just about everything

    def test_FDX1_with_routing_4(self):
        """
        Test if FDX is loaded and routed properly
        """
        self.prepare('FDX1')
        NAME = 'FDX.EgnWp NzBYDq'
        emp_id = _testprep.add_test_employee(self.tdb, NAME)
        map_id = _testprep.add_test_map(self.tdb, "FOOFUR", 'TX', 'FORT BEND', 'FDX1')
        area_id = _testprep.add_test_area(self.tdb, 'QKsWuGUeNOCiHXhgZvj', emp_id, map_id = map_id)
        _testprep.add_test_map_page(self.tdb, map_id, 650)
        # Add map_cell row for A to Z
        _testprep.add_test_map_cell(self.tdb, map_id, 650, 'M', 1, area_id)

        # check if find_map_area_locator finds this locator/area
        rows = self.tdb.find_map_area_locator('FDX1', 650, 'M', 1, county='FORT BEND')
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['area_id'], area_id)
        self.assertEquals(rows[0]['emp_id'], emp_id)

        _testprep.add_test_client(self.tdb, 'MMGIT', 'FDX1')

        datadir_path = datadir.datadir.path
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH, "sample_tickets",
             "DIGGTESS","FDXTEST-2008-09-17-00061.txt"))
        raw = tl.tickets[0]
        raw = raw.replace('Aaron','MMGIT')
        drop("test_FDX_with_routing.txt", raw)

        # store the ticket using main.py
        m = main.Main(verbose=0, call_center='FDX1')
        m.log.logger.logfile = StringIO()
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        # assert the ticket is in the database
        id = m.ids[-1]
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_id"], id)

        # add one of the non-client locates as a new client
        try:
            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.clients = self.tdb.get_clients_dict()
            tr.log.logger.logfile = StringIO()
            found = 0
            for row in tr.clients["FDX1"]:
                if row.client_code == "MMGIT":
                    found = 1
            self.assert_(found, "MMGIT not found by router")

            # then run the router
            tr.run(runonce=1)
            log = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()
            # see if the new client has a client_id

            # inspect some locates
            r1 = self.tdb.getrecords("locate", ticket_id=id,
                 client_code="MMGIT")[0]
            self.assertEquals(r1["status"], locate_status.assigned)
            self.assert_(r1["client_id"])

            # Verify it is assigned to the right locator
            self.assertEquals(r1['assigned_to'],emp_id)
            # Verify that munic locator was used
            match_str = "locator %s found (BusinessLogic.route_map_area_with_code)" % (emp_id,)
            self.assertTrue(match_str in log)

        finally:
            # Clean up
            for letter in string.ascii_uppercase:
                _testprep.delete_test_map_cell(self.tdb, map_id, 650, letter, 'M', area_id)
            _testprep.delete_test_map_page(self.tdb, map_id, 650)
            _testprep.delete_test_map(self.tdb, "FOOFUR", 'TX', 'FORT BEND', 'FDX1')
            _testprep.delete_test_area(self.tdb, NAME, emp_id)
            _testprep.delete_test_employee(self.tdb, NAME)
            _testprep.delete_test_client(self.tdb, 'MMGIT')
            _testprep.clear_database(self.tdb)
            self.tearDown()    # delete just about everything

    def test_update_fcv3_1(self):
        """
        Test fcv3 ticket updates.
        Drop normal ticket, drop a 3 hr update, respond to 3hr update
        """
        self.prepare('FCV3')
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,
             "sample_tickets","fcv3","FCV3-2007-09-30-00025.txt"))
        raw = tl.getTicket()
        drop("FCV3_1.txt", raw)
        m = main.Main(verbose=0, call_center='FCV3')
        m.log.logger.logfile = StringIO()
        m.run(runonce=1)
        id = m.ids[-1]

        configuration = config.getConfiguration()
        configuration.responders['FCV3']['send_3hour'] = 0
        configuration.responders['FCV3']['send_emergencies'] = 0

        from test_irthresponder import MockIRTHResponder

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.only_call_center = 'FCV3' # ignore others
        ir.responses = ["250 OK"]

        # Close ticket out
        locates = self.tdb.getrecords("locate", ticket_id=id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Post an update
        raw = raw.replace("Transmit        Date: 09/30/07   Time: 02:48 PM",
                          "Transmit        Date: 09/30/07   Time: 04:48 PM")
        # Make it a 3hr rush
        raw = raw.replace('NEW  GRID NORM LREQ','3HRS GRID RUSH LREQ FTTP')
        drop("FCV3_2.txt", raw)

        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        id = m.ids[-1]
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_id"], id)

        # Close ticket out
        locates = self.tdb.getrecords("locate", ticket_id=id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond to ticket
        ir.respond_all()

        self.assertEquals(len(ir.sent), 1) # Should be queued
        t = self.tdb.getticket(id)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Sending Response: %s" % (t.ticket_number))
        self.assertNotEqual(match,None)

    def test_update_fcv3_2(self):
        """
        Test fcv3 ticket updates.
        Drop 3 hr ticket, drop another 3 hr update, respond to neither
        """
        self.prepare('FCV3')
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,
             "sample_tickets", "fcv3", "FCV3-2007-09-30-00025.txt"))
        raw = tl.getTicket()
        # Make it a 3hr rush
        raw = raw.replace('NEW  GRID NORM LREQ','3HRS GRID RUSH LREQ FTTP')
        drop("FCV3_1.txt", raw)
        m = main.Main(verbose=0, call_center='FCV3')
        m.run(runonce=1)
        id = m.ids[-1]

        configuration = config.getConfiguration()
        configuration.responders['FCV3']['send_3hour'] = 0
        configuration.responders['FCV3']['send_emergencies'] = 0

        from test_irthresponder import MockIRTHResponder

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.only_call_center = 'FCV3' # ignore others
        ir.responses = ["250 OK"]

        # Close ticket out
        locates = self.tdb.getrecords("locate", ticket_id=id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Post an update
        raw = raw.replace("Transmit        Date: 09/30/07   Time: 02:48 PM",
                          "Transmit        Date: 09/30/07   Time: 04:48 PM")
        # Make it a 3hr rush
        raw = raw.replace('NEW  GRID NORM LREQ','3HRS GRID RUSH LREQ FTTP')
        drop("FCV3_2.txt", raw)

        m.run(runonce=1)

        id = m.ids[-1]
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_id"], id)

        # Close ticket out
        locates = self.tdb.getrecords("locate", ticket_id=id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6)
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond to ticket
        ir.respond_all()

        self.assertEquals(len(ir.sent), 0) # Should not be queued
        t = self.tdb.getticket(id)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,
          "Skipping 3 hour ticket %s" % (t.ticket_number))
        self.assertNotEqual(match, None)

    def test_update_fcv3_3(self):
        """
        Test fcv3 ticket updates.
        Drop ticket, mark it as ongoing, drop 3 hr update, respond
        """
        self.prepare('FCV3')
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,
             "sample_tickets", "fcv3", "FCV3-2007-09-30-00025.txt"))
        raw = tl.getTicket()
        drop("FCV3_1.txt", raw)
        m = main.Main(verbose=0, call_center='FCV3')
        m.run(runonce=1)
        id = m.ids[-1]

        configuration = config.getConfiguration()
        configuration.responders['FCV3']['send_3hour'] = 0
        configuration.responders['FCV3']['send_emergencies'] = 0

        from test_irthresponder import MockIRTHResponder

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.only_call_center = 'FCV3' # ignore others
        ir.responses = ["425 OK"] # reprocess

        # Mark as ongoing
        locates = self.tdb.getrecords("locate", ticket_id=id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('O', locate_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond
        ir.respond_all()
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Ignored for now")
        self.assertNotEqual(match,None)

        # Post an update
        raw = raw.replace("Transmit        Date: 09/30/07   Time: 02:48 PM",
                          "Transmit        Date: 09/30/07   Time: 04:48 PM")
        # Make it a 3hr rush
        raw = raw.replace('NEW  GRID NORM LREQ','3HRS GRID RUSH LREQ FTTP')
        drop("FCV3_2.txt", raw)

        m.run(runonce=1)

        id = m.ids[-1]
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_id"], id)

        # Close ticket out
        locates = self.tdb.getrecords("locate", ticket_id=id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond to ticket
        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.only_call_center = 'FCV3' # ignore others
        ir.responses = ["250 OK"]
        ir.respond_all()

        self.assertEquals(len(ir.sent), 1) # Should be queued
        t = self.tdb.getticket(id)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Sending Response: %s" % (t.ticket_number))
        self.assertNotEqual(match,None)

    def test_update_fcv3_4(self):
        """
        Test fcv3 ticket updates.
        Drop normal ticket, mark locate, respond, drop a 3 hr update, do not respond to 3hr update
        """
        self.prepare('FCV3')
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","fcv3","FCV3-2007-09-30-00025.txt"))
        raw = tl.getTicket()
        drop("FCV3_1.txt", raw)
        m = main.Main(verbose=0, call_center='FCV3')
        m.run(runonce=1)
        id = m.ids[-1]

        configuration = config.getConfiguration()
        configuration.responders['FCV3']['send_3hour'] = 0
        configuration.responders['FCV3']['send_emergencies'] = 0

        from test_irthresponder import MockIRTHResponder

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.only_call_center = 'FCV3' # ignore others
        ir.responses = ["425 OK","250 OK"] # reprocess, done

        # Mark ticket as ongoing
        locates = self.tdb.getrecords("locate", ticket_id=id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('O', locate_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond to ticket
        ir.respond_all()

        self.assertEquals(len(ir.sent), 1)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Ignored for now")
        self.assertNotEqual(match,None)

        # Post an update
        raw = raw.replace("Transmit        Date: 09/30/07   Time: 02:48 PM",
                          "Transmit        Date: 09/30/07   Time: 04:48 PM")
        # Make it a 3hr rush
        raw = raw.replace('NEW  GRID NORM LREQ','3HRS GRID RUSH LREQ FTTP')
        drop("FCV3_2.txt", raw)

        m.run(runonce=1)

        id = m.ids[-1]
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_id"], id)

        # Mark locate
        locates = self.tdb.getrecords("locate", ticket_id=id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond to ticket
        ir.respond_all()

        self.assertEquals(len(ir.sent), 2) # Should be queued
        t = self.tdb.getticket(id)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Sending Response: %s" % (t.ticket_number))
        self.assertNotEqual(match,None)

    def test_update_occ2_1(self):
        """
        Test occ2/occ3 ticket updates.
        Drop tickets, verify responder behavior
        """
        configuration = config.getConfiguration()
        configuration.responders['OCC2']['send_3hour'] = 0
        configuration.responders['OCC2']['send_emergencies'] = 0

        from test_irthresponder import MockIRTHResponder

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        #ir.only_call_center = 'FCV3' # ignore others
        ir.responses = ["250 OK","250 OK"] # done, done

        m = main.Main(verbose=0, call_center='OCC2')

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ2","ticket_1.txt"))
        raw = tl.getTicket()
        drop("OCC3_1.txt", raw)

        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ2","ticket_2.txt"))
        raw = tl.getTicket()
        drop("OCC2_1.txt", raw)

        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ2","ticket_3.txt"))
        raw = tl.getTicket()
        drop("OCC3_2.txt", raw)

        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ2","ticket_4.txt"))
        raw = tl.getTicket()
        drop("OCC2_2.txt", raw)

        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ2","ticket_5.txt"))
        raw = tl.getTicket()
        drop("OCC3_3.txt", raw)

        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ2","ticket_6.txt"))
        raw = tl.getTicket()
        drop("OCC2_3.txt", raw)

        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ2","ticket_7.txt"))
        raw = tl.getTicket()
        drop("OCC3_4.txt", raw)

        m.run(runonce=1)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ2","ticket_8.txt"))
        raw = tl.getTicket()
        drop("OCC3_5.txt", raw)

        m.run(runonce=1)

        ticket_id = m.ids[-1]

        # Mark locates
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="DOM400")
        locate1_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate1_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate1_id,)
        self.tdb.runsql(sql)

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, 
                  client_code="VZN906")
        locate2_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate2_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate2_id,)
        self.tdb.runsql(sql)

        # Respond
        ir.respond_all()

        self.assertEquals(len(ir.sent), 2)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Removing locate from queue: %s" % (locate1_id,))
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Removing locate from queue: %s" % (locate2_id,))
        self.assertNotEqual(match,None)

    # deprecated; OCC3 is no longer in use
    def __test_OCC3_3hr_1(self):
        """
        Test OCC3 response to a 3 hr ticket with send_3hr = 0
        """
        self.prepare('OCC3')
        # Set the configuration?

        configuration = config.getConfiguration()
        # Set everything to occ2
        configuration = config.getConfiguration()
        configuration.responders['OCC2']['send_3hour'] = 0
        configuration.responders['OCC2']['send_emergencies'] = 0

        from test_irthresponder import MockIRTHResponder

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.responses = ["250 OK","250 OK"] # done, done

        m = main.Main(verbose=0, call_center='OCC3')

        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,
             "sample_tickets", "occ3", "occ3-2008-04-07.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("occ3_1.txt", raw)

        m.run(runonce=1)

        ticket_id = m.ids[-1]

        # Mark locates
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="WGL904")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        # Decrement the time by 6 minutes
        date_time = date.Date()
        date_time.dec_time(minutes=6)
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond
        ir.respond_all()
        # No responses should be sent since send_3hour is 0
        self.assertEquals(len(ir.sent), 0)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Skipping 3 hour ticket B808801291")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"    Reason:")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"    responder send_3hour = 0")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"    respond_to_original is False")
        self.assertNotEqual(match,None)

    def test_OCC3_3hr_2(self):
        """
        Test OCC3 response to a 3 hr ticket with send_3hr = 1
        """
        # Set the configuration?
        self.prepare('OCC3')

        configuration = config.getConfiguration()
        # Set everything to occ2
        configuration = config.getConfiguration()
        configuration.responders['OCC2']['send_3hour'] = 1
        configuration.responders['OCC2']['send_emergencies'] = 0

        from test_irthresponder import MockIRTHResponder

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.responses = ["250 OK","250 OK"] # done, done

        m = main.Main(verbose=0, call_center='OCC3')

        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,"sample_tickets","occ3","occ3-2008-04-07.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("occ3_1.txt", raw)

        m.run(runonce=1)

        ticket_id = m.ids[-1]

        # Mark locates
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="WGL904")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        # Decrement the time by 6 minutes
        date_time = date.Date()
        date_time.dec_time(minutes=6)
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond
        ir.respond_all()
        # No responses should be sent since send_emergencies is 0
        self.assertEquals(len(ir.sent), 0)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Skipping emergency ticket B808801291")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"    Reason:")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"    responder send_emergencies = 0")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"    respond_to_original is False")
        self.assertNotEqual(match,None)

    def test_OCC3_3hr_3(self):
        """
        Test OCC3 response to a 3 hr ticket with send_3hr = 0 and send_emergencies = 1
        """
        # Set the configuration?
        self.prepare('OCC3')

        configuration = config.getConfiguration()
        # Set everything to occ2
        configuration = config.getConfiguration()
        configuration.responders['OCC2']['send_3hour'] = 0
        configuration.responders['OCC2']['send_emergencies'] = 1

        from test_irthresponder import MockIRTHResponder

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.responses = ["250 OK","250 OK"] # done, done

        m = main.Main(verbose=0, call_center='OCC3')

        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,"sample_tickets","occ3","occ3-2008-04-07.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("occ3_1.txt", raw)

        m.run(runonce=1)

        ticket_id = m.ids[-1]

        # Mark locates
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="WGL904")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        # Decrement the time by 6 minutes
        date_time = date.Date()
        date_time.dec_time(minutes=6)
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond
        ir.respond_all()
        # No responses should be sent since send_emergencies is 0
        self.assertEquals(len(ir.sent), 0)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Skipping 3 hour ticket B808801291")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"    Reason:")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"    responder send_3hour = 0")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"    respond_to_original is False")
        self.assertNotEqual(match,None)

    def test_OCC3_3hr_4(self):
        """
        Test OCC3 response to a 3 hr ticket with send_3hr = 1 and send_emergencies = 1
        """
        self.prepare('OCC2', 'OCC3')

        configuration = config.getConfiguration()
        # Set everything to occ2
        configuration = config.getConfiguration()
        configuration.responders['OCC2']['send_3hour'] = 1
        configuration.responders['OCC2']['send_emergencies'] = 1

        from test_irthresponder import MockIRTHResponder

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.responses = ["250 OK","250 OK"] # done, done

        m = main.Main(verbose=0, call_center='OCC3')

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ3","occ3-2008-04-07.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("occ3_1.txt", raw)

        m.run(runonce=1)

        ticket_id = m.ids[-1]

        # Mark locates
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="WGL904")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        # Decrement the time by 6 minutes
        date_time = date.Date()
        date_time.dec_time(minutes=6)
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond
        ir.respond_all()
        # A response should be sent since send_emergencies is 1 and send_3hour is 1
        self.assertEquals(len(ir.sent), 1)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Responding for: OCC2/util13")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Sending Response: B808801291, WGL904, 10, ,")
        self.assertNotEqual(match,None)

    # deprecated; OCC3 is no longer in use.
    def __test_OCC3_3hr_5(self):
        """
        Test OCC2/OCC3 response to a series of tickets.
        Mantis 2029: 3 HOUR ticket rule to respond for 3 hour tickets Issue
        """
        # Set the configuration?

        configuration = config.getConfiguration()
        # Set everything to occ2
        configuration = config.getConfiguration()
        configuration.responders['OCC2']['send_3hour'] = 0
        configuration.responders['OCC2']['send_emergencies'] = 0

        from test_irthresponder import MockIRTHResponder

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.responses = ["250 OK"]

        m = main.Main(verbose=1, call_center='OCC2')
        m.log.logger.logfile.truncate(0)

        # Original ticket
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,
             "sample_tickets", "occ2", "occ2-2008-04-21-01112.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("occ1_1.txt", raw)

        m.run(runonce=1)
        ticket_id = m.ids[-1]
        t = self.tdb.getticket(ticket_id)
        m.log.logger.logfile.truncate(0)

        # Update call center
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,
             "sample_tickets", "occ2", "OCC3-2008-04-21-01805.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("occ3_1.txt", raw)

        m.run(runonce=1)
        ticket_id = m.ids[-1]
        t = self.tdb.getticket(ticket_id)

        # Verify ticket updated
        versions = self.tdb.getrecords("ticket_version", ticket_id=ticket_id)
        self.assertEquals(len(versions), 2)
        match, next_log_entry = SearchLogFile(m.log.logger.logfile.name,
                                "Updating OCC3 to OCC2")
        self.assertNotEqual(match, None)
        match, next_log_entry = SearchLogFile(m.log.logger.logfile.name,
                                "Processing ticket B811202153 (updated)")
        self.assertNotEqual(match, None)
        m.log.logger.logfile.truncate(0)

        # Update call center, duplicate
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,"sample_tickets","occ2","OCC3-2008-04-21-01817.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("occ3_1.txt", raw)

        m.run(runonce=1)
        ticket_id = m.ids[-1]
        t = self.tdb.getticket(ticket_id)

        # Verify ticket updated
        versions = self.tdb.getrecords("ticket_version", ticket_id=ticket_id)
        self.assertEquals(len(versions),3)
        match, next_log_entry = SearchLogFile(m.log.logger.logfile.name,"Updating OCC3 to OCC2")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(m.log.logger.logfile.name,"Processing ticket B811202153 (updated)")
        self.assertNotEqual(match,None)
        m.log.logger.logfile.truncate(0)

        # Set to ongoing
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="WGL904")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('O', locate_id,)
        self.tdb.runsql(sql)

        # Decrement the time by 6 minutes
        date_time = date.Date()
        date_time.dec_time(minutes=6)
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Respond
        ir.respond_all()
        # A response should be sent for the original ticket since there are no previous responses
        self.assertEquals(len(ir.sent), 1)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Responding for: OCC2/util13")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Sending Response: B811202153, WGL904, 60, ,")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Response: 250 OK")
        self.assertNotEqual(match,None)

        # 3 hr ticket
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,"sample_tickets","occ2","occ2-2008-04-25-00530.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("occ1_1.txt", raw)

        m.run(runonce=1)
        ticket_id = m.ids[-1]
        t = self.tdb.getticket(ticket_id)

        # Update call center
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,"sample_tickets","occ2","OCC3-2008-04-25-00815.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("occ3_1.txt", raw)

        m.run(runonce=1)
        ticket_id = m.ids[-1]
        t = self.tdb.getticket(ticket_id)

        # Update call center, duplicate
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,"sample_tickets","occ2","OCC3-2008-04-25-00822.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("occ3_1.txt", raw)

        m.run(runonce=1)
        ticket_id = m.ids[-1]
        t = self.tdb.getticket(ticket_id)

        # Set to marked
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="WGL904")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        # Decrement the time by 6 minutes
        date_time = date.Date()
        date_time.dec_time(minutes=6)
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        # Set to re-process
        ir.responses = ["425",]

        # Respond
        ir.respond_all()
        # A response should be sent for the original ticket since the previous response is marked 'O'
        self.assertEquals(len(ir.sent), 1)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Responding for: OCC2/util13")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Sending Response: B811202153, WGL904, 10, ,")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Response: 425")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"...Ignored for now")
        self.assertNotEqual(match,None)

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        # Set to re-process
        ir.responses = ["250 OK",]
        # Respond
        ir.respond_all()

        # Since the last attempt at a response resulted in a try again, another response should be sent
        self.assertEquals(len(ir.sent), 1)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Responding for: OCC2/util13")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Sending Response: B811202153, WGL904, 10, ,")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Response: 250 OK")
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Removing locate from queue: %s" % (locate_id,))
        self.assertNotEqual(match,None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"1 responses sent")
        self.assertNotEqual(match,None)

    def test_FMW2_1(self):
        """
        Test FMW2 response.
        Mantis 2035: Responder problem with FMW1/FMW3 out of VA
        """
        self.prepare('FMW1', 'FMW2', 'FMW3')

        # who formatted this code... grrr >=(
        configuration = config.getConfiguration()
        configuration.responders['FMW1'] = {}
        configuration.responders['FMW1']['name'] = 'FMW1'
        configuration.responders['FMW1']['server'] = '127.0.0.1'
        configuration.responders['FMW1']['port'] = '1234'
        configuration.responders['FMW1']['logins'] = [
          {'login': 'user', 'password': 'secret'}]
        configuration.responders['FMW1']['mappings'] = {"M":("10",''),
                                                        "ZM":("11",''),
                                                        "XM":("12",''),
                                                        "ZPM":("13",''),
                                                        "N":("30",''),
                                                        "ZN":("31",''),
                                                        "ZC":("32",''),
                                                        "ZPC":("33",''),
                                                        "XC":("40",''),
                                                        "XN":("42",''),
                                                        "XQ":("50",''),
                                                        "OH":("60","CONTACT"),
                                                        "O":("60","CONTACT"),
                                                        "XL":("61","CONTACT"),
                                                        "ZL":("61","CONTACT"),
                                                        "XD":("70",''),
                                                        "XDU":("71",''),
                                                        "XA":("90","CONTACT"),
                                                        "XB":("91",''),
                                                        "XF":("93",''),
                                                        "XI":("94",''),
                                                        "NL":("96",''),
                                                        "NC":("96",''),
                                                        "S":("96",''),
                                                        "XJ":("97",'')}
        configuration.responders['FMW1']['translations'] = {'PEP07':'PEP904',}
        configuration.responders['FMW1']['clientbased'] = 0
        configuration.responders['FMW1']['skip'] = []
        configuration.responders['FMW1']['all_versions'] = 0
        configuration.responders['FMW1']['clients'] = []
        configuration.responders['FMW1']['send_emergencies'] = 1
        configuration.responders['FMW1']['ack_dir'] = ''
        configuration.responders['FMW1']['ack_proc_dir'] = ''
        configuration.responders['FMW1']['send_3hour'] = 1
        configuration.responderconfigdata.data.append(('FMW1','irth',configuration.responders['FMW1']))

        configuration.responders['FMW3'] = {}
        configuration.responders['FMW3']['name'] = 'FMW3'
        configuration.responders['FMW3']['server'] = '127.0.0.1'
        configuration.responders['FMW3']['port'] = '1234'
        configuration.responders['FMW3']['logins'] = [{'login': 'user', 'password': 'secret'}]
        configuration.responders['FMW3']['mappings'] = {"M":("10",''),
                                                        "ZM":("11",''),
                                                        "XM":("12",''),
                                                        "ZPM":("13",''),
                                                        "N":("30",''),
                                                        "ZN":("31",''),
                                                        "ZC":("32",''),
                                                        "ZPC":("33",''),
                                                        "XC":("40",''),
                                                        "XN":("42",''),
                                                        "XQ":("50",''),
                                                        "OH":("60","CONTACT"),
                                                        "O":("60","CONTACT"),
                                                        "XL":("61","CONTACT"),
                                                        "ZL":("61","CONTACT"),
                                                        "XD":("70",''),
                                                        "XDU":("71",''),
                                                        "XA":("90","CONTACT"),
                                                        "XB":("91",''),
                                                        "XF":("93",''),
                                                        "XI":("94",''),
                                                        "NL":("96",''),
                                                        "NC":("96",''),
                                                        "S":("96",''),
                                                        "XJ":("97",'')}
        configuration.responders['FMW3']['translations'] = {'PEP07':'PEP904',}
        configuration.responders['FMW3']['clientbased'] = 0
        configuration.responders['FMW3']['skip'] = []
        configuration.responders['FMW3']['all_versions'] = 0
        configuration.responders['FMW3']['clients'] = []
        configuration.responders['FMW3']['send_emergencies'] = 1
        configuration.responders['FMW3']['ack_dir'] = ''
        configuration.responders['FMW3']['ack_proc_dir'] = ''
        configuration.responders['FMW3']['send_3hour'] = 1
        configuration.responderconfigdata.data.append(('FMW3', 'irth',
          configuration.responders['FMW3']))

        from test_irthresponder import MockIRTHResponder

        # make sure FMW2 has active clients, otherwise it won't be recognized
        # as update call center
        self.tdb.runsql("""
          update client
          set active = 1
          where update_call_center = 'FMW2'
        """)
        # ...and we need at least one client with FMW1/FMW2
        _testprep.add_test_client(self.tdb, 'BOGUS', 'FMW1',
         update_call_center='FMW2')
        cc = call_centers.get_call_centers(self.tdb)
        cc.reload()

        # due to messing around with the clients, and different ref data, we
        # have to make sure the PEP904 we want (FMW2/FMW2) is active, and any
        # others are inactive.
        self.tdb.runsql("""
          update client
          set active = 0
          where oc_code = 'PEP904'
          ;
          update client
          set active = 1
          where oc_code = 'PEP904' and call_center = 'FMW2'
        """)

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.responses = ["250 OK"]

        m = main.Main(verbose=0, call_center='FMW2')
        m.log.logger.logfile.truncate(0)

        # Original ticket
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets", "FMW2", "fmw2-2008-05-07-00059.txt"))
        raw = tl.getTicket()
        # Drop the ticket
        drop("fmw2_1.txt", raw)

        m.run(runonce=1)
        ticket_id = m.ids[-1]
        t = self.tdb.getticket(ticket_id)

        # Set to marked
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id,
                  client_code="PEP904")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id)
        self.tdb.runsql(sql)

        # Decrement the time by 6 minutes
        date_time = date.Date()
        date_time.dec_time(minutes=6)
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id)
        self.tdb.runsql(sql)

        # Respond
        ir.respond_all()
        # Since the last attempt at a response resulted in a try again, another
        # response should be sent
        self.assertEquals(len(ir.sent), 1)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Responding for: FMW1")
        self.assertNotEqual(match, None)
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Sending Response: B812800045, PEP904, 10, ,")
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"1 responses sent")
        self.assertNotEqual(match,None)

    def test_occ2_meet(self):
        self.prepare('OCC2')
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets", "occ2", "occ2-2008-05-23-00295.txt"))
        raw = tl.getTicket()

        drop("OCC2MEET.txt", raw)

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='OCC2')
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t = self.tdb.getticket(id)

        # Test the due dates (new rule as of Mantis #2739)
        self.assertEquals(t.due_date,'2008-05-29 07:00:00')
        self.assertEquals(t.legal_due_date,'2008-05-29 07:00:00')
        # Test the ticket kind
        self.assertNotEqual(t.ticket_type.find('MEET'),-1)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

    def test_occ2_meet_2(self):
        '''
        Test occ2 meet on a ticket with many versions
        '''
        self.prepare('OCC2', 'OCC3')
        tickets = ['OCC3-2008-07-14-00990.txt',
                   'occ2-2008-07-14-00634.txt',
                   'OCC3-2008-07-14-01004.txt',
                   'occ2-2008-07-17-00036.txt',
                   'OCC3-2008-07-17-03320.txt',
                   'OCC3-2008-07-17-03809.txt',
                   'OCC3-2008-07-17-04166.txt',
                   'OCC3-2008-07-17-04383.txt',
                   'OCC3-2008-07-17-04609.txt',
                   'OCC3-2008-07-17-04840.txt',
                   'OCC3-2008-07-17-05143.txt',
                   'OCC3-2008-07-17-05457.txt',
                   'OCC3-2008-07-17-05817.txt',
                   'OCC3-2008-07-17-06193.txt',
                   'OCC3-2008-07-17-06655.txt',
                   'OCC3-2008-07-17-07041.txt',
                   'OCC3-2008-07-17-07435.txt',
                   'OCC3-2008-07-17-07845.txt',
                   'OCC3-2008-07-17-08745.txt',
                   'OCC3-2008-07-17-09563.txt',
                   'OCC3-2008-07-17-10342.txt',
                   'OCC3-2008-07-17-11176.txt',
                   'OCC3-2008-07-18-01152.txt',
                   'OCC3-2008-07-18-02458.txt']
        for ticket in tickets:
            tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
                 "sample_tickets", "occ2", ticket))
            raw = tl.getTicket()

            drop("OCCMEET.txt", raw)

            # now let main.py run and see what happens...
            m = main.Main(verbose=0, call_center=ticket[:4].upper())
            #if ticket[0:4].lower() == 'occ2':
            #else:
            m.run(runonce=1)

            # what's the id of the ticket we just posted?
            id = m.ids[-1]

            # Get the ticket
            t = self.tdb.getticket(id)

            # Test the due dates (rules changed as of Mantis #2739)
            possible_due_dates = ['2008-07-16 10:00:00', '2008-07-17 07:00:00']
            self.assertTrue(t.due_date in possible_due_dates)
            #self.assertEquals(t.due_date,'2008-07-17 07:00:00')
            #self.assertEquals(t.legal_due_date,'2008-07-17 07:00:00')
            # Test the ticket kind
            #self.assertNotEqual(t.ticket_type.find('MEET'), -1)
            self.assertTrue('MEET' in t.ticket_type)

    def test_fbl2(self):
        '''
        Test fbl2 update. Tests a subtle bug in updateticket_sg in which the original ticket values are set
        to the new ticket's value, even if the new ticket value is null.
        '''
        self.prepare('FBL2')
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FBL2","FBL2-2008-06-05-00004.txt"))
        raw = tl.getTicket()

        drop("FBL2_1.txt", raw)

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='FBL2')
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id_1 = m.ids[-1]

        # Get the ticket
        t_1 = self.tdb.getticket(id_1)

        # Drop an update
        t2 = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FBL2","FBL2-2008-06-05-00005.txt"))
        raw = t2.getTicket()

        drop("FBL2_1.txt", raw)

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='FBL2')
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id_2 = m.ids[-1]

        # Get the ticket
        t_2 = self.tdb.getticket(id_2)

        self.assertEquals(t_1.legal_due_date,t_2.legal_due_date)

    def test_sca6_LongTicket(self):
        '''
        Test SCA6 long ticket
        '''
        self.prepare('SCA6')
        tl = ticketloader.TicketLoader(os.path.join("../testdata","sample_tickets","SCA6","sca6-2008-06-17-00933.txt"))
        raw = tl.getTicket()
        t1 = self.handler.parse(raw, ["SouthCaliforniaATT"])

        drop("SCA6_LongTicket.txt", raw)

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='SCA6')
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(len(t1.image),len(t2.image))

    def test_3004_1(self):
        """ Drop and parse 3004 emergency ticket """
        self.prepare('3004')
        # create a ticket file
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCT4","TEST-2008-09-23-00028_EMERG.txt"))
        raw = tl.getTicket()
        drop("test1.txt", raw)

        m = main.Main(verbose=0, call_center='3004')
        m.log.lock = 1
        m.run(runonce=1)
        id = m.ids[0]   # id of ticket that was just posted

        # get ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(t2.call_date,      '2008-08-19 16:27:00')
        self.assertEquals(t2.legal_due_date, '2008-08-19 18:27:00')

    def test_with_profiling(self):
        """
        Test with profiling
        """
        self.prepare('Atlanta')

        from show_profile_report import show_profile_report
        # create a ticket file
        tl = ticketloader.TicketLoader("../testdata/atlanta-1.txt")
        raw = tl.getTicket()
        drop("test1.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        #m.log.lock = 1
        m.run(runonce=1)
        prefix = "main_profile_g_"
        show_profile_report(prefix, m.call_center, m.tdb, verbose=0)
        d = date.Date().isodate()
        d = d.replace("-", "").replace(":", "").replace(" ", "_")
        basename = prefix + m.call_center + "_" + d
        self.assertTrue(os.path.exists(basename + ".txt"))
        self.assertTrue(os.path.exists(basename + "_csv.txt"))
        os.remove(basename + ".txt")
        os.remove(basename + "_csv.txt")

    def test_SCA1_followup_1(self):
        '''
        Mantis 2254: problems creating project tickets in CA
        '''
        self.prepare('SCA1')
        # pretend we don't have the do_not_route_until field
        fields = self.tdb.tabledefs['ticket']
        fields = [(k,v) for (k,v) in fields if k != 'do_not_route_until']
        self.tdb.tabledefs._cache['ticket'] = fields

        try:
            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA1","sca1-2009-02-18-00632.txt"))
            raw = tl.getTicket()

            drop("sca.txt", raw)

            m = main.Main(verbose=0, call_center='SCA1')
            m.log.lock = 1
            m.run(runonce=1)
            # Should have one ticket
            rows = self.tdb.getrecords("ticket")
            self.assertEquals(len(rows), 1)
            first_ticket = rows[0]
            locates_from_first = self.tdb.getrecords("locate", ticket_id=first_ticket['ticket_id'])

            # Set high profile for one of the locates
            self.tdb.update('locate','locate_id',locates_from_first[5]['locate_id'],high_profile=1)

            # let the router run
            tr = ticketrouter.TicketRouter(1, verbose=0)
            tr.log.logger.logfile = StringIO()
            tr.run(runonce=1)
            log = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()


            locates_from_first = self.tdb.getrecords("locate", ticket_id=first_ticket['ticket_id'])
            sql = "exec dbo.tickets_to_route_noxml"
            rows = self.tdb.runsql_result(sql)

            self.assertEquals(len(locates_from_first),9)
            # Should be nothing left to route
            self.assertEquals(len(rows),0)

            # All locates should have been processed
            [self.assertNotEquals(locate['status'],'-P') for locate in locates_from_first]

            for locate in locates_from_first:
                assigned_to = locate['assigned_to_id']
                if assigned_to is not None:
                    # Close all of the locates assigned
                    self.tdb.update('locate','locate_id',locate['locate_id'],closed=1)

            self.assertEquals(self.tdb.isclosed(first_ticket['ticket_id']), 1)

            self.tdb.runsql("exec create_followup_ticket %s,17007" % (m.ids[0],))
            # Should have two tickets
            rows = self.tdb.getrecords("ticket")
            self.assertEquals(len(rows), 2)

            rows = self.tdb.getrecords("ticket",ticket_type='PROJECT TICKET')
            self.assertEquals(len(rows), 1)
            ticket_copy = rows[0]

            locates_from_copy = self.tdb.getrecords("locate", ticket_id=ticket_copy['ticket_id'])
            # All locates from the copy should be un-assigned since the -N locates are not copied
            [self.assertEquals(loc['status'],locate_status.blank) for loc in locates_from_copy]

            # let the router run
            tr = ticketrouter.TicketRouter(1, verbose=0)
            tr.run(runonce=1)

            locates_from_first = self.tdb.getrecords("locate", ticket_id=first_ticket['ticket_id'])
            self.assertEquals(len(locates_from_first),9)

            locates_from_copy = self.tdb.getrecords("locate", ticket_id=ticket_copy['ticket_id'])
            self.assertEquals(len(locates_from_copy),4)
            # All locates from the copy should be assigned since the -N locates are not copied
            [self.assertEquals(loc['status'],locate_status.assigned) for loc in locates_from_copy]
            self.assertEquals(locates_from_first[5]['status'],locate_status.assigned)
        finally:
            self.tdb.tabledefs.reset()

    def test_SCA1_followup_2(self):
        '''
        Create a copy of the copy
        Mantis 2254: problems creating project tickets in CA
        '''
        self.prepare('SCA1')
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets", "SCA1", "sca1-2009-03-19-00976.txt"))
        raw = tl.getTicket()

        drop("sca.txt", raw)

        m = main.Main(verbose=0, call_center='SCA1')
        m.log.lock = 1
        m.run(runonce=1)
        # Should have one ticket
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        first_ticket = rows[0]
        locates_from_first = self.tdb.getrecords("locate", ticket_id=first_ticket['ticket_id'])

        # Set high profile for one of the locates
        self.tdb.update('locate','locate_id',locates_from_first[3]['locate_id'],high_profile=1)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        locates_from_first = self.tdb.getrecords("locate", ticket_id=first_ticket['ticket_id'])
        for locate in locates_from_first:
            assigned_to = locate['assigned_to_id']
            if assigned_to is not None:
                # Close all of the locates assigned
                self.tdb.update('locate','locate_id',locate['locate_id'],closed=1)

        locates_from_first = self.tdb.getrecords("locate", ticket_id=first_ticket['ticket_id'])

        self.assertEquals(self.tdb.isclosed(first_ticket['ticket_id']), True)

        self.tdb.runsql("exec create_followup_ticket %s,17007" % (m.ids[0],))
        # Should have two tickets
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 2)

        rows = self.tdb.getrecords("ticket",ticket_type='PROJECT TICKET')
        self.assertEquals(len(rows), 1)
        ticket_copy = rows[0]

        locates_from_copy = self.tdb.getrecords("locate", ticket_id=ticket_copy['ticket_id'])
        # All locates from the copy should be un-assigned since the -N locates are not copied
        [self.assertEquals(loc['status'],locate_status.blank) for loc in locates_from_copy]

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        locates_from_first = self.tdb.getrecords("locate", ticket_id=first_ticket['ticket_id'])
        self.assertEquals(len(locates_from_first),4)

        locates_from_copy = self.tdb.getrecords("locate", ticket_id=ticket_copy['ticket_id'])
        self.assertEquals(len(locates_from_copy),2)
        # All locates from the copy should be assigned since the -N locates are not copied
        [self.assertEquals(loc['status'],locate_status.assigned) for loc in locates_from_copy]
        [self.assertEquals(loc['status'],locate_status.assigned) for loc in locates_from_first[2:]]

        # Copy the copy
        self.tdb.runsql("exec create_followup_ticket %s,17007" % (ticket_copy['ticket_id'],))
        # Should have three tickets
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 3)

        rows = self.tdb.getrecords("ticket",ticket_type='PROJECT TICKET')
        # Should be two
        self.assertEquals(len(rows), 2)
        ticket_copy_of_copy = rows[1]

        locates_from_copy_of_copy = self.tdb.getrecords("locate", ticket_id=ticket_copy_of_copy['ticket_id'])
        [self.assertEquals(loc['status'],locate_status.blank) for loc in locates_from_copy_of_copy]
        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        locates_from_copy_of_copy = self.tdb.getrecords("locate", ticket_id=ticket_copy_of_copy['ticket_id'])
        self.assertEquals(len(locates_from_copy_of_copy),2)
        # All locates from the copy should be assigned
        [self.assertEquals(loc['status'],locate_status.assigned) for loc in locates_from_copy_of_copy]

    def test_SCA1_followup_3(self):
        '''
        Mantis 2254: problems creating project tickets in CA
        '''
        self.prepare('SCA1')
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA1","sca1-2009-02-18-00632.txt"))
        raw = tl.getTicket()
        drop("sca1.txt", raw)
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA1","sca1-2009-03-19-00976.txt"))
        raw = tl.getTicket()

        drop("sca2.txt", raw)

        m = main.Main(verbose=0, call_center='SCA1')
        m.log.lock = 1
        m.run(runonce=1)
        # Should have two tickets
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 2)
        first_ticket = rows[0]
        second_ticket = rows[1]
        locates_from_first = self.tdb.getrecords("locate")

        # Set high profile for one of the locates on the first ticket
        self.tdb.update('locate','locate_id',locates_from_first[5]['locate_id'],high_profile=1)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        locates_from_first = self.tdb.getrecords("locate")
        sql = "exec dbo.tickets_to_route_noxml"
        rows = self.tdb.runsql_result(sql)

        self.assertEquals(len(locates_from_first),13)
        # Should be nothing left to route
        self.assertEquals(len(rows),0)

        for locate in locates_from_first:
            assigned_to = locate['assigned_to_id']
            if assigned_to is not None:
                # Close all of the locates assigned
                self.tdb.update('locate','locate_id',locate['locate_id'],closed=1)

        self.assertEquals(self.tdb.isclosed(m.ids[0]), 1)
        self.assertEquals(self.tdb.isclosed(m.ids[1]), 1)

        self.tdb.runsql("exec create_followup_ticket %s,17007" % (m.ids[0],))
        # Should have three tickets
        self.assertEquals(self.tdb.count("ticket"), 3)
        rows = self.tdb.getrecords("ticket",ticket_type='PROJECT TICKET')
        self.assertEquals(len(rows), 1)

        ticket_copy = rows[0]
        locates_from_copy = self.tdb.getrecords("locate", ticket_id=ticket_copy['ticket_id'])
        # All locates from the copy should be un-assigned since the -N locates are not copied
        [self.assertEquals(loc['status'],locate_status.blank) for loc in locates_from_copy]

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        locates_from_first = self.tdb.getrecords("locate", ticket_id=first_ticket['ticket_id'])
        self.assertEquals(len(locates_from_first),9)

        locates_from_copy = self.tdb.getrecords("locate", ticket_id=ticket_copy['ticket_id'])
        self.assertEquals(len(locates_from_copy),4)
        # All locates from the copy should be assigned since the -N locates are not copied
        [self.assertEquals(loc['status'],locate_status.assigned) for loc in locates_from_copy]
        self.assertEquals(locates_from_first[5]['status'],locate_status.assigned)

    def test_spam_1(self):
        '''
        Mantis 2321: ignore bogus emails for NCA2 responses
        Spam is being processed
        '''
        self.prepare('NCA2')

        datadir_path = datadir.datadir.path
        spam_tickets = glob(os.path.join(_testprep.TICKET_PATH,
                       "sample_tickets","NCA2","nca2-2009-06*.txt"))
        for ticket_file in spam_tickets:
            tl = ticketloader.TicketLoader(ticket_file,chr(12))
            raw = tl.tickets[0]
            drop(os.path.basename(ticket_file), raw)

        m = main.Main(verbose=0, call_center='NCA2')
        m.log.lock = 1
        m.log.logger.logfile = StringIO()
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()
        # Should have no tickets
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows),0)
        self.assertEquals(log.count('Probable spam'), 12)
        self.assertTrue('0 tickets processed' in log)

    test_spam_1.fails=1

    def test_summary_1(self):
        self.prepare('LNG1')
        try:
            self.empty_summary_fragment()
        except:
            pass
        _testprep.add_test_call_center(self.tdb, 'LNG1')
        call_centers.get_call_centers(self.tdb).reload()

        tickets = glob(os.path.join(_testprep.TICKET_PATH, "sample_tickets",
                                    "LNG1","TEST-2009-06-17*.txt"))
        # Drop all of the tickets
        for i,ticket_file in enumerate(tickets):
            f = open(ticket_file)
            raw = string.join(f.readlines(),'\n')
            f.close()
            drop("LNG1_xml_%d.txt" % (i,), raw)
        # Parse the lot
        m = main.Main(verbose=0, call_center='LNG1')
        m.log.logger.logfile = StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        self.assertTrue('79 tickets found on summary' in log)
        self.assertTrue('735 tickets found on summary' in log)
        rows = self.tdb.runsql_result("exec check_summary_by_ticket_LNG1 '03/02/2009','LNG1'")
        # Should all be found
        [self.assertEquals(row['found'],'0') for row in rows]
        self.assertEquals(len(filter(lambda x: x['client_code'] == 'NWN01',rows)),735)
        self.assertEquals(len(filter(lambda x: x['client_code'] == 'NWN02',rows)),79)

    def test_LOR1_new_client(self):
        '''
        Mantis 2328: unable to add new client to LOR1 ticket
        '''
        self.prepare('LOR1')
        try:
            # add client, client code
            delete_test_client = []
            delete_call_center = []
            rows = self.tdb.getrecords('call_center', cc_code='LOR1')
            if not rows:
                delete_call_center.append('LOR1')
                _testprep.add_test_call_center(self.tdb,'LOR1')
            rows = self.tdb.getrecords('call_center', cc_code='LNG1')
            if not rows:
                delete_call_center.append('LNG1')
                _testprep.add_test_call_center(self.tdb,'LNG1')

            rows = self.tdb.getrecords('client', oc_code='FALCON02', call_center='LOR1')
            if not rows:
                client_id = _testprep.add_test_client(self.tdb, 'FALCON02', 'LOR1')
                delete_test_client.append(client_id)

            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LOR1","LOR1-2009-06-29-00153.txt"))
            raw = tl.getTicket()
            drop("LOR1.txt", raw)
            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LOR1","LOR1-2009-06-29-00154.txt"))
            raw = tl.getTicket()
            drop("LOR2.txt", raw)
            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LOR1","LOR1-2009-06-29-01075.txt"))
            raw = tl.getTicket()
            drop("LOR3.txt", raw)
            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LOR1","LOR1-2009-06-29-01076.txt"))
            raw = tl.getTicket()
            drop("LOR4.txt", raw)
            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LOR1","LOR1-2009-06-29-01252.txt"))
            raw = tl.getTicket()
            drop("LOR5.txt", raw)
            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LOR1","LOR1-2009-06-29-01253.txt"))
            raw = tl.getTicket()
            drop("LOR6.txt", raw)

            m = main.Main(verbose=0, call_center='LOR1')
            m.log.lock = 1
            m.log.logger.logfile = StringIO()
            m.run(runonce=1)
            ticket_id = m.ids[0]   # id of ticket that was just posted
            t2 = self.tdb.getticket(ticket_id)
            self.assertEquals(t2.locates[0].client_code, 'FALCON02')

            ticket_id = m.ids[1]   # id of ticket that was just posted
            t2 = self.tdb.getticket(ticket_id)
            self.assertEquals(t2.locates[0].client_code, 'FALCON02')
            self.assertEquals(t2.locates[1].client_code, 'PGE01')

            ticket_id = m.ids[2]   # id of ticket that was just posted
            t2 = self.tdb.getticket(ticket_id)
            self.assertEquals(t2.locates[0].client_code, 'PGE04')
            self.assertEquals(t2.locates[1].client_code, 'WILBRB02')
            self.assertEquals(t2.locates[2].client_code, 'FALCON02')

            ticket_id = m.ids[3]   # id of ticket that was just posted
            t2 = self.tdb.getticket(ticket_id)
            self.assertEquals(t2.locates[0].client_code, 'PGE04')
            self.assertEquals(t2.locates[1].client_code, 'WILBRB02')
            self.assertEquals(t2.locates[2].client_code, 'FALCON02')

            ticket_id = m.ids[4]   # id of ticket that was just posted
            t2 = self.tdb.getticket(ticket_id)
            self.assertEquals(t2.locates[0].client_code, 'PPL15')
            self.assertEquals(t2.locates[1].client_code, 'FALCON02')

            ticket_id = m.ids[5]   # id of ticket that was just posted
            t2 = self.tdb.getticket(ticket_id)
            self.assertEquals(t2.locates[0].client_code, 'PPL15')
            self.assertEquals(t2.locates[1].client_code, 'FALCON02')

            log = m.log.logger.logfile.getvalue()
            m.log.logger.logfile.close()
        finally:
            _testprep.clear_database(self.tdb)
            for test_client in delete_test_client:
                _testprep.delete_test_client(self.tdb, test_client)
            for cc in delete_call_center:
                _testprep.delete_test_call_center(self.tdb, cc)

    def test_LWA1_1(self):
        """
        Mantis 2457: Some LWA tickets do not recognize clients on tickets.
        """
        tl = ticketloader.TicketLoader(os.path.join(TICKET_PATH,
             "sample_tickets", "LWA1", "LWA1-2009-12-14-00345.txt"))
        raw = tl.tickets[0]
        raw = raw.replace('PSEELC45', 'CLIENT1')
        raw = raw.replace('PSEELC46', 'CLIENT2')
        raw = raw.replace('PSEGAS40', 'CLIENT3')
        raw = raw.replace('PSEGAS41', 'CLIENT4')
        drop("test_LWA1_1.txt", raw, dir='test_incoming_LWA1')

        # store the ticket using main.py
        m = main.Main(verbose=0, call_center='LWA1')
        m.log.logger.logfile = StringIO()
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        # assert the ticket is in the database
        id = m.ids[-1]
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_id"], id)
        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertTrue('No UQ clients found for LWA1 ticket(s)' in log)
        self.assertTrue('Locates on ticket: CLIENT1,CLIENT2,CLIENT3,CLIENT4'
          in log)

    def test_3004_alert(self):
        # Mantis #2845
        # Assumption: AGL109 is in the client table for 3004
        self.prepare('3004')

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "3004-2011-08-agl", "TEST-2011-08-03-00004.txt"))
        raw = tl.getTicket()
        drop("test-3004-alert.txt", raw, dir="test_incoming")

        #self.conf_copy.processes[0]['format'] = '3004'
        p = {'incoming': 'test_incoming',
             'format': '3004',
             'processed': 'test_processed',
             'error': 'test_error',
             'attachments': ''}
        self.conf_copy.processes[0] = p
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='3004')
            m.run(runonce=1)

        # get the locates for the ticket we just posted
        ticket_id = m.ids[-1]
        # there should be some, at least
        locs = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assertTrue(locs)
        # there should be one with client code AGL109
        self.assertTrue([row for row in locs if row['client_code'] == 'AGL109'])
        # check the alert field on the locates; only AGL109 should be set
        for loc in locs:
            if loc['client_code'] == 'AGL109':
                self.assertEquals(loc['alert'], 'A')
            else:
                self.assertEquals(loc['alert'], None)

        trow = self.tdb.getrecords('ticket', ticket_id=ticket_id)[0]
        self.assertEquals(trow['alert'], 'A')

    def test_FMW1_merged_audits(self):
        # Mantis #3173

        self.prepare('FMW1')

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FMW1-2012-11-03-merged",
             "FMB1-2012-10-25-00-12-13-868-B2956.txt"))
        raw = tl.getTicket()
        drop("test-fmw1-merged.txt", raw, dir="test_incoming")

        p = {'incoming': 'test_incoming',
             'format': 'FMW1',
             'processed': 'test_processed',
             'error': 'test_error',
             'attachments': ''}
        self.conf_copy.processes[0] = p
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='FMW1')
            m.run(runonce=1)

        rows = self.tdb.getrecords('summary_header')
        self.assertTrue(rows)
        rows = self.tdb.getrecords('summary_detail')
        self.assertEquals(len(rows), 178)

    def test_FNV1_audits(self):
        # Mantis #3216

        self.prepare('FNV1')

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FNV1-2013-01-08", "FNV1-audit.txt"))
        raw = tl.getTicket()
        drop("test-fnv1-audit.txt", raw, dir='test_incoming')

        p = {'incoming': 'test_incoming',
             'format': 'FNV1',
             'processed': 'test_processed',
             'error': 'test_error',
             'attachments': ''}
        self.conf_copy.processes[0] = p
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='FNV1')
            m.run(runonce=1)

        rows = self.tdb.getrecords('summary_header')
        self.assertTrue(rows)
        rows = self.tdb.getrecords('summary_detail')
        self.assertEquals(len(rows), 480)

        rows = self.tdb.getrecords('summary_fragment')
        self.assertFalse(rows)


if __name__ == "__main__":
    unittest.main()

