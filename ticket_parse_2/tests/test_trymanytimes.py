# test_trymanytimes.py

import site; site.addsitedir('.')
import unittest
#
from trymanytimes import TryManyTimes
import dbinterface_old as dbinterface

class TestTryManyTimes(unittest.TestCase):

    def test_retry_without_log(self):
        data = []
        def f():
            data.append("!")
            raise dbinterface.TimeoutError, "Timeout expired"
        tmt = TryManyTimes(3)
        try:
            result = tmt(f)
        except dbinterface.TimeoutError:
            pass # ok
        except:
            self.Fail("TimeoutError expected")
        else:
            self.Fail("TimeoutError expected")

        # inspect data to see if we actually called it 3 times
        self.assertEquals(len(data), 3)

    def test_no_error(self):
        data = []
        def f(x, y, z):
            data.append("!")
            return 2*x + y/z
        tmt = TryManyTimes(5)
        result = tmt(f, 1, 4, 2)
        self.assertEquals(result, 4)
        self.assertEquals(len(data), 1)

    def test_non_timeout_error(self):
        data = []
        def f(x, y, z):
            data.append("!")
            return 2*x + y/z
        tmt = TryManyTimes(5)
        try:
            result = tmt(f, 1, 4, 0)
        except ZeroDivisionError:
            pass # ok
        except:
            self.Fail("ZeroDivisionError expected")
        else:
            self.Fail("ZeroDivisionError expected")

        # only call this once, not 5 times, because it's not a timeout!
        self.assertEquals(len(data), 1) # and not 5

    def test_retry_with_log(self):
        class Logger:
            def __init__(self):
                self.data = []
            def log_event(self, s):
                self.data.append(s)
        log = Logger()

        data = []
        def f():
            data.append("!")
            raise dbinterface.TimeoutError, "Timeout expired"
        tmt = TryManyTimes(3, log)
        try:
            result = tmt(f)
        except dbinterface.TimeoutError:
            pass # ok
        except Exception, e:
            self.fail("TimeoutError expected, got this instead: %s: %s" % (
             e.__class__.__name__, e.args[0]))
        else:
            self.fail("TimeoutError expected")

        # inspect data to see if we actually called it 3 times
        self.assertEquals(len(data), 3)
        # check the log as well
        self.assertEquals(len(log.data), 5)
        self.assertEquals(log.data[-1], "[TryManyTimes] Attempt 3/3 failed due to timeout.")


if __name__ == "__main__":

    unittest.main()

