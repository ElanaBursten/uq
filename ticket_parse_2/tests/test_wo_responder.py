# test_wo_responder.py

from __future__ import with_statement
import copy
import os
import shutil
import site; site.addsitedir('.')
import unittest
import xml.etree.ElementTree as ET
#
import _testprep
import callcenters
import config
import date
import et_tools
import ticket_db
import ticketloader
import ticketparser
import tools
import wo_responder
#
from mockery import *

# template to write a dummy ack file for testing.
ACK_TEMPLATE = """\
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<XML Type="ACK">
  <ACK>{{ok}}</ACK>
  <RESPONSEID>{{respid}}</RESPONSEID>
  <REASON>{{reason}}</REASON>
  <INTERNALNUMBER>{{number}}</INTERNALNUMBER>
</XML>
"""

UNICODE_NOTE = u'Unicode test note: \u2018testing\u2026\u2019 etc\u2026'

class TestWorkOrderResponder(unittest.TestCase):

    FILENAMES = ['00009', '00010', '00011']
    TEST_DIRS = ['wo-resp-dir', 'wo-ack-dir']

    def setUp(self):
        self.handler = ticketparser.TicketHandler()
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.conf_copy = copy.deepcopy(config.getConfiguration())

        # add config stuff for LAM01 WO responder
        self.conf_copy.wo_responders = {
            'LAM01': {'name': 'LAM01',
                      'resp_dir': 'wo-resp-dir',
                      'ack_dir': 'wo-ack-dir',
                     }
        }

        # add test directories
        for dirname in self.TEST_DIRS:
            try:
                os.mkdir(dirname)
            except:
                pass

    def tearDown(self):
        for dirname in self.TEST_DIRS:
            try:
                shutil.rmtree(dirname)
            except:
                pass

    def add_some_tickets(self, close=1):
        for filepart in self.FILENAMES:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "work-orders", "LAM01", "LAM01-2011-03-07-%s-A01.txt" %
                 filepart))
            raw = tl.tickets[0]
            wo = self.handler.parse(raw, ["Lambert"], call_center='LAM01')
            wo.driveway_bore_count = 33 # must show up in response XML
            wo.insert(self.tdb)

        # check that we have 3 records in work_orders
        rows = self.tdb.getrecords('work_order')
        self.assertEquals(len(rows), 3)

        # pseudo-route these work orders
        # note: value of @Closed=0 is important here; a record should be added
        # to the responder queue even if @Closed<>1 (re Mantis #2802)
        for row in rows:
            self.tdb.runsql("""
              exec assign_work_order %s, %s, 'ROUTER'
            """ % (row['wo_id'], '200'))
            self.tdb.runsql("""
             exec update_work_order_status %s, 'C', 0,
               '2011-03-23 13:00:00', 200, 'ROUTER', NULL
            """ % row['wo_id'])
            
            self.tdb.runsql("""
             insert notes (foreign_id, foreign_type, note, uid, entry_date, active)
             values (%s, 7, '%s', 500, getdate(), 1)
            """ % (row['wo_id'], UNICODE_NOTE))

            # close work order
            if close:
                self.tdb.runsql("""
                 update work_order
                 set closed=1, closed_date=GETDATE()
                 where wo_id=%s
                """ % row['wo_id'])

            # apparently this already adds records to wo_responder_queue ^_^

            # TODO: add notes, attachments; verify that they are in the
            # response

    def test_get_pending_responses(self):
        self.add_some_tickets()
        options = wo_responder.WorkOrderResponderOptions(verbose=0,
         sends_email=0)
        wor = wo_responder.WorkOrderResponder(options)
        rows = wor.get_pending_responses('LAM01')
        self.assertEquals(len(rows), 3)
        #print rows[0]

    def test_LAM01(self, closed=1):
        # will be tested twice; once with closed work orders, once with
        # non-closed ones. non-closed WOs should be processed as well by the
        # responder (and are returned by the stored proc).

        self.add_some_tickets(closed)
        options = wo_responder.WorkOrderResponderOptions(verbose=0,
         sends_email=0)
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            wor = wo_responder.WorkOrderResponder(options)
            wor.run()

        # there should be 3 files in the resp-dir directory
        resp_files = os.listdir('wo-resp-dir')
        self.assertEquals(len(resp_files), 3)

        # pick a file and inspect it
        idx = tools.findfirst(resp_files, lambda f: 'V0002850' in f)
        fn = os.path.join('wo-resp-dir', resp_files[idx])
        # XXX check response id ^_^
        xml = ET.parse(fn).getroot()
        self.assertEquals(xml.find('Response/InternalJobID').text, 'V0002850')
        self.assertEquals(xml.find('Response/NumDriveWayBores').text, '33')
        self.assertEquals(xml.find('Response/NumRoadBores').text, '0')
        self.assertEquals(xml.find('Response/Remarks').text, 'BSW WO#V0002850')
        self.assertEquals(xml.find('Response/Notes').text, UNICODE_NOTE)

        # the wo_responder_queue should be empty
        worq_rows = self.tdb.getrecords('wo_responder_queue')
        self.assertEquals(len(worq_rows), 0)

        # check the wo_response_log
        worl_rows = self.tdb.getrecords('wo_response_log')
        self.assertEquals(len(worl_rows), 3)
        # check contents...
        for row in worl_rows:
            self.assertEquals(row['success'], '0', repr(row))
        wrong_row = worl_rows[1] # will be used later on

        # add ack files
        resp_id = worl_rows[0]['wo_response_id']
        xml = ACK_TEMPLATE.replace('{{ok}}', "OK")
        xml = xml.replace('{{number}}', "V0002850")
        xml = xml.replace('{{reason}}', "-")
        xml = xml.replace('{{respid}}', resp_id)
        path = os.path.join('wo-ack-dir', 'ack1.xml')
        with open(path, 'wb') as f:
            f.write(xml)

        # run responder again
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            wor = wo_responder.WorkOrderResponder(options)
            wor.run()

        # check wo_response_log
        # one of them should have success=1 now
        worl_rows = self.tdb.getrecords('wo_response_log')
        for row in worl_rows:
            if row['wo_response_id'] == resp_id:
                self.assertEquals(row['success'], '1')
                self.assertEquals(row['reply'], '-')
            else:
                self.assertEquals(row['success'], '0')
                self.assertEquals(row['reply'], '(ack waiting)')

        # verify that ack files are gone
        ack_files = os.listdir('wo-ack-dir')
        self.assertEquals(ack_files, [])

        # try an ack file that is not OK
        xml = ACK_TEMPLATE.replace('{{ok}}', "NOT OK")
        xml = xml.replace('{{number}}', 'V0002899')
        xml = xml.replace('{{reason}}', "BAD ExtentOfWork, BAD NumRoadBores")
        xml = xml.replace('{{respid}}', wrong_row['wo_response_id'][:40])
        path = os.path.join('wo-ack-dir', 'ack2.XML')
        with open(path, 'wb') as f:
            f.write(xml)

        # run responder again
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            wor = wo_responder.WorkOrderResponder(options)
            wor.run()

        # verify that ack files are gone
        ack_files = os.listdir('wo-ack-dir')
        self.assertEquals(ack_files, [])

        # check record in response log
        z = self.tdb.getrecords('wo_response_log',
            wo_response_id=wrong_row['wo_response_id'])[0]
        self.assertEquals(z['success'], '0')
        self.assertTrue(z['reply'].startswith('BAD ExtentOfWork'))

    def test_LAM01_not_closed(self):
        self.test_LAM01(closed=0)

    def test_unicode_in_ack(self):
        path = os.path.join(_testprep.TICKET_PATH, "responder", "LAM01",
               "ack", "LocateResponseAck_V0009907_20110415_1404.XML")
        data = open(path, 'rb').read()
        with open(os.path.join('wo-ack-dir', 'ack-unicode.XML'), 'wb') as f:
            f.write(data)

        # run responder; this should pick up and correctly process an ack file
        # with Unicode encoding (UTF-8)
        options = wo_responder.WorkOrderResponderOptions(verbose=0,
         sends_email=0)
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            wor = wo_responder.WorkOrderResponder(options)
            wor.run()

if __name__ == "__main__":
    unittest.main()

