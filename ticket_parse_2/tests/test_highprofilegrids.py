# test_highprofilegrids.py

import unittest
import site; site.addsitedir('.')
#
import client
import date
import highprofilegrids as hpg
import locate
import ticket_db
import ticket_grid
import ticketparser
import ticket as ticketmod
import _testprep

class TestHighProfileGrids(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()

    # NOTE: we test Main's usage of grids in test_main.py

    def test_hp_grids(self):
        # add some test records
        self.tdb.delete('hp_grid', call_center='XYZ')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ABC', 'Harry')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ABC', 'Ron')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ABC', 'Hermione')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'DEF', 'Snape')
        _testprep.add_test_hp_grid(self.tdb, 'ZYX', 'PQR', 'Dumbledore')

        hpgrid = hpg.HighProfileGrids(self.tdb)
        self.assertEquals(hpgrid.data['XYZ']['ABC'], ['Harry', 'Ron', 'Hermione'])
        self.assertEquals(hpgrid.get('XYZ', 'DEF'), ['Snape'])
        self.assertEquals(hpgrid.get('BLAH', 'BLAH'), [])
        self.assertEquals(hpgrid.get('XYZ', 'FOO'), [])

        self.assertEquals(len(hpgrid.data['XYZ']), 2)

    def test_checking(self):
        # test checking of high profile grids

        # add some test records
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ABC', 'Harry')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ABC', 'Ron')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ABC', 'Hermione')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'DEF', 'Snape')
        _testprep.add_test_hp_grid(self.tdb, 'ZYX', 'PQR', 'Dumbledore')

        # delete ticket_grid_log
        self.tdb.dbado.deleterecords('ticket_grid_log')

        t = ticketmod.Ticket()
        t.ticket_number = '1234567890'
        t.ticket_format = 'XYZ'
        t.locates = [locate.Locate("ABC"), locate.Locate("DEF")]
        t.grids = ['Harry', 'Ron', 'Seamus', 'Ginny', 'Dumbledore']
        t.image = "blah"
        id = self.tdb.insertticket(t)

        # XYZ has 5 clients
        clients = [client.Client(client_code=z)
                   for z in ["ABC", "DEF", "PQR", "FOO", "BAR"]]

        log = []
        def callback(ticket, ticket_id, client, relevant_grids, grid_email):
            log.append((ticket, ticket_id, client, relevant_grids, grid_email))
            return 1

        hpgrid = hpg.HighProfileGrids(self.tdb)
        hpgrid.check(t, id, -1, clients, callback)

        # For client ABC, there are three HP grids (Harry, Ron, Hermione); two
        # of there appear on the ticket; they are the "relevant grids" that
        # will be passed to the callback.
        # For client DEF, there's one HP grid (Snape), which doesn't appear
        # on the ticket.
        # Grid 'Dumbledore' appears on the ticket, but its client (PQR) does
        # not; therefore no callback.

        self.assertEquals(len(log), 1)
        self.assertEquals(log[0][2], t.locates[0])
        self.assertEquals(log[0][3], ['Harry', 'Ron'])
        self.assertEquals(log[0][4], "")

        # check has_hp_grids method
        x = hpgrid.has_hp_grids(t, None)
        self.assertEquals(bool(x), True)

        # check ticket_grid_log table
        rows = self.tdb.dbado.getrecords('ticket_grid_log', ticket_id=id)
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['client_code'], 'ABC')

        # latest "sent" date should be today
        d = ticket_grid.get_date_sent(self.tdb, t.ticket_format, t.ticket_number, 'ABC')
        today = date.Date().isodate()[:10]
        self.assertEquals(d[:10], today)

        t.grids = ["Fred", "Barney"]
        self.assertEquals(bool(hpgrid.has_hp_grids(t, None)), False)
        t.grids.append('Ron')
        self.assertEquals(bool(hpgrid.has_hp_grids(t, None)), True)

    def test_get_hp_grid_clients(self):
        # add some test records
        self.tdb.delete('hp_grid', call_center='XYZ')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ABC', 'Harry')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ABC', 'Ron')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ABC', 'Hermione')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'DEF', 'Snape')
        _testprep.add_test_hp_grid(self.tdb, 'ZYX', 'PQR', 'Dumbledore')

        # make dummy ticket
        t = ticketmod.Ticket()
        t.ticket_number = '1234567890'
        t.ticket_format = 'XYZ'
        t.locates = [locate.Locate("ABC"), locate.Locate("DEF")]
        t.grids = ['Harry', 'Ron', 'Seamus', 'Ginny', 'Dumbledore']
        t.image = "blah"

        hpgrid = hpg.HighProfileGrids(self.tdb)
        locates = hpgrid.get_hp_grid_clients(t, None)
        self.assertEquals(locates, t.locates[:1]) # only 'ABC', not 'DEF'



if __name__ == "__main__":

    unittest.main()

