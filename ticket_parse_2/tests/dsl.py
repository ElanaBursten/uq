# dsl.py
# A DSL (sort of) for testing tickets and such.

# TODO: Allow for selecting the N-th tickets from a file... currently it just
# picks the first one (index 0).

"""
How to use:

SETTING UP:

formats: Foo, Bar, ...

    Set the available formats for the current file. A separate test run will be
    done for each of these formats. Specifying only one format is OK; multiple
    names can be delimited by spaces and/or commas.

source: foo/bar/some_call_center.txt

    Load the first ticket (as raw data) from the given file, then parses it
    using the current format. Multiple source files can be specified in the
    same file, this will then cause a new ticket to be loaded and parsed.

TESTING:

- fieldname: value

    Take the value of the current ticket's field <fieldname>, and compare it
    against the specified value. Both values are expected to be strings. Leading
    and trailing whitespace will be ignored. Empty values are allowed.

? expression
! value

    '?' gets the result of an expression, and '!' tests it against an expected
    value. Both must be expressions that can be evaluated by Python (i.e.
    there is no implicit string comparison going on).

    For example:

        ? t.locates[0].client_code
        ! 'AGL332'

~ decimals fieldname: value

    Compare the value of the field to the given value. Both have to be numbers,
    and only the number of decimals specified are compared. Useful to compare
    floating point values, which are often inexact.

-start- fieldname: value
-end- fieldname: value

    Test if the given field starts/end with the given value.

-x- code

    Execute code. Useful if we need to do the odd import statement or
    something. Tests that require more custom code are better written in a
    Python test case, of course.

"""

import os
import sys
#
import filetools
import static_tables
import ticketloader
import tools
import _testprep

class TestDSLError(Exception):
    pass

class TestDSL:

    def __init__(self, filename=None):
        self.source = ""
        self.formats = []
        self.format = ""
        self.current_string = ""
        self.ticket_image = ""
        self.ticket = None
        self.last_expr = None # last ? expression
        self.last_result = None # last ! result
        self.data = ""
        self.filename = filename
        self.shortname = os.path.split(filename)[1]

        self.setup_hooks()

        # if a filename is specified, load it.
        if filename:
            self.data = filetools.read_file(filename, 'r')

    def namespace(self):
        return {'t': self.ticket, 'obj': self}

    def setup_hooks(self):
        self.indicators = {
            'source:':     self.do_source,
            'formats:':    self.do_formats,
            '-':           self.do_minus,
            '?':           self.do_question_mark,
            '!':           self.do_exclamation_mark,
            '~':           self.do_tilde,
            '-start-':     self.do_start,
            '-end-':       self.do_end,
            '-x-':         self.do_execute,
        }

    def scan_formats(self, testcase):
        lines = self.data.split('\n')
        for line in lines:
            if line.lstrip().startswith("formats:"):
                s = line.lstrip()[8:].strip()
                self.do_formats(s, testcase)
                return

    def run_tests(self, testcase):
        """ Run the tests specified in a previously loaded file with test data.
            An instance of a TestCase must be passed to run assertion methods
            on (assertEquals, etc). """
        lines = self.data.split('\n')
        for line in lines:
            if not line.strip():
                continue # empty lines are ignored
            if line.lstrip().startswith('#'):
                continue # comments are ignored
            if line.strip() == 'list':
                self.do_list() # todo(dan) use self.indicators?
            else:
                indicator, rest = line.split(None, 1) # first "word", kind of
                f = self.indicators.get(indicator)
                if not f:
                    raise TestDSLError("Invalid indicator: %r" % indicator)
                f(rest, testcase)

    def run_all_tests(self, testcase):
        # look for "formats:" line
        self.scan_formats(testcase)
        # run tests for each of these formats
        for fmt in self.formats:
            self.format = fmt
            self.run_tests(testcase)

    #
    # hooks

    def do_source(self, s, testcase):
        """ Specifying the source causes the first ticket to be loaded and
            parsed for the current format. """
        if not s.startswith("/"):
            s = os.path.join(_testprep.TICKET_PATH, s.strip())
        self.source = s
        tl = ticketloader.TicketLoader(s)
        self.ticket_image = tl.tickets[0]
        self.ticket = testcase.handler.parse(self.ticket_image, [self.format])

    def do_list(self):
        print >>sys.stderr, '\n********** START LIST **********' 
        print >>sys.stderr, 'source:', self.source.replace('/', '\\')

        attribute_list = [f[0] for f in sorted(static_tables.ticket)]
        attribute_list += ['grids', 'locates']
        # todo(dan) Should some of these be skipped instead?
        uncommented_attributes = ['active', 'parse_errors', 'parsed_ok']

        for attribute in attribute_list:
            value = getattr(self.ticket, attribute)
            line_start = '#'
            if attribute in uncommented_attributes:
                line_start = ''

            if attribute == 'parsed_ok':
                print >>sys.stderr, line_start + '~ 0 parsed_ok: 1.0'
            elif attribute in ['work_lat', 'work_long']:
                value = 1.0 * long(float(value) * 10L**6) / 10L**6
                print >>sys.stderr, line_start + '~ 6', attribute + ':', value
            elif attribute == 'locates':
                print >>sys.stderr
                print >>sys.stderr, line_start + \
                  '? [l.client_code for l in t.locates]'
                print >>sys.stderr, line_start + \
                  '!', [l.client_code for l in self.ticket.locates]
            elif attribute == 'grids':
                pass # todo(dan) add this
            else:
                value = value or ''
                lines = value.split('\n')
                if len(lines) > 1:
                    print >>sys.stderr, line_start + \
                      '-start-', attribute + ':', lines[0]
                    print >>sys.stderr, line_start + \
                      '-end-', attribute + ':', lines[-1]
                else:
                    print >>sys.stderr, line_start + '-', attribute + ':', value

        print >>sys.stderr, '********** END LIST **********'

    def do_formats(self, s, testcase):
        if self.formats:
            return
        formats = s.split()
        formats = [fmt.strip(";,") for fmt in formats]
        self.formats = formats

    def do_minus(self, s, testcase):
        try:
            fieldname, value = s.split(None, 1) # e.g. "ticket_number: 123456"
        except ValueError:
            # empty values are allowed
            if s.strip().endswith(':'):
                fieldname, value = s.strip().rstrip(':'), ""
            else:
                raise TestDSLError("Could not unpack line: %r" % s)
        fieldname = fieldname.rstrip(':')
        if not hasattr(self.ticket, fieldname):
            raise TestDSLError("[%s] Unknown ticket attribute: %s" % (
              self.shortname, fieldname))
        real_value = getattr(self.ticket, fieldname)
        testcase.assertEquals(value.strip(), (real_value or '').strip(),
          "[%s] %s: expected %r, got %r instead" % (self.shortname, fieldname,
          value, real_value))

    def do_question_mark(self, s, testcase):
        """ Evaluate the Python expression following the '?' and store its
            value. """
        self.last_expr = s
        self.last_result = eval(s, globals(), self.namespace())

    def do_exclamation_mark(self, s, testcase):
        """ Compare the value of the previous expression against the value
            specified. """
        e1 = self.last_result
        e2 = eval(s, globals(), self.namespace())
        testcase.assertEquals(e1, e2, "[%s] %s: expected %r, got %r instead" % (
          self.shortname, self.last_expr, e2, e1))

    def do_tilde(self, s, testcase):
        decimals, fieldname, value = s.split()
        decimals = int(decimals)
        fieldname = fieldname.rstrip(':')
        value = float(value)

        multiplier = 10L**decimals
        i1 = long(float(getattr(self.ticket, fieldname)) * multiplier)
        i2 = long(value * multiplier)
        testcase.assertEquals(i1, i2, "%s => got %s instead" % (
          s, (1.0*i1)/multiplier))

    def do_start(self, s, testcase):
        fieldname, value = s.split(None, 1)
        fieldname = fieldname.rstrip(":")
        real_value = getattr(self.ticket, fieldname)
        testcase.assertTrue(real_value.startswith(value),
          "[%s] %s: expected to start with %r; starts with %r instead" % (
            self.shortname, fieldname, value, real_value[:len(value)]))

    def do_end(self, s, testcase):
        fieldname, value = s.split(None, 1)
        fieldname = fieldname.rstrip(":")
        real_value = getattr(self.ticket, fieldname)
        testcase.assertTrue(real_value.endswith(value),
          "[%s] %s: expected to end with %r; ends with %r instead" % (
            self.shortname, fieldname, value, real_value[-len(value):]))

    def do_execute(self, s, testcase):
        exec s in self.namespace(), globals()

