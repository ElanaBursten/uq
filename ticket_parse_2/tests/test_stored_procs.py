# test_stored_procs.py

import site; site.addsitedir('.')
import string
import unittest
#
import _testprep
import date
import date_special
import locate
import ticket_db
import ticketloader
import ticketparser

class TestStoredProcs(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()

    def test_close_old_locates(self):
        """ Test the close_old_locates stored proc. """

        # XXX this test does not work when run on a holiday!  bad design,
        # but how to fix it?  the restriction is built into the stored proc...
        holiday_list = date_special.read_holidays(self.tdb)
        # create test employees
        emp200 = _testprep.add_test_employee(self.tdb, 'Test200')
        emp201 = _testprep.add_test_employee(self.tdb, 'Test201')
        empauto = _testprep.add_test_employee(self.tdb, 'autocloser')
        _testprep.add_test_client(self.tdb, 'AGL18', 'Atlanta', False, False)

        # add some tickets
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        ids = []
        for raw in tl.tickets:
            t = self.handler.parse(raw, ["Atlanta"])
            id = self.tdb.insertticket(t, whodunit='parser')
            ids.append(id)

        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 5)

        # pretend that all these tickets came in today
        sql = "update ticket set transmit_date = getdate(), call_date = getdate()"
        self.tdb.runsql(sql)

        # ...except for the first
        sql = """
         update ticket set transmit_date = getdate() - 1
         where ticket_id = %s
        """ % (ids[0],)
        self.tdb.runsql(sql)

        # pretend all locates of the first ticket are clients
        sql = """
         update locate
         set status = '-R'
         where ticket_id = %s
        """ % (ids[0],)
        self.tdb.runsql(sql)

        # pretend we assigned USW01 of the first ticket to locator emp200
        sql = """
         update locate
         set assigned_to = %s
         where ticket_id = %s
         and client_code = 'USW01'
        """ % (emp200, ids[0],)
        self.tdb.runsql(sql)

        # make sure employee emp200 has autoclosed = 0
        sql = """
         update employee
         set autoclose = 0
         where emp_id = %s
        """ % (emp200,)
        self.tdb.runsql(sql)

        # pretend we assigned AGL18 of the first ticket to locator emp201
        sql = """
         update locate
         set assigned_to = %s
         where ticket_id = %s
         and client_code = 'AGL18'
        """ % (emp201, ids[0],)
        self.tdb.runsql(sql)

        # make sure employee emp201 has autoclosed = 1
        sql = """
         update employee
         set autoclose = 1
         where emp_id = %s
        """ % (emp201,)
        self.tdb.runsql(sql)

        # run stored proc
        sql = "exec close_old_locates_3 'C', 1, %s" % (empauto,)
        self.tdb.runsql(sql)

        # check which tickets/locates are closed
        today = date.Date()
        yesterday = today - 1
        locates = self.tdb.getrecords('locate')
        numclosed = 0
        for loc in locates:
            if loc['ticket_id'] == ids[0]:
                if loc['client_code'] == 'USW01':
                    self.assertEquals(loc['closed'], '0')
                    self.assertEquals(loc['status'], '-R')
                else:
                    if date_special.isholiday(today, loc['client_code'], holiday_list):
                        self.assertEquals(loc['closed'], '0')
                        self.assertEquals(loc['status'], '-R')
                    else:
                        self.assertEquals(loc['closed'], '1')
                        self.assertEquals(loc['status'], 'C')

                        if loc['client_code'] == 'AGL18':
                            self.assertEquals(loc['closed_by_id'], emp201)
                        else:
                            # closed_by_id should have default value
                            self.assertEquals(loc['closed_by_id'], empauto)
                        numclosed += 1
            else:
                self.assertEquals(loc['closed'], '0')
                self.assertNotEqual(loc['status'], 'C')

        if date_special.isholiday(today, '*', holiday_list):
            self.assertEquals(numclosed, 0)
        else:
            self.assertEquals(numclosed, 13-1)

    def test_find_munic_area_locator_2(self):
        # clean up
        self.tdb.delete('county_area', state='XY')

        # create new records
        area = 400
        _testprep.add_test_county_area(self.tdb, 'CITY', area, state='XY',
         call_center='XYZZY')
        _testprep.add_test_county_area(self.tdb, 'CITY', area, state='XY')
        _testprep.add_test_county_area(self.tdb, 'CITY', area, state='XY',
         call_center='FUBAR')
        sql = """insert county_area (state, county, munic, area_id)
                  values ('XY', 'CITY', 'BAR', 400)"""
        self.tdb.runsql(sql)

        def find(state, county, municipality, call_center):
            sql = "exec find_munic_area_locator_2 '%s', '%s', '%s', '%s'" % (
                  state, county, municipality, call_center)
            return self.tdb.runsql_result(sql)

        rows = find('XY', 'CITY', 'BAR', 'XYZZY')
        self.assertEquals(len(rows), 3)
        # order is important... we want the call center match first
        self.assertEquals(rows[0]['call_center'], 'XYZZY')
        self.assertEquals(rows[1]['call_center'], '')
        self.assertEquals(rows[2]['call_center'], None)

        rows = find('XY', 'CITY', 'BAR', 'FUBAR')
        self.assertEquals(len(rows), 3)
        self.assertEquals(rows[0]['call_center'], 'FUBAR')
        self.assertEquals(rows[1]['call_center'], '')
        self.assertEquals(rows[2]['call_center'], None)

        # test "official" method
        rows = self.tdb.find_munic_area_locator_2('XY', 'CITY', 'BAR', 'XYZZY')
        self.assertEquals(rows[0]['call_center'], 'XYZZY')

    def test_find_qtrmin_2(self):
        # XXX later: add our own area and locator, so we are not dependent on
        # the contents of the database

        # cleanup
        self.tdb.delete('map_qtrmin', qtrmin_lat='1234E')

        # find locator associated with area 4510
        locator_id = self.tdb.runsql_result("""
          select locator_id from area where area_id = 4510
        """)[0]['locator_id']
        # make sure this locator is active
        self.tdb.runsql("""
          update employee
          set active=1, can_receive_tickets=1
          where emp_id = %d
        """ % int(locator_id))

        # add test records
        _testprep.add_test_qtrmin(self.tdb, '1234E', '5678E', 4510, '')
        _testprep.add_test_qtrmin(self.tdb, '1234E', '5678E', 4510, 'XYZZY')
        _testprep.add_test_qtrmin(self.tdb, '1234E', '5678E', 4510, 'FUBAR')
        # note: adding a record with call_center=NULL is impossible, because
        # the field defaults to '' (empty string)

        rows = self.tdb.getrecords('map_qtrmin', qtrmin_lat='1234E')
        self.assertEquals(len(rows), 3)

        def find(lat, long, call_center):
            sql = "exec find_qtrmin_2 '%s', '%s', '%s'" % (
                  lat, long, call_center)
            return self.tdb.runsql_result(sql)

        rows = find('1234E', '5678E', 'XYZZY')
        self.assertEquals(len(rows), 2)
        # order is important... we want the call center match first
        self.assertEquals(rows[0]['call_center'], 'XYZZY')
        self.assertEquals(rows[1]['call_center'], '')

        rows = find('1234E', '5678E', 'FUBAR')
        self.assertEquals(len(rows), 2)
        # order is important... we want the call center match first
        self.assertEquals(rows[0]['call_center'], 'FUBAR')
        self.assertEquals(rows[1]['call_center'], '')

        # use "official" method
        rows = self.tdb.find_qtrmin_2('1234E', '5678E', 'XYZZY')
        self.assertEquals(rows[0]['call_center'], 'XYZZY')

        # if there is no record with the given call center in map_qtrmin, then
        # a similar record with empty call center field qualifies too:
        rows = self.tdb.find_qtrmin_2('1234E', '5678E', 'NOSUCHTHING')
        self.assertEquals(rows[0]['call_center'], '')

    def test_get_pending_responses(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=id)
        lid1 = locates[0]['locate_id']
        self.tdb.runsql("exec insert_responder_queue %s" % (lid1,))

        rows = self.tdb.runsql_result("exec get_pending_responses 'Atlanta'")
        self.assertEquals(len(rows), 1)

        # try again, this time with do_not_respond_before in far future
        _testprep.clear_database(self.tdb)
        t.do_not_respond_before = (date.Date() + 1000).isodate()
        # "unlink" ticket_ids before storing again
        t.ticket_id = None
        for loc in t.locates:
            loc.ticket_id = loc.locate_id = None
        id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=id)
        lid1 = locates[0]['locate_id']
        self.tdb.runsql("exec insert_responder_queue %s" % (lid1,))

        rows = self.tdb.runsql_result("exec get_pending_responses 'Atlanta'")
        self.assertEquals(len(rows), 0)

    def test_insert_ticket_ack(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_insert_ticket_ack.txt')

        self.tdb.insert_ticket_ack(id)
        trows = self.tdb.getrecords("ticket_ack")
        self.assertEquals(len(trows), 1)
        self.assertEquals(trows[0]['ticket_id'], id)

        # if the ticket_id already exists, we should not get an error
        self.tdb.insert_ticket_ack(id)
        trows = self.tdb.getrecords("ticket_ack")
        self.assertEquals(len(trows), 1)
        self.assertEquals(trows[0]['ticket_id'], id)
        self.assertEquals(trows[0]['locator_emp_id'], None)

        # insert another ticket
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ['Atlanta'])
        id2 = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id2, t, 'test_insert_ticket_ack.txt')

        locates2 = self.tdb.getrecords("locate", ticket_id=id2)
        self.assert_(locates2)
        lid1 = locates2[0]['locate_id']

        sql = """
         insert assignment (locate_id, locator_id, active, added_by)
         values (%s, 200, 1, 'test')
        """ % (lid1,)
        self.tdb.runsql(sql)

        # check that assignment is actually there
        assignments = self.tdb.getrecords("assignment", locate_id=lid1)
        self.assertEquals(len(assignments), 1)

        # add ticket_ack for this ticket
        self.tdb.insert_ticket_ack(id2)

        # check locator_emp_id
        acks = self.tdb.getrecords("ticket_ack", ticket_id=id2)
        self.assertEquals(len(acks), 1)
        self.assertEquals(acks[0]['locator_emp_id'], '200')

    def test_insert_ticket_ack_again(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_insert_ticket_ack_again.txt')

        self.tdb.insert_ticket_ack(id)
        trows = self.tdb.getrecords("ticket_ack")
        self.assertEquals(len(trows), 1)
        self.assertEquals(trows[0]['ticket_id'], id)
        self.assertEquals(trows[0]['locator_emp_id'], None)

        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assert_(locates)
        lid1 = locates[0]['locate_id']

        # insert assignment so we have a locator
        sql = """
         insert assignment (locate_id, locator_id, active, added_by)
         values (%s, 200, 1, 'test')
        """ % (lid1,)
        self.tdb.runsql(sql)

        # check that assignment is actually there
        assignments = self.tdb.getrecords("assignment", locate_id=lid1)
        self.assertEquals(len(assignments), 1)

        # add ticket_ack for this ticket
        self.tdb.insert_ticket_ack(id)
        trows = self.tdb.getrecords("ticket_ack")
        self.assertEquals(len(trows), 1)
        self.assertEquals(trows[0]['ticket_id'], id)
        self.assertEquals(trows[0]['has_ack'], '0')
        self.assertEquals(trows[0]['locator_emp_id'], '200')

    def test_insert_ticket_ack_3(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Atlanta'])
        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_insert_ticket_ack_3_1.txt')

        self.tdb.insert_ticket_ack(id)
        trows = self.tdb.getrecords("ticket_ack")
        self.assertEquals(len(trows), 1)
        self.assertEquals(trows[0]['ticket_id'], id)
        self.assertEquals(trows[0]['locator_emp_id'], None)

        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assert_(locates)
        lid1 = locates[0]['locate_id']

        # insert assignment so we have a locator
        sql = """
         insert assignment (locate_id, locator_id, active, added_by)
         values (%s, 200, 1, 'test')
        """ % (lid1,)
        self.tdb.runsql(sql)

        # check that assignment is actually there
        assignments = self.tdb.getrecords("assignment", locate_id=lid1)
        self.assertEquals(len(assignments), 1)

        # add ticket_ack for this ticket
        self.tdb.insert_ticket_ack(id)

        trows = self.tdb.getrecords("ticket_ack")
        self.assertEquals(len(trows), 1)
        self.assertEquals(trows[0]['ticket_id'], id)
        self.assertEquals(trows[0]['has_ack'], '0')
        self.assertEquals(trows[0]['locator_emp_id'], '200')

        # Ack the ticket ack
        sql = """
         update ticket_ack set has_ack = 1
         where ticket_id = %s
        """ % (id,)
        self.tdb.runsql(sql)
        # change the transmit date
        raw = string.replace(raw, "2001-12-31 10:14:39", "2001-12-31 12:14:39")
        t = self.handler.parse(raw, ['Atlanta'])
        id2 = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id2, t, 'test_insert_ticket_ack_3_2.txt')
        # re-ack ticket_ack for this ticket
        self.tdb.insert_ticket_ack(id2)
        trows = self.tdb.getrecords("ticket_ack")
        self.assertEquals(len(trows), 2)
        self.assertEquals(trows[0]['ticket_id'], id)
        self.assertEquals(trows[0]['has_ack'], '1')
        self.assertEquals(trows[1]['ticket_id'], id2)
        self.assertEquals(trows[1]['has_ack'], '0')

    def __OBSOLETE_test_insert_fax_queue(self):
        # first, we need a ticket with locates
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Atlanta"])
        id = self.tdb.insertticket(t)

        # get the locates that were inserted
        locates = self.tdb.getrecords("locate", ticket_id=id)
        locate_id = locates[0]['locate_id']

        # change the status of the first locate so we get an entry in the
        # locate_status table (done by locate trigger)
        sql = """
         update locate
         set status = 'M', closed_date = '%s'
         where locate_id = %s
        """ % (date.Date().isodate(), locate_id,)
        self.tdb.runsql(sql)

        # check locate_status
        locate_status_rows = self.tdb.getrecords("locate_status", locate_id=locate_id)
        self.assertEquals(len(locate_status_rows), 1)

        # insert a rule in fax_queue_rules
        _testprep.insert_fax_queue(self.tdb, 'Atlanta',
         locates[0]['client_code'], 'M', 'GA')

        # empty fax queue so we have a "clean slate" to work with
        self.tdb.deleterecords("fax_queue")

        # try to insert a locate in the fax_queue
        sql = "exec insert_fax_queue %s" % (locate_id,)
        self.tdb.runsql(sql)
        # insert another locate_id that should not make the queue, because
        # there's no rule for it
        sql = "exec insert_fax_queue %s" % (locates[1]['locate_id'],)
        self.tdb.runsql(sql)

        queued = self.tdb.getrecords("fax_queue")
        self.assertEquals(len(queued), 1)

    def __OBSOLETE_test_queue_faxes_3(self):
        # insert tickets for various call centers
        stuff = [
            ("../testdata/Atlanta-1.txt", "Atlanta", "GIEGELGAK"),
            ("../testdata/Orlando-1.txt", "Orlando", "BIZNICH"),
            ("../testdata/Houston1-1.txt", "Houston1", "SHAKEZULA"),
        ]
        ids = []
        for filename, call_center, testterm in stuff:
            tl = ticketloader.TicketLoader(filename)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, [call_center])
            t.locates.insert(0, locate.Locate(testterm))
            id = self.tdb.insertticket(t)
            ids.append(id)

        # make sure there are rules for these centers
        _testprep.insert_fax_queue(self.tdb, 'Atlanta', 'GIEGELGAK', 'M', 'GA')
        _testprep.insert_fax_queue(self.tdb, 'FOL1', 'BIZNICH', '*', 'FL')
        _testprep.insert_fax_queue(self.tdb, 'FHL1', 'SHAKEZULA', 'M', 'TX')

        # empty fax queue so we have a "clean slate" to work with
        self.tdb.deleterecords("fax_queue")
        self.tdb.deleterecords("fax_message_detail")
        self.tdb.deleterecords("fax_message")

        # which locates do we use?
        loc1 = self.tdb.getrecords("locate", ticket_id=ids[0], client_code='GIEGELGAK')[0]
        loc2 = self.tdb.getrecords("locate", ticket_id=ids[1], client_code='BIZNICH')[0]
        loc3 = self.tdb.getrecords("locate", ticket_id=ids[2], client_code='SHAKEZULA')[0]

        # change the status of the first locate so we get an entry in the
        # locate_status table (done by locate trigger)
        for loc in (loc1, loc2, loc3):
            sql = """
             update locate
             set status = 'M', closed_date = '%s'
             where locate_id = %s
            """ % (date.Date().isodate(), loc['locate_id'])
            self.tdb.runsql(sql)

            # check locate_status
            locate_status_rows = self.tdb.getrecords("locate_status", locate_id=loc['locate_id'])
            self.assertEquals(len(locate_status_rows), 1)

        # insert a few records in the fax_queue
        for loc in (loc1, loc2, loc3):
            sql = "exec insert_fax_queue %s" % (loc['locate_id'],)
            self.tdb.runsql(sql)

        queued = self.tdb.getrecords("fax_queue")
        self.assertEquals(len(queued), 3)

        # run queue_faxes_3 for one center
        sql = "exec queue_faxes_3 'Atlanta', 1"
        rows = self.tdb.runsql_result(sql)
        # check what comes back
        self.assertEquals(rows[0]['LocatesQueued'], '1')
        self.assertEquals(rows[0]['FaxesQueued'], '1')

        # check what's in fax_message and fax_message_detail
        fmrows = self.tdb.getrecords("fax_message")
        self.assertEquals(len(fmrows), 1)
        self.assertEquals(fmrows[0]['call_center'], 'Atlanta')
        self.assertEquals(fmrows[0]['previous_queue_date'], None)
        self.assertEquals(fmrows[0]['fax_dest'], '404-361-1509')

        fmdrows = self.tdb.getrecords("fax_message_detail")
        self.assertEquals(len(fmdrows), 1)
        self.assertEquals(fmdrows[0]['fm_id'], fmrows[0]['fm_id'])
        self.assertEquals(fmdrows[0]['ticket_id'], ids[0])
        self.assertEquals(fmdrows[0]['locate_id'], loc1['locate_id'])

        # check that the right records were removed from fax_queue
        # and that others are still there
        still_queued = self.tdb.getrecords("fax_queue")
        self.assertEquals(len(still_queued), 2) # Orlando and Houston
        self.assertEquals(still_queued[0]['call_center'], 'FOL1')
        self.assertEquals(still_queued[1]['call_center'], 'FHL1')


# When this module is executed from the command-line, run all its tests
if __name__ == '__main__':

    unittest.main()

