# test_archiver.py

import copy
import os
import shutil
import site; site.addsitedir('.')
import sys
import tarfile
import tempfile
import time
import traceback
import unittest
#
import archiver
import date
import config
from mockery import *
from test_archiver import create_test_dirs, clear, delete_test_dirs, drop_file,\
                          with_config_copy

class TestArchiver2(unittest.TestCase):

    def setUp(self):
        configuration = config.getConfiguration()
        self.conf_copy = copy.deepcopy(configuration)

        try:
            delete_test_dirs(['c:\\temp\\archiver'])
        except:
            pass
        os.mkdir(os.path.join('c:/temp','archiver'))

        self.TEST_DIRECTORIES = [os.path.join("..", "testdata", "arch")]
        self.tmpdir = tempfile.mkdtemp()
        self.conf_copy.archiver['additional_dirs'] = [self.tmpdir,]
        self.TEST_DIRECTORIES.append(self.tmpdir)

        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'format': 'ABC',
          'processed': 'test_processed_ABC'},
         {'format': 'DEF',
          'processed': 'test_processed_DEF'}]

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['format','processed']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)
        clear(os.path.join('c:/temp','archiver'))

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)
        delete_test_dirs(['c:\\temp\\archiver'])

    @with_config_copy
    def test_archive_funky_path_1(self):
        '''
        Test path permutations - c:\\temp
        '''
        self.conf_copy.archiver['archive_dir'] = 'c:\\temp\\archiver'

        # ABC
        drop_file(self.conf_copy.processes[0]['processed'], "one", "2006-03-01")
        drop_file(self.conf_copy.processes[0]['processed'], "two", "2006-03-02")
        drop_file(self.conf_copy.processes[0]['processed'], "three", "2006-04-07")
        # DEF
        drop_file(self.conf_copy.processes[1]['processed'], "one", "2006-03-01")
        drop_file(self.conf_copy.processes[1]['processed'], "two", "2006-03-02")
        drop_file(self.conf_copy.processes[1]['processed'], "three", "2006-04-07")
        # Additional directory
        drop_file(self.conf_copy.archiver['additional_dirs'][0], "one",
          "2006-03-01")
        drop_file(self.conf_copy.archiver['additional_dirs'][0], "two",
          "2006-03-02")
        drop_file(self.conf_copy.archiver['additional_dirs'][0], "three",
          "2006-04-07")

        ar = archiver.Archiver(current_date="2006-04-07", verbose=0)
        ar.make = True
        ar.delete = True
        ar.archive_all(call_centers_to_archive=None)

        targetdir = self.conf_copy.archiver['archive_dir']
        tf = tarfile.open(
          os.path.join(targetdir, 'ABC-2006-04-02.tar.bz2'), 'r|bz2')
        files_in_archive = [os.path.normpath(name) for name in tf.getnames()]
        self.assertEquals(len(files_in_archive),2)
        self.assertEquals(files_in_archive[0],
          os.path.join('test_processed_ABC', 'one'))
        self.assertEquals(files_in_archive[1],
          os.path.join('test_processed_ABC', 'two'))
        tf.close()
        tf = tarfile.open(
          os.path.join(targetdir, 'DEF-2006-04-02.tar.bz2'), 'r|bz2')
        files_in_archive = [os.path.normpath(name) for name in tf.getnames()]
        self.assertEquals(len(files_in_archive), 2)
        self.assertEquals(files_in_archive[0],
          os.path.join('test_processed_DEF', 'one'))
        self.assertEquals(files_in_archive[1],
          os.path.join('test_processed_DEF', 'two'))
        tf.close()

        tf = tarfile.open(os.path.join(targetdir,
          os.path.split(self.tmpdir)[1]+'-2006-04-02.tar.bz2'), 'r|bz2')
        files_in_archive = [os.path.normpath(name) for name in tf.getnames()]
        self.assertEquals(len(files_in_archive), 2)
        self.assertEquals(files_in_archive[0],
          os.path.join(self.tmpdir, 'one')[3:])
          # Strip off the leading c:, d:, etc. from join
        self.assertEquals(files_in_archive[1],
          os.path.join(self.tmpdir,'two')[3:])
        tf.close()

    @with_config_copy
    def test_archive_funky_path_2(self):
        '''
        Test path permutations - c:/temp
        '''
        self.conf_copy.archiver['archive_dir'] = 'c:/temp/archiver'
        p0 = self.conf_copy.processes[0]['processed']
        p1 = self.conf_copy.processes[1]['processed']

        # ABC
        drop_file(p0, "one", "2006-03-01")
        drop_file(p0, "two", "2006-03-02")
        drop_file(p0, "three", "2006-04-07")
        # DEF
        drop_file(p1, "one", "2006-03-01")
        drop_file(p1, "two", "2006-03-02")
        drop_file(p1, "three", "2006-04-07")

        # Additional directory
        drop_file(self.conf_copy.archiver['additional_dirs'][0], "one",
          "2006-03-01")
        drop_file(self.conf_copy.archiver['additional_dirs'][0], "two",
          "2006-03-02")
        drop_file(self.conf_copy.archiver['additional_dirs'][0], "three",
          "2006-04-07")
        ar = archiver.Archiver(current_date="2006-04-07", verbose=0)
        ar.make = True
        ar.delete = True
        ar.archive_all(call_centers_to_archive=None)

        targetdir = self.conf_copy.archiver['archive_dir']
        tf = tarfile.open(
         os.path.join(targetdir, 'ABC-2006-04-02.tar.bz2'), 'r|bz2')
        files_in_archive = [os.path.normpath(name) for name in tf.getnames()]
        self.assertEquals(len(files_in_archive),2)
        self.assertEquals(files_in_archive[0],
          os.path.join('test_processed_ABC','one'))
        self.assertEquals(files_in_archive[1],
          os.path.join('test_processed_ABC','two'))
        tf.close()
        tf = tarfile.open(
          os.path.join(targetdir,'DEF-2006-04-02.tar.bz2'),'r|bz2')
        files_in_archive = [os.path.normpath(name) for name in tf.getnames()]
        self.assertEquals(len(files_in_archive),2)
        self.assertEquals(files_in_archive[0],
          os.path.join('test_processed_DEF','one'))
        self.assertEquals(files_in_archive[1],
          os.path.join('test_processed_DEF','two'))
        tf.close()
        tf = tarfile.open(
          os.path.join(targetdir,
          os.path.split(self.tmpdir)[1]+'-2006-04-02.tar.bz2'),'r|bz2')
        files_in_archive = [os.path.normpath(name) for name in tf.getnames()]
        self.assertEquals(len(files_in_archive),2)
        self.assertEquals(files_in_archive[0],
          os.path.join(self.tmpdir,'one')[3:])
          # Strip off the leading c:, d:, etc. from join
        self.assertEquals(files_in_archive[1],
          os.path.join(self.tmpdir,'two')[3:])
        tf.close()


if __name__ == '__main__':

    unittest.main()

