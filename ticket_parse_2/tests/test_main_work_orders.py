# test_main_work_orders.py
#
# TODO: check what happens if parsing a work order FAILS

from __future__ import with_statement
import copy
import os
import re
import shutil
import site; site.addsitedir('.')
import subprocess
import sys
import time
import unittest
import win32api
#
import attachment
import config
import emailtools
import filetools
import main
import receiver
import ticket_db
import ticketloader
import _testprep
import work_order
from formats import Lambert

from mockery import *
from test_main import drop

class TestMainWorkOrders(unittest.TestCase):

    DIRS = ["incoming", "processed", "error", "attachments", "att-processed"]

    def setUp(self):
        # make test directories
        for dir in self.DIRS:
            try:
                os.mkdir("test-" + dir + "-wo")
            except:
                pass

        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

        # make sure LAM01 exists with one client called LAMBERT01
        self._setup_LAM01()

        self.conf_copy = copy.deepcopy(config.getConfiguration())
        # set up configuration for LAM01, with separate directories
        self.conf_copy.processes = [
            {'incoming': 'test-incoming-wo',
             'format': 'LAM01',
             'processed': 'test-processed-wo',
             'error': 'test-error-wo',
             'attachments': ''}
        ]

        _testprep.delete_utility_ini()

    def _setup_LAM01(self):
        # make sure we have a call center LAM01
        _testprep.add_test_call_center(self.tdb, 'LAM01')
        # make sure it has one client called LAMBERT01
        _testprep.add_test_client(self.tdb, 'LAMBERT01', 'LAM01')

    def tearDown(self):
        # remove test directories
        for dir in self.DIRS:
            try:
                shutil.rmtree("test-" + dir + "-wo")
            except:
                pass
        _testprep.delete_utility_ini()

    def test_work_order(self):
        # grab a work order
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01-2011-03-07-00015-A01.txt"))
        raw = tl.tickets[0]
        drop("LAM01-wo-1.txt", raw, "test-incoming-wo")

        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='LAM01')
            m.run(runonce=1)

        # check that file was moved to 'processed' directory
        self.assertEquals(os.listdir('test-incoming-wo'), [])
        self.assertEquals(os.listdir('test-processed-wo'), ['LAM01-wo-1.txt'])

        # check db contents
        rows = self.tdb.getrecords('work_order')
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['client_order_num'], 'BMD982O00')
        self.assertEquals(rows[0]['wo_source'], 'LAM01')
        wo_id = rows[0]['wo_id']

        # check if we can retrieve this record through WorkOrder.load
        wo = work_order.WorkOrder.load(self.tdb, wo_id)
        self.assertEquals(wo.client_order_num, 'BMD982O00')
        self.assertEquals(wo.caller_name, 'DARRELL MIMS')

        # check that due date has been set by main.py (due date on work order
        # is not correct)
        self.assertEquals(wo.due_date, '2011-03-02 10:31:00')

        # check that we only have one client for LAM01, and that the work
        # order's client_id is set to that client's client_id
        self.assertEquals(len(m.clients['LAM01']), 1)
        self.assertEquals(m.clients['LAM01'][0].oc_code, 'LAMBERT01')
        client_id = m.clients['LAM01'][0].client_id
        self.assertEquals(wo.client_id, client_id)

        # check that we have one record in work_order_version
        wovs = self.tdb.getrecords('work_order_version')
        self.assertEquals(len(wovs), 1)

        # add another work order with the same number but different data
        raw = raw.replace('DARRELL MIMS', 'JOHN DOE')
        raw = raw.replace('BMD982O00', 'BMX0404')
        raw = raw.replace('NORMAL', 'CANCEL')
        drop("LAM01-wo-2.txt", raw, "test-incoming-wo")

        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='LAM01')
            m.run(runonce=1)

        # check that file was moved to 'processed' directory
        self.assertEquals(os.listdir('test-incoming-wo'), [])
        self.assertEquals(os.listdir('test-processed-wo'),
          ['LAM01-wo-1.txt', 'LAM01-wo-2.txt'])

        # check db contents again; original record should have been updated
        rows = self.tdb.getrecords('work_order')
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['client_order_num'], 'BMX0404')
        self.assertEquals(rows[0]['caller_name'], 'JOHN DOE')
        self.assertEquals(rows[0]['wo_source'], 'LAM01')
        self.assertEquals(rows[0]['kind'], 'CANCEL')
        self.assertEquals(rows[0]['active'], '1')
        self.assertEquals(rows[0]['wo_id'], wo_id)
        #wo_id = rows[0]['wo_id']

        # check that we have two records in work_order_version, same wo_id
        wovs = self.tdb.getrecords('work_order_version')
        self.assertEquals(len(wovs), 2)
        self.assertEquals(wovs[0]['wo_id'], wo_id)
        self.assertEquals(wovs[1]['wo_id'], wo_id)

        # add another work order, different number
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01-2011-03-07-00016-A01.txt"))
        raw = tl.tickets[0]
        drop("LAM01-wo-3.txt", raw, "test-incoming-wo")

        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='LAM01')
            m.run(runonce=1)

        # check that file was moved to 'processed' directory
        self.assertEquals(os.listdir('test-incoming-wo'), [])
        self.assertEquals(os.listdir('test-processed-wo'),
          ['LAM01-wo-1.txt', 'LAM01-wo-2.txt', 'LAM01-wo-3.txt'])

        # check db contents again; there should be two records now
        rows = self.tdb.getrecords('work_order')
        self.assertEquals(len(rows), 2)
        self.assertEquals(sorted([row['wo_number'] for row in rows]),
          ['LAMV0002897', 'LAMV0002899'])

    def test_work_order_audit(self):
        # grab a work order audit
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-order-audits", "LAM01", "LAM01-2011-03-07-00129-A01.txt"))
        raw = tl.tickets[0]
        drop("LAM01-woa-1.txt", raw, "test-incoming-wo")

        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='LAM01')
            m.run(runonce=1)

        # check that file was moved to 'processed' directory
        self.assertEquals(os.listdir('test-incoming-wo'), [])
        self.assertEquals(os.listdir('test-processed-wo'), ['LAM01-woa-1.txt'])

        # check contents of database
        rows = self.tdb.getrecords('wo_audit_header')
        self.assertEquals(len(rows), 1)
        woa_id = int(rows[0]['wo_audit_header_id'])
        self.assertEquals(rows[0]['wo_source'], 'LAM01')
        self.assertEquals(rows[0]['summary_date'], '2011-02-16 00:00:00')

        rows = self.tdb.getrecords('wo_audit_detail', wo_audit_header_id=woa_id)
        self.assertEquals(len(rows), 40)
        for row in rows:
            self.assertTrue(row['client_wo_number'].startswith('V'))

    def test_ticket_with_work_order(self):
        """ Add a work order, then have main.py process a ticket that
            references that work order. Test that main.py
            - finds this work order
            - associates the WO with the ticket (ticket_work_order table)
            - assigns the ticket's clients to the WO's locator
        """
        # set up main.py for FMB1, which will have work orders from LAM01
        self.conf_copy.processes = [
            {'incoming': 'test-incoming-wo',
             'format': 'FMW1',
             'processed': 'test-processed-wo',
             'error': 'test-error-wo',
             'attachments': ''}
        ]

        # make sure we have at least one active client for FMW1, and that
        # it's on the ticket
        _testprep.add_test_client(self.tdb, 'TRU02', 'FMW1')

        # insert a work order, manually
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01-2011-03-23-00014-A01.txt"))
        raw = tl.tickets[0]
        wop = Lambert.WorkOrderParser()
        wo = wop.parse(raw)
        self.assertEquals(wo.client_wo_number, "V0002828")
        wo.assigned_to_id = 200 # FIXME?
        wo_id = wo.insert(self.tdb)

        # grab a ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "fmw1-1.txt"))
        raw = tl.tickets[0]
        raw = raw.replace("/03", "/11") # let's make this more recent
        raw = raw.replace("CREW ON SITE",
              "CREW ON SITE; ALSO BSW WO#V0002828 AND SUCH")
        drop("FMW1-1.txt", raw, "test-incoming-wo")

        # run main.py and process this ticket
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='FMW1')
            m.run(runonce=1)

        # get ticket that has been inserted
        rows = self.tdb.getrecords('ticket', ticket_number='01034682')
        ticket_id = rows[0]['ticket_id']

        # check the ticket_work_order table
        two_rows = self.tdb.getrecords('work_order_ticket')
        self.assertEquals(len(two_rows), 1)
        self.assertEquals(int(two_rows[0]['wo_id']), int(wo_id))
        self.assertEquals(int(two_rows[0]['ticket_id']), int(ticket_id))

        # get the ticket using the pseudo-ORM
        import ticket
        tk = ticket.Ticket.load(self.tdb, ticket_id)
        for loc in tk.locates:
            if loc.client_code == 'TRU02':
                self.assertEquals(loc.status, '-R')
                self.assertEquals(loc.assigned_to, '200')
                self.assertEquals(loc.assigned_to_id, '200')

    test_ticket_with_work_order.has_error=1

    def test_ticket_with_work_order_update(self):
        # set up main.py for FMB1, which will have work orders from LAM01
        self.conf_copy.processes = [
            {'incoming': 'test-incoming-wo',
             'format': 'FMB1',
             'processed': 'test-processed-wo',
             'error': 'test-error-wo',
             'attachments': ''}
        ]

        # make sure we have at least these active client for FMB1, and that
        # they're on the tickets
        _testprep.add_test_client(self.tdb, 'BGEBA', 'FMB1')
        _testprep.add_test_client(self.tdb, 'BGEBAG', 'FMB1')
        _testprep.add_test_client(self.tdb, 'VBT', 'FMB1')

        # insert a work order, manually
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01-2011-03-23-00014-A01.txt"))
        raw = tl.tickets[0]
        raw = raw.replace("V0002828", "V0010441")
        wop = Lambert.WorkOrderParser()
        wo = wop.parse(raw)
        self.assertEquals(wo.client_wo_number, "V0010441")
        wo.assigned_to_id = 200 # FIXME?
        wo_id = wo.insert(self.tdb)

        # grab a ticket
        # locates expected on this ticket that are clients: BGEBA, BGEBAG
        path_root = os.path.join(_testprep.TICKET_PATH,
          "work-order-prelim", "wo-routing-test-data-2011-04-18")

        tl = ticketloader.TicketLoader(os.path.join(path_root,
             "FMB1-2011-04-18-03752.txt"))
        raw = tl.tickets[0]
        drop("FMB-3752.txt", raw, "test-incoming-wo")

        # run main.py and process this ticket
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='FMB1')
            m.run(runonce=1)

        # get the ticket that was just inserted
        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)
        ticket_id = tix[0]['ticket_id']

        # get the ticket using the pseudo-ORM
        import ticket
        tk = ticket.Ticket.load(self.tdb, ticket_id)
        self.assertEquals(len(tk.locates), 2) # volatile
        for loc in tk.locates:
            if loc.client_code in ('BGEBA', 'BGEBAG'):
                self.assertEquals(loc.status, '-R')
                self.assertEquals(loc.assigned_to, '200')
                self.assertEquals(loc.assigned_to_id, '200')

        # so far, so good
        # now let's drop an update for this ticket
        # has locate: VBT
        tl = ticketloader.TicketLoader(os.path.join(path_root,
             "FMB1-2011-04-18-03753.txt"))
        raw = tl.tickets[0]
        drop("FMB-3753.txt", raw, "test-incoming-wo")

        # run main.py again and process this ticket
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='FMB1')
            m.run(runonce=1)

        # get ticket again and inspect it; all locates must have been assigned
        # to the work order's locator
        tk = ticket.Ticket.load(self.tdb, ticket_id)
        self.assertEquals(len(tk.locates), 3) # volatile
        self.assertEquals([x.client_code for x in tk.locates],
                          ['BGEBA', 'BGEBAG', 'VBT'])
        for loc in tk.locates:
            self.assertEquals(loc.status, '-R')
            self.assertEquals(loc.assigned_to, '200')
            self.assertEquals(loc.assigned_to_id, '200')

    test_ticket_with_work_order_update.fails=1

    def test_work_order_audit_report(self):
        path = os.path.join(_testprep.TICKET_PATH, 'work-order-audits', 'LAM01',
               'LAM01-2011-03-23-00051-A01.txt')
        data = filetools.read_file(path, 'rb')
        drop("LAM01-zzz-audit.txt", data, "test-incoming-wo")

        re_issue_date = re.compile("<ISSUEDATE>(.*?)</ISSUEDATE>")
        standard_issue_date = "<ISSUEDATE>3/23/2011 11:25:00 PM</ISSUEDATE>"
        tix = ['00001']
        for num in tix:
            path = os.path.join(_testprep.TICKET_PATH, 'work-orders', 'LAM01',
                   'LAM01-2011-03-23-%s-A01.txt' % num)
            data = filetools.read_file(path, 'rb')
            # for now, fix the issue date to be the same as the audit date
            data = re_issue_date.sub(standard_issue_date, data)
            drop("LAM01-wo-%s.txt" % num, data, 'test-incoming-wo')

        files = os.listdir('test-incoming-wo')
        self.assertEquals(len(files), 1+len(tix))

        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            m = main.Main(verbose=0, call_center='LAM01')
            m.run(runonce=1)

        # WO V0002683 must be in work_order table
        rows = self.tdb.getrecords('work_order', client_wo_number='V0002683')
        self.assertTrue(rows)

        rows = self.tdb.runsql_result("""
         exec check_summary_by_work_order '2011-03-23', 'LAM01'
        """)
        z = [row for row in rows if row['ticket_number'] == 'V0002683']
        self.assertEquals(len(z), 1)
        self.assertEquals(int(z[0]['found']), 1)

        # work orders that we didn't store, should not be found
        z = [row for row in rows if row['ticket_number'] != 'V0002683']
        for row in z:
            self.assertEquals(int(z[0]['found']), 0)

    def test_LAM01_attachments(self):
        """ Test handling of an email containing two attachments: a work order
            in the usual XML format, and a "regular" attachment (in this case,
            an image). Pass it through Receiver and Main and check everything
            is handled properly. """

        self.tdb.delete('attachment')

        # set up receiver
        cfg = receiver.ReceiverConfiguration(self.conf_copy, 'LAM01')
        cfg.logdir = "logs"
        cfg.smtpinfo = emailtools.SMTPInfo(host='blah', from_address='blah')
        cfg.statusdir = "test-status"
        cfg.attachment_dir = 'test-stored-attachments'
        cfg.admins = []
        d = {'server': 'blah', 'username': 'foo', 'password': 'bar',
             'destination': 'test-incoming-wo',
             'call_center': 'LAM01', 'full': 1,
             'store_attachments': True,
             'attachment_dir': 'test-attachments-wo',
             'attachment_proc_dir': 'att-processed-wo',
             'file_ext': '.xml',
             'ignore_attachments': False}
        cfg.accounts.append(d)

        # get raw email
        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "LAM01-attachment-1.txt")
        data = filetools.read_file(path)
        lines = data.split('\n')

        # run Receiver.process_message(), which should produce a decoded base64
        # file
        rec = receiver.Receiver(cfg, receiver.ReceiverOptions(verbose=0))
        rec.process_message(lines, d)

        # check that files are created in the right directories
        tickets = os.listdir('test-incoming-wo')
        atts = os.listdir('test-attachments-wo')
        self.assertEquals(len(tickets), 1)
        self.assertEquals(len(atts), 1)
        self.assertTrue('A01' in atts[0])
        # contains uuencoded file; main.py needs to extract the actual file

        # set up main (so it points to the directories above with the work
        # order and the attachment)
        self.conf_copy.processes[0].update({
            'attachments': 1,
            'attachment_dir': 'test-attachments-wo',
            'attachment_proc_dir': 'test-att-processed-wo',
            'attachment_user': 200,
        })

        # set up and run custom FTP server on port 2100 (to avoid clashes on
        # build server which already uses port 21)
        ftp_dir = 'test_ftp'
        if not os.path.exists(ftp_dir):
            os.makedirs(ftp_dir)
        self.tdb.runsql("""
          update upload_location set server = '127.0.0.1',
          username = 'testuser', password = '123456789', directory = '',
          port = 2100
          where description = 'Download'
        """)
        whereami = os.path.dirname(os.path.abspath(__file__))
        ftp_path = os.path.join(whereami, "ftp_server.py")
        # note: assumes that python.exe is in path!
        ftp_server = subprocess.Popen([sys.executable, ftp_path, "2100"])
        print >> sys.stderr, "FTP server handle =", int(ftp_server._handle)
        time.sleep(2)

        # run main to process work order
        try:
            with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
                m = main.Main(verbose=0, call_center='LAM01')
                m.log.lock = 1 # suppress email
                m.run(runonce=1)
        finally:
            win32api.TerminateProcess(int(ftp_server._handle), -1)
        time.sleep(2)

        # check what happened to the WO and the attachment
        worows = self.tdb.getrecords('work_order')
        self.assertEquals(len(worows), 1)
        wo_id = worows[0]['wo_id']

        arows = self.tdb.getrecords('attachment')
        self.assertEquals(len(arows), 1)
        self.assertTrue(arows[0]['orig_filename'].endswith('.tif'))
        self.assertEquals(arows[0]['attached_by'], '200')
        self.assertEquals(arows[0]['foreign_type'], '7')
        self.assertEquals(arows[0]['foreign_id'], wo_id)

        # check directories again
        self.assertEquals(os.listdir('test-incoming-wo'), [])
        self.assertEquals(os.listdir('test-attachments-wo'), [])
        self.assertEquals(len(os.listdir('test-att-processed-wo')), 1)

        # TODO(dan) Check if file uploaded to the upload directory
        # TODO(dan) Clean up uploaded files

        # check attachment record in database
        atts = self.tdb.getrecords('attachment')
        self.assertEquals(len(atts), 1)
        # check that we stored the original filename, not the temp filename
        self.assertEquals(atts[0]['orig_filename'],
                          "v0013991_WO_201105100000135D7E927.tif")

if __name__ == "__main__":
    unittest.main()

