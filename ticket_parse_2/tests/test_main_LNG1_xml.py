# test_main_LNG1_xml.py

import copy
import glob
import os
import site; site.addsitedir('.')
import string
import StringIO
import unittest
#
import _testprep
import call_centers
import config
import datadir
import main
import ticket_db
import ticketloader
import ticketparser
import tools

from test_main import drop
from test_archiver import create_test_dirs, clear, delete_test_dirs
from mockery_tools import *

class TestMainLNG1XML(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

        # this is messy, but not easily replaced with with_call_center_copy
        self.cc = call_centers.get_call_centers(self.tdb)

        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

        # add new client code
        self.delete_test_client = []
        self.delete_call_center = []
        rows = self.tdb.getrecords('call_center', cc_code='LOR1')
        if not rows:
            self.delete_call_center.append('LOR1')
            _testprep.add_test_call_center(self.tdb,'LOR1')
        rows = self.tdb.getrecords('call_center', cc_code='LNG1')
        if not rows:
            self.delete_call_center.append('LNG1')
            _testprep.add_test_call_center(self.tdb, 'LNG1')

        rows = self.tdb.getrecords('call_center', cc_code='LWA1')
        if not rows:
            self.delete_call_center.append('LWA1')
            _testprep.add_test_call_center(self.tdb, 'LWA1')

        rows = self.tdb.getrecords('client', oc_code='NNG01', call_center='LOR1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'NNG01', 'LOR1',
                         update_call_center='LNG1')
            self.delete_test_client.append(client_id)

        rows = self.tdb.getrecords('client', oc_code='NWN02', call_center='LWA1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'NWN02', 'LWA1',
                        update_call_center='LNG1')
            self.delete_test_client.append(client_id)

        rows = self.tdb.getrecords('client', oc_code='NWN01', call_center='LOR1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'NWN01', 'LOR1',
                        update_call_center='LNG1')
            self.delete_test_client.append(client_id)

        rows = self.tdb.getrecords('client', oc_code='CFG01', call_center='LOR1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'CFG01', 'LOR1')
            self.delete_test_client.append(client_id)
        rows = self.tdb.getrecords('client', oc_code='CMCST01', call_center='LOR1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'CMCST01', 'LOR1')
            self.delete_test_client.append(client_id)
        rows = self.tdb.getrecords('client', oc_code='GTC13', call_center='LOR1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'GTC13', 'LOR1')
            self.delete_test_client.append(client_id)
        rows = self.tdb.getrecords('client', oc_code='PGE03', call_center='LOR1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'PGE03', 'LOR1')
            self.delete_test_client.append(client_id)
        rows = self.tdb.getrecords('client', oc_code='USA01', call_center='LOR1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'USA01', 'LOR1')
            self.delete_test_client.append(client_id)

        rows = self.tdb.getrecords('client', oc_code='CMCST03', call_center='LWA1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'CMCST03', 'LWA1')
            self.delete_test_client.append(client_id)
        rows = self.tdb.getrecords('client', oc_code='CPU01', call_center='LWA1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'CPU01', 'LWA1')
            self.delete_test_client.append(client_id)
        rows = self.tdb.getrecords('client', oc_code='QLNWA06', call_center='LWA1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'QLNWA06', 'LWA1')
            self.delete_test_client.append(client_id)
        rows = self.tdb.getrecords('client', oc_code='VAN01', call_center='LWA1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'VAN01', 'LWA1')
            self.delete_test_client.append(client_id)
        rows = self.tdb.getrecords('client', oc_code='WSDOT01', call_center='LWA1')
        if not rows:
            client_id = _testprep.add_test_client(self.tdb, 'WSDOT01', 'LWA1')
            self.delete_test_client.append(client_id)

        self.cc.reload()

    def prepare(self):
        # replace the real configuration with our test data
        self.conf_copy.processes = [{'incoming': 'test_incoming_LNG1',
                                     'format': 'LNG1',
                                     'processed': 'test_processed_LNG1',
                                     'error': 'test_error_LNG1',
                                     'attachments': ''},
                                    {'incoming': 'test_incoming_LOR1',
                                     'format': 'LOR1',
                                     'processed': 'test_processed_LOR1',
                                     'error': 'test_error_LOR1',
                                     'attachments': ''}]

        self.TEST_DIRECTORIES = []

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['incoming','processed', 'error']:
                direc = process.get(key, '')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)

    def tearDown(self):
        #_testprep.clear_database(self.tdb)
        for test_client in self.delete_test_client:
            _testprep.delete_test_client(self.tdb, test_client)
        for cc in self.delete_call_center:
            _testprep.delete_test_call_center(self.tdb, cc)

        delete_test_dirs(self.TEST_DIRECTORIES)

    @with_config_copy
    def test_xml_1(self):
        """
        Mantis 2148: xml ticket delivery
        We have a customer that is moving to a new system and will be sending
        us tickets in an xml format (samples attached). I noticed that when the
        receiver extracted the tickets from the test account is assigned them
        odd file names - seems like an issue reading the xml ticket. We will
        need to parse the ticket into the system and find a way to provide the
        users with a readable image so they do not need to read xml. The start
        date on this is 12/10.
        """
        self.prepare()
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "xml_tickets", "TEST-2008-10-27-00001-A01.xml"))
        raw = tl.tickets[0]

        drop("LNG1_xml.txt", raw, dir='test_incoming_LNG1')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='LNG1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(len(t2.locates), 2)
        self.assertEquals(t2.locates[0].client_code, 'NNG01')
        self.assertEquals(t2.locates[1].client_code, 'NWN01')
        # due date is calculated according to LOR1 rules, because ticket is
        # adapted from LNG1 to LOR1 (see call_center_adapters.py)
        self.assertEquals(t2.due_date, '2008-09-17 11:15:00')
        self.assertEquals(t2.work_remarks, 'CLRS TRSQ')
        self.assertEquals(t2.work_description,
          'ADD IS APX 50FT NW FROM INTER. MARK ENTIRE PROP.')
        self.assertTrue("Ticket adapted: LNG1 -> LOR1" in log)
        self.assertTrue("Exclude list for LNG1 based on LOR1: ['CFG01', 'CMCST01', 'GTC13', 'PGE03', 'USA01']" in log)

    @with_config_copy
    def test_xml_2(self):
        self.prepare()
        try:
            os.remove(os.path.join(datadir.get_datadir().path,
             'main_summaries-test.db'))
        except:
            pass

        tickets = tools.safeglob(os.path.join("..", "testdata",
         "sample_tickets", "xml_tickets", "TEST-2008-10-27*.xml"))
        # Drop all of the tickets
        for i,ticket_file in enumerate(tickets):
            f = open(ticket_file)
            raw = string.join(f.readlines(),'\n')
            f.close()
            drop("LNG1_xml_%d.txt" % (i,), raw, dir='test_incoming_LNG1')
        # Parse the lot
        m = main.Main(verbose=0, call_center='LNG1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        self.assertEquals(len(m.ids),30)
        # Make a fake Northwest summary for 2008-09-14
        rows = self.tdb.runsql_result("select ticket_number from ticket where transmit_date < '2008-09-14'")
        summ = StringIO.StringIO()
        summ.write('\n')
        summ.write('NWN01\n')
        summ.write('\n')
        summ.write('09/14/2008\n')
        summ.write('\n')
        # Write the ticket numbers 7 on a line
        def col_split(seq, size):
            return [seq[i:i+size] for i in range(0, len(seq), size)]
        tick_num_rows = col_split([(' | ' + row['ticket_number']) for row in rows],7)
        for tick_num_row in tick_num_rows:
            summ.write(string.join(tick_num_row, '') + '\n')
        summ.write('\n')
        summ.write('Total Tickets: %d' % len(rows))
        raw = summ.getvalue()
        summ.close()

        drop("LNG1_summ.txt", raw, dir = 'test_incoming_LNG1')
        # Parse the summary
        m = main.Main(verbose=0, call_center='LNG1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        self.assertTrue('6 tickets found on summary' in log)
        rows = self.tdb.runsql_result("exec check_summary_by_ticket_LNG1 '09/14/2008','LNG1'")
        # Should all be found
        [self.assertEquals(row['found'],'1') for row in rows]
        [self.assertEquals(row['client_code'],'NWN01') for row in rows]

    @with_config_copy
    def test_xml_3(self):
        '''
        2222: ticket parser is not adding locates for LNG1 tickets
        '''
        self.prepare()
        f = open(os.path.join("../testdata","sample_tickets","NWN","LNG1-2009-02-11-00464-A01.xml"))
        raw = string.join(f.readlines(),'\n')
        f.close()

        drop("LNG1_xml.txt", raw, dir = 'test_incoming_LNG1')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='LNG1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(len(t2.locates),1)
        self.assertEquals(t2.locates[0].client_code, 'NWN02')
        self.assertTrue("Ticket adapted: LNG1 -> LWA1" in log)
        self.assertTrue("Exclude list for LNG1 based on LWA1: ['CMCST03', 'CPU01', 'QLNWA06', 'VAN01', 'WSDOT01']" in log)

    @with_config_copy
    def test_check_summary_by_locate_LNG1_1(self):
        '''
        Test check_summary_by_locate_LNG1 with missing LNG1 ticket
        '''
        self.prepare()
        try:
            os.remove(os.path.join(datadir.get_datadir().path,'main_summaries-test.db'))
        except:
            pass

        # Ticket number 4065047
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","Oregon2-1.txt"))
        raw = tl.getTicket()
        drop("LOR1_1.txt", raw, dir='test_incoming_LOR1')
        # Parse the ticket
        m = main.Main(verbose=0, call_center='LOR1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        self.assertEquals(len(m.ids),1)
        # Make a fake Northwest summary for 2008-09-14
        rows = self.tdb.runsql_result("select ticket_number from ticket")
        summ = StringIO.StringIO()
        summ.write('\n')
        summ.write('NWN01\n')
        summ.write('\n')
        summ.write('04/14/2004\n')
        summ.write('\n')
        # Write the ticket numbers 7 on a line
        def col_split(seq, size):
            return [seq[i:i+size] for i in range(0, len(seq), size)]
        tick_num_rows = col_split([(' | ' + row['ticket_number']) for row in rows],7)
        for tick_num_row in tick_num_rows:
            summ.write(string.join(tick_num_row, '') + '\n')
        summ.write('\n')
        summ.write('Total Tickets: %d' % len(rows))
        raw = summ.getvalue()
        summ.close()

        drop("LNG1_summ.txt", raw, dir = 'test_incoming_LNG1')
        # Parse the summary
        m = main.Main(verbose=0, call_center='LNG1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        self.assertTrue('1 tickets found on summary' in log)
        rows = self.tdb.runsql_result("exec check_summary_by_locate_LNG1 '04/14/2004','LNG1'")
        # None should be found since the LNG1 ticket was not found
        [self.assertEquals(row['found'],'0') for row in rows]
        [self.assertEquals(row['client_code'],'NWN01') for row in rows]

    @with_config_copy
    def test_check_summary_by_locate_LNG1_2(self):
        '''
        Test check_summary_by_locate_LNG1 with LOR1 and LNG1 ticket
        '''
        self.prepare()
        try:
            os.remove(os.path.join(datadir.get_datadir().path,'main_summaries-test.db'))
        except:
            pass

        # LOR1 ticket number 4065047
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","Oregon2-1.txt"))
        raw = tl.getTicket()
        drop("LOR1_1.txt", raw, dir='test_incoming_LOR1')
        # LNG1 ticket
        f = open(os.path.join("../testdata","sample_tickets","NWN","LNG1-2009-02-11-00464-A01.xml"))
        raw = string.join(f.readlines(),'\n')
        f.close()
        raw = raw.replace('TicketNum="9029078"', 'TicketNum="4065047"')
        raw = raw.replace('2/11/09', '4/14/04')
        drop("LNG1_xml.txt", raw, dir='test_incoming_LNG1')

        # Parse the tickets (we have to run Main twice, once for each center)
        m = main.Main(verbose=0, call_center='LNG1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()
        self.assertEquals(len(m.ids), 1)

        m = main.Main(verbose=0, call_center='LOR1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()
        self.assertEquals(len(m.ids), 1)

        # Make a fake Northwest summary for 2004-04-14
        rows = self.tdb.runsql_result("select ticket_number from ticket where source = 'LNG1'")
        summ = StringIO.StringIO()
        summ.write('\n')
        summ.write('NWN02\n')
        summ.write('\n')
        summ.write('04/14/2004\n')
        summ.write('\n')
        # Write the ticket numbers 7 on a line
        def col_split(seq, size):
            return [seq[i:i+size] for i in range(0, len(seq), size)]
        tick_num_rows = col_split([(' | ' + row['ticket_number']) for row in rows],7)
        for tick_num_row in tick_num_rows:
            summ.write(string.join(tick_num_row, '') + '\n')
        summ.write('\n')
        summ.write('Total Tickets: %d' % len(rows))
        raw = summ.getvalue()
        summ.close()

        drop("LNG1_summ.txt", raw, dir='test_incoming_LNG1')
        # Parse the summary
        m = main.Main(verbose=0, call_center='LNG1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        self.assertTrue('1 tickets found on summary' in log)
        rows = self.tdb.runsql_result("exec check_summary_by_locate_LNG1 '04/14/2004','LNG1'")
        # None should be found since the LNG1 ticket was not found
        [self.assertEquals(row['found'],'1') for row in rows]
        [self.assertEquals(row['client_code'],'NWN02') for row in rows]

    @with_config_copy
    def test_check_summary_by_locate_LNG1_3(self):
        '''
        Test check_summary_by_locate_LNG1 with missing LOR1
        '''
        self.prepare()
        try:
            os.remove(os.path.join(datadir.get_datadir().path,'main_summaries-test.db'))
        except:
            pass

        # LNG1 ticket
        f = open(os.path.join("../testdata","sample_tickets","NWN","LNG1-2009-02-11-00464-A01.xml"))
        raw = string.join(f.readlines(),'\n')
        f.close()
        raw = raw.replace('TicketNum="9029078"', 'TicketNum="4065047"')
        raw = raw.replace('2/11/09', '4/14/04')
        drop("LNG1_xml.txt", raw, dir='test_incoming_LNG1')

        # Parse the ticket
        m = main.Main(verbose=0, call_center='LNG1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()
        self.assertEquals(len(m.ids),1)
        # Make a fake Northwest summary for 2004-04-14
        rows = self.tdb.runsql_result("select ticket_number from ticket where source = 'LNG1'")
        summ = StringIO.StringIO()
        summ.write('\n')
        summ.write('NWN02\n')
        summ.write('\n')
        summ.write('04/14/2004\n')
        summ.write('\n')
        # Write the ticket numbers 7 on a line
        def col_split(seq, size):
            return [seq[i:i+size] for i in range(0, len(seq), size)]
        tick_num_rows = col_split([(' | ' + row['ticket_number'])
                                   for row in rows], 7)
        for tick_num_row in tick_num_rows:
            summ.write(string.join(tick_num_row, '') + '\n')
        summ.write('\n')
        summ.write('Total Tickets: %d' % len(rows))
        raw = summ.getvalue()
        summ.close()

        drop("LNG1_summ.txt", raw, dir='test_incoming_LNG1')
        # Parse the summary
        m = main.Main(verbose=0, call_center='LNG1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        self.assertTrue('1 tickets found on summary' in log)
        rows = self.tdb.runsql_result("""
          exec check_summary_by_locate_LNG1 '04/14/2004','LNG1'
        """)
        # None should be found since the LNG1 ticket was not found
        [self.assertEquals(row['found'],'1') for row in rows]
        [self.assertEquals(row['client_code'],'NWN02') for row in rows]

if __name__ == "__main__":
    unittest.main()

