# test_0.py

import string
import unittest
import site; site.addsitedir('.')
#
import dbinterface_old as dbinterface
import ticket_db

TABLES = [
    'area',
    'asset_type',
    'call_center',
    'client',
    'county_area',
    'customer',
    'employee',
    'employee_group',
    'employee_right',
    'followup_ticket_type',
    'group_definition',
    'group_right',
    'holiday',
    'locating_company',
    'map',
    'map_cell',
    'map_page',
    'map_qtrmin',
    'map_trs',
    'office',
    'profit_center',
    'reference',
    'restricted_use_message',
    'right_definition',
    'right_restriction',
    'status_group',
    'status_group_item',
    'statuslist',
    'term_group',
    'term_group_detail',
    'ticket_ack_match',
    'upload_file_type',
    'upload_location',
    'users',
    'value_group',
    'value_group_detail',
]

N = 1 # may be set to higher value in future

class Test_0(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

    def test_tables_populated(self):
        empty = []
        for tablename in TABLES:
            c = self.tdb.count(tablename)
            if c < N:
                empty.append(tablename)

        if empty:
            a = ["The following tables should be populated but are empty:"]
            a.extend(empty)
            errmsg = string.join(a, '\n')
            self.fail(errmsg)

