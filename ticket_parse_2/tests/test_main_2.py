# test_main_2.py
# Created: 2002.11.20 HN

from __future__ import with_statement
import httplib
import os
import shutil
import site; site.addsitedir('.')
import string
import unittest
#
import _testprep
import call_centers
import config
import dbinterface_old as dbinterface
import geocoder
import mail2
import main
import mock_objects
import requests
import ticket_db
import ticketloader
import ticketparser
import ticketrouter
from mockery import *

class MockResponse:
    def __init__(self, status_code):
        self.status_code = status_code
    def json(self):
        pass

class TestMain2(unittest.TestCase):
    """ These tests simulate runs of main.py (and maybe the router) with real
        tickets, to check if there are any obvious anomalies. Should be
        valuable when testing new code or new call centers.
    """

    def send(self,xml):
        self.xml = xml

    def setUp(self):
        self.config = config.getConfiguration(reload=True)
        # replace the real configuration with our test data
        self.config.processes = [
         # one set for general testing...
         {'incoming': 'test_incoming',
          'format': '',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
         # and one for FCL2 specifically
         {'incoming': 'test_incoming_FCL2',
          'format': 'FCL2',
          'processed': 'test_processed_FCL2',
          'error': 'test_error_FCL2',
          'attachments': ''},
         # and one for FAM1
         {'incoming': 'test_incoming_FAM1',
          'format': 'FAM1',
          'processed': 'test_processed_FAM1',
          'error': 'test_error_FAM1',
          'attachments': ''},
         {'incoming': 'test_incoming',
          'format': 'FCO1',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
         {'incoming': 'test_incoming',
          'format': 'NCA2',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
        ]

        self.tdb = ticket_db.TicketDB()
        _testprep.create_test_dirs()
        _testprep.set_cc_routing_file()
        _testprep.clear_database(self.tdb)

        mail2.TESTING = 1
        mail2._test_mail_sent = []

        for process in self.config.processes:
            #incoming, name, processed, error, group = process
            _testprep.clear(process['incoming'])
            _testprep.clear(process['processed'])
            _testprep.clear(process['error'])

    def prepare(self, call_center, *more_centers):
        self.config.processes = []
        for center in [call_center] + list(more_centers):
         # one set for general testing...
         p = {'incoming': 'test_incoming',
              'format': center,
              'processed': 'test_processed',
              'error': 'test_error',
              'attachments': ''}
         self.config.processes.append(p)
         call_centers.get_call_centers(self.tdb).reload()

    def prepare2(self, call_center, geocode_latlong=0, min_precision=30):
        self.config.processes = [{
         'incoming': 'test_incoming',
         'format': call_center,
         'processed': 'test_processed',
         'error': 'test_error',
         'attachments': '',
         'geocode_latlong': geocode_latlong}]
        call_centers.get_call_centers(self.tdb).reload()
        self.config.geocoder['min_precision'] = min_precision

    def tearDown(self):
        self.config = config.getConfiguration(reload=True)

    def drop(self, filename, data):
        """ Write a file (containing one or more images) to the
            test_incoming directory. """
        fullname = os.path.join("test_incoming", filename)
        f = open(fullname, "wb")
        f.write(data)
        f.close()

    def clear_dir(self, dirname):
        """ Empty a directory. """
        filenames = os.listdir(dirname)
        for filename in filenames:
            fullname = os.path.join(dirname, filename)
            os.remove(fullname)

    ###
    ### helper function

    def _test_format(self, filename, format, all_unique=1, verbose=0):
        fullname = os.path.join(_testprep.TICKET_PATH, filename)
        target = os.path.join("test_incoming", filename)

        # get info about these tickets
        tl = ticketloader.TicketLoader(fullname)
        del tl.tickets[10:] # up to 10 tickets can stay
        # what call center are they from?
        call_center = call_centers.format2cc[format]
        _testprep.set_call_center_active(self.tdb, call_center)
        call_centers.get_call_centers(self.tdb).reload()
        self.prepare(call_center)

        # take the top ten tickets and glue them together
        data = string.join(tl.tickets, chr(12))
        with open(target, "wb") as f:
            f.write(data)

        # process tickets with main
        m = main.Main(verbose=verbose, call_center=call_center)
        #m.log.lock = 1
        m.run(runonce=1)

        # how many tickets are these in the database?
        tickets_in_db = self.tdb.getrecords("ticket",
                        ticket_format=call_center)
        if all_unique:
            self.assertEquals(len(tickets_in_db), len(tl.tickets),
             "%d tickets in file, %d in database" % (len(tl.tickets),
             len(tickets_in_db)))

        # check if client_id is set for all clients
        clients = m.clients.get(call_center, [])
        for ticket in tickets_in_db:
            ticket_id = ticket["ticket_id"]
            locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
            for locate in locates:
                for client in clients:
                    if client.client_code == locate["client_code"]:
                        client_id = client.client_id
                        self.assertEquals(locate["client_id"], client_id,
                         "Invalid client id for %s (expected: %s, real: %s)" % (
                         locate["client_code"], client_id, locate["client_id"]))

        # TODO:
        # test other stuff, like:
        # - do we have all the locates?

        # test if the due_date is always set
        for row in tickets_in_db:
            self.assert_(row.get("due_date", None),
             "due_date not set (%s, %s)" % (call_center, row["ticket_number"]))

        # TODO:
        # the same should be done for summaries, but this isn't as easy because
        # of "multipart" audits. The number of "files" found by the
        # ticketloader is not necessarily the same as the number of summaries.
        # Plus, cutting off after 10 "files" might render a multipart summary
        # incomplete.

        return m.ids

    ###
    ### the tests

    def test_Atlanta(self):
        self._test_format("Atlanta-1.txt", "Atlanta")

    def test_NewJersey(self):
        self._test_format("NewJersey-1.txt", "NewJersey")

    def test_NorthCarolina(self):
        self._test_format("NorthCarolina-1.txt", "NorthCarolina")

    def test_SouthCarolina(self):
        self._test_format("SouthCarolina-1.txt", "SouthCarolina")

    def test_Richmond1(self):
        self._test_format("Richmond1-1.txt", "Richmond1")

    def test_Richmond3(self):
        self._test_format("Richmond3-1.txt", "Richmond3")

    #def test_WestVirginia(self):
    #    self._test_format("WestVirginia-1.txt", "WestVirginia")

    def test_Pennsylvania(self):
        self._test_format("Pennsylvania-1.txt", "Pennsylvania")

    def test_Orlando(self):
        self._test_format("Orlando-1.txt", "Orlando")

    def test_Jacksonville(self):
        self._test_format("Jacksonville-1.txt", "Jacksonville")

    def test_Pensacola(self):
        self._test_format("Orlando-1.txt", "Pensacola")

    def test_Kentucky(self):
        self._test_format("Kentucky-1.txt", "Kentucky")

    def test_Dallas1(self):
        self._test_format("Dallas1-1.txt", "Dallas1")

    def test_Mississippi(self):
        self._test_format("Mississippi-1.txt", "Mississippi")

    def test_Baltimore(self):
        ids = self._test_format("Baltimore-1.txt", "Baltimore")

        # test do_not_respond_before date...
        tickets = self.tdb.getrecords('ticket')
        for row in tickets:
            self.assertEquals(row['ticket_format'], 'FMB1')
            self.assert_(row['do_not_respond_before'])

        row = [row for row in tickets if row['ticket_id'] == ids[0]][0]
        self.assertEquals(row['ticket_number'], '00356468')
        self.assertEquals(row['do_not_respond_before'], '2002-05-15 08:00:00')

        # test it for updates...
        raw = row['image']
        raw = raw.replace('05/14/02', '05/15/02')
        self.drop('bal2.txt', raw)

        # process tickets with main
        m = main.Main(verbose=0, call_center='FMB1')
        #m.log.lock = 1
        m.run(runonce=1)

        tickets = self.tdb.getrecords('ticket', ticket_number='00356468')
        self.assertEquals(len(tickets), 1)
        self.assertEquals(tickets[0]['do_not_respond_before'],
                          '2002-05-15 08:00:00')

    test_Baltimore.fails=1

    def test_Houston(self):
        ids = self._test_format("Houston1-1.txt", "Houston1", all_unique=0)
        # there are some updates, so we use all_unique=0

        # test do_not_respond_before date...
        tickets = self.tdb.getrecords('ticket')
        for row in tickets:
            self.assertEquals(row['ticket_format'], 'FHL1')
            self.assertFalse(row['do_not_respond_before'])

        row = [row for row in tickets if row['ticket_id'] == ids[0]][0]
        self.assertEquals(row['ticket_number'], '29400736')


    def test_client_id(self):
        """ Test presence of locate.client_id with various parsing and routing
            configurations. """

        CLIENT_CODE = 'POPO'
        CALL_CENTER = 'FCO1'

        # make sure client code does not exist
        self.tdb.delete('client', oc_code=CLIENT_CODE, call_center=CALL_CENTER)

        tl = ticketloader.TicketLoader('../testdata/Colorado-1.txt')
        raw = tl.tickets[0]
        raw = raw.replace('ATDW00', CLIENT_CODE)

        # drop raw ticket so main.py can find it
        self.drop('test_client_id_1.txt', raw)

        # run main.py
        m = main.Main(verbose=0, call_center=CALL_CENTER)
        #m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords('ticket', ticket_number='0458815')
        self.assertEquals(len(rows), 1)
        ticket_id = rows[0]['ticket_id']
        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assertEquals(len(locates), 1)
        self.assertEquals(locates[0]['client_code'], CLIENT_CODE)
        self.assertEquals(locates[0]['client_id'], None)
        if m._flag_non_clients:
            self.assertEquals(locates[0]['status'], '-N')
        else:
            self.assertEquals(locates[0]['status'], '-P')

        # run router...
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # get the locates again and check the values
        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assertEquals(len(locates), 1)
        self.assertEquals(locates[0]['client_code'], CLIENT_CODE)
        self.assertEquals(locates[0]['client_id'], None)
        self.assertEquals(locates[0]['status'], '-N')   # "not a client"

        # ticket should not be closed, otherwise our update won't work
        self.tdb.runsql("update locate set closed=0 where ticket_id=%s" % (
         ticket_id,))

        # add client code
        client_id = _testprep.add_test_client(self.tdb, CLIENT_CODE, CALL_CENTER)

        # change ticket so it will be updated
        raw = raw.replace('07/30/02', '07/31/02')

        # drop raw ticket so main.py can find it
        self.drop('test_client_id_2.txt', raw)

        # run main.py again
        # use a new instance (this is very important!)
        m = main.Main(verbose=0, call_center=CALL_CENTER)
        #m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords('ticket', ticket_number='0458815')
        self.assertEquals(len(rows), 1)
        ticket_id = rows[0]['ticket_id']
        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assertEquals(len(locates), 1)
        self.assertEquals(locates[0]['client_code'], CLIENT_CODE)
        self.assertEquals(locates[0]['client_id'], client_id)
        self.assertEquals(locates[0]['status'], '-P')

        # run router again
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # get the locates again and check the values
        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assertEquals(len(locates), 1)
        self.assertEquals(locates[0]['client_code'], CLIENT_CODE)
        self.assertEquals(locates[0]['client_id'], client_id)
        self.assertEquals(locates[0]['status'], '-R')   # "not a client"

    test_client_id.fails=1

    def test_insert_plat(self):
        self.prepare('NCA2')
        tl = ticketloader.TicketLoader("../testdata/NorthCaliforniaATT-1.txt")
        raw = tl.tickets[0]
        self.drop("NCA2-insert.txt", raw)

        m = main.Main(verbose=0, call_center='NCA2')
        m.log.lock = 1
        m.run(runonce=1)

        ticket_id = m.ids[-1]
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        self.assertEquals(len(locates), 1)

        plats = self.tdb.getrecords('locate_plat', locate_id=locates[0]['locate_id'])
        self.assertEquals(len(plats), 1)
        self.assertEquals(plats[0]['plat'], 'THCHCA01')

    def test_update_plat(self):
        tl = ticketloader.TicketLoader("../testdata/NorthCaliforniaATT-1.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['NorthCalifornia']) # !
        t.locates = [x for x in t.locates if x.client_code != 'PBTHAN']
        self.assertEquals(t.ticket_format, 'NCA1')
        ticket_id = self.tdb.insertticket(t) # does not have plat

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        self.assertEquals(len(locates), 5)

        # then: create "update" that does have plat
        self.drop("NCA2-update.txt", raw) # same ticket, now for NCA2

        m = main.Main(verbose=0, call_center='NCA2')
        m.log.lock = 1
        m.run(runonce=1)

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        self.assertEquals(len(locates), 6)
        loc = [x for x in locates if x['client_code'] == 'PBTHAN'][0]

        plats = self.tdb.getrecords('locate_plat', locate_id=loc['locate_id'])
        self.assertEquals(len(plats), 1)
        self.assertEquals(plats[0]['plat'], 'THCHCA01')

        # there should be no plats for other locates:
        all_plats = self.tdb.getrecords('locate_plat')
        self.assertEquals(len(all_plats), 1)

    def test_update_plat_2(self):
        # like test_update_plat, but locate that is going to have plat already
        # exists in database.
        self.prepare('NCA2')

        tl = ticketloader.TicketLoader("../testdata/NorthCaliforniaATT-1.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['NorthCalifornia']) # !
        #t.locates = [x for x in t.locates if x.client_code != 'PBTHAN']
        self.assertEquals(t.ticket_format, 'NCA1')
        ticket_id = self.tdb.insertticket(t) # does not have plat

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        self.assertEquals(len(locates), 6)

        # then: create "update" that does have plat
        self.drop("NCA2-update.txt", raw) # same ticket, now for NCA2

        m = main.Main(verbose=0, call_center='NCA2')
        m.log.lock = 1
        m.run(runonce=1)

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        self.assertEquals(len(locates), 6)
        loc = [x for x in locates if x['client_code'] == 'PBTHAN'][0]

        plats = self.tdb.getrecords('locate_plat', locate_id=loc['locate_id'])
        self.assertEquals(len(plats), 1)
        self.assertEquals(plats[0]['plat'], 'THCHCA01')

        # there should be no plats for other locates:
        all_plats = self.tdb.getrecords('locate_plat')
        self.assertEquals(len(all_plats), 1)

    def test_main_ticket_ack(self):
        self.prepare('OCC2')
        tl = ticketloader.TicketLoader("../testdata/vupsnew-1.txt")
        raw = tl.tickets[0] # emergency
        self.drop("occ-1.txt", raw)
        self.drop("occ-2.txt", raw)

        m = main.Main(verbose=0, call_center='OCC2')
        #m.log.lock = 1
        m.run(runonce=1)

        # what's in ticket_ack?
        rows = self.tdb.getrecords("ticket_ack")
        # insert_ticket_ack should *not* have been called
        self.assertEquals(rows, [])

    def test_main_ticket_ack_2(self):
        self.prepare('Atlanta')
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[4] # Emergency
        self.drop("test-1.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        m.run(runonce=1)

        # run router
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.run(runonce=1)

        # Ack the ticket ack
        sql = """
         update ticket_ack set has_ack = 1
         where ticket_id = %s
        """ % (m.ids[0],)
        self.tdb.runsql(sql)

        # change the transmit date
        raw = string.replace(raw, "02/24/04 17:01:29", "02/24/04 18:01:29")
        self.drop("test-2.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        m.run(runonce=1)

        # run router
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.run(runonce=1)
        rows = self.tdb.getrecords("ticket_ack")
        self.assertEquals(len(rows),2) # There should be two rows, 1 acked and one not
        self.assert_(int(rows[0]["has_ack"]))
        self.assert_(not int(rows[1]["has_ack"]))

    def test_main_ticket_ack_3(self):
        """
        Test ack'ing a ticket without a version
        """
        self.prepare('Atlanta')
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.tickets[4] # Emergency
        self.drop("test-1.txt", raw)

        m = main.Main(verbose=0, call_center='Atlanta')
        m.run(runonce=1)

        # Delete the ticket version
        sql = """
         delete from ticket_version
         where ticket_id = %s
        """ % (m.ids[0],)
        self.tdb.runsql(sql)

        # run router
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.run(runonce=1)

        rows = self.tdb.getrecords("ticket_ack")
        self.assertEquals(len(rows),1)

    def test_NewJersey2_service_area_code(self):
        self.prepare('NewJersey', 'NewJersey2')
        # insert NewJersey client
        _testprep.add_test_client(self.tdb, 'X11', 'NewJersey')
        _testprep.add_test_client(self.tdb, 'X12', 'NewJersey',
         update_call_center='NewJersey2')

        tl = ticketloader.TicketLoader("../testdata/NewJersey2-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace('GP4', 'X11')
        self.drop("nj1.txt", raw)

        m = main.Main(verbose=0, call_center='NewJersey')
        #m.log.lock = 1
        m.run(runonce=1)

        # verify that ticket does NOT have service_area_code
        trow = self.tdb.getrecords("ticket", ticket_number="063540056")[0]
        self.assertEquals(trow['service_area_code'], None)

        # drop same ticket, as NewJersey2, with slightly older transmit date
        raw = tl.tickets[0]
        raw = raw.replace("At: 0728", "At: 0726")
        raw = raw.replace('GP4', 'X12')
        self.drop("nj2.txt", raw)

        m = main.Main(verbose=0, call_center='NewJersey2')
        #m.log.lock = 1
        m.run(runonce=1)

        # verify that ticket DOES have service_area_code
        rows = self.tdb.getrecords("ticket", ticket_number="063540056")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['service_area_code'], 'JC')

    def test_NCA_due_date_1(self, update_due_date=True):
        # Mantis 2828
        # note: this should work even if the transmit date of the update <= that
        # of the original ticket. =/
        self.prepare('NCA1', 'NCA2')

        # grab an NCA1 ticket, process it through main, check due date
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "NCA1-2010-03-26", "TEST-2010-02-01-00007.txt"))
        raw = tl.tickets[0]
        raw = raw.replace('02/01/10', '01/27/10') # fix transmit date
        self.drop("nca1-1.txt", raw)

        m = main.Main(verbose=0, call_center='NCA1')
        m.run(runonce=1)

        # check dates; due date should be set according to NCA1 rules
        rows = self.tdb.getrecords('ticket')
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['ticket_number'], '0000038')
        self.assertEquals(rows[0]['call_date'], '2010-01-27 14:37:00')
        self.assertEquals(rows[0]['work_date'], '2010-01-29 14:45:00')
        self.assertEquals(rows[0]['transmit_date'], '2010-01-27 16:09:24')
        self.assertEquals(rows[0]['due_date'], '2010-01-29 17:00:00')
        self.assertEquals(rows[0]['con_zip'], '95624')

        # there should be only one version of the ticket right now
        vrows = self.tdb.getrecords('ticket_version')
        self.assertEquals(len(vrows), 1)

        # do the same with an NCA2 ticket that updates the previous one
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "NCA2-2010-07-01-remarks", "NCA2-2010-07-01-01100.txt")) # normal ticket
        raw = tl.tickets[0]
        # massage the ticket so it has the same ticket number, etc
        # transmit date of update is < transmit date of original, but the due
        # date is updated anyway.
        raw = raw.replace('0069852', '0000038')
        raw = raw.replace('07/01/10', '01/27/10') # includes call_date, 1/27 is a wednesday
        if update_due_date:
            raw = raw.replace('03/18/10 at 12:00', '01/30/10 at 16:00') # work_date
        else:
            raw = raw.replace('03/18/10 at 12:00', '01/29/10 at 17:30') # work_date
        self.drop('nca2-1.txt', raw)

        m = main.Main(verbose=0, call_center='NCA2')
        m.run(runonce=1)

        # there should be 2 records in ticket_version now
        vrows = self.tdb.getrecords('ticket_version')
        self.assertEquals(len(vrows), 2)

        # check the due date (should have been updated)
        rows = self.tdb.getrecords('ticket')
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['ticket_number'], '0000038')
        # call date should be still the same (this is not a full update where
        # the dates are updated as well)
        self.assertEquals(rows[0]['call_date'], '2010-01-27 14:37:00')
        self.assertEquals(rows[0]['work_date'], '2010-01-29 14:45:00')
        # make sure it has the NCA2 due date
        if update_due_date:
            self.assertEquals(rows[0]['due_date'], '2010-01-29 17:00:00')
        else:
            self.assertEquals(rows[0]['due_date'], '2010-01-29 17:00:00')

    def test_NCA_due_date_2(self):
        # run the same tests again, but this time make the work date < call
        # date + 48h to 7PM
        self.test_NCA_due_date_1(update_due_date=False)

    def test_hp_info(self):
        # Mantis #2869: insert a ticket with HP info; verify that we have
        # ticket_hp records with the correct data.
        self.prepare('3004')

        # grab a ticket, process it through main, check due date
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "3004-2011-08-agl", "TEST-2011-08-03-00005.txt"))
        raw = tl.tickets[0]
        self.drop("3004-agl-1.txt", raw)

        m = main.Main(verbose=0, call_center='3004')
        m.run(runonce=1)

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)
        ticket_id = tix[0]['ticket_id']

        thprows = self.tdb.getrecords('ticket_hp', ticket_id=ticket_id)
        self.assertEquals(len(thprows), 29)
        self.assertEquals(thprows[0]['data'],
         'AGL108-HP : Bare Steel    24-300-S')

    def test_timezone(self):
        # Mantis #2856 -- add time zone info to call_center table
        # make sure we have time zones in reference table
        #self.tdb.runsql(TIMEZONE_SQL)
        ref = self.tdb.getrecords('reference', type='timezone', code='EST/EDT')
        # then set all call centers to EST/EDT (for now)
        self.tdb.runsql("""
          update call_center
          set time_zone = %d
        """ % (int(ref[0]['ref_id'])), )

        cc = call_centers.get_call_centers(self.tdb)
        cc.reload() # make sure we have up-to-date call center data
        atl = cc.get('Atlanta')
        self.assertEquals(atl['cc_code'], 'Atlanta')
        self.assertEquals(atl['timezone_code'], 'EST/EDT')

    def _parse_NewJersey_with_geocoder(self, call_center, http_responses,
     ticket_file):
        self.prepare2(call_center, geocode_latlong=1, min_precision=30)

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
         'tickets', 'NewJersey-2014-01', ticket_file))
        self.drop(ticket_file, tl.tickets[0])

        m = main.Main(verbose=0, call_center=call_center)

        mock_objects.attempt = 0

        def get(*args, **kwargs):
            mock_objects.attempt += 1
            return MockResponse(http_responses[mock_objects.attempt - 1][0])
        def json(*args, **kwargs):
            return http_responses[mock_objects.attempt - 1][1]

        with Mockery(requests, 'get', get), \
         Mockery(MockResponse, 'json', json):
            m.run(runonce=1)

    def _verify_expected_values(self, ticket, expected_values):
        for fld in expected_values:
            self.assertEquals(ticket[fld], expected_values[fld])

    def _verify_mail_sent(self, message_num, changed_fields, message_segments=[]):
        for fld, old, new in changed_fields:
            self.assertTrue('old ' + fld + ': ' + str(old) in
             mail2._test_mail_sent[message_num][2])
            self.assertTrue('new ' + fld + ': ' + str(new) in
             mail2._test_mail_sent[message_num][2])
        for seg in message_segments:
            self.assertTrue(seg in mail2._test_mail_sent[message_num][2])

    def test_geocoder_1(self):
        # Status = "OK". 1 match.
        json1 = {u'status': u'OK', u'results': [
         {u'geometry': {u'location_type': u'RANGE_INTERPOLATED',
         u'location': {u'lat': 40.0167542, u'lng': -74.1415494}}}]}

        # Initialize ticket with street address, and no lat/long. First geocode
        # attempt fails with status 500.
        self._parse_NewJersey_with_geocoder('NewJersey', [(500, None),
         (200, json1)], 'NewJersey-2014-01-06-05-26-25-012-CWTN3.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify geocoding is correct
        expected_values = {'work_lat': '40.016754', 'work_long': '-74.141549',
         'geocode_precision': str(geocoder.precision_translations[
         'RANGE_INTERPOLATED']), 'work_address_street': 'DREW PL',
         'map_page': '4001D7408C'}
        self._verify_expected_values(tix[0], expected_values)
        self.assertTrue(len(mail2._test_mail_sent) == 0)

        # Update ticket from update call center. Update has modified
        # work_address_street, and lat/long added
        self._parse_NewJersey_with_geocoder('NewJersey2', [],
         'NewJersey-2014-01-06-05-26-25-012-CWTN3-modified2.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify geocoding and work_address_street didn't change 
        self._verify_expected_values(tix[0], expected_values)
        self.assertTrue(len(mail2._test_mail_sent) == 0)

        # Update ticket from main call center. Update has modified
        # work_address_street
        self._parse_NewJersey_with_geocoder('NewJersey', [],
         'NewJersey-2014-01-06-05-26-25-012-CWTN3-modified1.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify work_address_street updated, and warning message sent 
        expected_values['work_address_street'] = 'MODIFIED DREW PL'
        self._verify_expected_values(tix[0], expected_values)
        self._verify_mail_sent(0,
         [('work_address_street', 'DREW PL', 'MODIFIED DREW PL')])

        # Update ticket from main call center. Update has lat/long added, that's
        # different from the geocoded values.
        self._parse_NewJersey_with_geocoder('NewJersey', [],
         'NewJersey-2014-01-06-05-26-25-012-CWTN3-modified2.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify lat, long, and geocode_precision updated, and warning message
        # sent. 
        expected_values['work_lat'] = '40.19759'
        expected_values['work_long'] = '-74.028701'
        expected_values['geocode_precision'] = \
         str(ticket_db.blank_geocode_precision)
        expected_values['map_page'] = '4011A7401B'
        self._verify_expected_values(tix[0], expected_values)
        self._verify_mail_sent(1,
         [('work_lat', '40.016754', '40.19759'), ('map_page', '4001D7408C',
         '4011A7401B')])

    def test_geocoder_2(self):
        # Status = "OK". 2 matches.
        json1 = {u'status': u'OK', u'results': [
         {u'geometry': {u'location_type': u'APPROXIMATE',
         u'location': {u'lat': 40.0167999, u'lng': -74.1415999}}},
         {u'geometry': {u'location_type': u'ROOFTOP',
         u'location': {u'lat': 40.0167542, u'lng': -74.1415494}}}]}

        # Initialize ticket with cross streets, and no lat/long
        self._parse_NewJersey_with_geocoder('NewJersey', [(200, json1)],
         'NewJersey-2014-01-13-09-05-51-118-2GVMS.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify geocoding is correct
        expected_values = {'work_lat': '40.016754', 'work_long': '-74.141549',
         'geocode_precision': str(geocoder.precision_translations[
         'ROOFTOP']), 'work_address_street': 'OLD FREEHOLD RD',
         'map_page': '4001D7408C'}
        self._verify_expected_values(tix[0], expected_values)
        self.assertTrue(len(mail2._test_mail_sent) == 0)

        # Update ticket from main call center. Update has lat/long added, that's
        # the same as the geocoded values.
        self._parse_NewJersey_with_geocoder('NewJersey', [],
         'NewJersey-2014-01-13-09-05-51-118-2GVMS-modified1.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify geocoded values haven't changed, except for geocode_precision
        expected_values['geocode_precision'] = \
         str(ticket_db.blank_geocode_precision)
        self._verify_expected_values(tix[0], expected_values)
        self._verify_mail_sent(0, [('geocode_precision',
         geocoder.precision_translations['ROOFTOP'], 
         ticket_db.blank_geocode_precision)])

    def test_geocoder_3(self):
        # Status = "ZERO_RESULTS". There shouldn't be a retry.
        json1 = {u'status': u'ZERO_RESULTS', u'results': []}
        json2 = {u'status': u'OK', u'results': [
         {u'geometry': {u'location_type': u'RANGE_INTERPOLATED',
         u'location': {u'lat': 40.0167542, u'lng': -74.1415494}}}]}

        # Initialize ticket
        self._parse_NewJersey_with_geocoder('NewJersey', [(200, json1),
         (200, json2)], 'NewJersey-2014-01-13-09-05-51-118-2GVMS.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify ZERO_RESULTS recorded correctly
        expected_values = {'work_lat': '0', 'work_long': '0',
         'geocode_precision': '0', 'work_address_street': 'OLD FREEHOLD RD',
         'map_page': ''}
        self._verify_expected_values(tix[0], expected_values)
        self.assertTrue(len(mail2._test_mail_sent) == 0)

        # Update ticket from main call center. Update has no changes.
        self._parse_NewJersey_with_geocoder('NewJersey', [],
         'NewJersey-2014-01-13-09-05-51-118-2GVMS.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify nothing changed
        self._verify_expected_values(tix[0], expected_values)
        self.assertTrue(len(mail2._test_mail_sent) == 0)

    def test_geocoder_4(self):
        json1 = {u'status': u'OK', u'results': [
         {u'geometry': {u'location_type': u'RANGE_INTERPOLATED',
         u'location': {u'lat': 40.0167542, u'lng': -74.1415494}}}]}

        # Initialize ticket. Geocoding should fail without retrying.
        self._parse_NewJersey_with_geocoder('NewJersey',
         [(geocoder.no_retry_statuses[0], {}), (200, json1)], 'NewJersey-2014-01-13-09-05-51-118-2GVMS.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify geocoding failed
        expected_values = {'work_lat': '0', 'work_long': '0',
         'geocode_precision': None, 'work_address_street': 'OLD FREEHOLD RD',
         'map_page': ''}
        self._verify_expected_values(tix[0], expected_values)
        self.assertTrue(len(mail2._test_mail_sent) == 0)

    def test_geocoder_5(self):
        json1 = {u'status': u'OVER_QUERY_LIMIT', u'results': []}
        json2 = {u'status': u'OK', u'results': [
         {u'geometry': {u'location_type': u'RANGE_INTERPOLATED',
         u'location': {u'lat': 40.0167542, u'lng': -74.1415494}}}]}

        # Initialize ticket. Geocoding should fail without retrying.
        self._parse_NewJersey_with_geocoder('NewJersey',
         [(200, json1), (200, json2)], 'NewJersey-2014-01-13-09-05-51-118-2GVMS.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify geocoding failed, and an OVER_QUERY_LIMIT message was sent
        expected_values = {'work_lat': '0', 'work_long': '0',
         'geocode_precision': None, 'work_address_street': 'OLD FREEHOLD RD',
         'map_page': ''}
        self._verify_expected_values(tix[0], expected_values)
        self._verify_mail_sent(0, [], ['OVER_QUERY_LIMIT'])

    def test_geocoder_6(self):
        json1 = {u'status': u'OK', u'results': [
         {u'geometry': {u'location_type': u'RANGE_INTERPOLATED',
         u'location': {u'lat': 40.0167542, u'lng': -74.1415494}}}]}

        # Initialize ticket. Geocoding should fail after max retries.
        self._parse_NewJersey_with_geocoder('NewJersey',
         [(500, None), (500, None), (500, None), (500, None), (200, json1)], 'NewJersey-2014-01-13-09-05-51-118-2GVMS.txt')

        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # Verify geocoding failed
        expected_values = {'work_lat': '0', 'work_long': '0',
         'geocode_precision': None, 'work_address_street': 'OLD FREEHOLD RD',
         'map_page': ''}
        self._verify_expected_values(tix[0], expected_values)
        self.assertTrue(len(mail2._test_mail_sent) == 0)

    def test_disabling_event_log(self):
        # Verify no event_log records exist
        events = self.tdb.runsql_result("select * from event_log")
        self.assertEquals(len(events), 0)

        self.prepare('NCA1')

        # Grab an NCA1 ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
         "tickets", "NCA1-2013-08-28-qtrmin",
         "NCA1-2013-08-22-17-40-24-783-39EUE.txt"))
        self.drop("nca1-1.txt", tl.tickets[0])

        # todo(dan) Disable event logging, then process ticket through main
        self.config.store_events = 0
        m = main.Main(verbose=0, call_center='NCA1')
        m.run(runonce=1)

        # Get ticket record, and verify ticket_number
        tickets = self.tdb.getrecords('ticket')
        self.assertEquals(len(tickets), 1)
        self.assertEquals(tickets[0]['ticket_number'], '0327669')

        # Verify no event_log records were inserted
        events = self.tdb.runsql_result("select * from event_log")
        self.assertEquals(len(events), 0)

    def test_ticket_received_event(self):
        self.prepare('NCA1')

        # Grab an NCA1 ticket, process it through main
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
         "tickets", "NCA1-2013-08-28-qtrmin",
         "NCA1-2013-08-22-17-40-24-783-39EUE.txt"))
        self.drop("nca1-1.txt", tl.tickets[0])

        m = main.Main(verbose=0, call_center='NCA1')
        m.run(runonce=1)

        # Get ticket record, and verify some values
        tickets = self.tdb.getrecords('ticket')
        self.assertEquals(len(tickets), 1)
        self.assertEquals(tickets[0]['ticket_number'], '0327669')
        ticket_id = tickets[0]['ticket_id']

        # Get locates for ticket, and verify total count
        locates = self.tdb.runsql_result("""
         select * from locate
         where ticket_id = %s
         order by locate_id
         """ % (ticket_id))
        self.assertEquals(len(locates), 7)

        # Get client record for NCA1/COMDSP
        clients = self.tdb.runsql_result("""
         select * from client
         where call_center = 'NCA1' and oc_code = 'COMDSP'
         """)
        self.assertEquals(len(clients), 1)

        # Get event_log record for TicketReceived event. It should be the first
        # event for this ticket.
        events = self.tdb.runsql_result("""
         select * from event_log
         where aggregate_type = 1 and aggregate_id = %s and version_num = 1
         """ % (ticket_id))
        self.assertEquals(len(events), 1)

        # Build the expected event_data

        aggregate_key = '{"rec":{"1":{"i32":1},"2":{"i32":%s}}}' % (ticket_id)

        locs = '{"1":{"i32":%s},"3":{"str":"AGASPL"}}' % (
         locates[0]['locate_id'])
        locs += ',{"1":{"i32":%s},"3":{"str":"CTYPLA"}}' % (
         locates[1]['locate_id'])
        locs += ',{"1":{"i32":%s},"2":{"i32":%s},"3":{"str":"COMDSP"},"4":{"str":"%s"}}'
        locs = locs % (locates[2]['locate_id'], locates[2]['client_id'],
         clients[0]['client_name'])
        locs += ',{"1":{"i32":%s},"3":{"str":"ELDIRR"}}' % (
         locates[3]['locate_id'])
        locs += ',{"1":{"i32":%s},"3":{"str":"JSWELD"}}' % (
         locates[4]['locate_id'])
        locs += ',{"1":{"i32":%s},"3":{"str":"PACBEL"}}' % (
         locates[5]['locate_id'])
        locs += ',{"1":{"i32":%s},"3":{"str":"PGEPLA"}}' % (
         locates[6]['locate_id'])
        locate_list = '{"lst":["rec",7,' + locs + ']}'

        ticket_received = '{"rec":{"5":{"rec":{"1":{"str":"0327669"},' +\
         '"2":{"str":"NCA1"},"3":{"str":"COMDSP"},"4":{"str":"NORMAL"},' +\
         '"5":{"str":"NORMAL NOTICE"},"6":{"str":"2013-08-22 14:38:04"},' +\
         '"7":{"str":"2013-08-26 17:00:00"},"8":' + locate_list + '}}}}'

        event_data = '{"2":' + aggregate_key + ',"3":' + ticket_received + '}'

        # Verify the event_data
        self.assertEquals(events[0]['event_data'], event_data)


if __name__ == "__main__":

    unittest.main()

