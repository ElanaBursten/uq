# test_summarycollection.py
#
# NOTE: If this test unit causes "insecure string pickle" errors, look for a
# file called test1.db and remove it.

import os
import site; site.addsitedir('.')
import unittest
#
import datadir
import summarycollection
import ticketparser
import ticketloader
import tools
import ticket_db

class TestSummaryCollection(unittest.TestCase):

    def setUp(self):
        self.handler = ticketparser.TicketHandler()
        self.tdb = ticket_db.TicketDB()
        self.tdb.runsql('delete from summary_fragment')

    def testAtlantaSummaries(self):
        self.sc = summarycollection.SummaryCollection(self.tdb,
         'Atlanta', ['Atlanta'], self.handler.parse)

        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt", chr(12))

        # the first summary is single-part and therefore complete...
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ['Atlanta'])
        self.sc.add(summ)

        c = self.sc.find_complete()
        self.assertEquals(len(c), 1)
        self.sc.remove_summaries_from_db(c[0][0], c[0][1])
        self.sc.remove_summaries_from_dict(c[0][0], c[0][1])

        # then we have a multipart summary (2 parts)...
        summ = self.handler.parse(tl.getTicket())
        self.sc.add(summ)

        c = self.sc.find_complete()
        self.assertEquals(len(c), 0)    # not complete yet

        summ = self.handler.parse(tl.getTicket())
        self.sc.add(summ)

        c = self.sc.find_complete()
        self.assertEquals(len(c), 1)    # now it should be complete
        call_center, client_code = c[0]

        parts = self.sc.get(call_center, client_code)
        self.assertEquals(len(parts), 2)    # 2 parts
        self.assertEquals(parts[-1].expected_tickets, len(parts[0].data) +
         len(parts[1].data))
        self.assert_(self.sc.tickets_complete(parts))

    def testAtlantaSummaries_2(self):
        self.sc = summarycollection.SummaryCollection(self.tdb,
         'Atlanta', ['Atlanta'], self.handler.parse)

        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt", chr(12))

        raw = tl.getTicket()
        summ = self.handler.parse(raw, ['Atlanta'])
        self.sc.add(summ)
        self.assertTrue(self.sc.mark_complete(None) == None)
        self.assertTrue(self.sc.tickets_complete(None) == 0)

    ### XXX Test one with New Jersey
    def testNewJerseySummaries_1(self):
        self.sc = summarycollection.SummaryCollection(self.tdb,
         'NewJersey', ['NewJersey'], self.handler.parse)
        tl = ticketloader.TicketLoader("../testdata/sum-NewJersey.txt", "~~~~~")

        for raw in tl.tickets:
            summ = self.handler.parse(raw, ["NewJersey"])
            self.assertEquals(summ.call_center, "NewJersey")
            self.sc.add(summ)

        parts = self.sc.get("NewJersey", "NJN")
        self.assertEquals(len(parts), 4)
        self.assertEquals(self.sc.iscomplete("NewJersey", "NJN"), 1)
        self.assert_(self.sc.tickets_complete(parts))

    def testNewJerseySummaries_2(self):
        '''
        2287: New Jersey audits failing to parse
        NJ audits can now be multipart, arriving in some random order.
        '''
        self.sc = summarycollection.SummaryCollection(self.tdb,
         'NewJersey', ['NewJersey'], self.handler.parse)

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","NewJersey2","GSU1-2009-04-28-00036.txt"))
        raw = tl.getTicket()

        # then we have a multipart summary (2 parts)...
        summ = self.handler.parse(raw, ["NewJersey"])
        self.sc.add(summ)

        c = self.sc.find_complete()
        self.assertEquals(len(c), 0)    # not complete yet

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","NewJersey2","GSU1-2009-04-28-00039.txt"))
        raw = tl.getTicket()

        # then we have a multipart summary (2 parts)...
        summ = self.handler.parse(raw, ["NewJersey"])
        self.sc.add(summ)

        c = self.sc.find_complete()
        self.assertEquals(len(c), 1)    # now it should be complete
        call_center, client_code = c[0]

        parts = self.sc.get(call_center, client_code)
        self.assertEquals(len(parts), 2)    # 2 parts
        self.assert_(self.sc.tickets_complete(parts))

    def testLCC1Summaries(self):
        self.sc = summarycollection.SummaryCollection(self.tdb,
         'LCC1', ['Oregon3'], self.handler.parse)

        # the first summary is single-part and therefore complete...
        tl = ticketloader.TicketLoader("../testdata/audits/LCC1-2016-02/LCC1-2016-02-23-16-55-09-854-NQMMY.txt", chr(12))
        summ = self.handler.parse(tl.getTicket(), ['Oregon3'])
        self.sc.add(summ)

        c = self.sc.find_complete()
        self.assertEquals(len(c), 1)
        self.sc.remove_summaries_from_db(c[0][0], c[0][1])
        self.sc.remove_summaries_from_dict(c[0][0], c[0][1])

        # then we have a multipart summary (2 parts)...
        tl = ticketloader.TicketLoader("../testdata/audits/LCC1-2016-02/LCC1-2016-02-23-16-55-09-854-NQMMY-modified1.txt", chr(12))
        summ = self.handler.parse(tl.getTicket(), ['Oregon3'])
        self.sc.add(summ)

        c = self.sc.find_complete()
        self.assertEquals(len(c), 0)    # not complete yet

        tl = ticketloader.TicketLoader("../testdata/audits/LCC1-2016-02/LCC1-2016-02-23-16-55-09-854-NQMMY-modified2.txt", chr(12))
        summ = self.handler.parse(tl.getTicket(), ['Oregon3'])
        self.sc.add(summ)

        c = self.sc.find_complete()
        self.assertEquals(len(c), 1)    # now it should be complete
        call_center, client_code = c[0]

        parts = self.sc.get(call_center, client_code)
        self.assertEquals(len(parts), 2)    # 2 parts
        self.assertEquals(parts[-1].expected_tickets, len(parts[0].data) +
         len(parts[1].data))
        self.assert_(self.sc.tickets_complete(parts))

    ### Test reloading of existing collection of data
    def testReload(self):
        self.tdb.runsql('delete from summary_fragment')
        self.sc = summarycollection.SummaryCollection(self.tdb,
         'Atlanta', ['Atlanta'], self.handler.parse)
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt", chr(12))

        # the first summary is single-part and therefore complete...
        raw = tl.getTicket()
        summ = self.handler.parse(raw)
        # save client_code and call_center, so we can find it later
        client_code = summ.client_code
        call_center = summ.call_center
        self.sc.add(summ)

        # create new instance and check if this summary
        # is still there -- it should be
        self.sc = summarycollection.SummaryCollection(self.tdb,
         'Atlanta', ['Atlanta'], self.handler.parse)
        z = self.sc.get(call_center, client_code)
        self.assert_(len(z) >= 1)

        # we don't need it anymore, so remove it
        self.sc.remove_summaries_from_db(call_center, client_code)
        self.sc.remove_summaries_from_dict(call_center, client_code)


    ### XXX Test other parts of collection... what happens if it's empty,
    ### does get() return empty list, etc.




if __name__ == "__main__":

    unittest.main()
