# test_work_order_version.py

import os
import site; site.addsitedir('.')
import unittest
#
from formats import Lambert
import ticket_db
import ticketloader
from work_order import WorkOrder
from work_order_version import WorkOrderVersion
import _testprep

class TestWorkOrderVersion(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

    def test_add_version(self):
        # grab a work order, parse it, and insert it
        path = os.path.join(_testprep.TICKET_PATH,
               "work-orders", "LAM01", "LAM01-2011-03-07-00009-A01.txt")
        tl = ticketloader.TicketLoader(path)
        raw = tl.tickets[0]
        wop = Lambert.WorkOrderParser()
        wo = wop.parse(raw)
        wo._filedate = '2011-03-07 12:30:00'
        # ^ add manually; this is normally added by main.py
        wo_id = wo.insert(self.tdb)

        self.assertTrue(wo_id)

        # insert a work order version record for it
        path = path[:100]
        wov_id = WorkOrderVersion.add_version(self.tdb, wo_id, wo, path)
        self.assertTrue(wov_id)

        # add another version
        wo.work_type = 'BLAH'
        wov_id_2 = WorkOrderVersion.add_version(self.tdb, wo_id, wo, path)

        wovs = self.tdb.getrecords('work_order_version')
        self.assertEquals(len(wovs), 2)
        self.assertEquals(wovs[0]['wo_id'], wo_id)
        self.assertEquals(wovs[1]['wo_id'], wo_id)


if __name__ == "__main__":
    unittest.main()
