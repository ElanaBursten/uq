# backend_tst_case.py

import os
import poplib
import shutil
import sys
import unittest
import windows_tools
#
import _testprep
import abstractresponder
import config
import datadir
import dbinterface_ado
import emailresponder
import filetools
import mail2
import main
import mock_pop3
import ticket_db
import tst_config
from mockery import *

TEST_DIRECTORY_PATHS = ['in', 'proc', 'error', 'ack_in', 'ack_proc', 'in_2', 'proc_2', 'error_2',
                        'ack_in_2', 'ack_proc_2']
DEFAULT_INCOMING_PATH = 'in'
TEST_CONFIG_FILENAME = 'test_config.xml'

module_dbado = None


class TestConfigManager(object):
    def __enter__(self):
        self.original_configuration = config.getConfiguration()
        config.setConfiguration(config.Configuration(filename=TEST_CONFIG_FILENAME))
        return self

    def __exit__(self, ret_value, value, tb):
        config.setConfiguration(self.original_configuration)
        return ret_value is None  # reraise exception if there was any


def setup_module():
    global module_dbado
    module_dbado = dbinterface_ado.DBInterfaceADO()


class BackendTestCase(unittest.TestCase):
    def setUp(self):
        for path in TEST_DIRECTORY_PATHS:
            full_path = os.path.join(datadir.datadir.path, path)

            if os.path.isdir(full_path):
                shutil.rmtree(full_path)
            try:
                os.mkdir(full_path)
            except:
                raise

        mail2._test_mail_sent = []
        mock_pop3.POP3_ACCOUNTS = {}

        self.tdb = ticket_db.TicketDB(dbado=module_dbado)
        _testprep.clear_database(self.tdb)

        self.test_config = tst_config.TestConfig()

    def tearDown(self):
        pass

    def add_incoming_files(self, source_path, filenames, incoming_path=DEFAULT_INCOMING_PATH):
        if not os.path.isabs(source_path):
            source_path = os.path.join(_testprep.TICKET_PATH, source_path)
        if not os.path.isabs(incoming_path):
            incoming_path = os.path.join(datadir.datadir.path, incoming_path)

        filetools.copy_files(source_path, incoming_path, filenames)

    def add_pop3_account(self, server, username):
        mock_pop3.add_account(server, username)

    def add_pop3_message_files(self, source_path, filenames, server, username):
        if not os.path.isabs(source_path):
            source_path = os.path.join(_testprep.TICKET_PATH, source_path)
        mock_pop3.add_messages_to_account(server, username, filenames, source_path)

    def run_parser(self, call_center):
        with TestConfigManager():
            m = main.Main(call_center=call_center, verbose=0)
            m.log.lock = 1
            m.run(runonce=1)

    def run_email_responders(self):
        with TestConfigManager(), Mockery(sys, 'argv', ['']), \
                Mockery(windows_tools, 'setconsoletitle', mock_dummy),\
                Mockery(poplib, 'POP3', mock_pop3.MockPOP3),\
                Mockery(poplib, 'POP3_SSL', mock_pop3.MockPOP3_SSL),\
                Mockery(mail2, 'TESTING', 1):
            abstractresponder.run_toplevel(emailresponder.EmailResponder)

    def status_ticket(self, ticket_number, client_code, status):
        module_dbado.runsql("""
            update locate set status = '%s' where client_code = '%s' and ticket_id = (
                select ticket_id from ticket where ticket_number = '%s')""" % (
            status, client_code, ticket_number))

    def verify_email_responders_config(self, expected_config=None):
        with TestConfigManager():
            if expected_config is None:
                # use to help create config
                print >> sys.stderr, 'expected_config =', config.getConfiguration().emailresponders
            else:
                self.assertEquals(config.getConfiguration().emailresponders, expected_config)

    def verify_results(self, sql, result_fields=None, expected_results=None):
        rows = module_dbado.runsql_result(sql)
        if (result_fields is None) and rows:
            # use to help create result_fields list
            result_fields = rows[0].keys()
            print >> sys.stderr, 'result_fields =', result_fields

        results = []
        for row in rows:
            result_row = []
            for fld in result_fields:
                result_row.append(row[fld])
            results.append(result_row)
        if expected_results is None:
            # use to help create expected_results list
            print >> sys.stderr, 'expected_results =', results
        else:    
            self.assertEquals(results, expected_results)
