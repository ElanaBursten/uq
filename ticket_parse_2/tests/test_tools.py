# test_tools.py
# Test some functions in the tools.py module.
# Created: 05 Mar 2002, Hans Nowak
# Last update: 22 Mar 2002, Hans Nowak

import os
import string
import unittest
import site; site.addsitedir('.')
#
import _testprep
import filetools
import tools
import ticket_db

class TestTools(unittest.TestCase):

    def tearDown(self):
        filetools.remove_file("foo")
        filetools.remove_file("bar")

    def test_grid2decimal(self):
        self.assertEquals(tools.grid2decimal("3330A8430A"),
         (33 + (30.875/60), 84 + (30.875/60)))
        self.assertEquals(tools.grid2decimal("3339D8422D"),
         (33 + 39.125/60, 84 + 22.125/60))
        self.assertEquals(tools.grid2decimal("3341C8424B"),
         (33+ 41.375/60, 84 + 24.625/60))
        self.assertEquals(tools.grid2decimal("3359A8459C"),
         (33 + 59.875/60, 84 + 59.375/60))
        # does the asterisk work? this is the same as B
        self.assertEquals(tools.grid2decimal("3341C8424*"),
         (33+ 41.375/60, 84 + 24.5/60))
        # trailing - should be ignored
        self.assertEquals(tools.grid2decimal("3341C8424*-"),
         (33+ 41.375/60, 84 + 24.5/60))

        # test new "long" values
        self.assertEquals(tools.grid2decimal("3341*09614*"),
         (33 + 41.5/60.0, 96 + 14.5/60.0))
        self.assertEquals(tools.grid2decimal("3341*10114*"),
         (33 + 41.5/60.0, 101 + 14.5/60.0))

    def test_decimal2grid(self):
        # These are the reverse of the tests above
        self.assertEquals("3330A8430A", tools.decimal2grid(33 + (30.875/60), -(84 + (30.875/60))))
        self.assertEquals("3339D8422D", tools.decimal2grid(33 + 39.125/60, -(84 + 22.125/60)))
        self.assertEquals("3341C8424B", tools.decimal2grid(33+ 41.375/60, -(84 + 24.625/60)))
        self.assertEquals("3359A8459C", tools.decimal2grid(33 + 59.875/60, -(84 + 59.375/60)))
        self.assertEquals("3309A8459C", tools.decimal2grid(33 + 9.875/60, -(84 + 59.375/60)))
        self.assertEquals("3309A0459C", tools.decimal2grid(33 + 9.875/60, -(4 + 59.375/60)))
        # an example from Tony:
        self.assertEquals("4305C8800A", tools.decimal2grid(43.088, -88.0143))

        # test new "long" values
        self.assertEquals("4305C10100A", tools.decimal2grid(43.088, -101.0143))

    def test_grid2decimal_2(self):
        """ Test if input for grid2decimal is valid. """
        try:
            z = tools.grid2decimal("3371D8424B")
        except AssertionError, e:
            self.assertTrue("Invalid value for lat_minutes: 71.1" in e.args[0])
        else:
            self.fail()

        try:
            z = tools.grid2decimal("9971D8484D")
        except AssertionError, e:
            self.assertEquals(e.args[0], "Invalid value for lat_degrees: 99")
        else:
            self.fail()

        try:
            z = tools.grid2decimal("3344D8472D")
        except AssertionError, e:
            self.assertTrue("Invalid value for long_minutes: 72.1" in e.args[0])
        else:
            self.fail()

    def test_average(self):
        a = [1, 2, 3, 4, 5]
        self.assertEquals(tools.average(a), 3)

        b = [2.0, 3.0]
        self.assertEquals(tools.average(b), 2.5)

    def test_can_open_excl(self):
        f = open("foo", "w")
        print >> f, "test test"

        # file is still open... see if we can get exclusive access
        #self.assertEquals(tools.can_open_excl("foo"), 0)
        #print tools.can_open_excl("foo")
        print >> f, "another line"
        f.close()
        #print tools.can_open_excl("foo")
        # For some reason this isn't working yet...

    def test_can_rename(self):
        f = open("bar", "w")
        print >> f, "test test"

        # we should not be able to rename this file
        self.assertEquals(tools.can_rename("bar"), 0)
        print >> f, "another line"
        f.close()

    def test_remove_crud(self):
        cruddy = "Bla bla \x03 bla bla \n\x08 \xff bla \x0c hum \x80"
        clean = tools.remove_crud(cruddy)
        # 3 line noise characters should have been removed:
        self.assertEquals(len(cruddy)-4, len(clean))
        self.assertEquals(string.find(clean, "\x03"), -1)

    def MOVED__test_isodate(self):
        i = tools.isodate
        self.assertEquals(i("04/08/02 12:05"), "2002-04-08 12:05:00")
        self.assertEquals(i("04/08/02 08AM"), "2002-04-08 08:00:00")
        self.assertEquals(i("04/08/02 08PM"), "2002-04-08 20:00:00")
        self.assertEquals(i("04/08/02 12:05 PM"), "2002-04-08 12:05:00")
        self.assertEquals(i("04/08/02 12:05 AM"), "2002-04-08 00:05:00")
        self.assertEquals(i("04/08/02"), "2002-04-08 00:00:00")

    def test_valid_locate_name(self):
        v = tools.valid_locate_name
        self.assertEquals(v("D1PC3"), 1)
        self.assertEquals(v("Pk2"), 0)  # contains lowercase
        self.assertEquals(v(""), 0) # empty
        self.assertEquals(v("K*9"), 1)  # asterisk is tolerated

    def test_findfirst(self):
        lst = [0, 42, 3, 1, 3.1415, 18, 78, -2]
        idx = tools.findfirst(lst, lambda n: n < 0)
        self.assertEquals(idx, 7)
        idx = tools.findfirst(lst, lambda n: n == 3)
        self.assertEquals(idx, 2)
        idx = tools.findfirst(lst, lambda n: n > 100)
        self.assertEquals(idx, -1)

    def test_struct(self):
        '''
        cl = tools.Client(client_code="D00P")
        self.assertEquals(cl.client_id, None)
        try:
            cl.x = 42
        except AttributeError:
            pass
        else:
            self.fail("Attempting to set illegal attribute")
        #print cl
        '''
        rr = tools.RoutingResult(locator=333)
        self.assertEquals(rr.data, None)
        try:
            rr.x = 42
        except AttributeError:
            pass
        else:
            self.fail("Attempting to set illegal attribute")

    def test_func_name(self):
        class Foo:
            def bar(self): pass
        foo = Foo()
        self.assertEquals(tools.func_name(foo.bar), "Foo.bar")
        def g(x): pass
        self.assertEquals(tools.func_name(g), "g")

    def test_get_fields(self):
        class Foo:
            bar = 42
            def __init__(self):
                self.baz = 0
        foo = Foo()
        fields = tools.get_fields(foo)
        self.assertEquals(fields, ["bar", "baz"])   # no __dict__ or anything

    def test_ado_to_string(self):
        a = tools.ado_to_string
        self.assertEquals(a(1), "1")
        self.assertEquals(a("foo"), "foo")

        if tools.has_booleans:
            self.assertEquals(a(True), "1")

    def test_fmt(self):
        name = "Fred"
        self.assertEquals(tools.fmt("hello", name), "hello Fred")
        self.assertEquals(tools.fmt(), "")
        self.assertEquals(tools.fmt("hello", "hello", sep="-"), "hello-hello")
        self.assertEquals(tools.fmt(1, 2, 3, sep=", "), "1, 2, 3")
        self.assertEquals(tools.fmt(u"a", 42), "a 42")

    def test_safeint(self):
        data = [
            ('30', 30),
            ('30.0', 30),
            ('30.1', 30),
            ('30.', 30),
            ('abc', 0),
            (42, 42),
            (42.5, 42),
        ]
        for s, expected in data:
            z = tools.safeint(s)
            self.assertEquals(z, expected)

    def test_qtrminlong2decimal(self):
        q = tools.qtrminlong2decimal

        long, lat = q("3225000964900")
        self.assertEquals(lat, 32 + 25/60.0)
        self.assertEquals(long, 96 + 49/60.0)

        long, lat = q("325430097130A")
        self.assertEquals(lat, 32 + 54/60.0 + 45/3600.0)
        self.assertEquals(long, 97 + 13/60.0 + 15/3600.0)

        long, lat = q("335500098370B ")
        self.assertEquals(lat, 33 + 55/60.0 + 15/3600.0)
        self.assertEquals(long, 98 + 37/60.0 + 0/3600.0)

        long, lat = q("321800096380C ")
        self.assertEquals(lat, 32 + 18/60.0 + 0/3600.0)
        self.assertEquals(long, 96 + 38/60.0 + 15/3600.0)

        long, lat = q("325600097150D ")
        self.assertEquals(lat, 32 + 56/60.0 + 0/3600.0)
        self.assertEquals(long, 97 + 15/60.0 + 0/3600.0)

        long, lat = q("335500098363A ")
        self.assertEquals(lat, 33 + 55/60.0 + 15/3600.0)
        self.assertEquals(long, 98 + 36/60.0 + 45/3600.0)

        long, lat = q("320730096563B ")
        self.assertEquals(lat, 32 + 7/60.0 + 45/3600.0)
        self.assertEquals(long, 96 + 56/60.0 + 30/3600.0)

        long, lat = q("335500098363C ")
        self.assertEquals(lat, 33 + 55/60.0 + 0/3600.0)
        self.assertEquals(long, 98 + 36/60.0 + 45/3600.0)

        long, lat = q("321030096523D ")
        self.assertEquals(lat, 32 + 10/60.0 + 30/3600.0)
        self.assertEquals(long, 96 + 52/60.0 + 30/3600.0)

    def test_Transparent(self):
        t = tools.Transparent("foo.bar")
        self.assertEquals(repr(t), "foo.bar")

    def test_quote_apostrophes(self):
        s = "'Fred', he said."
        t = tools.quote_apostrophes(s)
        self.assertEquals(t, "''Fred'', he said.")

        sql = """
          insert response_log (locate_id, response_date, call_center, status,
                               response_sent, success, reply)
          values ('146436487', '2005-10-17 03:23:10', '7501', 'M', '003', 0,
                  '%s')
        """ % (tools.quote_apostrophes(s),)
        tdb = ticket_db.TicketDB()
        tdb.runsql(sql)

    def test_is_numeric(self):
        data = [
            ("12", True),
            ("12a", False),
            ("a", False),
            ("", False),
            ("-3", True),
            ("12-3", False),
        ]
        for s, result in data:
            self.assertEquals(tools.is_numeric(s), result)

    def test_separate_by(self):
        data = open("../testdata/preprocessor/PNA1/07858 Prince coversheet 05-11-06.000").read()
        data = data.lstrip()

        parts = tools.separate_by(data, "REPORT PROG: DP22052")
        self.assertEquals(len(parts), 11)

        # call separate_by with multiple substrings
        parts = tools.separate_by(data, ["REPORT PROG:", "WEST NASSAU - LONG ISLAND"])
        self.assertEquals(len(parts), 11)

        # test detail sheet
        data = open("../testdata/preprocessor/PNA1/07858 Prince workorders 05-11-06.000").read()
        data = data.lstrip("\n") # removes first (empty) line
        parts = tools.separate_by(data, "CABLEVISION.COM")
        self.assertEquals(len(parts), 57)

    def test_is_spam(self):
        """
        Tests the is_spam method
        """
        spammy = ["Some test <HTML> some html </HTML>",
                  "Some test <html> some html </HTML>",
                  "A link http://spammy.com"]
        for spam in spammy:
            self.assertTrue(tools.is_spam(spam))

    test_is_spam.fails=1

    def test_with_attrs(self):
        @tools.with_attrs(foo=42, bar="hello")
        def example(x):
            return x+1
        self.assertEquals(example(10), 11) # check that function still works
        self.assertEquals(getattr(example, 'foo', None), 42)
        self.assertEquals(getattr(example, 'bar', None), 'hello')

    def test_find_block(self):
        b = tools.find_block(range(20), lambda n: n >= 5, lambda n: n > 10)
        self.assertEquals(b, [5, 6, 7, 8, 9, 10])

    def test_fix_ampersands_in_xml(self):
        # invalid XML
        data = """
          <memberitem>
            <member>ATT392</member>
            <group_code>ATT</group_code>
            <description>AT&T</description>
          </memberitem>
        """
        fixed = tools.fix_ampersands_in_xml(data)
        self.assertFalse("AT&T" in fixed)
        self.assertTrue("AT&amp;T" in fixed)
        self.assertTrue("<description>AT&amp;T</description>" in fixed)
        self.assertEquals(len(fixed), len(data) + 4)

        # valid chunk of XML
        data = """
          <memberitem>
            <member>ATT392</member>
            <group_code>ATT</group_code>
            <description>AT&amp;T</description>
          </memberitem>
        """
        fixed = tools.fix_ampersands_in_xml(data)
        self.assertEquals(data, fixed) # there should be no changes

    def test_get_headers(self):
        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "AGL-2010-11-02.txt")
        lines = filetools.read_file(path).split('\n')
        headers = tools.parse_headers(lines)

        self.assertEquals(len(headers), 15-1) # 15 minus a duplicate
        self.assertEquals(headers['Content-Transfer-Encoding'], 'base64')

    def test_safe_convert_base64(self):
        s64 = "aGVsbG8gd29ybGQ=\n"
        self.assertEquals(tools.safe_convert_base64(s64), "hello world")

        s65 = "NOT base64, obviously"
        self.assertEquals(tools.safe_convert_base64(s65), None)

    def test_extract_date_from_filename(self):
        path = os.path.join(_testprep.TICKET_PATH, 'work-orders', 'LAM01',
               'LAM01-2011-03-07-00009-A01.txt')
        filename = os.path.split(path)[1]
        ds = tools.extract_date_from_filename(filename)
        self.assertEquals(ds, '2011-03-07')

    def test_get_attachment_filename(self):
        path = os.path.join(_testprep.TICKET_PATH, 'receiver',
               'LAM01-attachment-1.txt')
        lines = open(path, 'r').read().split('\n')
        att1 = tools.find_block(lines[38:],
               lambda s: s.startswith("----boundary_1_6949dda8-"),
               lambda s: s.startswith("---"))
        blurb = string.join(att1, '\n')
        att_name = tools.get_attachment_filename(blurb)
        self.assertEquals(att_name, "Locate_V0013991_20110510_1832.XML")

    def test_filetools_filesize(self):
        self.assertEquals(filetools.filesize('xyzzy'), 0) # does not exist
        path = os.path.join(_testprep.TICKET_PATH, 'Atlanta-1.txt')
        self.assertEquals(filetools.filesize(path), 8325)

    def test_attempt(self):
        a = tools.attempt(None,
                          lambda: 1/0,
                          lambda: [1,2,3][4],
                          lambda: 42)
        self.assertEquals(a, 42)

        b = tools.attempt(None)
        self.assertEquals(b, None)

        try:
            c = tools.attempt(None, lambda: 1/0)
        except ZeroDivisionError:
            pass # this is expected
        except:
            self.fail()
        else:
            self.fail()

    def test_get_hp_reasons(self):
        self.assertEquals(tools.get_hp_reasons('HPR-36,37,40,42 COX123'),
          [36, 37, 40, 42])


if __name__ == "__main__":
    unittest.main()
