# test_soapresponder.py

from __future__ import with_statement
import os
import unittest
#
from mockery import *
import _testprep
import config
import soapresponder
import ticket_db
import ticketloader
import ticketparser

class MockSOAPResponder(soapresponder.SOAPResponder):
    sent = []
    def send_response(self, ticket_number, membercode, responsecode,
                      explanation=None, locator_name='', version='0'):

        membercode = self.translate_term(membercode, {})

        rs = self.make_response_string(ticket_number, membercode,
             responsecode, explanation, version)
        self.log.log_event("Sending response string: %r" % rs)

        return "250"

class TestSOAPResponder(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.test_emp_id = _testprep.add_test_employee(self.tdb, "TestSOAP")
        self.cfg = config.getConfiguration(reload=True)

    def tearDown(self):
        config.getConfiguration(reload=True)

    def test_responses_from_base_responder(self, call_center='3004'):
        # for testing purposes, we pretend that 3004 is not an update call
        # center, so we can check if the "sender" in the response logs is
        # "UQ", rather than "STS" (which is used by 3003)
        # set up config
        z = {'ack_dir': '',
             'ack_proc_dir': '',
             'mappings': {'M': ('1', 'A'), 'N': ('2', 'B')},
             'logins': [{'login': 'BGAWS', 'password': '123'}],
             'clients': ['BGAWS', 'AGL101'],
             'all_versions': 0,
             'id': 'spam',
             'name': call_center,
             'url': 'http://example.com/wsdl',
             'send_3hour': 1,
             'send_emergencies': 1,
             'skip': [],
             'status_code_skip': [],
             'translations': {}}

        try:
            del self.cfg.soap_responders[call_center]
        except:
            pass
        self.cfg.soap_responders[call_center] = z
        self.cfg.responderconfigdata.add(z['name'], 'soap', z, unique=True)

        # add tickets
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "3006-2012-11-01",
             "3003-2012-10-30-11-28-08-506-D9V55.txt"))
        raw = tl.tickets[0]

        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['GA3003'])
        t.ticket_format = call_center
        t.locates[0].status = 'M'
        assert t.locates[0].client_code == 'BGAWS'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add another ticket
        t = handler.parse(raw.replace('-014', '-015'), ['GA3003'])
        t.ticket_format = call_center
        t.locates[0].status = 'N'
        ticket_id_2 = self.tdb.insertticket(t, whodunit='parser')

        # make sure we have two tickets in the database
        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 2)

        # check responder queue
        rq = self.tdb.getrecords('responder_queue', ticket_format=call_center)
        self.assertEquals(len(rq), 2)

        # run mock responder
        import suds.client
        with Mockery(suds.client, 'Client', lambda x: None):
            resp = MockSOAPResponder(verbose=0)
            resp.log.logger.keep = True # keep logs
            resp.respond_all()
            resp.log.logger.keep = False

        # check output, response log
        sender = 'STS' if call_center == '3003' else 'UQ'
        msg = "'DATA 10262-320-014,BGAWS,%s,1A,'" % sender
        found = any(s for s in resp.log.logger.logged 
                    if ("response string: " + msg) in s)
        self.assertTrue(found)

        # response queue should be empty now
        rq = self.tdb.getrecords('responder_queue', ticket_format=call_center)
        self.assertEquals(len(rq), 0)

        rl = self.tdb.getrecords('response_log')
        rl.sort(key=lambda x: int(x['locate_id']))
        self.assertEquals(rl[0]['response_sent'], '1A')
        self.assertEquals(rl[1]['response_sent'], '2B')
        self.assertEquals(rl[0]['success'], '1')
        self.assertEquals(rl[1]['success'], '1')

    def test_responder_for_3003(self):
        #3003 should respond with STS instead of UQ
        self.test_responses_from_base_responder(call_center='3003')

if __name__ == "__main__":
    unittest.main()

