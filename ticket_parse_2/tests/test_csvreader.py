# test_csvreader.py

import os
import site; site.addsitedir('.')
import unittest
#
import csvreader

class TestCSVReader(unittest.TestCase):

    def setUp(self):
        self.data1 = open(os.path.join("..","testdata","ultra-cr.txt")).read()
        self.data2 = open(os.path.join("..","testdata","test_csv_1.txt")).read()

    def test_fieldnames(self):
        csv = csvreader.CSVReader(self.data1)
        self.assertEquals(csv.fieldnames[0], 'Ticket Number')
        self.assertEquals(csv.fieldnames[2], ' Transmit Date') # notice the space

    def test_parsing_tokens(self):
        csv = csvreader.CSVReader(self.data1)
        values = csv.parse_line()
        self.assertEquals(values[0], "01165194")
        self.assertEquals(values[1], "1570")
        self.assertEquals(values[-1], "Ken")

    def test_parsing_dict(self):
        csv = csvreader.CSVReader(self.data1)
        d = csv.parse_line_as_dict()
        self.assertEquals(d['Ticket Number'], "01165194")
        self.assertEquals(d['Transmit Date'], "1/11/2006 2:44:00 PM")
        self.assertEquals(d['First Name'], "Ken")

    def test_002(self):
        csv = csvreader.CSVReader(self.data1)
        for i in range(20):
            d = csv.parse_line_as_dict()
        self.assertEquals(d['Ticket Number'], "02462849")
        self.assert_('\n' in d['Extent of Work'])

        # let's try the next line
        d = csv.parse_line_as_dict()
        self.assertEquals(d['Ticket Number'], "02462849")

    def test_looping(self):
        csv = csvreader.CSVReader(self.data1)
        x = []
        for d in csv:
            x.append(d)
        self.assertEquals(len(x), 64)

    def test_more(self):
        csv = csvreader.CSVReader(self.data2)
        self.assertEquals(csv.fieldnames, ["A", "B", "C", "D", "E"])

        d = csv.parse_line_as_dict()
        self.assertEquals(d['A'], 'Hello')
        self.assertEquals(d['B'], 'my friends')
        self.assertEquals(d['C'], "this is ")

        d = csv.parse_line_as_dict()
        self.assertEquals(d['A'], 'My name is "Fred"')
        self.assertEquals(d['B'], "and yours?")
        self.assertEquals(d['C'], 'My name is "Hank",\nhe said.')
        self.assertEquals(d['D'], "oh!")

    def test_something_that_broke_pythons_csv(self):
        data = open(os.path.join("..","testdata","ultra-cr-2.txt")).read()
        if "\r" not in data:
            data = data.replace("\n", "\r\n")
        csv = csvreader.CSVReader(data)
        d = csv.next()
        # if we survive this, it's probably parsed correctly

        self.assertEquals(d['Ticket Number'], "08263635")
        self.assertEquals(d['Extent of Work'][:10], " MARK THE INTERSECTION"[:10])
        self.assertEquals(d['Extent of Work'][-10:], "ROAD RIGHT OF WAY."[-10:])
        self.assertEquals(d['First Name'], 'Joshua\r') # ..?

    def test_invalid_csv(self):
        data = open(os.path.join("..","testdata","ultra-cr-error.txt")).read()
        csv = csvreader.CSVReader(data)
        try:
            d = csv.next()
        except csvreader.CSVReaderError, e:
            msg = e.args[0]
            self.assert_(msg.startswith("Too many fields: 71 (expected 35)"))
        except:
            self.fail("CSVReaderError expected")
        else:
            self.fail("CSVReaderError expected")

        # XXX not an easy problem... even though the actual error is in the
        # first line, the parser goes on gatherer invalid data until it stops
        # at (what it thinks is) line 42.
        # However, I don't think this kind of error will appear very often
        # (if at all) in the Ultra CSV files.


if __name__ == "__main__":

    unittest.main()
