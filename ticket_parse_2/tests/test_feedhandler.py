# test_feedhandler.py

from __future__ import with_statement
import os
import site; site.addsitedir('.')
import unittest
#
import config
import feedhandler
import tools
import _testprep

class Null:
    def __getattr__(self, name):
        return self
    def __call__(self, *args, **kwargs):
        return self

class TestFeedHandler(unittest.TestCase):

    def setUp(self):
        _testprep.create_test_dirs()

    def test_001(self):
        cfg = config.getConfiguration()
        # add some test data
        cfg.processes = []
        cfg.processes.append({
            'incoming': 'test_incoming',
            'format': 'Atlanta',
            'processed': 'test_processed',
            'error': 'test_error',
            'group': '1',
            'attachments': '',
            'attachment_dir': '',
            'attachment_proc_dir': '',
        })
        cfg.processes.append({
            'incoming': 'test_incoming_FCL2',
            'format': 'FCL2',
            'processed': 'test_processed_FCL2',
            'error': 'test_error_FCL2',
            'group': '2',
            'attachments': '',
            'attachment_dir': '',
            'attachment_proc_dir': '',
        })

        fh = feedhandler.FeedHandler(cfg, Null(), 'FCL2', verbose=0)

        # drop one file in group 1, two in group 2
        self.drop('test_incoming', 'one.txt', "blahblah")
        self.drop('test_incoming_FCL2', 'two.txt', 'zzzz')
        self.drop('test_incoming_FCL2', 'three.txt', 'gfgfgfd')

        tickets = fh.get_all_files()
        self.assertEquals(len(tickets), 2)
        self.assert_(tickets[0].filename in ['two.txt', 'three.txt'])
        self.assert_(tickets[1].filename in ['two.txt', 'three.txt'])

    def tearDown(self):
        _testprep.delete_test_dirs()

    def drop(self, dir, filename, data):
        fullname = os.path.join(dir, filename)
        with open(fullname, 'wb') as f:
            f.write(data)



if __name__ == "__main__":

    unittest.main()

