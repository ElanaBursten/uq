# test_regexen.py

import re
import site; site.addsitedir('.')
import unittest

ticket = """\
USW01  00095 GAUPC 12/31/01 10:14:39 12311-109-056-000 NORMAL RESTAKE
Underground Notification
Ticket : 12311-109-056 Date: 12/31/01 Time: 10:11 Revision: 000
Old Tkt: 12141-300-044 Date: 12/19/01 Time: 00:12

State: GA  County: FULTON       Place: ATLANTA
Addr : From:        To:        Name:    SPRING GARDEN                  DR   SW
Cross: From:        To:        Name:
Offset:
Subdivision:
RR Subdivision:            RR Marker:            Mile Marker:
Locat: LOCATE BOTH SIDES OF THE STREET    AT THE INT. OF QUAKER STREET GOING
: 200 FT. IN ALL DIRECTIONS - INCLUDING ALL SERVICES IN BETWEEN

Grids    : 3341C8424B   3341C8424A   3341D8424B   3341D8424A
Work type: NEW GAS RENEWAL REPLACING GAS
Work date: 11/02/01 Time: 07:00 Hrs notc: 000 Priority: 3
Legal day: 01/04/02 Time: 07:00 Good thru: 01/21/02 Restake by: 01/16/02
RespondBy: 01/03/02 Time: 23:59 Duration : 2 WKS
Done for : AGL
Crew on Site: N  White-lined: N  Railroad: N  Blasting: N  Boring: Y

Remarks : RESTAKE OF 10301-300-011
: RESTAKE OF 11141-300-020
: RESTAKE OF 11291-300-020
: RESTAKE OF: 12141-300-044
: * MEA70 WAS NOT ON THE ORIGINAL VERSION OF THIS TICKET
: *** WILL BORE BOTH SIDES OF ROAD

Company : GENERAL PIPELINE COMPANY                  Type: CONT
Co addr : 4090 BONSAL ROAD
City    : CONLEY                         State   : GA Zip: 30027
Caller  : TONYA MASON                    Phone   : 404-361-0005
Fax     : 404-361-1509                   Alt. Ph.:
Cellular: 404-361-0005
Submitted date: 12/31/01 Time: 10:11 Oper: 109 Chan: 999
Mbrs : AGL18  ATT02  ATTBB  BSCA   DOTI   EPT50  EPT52  FUL02  GP145  MEA70
: OMN01  TCG01  USW01
"""

class TestRegexen(unittest.TestCase):

    def test1(self):
        # emulate what AtlantaParser does with the ticket:
        re_work_address_street = re.compile("^Addr :.*Name: +(.*)$",
         re.MULTILINE)
        m = re_work_address_street.search(ticket)
        if m:
            #print repr(m.group(1))
            pass    # we shouldn't print during a test
        else:
            raise "No match!"

    def test2(self):
        re_work_address_number = re.compile("^Addr : From: +(.*?) +To:",
         re.MULTILINE)
        m = re_work_address_number.search(ticket)
        if m:
            #print repr(m.group(1))
            pass
        else:
            raise "No match!"



if __name__ == "__main__":

    unittest.main()

