# test_ticket_alert.py

from glob import glob
import os
import site; site.addsitedir('.')
import unittest
#
import _testprep
import call_centers
import highprofilegrids
import locate
import static_tables
import ticket as ticketmod
import ticket_alert
import ticket_db
import ticketloader
import ticketparser
import tools

class TestTicketAlert(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.handler = ticketparser.TicketHandler()
        _testprep.add_test_call_center(self.tdb, "FDX4")

    def tearDown(self):
        _testprep.delete_test_call_center(self.tdb, "FDX4")
        _testprep.clear_database(self.tdb)

    def test_alert_by_ticket_keyword_1(self):
        if static_tables.alert_ticket_keyword and static_tables.alert_ticket_type:
            try:
                self.tdb.insertrecord('alert_ticket_type', call_center = 'SCA1', ticket_type = 'REMK REMK GRID')
                rec_id = self.tdb.dbado.get_max_id('alert_ticket_type', 'alert_ticket_type_id')
                self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'Exposed')
                self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'RESPOND')
                self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'UTI')
                self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'Further Marks')
                self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'ASAP')

                # parse tickets with that term id on it
                tickets = tools.safeglob(os.path.join("..","testdata","sample_tickets","SCA1","sca1-2009-08-21-*.txt"))
                for ticket in tickets:
                    tl = ticketloader.TicketLoader(ticket)
                    raw = tl.getTicket()
                    t = self.handler.parse(raw, ['SouthCalifornia'])
                    d = self.tdb.get_ticket_alert_keywords()
                    client_dict = self.tdb.get_clients_dict()
                    tah = ticket_alert.TicketAlertHandler(self.tdb, client_dict,
                          None, ticket_alert_keywords = d)
                    x = tah._has_ticket_keyword(t)
                    self.assertTrue(x)
            finally:
                self.tdb.deleterecords('alert_ticket_keyword', alert_ticket_type_id = rec_id)
                self.tdb.deleterecords('alert_ticket_type', alert_ticket_type_id = rec_id)


    def test_alert_by_ticket_keyword_2(self):
        if static_tables.alert_ticket_keyword and static_tables.alert_ticket_type:
            self.tdb.insertrecord('alert_ticket_type', call_center = 'SCA1', ticket_type = 'GRID')
            rec_id = self.tdb.dbado.get_max_id('alert_ticket_type', 'alert_ticket_type_id')
            self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'AT&T')
            # parse ticket with that term id on it
            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA1","sca1-2009-08-04-00071.txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, ['SouthCalifornia'])
            d = self.tdb.get_ticket_alert_keywords()
            client_dict = self.tdb.get_clients_dict()
            tah = ticket_alert.TicketAlertHandler(self.tdb, client_dict, None,
                  ticket_alert_keywords = d)
            x = tah._has_ticket_keyword(t)
            self.assertFalse(x)

    def test_alert_by_ticket_keyword_3(self):
        if static_tables.alert_ticket_keyword and static_tables.alert_ticket_type:
            self.tdb.insertrecord('alert_ticket_type', call_center = 'SCA1', ticket_type = 'GRID')
            rec_id = self.tdb.dbado.get_max_id('alert_ticket_type', 'alert_ticket_type_id')
            self.tdb.insertrecord('alert_ticket_keyword', alert_ticket_type_id = rec_id, keyword = 'AT&T')
            # parse ticket with that term id on it
            tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA1","sca1-2009-03-19-00976.txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, ['SouthCalifornia'])
            d = self.tdb.get_ticket_alert_keywords()
            client_dict = self.tdb.get_clients_dict()
            tah = ticket_alert.TicketAlertHandler(self.tdb, client_dict, None,
                  ticket_alert_keywords = d)
            x = tah._has_ticket_keyword(t)
            self.assertFalse(x)

    def test_alert_by_client(self):
        # add a test client
        client_id = _testprep.add_test_client(self.tdb, 'DUKI01', 'NewJersey')
        # set alert = 1
        self.tdb.runsql("""
         update client
         set alert=1
         where client_id = %s
        """ % (client_id,))

        # parse ticket with that term id on it
        tl = ticketloader.TicketLoader("../testdata/newjersey-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("UW4", "DUKI01")
        t = self.handler.parse(raw, ['NewJersey'])

        # make sure it has an alert
        client_dict = self.tdb.get_clients_dict()
        tah = ticket_alert.TicketAlertHandler(self.tdb, client_dict, None)
        x = tah.has_alert(t)
        self.assertEquals(x, 1)

    def test_alert_by_client_2(self):
        client_id = _testprep.add_test_client(self.tdb, 'DUKI02', 'NewJersey')
        # make sure alert=0
        self.tdb.runsql("""
         update client
         set alert=0
         where client_id = %s
        """ % (client_id,))

        # parse ticket with that term id on it
        tl = ticketloader.TicketLoader("../testdata/newjersey-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("UW4", "DUKI02")
        t = self.handler.parse(raw, ['NewJersey'])

        # make sure it does not have an alert
        client_dict = self.tdb.get_clients_dict()
        tah = ticket_alert.TicketAlertHandler(self.tdb, client_dict, None)
        x = tah.has_alert(t)
        self.assertEquals(x, 0)

    def test_alert_by_client_3(self):
        cc = call_centers.get_call_centers(self.tdb)
        cc.ucc['FDX4'] = 'FDX1'
        try:
            self.tdb.runsql("""
             delete client
             where call_center = 'FDX4'
             or update_call_center = 'FDX4'
            """)

            client_id = _testprep.add_test_client(self.tdb, 'PNE', 'FDX1',
                        update_call_center='FDX4')
            # make sure alert=1
            self.tdb.runsql("""
             update client
             set active=1, alert=1, update_call_center='FDX4'
             where client_id = %s
            """ % (client_id,))

            # parse ticket with that term id on it
            tl = ticketloader.TicketLoader("../testdata/alert-FDX4.txt")
            raw = tl.tickets[0]
            t = self.handler.parse(raw, ['Dallas4'])
            self.assertEquals(t.ticket_format, 'FDX4')
            t._update_call_center = t.ticket_format
            t.ticket_format = cc.update_call_center(t.ticket_format)
            self.assertEquals(t.ticket_format, 'FDX1')

            # make sure it does have an alert
            client_dict = self.tdb.get_clients_dict()
            self.assertEquals(client_dict.has_key('FDX4'), False)
            tah = ticket_alert.TicketAlertHandler(self.tdb, client_dict, None)
            x = tah._has_client_alert(t)
            self.assertEquals(x, True)
        finally:
            del cc.ucc['FDX4']

    def test_alert_by_hp(self):
        # add some test records
        _testprep.add_test_call_center(self.tdb,'XYZ')
        _testprep.add_test_call_center(self.tdb,'ZYX')

        _testprep.add_test_client(self.tdb, 'ANJO75', 'XYZ')
        _testprep.add_test_client(self.tdb, 'JENN75', 'XYZ', active=0)

        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ANJO75', 'Harry')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ANJO75', 'Ron')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ANJO75', 'Hermione')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'JENN75', 'Snape')
        _testprep.add_test_hp_grid(self.tdb, 'ZYX', 'PQR', 'Dumbledore')

        # delete ticket_grid_log
        self.tdb.dbado.deleterecords('ticket_grid_log')

        t = ticketmod.Ticket()
        t.ticket_number = '1234567890'
        t.ticket_format = 'XYZ'
        t.locates = [locate.Locate("ANJO75"), locate.Locate("DEF")]
        t.grids = ['Harry', 'Ron', 'Seamus', 'Ginny', 'Dumbledore']
        t.image = "blah"

        hpgrid = highprofilegrids.HighProfileGrids(self.tdb)

        # make sure it has an alert
        client_dict = self.tdb.get_clients_dict()
        tah = ticket_alert.TicketAlertHandler(self.tdb, client_dict, hpgrid)
        x = tah.has_alert(t)
        self.assertEquals(x, 1)

        t.grids = ["Fred", "Barney", "Wilma"]
        x = tah.has_alert(t)
        self.assertEquals(x, 0)

        # there's a grid match, but the client is *inactive*...
        # make sure it doesn't get an alert!
        t.locates = [locate.Locate("JENN75"), locate.Locate("DEF")]
        t.grids = ['Harry', 'Ron', 'Seamus', 'Ginny', 'Dumbledore', 'Snape']
        x = tah.has_alert(t)
        self.assertEquals(x, 0)
        _testprep.delete_test_call_center(self.tdb,'XYZ')
        _testprep.delete_test_call_center(self.tdb,'ZYX')

        _testprep.delete_test_hp_grid(self.tdb, 'XYZ')
        _testprep.delete_test_hp_grid(self.tdb, 'ZYX')

    def test_get_hp_grid_clients(self):
        # add some test records
        _testprep.add_test_call_center(self.tdb,'XYZ')
        _testprep.add_test_call_center(self.tdb,'ZYX')
        _testprep.add_test_client(self.tdb, 'ANJO75', 'XYZ')
        _testprep.add_test_client(self.tdb, 'JENN75', 'XYZ', active=0)
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ANJO75', 'Harry')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ANJO75', 'Ron')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'ANJO75', 'Hermione')
        _testprep.add_test_hp_grid(self.tdb, 'XYZ', 'JENN75', 'Snape')
        _testprep.add_test_hp_grid(self.tdb, 'ZYX', 'PQR', 'Dumbledore')

        t = ticketmod.Ticket()
        t.ticket_number = '1234567890'
        t.ticket_format = 'XYZ'
        t.locates = [locate.Locate("ANJO75"), locate.Locate("DEF")]
        t.grids = ['Harry', 'Ron', 'Seamus', 'Ginny', 'Dumbledore']
        t.image = "blah"

        client_dict = self.tdb.get_clients_dict()
        hpgrid = highprofilegrids.HighProfileGrids(self.tdb)
        tah = ticket_alert.TicketAlertHandler(self.tdb, client_dict, hpgrid)
        alert_locates = tah.get_hp_grid_clients(t)
        self.assertEquals(alert_locates, t.locates[:1])
        _testprep.delete_test_call_center(self.tdb,'XYZ')
        _testprep.delete_test_call_center(self.tdb,'ZYX')
        _testprep.delete_test_hp_grid(self.tdb, 'XYZ')
        _testprep.delete_test_hp_grid(self.tdb, 'ZYX')

if __name__ == "__main__":
    unittest.main()

