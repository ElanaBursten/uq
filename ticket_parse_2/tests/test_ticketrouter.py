# test_ticketrouter.py
# Created: 15 Feb 2002, Hans Nowak

# Note:
# These tests deal with the ticketrouter. In some cases we're testing if a
# certain client is assigned to a given locator. These locators may change over
# time, so unexpected errors might occur. If that is the case, check by hand
# (in the database) what the locator should be according to the data of the
# ticket.

from __future__ import with_statement
import csv
import httplib
import inspect
import os
import site; site.addsitedir('.')
import string
import StringIO
import sys
import unittest
#
import blacklist
import businesslogic
import call_centers
import config
import datadir
import date
import dbinterface_old as dbinterface
import errorhandling2
import filetools
import locate
import locate_status
import mail2
import test_call_centers
import ticket
import ticket_db
import ticketloader
import ticketparser
import ticketrouter
import tools
import _testprep
from callcenters import NewJersey
from search_log_file import SearchLogFile
from mockery import *
from mockery_tools import *

class TestTicketRouter(unittest.TestCase):

    def send(self,xml):
        self.xml = xml

    def setUp(self):
        mail2.TESTING = 1
        mail2._test_mail_sent = []

        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        # empty tables so we won't have any accidental updates of existing
        # tickets where we expected an insert, etc.
        _testprep.clear_database(self.tdb)
        # the USW01 client is no longer active in the database, make it active
        sql = """ select call_center
                  from client where oc_code = 'USW01' """
        self.result_USW01 = self.tdb.runsql_result(sql)[0]
        _testprep.set_client_active(self.tdb, self.result_USW01['call_center'],
         "USW01")
        # Delete the routing error file
        BUGFILE = "routing_error_skip.txt"
        if datadir.datadir.exists(BUGFILE):
            datadir.datadir.remove(BUGFILE)

    def tearDown(self):
        # Set it back inactive
        _testprep.set_client_inactive(self.tdb, self.result_USW01['call_center'], "USW01")

    def test_000_preparation(self):
        _testprep.set_cc_routing_file()
        _testprep.set_client_active(self.tdb, 'FCV2', 'ACR441')

    def getticket(self):
        tl = ticketloader.TicketLoader("../testdata/atlanta-2.txt", "~~~~~")
        raw = tl.getTicket()
        return self.handler.parse(raw, ["Atlanta"])

    def test_TicketRouter1(self):
        # make the ticketrouter first, because we need one of its methods
        ticket = self.getticket()
        ticket.kind = ticket.ticket_type = "EMERGENCY"  # for ticket_ack
        ticket.locates = [] # do not post any locates
        # post ticket to database
        id = self.tdb.insertticket(ticket)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, ticket, 'test_TicketRouter1.txt')

        # create a TicketRouter
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.log.lock = 1

        # now, let's add an assignable locate record to that ticket...
        value = tr._get_customers()[('USW01','Atlanta')]
        client = 'USW01'
        call_center = 'Atlanta'
        client_id = value
        sql = """
         insert locate
         (ticket_id, client_code, status, client_id)
         values ('%s', '%s', '%s', '%s')
        """ % (id, client, locate_status.blank, client_id)
        self.tdb.runsql(sql)

        # what locate was just added? (we didn't get an id back, so we'll
        # have to get it "by hand")
        sql = "select * from locate where ticket_id = %s" % (id)
        result = self.tdb.runsql_result(sql)
        self.assertEquals(len(result), 1)
        locate_id = result[0]["locate_id"]

        # we now have at least one unassigned locate, viz. the new one
        # start the ticket router to find and assign it:
        tr.run(runonce=1)

        # check if the locate is assigned now
        assigned = self.tdb.isassigned(locate_id)
        self.assert_(assigned, "locate %s is not assigned" % (locate_id))

        # check if a record was added to ticket_ack (emergency!)
        result = self.tdb.getrecords("ticket_ack")
        self.assertEquals(len(result), 1)
        self.assertEquals(result[0]["ticket_id"], id)

    def test_find_grid_area_locator(self):
        # add test data:
        # a map
        map_id = _testprep.add_test_map(self.tdb, 'testname', 'TS',
                 'testcounty', 'testcode')
        # an employee
        emp_id = _testprep.add_test_employee(self.tdb, "Henk")
        # an area
        area_id = _testprep.add_test_area(self.tdb, 'testarea', emp_id, map_id)
        # a map_page record, for page 332
        _testprep.add_test_map_page(self.tdb, map_id, 332)
        # a map_cell record
        _testprep.add_test_map_cell(self.tdb, map_id, 332, 'X', 10, area_id)
        # a map_geo record using the map/page
        _testprep.add_test_map_geo(self.tdb, map_id, 332, 'X', '10', 50.0, 51.0,
         5.0, 6.0)

        result = self.tdb.find_grid_area_locator(latitude=50.55,
                 longitude=5.55)
        self.assert_(result, "Locator(s) expected")

        r = result[0]
        self.assertEquals(r["map_id"], map_id)
        self.assertEquals(r["map_page_num"], "332")
        self.assertEquals(r["x_part"], "X")
        self.assertEquals(r["y_part"], "10")
        self.assertEquals(r["area_name"], "testarea")
        self.assertEquals(r["emp_id"], emp_id)
        self.assertEquals(r["short_name"], "Henk")

    def test_precondition(self):
        # Assert that the default locators really exist.
        callcenters = call_centers.cc2format.keys()
        for key in callcenters:
            if key in test_call_centers.NOT_DONE:
                continue
            name = "%sBusinessLogic" % (key)
            if hasattr(businesslogic, name):
                blclass = getattr(businesslogic, name)
                value = blclass.DEFAULT_LOCATOR
                if not value:
                    continue    # None, for callcenters that are not in
                                # production yet
            else:
                continue    # no default locator available
            sql = "select * from employee where emp_id = '%s'"
            sql = sql % (value)
            result = self.tdb.runsql_result(sql)
            # exclude LocInc call centers
            if not key.startswith("L"):
                self.assert_(result, "Default locator %s does not exist" % (
                 (key, value),))

        z = self.tdb.count("client")
        self.assert_(z, "Client table is empty")

    # Store a ticket with at least two assignable locates, both of
    # which need to be resolved through mapinfo (i.e. they are not assigned
    # to hard-coded client). Check if there are two new assignment records
    # and if the locate records are as they should be.
    def test_foo(self):
        try:
            call_center = 'Atlanta'
            ATL01_activated = False
            results = self.tdb.getrecords("client", call_center = call_center, oc_code = 'ATL01', active = 1)
            if not results:
                ATL01_activated = True
                _testprep.set_client_active(self.tdb, call_center, 'ATL01')
            USW01_activated = False
            results = self.tdb.getrecords("client", call_center = call_center, oc_code = 'USW01', active = 1)
            if not results:
                USW01activated = True
                _testprep.set_client_active(self.tdb, call_center, 'USW01')

            # grab any old ticket
            t = self.getticket()
            t.due_date = '2001-12-20 08:25:08'

            # inject some locates that are not pre-assigned to a locator
            t.locates = [locate.Locate("ATL01"), locate.Locate("USW01")]

            # post the ticket and keep the ID
            id = self.tdb.insertticket(t)
            # note that this does NOT set the client_id of locates

            # let the ticket router have its way
            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            #tr.log.lock = 1
            tr.run(runonce=1)

            # what locates were just added?
            sql = "select * from locate where ticket_id = %s" % (id)
            result = self.tdb.runsql_result(sql)
            self.assertEquals(len(result), 2)
            self.assertEquals(result[0]["status"], locate_status.assigned)
            self.assertEquals(result[1]["status"], locate_status.assigned)
            locate_id_1 = result[0]["locate_id"]
            locate_id_2 = result[1]["locate_id"]

            # test first assignment
            sql = "select * from assignment where locate_id = %s" % (locate_id_1)
            result = self.tdb.runsql_result(sql)
            self.assertEquals(len(result), 1)
            self.assertEquals(result[0]['workload_date'], t.due_date)

            # test second assignment
            sql = "select * from assignment where locate_id = %s" % (locate_id_2)
            result = self.tdb.runsql_result(sql)
            self.assertEquals(len(result), 1)
            self.assertEquals(result[0]['workload_date'], t.due_date)

            # test ticket_ack usage; none should be posted (no emergency)
            results = self.tdb.getrecords("ticket_ack")
            self.assertEquals(len(results), 0)
        finally:
            if ATL01_activated:
                _testprep.set_client_inactive(self.tdb, call_center, 'ATL01')
            if USW01_activated:
                _testprep.set_client_inactive(self.tdb, call_center, 'USW01')


    def test_Richmond1_routing_1(self):
        _testprep.make_client(self.tdb, 'CMG01', 'FCV1')

        tl = ticketloader.TicketLoader("../testdata/richmond1-1.txt", chr(12))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Richmond1"])
        t.locates[1] = locate.Locate("BLAH01")
        del t.locates[2:]   # remove others, they're unnecessary

        # this first ticket is a good choice, because it contains a locate
        # that is a client, and one that isn't.

        # post the ticket and keep its ID
        id = self.tdb.insertticket(t)

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # now check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 2)

        self.assertEquals(results[0]["client_code"], "CMG01")
        self.assertEquals(results[0]["status"], locate_status.assigned)

        self.assertEquals(results[1]["client_code"], "BLAH01")
        self.assertEquals(results[1]["status"], locate_status.not_a_customer)

        locate_id = results[0]["locate_id"]
        assignments = self.tdb.getrecords("assignment", locate_id=locate_id)
        self.assertEquals(len(assignments), 1)
        # STAF 11 K01 should be emp_id 448, James Nance
        #self.assertEquals(assignments[0]["locator_id"], "448")
        # locator isn't relevant, as long as it's assigned

    def test_Richmond1_routing_2(self):
        # test if ticket without mapinfo is routed to the default locator.
        tl = ticketloader.TicketLoader("../testdata/richmond1-1.txt", chr(12))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Richmond1"])
        del t.locates[1:]
        t.map_page = "" # no more routing based on map_page

        # post the ticket and keep its ID
        id = self.tdb.insertticket(t)

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # now check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 1)

        self.assertEquals(results[0]["client_code"], "CMG01")
        self.assertEquals(results[0]["status"], locate_status.assigned)
        locate_id = results[0]["locate_id"]

        assignments = self.tdb.getrecords("assignment", locate_id=locate_id)
        self.assertEquals(len(assignments), 1)
        from callcenters import FCV1
        self.assertEquals(assignments[0]["locator_id"],
                          FCV1.BusinessLogic.DEFAULT_LOCATOR)

    def test_Richmond2_routing_1(self):
        """ Test "Augusta" routing (method 1). """

        tl = ticketloader.TicketLoader("../testdata/richmond2-1.txt", chr(12))
        raw = tl.tickets[4]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw)

        self.assertEquals(t.work_county, "AUGUSTA")
        # add two locates; one's a customer, the other is not
        t.locates = [locate.Locate("ACR441"),
                     locate.Locate("BLAH02")]
        # make sure ACR441 is active
        a = self.tdb.getrecords('client', call_center='FCV2', oc_code='ACR441')
        self.assertEquals(a[0]['active'], '1')

        id = self.tdb.insertticket(t)

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # now check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 2)

        self.assertEquals(results[0]["client_code"], "ACR441")
        self.assertEquals(results[0]["status"], locate_status.assigned)
        self.assertEquals(results[1]["client_code"], "BLAH02")
        self.assertEquals(results[1]["status"],
         locate_status.not_a_customer)

        locate_id = results[0]["locate_id"]
        assignments = self.tdb.getrecords("assignment", locate_id=locate_id)
        self.assertEquals(len(assignments), 1)
        self.assertEquals(int(assignments[0]["locator_id"]), 399)

    def test_Richmond2_routing_2(self):
        """ Test "Augusta" routing (method 3). """
        tl = ticketloader.TicketLoader("../testdata/richmond2-1.txt", chr(12))
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw)

        # add two locates; one's a customer, the other is not
        t.locates = [locate.Locate("ACR441"),
                     locate.Locate("BLAH02")]

        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_Richmond2_routing_2.txt')

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # now check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 2)

        self.assertEquals(results[0]["client_code"], "ACR441")
        self.assertEquals(results[0]["status"], locate_status.assigned)
        self.assertEquals(results[1]["client_code"], "BLAH02")
        self.assertEquals(results[1]["status"], locate_status.not_a_customer)

        locate_id = results[0]["locate_id"]
        assignments = self.tdb.getrecords("assignment",
                      locate_id=locate_id)
        self.assertEquals(len(assignments), 1)
        self.assertEquals(assignments[0]["locator_id"], "339") # may change

    def test_Richmond2_routing_2a(self):
        """ Test routing (method 2). """
        tl = ticketloader.TicketLoader("../testdata/richmond2-1.txt", chr(12))
        raw = tl.tickets[1]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw)

        # add two locates; one's a customer, the other is not
        t.locates = [locate.Locate("ACR441"),
         locate.Locate("BLAH02")]

        id = self.tdb.insertticket(t)

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # now check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 2)

        self.assertEquals(results[0]["client_code"], "ACR441")
        self.assertEquals(results[0]["status"], locate_status.assigned)
        self.assertEquals(results[1]["client_code"], "BLAH02")
        self.assertEquals(results[1]["status"],
         locate_status.not_a_customer)

        locate_id = results[0]["locate_id"]
        assignments = self.tdb.getrecords("assignment",
                      locate_id=locate_id)
        self.assertEquals(len(assignments), 1)
        #self.assertEquals(assignments[0]["locator_id"], "735") # may change


    def test_Richmond2_routing_3(self):
        """ Test routing (method 4). """
        tl = ticketloader.TicketLoader("../testdata/richmond2-1.txt", chr(12))
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw)
        t.map_page = "" # no mapinfo for this one

        # add two locates; one's a customer, the other is not
        t.locates = [locate.Locate("ACR441"),
         locate.Locate("BLAH02")]

        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_Richmond2_routing_3.txt')

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # now check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 2)

        self.assertEquals(results[0]["client_code"], "ACR441")
        self.assertEquals(results[0]["status"], locate_status.assigned)
        self.assertEquals(results[1]["client_code"], "BLAH02")
        self.assertEquals(results[1]["status"],
         locate_status.not_a_customer)

        locate_id = results[0]["locate_id"]
        assignments = self.tdb.getrecords("assignment",
                      locate_id=locate_id)
        self.assertEquals(len(assignments), 1)
        self.assertEquals(assignments[0]["locator_id"], "339") # may change

    def test_Richmond2_routing_4(self):
        """ Test routing (default locator). """
        tl = ticketloader.TicketLoader("../testdata/richmond2-1.txt", chr(12))
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw)
        t.map_page = ""
        t.work_city = "GAINESVILLE"
        t.work_county = "ALACHUA"

        # add two locates; one's a customer, the other is not
        t.locates = [locate.Locate("ACR441"),
         locate.Locate("BLAH02")]

        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_Richmond2_routing_4.txt')

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # now check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 2)

        self.assertEquals(results[0]["client_code"], "ACR441")
        self.assertEquals(results[0]["status"], locate_status.assigned)
        self.assertEquals(results[1]["client_code"], "BLAH02")
        self.assertEquals(results[1]["status"],
         locate_status.not_a_customer)

        from callcenters import FCV2
        locate_id = results[0]["locate_id"]
        assignments = self.tdb.getrecords("assignment",
         locate_id=locate_id)
        self.assertEquals(len(assignments), 1)
        self.assertEquals(assignments[0]["locator_id"],
         FCV2.BusinessLogic.DEFAULT_LOCATOR)

    def test_Richmond_routing_5(self):
        # what happens when we pass an invalid map_page?
        # this row should slide past all three methods, and return None!
        row = {"work_county": "UNKNOWN", "map_page": "ABC3",
               "ticket_format": "FCV2"}
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        from callcenters import FCV2
        logic = FCV2.BusinessLogic(tr.tdb, [])
        routingresult = logic.locator(row)
        self.assertEquals(routingresult.locator, logic.DEFAULT_LOCATOR)

    def test_NorthCarolina_routing_1(self):
        # make sure CTC01 is a client for FCL1
        _testprep.make_client(self.tdb, "CTC01", "FCL1")

        # grab any old NC ticket
        tl = ticketloader.TicketLoader("../testdata/northcarolina-1.txt")
        raw = tl.getTicket()
        ticket = self.handler.parse(raw, ["NorthCarolina"])
        ticket.locates = map(locate.Locate, ["CTC01", "BLAH99"])
        ticket.work_county = "ALEXANDER"
        ticket.work_city = "TAYLORSVILLE"

        # post it...
        id = self.tdb.insertticket(ticket)

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # now inspect the contents...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 2)
        self.assertEquals(results[0]["client_code"], "CTC01")
        self.assertEquals(results[1]["client_code"], "BLAH99")
        self.assertEquals(results[0]["status"], locate_status.assigned)
        self.assertEquals(results[1]["status"],
         locate_status.not_a_customer)

        # check assignment table
        locate_id = results[0]["locate_id"]
        results = self.tdb.getrecords("assignment", locate_id=locate_id)
        self.assertEquals(len(results), 1)
        #print ">>", results[0]
        #self.assertEquals(results[0]["locator_id"], "2664")  # may change
        # locator doesn't really matter now, as long as it's routed

    def test_NorthCarolina_BEC(self):
        _testprep.add_test_call_center(self.tdb, "FCL1")
        _testprep.make_client(self.tdb, "BEC01", "FCL1")
        _testprep.make_client(self.tdb, "BEC02", "FCL1")
        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina"])
        # make sure BEC01/BEC02 are the only two _clients_
        t.locates = [locate.Locate(c)
                     for c in ["BEC01", "BEC02", "ZZZ02"]]
        id = self.tdb.insertticket(t)

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # check the results... BEC01 and BEC02 should be -R now
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 3)
        self.assertEquals(results[0]["status"], locate_status.assigned)
        self.assertEquals(results[1]["status"], locate_status.assigned)
        self.assertEquals(results[2]["status"],
         locate_status.not_a_customer)

    def test_NorthCarolina_BEC_3(self):
        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina"])
        
        # make sure client exists
        _testprep.make_client(self.tdb, "BTV01", "FCL1")

        t.locates = [locate.Locate(c) for c in ["BLAH99", "BTV01"]]
        id = self.tdb.insertticket(t)

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 2)
        self.assertEquals(results[0]["status"], locate_status.not_a_customer)
        self.assertEquals(results[1]["status"], locate_status.assigned)

        # now "reparse" this ticket...
        t.locates.append(locate.Locate("BEC01"))
        self.assertEquals(t.locates[-1].locate_id, None)
        self.assertEquals(t.locates[-1].ticket_id, None)
        # update this ticket... there should be an insert (BEC01) and a
        # reassign (BLAH99)
        id2 = self.tdb.updateticket(id, t, newlocates=t.locates[-1:],
              reassign={results[0]["locate_id"]: results[0]}, post_image=0)
        self.assertEquals(id, id2)

        # let the router do its thing again
        tr.run(runonce=1)

        # check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 3)
        self.assertEquals(results[0]["status"], locate_status.not_a_customer)
        self.assertEquals(results[1]["status"], locate_status.assigned)
        # BEC01 should not be a customer, because CTC01 is there
        self.assertEquals(results[2]["status"], locate_status.not_a_customer)

    def test_NorthCarolina_BEC_4(self):
        _testprep.add_test_call_center(self.tdb, "FCL1")
        _testprep.make_client(self.tdb, "BEC01", "FCL1")
        _testprep.make_client(self.tdb, "BEC02", "FCL1")
        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina"])
        # add non-clients only
        t.locates = [locate.Locate(c)
                     for c in ["BLAH99", "FOOEY3"]]
        id = self.tdb.insertticket(t)

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 2)
        self.assertEquals(results[0]["status"], locate_status.not_a_customer)
        self.assertEquals(results[1]["status"], locate_status.not_a_customer)

        # now "reparse" this ticket...
        t.locates.append(locate.Locate("BEC01"))
        id2 = self.tdb.updateticket(id, t, newlocates=t.locates[-1:],
              reassign={results[0]["locate_id"]: results[0],
                        results[1]["locate_id"]: results[1]})
        self.assertEquals(id, id2)

        # let the router do its thing again
        tr.run(runonce=1)

        # check the results...
        results = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(results), 3)
        self.assertEquals(results[0]["status"], locate_status.not_a_customer)
        self.assertEquals(results[1]["status"], locate_status.not_a_customer)
        # BEC01 should be a customer, because there are no other clients
        self.assertEquals(results[2]["status"], locate_status.assigned)

    def test_blacklisting_1(self):

        t = self.getticket()  # any old ticket
        t.call_date = None
        id = self.tdb.insertticket(t)

        # blacklist this id
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.log.logger.logfile = StringIO.StringIO()

        #tr.log.lock = 1
        tr.append_to_errorfile(id)

        tr.run(runonce=1)
        log = tr.log.logger.logfile.getvalue()
        tr.log.logger.logfile.close()
        # assert that this ticket_id has been seen and ignored
        self.assert_(id in tr.ignored)
        self.assertTrue('Blacklisted ticket id: %s' % (id,) in log)

    def test_blacklisting_2(self):
        # add data to a blacklist, including blank lines
        f = datadir.datadir.open(blacklist.BUGFILE, "a+")
        print >> f, '\n123\n 333 \n  \n'
        f.close()
                 
        ids = blacklist.BlackList(None).read()
        self.assertEquals(ids, [123, 333])

    def test_FCL1_RXMT_routing(self):
        # test if FCL1's RXMT tickets are added to ticket_ack
        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        t.ticket_type = "RXMT"
        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_FCL1_RXMT_routing.txt')

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # the id above should be in ticket_ack
        acks = self.tdb.getrecords("ticket_ack", ticket_id=id)
        self.assertEquals(len(acks), 1)

    def test_ticket_ack_locator_emp_id(self):
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        t.kind = t.ticket_type = 'EMERGENCY'
        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_ticket_ack_locator_emp_id.txt')
        # NOTE that this only works if this ticket has UQ clients!

        # let the router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.log.lock = 1
        tr.run(runonce=1)

        # the id above should be in ticket_ack
        acks = self.tdb.getrecords("ticket_ack", ticket_id=id)
        self.assertEquals(len(acks), 1)
        locates = self.tdb.getrecords("locate", ticket_id=id)
        locates = [loc for loc in locates if loc['status'] != '-N']
        # not only that, but ticket_ack.locator_emp_id should be set
        empids = [loc['assigned_to'] for loc in locates]
        self.assert_(empids)
        self.assertEquals(empids[0], acks[0]['locator_emp_id'])

    def test_NJ_warnings(self):
        # no "no UQ client" warnings should be sent for NJ/XXX.
        tl = ticketloader.TicketLoader("../testdata/NewJersey-1.txt")
        raw = tl.getTicket()
        raw = raw.replace("CDC = UW4", "CDC = XXX")
        t = self.handler.parse(raw)
        self.assert_(len(t.locates) > 1)

        # the following requires some trickery: we inject our own "method" in
        # the ErrorHandler class, to be able to check if the router attempts
        # to send warnings

        def log_warning(self, obj, dump=0, write=0, send=1):
            raise "Router attempted to send a warning!"

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        errorhandling2.ErrorHandler.log_warning = log_warning
        try:
            tr.run(runonce=1)
        finally:
            # make sure ErrorHandler is put back in its original state, so
            # other tests won't be affected by this
            reload(errorhandling2)
            self.assertNotEqual(
             id(errorhandling2.ErrorHandler.log_warning),
             id(log_warning))

    def test_Atlanta_routing_locator(self):
        # check if assignment of locator is the same after we put a new locator
        # finder mechanism in place

        # get the first ticket, store it, and keep its id
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        id = self.tdb.insertticket(t)

        # let the router run
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        locates = self.tdb.getrecords("locate", ticket_id=id)
        locate_id = locates[-1].get("locate_id")    # last one is USW01
        assignments = self.tdb.getrecords("assignment", locate_id=locate_id)
        self.assertEquals(len(assignments), 1)
        asg = assignments[0]
        #self.assertEquals(asg["locator_id"], "205")
        # actual locator doesn't really matter

    def test_holiday(self):
        # test if holidays really affect setting of due dates.

        tl = ticketloader.TicketLoader("../testdata/Richmond2-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        # set call date to July 2nd, so +48 would be July 4th, but that should
        # not be assigned as the due date because it's a holiday
        t.call_date = "2002-07-02 13:14:15"

        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_holiday.txt')

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        records = self.tdb.getrecords("ticket", ticket_id=id)
        self.assertEquals(len(records), 1)
        rec = records[0]
        # 2002.10.23: due_date is now computed by main.py, no longer by the
        # router.
        #--->8---
        #due_date = date.Date(rec["due_date"]).isodate()
        #self.assertEquals(due_date, "2002-07-08 13:14:15")
        # July 8th is correct. 2002-07-04 is a holiday, and so is 2002-07-05
        # for some call centers, including FCV2. So the next business day is
        # 2002-07-08!

    def test_tickets_to_route(self):
        # add any old ticket
        tl = ticketloader.TicketLoader("../testdata/Atlanta-3.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        self.assert_(t.work_type > "")
        id = self.tdb.insertticket(t)

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)

        results = tr._get_locates()
        self.assert_(len(results) > 0)
        self.assert_(results[0].has_key("work_type"))
        self.assertEquals(results[0].get("work_type", []), t.work_type)

    def test_tickets_to_route_2(self):
        # add any old ticket
        tl = ticketloader.TicketLoader("../testdata/Atlanta-3.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        self.assert_(t.work_type > "")
        id = self.tdb.insertticket(t)

        #results = self.tdb.dbado.call_template("tickets_to_route_noxml")
        results = self.tdb.dbado.call_result("tickets_to_route_noxml")

        #self.assert_(results.find("work_type") > -1)
        self.assert_(results[0].has_key('work_type'))

    def test_do_not_route_until_1(self):
        '''
        Tests that locates on a ticket with a do_not_route_until date in the future are
        not routed
        '''
        tl = ticketloader.TicketLoader("../testdata/Atlanta-3.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw)
        now = date.Date()
        now.inc(1)
        t.do_not_route_until = now.isodate()
        id = self.tdb.insertticket(t)

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.log.logger.logfile = StringIO.StringIO()

        results = tr._get_locates()
        self.assert_(len(results) == 0)
        log = tr.log.logger.logfile.getvalue()
        tr.log.logger.logfile.close()
        # assert that this ticket_id has been seen and ignored
        self.assertTrue('There are 11 pending locates' in log)
        self.assertTrue('There are 0 high profile locates' in log)
        self.assertTrue('There are now 0 high profile locates after association' in log)
        self.assertTrue('There are 0 pending locates after filtering' in log)



    def test_do_not_route_until_2(self):
        '''
        Tests that locates on an emergency ticket with a do_not_route_until date in the future are routed
        '''
        tl = ticketloader.TicketLoader("../testdata/Atlanta-3.txt")
        raw = tl.findfirst('EMERG')
        t = self.handler.parse(raw)
        now = date.Date()
        now.inc(1) # Set the do_not_route_until one day in the future
        t.do_not_route_until = now.isodate()
        id = self.tdb.insertticket(t)

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)

        results = tr._get_locates()
        self.assert_(len(results) == 15)

    def test_do_not_route_until_3(self):
        '''
        Tests that all locates on a ticket with one locate having high_profile
        set and with a do_not_route_until date in the future are routed
        '''
        tl = ticketloader.TicketLoader("../testdata/Atlanta-3.txt")
        raw = tl.getTicket() # Get the first ticket
        t = self.handler.parse(raw)
        now = date.Date()
        now.inc(1) # Set the do_not_route_until one day in the future
        t.do_not_route_until = now.isodate()
        ticket_id = self.tdb.insertticket(t)

        raw = tl.getTicket() # Get the next ticket
        t = self.handler.parse(raw)
        now = date.Date()
        now.inc(1)
        t.do_not_route_until = now.isodate()
        ticket_id = self.tdb.insertticket(t)
        from ticket import Ticket
        ticket = Ticket.load(self.tdb,ticket_id)
        # Set high_priority for the first locate
        self.tdb.updaterecord('locate', 'locate_id',
          int(ticket.locates[0].locate_id), high_profile=1)
        num_to_be_routed = len(ticket.locates)

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.log.logger.logfile = StringIO.StringIO()

        results = tr._get_locates()
        log = tr.log.logger.logfile.getvalue()
        tr.log.logger.logfile.close()
        # assert that this ticket_id has been seen and ignored
        self.assertTrue('There are 30 pending locates' in log)
        self.assertTrue('There are 1 high profile locates' in log)
        self.assertTrue('There are now 19 high profile locates after association' in log)
        self.assertTrue('There are 19 pending locates after filtering' in log)
        self.assert_(len(results) == num_to_be_routed)

    def test_do_not_route_until_4(self):
        '''
        Tests that if the ticket table has not been updated with the
        do_not_route_column, locates are routed as usual
        '''
        # pretend we don't have the do_not_route_until field
        fields = self.tdb.tabledefs['ticket']
        fields = [(k,v) for (k,v) in fields if k != 'do_not_route_until']
        self.tdb.tabledefs._cache['ticket'] = fields

        try:
            tl = ticketloader.TicketLoader("../testdata/Atlanta-3.txt")
            raw = tl.findfirst('EMERG')
            t = self.handler.parse(raw)
            id = self.tdb.insertticket(t)

            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)

            results = tr._get_locates()
            self.assert_(len(results) == 15)
        finally:
            self.tdb.tabledefs.reset()

    def test_do_not_route_until_5(self):
        '''
        Tests that if the ticket table has not been updated with the
        do_not_route_column, locates are routed as usual
        '''

        # pretend we don't have the do_not_route_until field
        fields = self.tdb.tabledefs['ticket']
        fields = [(k,v) for (k,v) in fields if k != 'do_not_route_until']
        self.tdb.tabledefs._cache['ticket'] = fields

        try:
            from ticket import Ticket

            tl = ticketloader.TicketLoader("../testdata/Atlanta-3.txt")
            raw = tl.getTicket() # Get the first ticket
            t = self.handler.parse(raw)
            now = date.Date()
            now.inc(1) # Set the do_not_route_until one day in the future
            t.do_not_route_until = now.isodate()
            ticket_id = self.tdb.insertticket(t)
            ticket = Ticket.load(self.tdb,ticket_id)
            num_to_be_routed = len(ticket.locates)

            raw = tl.getTicket() # Get the next ticket
            t = self.handler.parse(raw)
            now = date.Date()
            now.inc(1)
            t.do_not_route_until = now.isodate()
            ticket_id = self.tdb.insertticket(t)
            ticket = Ticket.load(self.tdb,ticket_id)
            # Set high_priority for the first locate
            self.tdb.updaterecord('locate', 'locate_id',
              int(ticket.locates[0].locate_id), high_profile=1)
            num_to_be_routed += len(ticket.locates)

            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.log.logger.logfile = StringIO.StringIO()

            results = tr._get_locates()
            log = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()
            # assert that this ticket_id has been seen and ignored
            self.assertTrue('There are 30 pending locates' in log)
            self.assertTrue('There are 1 high profile locates' in log)
            self.assertTrue('There are now 19 high profile locates after association' in log)
            self.assertTrue('There are 30 pending locates after filtering' in log)
            self.assert_(len(results) == num_to_be_routed)

        finally:
            self.tdb.tabledefs.reset()

    def test_routing_area_id(self):
        # Test if area_id is correctly set after routing.
        tl = ticketloader.TicketLoader("../testdata/Orlando-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Orlando"])
        # add a FOL1 client; if test fails, maybe this changed again
        t.locates.append(locate.Locate("TLMD03"))
        _testprep.set_client_active(self.tdb, 'FOL1', 'TLMD03') # just to be sure

        locator_id = _testprep.add_test_employee(self.tdb, 'Freddy')
        area_id = _testprep.add_test_area(self.tdb, 'Fred-Area', locator_id)
        self.tdb.delete('county_area', state='FL', county='ORANGE', munic='ORLANDO')
        ca_id = _testprep.add_test_county_area(self.tdb, 'FredTest', area_id, 'FL', 'FOL1')
        self.tdb.runsql("""
         update county_area
         set munic='ORLANDO', county='ORANGE'
         where county_area_id = %s
        """ % (ca_id,))

        id = self.tdb.insertticket(t)

        results = self.tdb.getrecords("ticket", ticket_id=id)
        self.assertEquals(len(results), 1)
        self.assertEquals(results[0].get("route_area_id"), None)

        # let router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        results = self.tdb.getrecords("ticket", ticket_id=id)
        self.assertEquals(len(results), 1)
        self.assert_(results[0]['route_area_id']) # if this fails, it's not routed anymore
        self.assertEquals(results[0]["route_area_id"], area_id)

    def test_pre_routing_FDX1(self):
        # make sure FDX1/D20 exists and is active
        self.tdb.deleterecords('client', oc_code='D20', call_center='FDX1')
        _testprep.add_test_client(self.tdb, 'D20', 'FDX1', active=1)
        # Add a non-null work_priority
        sql = """
         update ticket_ack_match
         set work_priority=10
         where call_center = 'FDX1'
        """
        self.tdb.runsql(sql)

        sql = """
         select work_priority from ticket_ack_match
         where call_center = 'FDX1'
        """
        rows = self.tdb.runsql_result(sql)
        for row in rows:
            self.assertEquals(row["work_priority"],'10')

        tl = ticketloader.TicketLoader("../testdata/Dallas1-1.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Dallas1"])
        t.locates.append(locate.Locate("D20"))

        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_pre_routing_FDX1.txt')

        rows = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(rows), 3)

        # let router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        rows = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(rows[2]["client_code"], "D20")
        locate_id = rows[2]["locate_id"]
        assignments = self.tdb.getrecords("assignment", locate_id=locate_id)
        self.assertEquals(len(assignments), 1)
        row = self.tdb.getrecords("ticket", ticket_id=id)
        self.assertEquals(row[0]["work_priority_id"],"10")

    def test_routing_FDX1_1(self):
        # make sure FDX1/D20 exists and is active
        self.tdb.deleterecords('client', oc_code='D20', call_center='FDX1')
        _testprep.add_test_client(self.tdb, 'D20', 'FDX1', active=1)
        NAME = 'test_routing_FDX1_1'
        emp_id = _testprep.add_test_employee(self.tdb, NAME)
        emp_id_2 = _testprep.add_test_employee(self.tdb, NAME+'2')
        area_id = _testprep.add_test_area(self.tdb, NAME, emp_id)
        area_id_2 = _testprep.add_test_area(self.tdb, NAME+'2', emp_id_2)
        map_id = _testprep.add_test_map(self.tdb, "FOOFUR", 'TX', 'DALLAS', 'FDX1')
        _testprep.add_test_map_page(self.tdb, map_id, 456)
        _testprep.add_test_map_cell(self.tdb, map_id, 456, 'A', '1', area_id)
        _testprep.add_test_map_cell(self.tdb, map_id, 456, 'A', 'W', area_id_2)

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH, "Dallas1-1.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Dallas1"])
        t.locates.append(locate.Locate("D20"))
        t.map_page = "FOOFUR 456W,A"

        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_pre_routing_FDX1.txt')

        rows = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(rows), 3)

        # let router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #Clear logs
        tr.log.logger.logfile.truncate(0)
        #tr.log.lock = 1
        tr.run(runonce=1)
        # Verify the locate and method
        logfile_name = tr.log.logger.logfile.name
        match_str = "locator %s found (BusinessLogic.route_map_area_with_code)" % (emp_id_2,)
        match, next_log_entry = SearchLogFile(logfile_name,match_str)
        self.assertNotEquals(match,None)

        # Clean up
        _testprep.delete_test_map_cell(self.tdb, map_id, 456, 'A', '1', area_id)
        _testprep.delete_test_map_cell(self.tdb, map_id, 456, 'A', 'W', area_id_2)
        _testprep.delete_test_map_page(self.tdb, map_id, 456)
        _testprep.delete_test_map(self.tdb, "FOOFUR", 'TX', 'DALLAS', 'FDX1')
        _testprep.delete_test_area(self.tdb, NAME, emp_id)
        _testprep.delete_test_area(self.tdb, NAME+'2', emp_id_2)
        _testprep.delete_test_employee(self.tdb, NAME)
        _testprep.delete_test_employee(self.tdb, NAME+'2')
        _testprep.clear_database(self.tdb)
        self.tdb.delete("client", oc_code = 'D20', call_center = 'FDX1')

    def test_routing_FDX1_2(self):
        """
        Test FDX1 mapsco routing
        """
        NAME = 'test_routing_FDX1_1'
        emp_id = _testprep.add_test_employee(self.tdb, NAME)
        area_id = _testprep.add_test_area(self.tdb, NAME, emp_id)
        map_id = _testprep.add_test_map(self.tdb, "FOOFUR", 'TX', 'DALLAS', 'FDX1')
        _testprep.add_test_map_page(self.tdb, map_id, 39)
        # Add map_cell row for A to Z
        for letter in string.ascii_uppercase:
            _testprep.delete_test_map_cell(self.tdb, map_id, 39, letter, 'A', area_id)
            _testprep.add_test_map_cell(self.tdb, map_id, 39, letter, 'A', area_id)

        _testprep.set_client_active(self.tdb, 'FDX1', 'MQ4')

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "FDX1", "FDX1-2007-12-23-00044.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Dallas1',])

        self.assertEquals(t.map_page,'MAPSCO 39A,T')
        self.assertEquals(t.ticket_format,'FDX1')

        id = self.tdb.insertticket(t)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, t, 'test_pre_routing_FDX1.txt')

        rows = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(rows), 1)

        # let router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #Clear logs
        tr.log.logger.logfile.truncate(0)
        #tr.log.lock = 1
        tr.run(runonce=1)
        # Verify the locate and method
        logfile_name = tr.log.logger.logfile.name
        match_str = "locator %s found (BusinessLogic.route_map_area_with_code)" % (emp_id,)
        match, next_log_entry = SearchLogFile(logfile_name,match_str)
        self.assertNotEquals(match,None)

        # Clean up
        for letter in string.ascii_uppercase:
            _testprep.delete_test_map_cell(self.tdb, map_id, 39, letter, 'A', area_id)
        _testprep.delete_test_map_page(self.tdb, map_id, 39)
        _testprep.delete_test_map(self.tdb, "FOOFUR", 'TX', 'DALLAS', 'FDX1')
        _testprep.delete_test_area(self.tdb, NAME, emp_id)
        _testprep.delete_test_employee(self.tdb, NAME)
        _testprep.clear_database(self.tdb)

    def test_route_multiple(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH, "Atlanta-1.txt"))
        ids = []
        emergencies = []
        should_be_ticket_ack = 0
        for i in range(5):
            # one of these is an emergency ticket
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["Atlanta"])
            if i == 3:
                t.ticket_type = 'DAMAGE'
            id = self.tdb.insertticket(t)
            # insert_ticket_ack needs the version
            self.tdb.add_ticket_version(id, t, 'test_route_multiple_%d.txt' % (i,))
            if i == 3:
                should_be_ticket_ack = id
            ids.append(id)
            if t.kind.startswith("EMER"):
                emergencies.append(id)

        # let router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # make sure it routes emergencies first, and ticket_ack next
        self.assertEquals(len(emergencies), 1)
        self.assertEquals(len(ids), 5)
        self.assertEquals(tr.routed[0], emergencies[0])
        self.assertEquals(tr.routed[1], should_be_ticket_ack)

    def test_routing_ward(self):
        tl = ticketloader.TicketLoader("../testdata/PA7501-ward.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["PA7501"])
        t.ticket_format = "XYZ"
        self.assertEquals(t.ward, '40')
        ticket_id = self.tdb.insertticket(t)
        _testprep.add_test_call_center(self.tdb, "XYZ")
        _testprep.make_client(self.tdb, "YA", "XYZ")

        # check if ward field is actually stored and retrieved
        t2 = self.tdb.getticket(ticket_id)
        self.assertEquals(t2.ward, '40')

        # set dummy businesslogic class
        import callcenters.c7501
        class XYZBusinessLogic(callcenters.c7501.BusinessLogic):
            DEFAULT_LOCATOR = 300
        businesslogic.XYZBusinessLogic = XYZBusinessLogic
        # NOTE: this test will break once we stop looking for BusinessLogic
        # classes in businesslogic.py

        from callcenters import Atlanta
        self.tdb.delete("routing_list", call_center="XYZ")
        locator = Atlanta.BusinessLogic.DEFAULT_LOCATOR
        area_id = _testprep.add_test_area(self.tdb, "lubbers", locator)
        _testprep.add_test_routing_list(self.tdb, "XYZ", "ward", "40", area_id)

        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.log.lock = 1
        tr.run(runonce=1)

        locates = self.tdb.getrecords("locate", ticket_id=ticket_id)
        ok = [loc for loc in locates if loc['status'] == '-R']
        self.assertEquals(len(ok), 1)
        self.assertEquals(ok[0]['client_code'], 'YA')
        self.assertEquals(ok[0]['assigned_to'], locator)

    def test_database_glitch(self):
        tr = ticketrouter.TicketRouter(1, verbose=0)
        try:
            raise Exception("Fred was deadlocked on lock resources, Wilma")
        except:
            self.assertEquals(tr.is_database_glitch(), True)
        else:
            self.fail("Failed to flag exception as database glitch")

    #
    # test_routinglist_client_code
    # - Make test client code, area, employee
    # - Put test data in routing list table, using client_code
    # - Route an actual ticket with the given client
    # - See where it ends up
    # - ...and where the other locates on the ticket end up?
    # - Of course, we must use a call center that uses this routing method!

    def test_routinglist_client_code(self):
        new_client_id = _testprep.add_test_client(self.tdb, 'ROUTE69', 'NewJersey')
        new_emp_id = _testprep.add_test_employee(self.tdb, 'HansDeBooij')
        new_area_id = _testprep.add_test_area(self.tdb, 'Area49', new_emp_id)
        _testprep.add_test_routing_list(self.tdb, 'NewJersey', 'client_code',
         'ROUTE69', new_area_id)

        tl = ticketloader.TicketLoader("../testdata/NewJersey-1.txt")
        raw = tl.tickets[0]
        raw = raw.replace("UW4", "ROUTE69")
        t = self.handler.parse(raw, ['NewJersey'])
        ticket_id, locate_ids = t.insert(self.tdb)
        tickets = self.tdb.getrecords("ticket", ticket_id=ticket_id)
        self.assertEquals(len(tickets), 1)

        # let router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #tr.log.lock = 1
        tr.run(runonce=1)

        # locate should have been routed to the new employee
        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assertEquals(len(locates), 1)
        self.assertEquals(locates[0]['client_code'], 'ROUTE69')
        self.assertEquals(locates[0]['assigned_to'], new_emp_id)

    def execute_raise(self,*args,**kwargs):
        raise dbinterface.DBFatalException(self.err_msg)

    def test_ep_1(self):
        """
        Mock the insertticket method so it raises a DBFatalException.
        Verify that no email is sent if the exception is from a deadlock.
        """
        self.config = config.getConfiguration()
        datadir_path = datadir.datadir.path
        log_dir = os.path.join(datadir_path, self.config.logdir)

        self.err_msg = "Transaction (Process ID 94) was deadlocked on lock"+\
                       " resources with another process and has been chosen as the"+\
                       " deadlock victim. Rerun the transaction."
        # make the ticketrouter first, because we need one of its methods
        ticket = self.getticket()
        ticket.kind = ticket.ticket_type = "EMERGENCY"  # for ticket_ack
        ticket.locates = [] # do not post any locates
        # post ticket to database
        id = self.tdb.insertticket(ticket)
        # insert_ticket_ack needs the version
        self.tdb.add_ticket_version(id, ticket, 'test_ep_1.txt')

        # create a TicketRouter
        tr = ticketrouter.TicketRouter(1, verbose=0)
        #Clear logs
        tr.log.logger.logfile.truncate(0)

        with Mockery(tr, 'task', self.execute_raise):

            # now, let's add an assignable locate record to that ticket...
            value = tr._get_customers()[('USW01','Atlanta')]
            client = 'USW01'
            call_center = 'Atlanta'
            client_id = value
            sql = """
             insert locate
             (ticket_id, client_code, status, client_id)
             values ('%s', '%s', '%s', '%s')
            """ % (id, client, locate_status.blank, client_id)
            self.tdb.runsql(sql)

            # we now have at least one unassigned locate, viz. the new one
            # start the ticket router to find and assign it:
            with Mockery(sys, 'stderr', StringIO.StringIO()):
                tr.run(runonce=1)

        logfile_name = tr.log.logger.logfile.name
        # Verify that no email was sent
        match, next_log_entry = SearchLogFile(logfile_name,
          "Mail notification sent to")
        self.assertEquals(next_log_entry,None)
        # Verify that there was the assigned exception
        match, next_log_entry = SearchLogFile(logfile_name,
          "raise dbinterface.DBFatalException")
        self.assertNotEquals(next_log_entry, None)

    def test_ep_2(self):
        """
        Mock the _post_updates_ado method so it raises a DBFatalException.
        Verify that no email is sent if the exception is from a deadlock.
        """
        self.err_msg = "Transaction (Process ID 94) was deadlocked on lock"+\
                       " resources with another process and has been chosen as the"+\
                       " deadlock victim. Rerun the transaction."

        # get the first ticket, store it, and keep its id
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        id = self.tdb.insertticket(t)

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #Clear logs
        tr.log.logger.logfile.truncate(0)

        with Mockery(tr, '_post_updates_ado', self.execute_raise):
            with Mockery(sys, 'stderr', StringIO.StringIO()):
                tr.run(runonce=1)

        logfile_name = tr.log.logger.logfile.name
        # Verify that no email was sent
        match, next_log_entry = SearchLogFile(logfile_name,
          "Mail notification sent to")
        if next_log_entry is not None:
            tr.log.logger.logfile.seek(0)
            for line in tr.log.logger.logfile.readlines():
                print line
        self.assertEquals(next_log_entry,None)
        # Verify that there was the assigned exception
        match, next_log_entry = SearchLogFile(logfile_name,
          "raise dbinterface.DBFatalException")
        self.assertNotEquals(next_log_entry,None)

    def test_ep_3(self):
        """
        Test _post_updates_ado for missing locate
        Mock the db.getrecords method so it returns an empty list.
        Verify that an error message is generated.
        """

        # get the first ticket, store it, and keep its id
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        id = self.tdb.insertticket(t)

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #Clear logs
        tr.log.logger.logfile.truncate(0)
        def getrecords(*args,**kwargs):
            """
            Inspect the stack and only mock getrecords if the calling function name
            is _post_updates_ado. Otherwise, do the real thing.
            """
            # XXX incompatible with IronPython
            for item in inspect.stack():
                if item[3] == '_post_updates_ado':
                    if args[0] == 'locate':
                        return []
                    else:
                        return True
            tdb = ticket_db.TicketDB()
            return tdb.getrecords(*args,**kwargs)

        with Mockery(tr.tdb, 'getrecords', getrecords):
            with Mockery(sys, 'stderr', StringIO.StringIO()):
                tr.run(runonce=1)

        logfile_name = tr.log.logger.logfile.name
        # Verify that locate does not exist message is in the log file
        match, next_log_entry = SearchLogFile(logfile_name,"Locate does not exist")
        self.assertNotEquals(match, None)

    def test_ep_4(self):
        """
        Test _post_updates_ado for missing locator
        Mock the db.getrecords method so it returns an empty list.
        Verify that an error message is generated.
        """

        # get the first ticket, store it, and keep its id
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Atlanta"])
        id = self.tdb.insertticket(t)

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        #Clear logs
        tr.log.logger.logfile.truncate(0)
        def getrecords(*args,**kwargs):
            """
            Inspect the stack and only mock getrecords if the calling function name
            is _post_updates_ado. Otherwise, do the real thing.
            """
            for item in inspect.stack():
                if item[3] == '_post_updates_ado':
                    if args[0] == 'employee':
                        return []
                    else:
                        return True
            tdb = ticket_db.TicketDB()
            return tdb.getrecords(*args,**kwargs)

        with Mockery(tr.tdb, 'getrecords', getrecords):
            with Mockery(sys, 'stderr', StringIO.StringIO()):
                tr.run(runonce=1)

        logfile_name = tr.log.logger.logfile.name
        # Verify that locate does not exist message is in the log file
        match, next_log_entry = SearchLogFile(logfile_name,
          "Employee does not exist")
        self.assertNotEquals(match, None)

    def test_with_profiling(self):
        from show_profile_report import show_profile_report
        t = self.getticket()

        # inject some locates that are not pre-assigned to a locator
        t.locates = [locate.Locate("ATL01"), locate.Locate("USW01")]

        # post the ticket and keep the ID
        id = self.tdb.insertticket(t)
        # note that this does NOT set the client_id of locates

        # let the ticket router have its way
        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.log.lock = 1
        tr.run(runonce=1)

        prefix = "router_profile_"
        show_profile_report(prefix, '', tr.tdb, verbose=0)
        d = date.Date().isodate()
        d = d.replace("-", "").replace(":", "").replace(" ", "_")
        basename = prefix + "_" + d
        self.assertTrue(os.path.exists(basename + ".txt"))
        self.assertTrue(os.path.exists(basename + "_csv.txt"))
        os.remove(basename + ".txt")
        os.remove(basename + "_csv.txt")

if __name__ == "__main__":

    unittest.main()



