import logging
import os
import string
import config

import ticket_db
import call_centers

#create logger
logger = logging.getLogger("__init__")
logger.setLevel(logging.DEBUG)
#create console handler and set level to error
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
#create file handler and set level to debug
fh = logging.FileHandler("spam.log")
fh.setLevel(logging.DEBUG)
#create formatter
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
#add formatter to ch and fh
ch.setFormatter(formatter)
fh.setFormatter(formatter)
#add ch and fh to logger
logger.addHandler(ch)
logger.addHandler(fh)

def setup_package():
    logger.info("In setup_package")
    """ Call center is in database? """
    # check that all call centers are actually in the database
    names = []
    tdb = ticket_db.TicketDB()
    results = tdb.dbado.getrecords("call_center")
    ccs = [row["cc_code"] for row in results]
    for key in call_centers.cc2format.keys():
        if key not in ccs:
            names.append(key)
            # insert missing call center in database
            sql = """
             insert call_center (cc_code, cc_name)
             values ('%s', 'test')
            """ % (key,)
            tdb.runsql(sql)

    if len(names) != 0:
        logger.info("These call centers were not in database and were inserted: %s" % (names,))


def teardown_package():
    logger.info("In teardown_package")
