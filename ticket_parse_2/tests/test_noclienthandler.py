# test_noclienthandler.py

import site; site.addsitedir('.')
import unittest
#
import businesslogic
import client
import locate
import noclienthandler
import ticket_db
import ticketloader
import ticketparser
import _testprep

test_log = []

class TestLog:
    def log_event(self, log_message, *args, **kwdargs):
        test_log.append(log_message)

class TestNoClientHandler(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.set_cc_routing_file()

    def test_has_uq_client(self):
        nchandler = noclienthandler.NoClientHandler(self.tdb, {}, TestLog())
        huq = nchandler.has_uq_client
        logic = businesslogic.getbusinesslogic("Atlanta", self.tdb, [])
        locates = ["A", "B", "C", "D"]
        clients = ["A", "E", "Q"]
        self.assert_(huq(locates, logic, clients))

        locates = ["P", "R", "S"]
        self.assertEquals(huq(locates, logic, clients), 0)

        locates = []
        self.assertEquals(huq(locates, logic, clients), 0)

        # now test a different flavor of logic
        logic = businesslogic.getbusinesslogic("NewJersey", self.tdb, [])
        locates = ["XXX", "YYY", "ZZZ"]
        # no clients as-is, but "XXX" is a special case
        self.assertEquals(huq(locates, logic, clients), 11)

    def test_check(self):
        global test_log

        # Parse an Atlanta ticket with USW01 as a locate
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        t1 = self.handler.parse(raw, ["Atlanta"])

        logic = businesslogic.getbusinesslogic("Atlanta", self.tdb, [])

        # Test with USW01 as a UQ client
        clients = {'Atlanta': [
          client.Client(client_code='USW01'),
          client.Client(client_code='ATL01XXX'),
          client.Client(client_code='ABS01XXX')]}
        nchandler = noclienthandler.NoClientHandler(self.tdb, clients, TestLog())
        nchandler.check(t1, 1001, logic)
        # No warnings
        self.assertEquals(test_log, [])
        self.assertEquals(nchandler.ticket_info, [])

        # Test with no UQ clients
        clients = {'Atlanta': [
          client.Client(client_code='USW01XXX'),
          client.Client(client_code='ATL01XXX'),
          client.Client(client_code='ABS01XXX')]}
        nchandler = noclienthandler.NoClientHandler(self.tdb, clients, TestLog())
        nchandler.check(t1, 1001, logic)
        self.assertEquals(test_log, [
          'No UQ clients found for Atlanta ticket(s)',
          'Locates on ticket: AGL18,ATT02,ATTBB,BSCA,DOTI,EPT50,EPT52,FUL02,GP145,MEA70,OMN01,TCG01,USW01',
          'Atlanta clients: USW01XXX,ATL01XXX,ABS01XXX'])
        self.assertEquals(nchandler.ticket_info, [
         ('Atlanta', 1001, t1.ticket_number, t1.image)])

        # Clear the test log
        test_log = []

        # Parse a LOR1 ticket with NNG01 as a locate: 
        tl = ticketloader.TicketLoader("../testdata/sample_tickets/LOR1/LOR1-2009-02-03-00500.txt")
        raw = tl.getTicket()
        t2 = self.handler.parse(raw, ["Oregon2"])

        logic = businesslogic.getbusinesslogic("LOR1", self.tdb, [])

        # Test with PGE01 as a UQ client
        clients = {'LOR1': [
          client.Client(client_code='PGE01'),
          client.Client(client_code='PPL15XXX'),
          client.Client(client_code='FALCON02XXX')]}
        nchandler = noclienthandler.NoClientHandler(self.tdb, clients, TestLog())
        nchandler.check(t2, 1002, logic)
        # No warnings
        self.assertEquals(test_log, [])
        self.assertEquals(nchandler.ticket_info, [])

        # Parse a LOR1 ticket with no UQ locates: 
        tl = ticketloader.TicketLoader("../testdata/sample_tickets/LOR1/LOR1-2009-06-29-01252.txt")
        raw = tl.getTicket()
        t3 = self.handler.parse(raw, ["Oregon2"])

        # Test with no UQ clients
        nchandler.check(t3, 1003, logic)
        self.assertEquals(test_log, [
          'No UQ clients found for LOR1 ticket(s)',
          'Locates on ticket: PPL15',
          'LOR1 clients: PGE01,PPL15XXX,FALCON02XXX'])
        self.assertEquals(nchandler.ticket_info, [
          ('LOR1', 1003, '9128876', t3.image)])

        # Parse a 2nd LOR1 ticket with no UQ locates: 
        tl = ticketloader.TicketLoader("../testdata/sample_tickets/LOR1/LOR1-2009-06-29-00153.txt")
        raw = tl.getTicket()
        t4 = self.handler.parse(raw, ["Oregon2"])

        # Test with no UQ clients
        nchandler.check(t4, 1004, logic)
        self.assertEquals(test_log[3:], [
          'No UQ clients found for LOR1 ticket(s)',
          'Locates on ticket: FALCON02',
          'LOR1 clients: PGE01,PPL15XXX,FALCON02XXX'])
        self.assertEquals(nchandler.ticket_info, [
          ('LOR1', 1003, '9128876', t3.image),
          ('LOR1', 1004, '9127890', t4.image)])

if __name__ == "__main__":

    unittest.main()

