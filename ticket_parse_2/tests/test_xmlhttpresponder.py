# test_xmlhttpresponder.py
# FIXME: file is full of copy & paste programming. :-(
# Move to test_xmlhttpresponder_1391.py (and similar) where possible.
# FIXME: we really shouldn't rely on log output.
# FIXME: global and class-level dependencies. >=(

from __future__ import with_statement
import copy
import datetime
import os
import re
import site; site.addsitedir('.')
import string
import StringIO
import time
import unittest
import xml.etree.ElementTree as ET
#
import _testprep
import abstractresponder
import businesslogic
import callcenters # package
import call_centers # module
import client
import config
import date
import locate
import locate_status
import mail2
import ticket_db
import ticketloader
import ticketparser
import ticketrouter
import xmlhttpresponder
from search_log_file import SearchLogFile
from mockery import *

consolidated_response_groups = [
    ['KC', 'KCG'],
    ['KD', 'KDG'],
    ['KE', 'KEG'],
    ['KF', 'KFG'],
    ['KG', 'KGG'],
]

def consolidate_response_status(self, rows):
    assert rows, "consolidate_response_status cannot be called with 0 rows"
    statuses = [row['status'] for row in rows]
    if 'M' in statuses:
        return 'M'
    elif 'C' in statuses:
        return 'C'
    else:
        return rows[0]['status']

class DummyText:
    def __init__(self, s):
        self.Text = self.xml = s

def create_mock_responder(rows, *args, **kwargs):

    class MockXMLHTTP:
        def open(self, *args, **kwargs): pass
        def setRequestHeader(self, *args, **kwargs): pass
        def send(self, *args, **kwargs): pass
        responseXML = DummyText("response posted")

    class MockXMLHTTPResponder(xmlhttpresponder.XMLHTTPResponder):
        sent = []
        xml_sent = []
        def __init__(self, *args, **kwargs):
            abstractresponder.AbstractResponder.__init__(self, *args, **kwargs)
            self.xmlhttp = MockXMLHTTP()

        def send_response(self, ticket, membercode, responsecode,
                          explanation=None, locator_name='', version='0'):
            result = xmlhttpresponder.XMLHTTPResponder.send_response(self,
                     ticket, membercode, responsecode, explanation,
                     locator_name, version)
            self.sent.append([ticket, membercode, responsecode, explanation,
                              version])
            return result

        def get_pending_responses_raw(self, call_center):
            sql = "exec get_pending_responses '%s'" % (call_center,)
            rows_from_db = self.dbado.runsql_result(sql)
            if not rows_from_db:
                return [row for row in rows
                        if row['ticket_format'] == call_center]
            return rows_from_db

        def make_xml(self, *args, **kwargs):
            xmlblurb = xmlhttpresponder.XMLHTTPResponder.make_xml(self, *args,
                       **kwargs)
            self.__class__.xml_sent.append(xmlblurb)
            return xmlblurb

    return MockXMLHTTPResponder(*args, **kwargs)

class log:
    def __init__(self):
        self.buff = []
    def log_event(self, msg):
        self.buff.append(msg)

class TestXMLHTTPResponder(unittest.TestCase):

    # mock various config methods
    #configuration = config.getConfiguration()
    #conf_copy = copy.deepcopy(configuration)

    def setUp(self):
        import config; cfg = config.getConfiguration(reload=True)
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.test_emp_id = _testprep.add_test_employee(self.tdb,
         "Test Employee 2", first_name="John", last_name="Tester")
        # creates a deep copy of the real configuration before each test, to be
        # used by said test without affecting the original configuration
        self.cfg = copy.deepcopy(config.getConfiguration())
        # to use this properly, do this inside tests:
        #   with Mockery(config, 'getConfiguration', lambda: self.cfg): ...

    def test_001(self):
        x = xmlhttpresponder.XMLHTTPResponder(verbose=0)
        x.log.lock = 1
        x.load_responder_data('7501', None)
        self.assertEquals(x.responderdata.name, '7501')

    def test_send_response_1(self):
        """
        Test the send_response method with bad xml
        """

        x = xmlhttpresponder.XMLHTTPResponder(verbose=0)
        x.load_responder_data('7501', None) # load any old responder data
        x.log.logger.logfile = StringIO.StringIO()
        with Mockery(x, 'make_xml', lambda s, *a, **kw: '& Bogus xml > <'):
            self.assertRaises(abstractresponder.ResponderError,
              x.send_response, '1234', 'A', '1')
            log = x.log.logger.logfile.getvalue()
            x.log.logger.logfile.close()
            self.assertTrue("XML response failed to parse" in log)

    def test_send_response_2(self):
        """
        Test the send_response method with escaped xml
        """
        x = xmlhttpresponder.XMLHTTPResponder(verbose=0)
        class reply:
            def __init__(self,text):
                self.Text = text
        class mock_xmlhttp:
            def __init__(self):
                self.responseXML = reply('Hello')
            def open(self,*args,**kwargs):
                pass
            def setRequestHeader(self,*args,**kwargs):
                pass
            def send(self,*args,**kwargs):
                pass
        x.xmlhttp = mock_xmlhttp()
        rd = callcenters.get_xmlhttpresponderdata('FDX1')()
        rd.log = log()
        def get_ticket_notes():
            return 'Notes &<>'
        rd.get_ticket_notes = get_ticket_notes
        rd.row = {'closed_date':None,
                  'first_arrival_date':date.Date().isodate()}
        rd.id = 'Id_&<>'
        rd.ONE_CALL_CENTER = "OCC &<>"
        rd.post_url = 'blah'
        rd.resp_url = 'foo'
        rd.translations = {}
        x.responderdata = rd
        x.send_response('1234', '1234', '1', explanation='Explanation',
                        locator_name='', version='0')

    def test_make_xml(self):
        """
        Test the make_xml method with un-escapeable string (fake)
        """
        import xml
        with Mockery(xml.sax.saxutils, 'escape',
             lambda s, *a, **kw: mock_exception(Exception, 'grrr')):

            x = xmlhttpresponder.XMLHTTPResponder(verbose=0)
            x.log.logger.logfile = StringIO.StringIO()
            self.assertRaises(abstractresponder.ResponderError, x.make_xml,
              '1234', 'A', '1', None, '', '0')
            log = x.log.logger.logfile.getvalue()
            x.log.logger.logfile.close()
            self.assertTrue(
              "Unable to escape special characters in explanation" in log)

    def test_consolidate_responses(self):
        with Mockery(config, 'getConfiguration', lambda: self.cfg):
            z = self.cfg.xmlhttpresponders['7501'] # must exist
            z['mappings']['X'] = ('001', '')
            z['mappings']['Y'] = ('002', '')
            z['mappings']['P'] = ('003', '')
            z['mappings']['C'] = ('004', '')
            z['mappings']['M'] = ('005', '')
            z['clients'] = [] # accept all clients
            z['send_emergencies'] = 0
            z['send_3hour'] = 0

            rows = [
              {'ticket_id': 1, 'locate_id': 100, 'client_code': 'KC',  'status': 'X',},
              {'ticket_id': 1, 'locate_id': 101, 'client_code': 'KCG', 'status': 'Y',},
              {'ticket_id': 1, 'locate_id': 102, 'client_code': 'KA',  'status': 'P',},
              {'ticket_id': 2, 'locate_id': 103, 'client_code': 'XYZ',  'status': 'P',},
            ]
            for row in rows:
                row['ticket_format'] = '7501'
                row['ticket_type'] = 'NORMAL'

            import callcenters.c7501
            b = callcenters.c7501.BusinessLogic
            b.consolidated_response_groups = consolidated_response_groups
            b.consolidate_response_status = consolidate_response_status

            xr = create_mock_responder(rows, verbose=0)
            default = {"kind": "NORMAL", "ticket_type": "test"}

            with Mockery(xr.tdb, 'getticket', lambda *a, **kw: default):
                xr.log.lock = 1
                xr.respond_all()
                self.assertEquals(xr.sent[0], ['', 'KA', '003', '', ''])
                self.assertEquals(xr.sent[1], ['', 'XYZ', '003', '', ''])
                self.assertEquals(xr.sent[2], ['', 'KC', '001', '', ''])

                xr.sent[:] = []
                rows[0]['status'] = 'C'
                xr.respond_all()
                self.assertEquals(xr.sent[0], ['', 'KA', '003', '', ''])
                self.assertEquals(xr.sent[1], ['', 'XYZ', '003', '', ''])
                self.assertEquals(xr.sent[2], ['', 'KC', '004', '', ''])

                xr.sent[:] = []
                rows[1]['status'] = 'M'
                xr.respond_all()
                self.assertEquals(xr.sent[0], ['', 'KA', '003', '', ''])
                self.assertEquals(xr.sent[1], ['', 'XYZ', '003', '', ''])
                self.assertEquals(xr.sent[2], ['', 'KC', '005', '', ''])

    def test_SCA1(self):
        with Mockery(config, 'getConfiguration', lambda: self.cfg):
            # assumes SCA1 info in testdata/datadir/config.xml
            self.assert_(self.cfg.xmlhttpresponders.get('SCA1')), \
             "SCA1 XMLHTTP block required in config.xml"
            # use custom mappings so we're not dependent on what happens to be in
            # config.xml
            z = self.cfg.xmlhttpresponders['SCA1']
            z['all_versions'] = 0
            z['mappings']['M'] = ('1', '')
            z['mappings']['N'] = ('2', '')
            z['clients'] = ['COX01', 'COX04', 'COX99']
            # make sure we don't have 'all_versions' set
            self.assertEquals(z.get('all_versions', 0), 0)

            rows = [
              {'ticket_id': 1, 'locate_id': 100, 'client_code': 'ABC01',  'status': 'C', 'closed_date': None},
              {'ticket_id': 1, 'locate_id': 101, 'client_code': 'COX01', 'status': 'M', 'closed_date': None},
              {'ticket_id': 1, 'locate_id': 102, 'client_code': 'BAH05',  'status': 'P', 'closed_date': None},
              {'ticket_id': 2, 'locate_id': 103, 'client_code': 'COX04',  'status': 'N', 'closed_date': None},
            ]
            for row in rows:
                row['ticket_format'] = 'SCA1'
                row['ticket_type'] = 'NORMAL'

            xr = create_mock_responder(rows, verbose=0)
            xr.log.logger.logfile = StringIO.StringIO()
            default = {"kind": "NORMAL", "ticket_type": "test"}
            with Mockery(xr.tdb, 'getticket', lambda *a, **kw: default):
                xr.log.lock = 1
                xr.respond_all()
                log = xr.log.logger.logfile.getvalue()
                xr.log.logger.logfile.close()
                # Verify that locdatetime injection logged
                self.assertEquals(log.count("Location date/time injected into xml based on current date/time"),2)
                self.assertEquals(len(xr.sent), 2)
                self.assertEquals(xr.sent[0], ['', 'COX01', '1', '', ''])
                self.assertEquals(xr.sent[1], ['', 'COX04', '2', '', ''])

    def test_SCA1_2(self):
        """
        Test that responses are sent if send_emergency is not specified
        """
        with Mockery(config, 'getConfiguration', lambda: self.cfg):
            # assumes SCA1 info in testdata/datadir/config.xml
            self.assert_(self.cfg.xmlhttpresponders.get('SCA1')), \
             "SCA1 XMLHTTP block required in config.xml"
            # use custom mappings so we're not dependent on what happens to be in
            # config.xml
            z = self.cfg.xmlhttpresponders['SCA1']
            z['all_versions'] = 0
            z['mappings']['M'] = ('1', '')
            z['mappings']['N'] = ('2', '')
            z['clients'] = ['COX01', 'COX04', 'COX99']
            # make sure we don't have 'all_versions' set
            self.assertEquals(z.get('all_versions', 0), 0)

            rows = [
              {'ticket_id': 1, 'locate_id': 100, 'client_code': 'ABC01',  'status': 'C', 'closed_date' : None},
              {'ticket_id': 1, 'locate_id': 101, 'client_code': 'COX01', 'status': 'M', 'closed_date' : None},
              {'ticket_id': 1, 'locate_id': 102, 'client_code': 'BAH05',  'status': 'P', 'closed_date' : None},
              {'ticket_id': 2, 'locate_id': 103, 'client_code': 'COX04',  'status': 'N', 'closed_date' : None},
            ]
            for row in rows:
                row['ticket_format'] = 'SCA1'
                row['ticket_type'] = 'EMERGENCY'
                row['ticket_number'] = 'X1234'

            xr = create_mock_responder(rows, verbose=0)
            xr.log.logger.logfile = StringIO.StringIO()
            default = {"kind":"EMERGENCY","ticket_type":"test"}
            with Mockery(xr.tdb, 'getticket', lambda *a, **kw: default):
                xr.log.lock = 1
                xr.respond_all()
                log = xr.log.logger.logfile.getvalue()
                xr.log.logger.logfile.close()
                # Verify that there are two responses
                self.assertEquals(len(xr.sent), 2)
                send = re.compile("Sending Response: X1234")
                self.assertEqual(len(send.findall(log)),2)

    def test_make_xml_SCA1(self):
        self.assert_(self.cfg.xmlhttpresponders.get('SCA1')), \
         "SCA1 XMLHTTP block required in config.xml"
        row = {'closed_date': None}

        xr = create_mock_responder(row, verbose=0)

        xr.log.lock = 1
        xr.load_responder_data('SCA1', None)
        self.assertEquals(xr.responderdata.name, 'SCA1')
        xr.responderdata.row = row
        xml = xr.make_xml(ticket='123', membercode='BLAH01', responsecode='42',
              explanation='geen', locator_name='bob the locator', version='2')
        self.assert_(xml.count("<Ticket_Number>123<"))
        self.assert_(xml.count("<Version_Number>2<"))

    def test_SCA1_multiple_versions(self):
        self.assert_(self.cfg.xmlhttpresponders.get('SCA1')), \
         "SCA1 XMLHTTP block required in config.xml"
        # use custom mappings so we're not dependent on what happens to be in
        # config.xml
        z = self.cfg.xmlhttpresponders['SCA1']
        z['mappings']['M'] = ('1', '')
        z['mappings']['N'] = ('2', '')
        z['clients'] = ['COX01', 'COX04', 'COX99']
        z['all_versions'] = 1

        self.tdb.delete('client', call_center='SCA1', oc_code='COX99')
        _testprep.add_test_client(self.tdb, 'COX99', 'SCA1')

        # get a SCA1 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/SouthCalifornia-1.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["SouthCalifornia"])
        t.locates = [locate.Locate('COX99')]
        t.locates[0].status = 'M'
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id
        # add another version or two for it
        t.revision = '01A'
        self.tdb.add_ticket_version(ticket_id, t, "one.txt")
        t.revision = '02A'
        self.tdb.add_ticket_version(ticket_id, t, "two.txt")
        # set some variables
        client_code = t.locates[0].client_code
        response_code = 'X'
        explanation = 'boo'

        rows = [
          {'ticket_id': ticket_id, 'locate_id': locate_id, 'client_code': 'COX99', 'status': 'M',},
        ]
        for row in rows:
            row['ticket_format'] = 'SCA1'
            row['ticket_type'] = 'NORMAL'

        import xmlhttpresponder_data as xhrd
        xr = create_mock_responder(rows, verbose=0)
        xr.log.logger.logfile = StringIO.StringIO()
        xr.responderdata = xhrd.getresponderdata(self.cfg, 'SCA1', None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1
        xr.respond_all()
        log = xr.log.logger.logfile.getvalue()
        xr.log.logger.logfile.close()
        # Verify that locdatetime injection logged
        self.assertEquals(log.count("Location date/time injected into xml based on current date/time"),2)

        self.assertEquals(len(xr.sent), 2)
        self.assertEquals(len(xr.xml_sent), 2)
        self.assertEquals(xr.sent[0], ['A0057805', 'COX99', '1', '', '01A'])
        self.assertEquals(xr.sent[1], ['A0057805', 'COX99', '1', '', '02A'])
        self.assertEquals(xr.xml_sent[0].count("<Version_Number>01</Version"), 1)
        self.assertEquals(xr.xml_sent[1].count("<Version_Number>02</Version"), 1)
        self.assertEquals(xr.xml_sent[0].count("<Version_Number>01A</Version"), 0)
        self.assertEquals(xr.xml_sent[1].count("<Version_Number>02A</Version"), 0)

    def test_FDX1_locate_closed_date(self):
        with Mockery(config, 'getConfiguration', lambda: self.cfg):
            call_center = 'FDX1'
            self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
             "%s XMLHTTP block required in config.xml"%call_center
            z = self.cfg.xmlhttpresponders[call_center]
            # use custom mappings so we're not dependent on what happens to be in
            # config.xml
            z['mappings']['M'] = ('1', '')
            z['mappings']['N'] = ('2', '')
            z['clients'] = ['AT1', 'BU1', 'BY1']
            z['all_versions'] = 1
            client_code = 'AT1'

            # get an FDX1 ticket
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "sample_tickets", "FDX1-2007-09-25-01263.txt"))
            raw = tl.tickets[0]
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["Dallas1"])
            status = 'M'
            t.locates = [locate.Locate(client_code)]
            t.locates[0].status = status

            # post it
            ticket_id = self.tdb.insertticket(t, whodunit='parser')
            locate_id = t.locates[0].locate_id
            # change the status of the  locate so we get an entry in the
            # locate_status table (done by locate trigger)
            locate_close_date = date.Date().isodate()
            sql = """
             update locate
             set status = 'M', closed=1, closed_by_id=%s, closed_how='test',
                 closed_date = '%s'
             where locate_id = %s
            """ % (self.test_emp_id, locate_close_date, locate_id,)
            self.tdb.runsql(sql)

            # add another version or two for it
            t.revision = '01A'
            self.tdb.add_ticket_version(ticket_id, t, "one.txt")
            t.revision = '02A'
            self.tdb.add_ticket_version(ticket_id, t, "two.txt")
            # set some variables
            client_code = t.locates[0].client_code
            response_code = 'X'
            explanation = 'boo'

            rows = [
              {'ticket_id': ticket_id, 'ticket_number': t.ticket_number,
               'locate_id': locate_id, 'client_code': client_code,
               'status': status,}
            ]
            for row in rows:
                row['ticket_format'] = call_center
                row['ticket_type'] = 'NORMAL'

            import xmlhttpresponder_data as xhrd
            xr = create_mock_responder(rows, verbose=0)
            xr.log.logger.logfile = StringIO.StringIO()
            xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
            self.assertEquals(xr.responderdata.all_versions, 1)
            xr.log.lock = 1
            xr.respond_all()
            log = xr.log.logger.logfile.getvalue()
            xr.log.logger.logfile.close()
            # Verify that locdatetime injection logged
            self.assertEquals(log.count("closed date injected into xml:"),2)
            # Parse out time
            _pat = re.compile("closed date injected into xml: ([0-9-]+ [0-9:]+)")
            matches = _pat.findall(log)
            self.assertEquals(len(matches), 2)
            self.assertEquals(matches[0],locate_close_date.replace("/", "-"))

            self.assertEquals(len(xr.sent), 2)
            self.assertEquals(len(xr.xml_sent), 2)
            self.assertEquals(xr.sent[0], [t.ticket_number, client_code, '1', '', '01A'])
            self.assertEquals(xr.sent[1], [t.ticket_number, client_code, '1', '', '02A'])
            self.assertEquals(xr.xml_sent[0].count("<Version_Number>01</Version"), 1)
            self.assertEquals(xr.xml_sent[1].count("<Version_Number>02</Version"), 1)
            self.assertEquals(xr.xml_sent[0].count("<Version_Number>01A</Version"), 0)
            self.assertEquals(xr.xml_sent[1].count("<Version_Number>02A</Version"), 0)

            # Verify that <Locate_DateTime> is the locate_close_date set above
            data = find_soap_node(xr.xml_sent[0], "Locate_DateTime").text
            self.assertEquals(str(data), locate_close_date.replace('-', '/'))

            # Verify the note
            data = find_soap_node(xr.xml_sent[0], "Note").text
            self.assertEquals(str(data), "Response by Utiliquest")

    def test_FDX1_jobsite_arrival_date(self):
        with Mockery(config, 'getConfiguration', lambda: self.cfg):
            call_center = 'FDX1'
            self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
             "%s XMLHTTP block required in config.xml"%call_center
            z = self.cfg.xmlhttpresponders[call_center]
            # use custom mappings so we're not dependent on what happens to be in
            # config.xml
            z['mappings']['M'] = ('1', '')
            z['mappings']['N'] = ('2', '')
            z['clients'] = ['AT1', 'BU1', 'BY1']
            z['all_versions'] = 1
            client_code = 'AT1'
            # get a FDX1 ticket
            import ticketparser, ticketloader, locate
            tl = ticketloader.TicketLoader("../testdata/sample_tickets/FDX1-2007-09-25-01263.txt")
            raw = tl.tickets[0]
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["Dallas1"])
            status = 'M'
            t.locates = [locate.Locate(client_code)]
            t.locates[0].status = status
            # post it
            ticket_id = self.tdb.insertticket(t, whodunit='parser')
            locate_id = t.locates[0].locate_id
            # change the status of the  locate so we get an entry in the
            # locate_status table (done by locate trigger)
            jobsite_arrival_date = date.Date().isodate()
            sql = """
             insert jobsite_arrival (ticket_id, arrival_date, emp_id, entry_method, added_by)
             values (%s,'%s', %s, '%s',%s)
            """ % (ticket_id, jobsite_arrival_date, self.test_emp_id,"Break",self.test_emp_id)
            self.tdb.runsql(sql)
            # add another version or two for it
            t.revision = '01A'
            self.tdb.add_ticket_version(ticket_id, t, "one.txt")
            t.revision = '02A'
            self.tdb.add_ticket_version(ticket_id, t, "two.txt")
            # set some variables
            client_code = t.locates[0].client_code
            response_code = 'X'
            explanation = 'boo'

            uid = _testprep.add_test_user(self.tdb, self.test_emp_id)
            d = '2007-01-01 12:13:14'

            self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, 'foo')
            self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, 'bar')
            self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, 'baz')

            rows = [
              {'ticket_id': ticket_id, 'ticket_number':t.ticket_number,'locate_id': locate_id, 'client_code': client_code, 'status': status,},
            ]
            for row in rows:
                row['ticket_format'] = call_center
                row['ticket_type'] = 'NORMAL'

            import xmlhttpresponder_data as xhrd
            xr = create_mock_responder(rows, verbose=0)
            xr.log.logger.logfile = StringIO.StringIO()
            xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
            self.assertEquals(xr.responderdata.all_versions, 1)
            xr.log.lock = 1
            xr.respond_all()
            log = xr.log.logger.logfile.getvalue()
            xr.log.logger.logfile.close()
            # Verify that locdatetime injection logged
            self.assertEquals(log.count("first_arrival_date injected into xml:"),2)
            # Parse out time
            _pat = re.compile("first_arrival_date injected into xml: ([0-9-]+ [0-9:]+)")
            matches = _pat.findall(log)
            self.assertEquals(len(matches), 2)
            self.assertEquals(matches[0],jobsite_arrival_date.replace("/", "-"))
            self.assertEquals(matches[1],jobsite_arrival_date.replace("/", "-"))

            self.assertEquals(len(xr.sent), 2)
            self.assertEquals(len(xr.xml_sent), 2)
            self.assertEquals(xr.sent[0], [t.ticket_number, client_code, '1', '', '01A'])
            self.assertEquals(xr.sent[1], [t.ticket_number, client_code, '1', '', '02A'])
            self.assertEquals(xr.xml_sent[0].count("<Version_Number>01</Version"), 1)
            self.assertEquals(xr.xml_sent[1].count("<Version_Number>02</Version"), 1)
            self.assertEquals(xr.xml_sent[0].count("<Version_Number>01A</Version"), 0)
            self.assertEquals(xr.xml_sent[1].count("<Version_Number>02A</Version"), 0)

            # Verify that <Locate_DateTime> is the jobsite_arrival_date set above
            data = find_soap_node(xr.xml_sent[0], "Locate_DateTime").text
            self.assertEquals(str(data), jobsite_arrival_date.replace("-", "/"))

            self.tdb.delete('jobsite_arrival', ticket_id=ticket_id)

            # Verify the note
            data = find_soap_node(xr.xml_sent[0], "Note").text
            self.assertEquals(str(data), "foo\nbar\nbaz")

    def test_6001_locate_closed_date(self):
        with Mockery(config, 'getConfiguration', lambda: self.cfg):
            call_center = '6001'
            self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
             "%s XMLHTTP block required in config.xml"%call_center
            z = self.cfg.xmlhttpresponders[call_center]
            # use custom mappings so we're not dependent on what happens to be in
            # config.xml
            z['mappings']['M'] = ('1', '')
            z['mappings']['N'] = ('2', '')
            z['clients'] = ['2AL', '2ES', '2FW']
            z['all_versions'] = 1
            client_code = '2AL'
            # get a 6001 ticket
            import ticketparser, ticketloader, locate
            tl = ticketloader.TicketLoader("../testdata/sample_tickets/6001-2007-09-25-01025.txt")
            raw = tl.tickets[0]
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["TX6001"])
            status = 'M'
            t.locates = [locate.Locate(client_code)]
            t.locates[0].status = status
            # post it
            ticket_id = self.tdb.insertticket(t, whodunit='parser')
            locate_id = t.locates[0].locate_id
            # change the status of the  locate so we get an entry in the
            # locate_status table (done by locate trigger)
            locate_close_date = date.Date().isodate()
            sql = """
             update locate
             set status = 'M', closed=1, closed_by_id=%s, closed_how='test',closed_date = '%s'
             where locate_id = %s
            """ % (self.test_emp_id,locate_close_date, locate_id,)
            self.tdb.runsql(sql)
            # add another version or two for it
            t.revision = '01A'
            self.tdb.add_ticket_version(ticket_id, t, "one.txt")
            t.revision = '02A'
            self.tdb.add_ticket_version(ticket_id, t, "two.txt")
            # set some variables
            client_code = t.locates[0].client_code
            response_code = 'X'
            explanation = 'boo'

            rows = [
              {'ticket_id': ticket_id, 'ticket_number':t.ticket_number,'locate_id': locate_id, 'client_code': client_code, 'status': status,},
            ]
            for row in rows:
                row['ticket_format'] = call_center
                row['ticket_type'] = 'NORMAL'

            import xmlhttpresponder_data as xhrd
            xr = create_mock_responder(rows, verbose=0)
            xr.log.logger.logfile = StringIO.StringIO()
            xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
            self.assertEquals(xr.responderdata.all_versions, 1)
            xr.log.lock = 1
            xr.respond_all()
            log = xr.log.logger.logfile.getvalue()
            xr.log.logger.logfile.close()

            # Verify that locdatetime injection logged
            self.assertEquals(log.count("closed date injected into xml:"),2)
            # Parse out time
            _pat = re.compile("closed date injected into xml: ([0-9-]+ [0-9:]+)")
            matches = _pat.findall(log)
            self.assertEquals(len(matches), 2)
            self.assertEquals(matches[0],locate_close_date.replace("/", "-"))
            self.assertEquals(matches[1],locate_close_date.replace("/", "-"))

            self.assertEquals(len(xr.sent), 2)
            self.assertEquals(len(xr.xml_sent), 2)
            self.assertEquals(xr.sent[0], [t.ticket_number, client_code, '1', '', '01A'])
            self.assertEquals(xr.sent[1], [t.ticket_number, client_code, '1', '', '02A'])
            self.assertEquals(xr.xml_sent[0].count("<Version_Number>01</Version"), 1)
            self.assertEquals(xr.xml_sent[1].count("<Version_Number>02</Version"), 1)
            self.assertEquals(xr.xml_sent[0].count("<Version_Number>01A</Version"), 0)
            self.assertEquals(xr.xml_sent[1].count("<Version_Number>02A</Version"), 0)

            # Verify that <Locate_DateTime> is the locate_close_date set above
            data = find_soap_node(xr.xml_sent[0], "Locate_DateTime").text
            self.assertEquals(str(data),locate_close_date.replace("-", "/"))

    def test_6001_jobsite_arrival_date(self):
        with Mockery(config, 'getConfiguration', lambda: self.cfg):
            call_center = '6001'
            self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
             "%s XMLHTTP block required in config.xml"%call_center
            z = self.cfg.xmlhttpresponders[call_center]
            # use custom mappings so we're not dependent on what happens to be in
            # config.xml
            z['mappings']['M'] = ('1', '')
            z['mappings']['N'] = ('2', '')
            z['clients'] = ['2AL', '2ES', '2FW']
            z['all_versions'] = 1
            client_code = '2AL'
            # get a 6001 ticket
            import ticketparser, ticketloader, locate
            tl = ticketloader.TicketLoader("../testdata/sample_tickets/6001-2007-09-25-01025.txt")
            raw = tl.tickets[0]
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["TX6001"])
            status = 'M'
            t.locates = [locate.Locate(client_code)]
            t.locates[0].status = status
            # post it
            ticket_id = self.tdb.insertticket(t, whodunit='parser')
            locate_id = t.locates[0].locate_id
            # change the status of the  locate so we get an entry in the
            # locate_status table (done by locate trigger)
            jobsite_arrival_date = date.Date().isodate()
            sql = """
             insert jobsite_arrival (ticket_id, arrival_date, emp_id, entry_method, added_by)
             values (%s,'%s', %s, '%s',%s)
            """ % (ticket_id, jobsite_arrival_date, self.test_emp_id,"Break",self.test_emp_id)
            self.tdb.runsql(sql)
            # add another version or two for it
            t.revision = '01A'
            self.tdb.add_ticket_version(ticket_id, t, "one.txt")
            t.revision = '02A'
            self.tdb.add_ticket_version(ticket_id, t, "two.txt")
            # set some variables
            client_code = t.locates[0].client_code
            response_code = 'X'
            explanation = 'boo'

            rows = [
              {'ticket_id': ticket_id, 'ticket_number':t.ticket_number,'locate_id': locate_id, 'client_code': client_code, 'status': status,},
            ]
            for row in rows:
                row['ticket_format'] = call_center
                row['ticket_type'] = 'NORMAL'

            import xmlhttpresponder_data as xhrd
            xr = create_mock_responder(rows, verbose=0)
            xr.log.logger.logfile = StringIO.StringIO()
            xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
            self.assertEquals(xr.responderdata.all_versions, 1)
            xr.log.lock = 1
            xr.respond_all()
            log = xr.log.logger.logfile.getvalue()
            xr.log.logger.logfile.close()

            # Verify that locdatetime injection logged
            self.assertEquals(log.count("first_arrival_date injected into xml:"),2)
            # Parse out time
            _pat = re.compile("first_arrival_date injected into xml: ([0-9-]+ [0-9:]+)")
            matches = _pat.findall(log)
            self.assertEquals(len(matches), 2)
            self.assertEquals(matches[0],jobsite_arrival_date.replace("/", "-"))
            self.assertEquals(matches[1],jobsite_arrival_date.replace("/", "-"))

            self.assertEquals(len(xr.sent), 2)
            self.assertEquals(len(xr.xml_sent), 2)
            self.assertEquals(xr.sent[0], [t.ticket_number, client_code, '1', '', '01A'])
            self.assertEquals(xr.sent[1], [t.ticket_number, client_code, '1', '', '02A'])
            self.assertEquals(xr.xml_sent[0].count("<Version_Number>01</Version"), 1)
            self.assertEquals(xr.xml_sent[1].count("<Version_Number>02</Version"), 1)
            self.assertEquals(xr.xml_sent[0].count("<Version_Number>01A</Version"), 0)
            self.assertEquals(xr.xml_sent[1].count("<Version_Number>02A</Version"), 0)

            # Verify that <Locate_DateTime> is the jobsite_arrival_date set above
            data = find_soap_node(xr.xml_sent[0], "Locate_DateTime").text
            self.assertEquals(str(data),jobsite_arrival_date.replace("-", "/"))
            self.tdb.delete('jobsite_arrival', ticket_id=ticket_id)

    def test_9001_no_notes(self):
        call_center = '9001'
        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml"%call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        client_code = 'SBF20'
        # get a 9001 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/Jacksonville-2.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["FL9001"])
        status = 'M'
        t.locates = [locate.Locate(client_code)]
        t.locates[0].status = status
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id
        client_code = t.locates[0].client_code
        response_code = 'X'
        explanation = 'boo'

        rows = []

        import xmlhttpresponder_data as xhrd
        xr = create_mock_responder(rows, verbose=0)
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1
        xr.respond_all()

        self.assertEquals(len(xr.sent), 1)
        self.assertEquals(len(xr.xml_sent), 1)
        # Verify the note
        data = find_soap_node(xr.xml_sent[0], "Notes", namespace="").text
        self.assertEquals(str(data), "Response by Utiliquest")

    def test_9001_with_notes_1(self):
        call_center = '9001'
        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml"%call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        client_code = 'SBF20'
        # get a 9001 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/Jacksonville-2.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["FL9001"])
        status = 'M'
        t.locates = [locate.Locate(client_code)]
        t.locates[0].status = status
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id
        client_code = t.locates[0].client_code
        response_code = 'X'
        explanation = 'boo'

        uid = _testprep.add_test_user(self.tdb, self.test_emp_id)
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, 'foo')
        self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, 'bar')
        self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, 'baz')

        rows = []

        import xmlhttpresponder_data as xhrd
        xr = create_mock_responder(rows, verbose=0)
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1
        xr.respond_all()

        self.assertEquals(len(xr.sent), 1)
        self.assertEquals(len(xr.xml_sent), 1)
        # Verify the note
        data = find_soap_node(xr.xml_sent[0], "Notes", namespace="").text
        self.assertEquals(str(data),"foo\nbar\nbaz")

    def test_9001_with_notes_2(self):
        call_center = '9001'
        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml"%call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        client_code = 'SBF20'
        # get a 9001 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/Jacksonville-2.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["FL9001"])
        status = 'M'
        t.locates = [locate.Locate(client_code)]
        t.locates[0].status = status
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id
        client_code = t.locates[0].client_code
        response_code = 'X'
        explanation = 'boo'

        uid = _testprep.add_test_user(self.tdb, self.test_emp_id)
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, 'foo>')
        self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, 'bar<')
        self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, 'baz&')
        self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, 'double quote "')
        # manually put it in the notes since insert_ticket_note will strip the single quote
        sql = """
         exec insert_ticket_note %s, %s, '%s', '%s'
        """ % (ticket_id, self.test_emp_id, d, "single quote junk''s")
        self.tdb.runsql(sql)

        rows = []

        import xmlhttpresponder_data as xhrd
        xr = create_mock_responder(rows, verbose=0)
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1
        xr.respond_all()

        self.assertEquals(len(xr.sent), 1)
        self.assertEquals(len(xr.xml_sent), 1)
        # Verify the note
        data = find_soap_node(xr.xml_sent[0], "Notes", namespace="").text
        self.assertEquals(str(data),
          'foo>\nbar<\nbaz&\ndouble quote "\nsingle quote junk\'s')

    def test_9001_with_notes_3(self):
        call_center = '9001'
        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml"%call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        client_code = 'SBF20'
        # get a 9001 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/Jacksonville-2.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["FL9001"])
        status = 'M'
        t.locates = [locate.Locate(client_code)]
        t.locates[0].status = status
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id
        client_code = t.locates[0].client_code
        response_code = 'X'
        explanation = 'boo'

        uid = _testprep.add_test_user(self.tdb, self.test_emp_id)
        d = '2007-01-01 12:13:14'
        # 251 characters
        long_note = "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "1"
        self.tdb.insert_ticket_note(ticket_id, self.test_emp_id, d, long_note)

        rows = []

        import xmlhttpresponder_data as xhrd
        xr = create_mock_responder(rows, verbose=0)
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1
        xr.respond_all()

        self.assertEquals(len(xr.sent), 1)
        self.assertEquals(len(xr.xml_sent), 1)
        # Verify the note
        data = find_soap_node(xr.xml_sent[0], "Notes", namespace="").text
        self.assertEquals(str(data), long_note[0:239]+"(More ...)\n")

    def test_9001_with_notes_4(self):
        try:
            call_center = '9001'
            SBF20_activated = False
            results = self.tdb.getrecords("client", call_center = call_center, oc_code = 'SBF20', active = 1)
            if not results:
                SBF20_activated = True
                _testprep.set_client_active(self.tdb, call_center, 'SBF20')
            SBF02_activated = False
            results = self.tdb.getrecords("client", call_center = call_center, oc_code = 'SBF02', active = 1)
            if not results:
                SBF02_activated = True
                _testprep.set_client_active(self.tdb, call_center, 'SBF02')

            self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
             "%s XMLHTTP block required in config.xml"%call_center
            z = self.cfg.xmlhttpresponders[call_center]
            z['all_versions'] = 1
            # get a 9001 ticket
            import ticketparser, ticketloader, locate
            tl = ticketloader.TicketLoader("../testdata/Jacksonville-2.txt")
            raw = tl.tickets[0]
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["FL9001"])
            status = '-P'
            # Add a known active client
            client_code = 'SBF20'
            t.locates.append(locate.Locate(client_code))
            t.locates[-1].status = status
            # Add another known active client
            client_code = 'SBF02'
            t.locates.append(locate.Locate(client_code))
            t.locates[-1].status = status
            # post it
            ticket_id = self.tdb.insertticket(t, whodunit='parser')

            # let the router run so the client_id will be set in the locate record
            # otherwise the ticket_notes view will return nada
            tr = ticketrouter.TicketRouter(1, verbose=0)
            tr.run(runonce=1)

            # update the locate with a new status so the responder_queue will be populated
            kwargs = {
                'status': 'M',
            }
            self.tdb.update('locate', 'locate_id', t.locates[-2].locate_id, **kwargs)
            self.tdb.update('locate', 'locate_id', t.locates[-1].locate_id, **kwargs)


            uid = _testprep.add_test_user(self.tdb, self.test_emp_id)
            # Insert notes for each locate
            results = self.tdb.getrecords("locate")
            notes = []
            for row in results:
                if row['status'] != locate_status.not_a_customer:
                    # insert a locate note
                    sql = """
                     insert notes
                     (foreign_type, foreign_id, entry_date,modified_date,uid,active,note)
                     values (5, '%s', getdate(),getdate(),%s,1,'A locate note for client %s')
                    """ % (row['locate_id'],uid,row['client_id'])
                    self.tdb.runsql(sql)

                    notes.append(self.tdb.get_all_notes(ticket_id, row['locate_id']))

            rows = []

            import xmlhttpresponder_data as xhrd
            xr = create_mock_responder(rows, verbose=0)
            xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
            self.assertEquals(xr.responderdata.all_versions, 1)
            xr.log.lock = 1
            xr.respond_all()

            self.assertEquals(len(xr.sent), 2)
            self.assertEquals(len(xr.xml_sent), 2)
            # Verify the first note
            data = find_soap_node(xr.xml_sent[0], "Notes", namespace="").text
            self.assertEquals(str(data),notes[0][0])
            # Verify the second note
            data = find_soap_node(xr.xml_sent[1], "Notes", namespace="").text
            self.assertEquals(str(data),notes[1][0])
        finally:
            if SBF20_activated:
                _testprep.set_client_inactive(self.tdb, call_center, 'SBF20')
            if SBF02_activated:
                _testprep.set_client_inactive(self.tdb, call_center, 'SBF02')

    def test_9001_with_notes_5(self):
        """
        Insert special characters in other fields
        """
        call_center = '9001'
        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml"%call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        # get a 9001 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/Jacksonville-2.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["FL9001"])
        status = '-P'
        # Add a known active client (perhaps check that it is indeed active?)
        client_code = 'SBF20'
        t.locates.append(locate.Locate(client_code))
        t.locates[-1].status = status
        # Add another known active client
        client_code = 'SBF02'
        t.locates.append(locate.Locate(client_code))
        t.locates[-1].status = status
        # Insert some special characters in the ticket number
        t.ticket_number = t.ticket_number+'&><'
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # let the router run so the client_id will be set in the locate record
        # otherwise the ticket_notes view will return nada
        tr = ticketrouter.TicketRouter(1, verbose=0)
        tr.run(runonce=1)

        # update the locate with a new status so the responder_queue will be populated
        kwargs = {
            'status': 'M',
        }
        self.tdb.update('locate', 'locate_id', t.locates[-2].locate_id, **kwargs)
        self.tdb.update('locate', 'locate_id', t.locates[-1].locate_id, **kwargs)


        uid = _testprep.add_test_user(self.tdb, self.test_emp_id)
        # Insert notes for each locate
        results = self.tdb.getrecords("locate")
        for row in results:
            # insert a locate note
            sql = """
             insert notes
             (foreign_type, foreign_id, entry_date,modified_date,uid,active,note)
             values (5, '%s', getdate(),getdate(),%s,1,'A locate note for client %s')
            """ % (row['locate_id'],uid,row['client_id'])
            self.tdb.runsql(sql)

        rows = []

        import xmlhttpresponder_data as xhrd
        xr = create_mock_responder(rows, verbose=0)
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1
        xr.respond_all()

        self.assertEquals(len(xr.sent), 2)
        self.assertEquals(len(xr.xml_sent), 2)
        # Verify that the special characters in the ticket number have been escaped
        data = find_soap_node(xr.xml_sent[0], "TicketNumber", namespace="").text
        self.assertEquals(str(data),'15823396&><')

    def test_1391_1(self):
        call_center = '1391'
        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml" % call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        client_code = 'B11'
        # get a 1391 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/1391-1.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["TN1391"])
        status = 'M'
        t.locates = [locate.Locate(client_code)]
        t.locates[0].status = status
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id
        client_code = t.locates[0].client_code

        locate_close_date = date.Date()
        locate_close_date.dec_time(hours=2)
        sql = """
         update locate
         set status = 'M', closed=1, closed_by_id=%s, closed_how='test',
             closed_date = '%s'
         where locate_id = %s
        """ % (self.test_emp_id, locate_close_date.isodate(), locate_id,)
        self.tdb.runsql(sql)

        rows = []

        import xmlhttpresponder_data as xhrd
        xr = create_mock_responder(rows, verbose=0)
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1

        # Verify the FacilityType
        # Get the utility type from the client
        sql = """
         select utility_type
         from client
         where oc_code = '%s'
        """ % (client_code,)
        results = self.tdb.runsql_result(sql)
        if results:
            if xr.responderdata.fac_types.has_key(results[0]["utility_type"]):
                fac_type = xr.responderdata.fac_types[results[0]["utility_type"]]
            else:
                fac_type = "O"
        else:
            fac_type = "O"
        self.assertEquals(fac_type,"P")

        xr.respond_all()

        self.assertEquals(len(xr.sent), 1)
        self.assertEquals(len(xr.xml_sent), 1)

        # Verify the TicketNum
        data = find_soap_node(xr.xml_sent[0], "TicketNum",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data), "042861573")

        # Verify the DispatchCode
        data = find_soap_node(xr.xml_sent[0], "DispatchCode",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),client_code)

        # Verify the ResponseCode
        data = find_soap_node(xr.xml_sent[0], "ResponseCode",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),xr.responderdata.mappings[status][0])

        data = find_soap_node(xr.xml_sent[0], "FacilityType",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),fac_type)

        # Verify the LocateDate/LocateTime
        data_date = find_soap_node(xr.xml_sent[0], "LocateDate",
                    namespace="{http://tnresponse.korterraweb.com/}").text
        data_time = find_soap_node(xr.xml_sent[0], "LocateTime",
                    namespace="{http://tnresponse.korterraweb.com/}").text
        year1, month1, day1, hour1, min1, sec1, wday1, yday1, idst1 = \
            time.strptime(string.join([str(data_date), str(data_time)]),
            "%Y-%m-%d %H:%M:%S")
        d1 = datetime.datetime(year1, month1, day1, hour1, min1, sec1)
        d2 = datetime.datetime(locate_close_date.year,
                               locate_close_date.month,
                               locate_close_date.day,
                               locate_close_date.hours,
                               locate_close_date.minutes,
                               locate_close_date.seconds)
        # Account for the call center in a later time zone
        d2 = d2 - datetime.timedelta(hours=1)
        td = d2-d1
        self.assert_(abs(td.seconds) < 60.0)

    def test_1391_2(self):
        call_center = '1391'
        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml"%call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        client_code = 'Test001'
        # get a 1391 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/1391-1.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["TN1391"])

        # Add a client with a bogus utility type
        self.tdb.delete("client", oc_code="Test001")
        new_client_id = _testprep.add_test_client(self.tdb, client_code, '1391')
        cl = client.Client.load(self.tdb, new_client_id)
        cl.utility_type = 'bogus'
        cl.update(self.tdb)
        status = 'M'
        t.locates = [locate.Locate(client_code)]
        t.locates[0].status = status
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id
        client_code = t.locates[0].client_code

        locate_close_date = date.Date()
        locate_close_date.dec_time(hours=2)
        sql = """
         update locate
         set status = 'M', closed=1, closed_by_id=%s, closed_how='test',closed_date = '%s'
         where locate_id = %s
        """ % (self.test_emp_id, locate_close_date.isodate(), locate_id,)
        self.tdb.runsql(sql)

        rows = []

        import xmlhttpresponder_data as xhrd
        xr = create_mock_responder(rows, verbose=0)
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1

        # Verify the FacilityType
        # Get the utility type from the client
        sql = """
         select utility_type
         from client
         where oc_code = '%s'
        """ % (client_code,)
        results = self.tdb.runsql_result(sql)
        if results:
            if xr.responderdata.fac_types.has_key(results[0]["utility_type"]):
                fac_type = xr.responderdata.fac_types[results[0]["utility_type"]]
            else:
                fac_type = "O"
        else:
            fac_type = "O"
        self.assertEquals(fac_type,"O")

        xr.respond_all()

        date_time = date.Date().isodate()

        self.assertEquals(len(xr.sent), 1)
        self.assertEquals(len(xr.xml_sent), 1)

        # Verify the TicketNum
        data = find_soap_node(xr.xml_sent[0], "TicketNum",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),"042861573")

        # Verify the DispatchCode
        data = find_soap_node(xr.xml_sent[0], "DispatchCode",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),client_code)

        # Verify the ResponseCode
        data = find_soap_node(xr.xml_sent[0], "ResponseCode",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),xr.responderdata.mappings[status][0])

        data = find_soap_node(xr.xml_sent[0], "FacilityType",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),fac_type)

        # Verify the LocateDate / LocateTime
        dat1 = find_soap_node(xr.xml_sent[0], "LocateDate",
               namespace="{http://tnresponse.korterraweb.com/}").text
        dat2 = find_soap_node(xr.xml_sent[0], "LocateTime",
               namespace="{http://tnresponse.korterraweb.com/}").text
        year1,month1,day1,hour1,min1,sec1,wday1,yday1,idst1 = \
            time.strptime(string.join([str(dat1),str(dat2)]),
            "%Y-%m-%d %H:%M:%S")
        d1 = datetime.datetime(year1,month1,day1,hour1,min1,sec1)
        d2 = datetime.datetime(locate_close_date.year,
                               locate_close_date.month,
                               locate_close_date.day,
                               locate_close_date.hours,
                               locate_close_date.minutes,
                               locate_close_date.seconds)
        # Account for the call center in a later time zone
        d2 = d2 - datetime.timedelta(hours=1)
        td = d2-d1
        self.assert_(abs(td.seconds) < 60.0)

        self.tdb.delete("client", oc_code="Test001")

    def test_9401_1(self):
        call_center = '9401'
        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml"%call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        client_code = 'UTAC'
        # get a 9401 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/TN9401-1.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["TN9401"])
        status = 'M'
        t.locates = [locate.Locate(client_code)]
        t.locates[0].status = status
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id
        client_code = t.locates[0].client_code

        locate_close_date = date.Date()
        locate_close_date.dec_time(hours=2)
        sql = """
         update locate
         set status = 'M', closed=1, closed_by_id=%s, closed_how='test',closed_date = '%s'
         where locate_id = %s
        """ % (self.test_emp_id, locate_close_date.isodate(), locate_id,)
        self.tdb.runsql(sql)

        rows = []

        import xmlhttpresponder_data as xhrd
        xr = create_mock_responder(rows, verbose=0)
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)

        # Verify the FacilityType
        # Get the utility type from the client
        sql = """
         select utility_type
         from client
         where oc_code = '%s'
        """ % (client_code,)
        results = self.tdb.runsql_result(sql)
        if results:
            if xr.responderdata.fac_types.has_key(results[0]["utility_type"]):
                fac_type = xr.responderdata.fac_types[results[0]["utility_type"]]
            else:
                fac_type = "O"
        else:
            fac_type = "O"
        self.assertEquals(fac_type,"P")

        xr.log.lock = 1
        xr.xmlhttp.responseXML.Text = "success"
        xr.respond_all()

        date_time = date.Date().isodate()

        self.assertEquals(len(xr.sent), 1)
        self.assertEquals(len(xr.xml_sent), 1)

        # Verify the TicketNum
        data = find_soap_node(xr.xml_sent[0], "TicketNum",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),"050310204")

        # Verify the DispatchCode
        data = find_soap_node(xr.xml_sent[0], "DispatchCode",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),client_code)

        # Verify the ResponseCode
        data = find_soap_node(xr.xml_sent[0], "ResponseCode",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),xr.responderdata.mappings[status][0])

        #check against mapping
        data = find_soap_node(xr.xml_sent[0], "FacilityType",
               namespace="{http://tnresponse.korterraweb.com/}").text
        self.assertEquals(str(data),fac_type)

        # Verify the LocateDate / LocateTime
        dat1 = find_soap_node(xr.xml_sent[0], "LocateDate",
               namespace="{http://tnresponse.korterraweb.com/}").text
        dat2 = find_soap_node(xr.xml_sent[0], "LocateTime",
               namespace="{http://tnresponse.korterraweb.com/}").text
        year1,month1,day1,hour1,min1,sec1,wday1,yday1,idst1 = \
            time.strptime(string.join([str(dat1), str(dat2)]),
            "%Y-%m-%d %H:%M:%S")
        d1 = datetime.datetime(year1,month1,day1,hour1,min1,sec1)
        d2 = datetime.datetime(locate_close_date.year,
                               locate_close_date.month,
                               locate_close_date.day,
                               locate_close_date.hours,
                               locate_close_date.minutes,
                               locate_close_date.seconds)
        # Account for the call center in a later time zone
        d2 = d2 - datetime.timedelta(hours=1)
        td = d2-d1
        self.assert_(abs(td.seconds) < 60.0)

    def test_9003(self):
        """ test 9003 responder """
        cc = call_centers.get_call_centers(self.tdb)
        update_call_centers_orig = copy.deepcopy(cc.ucc)
        namespace = "{http://tempuri.org/ContractLocatorResponseDataSet.xsd}"
        try:
            if not cc.update_call_center('9003'):
                cc.ucc['9003'] = '9001' # add it
            call_center = '9003'
            self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
             "%s XMLHTTP block required in config.xml" % call_center
            z = self.cfg.xmlhttpresponders[call_center]
            z['all_versions'] = 1
            z['clients'] = ['ABC01']
            client_code = 'ABC01'
            # get a 9401 ticket
            tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
                 "sample_tickets","9003-2007-12-11-00054.txt"))
            raw = tl.tickets[0]
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["FL9003"])
            status = 'M'
            t.locates = [locate.Locate(client_code)]
            t.locates[0].status = status
            # post it
            ticket_id = self.tdb.insertticket(t, whodunit='parser')
            locate_id = t.locates[0].locate_id
            client_code = t.locates[0].client_code

            rows = []

            import xmlhttpresponder_data as xhrd
            xr = create_mock_responder(rows, verbose=0)
            xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
            self.assertEquals(xr.responderdata.all_versions, 1)
            xr.log.lock = 1
            xr.only_call_center = '9003'
            xr.respond_all()

            date_time = date.Date().isodate()

            self.assertEquals(len(xr.sent), 1)
            self.assertEquals(len(xr.xml_sent), 1)

            # Verify the TicketNum
            data = find_soap_node(xr.xml_sent[0], "Ticket_Number",
                   namespace=namespace).text
            self.assertEquals(str(data),"345709157")

            # Verify the ResponseCode
            data = find_soap_node(xr.xml_sent[0], "Response_Code",
                   namespace=namespace).text
            self.assertEquals(str(data),xr.responderdata.mappings[status][0])

            # Verify the LocateDate
            data = find_soap_node(xr.xml_sent[0], "Locate_DateTime",
                   namespace=namespace).text
            year1, month1, day1, hour1, min1, sec1, wday1, yday1, idst1 = \
             time.strptime(str(data), "%Y/%m/%d %H:%M:%S")
            d1 = datetime.datetime(year1,month1,day1,hour1,min1,sec1)
            year2,month2,day2,hour2,min2,sec2,wday2,yday2,idst2 = \
             time.strptime(date_time,"%Y-%m-%d %H:%M:%S")
            d2 = datetime.datetime(year2,month2,day2,hour2,min2,sec2)
            td = d2-d1
            self.assert_(abs(td.seconds) < 60.0)
        finally:
            cc.ucc = update_call_centers_orig

    #
    # tests for 1421, which includes a facility code

    def test_1421_elec(self):
        self._test_1421(term_id="SCANA01E", facility="electric primary")

    def test_1421_phone(self):
        self._test_1421(term_id="SCANA01P", facility="fiber optics")

    def test_1421_gas(self):
        self._test_1421(term_id="SCANA01G", facility="gas distribution")

    def _test_1421(self, term_id, facility):

        # add test clients
        rec = self.tdb.getrecords('client', oc_code='SCANA01G')[0]
        self.tdb.updaterecord('client', 'client_id', int(rec['client_id']),
         utility_type='ugas')
        rec = self.tdb.getrecords('client', oc_code='SCANA01E')[0]
        self.tdb.updaterecord('client', 'client_id', int(rec['client_id']),
         utility_type='elec')
        rec = self.tdb.getrecords('client', oc_code='SCANA01P')[0]
        self.tdb.updaterecord('client', 'client_id', int(rec['client_id']),
         utility_type='phon') # !

        with Mockery(config, 'getConfiguration', lambda: self.cfg):
            call_center = '1421'
            z = {'ack_dir': '',
                 'ack_proc_dir': '',
                 'mappings': {'M':('1', ''),'N':('2', '')},
                 'clients': ['SCANA01E', 'SCANA01G', 'SCANA01P'],
                 'all_versions': 0,
                 'id': 'spam',
                 'initials': 'UQ',
                 'name': '1421',
                 'post_url': 'https://www.irth.com/irthnet/webservices/irthnetservice.asmx',
                 'resp_url': 'http://tempuri.org/AddContractLocatorResponses',
                 'send_3hour': 1,
                 'send_emergencies': 1,
                 'skip': [],
                 'status_code_skip': [],
                 'translations': {}
            }

            self.cfg.xmlhttpresponders[call_center] = z
            self.cfg.responderconfigdata.add(z['name'], 'xmlhttp', z)
            self.assert_(self.cfg.xmlhttpresponders.get(call_center),
             "%s XMLHTTP block required in config.xml" % call_center)

            tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
                 "sample_tickets", "1422", "1422-2009-04-14-00001.txt"))
            raw = tl.tickets[0]
            raw = raw.replace('SCEGT01', term_id)
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["SC1422"])
            status = 'M'
            t.locates[0].status = status
            t.ticket_format = '1421' # Do as main would do

            # post it
            ticket_id = self.tdb.insertticket(t, whodunit='parser')
            locate_id = t.locates[0].locate_id

            # change the status of the  locate so we get an entry in the
            # locate_status table (done by locate trigger)
            locate_close_date = date.Date().isodate()
            sql = """
             update locate
             set status = 'M', closed=1, closed_by_id=%s, closed_how='test',
                 closed_date = '%s'
             where locate_id = %s
            """ % (self.test_emp_id, locate_close_date, locate_id)
            self.tdb.runsql(sql)

            # set some variables
            client_code = t.locates[0].client_code
            self.assertEquals(client_code, term_id)
            response_code = 'X'
            explanation = 'boo'

            rows = [{'ticket_id': ticket_id,
                     'ticket_number':t.ticket_number,
                     'locate_id': locate_id,
                     'client_code': client_code,
                     'status': status}]
            for row in rows:
                row['ticket_format'] = call_center
                row['ticket_type'] = 'NORMAL'

            import xmlhttpresponder_data as xhrd
            xr = create_mock_responder(rows, verbose=0)
            xr.log.logger.logfile = StringIO.StringIO()
            xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
            self.assertEquals(xr.responderdata.all_versions, 0)
            xr.log.lock = 1
            xr.respond_all()
            log = xr.log.logger.logfile.getvalue()
            xr.log.logger.logfile.close()
            self.assertTrue('Responding for: 1421' in log)
            self.assertTrue(('Sending Response: 0903021230, ' + term_id + ', 1, ,') in log)
            self.assertTrue('closed date injected into xml:' in log)
            self.assertTrue('Response: response posted' in log)
            self.assertTrue('Removing locate from queue: %s' % locate_id in log)
            self.assertTrue('1 responses sent' in log)

            self.assertEquals(len(xr.sent), 1)
            self.assertEquals(len(xr.xml_sent), 1)
            self.assertEquals(xr.sent[0], [t.ticket_number, client_code, '1', '', ''])

            # inspect XML
            namespace = "{http://tempuri.org/ContractLocatorResponseDataSet.xsd}"
            data = find_soap_node(xr.xml_sent[0], "Facility_Type",
                   namespace=namespace).text
            self.assertEquals(str(data), facility)

            data = find_soap_node(xr.xml_sent[0], "CDC", namespace=namespace).text
            self.assertEquals(str(data), term_id)
            self.assertTrue('<Locator_Name>John Tester</Locator_Name' in
             xr.xml_sent[0])

    def test_FCL3_1(self):
        _testprep.add_test_client(self.tdb, 'TST03', 'FCL1', active=1,
         update_call_center='FCL3')

        with Mockery(config, 'getConfiguration', lambda: self.cfg):
            call_center = 'FCL1'
            z = {'ack_dir':'',
                 'ack_proc_dir':'',
                 'mappings':{'M':('1', ''),'N':('2', '')},
                 'clients':['TST03',],
                 'all_versions':0,
                 'id':'spam',
                 'initials':'UQ',
                 'name':'FCL1',
                 'post_url':'https://www.irth.com/irthnet/webservices/irthnetservice.asmx',
                 'resp_url':'http://tempuri.org/AddContractLocatorResponses',
                 'send_3hour':1,
                 'send_emergencies':1,
                 'skip':[],
                 'status_code_skip':[],
                 'translations':{}}

            self.cfg.xmlhttpresponders[call_center] = z
            self.cfg.responderconfigdata.add(z['name'], 'xmlhttp', z)
            self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
             "%s XMLHTTP block required in config.xml"%call_center
            import ticketparser, ticketloader, locate
            tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
                 "sample_tickets", "FCL3", "FCL3-2009-05-19-00012.txt"))
            raw = tl.tickets[0]
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["NorthCarolina2"])
            status = 'M'
            t.locates[0].status = status
            t.ticket_format = 'FCL1' # Do as main would do
            # post it
            ticket_id = self.tdb.insertticket(t, whodunit='parser')
            locate_id = t.locates[4].locate_id
            # change the status of the  locate so we get an entry in the
            # locate_status table (done by locate trigger)
            locate_close_date = date.Date().isodate()
            sql = """
             update locate
             set status = 'M', closed=1, closed_by_id=%s, closed_how='test',
                 closed_date = '%s'
             where locate_id = %s
            """ % (self.test_emp_id, locate_close_date, locate_id,)
            self.tdb.runsql(sql)
            # set some variables
            client_code = t.locates[4].client_code
            self.assertEquals(client_code, 'TST03')
            response_code = 'X'
            explanation = 'boo'

            rows = [
                    {'ticket_id': ticket_id,
                     'ticket_number':t.ticket_number,
                     'locate_id': locate_id,
                     'client_code': client_code,
                     'status': status,},]
            for row in rows:
                row['ticket_format'] = call_center
                row['ticket_type'] = 'NORMAL'

            import xmlhttpresponder_data as xhrd
            xr = create_mock_responder(rows, verbose=0)
            xr.log.logger.logfile = StringIO.StringIO()
            xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
            self.assertEquals(xr.responderdata.all_versions, 0)
            xr.log.lock = 1
            xr.respond_all()
            log = xr.log.logger.logfile.getvalue()
            xr.log.logger.logfile.close()
            self.assertTrue('Responding for: FCL1' in log)
            self.assertTrue('Sending Response: C091260252, TST03, 1, ,' in log)
            self.assertTrue('closed date injected into xml:' in log)
            self.assertTrue('Response: response posted' in log)
            self.assertTrue('Removing locate from queue: %s' % (locate_id,) in log)
            self.assertTrue('1 responses sent' in log)

            self.assertEquals(len(xr.sent), 1)
            self.assertEquals(len(xr.xml_sent), 1)
            self.assertEquals(xr.sent[0], [t.ticket_number, client_code, '1', '', '1'])

            # Verify that <Locate_DateTime> is the locate_close_date set above
            namespace = "{http://tempuri.org/ContractLocatorResponseDataSet.xsd}"
            data = find_soap_node(xr.xml_sent[0], "Version_Number",
                   namespace=namespace).text
            self.assertEquals(str(data), '1')

    def test_1421_pups(self):

        # add test clients
        _testprep.add_test_client(self.tdb, 'PEHZ51', '1421')

        with Mockery(config, 'getConfiguration', lambda: self.cfg):
            call_center = '1421'
            z = {'ack_dir': '',
                 'ack_proc_dir': '',
                 'mappings': {'M':('20', ''), 'N':('30', '')},
                 'clients': ['PEHZ51'],
                 'all_versions': 0,
                 'id': 'spam',
                 'initials': 'UQ',
                 'kind': 'pups',
                 'name': '1421',
                 'post_url': 'https://www.irth.com/irthnet/webservices/irthnetservice.asmx',
                 'resp_url': 'http://tempuri.org/AddContractLocatorResponses',
                 'send_3hour': 1,
                 'send_emergencies': 1,
                 'skip': [],
                 'status_code_skip': [],
                 'translations': {}
            }

            self.cfg.xmlhttpresponders[call_center] = z
            self.cfg.responderconfigdata.add(z['name'], 'xmlhttp', z)
            self.assert_(self.cfg.xmlhttpresponders.get(call_center),
             "%s XMLHTTP block required in config.xml" % call_center)

            tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
                 "tickets", "1421-2014-02-pups", 
                 "1421-2014-01-14-08-23-14-490-7N2P3.txt"))
            raw = tl.tickets[0]
            #raw = raw.replace('SCEGT01', term_id)
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["SC1421PUPS"])
            status = 'M'
            t.locates[2].status = status # PEHZ51
            t.ticket_format = '1421' # Do as main would do

            # post it
            ticket_id = self.tdb.insertticket(t, whodunit='parser')
            locate_id = t.locates[2].locate_id

            # change the status of the  locate so we get an entry in the
            # locate_status table (done by locate trigger)
            locate_close_date = date.Date().isodate()
            sql = """
             update locate
             set status = 'M', closed=1, closed_by_id=%s, closed_how='test',
                 closed_date = '%s'
             where locate_id = %s
            """ % (self.test_emp_id, locate_close_date, locate_id)
            self.tdb.runsql(sql)

            # set some variables
            client_code = t.locates[2].client_code
            self.assertEquals(client_code, 'PEHZ51')

            rows = [{'ticket_id': ticket_id,
                     'ticket_number': t.ticket_number,
                     'locate_id': locate_id,
                     'client_code': client_code,
                     'status': status}]
            for row in rows:
                row['ticket_format'] = call_center
                row['ticket_type'] = 'NORMAL'

            import xmlhttpresponder_data as xhrd
            xr = create_mock_responder(rows, verbose=0)
            xr.xmlhttp.responseXML = DummyText("""
              <locateResponses xmlns="http://example.com/foo">
                <auth user="" password=""/>
                <responses>
                  <response>
                    <ticket>1402020001</ticket>
                    <code>PEHZ51</code>
                    %(facility)s
                    <action>20</action><comment/>
                    <result>Success</result>
                  </response>
                </responses>
              </locateResponses>
            """)
            xr.log.logger.logfile = StringIO.StringIO()
            xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
            self.assertEquals(xr.responderdata.all_versions, 0)
            xr.log.lock = 1
            xr.respond_all()
            log = xr.log.logger.logfile.getvalue()
            xr.log.logger.logfile.close()
            self.assertTrue('Responding for: 1421' in log)
            self.assertTrue(('Sending Response: 1401140001, PEHZ51, 20, ,') in log)
            self.assertTrue('Response: Success' in log)
            self.assertTrue('Removing locate from queue: %s' % locate_id in log)
            self.assertTrue('1 responses sent' in log)



#
# auxiliary functions

import et_tools

# XXX maybe move to et_tools?
def find_soap_node(xml, node, namespace=None, debug=False):
    """ Search <xml> for <node> and return the first occurrence. Node will be
        prefixed with the given namespace (as seen in the responder XML
        packets). """
    if namespace is None:
        namespace = "{http://tempuri.org/ContractLocatorResponseDataSet.xsd}"
    doc = ET.fromstring(xml)
    if debug:
        et_tools.print_tree(doc)
    items = doc.findall(".//" + namespace + node)
    return items[0]

#)

if __name__ == "__main__":
    unittest.main()

