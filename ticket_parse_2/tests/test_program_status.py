# test_program_status.py

import os
import unittest
import site; site.addsitedir('.')
#
import datadir
import date
import program_status

class TestProgramStatus(unittest.TestCase):

    def test_001(self):
        mutexname = "main.py:UQ:99"
        q = program_status.write_status(mutexname)
        self.assert_(q is not None)

        # read the status and check if it's really a date
        pid, d = program_status.read_status(mutexname)
        today = date.Date().isodate()[:10]
        self.assertEquals(d[:10], today)
        self.assertEquals(pid, os.getpid())

        # check if the expected filename exists
        filename = os.path.join("status", "ps-main.py-UQ-99.txt")
        self.assert_(datadir.datadir.exists(filename))

        # attempt to read a status that doesn't exist
        e = program_status.read_status("kablooie-xyz")
        self.assertEquals(e, None)



if __name__ == "__main__":

    unittest.main()

