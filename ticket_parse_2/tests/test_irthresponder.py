# test_irthresponder.py

from __future__ import with_statement
import os
import site; site.addsitedir('.')
import unittest
#
import _testprep
import config
import date
import IRTHresponder
import mail2
import ticket_db
import ticketloader
import ticketparser
from search_log_file import SearchLogFile
from mockery import *

class MockIRTHResponder(IRTHresponder.IRTHresponder):
    def __init__(self, *args, **kwargs):
        IRTHresponder.IRTHresponder.__init__(self, *args, **kwargs)
        self.log.lock = 1
        self.sent = []
        self.responses = [] # "from server"
        self.response_gen = self.g_get_response()
    def g_get_response(self):
        for resp in self.responses:
            yield resp
    def login(self, call_center, login):
        pass
    def logout(self):
        pass
    def send_response(self, ticket, membercode, responsecode, explanation=None,
     locator_name='', version=''):
        self.sent.append((ticket, membercode, responsecode, explanation))
        resp = self.response_gen.next()
        return resp


class TestIRTHResponder(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

    def tearDown(self):
        _testprep.clear_database(self.tdb)

    def _try_one(self, status, response):
        t = _testprep.get_test_ticket(os.path.join(_testprep.TICKET_PATH,
            "Atlanta-1.txt"), 0, ["Atlanta"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="USW01")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % (status, locate_id,)
        self.tdb.runsql(sql)

        ir = MockIRTHResponder(verbose=0)
        ir.only_call_center = 'Atlanta' # ignore others
        ir.responses = [response]
        ir.respond_all()
        return ir

    def get_queue(self):
        return self.tdb.getrecords("responder_queue")

    def test_code_ok(self):
        ir = self._try_one('M', "250 OK")
        self.assertEquals(len(ir.sent), 1)
        self.assertEquals(ir.sent[0], ('12311-109-056', 'USW01', '1', ''))
        queued = self.get_queue()
        self.assertEquals(len(queued), 0) # should be removed from queue

    def test_invalid_status(self):
        ir = self._try_one('XL', "250 OK")
        self.assertEquals(len(ir.sent), 0)
        # nothing was sent because of exception
        queued = self.get_queue()
        self.assertEquals(len(queued), 0) # invalid status gets deleted

    def test_unacceptable_code(self):
        ir = self._try_one('M', '666 Bogus')
        self.assertEquals(len(ir.sent), 1)
        queued = self.get_queue()
        self.assertEquals(len(queued), 1) # leave for reprocessing

    def test_code_reprocess(self):
        ir = self._try_one('M', '425 Try again')
        queued = self.get_queue()
        self.assertEquals(len(queued), 1)

    def test_code_error(self):
        ir = self._try_one('M', '451 Invalid ticket')
        queued = self.get_queue()
        self.assertEquals(len(queued), 0) # invalid record must be removed

    def test_responderdata(self):
        ir = MockIRTHResponder(verbose=0)
        ir.only_call_center = 'FAQ1' # ignore others

        # FAQ1 has special rules and should get FAQ1ResponderData:
        ir.load_responder_data('FAQ1', None)
        self.assertEquals(ir.responderdata.__class__.__name__, "FAQ1ResponderData")
        s = ir.make_response('999-888-777', 'TERM01', '1', None)
        self.assertEquals(s, "DATA 999-888-777,UQ Responder,1,1\r\n")

        # also test Atlanta.  there are no special rules for it, so we
        # should use ResponderData:
        ir.load_responder_data('Atlanta', None)
        self.assertEquals(ir.responderdata.__class__.__name__, "ResponderData")
        s = ir.make_response('999-888-777', 'TERM01', '42', 'lajeninaja')
        self.assertEquals(s, "DATA 999-888-777,TERM01,UQ Responder,42,lajeninaja\r\n")

    def test_fcv3_1(self):
        """
        Test delayed responding for fcv3
        """
        t = _testprep.get_test_ticket(os.path.join(_testprep.TICKET_PATH,
            "sample_tickets","fcv3","FCV3-2007-09-30-00025.txt"),
            0, ["VUPSNewFCV3"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        ir = MockIRTHResponder(verbose=0)
        ir.only_call_center = 'FCV3' # ignore others
        ir.responses = ["250 OK"]
        ir.respond_all()
        self.assertEquals(len(ir.sent), 0) # Should not be queued because of 5 min wait

    def test_fcv3_2(self):
        """
        Test delayed responding for fcv3
        """
        t = _testprep.get_test_ticket(os.path.join(_testprep.TICKET_PATH,
            "sample_tickets", "fcv3", "FCV3-2007-09-30-00025.txt"), 0,
            ["VUPSNewFCV3"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)
        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        ir = MockIRTHResponder(verbose=0)
        ir.only_call_center = 'FCV3' # ignore others
        ir.responses = ["250 OK"]
        ir.respond_all()
        self.assertEquals(len(ir.sent), 1) # Should be queued because the locate insert_date should be older than 5 minutes

    def fcv3_3(self,send_emergencies=0,send_3hour=0):
        """
        Test send_emergencies and send_3hour
        """
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","fcv3","FCV3-2007-09-30-00025.txt"))
        raw = tl.tickets[0]
        # Make it a 3hr rush
        raw = raw.replace('NEW  GRID NORM LREQ','3HRS GRID RUSH LREQ FTTP')
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["VUPSNewFCV3"])

        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="AEP111")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        configuration = config.getConfiguration()
        configuration.responders['FCV3']['send_3hour'] = send_3hour
        configuration.responders['FCV3']['send_emergencies'] = send_emergencies

        ir = MockIRTHResponder(verbose=0)
        # Truncate the log file
        ir.log.logger.logfile.truncate(0)
        ir.only_call_center = 'FCV3' # ignore others
        ir.responses = ["250 OK"]
        ir.respond_all()
        return ir,t

    def test_fcv3_3(self):
        """
        Test send_emergencies=1 and send_3hour=1
        """
        ir,t = self.fcv3_3(send_emergencies=1,send_3hour=1)
        self.assertEquals(len(ir.sent), 1) # Should be queued
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Sending Response: %s" % (t.ticket_number))
        self.assertNotEqual(match,None)

    def test_fcv3_4(self):
        """
        Test send_emergencies=1 and send_3hour=0
        """
        ir,t = self.fcv3_3(send_emergencies=1,send_3hour=0)
        self.assertEquals(len(ir.sent), 0) # Should not be queued because send_3hour=0
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Skipping 3 hour ticket %s" % (t.ticket_number))
        self.assertNotEqual(match,None)

    def test_fcv3_5(self):
        """
        Test send_emergencies=0 and send_3hour=1
        """
        ir,t = self.fcv3_3(send_emergencies=0,send_3hour=1)
        self.assertEquals(len(ir.sent), 0) # Should not be queued because send_emergencies=0
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Skipping emergency ticket %s" % (t.ticket_number))
        self.assertNotEqual(match,None)

    def test_fcv3_6(self):
        """
        Test send_emergencies=0 and send_3hour=0
        """
        ir,t = self.fcv3_3(send_emergencies=0,send_3hour=0)
        self.assertEquals(len(ir.sent), 0) # Should not be queued because send_emergencies=0 AND send_3hour=0
        match, next_log_entry = SearchLogFile(ir.log.logger.logfile.name,"Skipping 3 hour ticket %s" % (t.ticket_number))
        self.assertNotEqual(match,None)


    def test_fde2_1(self):
        t = _testprep.get_test_ticket(os.path.join("..","testdata","sample_tickets","FDE2","fde2-2007-09-24-00002.txt"), 0, ["VUPSNewFDE2"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="DPE911")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        ir = MockIRTHResponder(verbose=0)
        ir.only_call_center = 'FDE2' # ignore others
        ir.responses = ["250 OK"]
        ir.respond_all()
        self.assertEquals(len(ir.sent), 0) # Should not be queued because of 5 min wait

    def test_fde2_2(self):
        t = _testprep.get_test_ticket(os.path.join("..","testdata","sample_tickets","FDE2","fde2-2007-09-24-00002.txt"), 0, ["VUPSNewFDE2"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="DPE911")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)
        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        ir = MockIRTHResponder(verbose=0)
        ir.only_call_center = 'FDE2' # ignore others
        ir.responses = ["250 OK"]
        ir.respond_all()
        self.assertEquals(len(ir.sent), 1) # Should be queued because the locate insert_date should be older than 5 minutes

    def test_fpk1_1(self):
        t = _testprep.get_test_ticket(os.path.join("..","testdata","sample_tickets","fpk1","FPK1-2007-09-24-00007.txt"), 0, ["VUPSNewFPK1"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="VZN185")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        ir = MockIRTHResponder(verbose=0)
        ir.only_call_center = 'FPK1' # ignore others
        ir.responses = ["250 OK"]
        ir.respond_all()
        self.assertEquals(len(ir.sent), 0) # Should not be queued because of 5 min wait

    def test_fpk1_2(self):
        t = _testprep.get_test_ticket(os.path.join("..","testdata","sample_tickets","fpk1","FPK1-2007-09-24-00007.txt"), 0, ["VUPSNewFPK1"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="VZN185")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)
        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        ir = MockIRTHResponder(verbose=0)
        ir.only_call_center = 'FPK1' # ignore others
        ir.responses = ["250 OK"]
        ir.respond_all()
        self.assertEquals(len(ir.sent), 1) # Should be queued because the locate insert_date should be older than 5 minutes

    def test_occ2_1(self):
        t = _testprep.get_test_ticket(os.path.join("..","testdata","sample_tickets","occ2","occ2-2007-09-24-00006.txt"), 0, ["VUPSNewOCC2"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="DOM400")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)

        ir = MockIRTHResponder(verbose=0)
        ir.only_call_center = 'OCC2' # ignore others
        ir.responses = ["250 OK"]
        ir.respond_all()
        self.assertEquals(len(ir.sent), 0) # Should not be queued because of 5 min wait

    def test_occ2_2(self):
        t = _testprep.get_test_ticket(os.path.join("..","testdata","sample_tickets","occ2","occ2-2007-09-24-00006.txt"), 0, ["VUPSNewOCC2"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="DOM400")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % ('M', locate_id,)
        self.tdb.runsql(sql)
        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        ir = MockIRTHResponder(verbose=0)
        ir.only_call_center = 'OCC2' # ignore others
        ir.responses = ["250 OK"]
        ir.respond_all()
        self.assertEquals(len(ir.sent), 1) # Should be queued because the locate insert_date should be older than 5 minutes

    def occ2_meet(self,status):
        t = _testprep.get_test_ticket(os.path.join("..","testdata","sample_tickets","occ2","occ2-2007-09-24-00006.txt"), 0, ["VUPSNewOCC2"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="DOM400")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % (status, locate_id,)
        self.tdb.runsql(sql)
        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        mock_sendmail = make_logger()
        with Mockery(mail2, 'sendmail', mock_sendmail):

            ir = MockIRTHResponder(verbose=0)
            ir.log.logger.logfile.truncate(0)
            ir.only_call_center = 'OCC2' # ignore others
            ir.responses = ["250 OK"]
            ir.log.lock = 0
            ir.respond_all()
            # No response since status is a meet status and the ticket is not
            # a meet ticket

        logged = mock_sendmail.log[0][0]

        self.assertEquals(len(ir.sent), 0)
        match, next_log_entry = \
          SearchLogFile(ir.log.logger.logfile.name,
           "Record skipped due to status %s and ticket is not a MEET ticket"
           % status)
        self.assertNotEqual(match, None)

        # Verify fake email
        body = logged[3]
        self.assertNotEquals(body.find(
         'Record skipped due to status %s and ticket is not a MEET ticket'
         % status), -1)

    def test_occ2_meet(self):
        '''
        Verify that a XC meet status on a non-meet ticket will not be sent
        '''
        rows = self.tdb.getrecords('statuslist',meet_only=1)
        self.assertTrue(len(rows) > 0)
        for row in rows:
            self.occ2_meet(row['status'])

    def test_status_code_skip(self):
        status = 'ZZZ'
        t = _testprep.get_test_ticket(os.path.join("..","testdata","sample_tickets","occ2","occ2-2007-09-24-00006.txt"), 0, ["VUPSNewOCC2"])
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locates = self.tdb.getrecords("locate", ticket_id=ticket_id, client_code="DOM400")
        locate_id = locates[0]['locate_id']
        sql = """
         update locate
         set status = '%s'
         where locate_id = %s
        """ % (status, locate_id,)
        self.tdb.runsql(sql)
        date_time = date.Date()
        date_time.dec_time(minutes=6) #Decrement the time by 6 minutes
        iso_date = date_time.isodate()
        sql = """
         update responder_queue
         set insert_date = '%s'
         where locate_id = %s
        """ % (iso_date, locate_id,)
        self.tdb.runsql(sql)

        # Mock the mail.sendmail function
        mock_sendmail = make_logger()
        with Mockery(mail2, 'sendmail', mock_sendmail):

            from StringIO import StringIO

            import irthresponder_data as ird

            ir = MockIRTHResponder(verbose=0)
            ir.only_call_center = 'OCC2' # ignore others
            ir.responses = ["250 OK"]
            ir.log.lock = 0
            ir.log.logger.logfile = StringIO()
            ir.responderdata = ird.getresponderdata(ir.config, 'OCC2', None)
            ir.responderdata.status_code_skip[:] = ['ZZZ',] # No Response
            ir.respond_all()
            # No response since status is a meet status and the ticket is not
            # a meet ticket

        self.assertEquals(len(ir.sent), 0)

        log = ir.log.logger.logfile.getvalue()
        ir.log.logger.logfile.close()
        self.assertEquals(log.count('Skipping: ZZZ'), 1)
        self.assertEquals(log.count('Removing locate from queue'), 1)
        self.assertNotEquals(log.find('--- Warning at'), -1)
        self.assertNotEquals(log.find('ticket number:  A726700008 client_code:  DOM400 work_date:  None status:  ZZZ'), -1)
        self.assertNotEquals(log.find('Mail notification sent to'), -1)
        self.assertNotEquals(log.find('0 responses sent'), -1)

        logged = mock_sendmail.log[0][0]

        # Verify email
        self.assertEquals(logged[2],
          'IRTHresponder warning (Skipped status codes)')
        self.assertNotEqual(logged[3].find(
          'ticket number:  A726700008 client_code:  DOM400 work_date:  '
        + 'None status:  ZZZ'), -1)



if __name__ == "__main__":

    unittest.main()

