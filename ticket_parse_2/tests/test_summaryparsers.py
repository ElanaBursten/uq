# test_summaryparsers.py
# Created: 04 Mar 2002, Hans Nowak
# TODO: Need to start using the testing DSL for this too!

from glob import glob
import os
import re
import site; site.addsitedir('.')
import string
import unittest
#
from search_log_file import SearchLogFile
import _testprep
import call_centers
import config
import date
import formats
import main
import recognize
import summaryparsers
import ticketloader
import ticketparser
import tools

class TestSummaryParsers(unittest.TestCase):

    def setUp(self):
        self.handler = ticketparser.TicketHandler()

    def getticket(self, city, separator="~~~~~"):
        filename = "../testdata/sum-%s.txt" % (city,)
        tl = ticketloader.TicketLoader(filename)
        raw = tl.getTicket()
        return raw

    def test_get_summaryparser(self):
        klass = summaryparsers.get_summaryparser("Atlanta")
        klass = summaryparsers.get_summaryparser("HoustonKorterra")
        klass = summaryparsers.get_summaryparser("NewJersey3")

    def test_Atlanta(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Atlanta'])

        self.assertEquals(len(summ.data), 50)
        self.assertEquals(summ.client_code, "ABS01")
        self.assertEquals(summ.summary_date, "2002-02-05 00:00:00") # - 1
        self.assertEquals(summ.summary_date_end, "2002-02-06 00:00:00")
        self.assertEquals(summ.call_center, "Atlanta")
        self.assertEquals(summ.expected_tickets, 50)
        t = summ.data[0]
        self.assertEquals(t[0], "00002")
        self.assertEquals(t[1], "01302-106-039")
        self.assertEquals(t[2], "00:09")
        self.assertEquals(t[3], "001")
        self.assertEquals(t[4], "L")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)

    def test_Atlanta_New(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","GA_summaries","3003-2008-12-01-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Atlanta4'])

        self.assertEquals(len(summ.data), 71)
        self.assertEquals(summ.client_code, "4720")
        self.assertEquals(summ.summary_date, "2008-11-30 00:00:00") # - 1
        self.assertEquals(summ.summary_date_end, "2008-12-01 00:00:00")
        self.assertEquals(summ.call_center, "FAU4")
        self.assertEquals(summ.expected_tickets, 71)
        t = summ.data[5]
        self.assertEquals(t.item_number, "00006")
        self.assertEquals(t.ticket_number, '11308-263-001')
        self.assertEquals(t.ticket_time, '07:06')
        self.assertEquals(t.revision, "001")
        self.assertEquals(t.type_code, "C")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)

    def test_Atlanta_New_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","GA_summaries","3003-2008-12-09-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Atlanta4'])

        self.assertEquals(len(summ.data), 986)
        self.assertEquals(summ.client_code, "STS99")
        self.assertEquals(summ.summary_date, '2008-12-08 00:00:00')
        self.assertEquals(summ.summary_date_end, '2008-12-09 00:00:00')
        self.assertEquals(summ.call_center, "FAU4")
        self.assertEquals(summ.expected_tickets, 986)

    def test_Atlanta_New_3(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","GA_summaries","FAU4-2008-12-09-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Atlanta4'])

        self.assertEquals(len(summ.data), 455)
        self.assertEquals(summ.client_code, 'UQA01')
        self.assertEquals(summ.summary_date, '2008-12-08 00:00:00')
        self.assertEquals(summ.summary_date_end, '2008-12-09 00:00:00')
        self.assertEquals(summ.call_center, "FAU4")
        self.assertEquals(summ.expected_tickets, 455)

    def testAtlantaSummaryHeaderFooter(self):
        # Test headers and footers.
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta.txt")
        summ = self.handler.parse(tl.tickets[0], ['Atlanta'])
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)

        # tickets 1 and 2 are multiparts:
        summ1 = self.handler.parse(tl.tickets[1], ['Atlanta'])
        self.assertEquals(summ1._has_header, 1)
        self.assertEquals(summ1._has_footer, 0)

        summ2 = self.handler.parse(tl.tickets[2], ['Atlanta'])
        self.assertEquals(summ2._has_header, 1)
        # Atlanta seems to always have headers, even with multiparts
        self.assertEquals(summ2._has_footer, 1)

        self.assertEquals(len(summ1.data) + len(summ2.data),
         summ2.expected_tickets)

    def testNorthCarolinaSummary(self):
        data = self.getticket("NorthCarolina")
        summ = self.handler.parse(data, ['NorthCarolina'])
        self.assertEquals(summ.call_center, "FCL1")
        self.assertEquals(len(summ.data), 141)
        self.assertEquals(summ.client_code, "BEC02")
        self.assertEquals(summ.summary_date, "2002-02-06 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-02-07 00:00:00")
        self.assertEquals(len(summ.data), summ.expected_tickets)
        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "A0060496")
        self.assertEquals(t[2], "08:00")
        self.assertEquals(t[3], "00A")
        self.assertEquals(t[4], "")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)

    def testNorthCarolinaSummary2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-NorthCarolina.txt")
        raw = tl.tickets[1] # second one
        summ = self.handler.parse(raw, ["NorthCarolina"])
        # this ticket starts with line noise, but we should be able to parse it
        # anyway, without the first line being mis-recognized
        self.assertEquals(summ.call_center, "FCL1")
        self.assertEquals(summ.summary_date, "2002-05-16 00:00:00")
        self.assertEquals(summ.client_code, "DPC1L")
        self.assertEquals(summ.expected_tickets, 104)
        self.assertEquals(len(summ.data), 104)

        raw = tl.tickets[2]
        summ = self.handler.parse(raw, ["NorthCarolina"])
        self.assertEquals(summ.call_center, "FCL1")
        self.assertEquals(summ.summary_date, "2002-09-26 00:00:00")
        self.assertEquals(summ.client_code, "DPC78")
        self.assertEquals(summ.expected_tickets, 21)
        self.assertEquals(len(summ.data), 21)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "A0536475")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "00A")

    def testNewJerseySummary_1(self):
        data = self.getticket("NewJersey")
        summ = self.handler.parse(data, ['NewJersey'])
        self.assertEquals(len(summ.data), 6)
        self.assertEquals(summ.summary_date, "2002-03-13 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-03-14 00:00:00")
        self.assertEquals(summ.client_code, "CCI")
        self.assertEquals(summ.call_center, "NewJersey")
        self.assertEquals(summ.expected_tickets, 6)
        self.assertTrue(summ.expected_tickets == len(summ.data))
        t = summ.data[0]
        self.assertEquals(t.item_number, '0215')
        self.assertEquals(t.ticket_number, '020720373')
        self.assertEquals(t.ticket_time, '09:43')
        self.assertEquals(t.revision, "")
        self.assertEquals(t.type_code, "")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)

    def testNewJerseySummary_2(self):
        tickets = tools.safeglob(os.path.join("..","testdata","sample_tickets","NewJersey2","GSU1-2009-04-28-000*.txt"))
        for ticket in tickets:
            tl = ticketloader.TicketLoader(ticket)
            raw = tl.getTicket()
            summ = self.handler.parse(raw, ["NewJersey"])

    def testNewJerseySummaryHeaderFooter(self):
        tl = ticketloader.TicketLoader("../testdata/sum-NewJersey.txt")

        summ3 = self.handler.parse(tl.tickets[3], ['NewJersey'])
        self.assertEquals(summ3.client_code, "NJN")
        self.assertEquals(summ3._has_header, 1)
        self.assertEquals(summ3._has_footer, 0)

        summ4 = self.handler.parse(tl.tickets[4], ['NewJersey'])
        self.assertEquals(summ4.client_code, "NJN")
        self.assertEquals(summ4._has_header, 0)
        self.assertEquals(summ4._has_footer, 0)

        total = 0
        for i in (3, 4, 5, 6):
            summ = self.handler.parse(tl.tickets[i], ['NewJersey'])
            total = total + len(summ.data)
        self.assertTrue(summ.expected_tickets <= total)

    def testNewJersey_summary_end_date(self):
        data = self.getticket("NewJersey")
        # a real example: tickets sent on 5, 6 and 7 Apr... summary date is
        # 7 Apr (on summary itself)... but should span 5 -8 Apr instead.
        data = string.replace(data, "020313", "020407")
        summ = self.handler.parse(data, ['NewJersey'])

        self.assertEquals(summ.summary_date, "2002-04-05 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-04-08 00:00:00")

    def test_Pennsylvania_1(self):
        data = self.getticket("Pennsylvania", chr(12))
        summ = self.handler.parse(data, ['Pennsylvania'])
        self.assertEquals(summ.call_center, "FWP2")
        self.assertEquals(len(summ.data), 21)
        self.assertEquals(summ.summary_date, "2002-03-13 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-03-14 00:00:00")
        self.assertEquals(summ.client_code, "KP")
        self.assertEquals(summ.expected_tickets, 21)
        self.assertEquals(summ.call_center, "FWP2")
        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "0720063")
        self.assertEquals(t[2], "07:23")
        self.assertEquals(t[3], "") # no revision for PA summaries
        self.assertEquals(t[4], "N")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)

    def test_Pennsylvania_2(self):
        data = self.getticket("Pennsylvania", chr(12))
        data = data.replace('*            03/13/02 FROM PENNSYLVANIA ONE CALL SYSTEM, INC.             *',
                            '*                     FROM PENNSYLVANIA ONE CALL SYSTEM, INC.             *')
        summ = self.handler.parse(data, ['Pennsylvania'])
        self.assertEquals(summ.summary_date, "")

    def test_Pennsylvania_3(self):
        data = self.getticket("Pennsylvania", chr(12))
        data = data.replace('    1 0720063 N  07:23AM',
                            '    1 0720063 N  12:23AM')
        summ = self.handler.parse(data, ['Pennsylvania'])
        self.assertEquals(summ.data[0].ticket_time, "00:23")

    def testRichmond1Summary(self):
        data = self.getticket("Richmond1", chr(12))
        summ = self.handler.parse(data, ["Richmond1"])
        self.assertEquals(summ.call_center, "FCV1")
        self.assertEquals(len(summ.data), 42)
        self.assertEquals(summ.summary_date, "2002-03-13 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-03-14 00:00:00")
        self.assertEquals(summ.client_code, "ADCC01")
        self.assertEquals(summ.expected_tickets, 42)
        self.assertEquals(summ.call_center, "FCV1")
        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "00174115") # parser prepends zeroes
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "48HR")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)

    def testRichmond2Summary_1(self):
        data = self.getticket("Richmond2", chr(12))
        summ = self.handler.parse(data, ['Richmond2'])
        self.assertEquals(summ.call_center, "FCV2")
        self.assertEquals(len(summ.data), 156)
        self.assertEquals(summ.summary_date, "2002-03-14 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-03-15 00:00:00")
        self.assertEquals(summ.client_code, "-")    # and not UTIL11
        self.assertEquals(summ.expected_tickets, 0)
        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "0128193")  # parser prepends zero
        self.assertEquals(t[2], "06:28")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "E")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 0)

        # test the 9th ticket
        filename = "../testdata/sum-Richmond2.txt"
        tl = ticketloader.TicketLoader(filename)
        for i in range(9):
            raw = tl.getTicket()
        summ = self.handler.parse(raw, ['Richmond2'])
        self.assertEquals(summ.expected_tickets, 1382)
        self.assertEquals(summ._has_header, 0)
        self.assertEquals(summ._has_footer, 1)

        # test all 9 parts of multipart tickets
        total = 0
        for i in range(9):
            summ = self.handler.parse(tl.tickets[i], ['Richmond2'])
            total = total + len(summ.data)
        self.assertEquals(summ.expected_tickets, total)

    def testRichmond2Summary_2(self):
        data = self.getticket("Richmond2", chr(12))
        data = data.replace('TICKET SUMMARY REPORT AT 03/14/02 07:09:45 PM FOR UTIL11               PAGE 1 ',
                            'TICKET SUMMARY REPORT AT                      FOR UTIL11               PAGE 1 ')
        summ = self.handler.parse(data, ['Richmond2'])
        self.assertEquals(summ.summary_date, "")

    def testWestVirginiaSummary(self):
        tl = ticketloader.TicketLoader("../testdata/sum-WestVirginia.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ['WestVirginia'])

        # WestVirginia's summary date is the current date; if it's before 8 AM,
        # presume the previous day
        today = date.Date()
        if today.hours < 8:
            today.dec()
        today.resettime()

        self.assertEquals(summ.call_center, "FWP1")
        self.assertEquals(summ.client_code, "AX")
        self.assertEquals(summ.summary_date, today.isodate())
        self.assertEquals(summ.expected_tickets, 12)
        self.assertEquals(len(summ.data), summ.expected_tickets)
        t = summ.data[0]
        self.assertEquals(t[0], "0005")
        self.assertEquals(t[1], "720008")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "wv")

        # test summaries without date
        tl = ticketloader.TicketLoader("../testdata/sum-WestVirginia-2.txt")
        raw = tl.tickets[4]
        summ = self.handler.parse(raw, ['WestVirginia'])

        self.assertEquals(summ.client_code, "GAN")
        today = date.Date()
        if today.hours < 8:
            today.dec()
        today.resettime()
        self.assertEquals(summ.summary_date, today.isodate())

    def testBatonRougeSummary(self):
        tl = ticketloader.TicketLoader("../testdata/sum-BatonRouge1.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["BatonRouge1"])

        self.assertEquals(summ.call_center, "FBL1")
        self.assertEquals(summ.client_code, "LF01")
        self.assertEquals(summ.summary_date, "2002-03-04 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-03-05 00:00:00")
        self.assertEquals(summ.expected_tickets, 186)
        self.assertEquals(len(summ.data), summ.expected_tickets)
        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "58108")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "48HR")

        u = summ.data[128]
        self.assertEquals(u[0], "33")

    def testFairfaxSummaryParser(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Fairfax.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Fairfax"])

        self.assertEquals(summ.call_center, "OCC1")
        self.assertEquals(summ.client_code, "CLG01")
        self.assertEquals(summ.summary_date, "2002-03-13 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-03-14 00:00:00")
        self.assertEquals(summ.expected_tickets, 147)
        self.assertEquals(len(summ.data), summ.expected_tickets)
        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "00174168") # parser prepends zeroes
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "48HR")

    def test_OhioSummaryParser_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Ohio.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ['Ohio'])

        self.assertEquals(summ.call_center, "FWP3")
        self.assertEquals(summ.client_code, "AGP")
        self.assertEquals(summ.summary_date, "2002-03-13 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-03-14 00:00:00")
        self.assertEquals(summ.expected_tickets, 15)
        self.assertEquals(len(summ.data), 15)
        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "0313-076-020-00")

    def test_OhioSummaryParser_2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Ohio.txt")
        raw = tl.getTicket()
        raw = raw.replace('TOTALS FOR CDC AGP      2             0             6             7',
                          'TOTALS FOR CDC AGP')
        summ = self.handler.parse(raw, ['Ohio'])

        self.assertEquals(summ.expected_tickets, 0)

    def testSouthCarolinaSummaryParser(self):
        tl = ticketloader.TicketLoader("../testdata/sum-SouthCarolina.txt")
        raw = tl.getTicket()
        handler = ticketparser.TicketHandler()
        summ = handler.parse(raw, ["SouthCarolina"])

        self.assertEquals(summ.call_center, "FCL2") # may break later
        self.assertEquals(summ.client_code, "TWGZ98")
        self.assertEquals(summ.summary_date, "2002-04-28 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-04-29 00:00:00")
        self.assertEquals(summ.expected_tickets, 0)

        raw = tl.tickets[2]
        summ = handler.parse(raw)
        self.assertEquals(summ.client_code, "TWJZ54")
        self.assertEquals(summ.summary_date, "2002-04-26 00:00:00")
        self.assertEquals(summ.expected_tickets, 13)
        self.assertEquals(len(summ.data), 13)
        t = summ.data[0]
        self.assertEquals(t[0], "001")
        self.assertEquals(t[1], "2137210")    # may break later
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[4], "NORMAL")

    def test_OrlandoSummaryParser_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Orlando.txt")
        raw = tl.getTicket()
        handler = ticketparser.TicketHandler()
        summ = handler.parse(raw, ["Orlando"])

        self.assertEquals(summ.call_center, "FOL1")
        self.assertEquals(summ.summary_date, "2002-04-26 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-04-27 00:00:00")
        self.assertEquals(summ.client_code, "BYERS3")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 0)
        self.assertEquals(summ.expected_tickets, 0)
        self.assertEquals(len(summ.data), 174)

        # the ticket above is part 1 of 2. here comes the next one:
        raw = tl.getTicket()
        summ = handler.parse(raw, ["Orlando"])
        self.assertEquals(summ.call_center, "FOL1")
        self.assertEquals(summ.summary_date, "2002-04-26 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-04-27 00:00:00")
        self.assertEquals(summ.client_code, "BYERS3")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 326)
        self.assertEquals(len(summ.data), 326-174)

    def test_OrlandoSummaryParser_2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Orlando.txt")
        raw = tl.getTicket()
        raw = raw.replace('BYERS3','BYERS3*')
        handler = ticketparser.TicketHandler()
        summ = handler.parse(raw, ["Orlando"])

        self.assertEquals(summ.client_code, "BYERS3")

    def test_OrlandoSummaryParser_3(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Orlando.txt")
        raw = tl.getTicket()
        raw = raw.replace('BYERS3 CALL SUNSHINE 04/26/02 23:45:46ET',
                          'BYERS3 CALL SUNSHINE')
        handler = ticketparser.TicketHandler()
        summ = handler.parse(raw, ["Orlando"])

        self.assertEquals(summ.summary_date, "")

    def test_Orlando_bug_1(self):
        # test Orlando "buggy"
        tl = ticketloader.TicketLoader("../testdata/sum-Orlando-bug-1.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Orlando'])

    def test_TennesseeSummaryParser_1(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","TNOCS","Audit-2008-12-19-00121.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Tennessee"])

        self.assertEquals(summ.client_code, "UTILNASH")
        self.assertEquals(summ.call_center, "FTL1")
        self.assertEquals(summ.summary_date, '2008-12-19 00:00:00')
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 120)
        self.assertEquals(len(summ.data), 120)

        t = summ.data[0]
        self.assertEquals(t.item_number, "0043")
        self.assertEquals(t.ticket_number, '083430147')
        self.assertEquals(t.ticket_time, "00:00")

        for t in summ.data:
            self.assert_(t[0].startswith("0"),
             "Invalid sequence: %s" % (t,))

    def test_TennesseeSummaryParser_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","TNOCS","FJL3-2009-01-20-00001.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Tennessee"])

        self.assertEquals(summ.client_code, "B10")
        self.assertEquals(summ.call_center, "FTL1")
        self.assertEquals(summ.summary_date, '2009-01-19 00:00:00')
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 1)
        self.assertEquals(len(summ.data), 1)

        t = summ.data[0]
        self.assertEquals(t.item_number, "0001")
        self.assertEquals(t.ticket_number, '090190735')
        self.assertEquals(t.ticket_time, "00:00")

        for t in summ.data:
            self.assert_(t[0].startswith("0"),
             "Invalid sequence: %s" % (t,))

    def test_TennesseeSummaryParser_3(self):
        tickets = tools.safeglob(os.path.join("../testdata","sample_tickets","TNOCS","*-2009-01-20-*.txt"))
        for ticket in tickets:
            tl = ticketloader.TicketLoader(ticket)
            raw = tl.getTicket()
            summ = self.handler.parse(raw, ["Tennessee"])
            self.assertEquals(summ._has_header, 1)
            self.assertEquals(summ._has_footer, 1)

    def test_TennesseeSummaryParser_4(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","TNOCS","FJL3-2009-01-20-00001.txt"))
        raw = tl.getTicket()
        raw = raw.replace("AUDIT FOR 1/19/2009","AUDIT FOR")
        summ = self.handler.parse(raw, ["Tennessee"])

        self.assertEquals(summ.summary_date, '')

    def test_DIGGTESS_SummaryParser(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FHL","FHL2-2009-01-13-00001.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["DIGGTESS"])

        self.assertEquals(summ.client_code, "P78")
        self.assertEquals(summ.call_center, "FDX1") # Whichever call center using DIGGTESS in cc2format comes first
        self.assertEquals(summ.summary_date, '2009-01-12 00:00:00')
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 26)
        self.assertEquals(len(summ.data), 26)

        t = summ.data[0]
        self.assertEquals(t.item_number, "0001")
        self.assertEquals(t.ticket_number, '090120059')
        self.assertEquals(t.ticket_time, "00:00")

        for t in summ.data:
            self.assert_(t[0].startswith("0"),
             "Invalid sequence: %s" % (t,))

    def testBaltimoreSummaryParser(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Baltimore.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Baltimore"])

        self.assertEquals(summ.client_code, "CPW01")
        self.assertEquals(summ.call_center, "FMB1")
        self.assertEquals(summ.summary_date, "2002-05-13 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 33)
        self.assertEquals(len(summ.data), 33)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "322552")   # no more leading zeroes
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "NR")

        # test another summary
        raw = tl.tickets[2] # the third one
        summ = self.handler.parse(raw, ["Baltimore"])
        self.assertEquals(len(summ.data), 29)
        t = summ.data[10]
        self.assertEquals(t[0], "18")   # and not "18*"
        self.assertEquals(t[1], "318742")   # no more leading zeroes

    def testWashingtonSummaryParser(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Washington.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Washington"])

        self.assertEquals(summ.client_code, "JTV02")
        self.assertEquals(summ.call_center, "FMW1")
        self.assertEquals(summ.summary_date, "2002-05-13 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 88)
        self.assertEquals(len(summ.data), 88)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "351304") # don't prepend zeroes
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "UPDT")

    def testRichmond3SummaryParser(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Richmond3.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Richmond3"])

        self.assertEquals(summ.client_code, "ZZAP12")
        self.assertEquals(summ.summary_date, "2002-06-21 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 218)
        self.assertEquals(len(summ.data), 218)

        t = summ.data[0]
        self.assertEquals(t[0], "03268")
        self.assertEquals(t[1], "A217200071")   # no revision
        self.assertEquals(t[2], "08:42")
        self.assertEquals(t[3], "00A")
        self.assertEquals(t[4], "")

    def testRichmond3SummaryParser_2(self):
        # now test a *real* ticket
        tl = ticketloader.TicketLoader("../testdata/sum-Richmond3.txt")
        raw = tl.tickets[2]
        summ = self.handler.parse(raw, ["Richmond3"])

        self.assertEquals(summ.client_code, "UTIL11")
        self.assertEquals(summ.summary_date, "2002-07-01 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 711)
        self.assertEquals(len(summ.data), 711)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "A218200006")
        self.assertEquals(t[2], "08:06")
        self.assertEquals(t[3], "00A")
        self.assertEquals(t[4], "E")

    def test_Fairfax2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Fairfax2.txt")
        raw = tl.tickets[1]
        summ = self.handler.parse(raw, ["Fairfax2"])

        self.assertEquals(summ.call_center, "OCC2")
        self.assertEquals(summ.client_code, "UTIL13")
        self.assertEquals(summ.summary_date, "2003-06-11 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 14)
        self.assertEquals(len(summ.data), 14)

        t = summ.data[0]
        self.assertEquals(t.getdata(),
         ["00001", "A316202180", "16:23", "00A", "NS"])

    def testJacksonville_1(self):
        # Basically the same as Orlando. Just like Orlando, it has a special
        # way to compute summary_date
        tl = ticketloader.TicketLoader("../testdata/sum-Jacksonville.txt")
        raw = tl.tickets[9]
        summ = self.handler.parse(raw)
        # it should have the *previous* date
        self.assertEquals(summ.summary_date, "2002-07-01 00:00:00")

        raw = tl.tickets[0]
        summ = self.handler.parse(raw)
        # take the *same* date
        self.assertEquals(summ.summary_date, "2002-05-08 00:00:00")

    def OLD_testColorado_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Colorado.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Colorado"])

        self.assertEquals(summ.call_center, "FCO1")
        self.assertEquals(summ.client_code, "MVEL02")
        self.assertEquals(summ.summary_date, "2002-07-29 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 164)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "0455157")
        self.assertEquals(t[2], "07:39")
        self.assertEquals(t[3], "000")
        self.assertEquals(t[4], "")

    # For nose
    OLD_testColorado_1.will_fail = True

    def test_Colorado_new(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Colorado-3.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Colorado'])

        self.assertEquals(summ.call_center, 'FCO1')
        self.assertEquals(summ.client_code, 'UNCC')
        self.assertEquals(summ.summary_date, "2004-06-21 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 4)

        t = summ.data[0]
        self.assertEquals(t[0], "0000")
        self.assertEquals(t[1], "0353254")
        self.assertEquals(t[2], "00:00")

        raw = tl.tickets[-1]
        summ = self.handler.parse(raw, ['Colorado'])
        self.assertEquals(summ.summary_date, "2004-06-21 00:00:00")

    test_Colorado_new.has_error=1

    def test_Colorado2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Colorado2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Colorado2"])

        self.assertEquals(summ.call_center, 'FCO2')
        self.assertEquals(summ.client_code, 'PCNDU0')
        self.assertEquals(summ.summary_date, "2005-03-29 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 134)
        self.assertEquals(len(summ.data), 134)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "B0000074")
        self.assertEquals(t[3], "00B")

    test_Colorado2.has_error=1

    def test_SanDiego_1(self):
        # XXX SanDiego doesn't exist anymore, but this test may be useful since
        # it's essentially SouthCalifornia
        tl = ticketloader.TicketLoader("../testdata/sum-SanDiego.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["SanDiego"])

        self.assertEquals(summ.call_center, "SCA2")
        self.assertEquals(summ.client_code, "UTLQST")
        self.assertEquals(summ.summary_date, "2002-07-30 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 92)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "A0010229")
        self.assertEquals(t[2], "07:37")
        self.assertEquals(t[3], "00A")
        self.assertEquals(t[4], "")

    def __test_Illinois_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Illinois.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Illinois"])

        self.assertEquals(summ.call_center, "Illinois") # FIXME
        self.assertEquals(summ.client_code, "UQST0A")
        self.assertEquals(summ.summary_date, "2002-07-30 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 1248)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "2110010")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], " ")

    def test_NorthCalifornia_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-NorthCalifornia.txt")
        raw = tl.tickets[1]
        summ = self.handler.parse(raw, ["NorthCalifornia"])

        self.assertEquals(summ.call_center, "NCA1")
        self.assertEquals(summ.client_code, "PBTHA5")
        self.assertEquals(summ.summary_date, "2002-09-13 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 0)
        self.assertEquals(summ.expected_tickets, 0)
        self.assertEquals(len(summ.data), 59*5)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "0339300")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "+")

        raw = tl.tickets[3]
        summ = self.handler.parse(raw)
        self.assertEquals(summ.expected_tickets, 733)

        raw = tl.tickets[4]
        summ = self.handler.parse(raw)
        self.assertEquals(summ.expected_tickets, 246)
        self.assertEquals(summ.summary_date, "2002-10-10 00:00:00")

    def test_Alabama_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Alabama.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Alabama"])

        self.assertEquals(summ.call_center, "FAU2")
        self.assertEquals(summ.client_code, "MOBG01")
        self.assertEquals(summ.summary_date, "2002-09-27 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 54)
        self.assertEquals(len(summ.data), 54)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "0268656")
        self.assertEquals(t[2], "07:56")
        self.assertEquals(t[3], "000")
        self.assertEquals(t[4], "R")

    def test_Kentucky_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Kentucky.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Kentucky"])

        self.assertEquals(summ.client_code, "0224")
        self.assertEquals(summ.summary_date, "2002-11-19 18:31:11")
        # FIXME this doesn't seem right! summary dates have 00:00:00!
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 21)
        self.assertEquals(len(summ.data), 21)

        t = summ.data[0]
        self.assertEquals(t[1], "20024700555")

    def test_Dallas1_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Dallas1.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Dallas1"])

        self.assertEquals(summ.call_center, "FDX1")
        self.assertEquals(summ.client_code, "CP3")
        self.assertEquals(summ.summary_date, "2002-10-31 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 6)
        self.assertEquals(len(summ.data), 6)

        t = summ.data[0]
        self.assertEquals(t[0], "0007")
        self.assertEquals(t[1], "023040836")

        # test the "weekend -> Friday" rule, using 22 Dec 2002
        raw = string.replace(raw, "021031", "021222")
        summ = self.handler.parse(raw, ["Dallas1"])
        self.assertEquals(summ.summary_date, "2002-12-20 00:00:00")

    def __test_Dallas2_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Dallas2.txt")
        raw = tl.tickets[8]
        summ = self.handler.parse(raw, ["Dallas2"])

        self.assertEquals(summ.call_center, "FDX2")
        self.assertEquals(summ.client_code, "KL2")
        self.assertEquals(summ.summary_date, "2002-10-18 00:00:00") # !
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 41)
        self.assertEquals(len(summ.data), 41)

        t = summ.data[0]
        self.assertEquals(t[0], "0035")
        self.assertEquals(t[1], "022915572")

    def __test_Dallas2_2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Dallas2-2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Dallas2"])

        self.assertEquals(summ.call_center, "FDX2")
        self.assertEquals(summ.client_code, "CCK")
        self.assertEquals(summ.summary_date, "2002-10-25 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 16)
        self.assertEquals(len(summ.data), 16)

        t = summ.data[0]
        self.assertEquals(t[0], "0012")
        self.assertEquals(t[1], "022981112")

    def test_Arkansas_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Arkansas.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Arkansas"])

        self.assertEquals(summ.call_center, "FDX3")
        self.assertEquals(summ.client_code, "VALOR")
        self.assertEquals(summ.summary_date, "2002-10-20 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 1)
        self.assertEquals(len(summ.data), 1)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "021020-0003")

    def test_Arkansas_2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Arkansas-2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Arkansas"])

        self.assertEquals(summ.call_center, "FDX3")
        self.assertEquals(summ.client_code, "VALOR")
        self.assertEquals(summ.summary_date, "2005-01-10 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 43)
        self.assertEquals(len(summ.data), 43)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "050110-0038")

    def test_Arkansas_3(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Arkansas-2.txt")
        raw = tl.tickets[0]
        raw = raw.replace('AUDIT FOR 01/10/05',
                          'AUDIT FOR ')
        summ = self.handler.parse(raw, ["Arkansas"])

        self.assertEquals(summ.summary_date, "")

    def test_Tulsa_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Tulsa.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Tulsa"])

        self.assertEquals(summ.call_center, "FOK1")
        self.assertEquals(summ.client_code, "R09912")
        self.assertEquals(summ.summary_date, "2002-10-21 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 6)
        self.assertEquals(len(summ.data), 6)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "02102107480140")

    def test_SanAntonio_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-SanAntonio.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["SanAntonio"])

        self.assertEquals(summ.call_center, "FSA1")
        self.assertEquals(summ.client_code, "L40")
        self.assertEquals(summ.summary_date, "2002-11-01 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 5)
        self.assertEquals(len(summ.data), 5)

        t = summ.data[0]
        self.assertEquals(t[0], "0091")
        self.assertEquals(t[1], "023051058")

    def test_Harlingen_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Harlingen.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Harlingen"])

        self.assertEquals(summ.call_center, "FTS1")
        self.assertEquals(summ.client_code, "CSD")
        self.assertEquals(summ.summary_date, "2002-10-18 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 141)
        self.assertEquals(len(summ.data), 141)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "2910738")
        # this may not be a real ticket...

    def test_Harlingen_2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Harlingen.txt")
        raw = tl.tickets[2]
        summ = self.handler.parse(raw, ["Harlingen"])

        self.assertEquals(summ.call_center, "FTS1")
        self.assertEquals(summ.client_code, "SSC")
        self.assertEquals(summ.summary_date, "2002-10-18 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 70)
        self.assertEquals(len(summ.data), 70)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "022915017")

    def test_Harlingen_3(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Harlingen-2.txt")
        raw = tl.tickets[2]
        summ = self.handler.parse(raw, ["Harlingen"])

        self.assertEquals(summ.call_center, "FTS1")
        self.assertEquals(summ.client_code, "CSD")
        # XXX summary date?
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 126)
        self.assertEquals(len(summ.data), 126)

    def test_Houston1_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Houston1.txt")
        raw = tl.tickets[2]
        summ = self.handler.parse(raw, ["Houston1"])

        self.assertEquals(summ.call_center, "FHL1")
        self.assertEquals(summ.client_code, "TWC40")
        self.assertEquals(summ.summary_date, "2002-10-20 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 7)
        self.assertEquals(len(summ.data), 7)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "29200172")
        self.assertEquals(t[4], "ROUT")

    def test_Houston1Alt(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Houston2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Houston1', 'Houston1Alt'])

        self.assertEquals(summ.call_center, 'FHL1')
        self.assertEquals(summ.client_code, 'XXX')
        self.assertEquals(summ.summary_date, '2004-03-10 00:00:00')
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 136)
        self.assertEquals(len(summ.data), 136)

        # rest is same as Houston2

    def test_AlabamaMobile_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-AlabamaMobile.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["AlabamaMobile"])

        self.assertEquals(summ.call_center, "FAM1")
        self.assertEquals(summ.client_code, "MOBG01")
        self.assertEquals(summ.summary_date, "2002-10-11 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 77)
        self.assertEquals(len(summ.data), 77)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "0282107")
        self.assertEquals(t[2], "07:20")
        self.assertEquals(t[3], "000")
        self.assertEquals(t[4], "")

    def test_AlabamaNew_1(self):
        # fake a call center code, to prevent errors
        import call_centers
        if not call_centers.format2cc.has_key("AlabamaNew"):
            call_centers.format2cc["AlabamaNew"] = "FAM0"

        tl = ticketloader.TicketLoader("../testdata/sum-AlabamaNew.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["AlabamaNew"])

        self.assertEquals(summ.client_code, "DECU01")
        self.assertEquals(summ.summary_date, "2002-11-13 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 25)
        self.assertEquals(len(summ.data), 25)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "02317-0010")

    def test_Mississippi_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Mississippi.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Mississippi"])

        self.assertEquals(summ.call_center, "FMS1")
        self.assertEquals(summ.client_code, "RC7")
        self.assertEquals(summ.summary_date, "2002-11-30 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 3)
        self.assertEquals(len(summ.data), 3)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "02113007360001")

    def test_Mississippi2_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Mississippi2.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Mississippi2"])

        self.assertEquals(summ.call_center, "FJL1")
        self.assertEquals(summ.client_code, "RC1")
        self.assertEquals(summ.summary_date, "2002-11-03 00:00:00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 1)
        self.assertEquals(len(summ.data), 1)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "02110314580002")

    def testRichmondNCSummary(self):
        tl = ticketloader.TicketLoader("../testdata/sum-NorthCarolina.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["RichmondNC"])
        #num = p.read(data)
        self.assertEquals(summ.call_center, "FCV4")
        self.assertEquals(len(summ.data), 141)
        self.assertEquals(summ.client_code, "BEC02")
        self.assertEquals(summ.summary_date, "2002-02-06 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-02-07 00:00:00")
        self.assertEquals(len(summ.data), summ.expected_tickets)
        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "A0060496")
        self.assertEquals(t[2], "08:00")
        self.assertEquals(t[3], "00A")
        self.assertEquals(t[4], "")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)

    def testAlabamaFLSummaryParser(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Orlando.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["AlabamaFL"])

        self.assertEquals(summ.call_center, "FAM2")
        self.assertEquals(summ.summary_date, "2002-04-26 00:00:00")
        self.assertEquals(summ.summary_date_end, "2002-04-27 00:00:00")
        self.assertEquals(summ.client_code, "BYERS3")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 0)
        self.assertEquals(summ.expected_tickets, 0)
        self.assertEquals(len(summ.data), 174)

    def testDelawareSummaryParser(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Delaware1.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Delaware1"])

        self.assertEquals(summ.call_center, "FDE1")
        self.assertEquals(summ.summary_date, "2003-02-19 00:00:00")
        self.assertEquals(summ.client_code, "CNTV01")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 14)
        self.assertEquals(len(summ.data), 14)

        t = summ.data[0]
        self.assertEquals(t.getdata(), ["1", "1067540", "00:00", "", "48HR"])

        # note: this type of format prepends a 0 to the audit's ticket number,
        # so it will be the same as the number on the actual ticket file

    def testTulsaWellscoSummaryParser(self):
        tl = ticketloader.TicketLoader("../testdata/sum-TulsaWellsco.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["TulsaWellsco"])

        self.assertEquals(summ.call_center, "FOK1")
        self.assertEquals(summ.summary_date, "2003-03-13 00:00:00") # !
        self.assertEquals(summ.client_code, "T09912")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 150)
        self.assertEquals(len(summ.data), 150)

        t = summ.data[0]
        self.assertEquals(t.getdata(),
         ["0089", "03031215261613", "00:00", "", ""])

    def test_Washington2_new(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Washington2-new.txt")
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Washington2"])

        self.assertEquals(summ.call_center, "FMW2")
        self.assertEquals(summ.summary_date, "2003-03-24 00:00:00")
        self.assertEquals(summ.client_code, "MISUTIL")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 961)
        self.assertEquals(len(summ.data), 961)

        t = summ.data[0]
        self.assertEquals(t.getdata(),
         ["00008", "MISUTIL2003032400018", "07:25", "", "*"])

    def test_Albuquerque_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Albuquerque.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Albuquerque"])
        self.assertEquals(summ.call_center, "FAQ1")
        self.assertEquals(summ.summary_date, "2003-04-28 00:00:00")
        self.assertEquals(summ.client_code, "NMOC")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 945)
        self.assertEquals(len(summ.data), 945)

    def test_Albuquerque_2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Albuquerque2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Albuquerque2"])
        self.assertEquals(summ.call_center, "FAQ2")
        self.assertEquals(summ.summary_date, "2003-07-01 00:00:00")
        self.assertEquals(summ.client_code, "PNM6")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 10)
        self.assertEquals(len(summ.data), 10)

        t = summ.data[0]
        self.assertEquals(t[0], "0002")
        self.assertEquals(t[1], "2003270897")

    def test_Albuquerque_3(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Albuquerque.txt")
        raw = tl.tickets[0]
        raw = raw.replace('*  for the period 2002/02/26 00:02:30 thru 2003/04/29 00:04:03.     *',
                          '*                                                                   *')
        today = date.Date()
        if today.hours <= 7:
            today.dec()
        today.resettime()
        summ = self.handler.parse(raw, ["Albuquerque"])
        self.assertEquals(summ.summary_date, today.isodate())

    def test_Wisconsin1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Wisconsin1.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Wisconsin1'])
        self.assertEquals(summ.call_center, "FWI1")
        self.assertEquals(summ.summary_date, "2004-03-04 00:00:00")
        self.assertEquals(summ.client_code, "WE1WN")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 76)
        self.assertEquals(len(summ.data), 76)

        t = summ.data[0]
        self.assertEquals(t[0], "0002")
        self.assertEquals(t[1], "20041003342")

    def test_Oregon(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Oregon.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Oregon"])

        self.assertEquals(summ.call_center, "FOR1")
        self.assertEquals(summ.summary_date, "2003-08-13 00:00:00")
        self.assertEquals(summ.client_code, "TCI18")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 92)
        self.assertEquals(len(summ.data), 92)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "150193")

    def test_Wisconsin2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Wisconsin2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Wisconsin2"])

        self.assertEquals(summ.call_center, 'FWI2')
        self.assertEquals(summ.summary_date, "2003-12-03 00:00:00")
        self.assertEquals(summ.client_code, "DHL03")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 162)
        self.assertEquals(len(summ.data), 162)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "9102785")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "RETR")

    def test_GreenvilleNew(self):
        tl = ticketloader.TicketLoader("../testdata/sum-GreenvilleNew.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["GreenvilleNew"])

        self.assertEquals(summ.call_center, "FGV1")
        self.assertEquals(summ.summary_date, "2004-02-04 00:00:00")
        self.assertEquals(summ.client_code, "ZZQ35")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 1445)
        self.assertEquals(len(summ.data), 1445)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "0402020249")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[4], "")
        # this format doesn't store the result strings like "DELIVERED"

        # test a slightly (?) different format
        tl = ticketloader.TicketLoader("../testdata/sum-GreenvilleNew-2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['GreenvilleNew'])

    def test_WashingtonState(self):
        tl = ticketloader.TicketLoader("../testdata/sum-WashingtonState.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["WashingtonState"])

        self.assertEquals(summ.call_center, 'LWA1')
        self.assertEquals(summ.summary_date, "2004-03-26 00:00:00")
        self.assertEquals(summ.client_code, "TEST03")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 88)
        self.assertEquals(len(summ.data), 88)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "4078037")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[4], "48HR")

    def test_GA3002(self):
        tl = ticketloader.TicketLoader("../testdata/sum-GA3002-1.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["GA3002"])

        self.assertEquals(summ.call_center, "3002")
        self.assertEquals(summ.summary_date, "2005-02-15 00:00:00")
        self.assertEquals(summ.client_code, "3002")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 2307)
        self.assertEquals(len(summ.data), 2307)

        t = summ.data[0]
        self.assertEquals(t[0], "")
        self.assertEquals(t[1], "02155-098-007")
        self.assertEquals(t[3], "0")

        # 2005-10-14: format without headers
        tl = ticketloader.TicketLoader("../testdata/sum-GA3002-2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['GA3002'])

        self.assertEquals(summ.call_center, "3002")
        self.assertEquals(summ.summary_date, "2005-10-12 00:00:00")
        self.assertEquals(summ.client_code, "3002")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 2869)
        self.assertEquals(len(summ.data), 2869)

        t = summ.data[0]
        self.assertEquals(t[0], "")
        self.assertEquals(t[1], "10065-021-006")
        self.assertEquals(t[3], "1")

    def test_AZ1102(self):
        tl = ticketloader.TicketLoader("../testdata/sum-AZ1102-1.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["AZ1102"])

        self.assertEquals(summ.call_center, "1102")
        self.assertEquals(summ.summary_date, "2005-02-21 00:00:00")
        self.assertEquals(summ.client_code, "STSINCE00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 6)
        self.assertEquals(len(summ.data), 6)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "2005020301219")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "001")
        self.assertEquals(t[4], "UPDT")

        # test audit with "empty" line
        tl = ticketloader.TicketLoader("../testdata/sum-AZ1102-2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["AZ1102"])

        self.assertEquals(summ.call_center, "1102")
        self.assertEquals(summ.summary_date, "2005-03-03 00:00:00")
        self.assertEquals(summ.client_code, "STSINCE00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 13)
        self.assertEquals(len(summ.data), 13)

        # test audit with no tickets
        raw = tl.tickets[1]
        summ = self.handler.parse(raw, ["AZ1102"])

        self.assertEquals(summ.call_center, "1102")
        self.assertEquals(summ.summary_date, "2006-09-04 00:00:00")
        self.assertEquals(summ.client_code, "STSINCE00")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 0)
        self.assertEquals(len(summ.data), 0)


    def test_NM1101(self):
        tl = ticketloader.TicketLoader("../testdata/sum-NM1101-1.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NM1101"])

        self.assertEquals(summ.call_center, "1101")
        self.assertEquals(summ.summary_date, "2005-03-01 00:00:00")
        self.assertEquals(summ.client_code, "VTEG")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 5)

        t = summ.data[0]
        self.assertEquals(t[0], "0002")
        self.assertEquals(t[1], "2005101076")
        self.assertEquals(t[2], "00:00")

        # try another one
        raw = tl.tickets[3]
        summ = self.handler.parse(raw, ["NM1101"])
        self.assertEquals(summ.client_code, "JONE")
        self.assertEquals(summ.expected_tickets, 338)
        self.assertEquals(len(summ.data), 338)

    @tools.with_attrs(will_fail=True)
    def test_Washington4(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Washington4.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Washington4"])

        self.assertEquals(summ.call_center, "FMW4")
        self.assertEquals(summ.summary_date, "2005-04-28 00:00:00")
        self.assertEquals(summ.client_code, "FMW4")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 1504)
        self.assertEquals(len(summ.data), 1504)

        t = summ.data[0]
        self.assertEquals(t[0], "")
        self.assertEquals(t[1], "5187543")
        self.assertEquals(t[3], "1")

    def test_VUPSNewOCC3(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ3","OCC3-2009-02-20-00654.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['VUPSNewOCC3'])

        self.assertEquals(summ.call_center, 'OCC3')
        self.assertEquals(summ.summary_date, '2009-02-09 00:00:00')
        self.assertEquals(summ.client_code, 'WGL904')
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 598)
        self.assertEquals(len(summ.data), 598)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.revision, '00B')
        self.assertEquals(t.ticket_number, 'B903501959')
        self.assertEquals(t.ticket_time, '07:00')
        self.assertEquals(t.type_code, 'NL')

    def test_VUPSNewOCC3_2_1(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ3","OCC3-2009-03-19-00918.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['VUPSNewOCC3_2'])

        self.assertEquals(summ.call_center, 'OCC3')
        self.assertEquals(summ.summary_date, '2009-03-18 00:00:00')
        self.assertEquals(summ.client_code, 'WGL904')
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 1036)
        self.assertEquals(len(summ.data), 1036)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.revision, '00B')
        self.assertEquals(t.ticket_number, 'B907700014')
        self.assertEquals(t.ticket_time, '00:00')
        self.assertEquals(t.type_code, 'NORMAL UPDATE')

    def test_VUPSNewOCC3_2_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ3","OCC3-2009-03-19-00919.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['VUPSNewOCC3_2'])

        self.assertEquals(summ.call_center, 'OCC3')
        self.assertEquals(summ.summary_date, '2009-03-18 00:00:00')
        self.assertEquals(summ.client_code, 'WGL904')
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 1036)
        self.assertEquals(len(summ.data), 1036)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.revision, '00B')
        self.assertEquals(t.ticket_number, 'B907700014')
        self.assertEquals(t.ticket_time, '00:00')
        self.assertEquals(t.type_code, 'NORMAL UPDATE')

    def test_VUPSNewOCC3_2_3(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ3","OCC3-2009-03-19-00919.txt"))
        raw = tl.tickets[0]
        raw = raw.replace('WGL904  VUPSXML  ClickScreener EOD for 03/18/2009',
                          'WGL904  VUPSXML  ClickScreener EOD for')
        summ = self.handler.parse(raw, ['VUPSNewOCC3_2'])

        self.assertEquals(summ.summary_date, '')

    def test_HarlingenTESS(self):
        tl = ticketloader.TicketLoader("../testdata/sum-HarlingenTESS.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["HarlingenTESS"])

        self.assertEquals(summ.call_center, "FTS1")
        self.assertEquals(summ.summary_date, "2005-05-10 00:00:00")
        self.assertEquals(summ.client_code, "TESS")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 60)
        self.assertEquals(len(summ.data), 60)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "051290134")
        self.assertEquals(t[2], "10:29")
        self.assertEquals(t[3], "00")
        self.assertEquals(t[4], "")

    def test_Delaware3(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Delaware3.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Delaware3'])

        self.assertEquals(summ.call_center, "FDE3")
        self.assertEquals(summ.summary_date, "2005-07-12 00:00:00")
        self.assertEquals(summ.client_code, "FTTPELEC")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 11)
        self.assertEquals(len(summ.data), 11)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "51900014")

    def test_TX1103(self):
        tl = ticketloader.TicketLoader("../testdata/sum-TX1103.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['TX1103'])

        self.assertEquals(summ.call_center, '1103')
        self.assertEquals(summ.summary_date, "2005-07-28 00:00:00")
        self.assertEquals(summ.client_code, "VTG")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 4)
        self.assertEquals(len(summ.data), 4)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "052090393")

    def test_WestVirginiaAEP(self):
        tl = ticketloader.TicketLoader("../testdata/fwp4-audit.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['WestVirginiaAEP'])

        self.assertEquals(summ.call_center, 'FWP4')
        self.assertEquals(summ.summary_date, "2005-12-27 00:00:00")
        self.assertEquals(summ.client_code, 'WVMISS')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 66)
        self.assertEquals(len(summ.data), 66)

        t = summ.data[0]
        self.assertEquals(t[0], "00005")
        self.assertEquals(t[1], "3600003")
        self.assertEquals(t[2], "07:43")
        self.assertEquals(t[3], "00")

    def test_TX6003(self):
        tl = ticketloader.TicketLoader("../testdata/6003-audit.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['TX6003'])

        self.assertEquals(summ.call_center, "6003")
        self.assertEquals(summ.summary_date, "2005-12-29 00:00:00")
        self.assertEquals(summ.client_code, 'ALTEL01')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 39)
        self.assertEquals(len(summ.data), 39)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "36350210")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "ROUT")

    def test_Dallas2New(self):
        tl = ticketloader.TicketLoader("../testdata/fdx2-audit.txt")
        raw = tl.tickets[1]
        summ = self.handler.parse(raw, ['Dallas2New'])

        self.assertEquals(summ.call_center, "FDX2")
        self.assertEquals(summ.summary_date, "2005-12-29 00:00:00")
        self.assertEquals(summ.client_code, 'TRICOT01')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 4)
        self.assertEquals(len(summ.data), 4)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "36350619")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "ROUT")

    def test_PA7501(self):
        # it should still support Pennsylvania
        tl = ticketloader.TicketLoader("../testdata/sum-Pennsylvania.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["PA7501"])
        self.assertEquals(summ.call_center, '7501')
        self.assertEquals(summ.summary_date, "2002-03-13 00:00:00")
        self.assertEquals(summ.client_code, "KP")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 21)
        self.assertEquals(len(summ.data), 21)

        t = summ.data[0]
        self.assertEquals(t[0], '1')
        self.assertEquals(t[1], "0720063")
        self.assertEquals(t[2], "07:23")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "N")

        # but it should also parse the changed (?) format
        tl = ticketloader.TicketLoader("../testdata/sum-PA7501-new.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["PA7501"])

        self.assertEquals(summ.call_center, '7501')
        self.assertEquals(summ.summary_date, "2005-06-20 00:00:00")
        self.assertEquals(summ.client_code, "KD")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 211)
        self.assertEquals(len(summ.data), 211)

        t = summ.data[0]
        self.assertEquals(t[0], '1')
        self.assertEquals(t[1], "1715013")
        self.assertEquals(t[2], "05:41")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "N")

    def test_PA7501_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","7501","7501-2008-12-09-00005.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["PA7501"])
        self.assertEquals(summ.call_center, '7501')
        self.assertEquals(summ.summary_date, '2008-12-08 00:00:00')
        self.assertEquals(summ.client_code, "BP")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 55)
        self.assertEquals(len(summ.data), 55)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '20083435235')
        self.assertEquals(t.ticket_time, '08:44')
        self.assertEquals(t.revision, "")
        self.assertEquals(t.type_code, "E")

    def test_PA7501_2012_02_19(self):
        # we currently only have a sample audit with 0 tickets on it. :(
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "7501-2012-02-19", "TEST-2012-02-07-00019.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["PA7501", "PA7501Meet"])
        self.assertEquals(summ.call_center, '7501')
        self.assertEquals(summ.summary_date, '2012-02-06 00:00:00')
        self.assertEquals(summ.client_code, "XZ")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 0)
        self.assertEquals(len(summ.data), 0)

    def test_Alaska(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Alaska.txt")
        raw = tl.tickets[1]
        summ = self.handler.parse(raw, ["Alaska"])

        self.assertEquals(summ.call_center, "LAK1")
        self.assertEquals(summ.summary_date, "2005-04-22 00:00:00")
        self.assertEquals(summ.client_code, "ID")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 32)
        self.assertEquals(len(summ.data), 32)

        t = summ.data[0]
        self.assertEquals(t[0], "0002")
        self.assertEquals(t[1], "2005170346")

        # try the empty one
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Alaska"])

        self.assertEquals(summ.call_center, "LAK1")
        self.assertEquals(summ.summary_date, "2005-04-23 00:00:00")
        self.assertEquals(summ.client_code, "ID")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 0)
        self.assertEquals(len(summ.data), 0)

    def test_Utah_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Utah_2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Utah'])

        self.assertEquals(summ.call_center, 'LUT1')
        self.assertEquals(summ.summary_date, '2008-04-14 00:00:00')
        self.assertEquals(summ.client_code, "BAJA")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 26)
        self.assertEquals(len(summ.data), 26)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "A81050038")
        self.assertEquals(t[2], "07:29")
        self.assertEquals(t[3], "00A")
        self.assertEquals(t[4], "")

        t = summ.data[4]
        self.assertEquals(t[0], "00005")
        self.assertEquals(t[1], "A81050113")
        self.assertEquals(t[2], "08:21")
        self.assertEquals(t[3], "00A")
        self.assertEquals(t[4], '')

    def test_Utah_2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Utah_2.txt")
        raw = tl.tickets[0]
        raw = raw.replace('*   THIS IS YOUR END OF OF DAY SUMMARY FOR 04/14/08.  THERE WILL BE NO    *',
                          '*   THIS IS YOUR END OF OF DAY SUMMARY FOR            THERE WILL BE NO    *')
        from summaryparsers import SummaryError
        self.assertRaises(SummaryError,self.handler.parse,raw, ['Utah'])

    def test_Nevada(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Nevada.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Nevada'])

        self.assertEquals(summ.call_center, 'LNV1')
        self.assertEquals(summ.summary_date, "2006-01-13 00:00:00")
        self.assertEquals(summ.client_code, 'CHACOM')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 1)
        self.assertEquals(len(summ.data), 1)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "0014832")
        self.assertEquals(t[2], "00:00")

    def test_Idaho(self):
        # Idaho is no longer active, but testing this format is still useful
        # because other formats (like IdahoNew) derive from it.  So we pretend
        # that LID1 still uses it:
        call_centers.cc2format['LID1'].append('Idaho')
        call_centers.format2cc['Idaho'] = 'LID1'

        tl = ticketloader.TicketLoader("../testdata/sum-Idaho.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Idaho'])

        self.assertEquals(summ.call_center, 'LID1')
        self.assertEquals(summ.summary_date, '2006-02-05 00:00:00')
        self.assertEquals(summ.client_code, 'LOCATE2')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 0)
        self.assertEquals(len(summ.data), 0)

    def test_SouthCaliforniaCOX(self):
        tl = ticketloader.TicketLoader("../testdata/sca1-cox-audit.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['SouthCaliforniaCOX'])

        self.assertEquals(summ.call_center, 'SCA5')
        self.assertEquals(summ.summary_date, "2006-02-01 00:00:00")
        self.assertEquals(summ.client_code, "COX")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 250)
        self.assertEquals(len(summ.data), 250)

        t = summ.data[0]
        self.assertEquals(t[0], '1')
        self.assertEquals(t[1], 'A0320213')

        # try "empty" audit

        raw = tl.tickets[1]
        summ = self.handler.parse(raw, ['SouthCaliforniaCOX'])

        self.assertEquals(summ.call_center, 'SCA5')
        self.assertEquals(summ.summary_date, "2006-05-13 00:00:00")
        self.assertEquals(summ.client_code, "COX")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 0)
        self.assertEquals(len(summ.data), 0)

        # try another audit
        tl = ticketloader.TicketLoader("../testdata/sca1-cox-audit-2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['SouthCaliforniaCOX'])

        self.assertEquals(summ.call_center, 'SCA5')
        self.assertEquals(summ.summary_date, "2007-02-09 00:00:00")
        self.assertEquals(summ.client_code, "COX")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 110)
        self.assertEquals(len(summ.data), 110)

        t = summ.data[0]
        self.assertEquals(t[0], '1')
        self.assertEquals(t[1], 'A70400012')

    def test_Greenville3(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Greenville3.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Greenville3'])

        self.assertEquals(summ.call_center, 'FGV3')
        self.assertEquals(summ.summary_date, "2006-04-12 00:00:00")
        self.assertEquals(summ.client_code, "DPCZ02")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 326)
        self.assertEquals(len(summ.data), 326)

        t = summ.data[0]
        self.assertEquals(t[0], '0001')
        self.assertEquals(t[1], "0604120024")
        self.assertEquals(t[2], "00:00")

    def test_IdahoNew(self):
        tl = ticketloader.TicketLoader("../testdata/sum-IdahoNew.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['IdahoNew'])

        self.assertEquals(summ.call_center, 'LID1')
        self.assertEquals(summ.summary_date, "2006-04-03 00:00:00")
        self.assertEquals(summ.client_code, "IDPOCL00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 19)
        self.assertEquals(len(summ.data), 19)

        t = summ.data[0]
        self.assertEquals(t[0], '0002')
        self.assertEquals(t[1], "2004051121")
        self.assertEquals(t[2], "00:00")

    # OBSOLETE?
    def __test_WashingtonState2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-WashingtonState2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['WashingtonState2'])

        self.assertEquals(summ.call_center, 'LWA2')
        self.assertEquals(summ.summary_date, "2006-04-19 00:00:00")
        self.assertEquals(summ.client_code, "CHARTR02")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 8)
        self.assertEquals(len(summ.data), 8)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "6114655")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[4], "EMER")

    def test_OregonFalcon(self):
        tl = ticketloader.TicketLoader("../testdata/sum-OregonFalcon.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["OregonFalcon"])

        self.assertEquals(summ.call_center, 'LOR2')
        self.assertEquals(summ.summary_date, "2006-05-10 00:00:00")
        self.assertEquals(summ.client_code, "TEST03")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 8)
        self.assertEquals(len(summ.data), 8)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "6093150")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[4], '48HR')

    def test_KentuckyNew(self):
        tl = ticketloader.TicketLoader("../testdata/sum-KentuckyNew.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['KentuckyNew'])

        self.assertEquals(summ.summary_date, "2006-07-11 00:00:00") # ?
        self.assertEquals(summ.client_code, "BELLTN")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 6)
        self.assertEquals(len(summ.data), 6)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "0607110006")
        self.assertEquals(t[2], "11:28")

    def test_NorthCalifornia2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-NorthCalifornia2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['NorthCalifornia2'])

        self.assertEquals(summ.call_center, 'LCA1')
        self.assertEquals(summ.summary_date, "2006-07-14 00:00:00")
        self.assertEquals(summ.client_code, "CHACRE")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 3)
        self.assertEquals(len(summ.data), 3)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "0250230")


    def test_Jacksonville2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Jacksonville2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Jacksonville2'])

        self.assertEquals(summ.call_center, 'FEF2')
        self.assertEquals(summ.summary_date, "2006-06-02 00:00:00")
        self.assertEquals(summ.client_code, "AC1110")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 135)
        self.assertEquals(len(summ.data), 135)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "150609831")
        self.assertEquals(t[2], "09:55")

    def test_Northwest_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Northwest.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Northwest'])

        self.assertEquals(summ.call_center, 'LNG1')
        self.assertEquals(summ.summary_date, "2006-08-22 00:00:00")
        self.assertEquals(summ.client_code, "LNG")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 11)
        self.assertEquals(len(summ.data), 11)

        t = summ.data[0]
        self.assertEquals(t[1], "6159487")

        raw = raw.replace('08/22/2006','')
        summ = self.handler.parse(raw, ['Northwest'])
        self.assertEquals(date.Date(summ.summary_date).as_tuple()[0:5],date.Date().as_tuple()[0:5])
        raw = tl.tickets[0]
        raw = raw.replace('Total Tickets: 11','Total Tickets: abc')
        summ = self.handler.parse(raw, ['Northwest'])
        self.assertEquals(summ.expected_tickets, 0)

    def test_Northwest_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LNG1","TEST-2009-06-17-00010.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Northwest'])

        self.assertEquals(summ.call_center, 'LNG1')
        self.assertEquals(summ.summary_date, '2009-03-02 00:00:00')
        self.assertEquals(summ.client_code, 'NWN02')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 79)
        self.assertEquals(len(summ.data), 79)

        t = summ.data[0]
        self.assertEquals(t.ticket_number, '9042033')

        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LNG1","TEST-2009-06-17-00011.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Northwest'])

        self.assertEquals(summ.call_center, 'LNG1')
        self.assertEquals(summ.summary_date, '2009-03-02 00:00:00')
        self.assertEquals(summ.client_code, 'NWN01')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 735)
        self.assertEquals(len(summ.data), 735)

        t = summ.data[0]
        self.assertEquals(t.ticket_number, '9035393')

    def test_Northwest_3(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Northwest.txt")
        raw = tl.tickets[0]
        raw = raw.replace('Total Tickets: 11',
                          'Total Tickets:')
        summ = self.handler.parse(raw, ['Northwest'])

        self.assertEquals(summ.expected_tickets, 0)

    def test_Atlanta2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Atlanta2.txt")
        raw = tl.tickets[1]
        summ = self.handler.parse(raw, ['Atlanta2'])

        self.assertEquals(summ.call_center, 'FAU3')
        self.assertEquals(summ.summary_date, "2006-11-13 00:00:00")
        self.assertEquals(summ.client_code, "FAU3")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 107)
        self.assertEquals(len(summ.data), 107)

        t = summ.data[0]
        self.assertEquals(t[1], "11136-500-060")

    def test_Atlanta3(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FAU4","FAU4-2008-10-17-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Atlanta4'])

        self.assertEquals(summ.call_center, 'FAU4')
        self.assertEquals(summ.summary_date, '2008-10-16 00:00:00')
        self.assertEquals(summ.client_code, "ATL01")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 3)
        self.assertEquals(len(summ.data), 3)

        t = summ.data[0]
        self.assertEquals(t[1], '10168-248-012')

    def test_SC1421(self, format='SC1421', call_center='1421'):
        tl = ticketloader.TicketLoader("../testdata/sum-SC1421.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, [format])

        self.assertEquals(summ.call_center, call_center)
        self.assertEquals(summ.client_code, "ZZQ06")
        self.assertEquals(summ.summary_date, "2006-09-21 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 136)
        self.assertEquals(len(summ.data), 136)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "0609211920")

        # test SCEG GAS audit
        raw = tl.tickets[1]
        summ = self.handler.parse(raw, ['SC1421'])

        self.assertEquals(summ.call_center, '1421')
        self.assertEquals(summ.client_code, "SCGZ02")
        self.assertEquals(summ.summary_date, "2007-07-09 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 173)
        self.assertEquals(len(summ.data), 173)

        t = summ.data[0]
        self.assertEquals(t[0], "0001")
        self.assertEquals(t[1], "0707090025")
    def test_SC1425(self):
        self.test_SC1421(format='SC1425', call_center='1425')

    def test_SC1421_2(self, format='SC1421', call_center='1421'):
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets", "1421", "1421-2009-07-07-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, [format])

        self.assertEquals(summ.call_center, call_center)
        self.assertEquals(summ.client_code, 'SCEDZ81')
        self.assertEquals(summ.summary_date, '2009-07-06 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 3)
        self.assertEquals(len(summ.data), 3)

        t = summ.data[0]
        self.assertEquals(t.item_number, "0001")
        self.assertEquals(t.ticket_number, "0907060717")
    def test_SC1425_2(self):
        self.test_SC1421_2(format='SC1425', call_center='1425')

    def test_SC1422(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","1422","SummaryAudit-1422.txt"))
        raw = tl.tickets[0]
        raw = raw.replace('SCEGT01',"SCANA01")
        summ = self.handler.parse(raw, ['SC1422'])

        self.assertEquals(summ.call_center, '1422')
        self.assertEquals(summ.client_code, "PUPS")
        self.assertEquals(summ.summary_date, '2009-04-16 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 50)
        self.assertEquals(len(summ.data), 50)

        t = summ.data[0]
        self.assertEquals(t.item_number, "1")
        self.assertEquals(t.ticket_number, "0903030076")

    def test_SC1422_ATT(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","1422","FGV1-2009-07-07-00002.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['SC1422_ATT'])

        self.assertEquals(summ.call_center, '1422')
        self.assertEquals(summ.client_code, 'ZZB25')
        self.assertEquals(summ.summary_date, '2009-07-06 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 907)
        self.assertEquals(len(summ.data), 907)

        t = summ.data[0]
        self.assertEquals(t.item_number, "0001")
        self.assertEquals(t.ticket_number, "0907060005")

    def test_NewJersey_1(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","NewJersey2","GSU1-2009-06-19-00013.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['NewJersey'])

        self.assertEquals(summ.call_center, 'NewJersey')
        self.assertEquals(summ.client_code, 'CC4')
        self.assertEquals(summ.summary_date, '2009-06-18 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 85)
        self.assertEquals(len(summ.data), 85)

    def test_NewJersey2_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-NewJersey2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['NewJersey2'])

        self.assertEquals(summ.call_center, 'NewJersey2')
        self.assertEquals(summ.client_code, "GP4")
        self.assertEquals(summ.summary_date, "2007-01-03 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 1)
        self.assertEquals(len(summ.data), 1)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0010')
        self.assertEquals(t.ticket_number, "070030021")

    def test_NewJersey2_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets", "NewJersey2", "GSU2-2009-04-20-01038.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['NewJersey2a'])

        self.assertEquals(summ.call_center, 'NewJersey2')
        self.assertEquals(summ.client_code, 'GSUPLS')
        self.assertEquals(summ.summary_date, '2009-04-20 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 1015)
        self.assertEquals(len(summ.data), 1015)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.ticket_number, 'GSUPLS091100001')

    def test_NewJersey2a(self):
        tl = ticketloader.TicketLoader("../testdata/sum-NewJersey2a.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['NewJersey2a'])

        self.assertEquals(summ.call_center, "NewJersey2")
        self.assertEquals(summ.client_code, "GSUPLS")
        self.assertEquals(summ.summary_date, "2007-01-02 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 738)
        self.assertEquals(len(summ.data), 738)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.ticket_number, 'GSUPLS070020001')

    def test_HoustonKorterra(self):
        tl = ticketloader.TicketLoader("../testdata/sum-HoustonKorterra.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["HoustonKorterra"])

        self.assertEquals(summ.call_center, "FHL3")
        self.assertEquals(summ.client_code, "1827")
        self.assertEquals(summ.summary_date, "2007-02-01 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 27)
        self.assertEquals(len(summ.data), 27)

        t = summ.data[0]
        self.assertEquals(t[0], '1')
        self.assertEquals(t[1], "070314792")

    def test_NorthCarolina_2(self):
        # format change, apparently introduced on 2007-01-01.
        tl = ticketloader.TicketLoader("../testdata/sum-NorthCarolina-2.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['NorthCarolina'])

        self.assertEquals(summ.call_center, "FCL1")
        self.assertEquals(summ.client_code, 'BEC06')
        self.assertEquals(summ.summary_date, "2007-02-02 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 30)
        self.assertEquals(len(summ.data), 30)

        t = summ.data[0]
        self.assertEquals(t[0], '00001')
        self.assertEquals(t[1], 'C070330069')

    def test_SouthCalifornia_ATT(self):
        tl = ticketloader.TicketLoader("../testdata/sum-SouthCaliforniaATT.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['SouthCaliforniaATT'])

        self.assertEquals(summ.call_center, 'SCA6')
        self.assertEquals(summ.client_code, 'SCA6')
        self.assertEquals(summ.summary_date, "2007-03-13 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 207)
        self.assertEquals(len(summ.data), 207)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "A70720017")
        self.assertEquals(t[2], "08:30")
        self.assertEquals(t[3], "00A")

        # 2010-05-05:
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "SCA6-2010-05-05", "SCA6-sample.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['SouthCaliforniaATT'])

        self.assertEquals(summ.call_center, 'SCA6')
        self.assertEquals(summ.client_code, 'SCA6')
        self.assertEquals(summ.summary_date, "2010-05-05 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 941)
        self.assertEquals(len(summ.data), 941)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "A01250001")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "00A")



    def test_NorthCaliforniaATT(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sum-NorthCaliforniaATT.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['NorthCaliforniaATT'])

        self.assertEquals(summ.call_center, 'NCA2')
        self.assertEquals(summ.client_code, 'NCA2')
        self.assertEquals(summ.summary_date, "2007-03-29 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 99)
        self.assertEquals(len(summ.data), 99)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "0108517")
        self.assertEquals(t[2], "08:21")
        self.assertEquals(t[3], "")

        # 2010-03-18: format change (time is no longer listed)
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NCA2-2010-03-18", "nca2-2010-03-18-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['NorthCaliforniaATT'])

        self.assertEquals(summ.call_center, 'NCA2')
        self.assertEquals(summ.client_code, 'NCA2')
        self.assertEquals(summ.summary_date, "2010-03-17 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 1157)
        self.assertEquals(len(summ.data), 1157)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "0070713")
        self.assertEquals(t[2], "00:00") # no time listed
        self.assertEquals(t[3], "")


    def test_NewJersey3_1(self):
        tl = ticketloader.TicketLoader("../testdata/sum-NewJersey3.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['NewJersey3'])

        self.assertEquals(summ.call_center, 'NewJersey3')
        self.assertEquals(summ.client_code, 'NJ3')
        self.assertEquals(summ.summary_date, "2007-03-15 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 26)
        self.assertEquals(len(summ.data), 26)

        t = summ.data[0]
        self.assertEquals(t[0], "13")
        self.assertEquals(t[1], "070740071")
        self.assertEquals(t[2], "07:51")

    def test_NewJersey3_2(self):
        tl = ticketloader.TicketLoader("../testdata/sum-NewJersey3.txt")
        raw = tl.tickets[0]
        import csv
        import StringIO
        f = StringIO.StringIO(raw)
        rdr = csv.reader(f)
        new_lines = []
        for line_num,line_list in enumerate(rdr):
            line_list = ['"'+item+'"' for item in line_list]
            if line_num == 0:
                new_lines.append(string.join(line_list,','))
            else:
                new_lines.append(string.join(line_list[0:3],','))
        f.close()
        raw = string.join(new_lines,'\n')
        summ = self.handler.parse(raw, ['NewJersey3'])

        today = date.Date()
        today.resettime()

        self.assertEquals(summ.summary_date, today.isodate())

    def test_BatonRouge3(self):
        tl = ticketloader.TicketLoader("../testdata/sum-BatonRouge3.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['BatonRouge3'])

        self.assertEquals(summ.call_center, 'FBL2')
        self.assertEquals(summ.client_code, 'FBL2')
        self.assertEquals(summ.summary_date, "2007-04-03 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 9)
        self.assertEquals(len(summ.data), 9)

        t = summ.data[0]
        self.assertEquals(t[0], "2")
        self.assertEquals(t[1], "70134038")
        self.assertEquals(t[2], "07:11")

    def test_FL9002(self):
        tl = ticketloader.TicketLoader("../testdata/sum-FL9002-new.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['FL9002'])

        self.assertEquals(summ.call_center, '9002')
        self.assertEquals(summ.client_code, '9002')
        self.assertEquals(summ.summary_date, '2007-03-11 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 45)
        self.assertEquals(len(summ.data), 45)

        t = summ.data[0]
        self.assertEquals(t[0], "3869")
        self.assertEquals(t[1], "068709181")
        self.assertEquals(t[2], "21:45")

    def test_Dallas4(self, format='Dallas4', call_center='FDX4'):
        tl = ticketloader.TicketLoader("../testdata/sum-Dallas4.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, [format])

        self.assertEquals(summ.call_center, call_center)
        self.assertEquals(summ.client_code, "TESS")
        self.assertEquals(summ.summary_date, "2007-09-12 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 2223)
        self.assertEquals(len(summ.data), 2223)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "072550121")

        raw = tl.tickets[0]
        raw = raw.replace('Total Items Sent: 2223','')
        summ = self.handler.parse(raw, [format])
        self.assertEquals(summ.expected_tickets, 0)

        raw = tl.tickets[0]
        raw = raw.replace('since 9/12/2007 9:26 AM (Eastern Standard Time)','')
        from summaryparsers import SummaryError
        self.failUnlessRaises(SummaryError,self.handler.parse,raw, [format])

    def test_TX6004(self):
        self.test_Dallas4(format='TX6004', call_center='6004')

    def test_OregonEM(self):
        tl = ticketloader.TicketLoader("../testdata/sum-OregonEM.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['OregonEM'])

        self.assertEquals(summ.call_center, 'LEM1')
        self.assertEquals(summ.client_code, 'TEST03')
        self.assertEquals(summ.summary_date, '2007-02-23 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 15)
        self.assertEquals(len(summ.data), 15)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "7032795")

        # test Washington One Call audit as well
        raw = tl.tickets[1]
        summ = self.handler.parse(raw, ['OregonEM'])

        self.assertEquals(summ.call_center, 'LEM1')
        self.assertEquals(summ.client_code, 'TEST03')
        self.assertEquals(summ.summary_date, '2007-02-16 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 13)
        self.assertEquals(len(summ.data), 13)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "7043629")


    def test_QWEST(self):
        tl = ticketloader.TicketLoader("../testdata/sum-qwest-1.txt")
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["QWEST_IDL"])

        self.assertEquals(summ.call_center, 'LQW1')
        self.assertEquals(summ.summary_date, "2004-04-30 00:00:00")
        self.assertEquals(summ.client_code, "QWEST")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 87)
        self.assertEquals(len(summ.data), 87)

        t = summ.data[0]
        self.assertEquals(t[0], "00055")
        self.assertEquals(t[1], "IDL2004043000103") # serial number
        self.assertEquals(t[2], "15:20")

        # after (slight) format change

        raw = tl.tickets[1]
        summ = self.handler.parse(raw, ["QWEST_IDL"])

        self.assertEquals(summ.call_center, 'LQW1')
        self.assertEquals(summ.summary_date, "2004-05-04 00:00:00")
        self.assertEquals(summ.client_code, "QWEST")
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(summ._has_footer, 1)
        self.assertEquals(summ.expected_tickets, 1640)
        self.assertEquals(len(summ.data), 1640)

        t = summ.data[0]
        self.assertEquals(t[0], "00060")
        self.assertEquals(t[1], "UULC2004050400030") # serial number
        self.assertEquals(t[2], "06:35")

        # another format change
        raw = tl.tickets[2]
        summ = self.handler.parse(raw, ["QWEST_IDL"])
        self.assertEquals(summ._has_header, 1)
        self.assertEquals(len(summ.data), 66)

        found = 0
        for t in summ.data:
            if t[1] == 'WOC2005050700014':
                found = 1
                break
        self.assert_(found)

    def test_translations(self):
        tl = ticketloader.TicketLoader("../testdata/sum-WashingtonState.txt")
        raw = tl.tickets[0]
        raw = raw.replace("TEST03", "FALCON01")
        summ = self.handler.parse(raw, ['WashingtonState'])

        self.assertEquals(summ.call_center, 'LWA1')
        self.assertEquals(summ.client_code, 'CHRTERWA01')


    def test_LCC1(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","LCC1_audit.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Oregon3'])

        self.assertEquals(summ.call_center, 'LCC1')
        self.assertEquals(summ.client_code, 'LCC1')
        self.assertEquals(summ.summary_date, '2007-12-18 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 24)
        self.assertEquals(len(summ.data), 24)

        t = summ.data[23]
        self.assertEquals(t[0], "24")
        self.assertEquals(t[1], "7445688")
        self.assertEquals(t[4], "CC7700")

    def test_HoustonKorterra(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FTL5","ftl5-2008-10-23-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["TennesseeKorterra"])

        self.assertEquals(summ.call_center, "FTL5")
        self.assertEquals(summ.client_code, 'MARY01')
        self.assertEquals(summ.summary_date, '2008-10-22 00:00:00')
        self.assertEquals(summ.summary_date_end, '2008-10-23 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 5)
        self.assertEquals(len(summ.data), 5)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '2591423')

    def test_SouthCaliforniaSCA2Korterra_1(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","SCA7","TEST-2009-04-15-00034.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["SouthCaliforniaSCA2Korterra"])

        self.assertEquals(summ.call_center, "SCA2")

    def test_NashvilleKorterra_1(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FNV3","FNV3-2009-04-02-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NashvilleKorterra"])

        self.assertEquals(summ.call_center, "FNV3")
        self.assertEquals(summ.client_code, 'U01')
        self.assertEquals(summ.summary_date, '2009-04-01 00:00:00')
        self.assertEquals(summ.summary_date_end, '2009-04-02 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 19)
        self.assertEquals(len(summ.data), 19)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '090910168')

    def test_NorthCarolina2_1(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FCL3","FCL3-2009-05-20-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NorthCarolina2"])

        self.assertEquals(summ.call_center, "FCL3")
        self.assertEquals(summ.client_code, 'NCOC')
        self.assertEquals(summ.summary_date, '2009-05-15 00:00:00')
        self.assertEquals(summ.summary_date_end, '2009-05-16 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 24)
        self.assertEquals(len(summ.data), 24)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '0905150031')

    def test_SC1423_1(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","1423","1423-2009-05-20-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["SC1423"])

        self.assertEquals(summ.call_center, "1423")
        self.assertEquals(summ.client_code, 'CPLZ05')
        self.assertEquals(summ.summary_date, '2009-05-15 00:00:00')
        self.assertEquals(summ.summary_date_end, '2009-05-16 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 24)
        self.assertEquals(len(summ.data), 24)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '0905150031')

    def test_VUPSNewOCC3_3(self):
        '''
        Test that the summary parser omits items not sent to utiliquest
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ3","OCC3-2009-05-30-00002.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["VUPSNewOCC3_2"])

        self.assertEquals(summ.call_center, "OCC3")
        self.assertEquals(summ.client_code, 'WGL904')
        self.assertEquals(summ.summary_date, '2009-05-29 00:00:00')
        self.assertEquals(summ.summary_date_end, '2009-05-30 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 1846)
        self.assertEquals(len(summ.data), 1846)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.ticket_number, 'B914900016')

    def test_LA1411_1(self):
        '''
        Mantis 2314: missing summary detail records and system reporting incomplete audit
        '''
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","Mantis_2314","1411-2009-06-05-00016.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["LA1411"])

        self.assertEquals(summ.call_center, "1411")
        self.assertEquals(summ.client_code, 'ETL01')
        self.assertEquals(summ.summary_date, '2009-06-04 00:00:00')
        self.assertEquals(summ.summary_date_end, '2009-06-05 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 71)
        self.assertEquals(len(summ.data), 71)

        t = summ.data[60]
        self.assertEquals(t.item_number, '16')
        self.assertEquals(t.ticket_number, '90233080')

    def test_Mantis2314(self):
        '''
        Mantis 2314: missing summary detail records and system reporting incomplete audit
        '''
        tickets = [(os.path.join('..','testdata','sample_tickets','Mantis_2314','1411-2009-06-05-00016.txt'), 'LA1411'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','1411-2009-06-05-00017.txt'), 'LA1411'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','1411-2009-06-05-00019.txt'), 'LA1411'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','3002-2009-06-05-00019.txt'), 'GA3002'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','FAU3-2009-06-05-00011.txt'), 'Atlanta2'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','FBL1-2009-06-05-00001.txt'), 'BatonRouge1'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','FBL1-2009-06-05-00025.txt'), 'BatonRouge1'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','fde1-2009-06-05-00011.txt'), 'Delaware1'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','fde3-2009-06-05-00001.txt'), 'Delaware3'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','fde3-2009-06-05-00002.txt'), 'Delaware3'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','fde3-2009-06-05-00003.txt'), 'Delaware3'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','fde3-2009-06-05-00004.txt'), 'Delaware3'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','fde3-2009-06-05-00005.txt'), 'Delaware3'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','fhl1-2009-06-05-00013.txt'), 'Houston1'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','fhl1-2009-06-05-00015.txt'), 'Houston1'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','fmw2-2009-06-04-00863.txt'), 'Washington2'),
                   #(os.path.join('..','testdata','sample_tickets','Mantis_2314','fmw4-2009-06-05-00028.txt'), 'Washington4'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','FNL1-2009-06-05-00005.txt'), 'BatonRouge2'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','FNL1-2009-06-05-00006.txt'), 'BatonRouge2'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','FNL1-2009-06-05-00018.txt'), 'BatonRouge2'),
                   (os.path.join('..','testdata','sample_tickets','Mantis_2314','sca6-2009-06-05-00001.txt'), 'SouthCaliforniaATT')]
        for ticket, format in tickets:
            tl = ticketloader.TicketLoader(ticket)
            raw = tl.getTicket()
            summ = self.handler.parse(raw, [format,])
            self.assertTrue(summ.expected_tickets == len(summ.data))

    def test_Washington4_200912(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FMW4-2009-12-15", "TEST-2009-12-17-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Washington4"])

        self.assertEquals(summ.call_center, "FMW4")
        self.assertEquals(summ.client_code, 'WGL06')
        self.assertEquals(summ.summary_date, '2009-12-16 00:00:00')
        self.assertEquals(summ.summary_date_end, '2009-12-17 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 2) # NOT 850!
        self.assertEquals(len(summ.data), 2)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00834')
        self.assertEquals(t.ticket_number, '9625376')
        self.assertEquals(t.revision, '000')
        self.assertEquals(t.type_code, 'STANDARD')
        self.assertEquals(t.ticket_time, '18:31')

        #
        # try another audit with > 1000 tickets

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FMW4-2009-12-15", "FMW4-2010-04-12-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Washington4"])

        self.assertEquals(summ.call_center, "FMW4")
        self.assertEquals(summ.client_code, 'WGL06')
        self.assertEquals(summ.summary_date, '2010-04-12 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-04-13 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 1242-113)
        self.assertEquals(len(summ.data), 1242-113)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.ticket_number, '10155797')
        self.assertEquals(t.revision, '000')
        self.assertEquals(t.type_code, 'STANDARD')
        self.assertEquals(t.ticket_time, '06:02')

    def test_Atlanta5a_201001(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FAU5-2010-01-13", "FAU5-2010-01-08-00007.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Atlanta5a"])

        self.assertEquals(summ.call_center, "FAU5")
        self.assertEquals(summ.client_code, 'AGL118')
        self.assertEquals(summ.summary_date, '2010-01-07 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-01-08 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 116-4) # 4 cleared
        self.assertEquals(len(summ.data), 116-4)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00089')
        self.assertEquals(t.ticket_number, '01070-243-008')
        self.assertEquals(t.revision, '0')
        self.assertEquals(t.type_code, 'Emergency Notice')
        self.assertEquals(t.ticket_time, '07:24')

    def test_GA3004a_201001(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "3004-2010-01-13", "3004-2010-01-08-00009.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["GA3004a"])

        self.assertEquals(summ.call_center, "3004")
        self.assertEquals(summ.client_code, 'AGL108')
        self.assertEquals(summ.summary_date, '2010-01-07 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-01-08 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 292-14)
        self.assertEquals(len(summ.data), 292-14)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00004')
        self.assertEquals(t.ticket_number, '01030-300-052')
        self.assertEquals(t.revision, '1')
        self.assertEquals(t.type_code, 'Normal Notice')
        self.assertEquals(t.ticket_time, '00:50')

    def test_NewJersey2010(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey-2010-02-15", "TEST-2010-01-14-00003.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey2010"])

        self.assertEquals(summ.call_center, "NewJersey")
        self.assertEquals(summ.client_code, 'ADC')
        self.assertEquals(summ.summary_date, '2010-01-08 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-01-09 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 2)
        self.assertEquals(len(summ.data), 2)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '100040101')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, 'ROUT')
        self.assertEquals(t.ticket_time, '00:00')

    # obsolete as of 2010-06-18
    def __test_NewJersey2010B(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey2-2010-05-28", "GSU2-2010-05-27-00783.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey2010B"])

        self.assertEquals(summ.call_center, "NewJersey2")
        self.assertEquals(summ.client_code, 'GSUPLS')
        self.assertEquals(summ.summary_date, '2010-05-27 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-05-28 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 751)
        self.assertEquals(len(summ.data), 751)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.ticket_number, 'GSUPLS101462241')
        self.assertEquals(t.revision, '00')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:16')

    def test_NewJersey2010B_Korterra(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey2-2010-06-18", "GSU2-2010-06-18-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey2010B"])

        self.assertEquals(summ.call_center, "NewJersey2")
        self.assertEquals(summ.client_code, 'GSU2')
        self.assertEquals(summ.summary_date, '2010-06-17 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-06-18 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 789)
        self.assertEquals(len(summ.data), 789)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '101672203') # 'NJ' is stripped
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

        #
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey2-2010-06-18", "GSU2-2010-06-19-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey2010B"])

        self.assertEquals(summ.call_center, "NewJersey2")
        self.assertEquals(summ.client_code, 'GSU2')
        self.assertEquals(summ.summary_date, '2010-06-19 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-06-20 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 173)
        self.assertEquals(len(summ.data), 173)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '101700008') # 'NJ' is stripped
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_NewJersey4(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey4-2010-03-15", "TEST-2010-02-18-00007.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey4"])

        self.assertEquals(summ.call_center, "NewJersey4")
        self.assertEquals(summ.client_code, 'CBLVSNWNYA')
        self.assertEquals(summ.summary_date, '2010-02-15 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-02-16 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 6)
        self.assertEquals(len(summ.data), 6)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0049')
        self.assertEquals(t.ticket_number, '02150-120-007-00')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, 'E')
        self.assertEquals(t.ticket_time, '00:00')

    def test_NewJersey4_2011_06(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey4-2011-11-06", "GSU4-2011-11-05-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey4"])

        self.assertEquals(summ.call_center, "NewJersey4")
        self.assertEquals(summ.client_code, 'unknown')
        self.assertEquals(summ.summary_date, '2011-11-04 00:00:00')
        self.assertEquals(summ.summary_date_end, '2011-11-05 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 101)
        self.assertEquals(len(summ.data), 101)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0001')
        self.assertEquals(t.ticket_number, '11041-165-004-00')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, 'E')
        self.assertEquals(t.ticket_time, '00:00')

    def test_Atlanta4_and_4a(self):
        """ Mantis #2523 """
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FAU4-2010-03-19", "FAU4-2010-03-19-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['Atlanta4']) # or 4a?

        self.assertEquals(len(summ.data), 681)
        self.assertEquals(summ.client_code, 'UQA01')
        self.assertEquals(summ.summary_date, '2010-03-18 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-03-19 00:00:00')
        self.assertEquals(summ.call_center, "FAU4")
        self.assertEquals(summ.expected_tickets, 681)

    #
    # XML audits

    def test_FairfaxXML(self, format="FairfaxXML", call_center='OCC2'):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "OCC2-2010-06-11", "OCC2-2010-06-15-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, [format])

        self.assertEquals(summ.call_center, call_center)
        self.assertEquals(summ.client_code, "UTIL13")
        self.assertEquals(summ.summary_date, "2010-06-14 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 519)
        self.assertEquals(len(summ.data), 519)

        t = summ.data[0]
        self.assertEquals(t.getdata(), ["1", "B016500001", "14:31", "00B", ""])

    def test_DelawareXML(self):
        self.test_FairfaxXML(format="DelawareXML", call_center="FDE2")

    def test_WashingtonXML(self):
        self.test_FairfaxXML(format="WashingtonXML", call_center="FMW3")

    def test_GA3004_sentri(self, format="GA3004b", call_center="3004"):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "3004-2010-06-29", "TEST-2010-06-29-00322.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, [format])

        self.assertEquals(summ.call_center, call_center)
        self.assertEquals(summ.client_code, 'AGL')
        self.assertEquals(summ.summary_date, '2010-06-29 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-06-30 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 22)
        self.assertEquals(len(summ.data), 22)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.ticket_number, '06290-301-462')
        self.assertEquals(t.revision, '0')
        self.assertEquals(t.type_code, 'Y')
        self.assertEquals(t.ticket_time, '14:13')

        # try another audit, 2010-08-23
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "3004-2010-06-29", "3004-2010-08-24-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, [format])

        self.assertEquals(summ.call_center, call_center)
        self.assertEquals(summ.client_code, 'AGL')
        self.assertEquals(summ.summary_date, '2010-08-23 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-08-24 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 2177)
        self.assertEquals(len(summ.data), 2177)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.ticket_number, '08230-220-057')
        self.assertEquals(t.revision, '0')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_Atlanta5b_sentri(self):
        self.test_GA3004_sentri(format="Atlanta5b", call_center="FAU5")

    def test_TennesseeNC_20100702(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FTL3-2010-07-01", "fct3-2010-06-22-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["TennesseeNC"])

        self.assertEquals(summ.call_center, 'FTL3')
        self.assertEquals(summ.client_code, 'BEC03')
        self.assertEquals(summ.summary_date, '2010-06-21 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-06-22 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 2)
        self.assertEquals(len(summ.data), 2)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.ticket_number, 'C101720273')
        self.assertEquals(t.revision, '00C')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '09:46')

    # FIXME: also test for 1421, etc.
    def _test_GreenvilleNew_20100729(self, call_center='FGV1',
      format='GreenvilleNew'):

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "142x-2010-07-29", "TEST-2010-08-04-00242.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, [format])

        self.assertEquals(summ.call_center, call_center)
        self.assertEquals(summ.client_code, 'ZZQ35')
        self.assertEquals(summ.summary_date, '2010-08-04 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-08-05 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 107)
        self.assertEquals(len(summ.data), 107)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0013')
        self.assertEquals(t.ticket_number, '2208040005')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_GreenvilleNew_20100729(self):
        # for now, assume the new audit is only for FGV1/1421
        formats = [('FGV1', 'GreenvilleNew'),
                   ('1421', 'SC1421'),
                   ('1425', 'SC1425')]
        for call_center, format in formats:
            self._test_GreenvilleNew_20100729(call_center, format)

    def test_MississippiKorterra(self):
        # re: Mantis 2697
        # This format does not strip any digits off the ticket number listed
        # on the audit; superclasses do.

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FJL4-2010-12-15", "FJL4-2010-12-14-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["MississippiKorterra"])

        self.assertEquals(summ.call_center, 'FJL4')
        self.assertEquals(summ.client_code, 'JCKSNU01')
        self.assertEquals(summ.summary_date, '2010-12-13 00:00:00')
        self.assertEquals(summ.summary_date_end, '2010-12-14 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 129)
        self.assertEquals(len(summ.data), 129)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '10121301190002')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_WV1181(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "1181-2011-03-26", "1181-2011-03-26-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["WV1181"])

        self.assertEquals(summ.call_center, '1181')
        self.assertEquals(summ.client_code, 'PE')
        self.assertEquals(summ.summary_date, '2011-03-25 00:00:00')
        self.assertEquals(summ.summary_date_end, '2011-03-26 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 3)
        self.assertEquals(len(summ.data), 3)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0001')
        self.assertEquals(t.ticket_number, '110840131')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_WV1181Alt(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "1181-2011-04-01", "1182-2011-03-30-00002.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["WV1181Alt"])

        self.assertEquals(summ.call_center, '1181')
        self.assertEquals(summ.client_code, '1181') # dummy
        self.assertEquals(summ.summary_date, '2011-03-29 00:00:00')
        self.assertEquals(summ.summary_date_end, '2011-03-30 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 54)
        self.assertEquals(len(summ.data), 54)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '110880023')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_WV1181_Korterra(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "1182-2012-02-25-suffix", "1182-2012-02-22-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["WV1181Korterra"])

        self.assertEquals(summ.call_center, '1181')
        self.assertEquals(summ.client_code, '1181') # dummy
        self.assertEquals(summ.summary_date, '2012-02-21 00:00:00')
        self.assertEquals(summ.summary_date_end, '2012-02-22 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 17)
        self.assertEquals(len(summ.data), 17)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '1205217019')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

        for t in summ.data:
            self.assertFalse(t.ticket_number.endswith(('PE', 'PEB')))

    def test_Baltimore2(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FMB2-2011-04-22", "FMB2-2011-04-22-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Baltimore2"])

        self.assertEquals(summ.call_center, 'FMB2')
        self.assertEquals(summ.client_code, 'FMB2') # dummy
        self.assertEquals(summ.summary_date, '2011-04-21 00:00:00')
        self.assertEquals(summ.summary_date_end, '2011-04-22 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 229)
        self.assertEquals(len(summ.data), 229)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '11197772')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_PensacolaAL(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FPL2-2011-04-29", "FAM1-2011-04-28-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["PensacolaAL"])

        self.assertEquals(summ.call_center, 'FPL2')
        self.assertEquals(summ.client_code, 'BYER04') # dummy
        self.assertEquals(summ.summary_date, '2011-04-27 00:00:00')
        self.assertEquals(summ.summary_date_end, '2011-04-28 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 203)
        self.assertEquals(len(summ.data), 203)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0001')
        self.assertEquals(t.ticket_number, '111170018')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_VUPSNewOCC4(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "OCC4-2011-09-30", "OCC4-2011-09-29-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["VUPSNewOCC4"])

        self.assertEquals(summ.call_center, 'OCC4')
        self.assertEquals(summ.client_code, 'VUPS') # dummy
        self.assertEquals(summ.summary_date, '2011-09-27 00:00:00')
        self.assertEquals(summ.summary_date_end, '2011-09-28 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 3)
        self.assertEquals(len(summ.data), 3)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, 'A127101272')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_Arizona(self):
        # Arizona Blue Stake, Mantis #2891
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "SCA3-2011-11-06", "sca3-2011-11-03-00001.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Arizona"])

        self.assertEquals(summ.call_center, 'SCA3')
        self.assertEquals(summ.client_code, 'FTCCOM06')
        self.assertEquals(summ.summary_date, '2011-11-02 00:00:00')
        self.assertEquals(summ.summary_date_end, '2011-11-03 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 1)
        self.assertEquals(len(summ.data), 1)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '2011110200655')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_SC1421A(self, format='SC1421A', call_center='1421'):
        # new formats for 1421/1422, Mantis #2977
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "1421-2012-03-09", "TEST-2012-03-09-00021.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, [format])

        self.assertEquals(summ.call_center, call_center)
        self.assertEquals(summ.client_code, 'SC811')
        self.assertEquals(summ.summary_date, '2012-03-09 00:00:00')
        self.assertEquals(summ.summary_date_end, '2012-03-10 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 18)
        self.assertEquals(len(summ.data), 18)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0001')
        self.assertEquals(t.ticket_number, '1203090036')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')
    def test_SC1425A(self):
        self.test_SC1421A(format='SC1425A', call_center='1425')

    def test_GA3006(self):
        # new format for 3006, Mantis #3095
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "3006-2012-11-01",
             "3003-2012-10-30-11-25-29-875-MA9E6.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["GA3006"])

        self.assertEquals(summ.call_center, '3006')
        self.assertEquals(summ.client_code, 'ATTGA')
        self.assertEquals(summ.summary_date, '2012-10-25 00:00:00')
        self.assertEquals(summ.summary_date_end, '2012-10-26 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 233)
        self.assertEquals(len(summ.data), 233)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '10252-244-038')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_Nashville2013(self):
        # Mantis #3216
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FNV1-2013-01-08", "FNV1-audit.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["Nashville2013"])

        self.assertEquals(summ.call_center, 'FNV1')
        self.assertEquals(summ.client_code, 'UTILNASH')
        self.assertEquals(summ.summary_date, '2013-01-08 00:00:00')
        self.assertEquals(summ.summary_date_end, '2013-01-09 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 480)
        self.assertEquals(len(summ.data), 480)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0001')
        self.assertEquals(t.ticket_number, '130080004')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_Washington_merged(self):
        # Mantis #3173: test audits that were merged with "good morning"
        # messages.
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
            "audits", "FMW1-2012-11-03-merged",
            "FMB1-2012-10-25-00-12-13-868-B2956.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Washington"])

        self.assertEquals(summ.client_code, "HRCA20")
        self.assertEquals(summ.call_center, "FMW1")
        self.assertEquals(summ.summary_date, "2012-10-24 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 178)
        self.assertEquals(len(summ.data), 178)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "122980014")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "ETKT")

    def test_Oregon4(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
            "audits", "LOR3-2013-03-09",
            "LOR3-2013-03-08-01-06-02-476-ICSC8.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Oregon4"])

        self.assertEquals(summ.client_code, "OUNC")
        self.assertEquals(summ.call_center, "LOR3")
        self.assertEquals(summ.summary_date, "2013-03-06 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 971)
        self.assertEquals(len(summ.data), 971)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "13041896")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "")

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
            "audits", "LOR3-2013-03-09",
            "LOR3-2013-03-09-01-05-29-217-MJLSW.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Oregon4"])

        self.assertEquals(summ.client_code, "OUNC")
        self.assertEquals(summ.call_center, "LOR3")
        self.assertEquals(summ.summary_date, "2013-03-08 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 436)
        self.assertEquals(len(summ.data), 436)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "13042981")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "")

    def test_Oregon5(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
            "audits", "LOR4-2013-03-09",
            "LOR4-2013-03-09-01-06-02-703-5WN7Y.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Oregon5"])

        self.assertEquals(summ.client_code, "OUNC")
        self.assertEquals(summ.call_center, "LOR4")
        self.assertEquals(summ.summary_date, "2013-03-08 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 53)
        self.assertEquals(len(summ.data), 53)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "13043115")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "")

    def test_WashingtonState3(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
            "audits", "LWA3-2013-03-09",
            "LWA3-2013-03-09-01-06-13-312-1D0EB.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["WashingtonState3"])

        self.assertEquals(summ.client_code, "UULC")
        self.assertEquals(summ.call_center, "LWA3")
        self.assertEquals(summ.summary_date, "2013-03-08 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 95)
        self.assertEquals(len(summ.data), 95)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "13051630")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "")

    def test_WashingtonState2(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
            "audits", "LWA2-2013-03-09",
            "LWA2-2013-03-09-01-05-44-953-RTI82.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["WashingtonState2"])

        self.assertEquals(summ.client_code, "UULC")
        self.assertEquals(summ.call_center, "LWA2")
        self.assertEquals(summ.summary_date, "2013-03-08 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 634)
        self.assertEquals(len(summ.data), 634)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "13051609")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "")

    def test_Oregon6(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
            "audits", "LOR5-2013-03-14",
            "LOR5-2013-03-12-01-05-25-030-C3K32.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["Oregon6"])

        self.assertEquals(summ.client_code, "OUNC")
        self.assertEquals(summ.call_center, "LOR5")
        self.assertEquals(summ.summary_date, "2013-03-11 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 791)
        self.assertEquals(len(summ.data), 791)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "13043959")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "")

    def test_WashingtonState4(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
            "audits", "LWA4-2013-03-14",
            "LWA4-2013-03-13-01-06-12-071-9QQWQ.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["WashingtonState4"])

        self.assertEquals(summ.client_code, "UULC")
        self.assertEquals(summ.call_center, "LWA4")
        self.assertEquals(summ.summary_date, "2013-03-12 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 890)
        self.assertEquals(len(summ.data), 890)

        t = summ.data[0]
        self.assertEquals(t[0], "1")
        self.assertEquals(t[1], "13054202")
        self.assertEquals(t[2], "00:00")
        self.assertEquals(t[3], "")
        self.assertEquals(t[4], "")

    def test_SouthCalifornia10(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
            "audits", "SCA10-2013-05-04",
            "SCA1-2013-04-22-22-21-34-007-JI2ZB.txt"))
        raw = tl.getTicket()
        summ = self.handler.parse(raw, ["SouthCalifornia10"])

        self.assertEquals(summ.client_code, "INLEMPUTA")
        self.assertEquals(summ.call_center, "SCA10")
        self.assertEquals(summ.summary_date, "2013-04-22 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 11)
        self.assertEquals(len(summ.data), 11)

        t = summ.data[0]
        self.assertEquals(t[0], "00001")
        self.assertEquals(t[1], "A31120277")
        self.assertEquals(t[2], "08:39")
        self.assertEquals(t[3], "00A")
        self.assertEquals(t[4], "")

    def test_NewJersey4_201306(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey4-2013-06-06",
             "NewJersey4-2013-05-30-00-23-11-269-KVYIY.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey4"])

        self.assertEquals(summ.call_center, "NewJersey4")
        self.assertEquals(summ.client_code, 'unknown')
        self.assertEquals(summ.summary_date, '2013-05-29 00:00:00')
        self.assertEquals(summ.summary_date_end, '2013-05-30 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 86)
        self.assertEquals(len(summ.data), 86)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0040')
        self.assertEquals(t.ticket_number, '05293-134-041-00')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, 'R')
        self.assertEquals(t.ticket_time, '00:00')

        # try another audit (this one has a missing type code for 0136)
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey4-2013-06-06",
             "NewJersey4-2013-06-05-00-11-32-992-WE6YJ.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey4"])

        self.assertEquals(summ.call_center, "NewJersey4")
        self.assertEquals(summ.client_code, 'unknown')
        self.assertEquals(summ.summary_date, '2013-06-04 00:00:00')
        self.assertEquals(summ.summary_date_end, '2013-06-05 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 151)
        self.assertEquals(len(summ.data), 151)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0037')
        self.assertEquals(t.ticket_number, '06043-146-015-00')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, 'R')
        self.assertEquals(t.ticket_time, '00:00')

    def test_AL811(self):
        # new audit format, JIRA #3333
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "FAU2-2013-06-12", 
             "FAU2-2013-06-12-01-07-56-655-E7FKS.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["AL811"])

        self.assertEquals(summ.call_center, 'FAU2')
        self.assertEquals(summ.client_code, 'AL811')
        self.assertEquals(summ.summary_date, '2013-06-11 00:00:00')
        self.assertEquals(summ.summary_date_end, '2013-06-12 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 4)
        self.assertEquals(len(summ.data), 4)

        t = summ.data[0]
        self.assertEquals(t.item_number, '0001')
        self.assertEquals(t.ticket_number, '131620334')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_NewJersey6(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey6-2013-11-01", 
             "NewJersey-2013-10-23-10-27-13-255-A17UG.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey6"])

        self.assertEquals(summ.call_center, 'NewJersey6')
        self.assertEquals(summ.client_code, 'Premier')
        self.assertEquals(summ.summary_date, '2013-10-21 00:00:00')
        self.assertEquals(summ.summary_date_end, '2013-10-22 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 47)
        self.assertEquals(len(summ.data), 47)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00010')
        self.assertEquals(t.ticket_number, '132940408')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

        # test another ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey6-2013-11-01", "ORU_NJ_audit.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey6"])

        self.assertEquals(summ.call_center, 'NewJersey6')
        self.assertEquals(summ.client_code, 'UtiliQ-NJ')
        self.assertEquals(summ.summary_date, '2013-12-12 00:00:00')
        self.assertEquals(summ.summary_date_end, '2013-12-13 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 32)
        self.assertEquals(len(summ.data), 32)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00010')
        self.assertEquals(t.ticket_number, '133460509')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_NewJersey7(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey7-2013-11-01", 
             "NewJersey4-2013-10-23-10-26-42-532-BN72B.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey7"])

        self.assertEquals(summ.call_center, 'NewJersey7')
        self.assertEquals(summ.client_code, 'PremierNY')
        self.assertEquals(summ.summary_date, '2013-10-21 00:00:00')
        self.assertEquals(summ.summary_date_end, '2013-10-22 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 51)
        self.assertEquals(len(summ.data), 51)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00010')
        self.assertEquals(t.ticket_number, '10213-145-029-00')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

        # test another ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey7-2013-11-01", "ORU_NY_audit.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey7"])

        self.assertEquals(summ.call_center, 'NewJersey7')
        self.assertEquals(summ.client_code, 'UtiliQ-NY')
        self.assertEquals(summ.summary_date, '2013-12-12 00:00:00')
        self.assertEquals(summ.summary_date_end, '2013-12-13 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 4)
        self.assertEquals(len(summ.data), 4)

        t = summ.data[0]
        self.assertEquals(t.item_number, '00001')
        self.assertEquals(t.ticket_number, '12123-500-005-00')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, '')
        self.assertEquals(t.ticket_time, '00:00')

    def test_GeorgiaXML(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "GeorgiaXML-2013-11", "Audit.xml.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ['GeorgiaXML'])

        self.assertEquals(summ.call_center, '3003') # ?
        self.assertEquals(summ.client_code, "SCB01")
        self.assertEquals(summ.summary_date, "2013-11-19 00:00:00")
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 6)
        self.assertEquals(len(summ.data), 6)

        t = summ.data[0]
        self.assertEquals(t.getdata(), 
         ["00001", "11193-240-015", "00:00", "", ""])

    def test_NewJersey9(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "audits", "NewJersey9-2014-08", 
             "NewJersey9-20143-12-50-49-492-123OV.txt"))
        raw = tl.tickets[0]
        summ = self.handler.parse(raw, ["NewJersey9"])

        self.assertEquals(summ.call_center, 'NewJersey9')
        self.assertEquals(summ.client_code, 'SCNJ5')
        self.assertEquals(summ.summary_date, '2014-08-12 00:00:00')
        self.assertEquals(summ.summary_date_end, '2014-08-13 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 179)
        self.assertEquals(len(summ.data), 179)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '142232573')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, 'ROUT')
        self.assertEquals(t.ticket_time, '00:00')

    def test_LCC1_Oregon3(self):
        tl = ticketloader.TicketLoader(os.path.join(
          _testprep.TICKET_PATH, "audits", "LCC1-2016-02", 
          "LCC1-2016-02-23-16-55-09-854-NQMMY.txt"))
        summ = self.handler.parse(tl.tickets[0], ["Oregon3"])

        self.assertEquals(summ.call_center, 'LCC1')
        self.assertEquals(summ.client_code, 'LCC1')
        self.assertEquals(summ.summary_date, '2016-02-22 00:00:00')
        self.assertEquals(summ.summary_date_end, '2016-02-23 00:00:00')
        self.assertEquals(summ._has_header, True)
        self.assertEquals(summ._has_footer, True)
        self.assertEquals(summ.expected_tickets, 6)
        self.assertEquals(len(summ.data), 6)

        t = summ.data[0]
        self.assertEquals(t.item_number, '1')
        self.assertEquals(t.ticket_number, '16043767')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, 'CC7711')
        self.assertEquals(t.ticket_time, '00:00')

        t = summ.data[5]
        self.assertEquals(t.item_number, '6')
        self.assertEquals(t.ticket_number, '16044251')
        self.assertEquals(t.revision, '')
        self.assertEquals(t.type_code, 'CC7760')
        self.assertEquals(t.ticket_time, '00:00')

if __name__ == "__main__":
    unittest.main()


