# test_xmlhttpresponder_FNV.py
# 2010-10-22  hn  Moved from test_xmlhttpresponder.py. Refactored, cleaned up.

from __future__ import with_statement
import copy
import os
import site; site.addsitedir('.')
import StringIO
import ticket_db
import unittest
#
import _testprep
import config
import locate
import mail2
import ticketloader
import ticketparser
import xmlhttpresponder
from mockery import *

import test_xmlhttpresponder as testresp

class TestXMLHTTPResponderFNV(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.test_emp_id = _testprep.add_test_employee(self.tdb, "Test Employee")
        # creates a deep copy of the real configuration before each test, to be
        # used by said test without affecting the original configuration
        self.cfg = copy.deepcopy(config.getConfiguration())
        # to use this properly, do this inside tests:
        #   with Mockery(config, 'getConfiguration', lambda: self.cfg): ...

    def _test_FNV1(self, call_center='FNV1', format="TN1391", status='M',
                   text="", log_text="", additional=""):
        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml"%call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        client_code = 'B01'
        # get a 1391 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/1391-1.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, [format])
        t.ticket_format = call_center
        t.locates = [locate.Locate(client_code)]
        t.locates[0].status = status
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id = t.locates[0].locate_id
        client_code = t.locates[0].client_code
        # Change the ticket format to FNV1
        sql = """
         update responder_queue
         set ticket_format = '%s'
         where locate_id = %s
        """ % (call_center, locate_id)
        self.tdb.runsql(sql)

        rows = []

        import xmlhttpresponder_data as xhrd
        xr = testresp.create_mock_responder(rows, verbose=0)
        xr.log.logger.logfile = StringIO.StringIO()
        xr.xmlhttp.responseXML.Text = text
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1
        xr.only_call_center = call_center
        xr.respond_all()
        log = xr.log.logger.logfile.getvalue()
        xr.log.logger.logfile.close()

        self.assertTrue(log_text in log)
        if additional:
            self.assertTrue(additional in log)

        # Verify that the response is interpreted correctly
        sql = "select * from response_log"
        results = self.tdb.runsql_result(sql)
        if text == 'success':
            self.assertEqual(results[0]['success'], '1')
        else:
            self.assertEqual(results[0]['success'], '0')

    def test_FNV1_1(self):
        self._test_FNV1(status="M",
                        text="success",
                        log_text="Response: success")

    def test_FNV1_2(self):
        """
        Test a ticket does not exist response
        """

        self._test_FNV1(status='M',
                        text="ticket does not exist",
                        log_text="Response: ticket does not exist")
        return

    def test_FNV1_3(self):
        """
        Test a dispatch code does not exist response
        """
        self._test_FNV1(status='M',
                        text="dispatch code does not exist",
                        log_text="Response: dispatch code does not exist")
        return

    def test_FNV1_4(self):
        """
        Test an invalid response code response
        """
        self._test_FNV1(status='M',
                        text="invalid response code",
                        log_text="Response: invalid response code")
        return

    def test_FNV1_5(self):
        """
        Test a dispatch code does not appear on ticket response
        """
        self._test_FNV1(
          status='M',
          text="dispatch code does not appear on ticket",
          log_text="Response: dispatch code does not appear on ticket")
        return

    def test_FNV1_6(self):
        """
        Test a dispatch code not authenticated response
        """
        self._test_FNV1(status='M',
                        text="dispatch code not authenticated",
                        log_text="Response: dispatch code not authenticated")
        return

    def test_FNV1_7(self):
        """
        Test a unknown facility Type response
        """
        self._test_FNV1(status='M',
                        text="unknown facility Type",
                        log_text="Response: unknown facility Type")
        return

    def test_FNV1_8(self):
        """
        Test a system error - please resend response
        """
        self._test_FNV1(status='M',
                        text="system error - please resend",
                        log_text="system error - please resend",
                        additional="...Ignored for now")
        return

    def test_FNV1_9(self):
        """
        Test if skips are handled correctly
        """
        call_center = 'FNV1'
        self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
         "%s XMLHTTP block required in config.xml"%call_center
        z = self.cfg.xmlhttpresponders[call_center]
        z['all_versions'] = 1
        # Add test client
        client_to_skip = 'CCC'
        _testprep.add_test_client(self.tdb, client_to_skip, 'FNV1')

        client_code = 'B01'
        # get a 1391 ticket
        import ticketparser, ticketloader, locate
        tl = ticketloader.TicketLoader("../testdata/1391-1.txt")
        raw = tl.tickets[0]
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["TN1391"])
        t.ticket_format = "FNV1"
        status = 'M'
        t.locates = [locate.Locate(client_code),locate.Locate(client_to_skip)]
        t.locates[0].status = status
        t.locates[1].status = status
        # post it
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        locate_id_1 = t.locates[0].locate_id
        locate_id_2 = t.locates[1].locate_id
        # Change the ticket format to FNV1
        sql = """
         update responder_queue
         set ticket_format = 'FNV1'
         where locate_id = %s or locate_id = %s
        """ % (locate_id_1,locate_id_2)
        self.tdb.runsql(sql)

        rows = []

        import xmlhttpresponder_data as xhrd
        xr = testresp.create_mock_responder(rows, verbose=0)
        xr.log.logger.logfile = StringIO.StringIO()
        xr.xmlhttp.responseXML.Text = "success"
        xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
        self.assertEquals(xr.responderdata.all_versions, 1)
        xr.log.lock = 1
        xr.only_call_center = 'FNV1'
        xr.respond_all()
        log = xr.log.logger.logfile.getvalue()
        xr.log.logger.logfile.close()
        self.assertTrue("Response: success" in log)
        self.assertTrue("Skipping: CCC" in log)

    def test_FNV1_10(self):
        """
        Test if status_code_skip are handled correctly
        """
        # Mock the sendmail method
        sent = []
        def mock_sendmail(*args, **kwargs):
            sent.append((args, kwargs))
            return True

        with Mockery(mail2, 'sendmail', mock_sendmail):
             with Mockery(config, 'getConfiguration', lambda: self.cfg):

                call_center = 'FNV1'
                self.assert_(self.cfg.xmlhttpresponders.get(call_center)), \
                 "%s XMLHTTP block required in config.xml"%call_center
                z = self.cfg.xmlhttpresponders[call_center]

                _testprep.add_test_client(self.tdb, 'B01', 'FNV1')
                _testprep.add_test_client(self.tdb, 'B02', 'FNV1')

                # get a 1391 ticket
                import ticketparser, ticketloader, locate
                tl = ticketloader.TicketLoader("../testdata/1391-1.txt")
                raw = tl.tickets[0]
                handler = ticketparser.TicketHandler()
                t = handler.parse(raw, ["TN1391"])
                t.ticket_format = "FNV1"
                status = 'ZZZ'
                t.locates = [locate.Locate('B01'),locate.Locate('B02')]
                t.locates[0].status = status
                t.locates[1].status = status

                # post it
                ticket_id = self.tdb.insertticket(t, whodunit='parser')
                locate_id_1 = t.locates[0].locate_id
                locate_id_2 = t.locates[1].locate_id

                # Change the ticket format to FNV1
                sql = """
                 update responder_queue
                 set ticket_format = 'FNV1'
                 where locate_id = %s""" % (locate_id_1,)
                self.tdb.runsql(sql)

                rows = []

                import xmlhttpresponder_data as xhrd
                xr = testresp.create_mock_responder(rows, verbose=0)

                xr.log.logger.logfile = StringIO.StringIO()

                xr.xmlhttp.responseXML.Text = "success"
                xr.responderdata = xhrd.getresponderdata(self.cfg, call_center, None)
                xr.responderdata.status_code_skip[:] = ['ZZZ',] # No Response

                xr.log.lock = 0
                xr.only_call_center = 'FNV1'
                xr.respond_all()
                log = xr.log.logger.logfile.getvalue()
                xr.log.logger.logfile.close()
                self.assertEquals(log.count('Skipping: ZZZ'),2)
                self.assertEquals(log.count('Removing locate from queue'),2)
                self.assertNotEquals(log.find('--- Warning at'),-1)
                self.assertNotEquals(log.find('ticket number:  042861573 client_code:  B01 work_date:  2004-10-15 14:45:00 status:  ZZZ'),-1)
                self.assertNotEquals(log.find('ticket number:  042861573 client_code:  B02 work_date:  2004-10-15 14:45:00 status:  ZZZ'),-1)
                self.assertNotEquals(log.find('Mail notification sent to'),-1)
                self.assertNotEquals(log.find('0 responses sent'),-1)

                # Verify email
                args, kwargs = sent[0]
                self.assertEquals(args[2],
                  'XMLHTTPResponder warning (Skipped status codes)')
                self.assertNotEqual(args[3].find('ticket number:  042861573 client_code:  B01 work_date:  2004-10-15 14:45:00 status:  ZZZ'), -1)
                self.assertNotEqual(args[3].find('ticket number:  042861573 client_code:  B02 work_date:  2004-10-15 14:45:00 status:  ZZZ'), -1)


if __name__ == "__main__":
    unittest.main()

