# test_datadir.py

import os
import shutil
import time
import unittest
import site; site.addsitedir('.')
#
import _testprep
import datadir

class TestDataDir(unittest.TestCase):

    def test_open(self):
        DIR = "c:/temp/test_datadir"
        try:
            os.makedirs(DIR)
        except:
            pass

        try:
            dd = datadir.DataDir(DIR)
            f = dd.open("log1.txt", "w")
            print >> f, "blah"
            f.close()

            f = dd.open("log1.txt")
            line = f.readline()
            f.close()
            self.assertEquals(line.strip(), "blah")

        finally:
            try:
                shutil.rmtree(DIR)
            except:
                pass

    def test_open_2(self):
        DIR = "c:/temp/test_datadir_2"
        try:
            os.makedirs(DIR)
        except:
            pass

        try:
            dd = datadir.DataDir(DIR)
            f = dd.open("logs/mylog.txt", "w")
            print >> f, "blah"
            f.close()

            f = dd.open("logs/mylog.txt")
            line = f.readline()
            f.close()
            self.assertEquals(line.strip(), "blah")

            a = dd.exists("logs/mylog.txt")
            self.assertEquals(a, True)

            b = dd.exists("logs/yourlog.txt")
            self.assertEquals(b, False)

        finally:
            try:
                shutil.rmtree(DIR)
            except:
                pass

    def test_filenames(self):
        # test absolute and relative filenames
        # we're actually testing whether the relevant os.path functions do
        # what they are supposed to do.
        dd = datadir.DataDir("c:\\temp")
        name1 = dd.get_filename("fred.txt")
        self.assertEquals(name1, "c:\\temp\\fred.txt")
        name2 = dd.get_filename("c:\\foobar\\hank.txt")
        self.assertEquals(name2, "c:\\foobar\\hank.txt")

        if _testprep.RUN_BROKEN_MYSHARE:
            name3 = dd.get_filename("\\myshare\\c$\\doggy.txt")
            self.assertEquals(name3, "\\myshare\\c$\\doggy.txt")

        name4 = dd.get_filename("..\\pmet\\dojo.txt")
        self.assertEquals(name4, "c:\\pmet\\dojo.txt")

    def test_id(self):
        DIR = "c:/temp/test_datadir"
        try:
            os.makedirs(DIR)
        except:
            pass

        try:
            dd = datadir.DataDir(DIR)
            f = dd.open("id.txt", "w")
            print >> f, "BLAHH"
            f.close()

            id = dd.get_id()
            self.assertEquals(id, "BLAHH")

        finally:
            try:
                shutil.rmtree(DIR)
            except:
                pass


if __name__ == "__main__":

    unittest.main()

