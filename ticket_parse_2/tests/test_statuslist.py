# test_statuslist.py

import site; site.addsitedir('.')
import unittest
#
import statuslist
import ticket_db

class TestStatusList(unittest.TestCase):

    def test_001(self):
        tdb = ticket_db.TicketDB()
        sl = statuslist.StatusList(tdb)

        # these are true, at least for now:
        self.assertEquals(sl.must_be_closed('M'), 1)
        self.assertEquals(sl.must_be_closed('O'), 0)



if __name__ == "__main__":

    unittest.main()
