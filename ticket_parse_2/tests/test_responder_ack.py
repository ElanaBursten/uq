# test_responder_ack.py

import os
import unittest
import site

site.addsitedir('.')
#
import config
import responder_ack
import ticket_db


class MockLogger:
    def log(self, *args, **kwargs): pass

    def log_event(self, *args, **kwargs): pass

NCA1_ACK_DIR = '../testdata/responder_ack/NCA1'


class TestResponderAck(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.log = MockLogger()
        self.config = config.getConfiguration()

    def test_get_ack_files(self):
        rack = responder_ack.ResponderAck(self.tdb, self.log, NCA1_ACK_DIR, "", 'NCA1', self.config, [])
        files = rack.get_ack_files()
        self.assertEquals(len(files), 3)

    def test_get_ack_data(self):
        rack = responder_ack.ResponderAck(self.tdb, self.log, NCA1_ACK_DIR, "", 'NCA1', self.config, [])
        filename = os.path.join(NCA1_ACK_DIR, 'sca6-2007-04-09-00001.txt')
        numbers = rack.get_ack_data(filename)
        self.assertEquals(len(numbers), 444)
        self.assertEquals(numbers[0], "19302293")
        self.assertEquals(numbers[-1], "19311678")

if __name__ == "__main__":

    unittest.main()