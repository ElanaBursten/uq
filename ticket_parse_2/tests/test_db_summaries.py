# test_db_summaries.py
# Created: 31 Mar 2002, Hans Nowak
#
# Test working with summaries in the database.

import unittest
import site; site.addsitedir('.')
#
import ticket_db
import ticketparser
import ticketloader
import _testprep

class TestDBSummaries(unittest.TestCase):

    def setUp(self):
        # delete ticket and summary tables
        self.tdb = ticket_db.TicketDB()
        _testprep.deletetickets(self.tdb)
        _testprep.deletesummaries(self.tdb)

        self.handler = ticketparser.TicketHandler()

    def getsummary(self, file, separator, number=1):
        """ Get the N-th ticket/summary from the given file. Counting starts
            at 1. """
        tl = ticketloader.TicketLoader(file, separator)
        for i in range(number):
            raw = tl.getTicket()
        return raw

    """ What do I want to test?

        1. We post a (complete) summary. Retrieve it from the database and
        see if the number of detail records is correct, if they have the
        right time and date, etc.

        2. We post the first part of a summary. Do a check.
        Then post the second part. Do another check.
        Then post the final part. Do another check.

        (I'll use the summaries in testdata/mixed-NewJersey.txt for this.)
    """

    def testCompleteNJSummary(self):
        """ Test complete NJ summary """
        raw = self.getsummary("../testdata/mixed-NewJersey.txt", chr(12), 2)
        summ = self.handler.parse(raw, ["NewJersey"])
        self.assertEquals(summ.call_center, "NewJersey")

        # post this summary
        hid, dids = self.tdb.storesummary(summ)#[0]

        # now, get the new record back and check if everything's OK
        results = self.tdb.getrecords("summary_header",
                  summary_header_id=hid)
        self.assertEquals(len(results), 1)  # and not 0
        row = results[0]
        self.assertEquals(row["client_code"], summ.client_code)
        self.assertEquals(row["expected_tickets"], str(summ.expected_tickets))

        # get the detail records
        details = self.tdb.getrecords("summary_detail", summary_header_id=hid)
        self.assertEquals(len(details), len(summ.data))

        for i in range(len(details)):
            row = details[i]
            data = summ.data[i]
            self.assertEquals(row["ticket_number"], data[1])
            # for more flavor, we should check the time!

    def testMultipartNJSummary(self):
        """ Test multipart NJ summary """
        # get three tickets parts
        parts = [self.getsummary("../testdata/mixed-NewJersey.txt", chr(12), i)
                 for i in (9, 10, 11)]
        summaries = [self.handler.parse(part, ["NewJersey"]) for part in parts]

        # do we all have the correct call center?
        for summ in summaries:
            self.assertEquals(summ.call_center, "NewJersey")

        # are _has_header and _has_footer correct?
        self.assertEquals(summaries[0]._has_header, 1)
        self.assertEquals(summaries[1]._has_header, 0)
        self.assertEquals(summaries[2]._has_header, 0)
        self.assertEquals(summaries[0]._has_footer, 0)
        self.assertEquals(summaries[1]._has_footer, 0)
        self.assertEquals(summaries[2]._has_footer, 1)

        ids = []
        for summ in summaries:
            hid, dids = self.tdb.storesummary(summ)
            ids.append(hid)

        self.assertEquals(len(ids), 3)
        self.assert_(ids[0] == ids[1] == ids[2])

        # the last (third) summary has footer info
        expected_tickets = summaries[2].expected_tickets
        self.assertEquals(expected_tickets, 381)
        total_summaries = len(summaries[0].data) + len(summaries[1].data) + \
                          len(summaries[2].data)
        self.assertTrue(total_summaries >= expected_tickets)

        # get header from database
        results = self.tdb.getrecords("summary_header", summary_header_id=hid)
        self.assertEquals(len(results), 1)
        row = results[0]
        self.assertEquals(row["expected_tickets"], `expected_tickets`)
        # we can test more things here...

        # get count of details here...
        c = self.tdb.count("summary_detail", summary_header_id=hid)
        self.assertTrue(c >= expected_tickets)




if __name__ == "__main__":

    unittest.main()

