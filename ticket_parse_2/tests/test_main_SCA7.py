# test_main_SCA7.py

from StringIO import StringIO
import glob
import os
import site; site.addsitedir('.')
import string
import unittest
#
import _testprep
import config
import main
import ticket_db
import ticketloader
import ticketparser
import tools

from test_main import drop
from test_archiver import create_test_dirs, clear, delete_test_dirs
from mockery_tools import *

class TestSCA7(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

        sql = "select * from call_center where cc_code = 'SCA7'"
        rows = self.tdb.runsql_result(sql)
        if not rows:
            _testprep.add_test_call_center(self.tdb, 'SCA7')
            self.delete_call_center = True
        else:
            self.delete_call_center = False

        sql = "select * from client where oc_code like 'SDG%'"
        rows = self.tdb.runsql_result(sql)
        self.client_orig = {}
        for row in rows:
            self.client_orig[row['client_id']] = row['update_call_center']
        # Set the update_call_center to SCA7
        for row in rows:
            self.tdb.updaterecord('client', 'client_id', row['client_id'],
             update_call_center='SCA7')

    def prepare(self):

        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'incoming': 'test_incoming_SCA7',
          'format': 'SCA7',
          'processed': 'test_processed_SCA7',
          'error': 'test_error_SCA7',
          'attachments': ''}]

        self.TEST_DIRECTORIES = []

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['incoming','processed', 'error']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)
        for client_id, update_call_center in self.client_orig.iteritems():
            self.tdb.updaterecord('client', 'client_id', int(client_id),
             update_call_center=update_call_center)
        if self.delete_call_center:
            _testprep.delete_test_call_center(self.tdb, 'SCA7')

    @with_config_copy
    def test_sca7_1(self):
        """
        Mantis 2131:
        The SCA7 tickets will update SCA1 tickets. For each SDG0x term id
        received, we need to add a corresponding SDG0xG term id. For example,
        if we receive SDG04 on the ticket, the parser should add locates SDG04
        and SDG04G to the ticket for marking. The current SDG rule that add a
        term id only if SDG09 is on the ticket will not be used.
        """
        self.prepare()

        f = open(os.path.join("..", "testdata", "sample_tickets", "SCA7",
            "sca7-2008-10-20-00003.txt"))
        raw = string.join(f.readlines(),'\n')
        f.close()

        drop("SCA7.txt", raw, dir='test_incoming_SCA7')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='SCA7')
        m.log.lock = 1
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(len(t2.locates),8)
        self.assertEquals(t2.locates[6].client_code,'SDG06G')
        self.assertEquals(t2.locates[6].added_by,'RULE')
        self.assertEquals(t2.locates[7].client_code,'SDG09G')
        self.assertEquals(t2.locates[7].added_by,'RULE')

    @with_config_copy
    def test_sca7_2(self):
        self.prepare()
        Oct_20_tickets = tools.safeglob(os.path.join("..", "testdata",
                         "sample_tickets", "SCA7", "sca7-2008-10-20-*.txt"))
        # Drop all of the Oct 20 tickets
        for i,ticket_file in enumerate(Oct_20_tickets):
            f = open(ticket_file)
            raw = string.join(f.readlines(),'\n')
            f.close()
            drop("SCA7_%d.txt" % i, raw, dir='test_incoming_SCA7')

        # Parse the lot
        m = main.Main(verbose=0, call_center='SCA7')
        m.log.lock = 1
        m.run(runonce=1)
        self.assertEquals(len(m.ids),51)

        # Drop the summary ticket
        f = open(os.path.join("..", "testdata", "sample_tickets", "SCA7",
            "sca7-2008-10-21-00001.txt"))
        raw = string.join(f.readlines(),'\n')
        f.close()

        drop("SCA7_summary.txt", raw, dir='test_incoming_SCA7')
        # Parse the summary
        m = main.Main(verbose=0, call_center='SCA7')
        m.log.lock = 1
        m.run(runonce=1)

        # Was summary complete?
        row = self.tdb.getrecords('summary_header', call_center='SCA7')[0]
        self.assertEquals(row['complete'],'1')

    @with_config_copy
    def test_sca7_3(self):
        """
        Mantis 2275:
        ...
        Tickets received in the SCA7:
        -------------------------------
        The new term id's are NCU01, NEU01, BCU01, and CMU01.
        1. We will need to pull the term id from the first line of the ticket
        following "KORTERRA JOB" - below are the first few lines of a ticket.
        2. For these term id's we will need to add a corresponding term id with
        "G" appended to the end like we do now. In the example below we would
        add NCU01 and NCU01G as locates to the ticket.
        """
        self.prepare()

        _testprep.add_test_client(self.tdb, 'NCU01', 'SCA1', active=1,
         update_call_center='SCA7')
        _testprep.add_test_client(self.tdb, 'NCU01G', 'SCA1', active=1,
         update_call_center='SCA7')

        f = open(os.path.join("..", "testdata", "sample_tickets", "SCA7",
            "TEST-2009-04-14-00240.txt"))
        raw = string.join(f.readlines(),'\n')
        f.close()

        drop("SCA7.txt", raw, dir='test_incoming_SCA7')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='SCA7')
        m.log.logger.logfile = StringIO()
        m.log.lock = 1
        m.run(runonce=1)

        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(len(t2.locates),2)
        self.assertEquals(t2.locates[0].client_code, 'NCU01')
        self.assertEquals(t2.locates[0].added_by, 'PARSER')
        self.assertEquals(t2.locates[1].client_code, 'NCU01G')
        self.assertEquals(t2.locates[1].added_by, 'RULE')


if __name__ == "__main__":
    unittest.main()

