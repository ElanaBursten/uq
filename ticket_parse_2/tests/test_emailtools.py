# test_emailtools.py

import unittest
import site; site.addsitedir('.')
#
import config
import emailtools

class TestEmailTools(unittest.TestCase):

    def test_smtpinfo(self):
        cfg = config.getConfiguration()
        smtpinfo = cfg.smtp_accs[0]
        newsi = smtpinfo.clone(port=2500, from_address="john@doe.com")
        self.assertEquals(newsi.port, 2500)
        self.assertEquals(newsi.from_address, "john@doe.com")
        self.assertEquals(smtpinfo.port, 25)

