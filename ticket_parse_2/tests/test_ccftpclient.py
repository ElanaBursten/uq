# test_ccftpserver.py

import os
import sys
import subprocess
import traceback
import unittest
import ftpresponder
import _testprep


class Test_CCFTPClient(unittest.TestCase):
    #Test suite for verifying the behavior of the CCFTPServer class.
    __ServerInfo = {
        'name': 'ABC', # name of the call center
        'login': "testuser",
        'outgoing_dir': "test_ftp",
        'password': "123456789",
        'server': 'localhost',
    }

    ftp_server = None

    def setUp(self):
        whereami = os.path.dirname(os.path.abspath(__file__))
        ftp_path = os.path.join(whereami, "ftp_server.py")
        ftp_dir = os.path.join(whereami, "test_ftp2")

        if not os.path.exists(ftp_dir):
            os.makedirs(ftp_dir)
        _testprep.clear(ftp_dir)

        # note: assumes that python.exe is in path!
        try:
            self.ftp_server = subprocess.Popen([sys.executable, ftp_path, "21", ftp_dir])
        except OSError:
            traceback.print_exc()

    def tearDown(self):
        try:
            self.ftp_server.kill()
        except:
            traceback.print_exc()

    def test_inactivity_timeout(self):
        """Verifies that attempting to use a disconnected server
        will automatically issue a reconnect.
        """

        FTPServer = ftpresponder.CCFTPClient(None, self.__ServerInfo, None)
        FTPServer.do_connect()
        FTPServer.disconnect()

        #Verify that performing server operations now attempts to connect if disconnected
        FTPServer.put("fakeFile.txt", "ABC123")
        FTPServer.disconnect()
        self.assertEqual('ABC123', FTPServer.get("fakefile.txt"))
        self.assertEqual(1, len(FTPServer._getlist()))
        FTPServer.disconnect()
        FTPServer.delete("fakeFile.txt")
        self.assertEqual(0, len(FTPServer._getlist()))


if __name__ == "__main__":

    unittest.main()
