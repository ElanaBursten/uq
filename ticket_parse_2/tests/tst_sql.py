# tst_sql.py

response_ack_sql = """
    select t.ticket_number, l.client_code, rl.call_center, rl.status, rl.response_sent, rl.success,
        rl.reply, rw.reference_number, rw.call_center as rw_call_center
    from response_log rl full join response_ack_waiting rw on rw.response_id = rl.response_id
    left join locate l on l.locate_id = rl.locate_id
    left join ticket t on t.ticket_id = l.ticket_id
    order by t.ticket_number, l.client_code, rl.response_id """