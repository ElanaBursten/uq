# test_businesslogic_screening.py

import inspect
import os
import unittest
#
import businesslogic
import datadir
import ticket_db
import ticketloader
import ticketparser
import tools
import _testprep

class TestBusinessLogicScreening(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()

    # fields we use to mimick a "row" like it is used by the router
    ROW_FIELDS = ["ticket_type", "kind", "ticket_number"]

    def _test_screening(self, callcenter, formats, funcs=[]):
        logic = businesslogic.getbusinesslogic(callcenter, self.tdb, [])
        path = os.path.join(_testprep.TICKET_PATH, "routing", "screening",
               callcenter, "*.txt")

        for fn in tools.safeglob(path):
            tl = ticketloader.TicketLoader(fn)
            raw = tl.tickets[0]
            t = self.handler.parse(raw, formats)
            row = dict((fieldname, getattr(t, fieldname, None))
                       for fieldname in self.ROW_FIELDS)
            screened = logic.screen(row, 0, raw)
            self.assertTrue(screened, "Invalid result for screening: %s" % fn)

            # invalidate the ticket in various ways, and make sure it is no
            # longer screened
            for desc, f in funcs:
                invalid = f(raw)
                t = self.handler.parse(invalid, formats)
                row = dict((fieldname, getattr(t, fieldname, None))
                           for fieldname in self.ROW_FIELDS)
                screened = logic.screen(row, 0, invalid)
                self.assertFalse(screened,
                  "Ticket should not be screened: %s, %s" % (fn, desc))

    #
    # SCA screening

    sca_funcs = [
        ('remark yes', lambda s: s.replace("Re-Mark: N", "Re-Mark: Y")),
        ('no update',  lambda s: s.replace("UPDT", "BOGUS")),
    ]

    def test_SCA1(self):
        self._test_screening("SCA1", ["SouthCalifornia"], self.sca_funcs)

    def test_SCA6(self):
        self._test_screening("SCA6", ["SouthCaliforniaATT"], self.sca_funcs)

