# test_ftpresponder.py
# FIXME: too many copy & paste tests in here...

from __future__ import with_statement
import copy
import datetime
import os
import shutil
import site; site.addsitedir('.')
import string
import sys
import time
import unittest
import xml.etree.ElementTree as ET
import config
import date
import et_tools
import errorhandling2
import ftpresponder
import ftpresponder_data
import locate
import mail2
import ticket_db
import ticketloader
import ticketparser
import tools
import _testprep
from mockery import *
from mockery_tools import with_config_copy, suppress_mail

TESTDIR = "ftpresp"

XML_RESULT = """\
<?xml version="1.0" encoding="UTF-8"?>
<ackcollection>
    <ack completionid="%s;%s">
        <ackCode>0</ackCode>
        <ackText>Success</ackText>
    </ack>
</ackcollection>
"""

class MockFTPResponder(ftpresponder.FTPResponder):
    def __init__(self, *args, **kwargs):
        ftpresponder.FTPResponder.__init__(self, *args, **kwargs)

class MockCCFTPClient(ftpresponder.CCFTPClient):
    def __init__(self, *args, **kwargs):
        ftpresponder.CCFTPClient.__init__(self, *args, **kwargs)
        try:
            os.mkdir(TESTDIR)
        except OSError:
            pass
    def do_connect(self):
        pass
    def disconnect(self):
        pass
    def put(self, filename, data):
        fullname = os.path.join(TESTDIR, filename)
        f = open(fullname, 'w')
        f.write(data)
        f.close()
    def get(self, filename):
        fullname = os.path.join(TESTDIR, filename)
        f = open(fullname, 'r')
        data = f.read()
        f.close()
        return data
    def delete(self, filename):
        fullname = os.path.join(TESTDIR, filename)
        os.remove(fullname)
    def getfilelist(self):
        return os.listdir(TESTDIR)
    def get_file_size(self, filename):
        return 42

class MockClient2(MockCCFTPClient):
    def _getlist(self):
        return [
         "drwxr-xr-x    2 byerba00 ftpuser      312 Jun 29  2006 this.out",
         "drwxr-xr-x    2 byerba00 ftpuser      456 Jun 29  2006 that.out",
         "04-10-07  01:58PM                 3190 byermd00_070410_135529.out",
        ]
    getfilelist = ftpresponder.CCFTPClient.getfilelist
    get_file_size = ftpresponder.CCFTPClient.get_file_size

class MockLogger:
    def log(self, *args, **kwargs): pass
    def log_event(self, *args, **kwargs): pass


# default configuration for a responder.
DEFAULT_CONFIG = {
    'name': 'XYZ', # name of the call center
    'clients': [], # list of term ids
    'file_ext': '',
    'id': '',
    'login': 'XYZ',
    'mappings': [], # dict: {'C': ('1', 'foo'), ...}
    'method': '',
    'outgoing_dir': 'processed',
    'password': 'bogus',
    'send_3hour': 1,
    'send_emergencies': 1,
    'server': 'ftp.example.com',
    'skip': [], # list of term ids
    'status_code_skip': [],
    'temp_dir': '',
    'translations': {},
    'parsed_locates_only': 1,
}

FHL3_SERVER_DICT = {
    'login': 'login', 'password': 'password', 'method': '',
    'server': 'server', 'name': 'FHL3', 'temp_dir': '',
    'file_ext': '', 'exclude_states': [],
    'mappings': {'-P': ('bogus', ''), 'M': ('ispaint', '')},
    'outgoing_dir': '', 'clients': [], 'skip': [],
    'status_code_skip': [], 'translations': {},
    'send_emergencies': 0, 'send_3hour': 0,
    'parsed_locates_only': 1,
}

FDE1_SERVER_DICT = {
    'login': 'login', 'password': 'password', 'method': '',
    'server': 'server', 'name': 'FDE1', 'temp_dir': '',
    'file_ext': '', 'exclude_states': [],
    'mappings': {'-P': ('1', ''), 'M': ('ispaint', '')},
    'outgoing_dir': '', 'clients': [], 'skip': [],
    'status_code_skip': [], 'translations': {},
    'send_emergencies': 0, 'send_3hour': 0,
    'parsed_locates_only': 1,
}

FDE1_SERVER_DICT2 = copy.deepcopy(FDE1_SERVER_DICT)
FDE1_SERVER_DICT2['translations'] = {'aaa': 'aaa-translated',
 'KCBA01': 'KCBA01-translated'}

FHL1_SERVER_DICT = {
    'login': 'login', 'password': 'password', 'method': '',
    'server': 'server', 'name': 'FHL1', 'temp_dir': '',
    'file_ext': '', 'exclude_states': [],
    'mappings': {'-P': ('1', ''), 'M': ('ispaint', '')},
    'outgoing_dir': '', 'clients': [], 'skip': [],
    'status_code_skip': [], 'translations': {},
    'send_emergencies': 0, 'send_3hour': 0,
    'parsed_locates_only': 1,
}

FTL1_SERVER_DICT = {
    'login': 'login', 'password': 'password', 'method': '',
    'server': 'server', 'name': 'FTL1', 'temp_dir': '',
    'file_ext': '', 'exclude_states': [],
    'mappings': {'C':('CLEAR','NO GAS LINES'),
                 'M':('LOCATED','MARKED'),
                 'O':('LOCATE DELAYED',''),
                 'NC':('CANNOT LOCATE',''),
                 'NP':('CLEAR','NO GAS LINES'),
                 'NL':('CLEAR','NO GAS LINES')},
    'outgoing_dir': '', 'clients': ['U07'],
    'skip': ['ALW','B03','CK4','CM','MCI','SBU'],
    'status_code_skip': [], 'translations': {},
    'send_emergencies': 0, 'send_3hour': 0,
    'parsed_locates_only': 1,
}

FJL1_SERVER_DICT = {
    'login': 'login', 'password': 'password', 'method': '',
    'server': 'server', 'name': 'FJL1', 'temp_dir': '',
    'file_ext': '', 'exclude_states': [],
    'mappings': {
        'C':  ('CLEAR', 'NO GAS LINES'),
        'M':  ('LOCATED', 'MARKED'),
        'O':  ('LOCATE DELAYED', ''),
        'NC': ('CANNOT LOCATE', ''),
        'NP': ('CLEAR', 'NO GAS LINES'),
        'NL': ('CLEAR', 'NO GAS LINES')
    },
    'outgoing_dir': '', 'clients': ['MS0189'],
    'skip': ['ALW','B03','CK4','CM','MCI','SBU'],
    'status_code_skip': [], 'translations': {},
    'send_emergencies': 0, 'send_3hour': 0,
    'parsed_locates_only': 1,
}


class TestFTPResponder(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)
        self.test_emp_id = _testprep.add_test_employee(self.tdb, "TestEmployee")
        self.handler = ticketparser.TicketHandler()
        cfg = config.getConfiguration()
        cftp = cfg.ftpresponders # shorthand

        self.conf_copy = copy.deepcopy(cfg)

        # set 'method' attr if it doesn't already exist
        for name, d in cfg.ftpresponders.items():
            try:
                d['method']
            except KeyError:
                d['method'] = ''

        try:
            shutil.rmtree(TESTDIR)
        except:
            pass

        try:
            os.mkdir(TESTDIR)
        except OSError:
            pass

    def tearDown(self):
        # remove test directory when done
        try:
            shutil.rmtree(TESTDIR)
        except:
            pass

    def add_responder_config(self, cfg, call_center, d):
        # add to ftpresponders; will be phased out
        cfg.ftpresponders[call_center] = d
        # add to responderconfigdata; this is the preferred method nowadays,
        # because it allows multiple responders of the same type for the same
        # call center
        cfg.responderconfigdata.add(call_center, 'ftp', d)

    # obsolete -- Mantis #2961
    def __test_FMW1(self):
        serverdict = {
            'login': 'login', 'password': 'password',
            'server': 'server', 'name': 'FMW1', 'temp_dir': '',
            'file_ext': '', 'exclude_states': ['OH'],
            'mappings': {'-P': ('1', '')},
            'outgoing_dir': '', 'clients': [], 'skip': [],
            'status_code_skip': [],
            'translations': {}, 'send_emergencies': 0, 'send_3hour': 0,
            'parsed_locates_only': 1,
            'method': ''}
        self.add_responder_config(self.conf_copy, 'FMW1', serverdict)

        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/Washington-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Washington'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1

            resp.run2(serverdict)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0') # not flagged as success yet

        # pretend that the server writes a result file
        result_file = os.path.splitext(stored_files[0])[0] + ".out"
        data = ['9614167','BGEBC','2','12/13/09','10:12:23','00','Success']
        data = string.join(data, '|') + "\n"
        resp.server.put(result_file, data)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        del resp
        resp2 = MockFTPResponder(dont_send=1, verbose=0)
        resp2.log.lock = 1

        resp2.run2(serverdict)

        # there should still be *one* log record, with success=1
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '1')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 0)

    def test_non_mapped_status(self):
        serverdict = {
            'login': 'login', 'password': 'password', 'method': '',
            'server': 'server', 'name': 'FMW1', 'temp_dir': '',
            'file_ext': '', 'exclude_states': ['OH'],
            'mappings': {'-P': ('1', '')},
            'outgoing_dir': '', 'clients': [], 'skip': [],
            'status_code_skip': [],
            'translations': {}, 'send_emergencies': 0, 'send_3hour': 0,
            'parsed_locates_only': 1,
        }
        self.add_responder_config(self.conf_copy, 'FMW1', serverdict)
        #cftp['FMW1']['mappings']['-P'] = ('1','')
        #cftp['FMW1']['exclude_states'] = ['OH']
        #cfg = self.conf_copy.responderconfigdata.get_responders('FMW1', 'ftp')
        #d = cfg[0][2]
        #d['mappings']['-P'] = ('1', '')
        #d['exclude_states'] = ['OH']

        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/Washington-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Washington'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assertTrue(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        # Change the status
        self.tdb.update('locate', 'locate_id', locate_id, status='ZZZ')
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server

        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1

            from StringIO import StringIO
            resp.log.logger.logfile = StringIO()

            resp.run2(serverdict)
            log = resp.log.logger.logfile.getvalue()
            resp.log.logger.logfile.close()

        self.assertTrue("UnknownStatusError: Unknown status: ZZZ" in log)
        self.assertTrue("Removing locate from queue" in log)
        self.assertTrue("All records skipped -- nothing to send" in log)

    test_non_mapped_status.has_error=1

    def test_FHL3(self):
        self.add_responder_config(self.conf_copy, 'FHL3', FHL3_SERVER_DICT)
        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/HoustonKorterra-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['HoustonKorterra'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        for loc in t.locates:
            loc.status = 'M'
        self.assertEquals(t.ticket_format, 'FHL3')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locate_close_date = date.Date()
        locate_close_date.dec_time(hours=2)
        for locate in t.locates:
            sql = """
             update locate
             set status = 'M', closed=1, closed_by_id=%s, closed_how='test',
             closed_date = '%s'
             where locate_id = %s
            """ % (self.test_emp_id, locate_close_date.isodate(),
                   locate.locate_id,)
            self.tdb.runsql(sql)

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'baz')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        resp = MockFTPResponder(dont_retrieve=1, verbose=0)
        resp.log.lock = 1

        resp.run2(FHL3_SERVER_DICT)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file and the XML
        data = resp.server.get(stored_files[0])
        doc = ET.fromstring(data)

        # check the completion date
        data_date = et_tools.find(doc, 'Completiondtdate').text
        data_time = et_tools.find(doc, 'Completiondttime').text
        year1, month1, day1, hour1, min1, sec1, wday1, yday1, idst1 = \
            time.strptime(string.join([str(data_date), str(data_time[:-4])]),
             "%Y-%m-%d %H:%M:%S")
        d1 = datetime.datetime(year1,month1,day1,hour1,min1,sec1)
        d2 = datetime.datetime(locate_close_date.year,
                               locate_close_date.month,
                               locate_close_date.day,
                               locate_close_date.hours,
                               locate_close_date.minutes,
                               locate_close_date.seconds)
        td = d2-d1
        self.assert_(abs(td.seconds) < 60.0)

        self.assert_(data.count('<ispaint>1</ispaint>'))

        # check if ticket notes are in remarks section
        self.assert_(data.count('<Remarks>foo'))
        self.assert_(data.count('baz</Remarks>'))

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet
        locate_id = logs[0]['locate_id']
        resp_id = logs[0]['response_id']

        # pretend that the server writes a result file
        result_file = os.path.splitext(stored_files[0])[0] + "_ack.xml"
        data = XML_RESULT % (locate_id, resp_id)
        resp.server.put(result_file, data)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        del resp
        resp2 = MockFTPResponder(dont_send=1, verbose=0)
        resp2.log.lock = 1

        resp2.run2(FHL3_SERVER_DICT)

        # there should still be *one* log record, with success=1
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '1')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 0)

    def test_FHL3_locate_notes(self):
        # post a ticket
        self.add_responder_config(self.conf_copy, 'FHL3', FHL3_SERVER_DICT)
        tl = ticketloader.TicketLoader('../testdata/HoustonKorterra-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['HoustonKorterra'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        for loc in t.locates:
            loc.status = '-P'
        self.assertEquals(t.ticket_format, 'FHL3')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # update the locate with a new status so the responder_queue will be populated
        kwargs = {
            'status': 'M',
            'client_id':'3294',
        }
        for loc in t.locates:
            self.tdb.update('locate', 'locate_id', loc.locate_id, **kwargs)

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # Insert notes for each locate
        for loc in locates:
            # insert a locate note
            sql = """
             insert notes
             (foreign_type, foreign_id, entry_date, modified_date, uid, active,
              note)
             values (5, '%s', getdate(), getdate(), %s, 1,
              '< > & A locate note for client %s')
            """ % (loc['locate_id'],uid,loc['client_id'])
            self.tdb.runsql(sql)

        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        resp = MockFTPResponder(dont_retrieve=1, verbose=0)
        resp.log.lock = 1

        # we're using FHL3 so we're getting the right FTPResponderData
        resp.run2(FHL3_SERVER_DICT)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file and the XML
        data = resp.server.get(stored_files[0])
        self.assert_(data.count('<ispaint>1</ispaint>'))

        # check if ticket notes are in remarks section
        self.assert_(data.count('<Remarks>'))
        self.assert_(data.count('</Remarks>'))
        # Verify the note
        doc = ET.fromstring(data)
        sd = et_tools.find(doc, 'Remarks').text
        self.assertTrue(sd.find('< > & A locate note for client') > -1)
        self.assertTrue(sd.find('COP') > -1)

    def test_FHL3_bad_ack(self):
        # post a ticket
        self.add_responder_config(self.conf_copy, 'FHL3', FHL3_SERVER_DICT)
        tl = ticketloader.TicketLoader('../testdata/HoustonKorterra-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['HoustonKorterra'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        for loc in t.locates:
            loc.status = 'M'
        self.assertEquals(t.ticket_format, 'FHL3')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'baz')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        resp = MockFTPResponder(dont_retrieve=1, verbose=0)
        resp.log.lock = 1

        # we're using FHL3 so we're getting the right FTPResponderData
        resp.run2(FHL3_SERVER_DICT)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file and the XML
        data = resp.server.get(stored_files[0])
        self.assert_(data.count('<ispaint>1</ispaint>'))

        # check if ticket notes are in remarks section
        self.assert_(data.count('<Remarks>foo'))
        self.assert_(data.count('baz</Remarks>'))

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet
        locate_id = logs[0]['locate_id']
        resp_id = logs[0]['response_id']

        # pretend that the server writes a result file
        result_file = os.path.splitext(stored_files[0])[0] + "_ack.xml"

        XML_RESULT = """<?xml version="1.0" encoding="UTF-8"?>
        <ackcollection>
                <ack completionid="%s;%s">
                    <ackCode></ackCode>
                    <ackText></ackText>
                </ack>
        </ackcollection>
        """

        data = XML_RESULT % (locate_id, resp_id)
        resp.server.put(result_file, data)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        del resp
        resp2 = MockFTPResponder(dont_send=1, verbose=0)
        resp2.log.lock = 1

        resp2.run2(FHL3_SERVER_DICT)

        # there should still be *one* log record, with success=1
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 1)

    def test_FHL3_long_note(self):
        # post a ticket
        self.add_responder_config(self.conf_copy, 'FHL3', FHL3_SERVER_DICT)
        tl = ticketloader.TicketLoader('../testdata/HoustonKorterra-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['HoustonKorterra'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        for loc in t.locates:
            loc.status = 'M'
        self.assertEquals(t.ticket_format, 'FHL3')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2007-01-01 12:13:14'
        # 251 characters
        long_note = "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "1"
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, long_note)


        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        resp = MockFTPResponder(dont_retrieve=1, verbose=0)
        resp.log.lock = 1

        # we're using FHL3 so we're getting the right FTPResponderData
        resp.run2(FHL3_SERVER_DICT)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file and the XML
        data = resp.server.get(stored_files[0])
        self.assert_(data.count('<ispaint>1</ispaint>'))

        # check if ticket notes are in remarks section
        # Verify the note
        doc = ET.fromstring(data)
        child_data = et_tools.find(doc, 'Remarks').text
        self.assertEquals(str(child_data),long_note[0:239]+"(More ...)\n")

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet
        locate_id = logs[0]['locate_id']
        resp_id = logs[0]['response_id']

        # pretend that the server writes a result file
        result_file = os.path.splitext(stored_files[0])[0] + "_ack.xml"
        data = XML_RESULT % (locate_id, resp_id)
        resp.server.put(result_file, data)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        resp2 = MockFTPResponder(dont_send=1, verbose=0)
        resp2.log.lock = 1

        resp2.run2(FHL3_SERVER_DICT)

        # there should still be *one* log record, with success=1
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '1')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 0)

    def _test_FHL3_with_emergencies(self, send_emergencies):
        # post a ticket
        serverdict = copy.deepcopy(FHL3_SERVER_DICT)
        serverdict['send_emergencies'] = int(send_emergencies)
        self.add_responder_config(self.conf_copy, "FHL3", FHL3_SERVER_DICT)
        tl = ticketloader.TicketLoader('../testdata/HoustonKorterra-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['HoustonKorterra'])
        t.kind = 'EMERGENCY'
        t.ticket_type = 'EMERGENCY'
        for loc in t.locates:
            loc.status = 'M'
        self.assertEquals(t.ticket_format, 'FHL3')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assertTrue(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # what files are stored?
            stored_files = resp.server.getfilelist()
            if send_emergencies:
                self.assertEquals(len(stored_files), 1)
                # inspect this file and the XML
                data = resp.server.get(stored_files[0])
                self.assert_(data.count('<ispaint>1</ispaint>'))
            else:
                self.assertEquals(len(stored_files), 0)

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        if send_emergencies:
            self.assertEquals(len(logs), 1)
            self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet
            locate_id = logs[0]['locate_id']
            resp_id = logs[0]['response_id']

            # pretend that the server writes a result file
            result_file = os.path.splitext(stored_files[0])[0] + "_ack.xml"
            data = XML_RESULT % (locate_id, resp_id)
            resp.server.put(result_file, data)

            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 2)
        else:
            self.assertEquals(len(logs), 0)
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 0)

        # create a new responder
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp2 = MockFTPResponder(dont_send=1, verbose=0)
            resp2.log.lock = 1

            resp2.run2(serverdict)

        # there should still be *one* log record, with success=1
        logs = self.tdb.getrecords('response_log')
        if send_emergencies:
            self.assertEquals(len(logs), 1)
            self.assertEquals(logs[0]['success'], '1')
        else:
            self.assertEquals(len(logs), 0) # record was skipped

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 0)

    def test_FHL3_with_emergencies(self):
        # emergency ticket in queue; send_emergencies flag is 1
        self._test_FHL3_with_emergencies(True)

    def test_FHL3_without_emergencies(self):
        # emergency ticket in queue; send_emergencies flag is 0
        self._test_FHL3_with_emergencies(False)

    def test_get_file_size(self):
        serverdict = {
            'login': 'login', 'password': 'password',
            'server': 'server', 'name': 'FMW1', 'temp_dir': '',
            'file_ext': '', 'exclude_states': ['OH'],
            'mappings': {'-P': ('1', '')},
            'outgoing_dir': '', 'clients': [], 'skip': [],
            'status_code_skip':[],
            'translations': {}, 'send_emergencies': 0, 'send_3hour': 0,
            'parsed_locates_only': 1,
        }
        cfg = config.getConfiguration()
        server = MockClient2(cfg, serverdict, MockLogger())

        self.assertEquals(server.get_file_size("this.out"), 312)
        self.assertEquals(server.get_file_size("byermd00_070410_135529.out"), 3190)

    def test_file_can_be_read(self):
        # start a responder...
        def create_server(self, serverdict):
            self.server = MockClient2(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        resp = MockFTPResponder(dont_send=1, verbose=0)
        resp.log.lock = 1

        serverdict = {
            'login': 'login', 'password': 'password',
            'server': 'server', 'name': 'FMW1', 'temp_dir': '',
            'file_ext': '', 'exclude_states': ['OH'],
            'mappings': {'-P': ('1', '')},
            'outgoing_dir': '', 'clients': [], 'skip': [],
            'status_code_skip': [],
            'translations': {}, 'send_emergencies': 0, 'send_3hour': 0,
            'parsed_locates_only': 1,
        }
        resp.run2(serverdict)
        # will implicitly invoke file_can_be_read() using faked FTP retr lines

    def test_is_traditional_ftp_line(self):
        cfg = config.getConfiguration()
        server = MockClient2(cfg, {}, MockLogger())
        files = server._getlist()
        t = server.is_traditional_ftp_line

        self.assertEquals(t(files[0]), True)
        self.assertEquals(t(files[2]), False)

    def test_FBL1_long_note(self, old=1):
        method = old and '2' or '1'
        serverdict = {
            'login': 'login', 'password': 'password', 'method': method,
            'server': 'server', 'name': 'FBL1', 'temp_dir': '',
            'file_ext': '.la', 'exclude_states': [],
            'mappings': {'M':('C', '')},
            'outgoing_dir': '', 'clients': ['ABC01',], 'skip': [],
            'status_code_skip':[],
            'translations': {}, 'send_emergencies': 0, 'send_3hour': 0,
            'parsed_locates_only': 1,
        }
        self.add_responder_config(self.conf_copy, 'FBL1', serverdict)

        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/BatonRouge1-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['BatonRouge1'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        # Create test client
        _testprep.add_test_client(self.tdb, 'ABC01', 'FBL1')
        # Replace the locate
        t.locates = [locate.Locate(client='ABC01'),]
        t.locates[0].status = 'M'
        self.assertEquals(t.ticket_format, 'FBL1')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2007-01-01 12:13:14'
        # 251 characters
        long_note = "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "12345678901234567890123456789012345678901234567890"+\
                    "1"
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, long_note)

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file
        data = resp.server.get(stored_files[0])
        if old:
            s = "000002050CABC01"
            self.assertEquals(data[:len(s)], s)
            self.assertEquals(data.count('|'), 2)
            self.assertEquals(data.count('*'), 1)
        else:
            s = "000002050|ABC01|C|"
            self.assertEquals(data[:len(s)], s)
            self.assertEquals(data.count('|'), 3)
            self.assertEquals(data.count('*'), 0)

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet
        locate_id = logs[0]['locate_id']
        resp_id = logs[0]['response_id']

        # pretend that the server writes a result file
        result_file = os.path.splitext(stored_files[0])[0] + ".out"
        reply = {
            "ticket_number": t.ticket_number,
            "company": t.locates[0].client_code,
            "status": "C",
            "date": datetime.date.today().isoformat().replace('-',''),
            "time": datetime.datetime.now().time().isoformat()[:8].replace(':',''),
            "result": "00"}
        if old:
            data = "%(ticket_number)s\t%(company)s\t%(status)s\t%(date)s\t%(time)s\t%(result)s\n" % reply
        else:
            data = "%(ticket_number)s|%(company)s|%(status)s|%(date)s|%(time)s|%(result)s||\n" % reply
        resp.server.put(result_file, data)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp2 = MockFTPResponder(dont_send=1, verbose=0)
            resp2.log.lock = 1
            resp2.run2(serverdict)

        # there should still be *one* log record, with success=1
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '1')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 0)

    def test_FBL1_new(self):
        self.test_FBL1_long_note(old=0)

    def test_FBL1_2(self, old=1):
        method = old and '2' or '1'
        serverdict = {
            'login': 'login', 'password': 'password', 'method': method,
            'server': 'server', 'name': 'FBL1', 'temp_dir': '',
            'file_ext': '.la', 'exclude_states': [],
            'mappings': {'M': ('C', '')},
            'outgoing_dir': '', 'clients': ['ABC01',], 'skip': [],
            'status_code_skip':[],
            'translations': {}, 'send_emergencies': 0, 'send_3hour': 0
        }
        self.add_responder_config(self.conf_copy, 'FBL1', serverdict)

        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/BatonRouge1-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['BatonRouge1'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        # Create test client
        _testprep.add_test_client(self.tdb, 'ABC01', 'FBL1')
        # Replace the locate
        t.locates = [locate.Locate(client='ABC01'),]
        t.locates[0].status = 'M'
        self.assertEquals(t.ticket_format, 'FBL1')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2007-01-01 12:13:14'

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file
        data = resp.server.get(stored_files[0])
        if old:
            self.assertEquals(data.count('|'), 2)
            self.assertEquals(data.count('*'), 1)
        else:
            self.assertEquals(data.count('|'), 3)
            self.assertEquals(data.count('*'), 0)

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet
        locate_id = logs[0]['locate_id']
        resp_id = logs[0]['response_id']

        # pretend that the server writes a result file
        result_file = os.path.splitext(stored_files[0])[0] + ".out"
        reply = {
            "ticket_number": t.ticket_number,
            "company": t.locates[0].client_code,
            "status": "C",
            "date": datetime.date.today().isoformat().replace('-',''),
            "time": datetime.datetime.now().time().isoformat()[:8].replace(':',''),
            "result": "01"
        }
        if old:
            data = "%(ticket_number)s\t%(company)s\t%(status)s\t%(date)s\t%(time)s\t%(result)s\n" % reply
        else:
            data = "%(ticket_number)s|%(company)s|%(status)s|%(date)s|%(time)s|%(result)s||\n" % reply

        # Since we write two lines for every response with the second line a
        # comment, the reply will have two lines
        reply = {
            "ticket_number": t.ticket_number,
            "company": t.locates[0].client_code,
            "status": "*",
            "date": datetime.date.today().isoformat().replace('-',''),
            "time": datetime.datetime.now().time().isoformat()[:8].replace(':',''),
            "result": "01"}
        if old:
            data += "%(ticket_number)s\t%(company)s\t%(status)s\t%(date)s\t%(time)s\t%(result)s\n" % reply
        else:
            data += "%(ticket_number)s|%(company)s|%(status)s|%(date)s|%(time)s|%(result)s||\n" % reply

        resp.server.put(result_file, data)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp2 = MockFTPResponder(dont_send=1, verbose=0)
            resp2.log.lock = 1
            resp2.run2(serverdict)

        # there should be *one* log record, with success=0
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 0)

    def test_FBL1_2_new(self):
        self.test_FBL1_2(old=0)


    def test_FMW1_retry(self):
        serverdict = {
            'login': 'login', 'password': 'password',
            'server': 'server', 'name': 'FMW1', 'temp_dir': '',
            'file_ext': '', 'exclude_states': ['OH'],
            'mappings': {'-P': ('1', '')},
            'outgoing_dir': '', 'clients': [], 'skip': [],
            'status_code_skip':[],
            'translations': {}, 'send_emergencies': 0, 'send_3hour': 0
        }
        self.add_responder_config(self.conf_copy, 'FMW1', serverdict)

        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/Washington-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Washington'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'

        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            MockFTPResponder.create_server = create_server
            resp = MockFTPResponder(verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # what files are stored?
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 1)

            # there should be *one* log record, with success = 0, and reply =
            # '(ftp waiting)'
            logs = self.tdb.getrecords('response_log')
            self.assertEquals(len(logs), 1)
            self.assertEquals(logs[0]['success'], '0')
            self.assertEquals(logs[0]['reply'], '(ftp waiting)')

            # pretend that the server writes a result file
            # Error 19:  Operation Timed Out. Recommend Retry.
            ackdata = """\
<?xml version="1.0" encoding="utf-8"?>
<Results>
<Result>
<StateCode>NJ</StateCode>
<TicketNumber>00357291</TicketNumber>
<DistrictCode>TDC01</DistrictCode>
<StatusCode>1</StatusCode>
<ProcessedDateTime>2010-12-16 14:58:21</ProcessedDateTime>
<ResultCode>19</ResultCode>
<ResultMessage>Operation Timed Out. Recommend Retry.</ResultMessage>
</Result>
</Results>"""
            result_file = os.path.splitext(stored_files[0])[0] + ".rxml"
            resp.server.put(result_file, ackdata)

            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 2)
        # create a new responder
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp2 = MockFTPResponder(verbose=0)
            resp2.log.lock = 1
            resp2.run2(serverdict)

        # there should still be *one* log record, with success = 0, and reply
        # changed to '(ftp retry)'
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')
        self.assertEquals(logs[0]['reply'], '(ftp retry)')

        # Since it failed, there should still be a record in the responder_queue
        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 1)

        # create a new responder
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp2 = MockFTPResponder(verbose=0)
            resp2.log.lock = 1
            resp2.run2(serverdict)

        # There should be 2 log records
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 2)
        self.assertEquals(logs[0]['success'], '0')
        self.assertEquals(logs[0]['reply'], '(ftp retry)')
        self.assertEquals(logs[1]['success'], '0')
        self.assertEquals(logs[1]['reply'], '(ftp waiting)')

        # There should still be 1 record in the responder_queue
        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 1)

        # Roll back the response_date to simulate time passing.
        for loc in locates:
            sql = """
                update response_log set response_date = DateAdd(minute, -120, GetDate())
                where locate_id = %s
                """ % (loc['locate_id'])
            self.tdb.runsql(sql)

        # Verify that the locate is responded to now that 2 hours has passed.
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp2 = MockFTPResponder(verbose=0)
            resp2.create_server(serverdict)
            stored_files = resp2.server.getfilelist()
            for file in stored_files:
                resp2.server.delete(file)
            stored_files = resp2.server.getfilelist()
            self.assertEquals(len(stored_files), 0)
            resp2.log.lock = 1
            resp2.run2(serverdict)
            stored_files = resp2.server.getfilelist()
            self.assertEquals(len(stored_files), 1)
            
    test_FMW1_retry.has_error=1

    def test_FDE1_timeout(self):
        self.add_responder_config(self.conf_copy, "FDE1", FDE1_SERVER_DICT)
        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/Delaware1-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Delaware1'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            MockFTPResponder.create_server = create_server
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1

            resp.run2(FDE1_SERVER_DICT)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet
        self.assertEquals(logs[0]['reply'], '(ftp waiting)')  # waiting for ack

        # pretend that the server writes a result file
        # Error 19:  Operation Timed Out.  Recommend Retry.
        ackdata = """\
<?xml version="1.0" encoding="utf-8"?>
<Results>
<Result>
<StateCode>DE</StateCode>
<TicketNumber>01069281</TicketNumber>
<DistrictCode>KCBA01</DistrictCode>
<StatusCode>1</StatusCode>
<ProcessedDateTime>2013-3-13 01:00:21</ProcessedDateTime>
<ResultCode>19</ResultCode>
<ResultMessage>Operation Timed Out. Recommend Retry.</ResultMessage>
</Result>
</Results>"""
        result_file = os.path.splitext(stored_files[0])[0] + ".rxml"
        resp.server.put(result_file, ackdata)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp2 = MockFTPResponder(dont_send=1, verbose=0)
            resp2.log.lock = 1
            resp2.run2(FDE1_SERVER_DICT)

        # there should still be *one* log record, with success=0 - Failure
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')
        self.assertEquals(logs[0]['reply'], '(ftp retry)')  # retry responding

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 1)
        # Since it failed, there should still be a record in the responder_queue

    test_FDE1_timeout.has_error=1

    def test_FDE1_term_translation(self):
        # post a ticket
        self.add_responder_config(self.conf_copy, 'FDE1', FDE1_SERVER_DICT2)
        tl = ticketloader.TicketLoader('../testdata/Delaware1-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Delaware1'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)

        # there should now be 1 record in the queue
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)
        self.assertEquals(responses[0]['client_code'], 'KCBA01')

        # there should be no log records
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 0)
        
        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(FDE1_SERVER_DICT2)

        # there should be 1 file
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)

        # pretend that the server writes a result file
        ackdata = """\
<?xml version="1.0" encoding="utf-8"?>
<Results>
<Result>
<StateCode>DE</StateCode>
<TicketNumber>01069281</TicketNumber>
<DistrictCode>KCBA01-translated</DistrictCode>
<StatusCode>1</StatusCode>
<ProcessedDateTime>2013-3-13 01:00:21</ProcessedDateTime>
<ResultCode>0</ResultCode>
<ResultMessage>Success</ResultMessage>
</Result>
</Results>"""
        result_file = os.path.splitext(stored_files[0])[0] + ".rxml"
        resp.server.put(result_file, ackdata)

        # there should be 2 files
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp2 = MockFTPResponder(dont_send=1, verbose=0)
            resp2.log.lock = 1
            resp2.run2(FDE1_SERVER_DICT2)

        # there should be 1 log record, with success=1
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '1')

        # the queue should be empty
        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 0) 

    test_FDE1_term_translation.has_error=1

    def test_FHL3_timeout(self):
        self.add_responder_config(self.conf_copy, 'FHL3', FHL3_SERVER_DICT)
        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/HoustonKorterra-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['HoustonKorterra'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        for loc in t.locates:
            loc.status = 'M'
        self.assertEquals(t.ticket_format, 'FHL3')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'baz')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        resp = MockFTPResponder(dont_retrieve=1, verbose=0)
        resp.log.lock = 1

        # we're using FHL3 so we're getting the right FTPResponderData
        resp.run2(FHL3_SERVER_DICT)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file and the XML
        data = resp.server.get(stored_files[0])
        self.assert_(data.count('<ispaint>1</ispaint>'))

        # check if ticket notes are in remarks section
        self.assert_(data.count('<Remarks>foo'))
        self.assert_(data.count('baz</Remarks>'))

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet
        locate_id = logs[0]['locate_id']
        resp_id = logs[0]['response_id']

        # pretend that the server writes a result file
        result_file = os.path.splitext(stored_files[0])[0] + "_ack.xml"
        XML_RESULT_2 = """<?xml version="1.0" encoding="UTF-8"?>
        <ackcollection>
          <ack completionid="%s;%s">
            <ackCode>19</ackCode>
            <ackText>Error 19:  Operation Timed Out.  Recommend Retry.</ackText>
          </ack>
        </ackcollection>
        """
        data = XML_RESULT_2 % (locate_id, resp_id)
        resp.server.put(result_file, data)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        resp2 = MockFTPResponder(dont_send=1, verbose=0)
        resp2.log.lock = 1

        resp2.run2(FHL3_SERVER_DICT)

        # there should still be *one* log record, with success=0
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 1)

    def test_FHL3_unknown(self):
        self.add_responder_config(self.conf_copy, 'FHL3', FHL3_SERVER_DICT)
        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/HoustonKorterra-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['HoustonKorterra'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        for loc in t.locates:
            loc.status = 'M'
        self.assertEquals(t.ticket_format, 'FHL3')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'baz')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        resp = MockFTPResponder(dont_retrieve=1, verbose=0)
        resp.log.lock = 1

        resp.run2(FHL3_SERVER_DICT)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file and the XML
        data = resp.server.get(stored_files[0])
        self.assert_(data.count('<ispaint>1</ispaint>'))

        # check if ticket notes are in remarks section
        self.assert_(data.count('<Remarks>foo'))
        self.assert_(data.count('baz</Remarks>'))

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet
        locate_id = logs[0]['locate_id']
        resp_id = logs[0]['response_id']

        # pretend that the server writes a result file
        result_file = os.path.splitext(stored_files[0])[0] + "_ack.xml"
        XML_RESULT_2 = """<?xml version="1.0" encoding="UTF-8"?>
        <ackcollection>
           <ack completionid="%s;%s">
              <ackCode>14</ackCode>
              <ackText>Error 14:  Unknown error.  Recommend Retry.</ackText>
           </ack>
        </ackcollection>
        """
        data = XML_RESULT_2 % (locate_id, resp_id)
        resp.server.put(result_file, data)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        resp2 = MockFTPResponder(dont_send=1, verbose=0)
        resp2.log.lock = 1

        resp2.run2(FHL3_SERVER_DICT)

        # there should still be *one* log record, with success=0
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 1)

    def test_FMB1_timeout(self):
        serverdict = {
            'login': 'login', 'password': 'password',
            'server': 'server', 'name': 'FMB1', 'temp_dir': '',
            'file_ext': '', 'exclude_states': [],
            'mappings': {'M': ('1', '')},
            'outgoing_dir': '', 'clients': [], 'skip': [],
            'status_code_skip': [],
            'translations': {}, 'send_emergencies': 0, 'send_3hour': 0
        }
        self.add_responder_config(self.conf_copy, 'FMB1', serverdict)

        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/Baltimore-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Baltimore'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        for loc in t.locates:
            loc.status = 'M'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0') # not flagged as success yet

        # pretend that the server writes a result file
        result_file = os.path.splitext(stored_files[0])[0] + ".out"
        # Error 19:  Operation Timed Out.  Recommend Retry.
        data = ['dummy', 'dummy', 'dummy', '121503', '120000', '19']
        data = string.join(data, '\t') + "\n"
        resp.server.put(result_file, data)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp2 = MockFTPResponder(dont_send=1, verbose=0)
            resp2.log.lock = 1
            resp2.run2(serverdict)

        # there should still be *one* log record, with success=0 - Failure
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 1)
        # Since it failed, there should still be a record in the responder_queue

    test_FMB1_timeout.has_error=1

    def test_FMB1_unknown(self):
        serverdict = {
            'login': 'login', 'password': 'password',
            'server': 'server', 'name': 'FMB1', 'temp_dir': '',
            'file_ext': '', 'exclude_states': [],
            'mappings': {'M': ('1', '')},
            'outgoing_dir': '', 'clients': [], 'skip': [],
            'status_code_skip':[],
            'translations': {}, 'send_emergencies': 0, 'send_3hour': 0
        }
        self.add_responder_config(self.conf_copy, "FMB1", serverdict)

        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/Baltimore-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Baltimore'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        for loc in t.locates:
            loc.status = 'M'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet

        # pretend that the server writes a result file
        result_file = os.path.splitext(stored_files[0])[0] + ".out"
        # Error 14:  Unknown error.  Recommend Retry.
        data = ['dummy', 'dummy', 'dummy', '121503', '120000', '14']
        data = string.join(data, '\t') + "\n"
        resp.server.put(result_file, data)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp2 = MockFTPResponder(dont_send=1, verbose=0)
            resp2.log.lock = 1
            resp2.run2(serverdict)

        # there should still be *one* log record, with success=0 - Failure
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 1)
        # Since it failed, there should still be a record in the responder_queue

    test_FMB1_unknown.has_error=1

    def test_FHL3_locate_date_1(self):
        self.add_responder_config(self.conf_copy, 'FHL3', FHL3_SERVER_DICT)
        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/HoustonKorterra-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['HoustonKorterra'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        for loc in t.locates:
            loc.status = 'M'
        self.assertEquals(t.ticket_format, 'FHL3')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'baz')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        resp = MockFTPResponder(dont_retrieve=1, verbose=0)
        resp.log.lock = 1

        loc_date_time = date.Date().isodate()
        resp.run2(FHL3_SERVER_DICT)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file and the XML
        data = resp.server.get(stored_files[0])
        doc = ET.fromstring(data)
        completion_date = et_tools.find(doc, 'Completiondtdate').text
        self.assertEquals(str(completion_date),loc_date_time[:10])
        completion_time = et_tools.find(doc, 'Completiondttime').text
        # Check that times are within 10 seconds
        t1 = tools.strptime(completion_time[:-4], "%H:%M:%S")
        t2 = tools.strptime(loc_date_time[11:], "%H:%M:%S")
        td = t1-t2
        self.assertTrue(td.seconds < 10)

    def test_FHL3_locate_date_2(self):
        self.add_responder_config(self.conf_copy, "FHL3", FHL3_SERVER_DICT)
        # post a ticket
        tl = ticketloader.TicketLoader('../testdata/HoustonKorterra-1.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['HoustonKorterra'])
        t.kind = 'NORMAL'
        t.ticket_type = 'SOMETHING'
        self.assertEquals(t.ticket_format, 'FHL3')
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        locate_close_date = date.Date()
        locate_close_date.dec_time(hours=1)
        locate_close_date = locate_close_date.isodate()
        for loc in t.locates:
            sql = """
             update locate
             set status = 'M', closed=1, closed_how='test',closed_date = '%s'
             where locate_id = %s
            """ % (locate_close_date, loc.locate_id,)
            self.tdb.runsql(sql)


        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # insert a locate in the responder queue
        self.tdb.delete('responder_queue')
        locate_id = locates[0]['locate_id']
        self.tdb.insert_responder_queue(locate_id)
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        resp = MockFTPResponder(dont_retrieve=1, verbose=0)
        resp.log.lock = 1

        resp.run2(FHL3_SERVER_DICT)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file and the XML
        data = resp.server.get(stored_files[0])
        doc = ET.fromstring(data)
        completion_date = et_tools.find(doc, 'Completiondtdate').text
        self.assertEquals(str(completion_date),locate_close_date[:10])
        completion_time = et_tools.find(doc, 'Completiondttime').text
        # Only check hour & minute
        self.assertEquals(str(completion_time)[0:6],locate_close_date[11:17])

    def FTL5_Body(self, status):
        self.add_responder_config(self.conf_copy, 'FTL1', FTL1_SERVER_DICT)
        # Create call center
        _testprep.add_test_call_center(self.tdb, 'FTL5')
        # Create test client
        client_id = _testprep.add_test_client(self.tdb, 'U07', 'FTL1',
          update_call_center='FTL5')
        # post a ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "FTL5", "ftl5-2008-10-22-00003.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['TennesseeKorterra'])
        for loc in t.locates:
            loc.status = '-P'
        t.ticket_format = 'FTL1'
        t.source = 'FTL5'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # update the locate with a new status so the responder_queue will be
        # populated
        kwargs = {
            'status': '%s' % (status,),
            'client_id':client_id,
        }
        for loc in t.locates:
            self.tdb.update('locate', 'locate_id', loc.locate_id, **kwargs)

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2007-01-01 12:13:14'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # Insert notes for each locate
        for loc in locates:
            # insert a locate note
            sql = """
             insert notes
             (foreign_type, foreign_id, entry_date, modified_date, uid, active,
              note)
             values (5, '%s', getdate(), getdate(), %s, 1,
                     '< > & A locate note for client %s')
            """ % (loc['locate_id'], uid, loc['client_id'])
            self.tdb.runsql(sql)

        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 7)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(FTL1_SERVER_DICT)
            return resp

    def _test_FTL5(self, status=None, remarks1='< > & A locate note for client',
                   remarks2='U07', reason=None, ismarked=None, isclear=None,
                   actcode=None, job='082520577', operator='UTILIQUEST'):
        # NOTE:
        # if actcode is None, we check that it ISN'T in the response.

        resp = self.FTL5_Body(status)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file and the XML
        data = resp.server.get(stored_files[0])

        # Verify the xml
        doc = ET.fromstring(data)
        sd = et_tools.find(doc, 'Remarks').text
        self.assertTrue(sd.find(remarks1) > -1)
        self.assertTrue(sd.find(remarks2) > -1)
        sd = et_tools.find(doc, 'reasonid').text
        self.assertEquals(sd, reason)
        sd = et_tools.find(doc, 'ismarked1').text
        self.assertEquals(sd, ismarked)
        sd = et_tools.find(doc, 'isclearexcavate1').text
        self.assertEquals(sd, isclear)

        if actcode is None:
            z = list(et_tools.findall(doc, 'actcode1'))
            self.assertEquals(z, [], "actcode1 should not be present")
        else:
            sd = et_tools.find(doc, 'actcode1').text
            self.assertEquals(sd, actcode)

        sd = et_tools.find(doc, 'Jobid').text
        self.assertEquals(sd, job)
        sd = et_tools.find(doc, 'Operatorid').text
        self.assertEquals(sd, operator)

    def test_FTL5_1(self):
        self._test_FTL5(status='C',
                        reason='CLEAR',
                        ismarked='0',
                        isclear='1',
                        actcode='NO GAS LINES')

    def test_FTL5_2(self):
        self._test_FTL5(status='M',
                       reason='LOCATED',
                       ismarked='1',
                       isclear='0',
                       actcode='MARKED')

    def test_FTL5_3(self):
        self._test_FTL5(status='O',
                        reason='LOCATE DELAYED',
                        ismarked='0',
                        isclear='0',
                        actcode=None)

    def test_FTL5_4(self):
        self._test_FTL5(status='NC',
                        reason='CANNOT LOCATE',
                        ismarked='0',
                        isclear='0',
                        actcode=None)

    def test_FTL5_5(self):
        self._test_FTL5(status='NP',
                        reason='CLEAR',
                        ismarked='0',
                        isclear='1',
                        actcode='NO GAS LINES')

    def test_FTL5_6(self):
        self._test_FTL5(status='NL',
                        reason='CLEAR',
                        ismarked='0',
                        isclear='1',
                        actcode='NO GAS LINES')

    @with_config_copy
    def test_FTL_skip_status_codes(self):
        """
        Test if status_code_skip are handled correctly
        """
        self.add_responder_config(self.conf_copy, 'FTL1', FTL1_SERVER_DICT)
        # Mock the sendmail method
        sendmail_log = []
        def mock_sendmail(*args, **kwargs):
            sendmail_log.append((args, kwargs))

        with Mockery(mail2, 'sendmail', mock_sendmail):

            # self.conf_copy is created by with_config_copy decorator
            z = self.conf_copy.responderconfigdata.get_responders('FTL1',
                'ftp')[0]
            z[2]['status_code_skip'] = ['ZZZ']

            # mock the get_status_code_skip_data method
            from responder_config_data import ResponderConfigData
            with Mockery(ResponderConfigData, 'get_status_code_skip_data',
                 mock_returns(['ZZZ'])):

                # Create call center
                _testprep.add_test_call_center(self.tdb, 'FTL5')
                # Create test client
                client_id = _testprep.add_test_client(self.tdb, 'U07', 'FTL1',
                            update_call_center='FTL5')
                # post a ticket
                tl = ticketloader.TicketLoader(os.path.join("../testdata",
                     "sample_tickets", "FTL5", "ftl5-2008-10-22-00003.txt"))
                raw = tl.tickets[0]
                t = self.handler.parse(raw, ['TennesseeKorterra'])
                for loc in t.locates:
                    loc.status = 'ZZZ'
                self.assertEquals(t.ticket_format, 'FTL5')
                t.ticket_format = 'FTL1'
                t.source = 'FTL5'
                ticket_id = self.tdb.insertticket(t, whodunit='parser')

                # update the locate with a new status so the responder_queue
                # will be populated
                kwargs = {
                    'status': 'ZZZ',
                    'client_id':client_id,
                }
                for loc in t.locates:
                    self.tdb.update('locate', 'locate_id', loc.locate_id,
                      **kwargs)

                # add ticket notes; these must show up in <Remarks> section
                emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
                uid = _testprep.add_test_user(self.tdb, emp_id)

                locates = self.tdb.getrecords('locate',
                          ticket_id=ticket_id)
                self.assert_(locates)

                # Insert notes for each locate
                for loc in locates:
                    # insert a locate note
                    sql = """
                     insert notes
                     (foreign_type, foreign_id, entry_date, modified_date, uid,
                      active, note)
                     values (5, '%s', getdate(), getdate(), %s, 1,
                             '< > & A locate note for client %s')
                    """ % (loc['locate_id'],uid,loc['client_id'])
                    self.tdb.runsql(sql)

                responses = self.tdb.getrecords('responder_queue')
                self.assertEquals(len(responses), 7)

                # start a responder...
                def create_server(self, serverdict):
                    self.server = MockCCFTPClient(self.config, serverdict,
                                  MockLogger())
                MockFTPResponder.create_server = create_server
                resp = MockFTPResponder(dont_retrieve=1, verbose=0)

                resp.log.lock = 0
                from StringIO import StringIO
                resp.log.logger.logfile = StringIO()

                serverdict = {'login': 'login', 'password': 'password',
                              'server': 'server', 'name': 'FTL1',
                              'temp_dir': '',
                              'file_ext': '', 'exclude_states': [],
                              'mappings': {'C':('CLEAR','NO GAS LINES'),
                                           'M':('LOCATED','MARKED'),
                                           'O':('LOCATE DELAYED',''),
                                           'NC':('CANNOT LOCATE',''),
                                           'NP':('CLEAR','NO GAS LINES'),
                                           'NL':('CLEAR','NO GAS LINES')},
                              'outgoing_dir': '',
                              'clients': ['U07'],
                              'skip': ['ALW','B03','CK4','CM','MCI','SBU'],
                              'status_code_skip':['ZZZ'],
                              'translations': {}, 'send_emergencies': 0,
                              'send_3hour': 0,
                              }
                resp.run2(serverdict)
                log = resp.log.logger.logfile.getvalue()
                resp.log.logger.logfile.close()

                # what files are stored?
                stored_files = resp.server.getfilelist()
                self.assertEquals(len(stored_files), 0)

            self.assertEquals(log.count('Skipping: ZZZ'),7)
            self.assertEquals(log.count('Removing locate from queue'),7)
            self.assertNotEquals(log.find('--- Warning at'),-1)
            self.assertNotEquals(log.find('ticket number:  082520577 client_code:  ALW work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEquals(log.find('ticket number:  082520577 client_code:  B03 work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEquals(log.find('ticket number:  082520577 client_code:  CK4 work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEquals(log.find('ticket number:  082520577 client_code:  CM work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEquals(log.find('ticket number:  082520577 client_code:  MCI work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEquals(log.find('ticket number:  082520577 client_code:  SBU work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEquals(log.find('ticket number:  082520577 client_code:  U07 work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEquals(log.find('Mail notification sent to'),-1)
            self.assertNotEquals(log.find('All records skipped -- nothing to send'),-1)

            args = sendmail_log[0][0]

            # Verify email
            self.assertEquals(args[2],
              'FTPresponder warning (Skipped status codes)')
            self.assertNotEqual(args[3].find('ticket number:  082520577 client_code:  ALW work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEqual(args[3].find('ticket number:  082520577 client_code:  B03 work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEqual(args[3].find('ticket number:  082520577 client_code:  CK4 work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEqual(args[3].find('ticket number:  082520577 client_code:  CM work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEqual(args[3].find('ticket number:  082520577 client_code:  MCI work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEqual(args[3].find('ticket number:  082520577 client_code:  SBU work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)
            self.assertNotEqual(args[3].find('ticket number:  082520577 client_code:  U07 work_date:  2008-09-11 08:45:00 status:  ZZZ'),-1)

    def FJL4_body(self,status):
        self.add_responder_config(self.conf_copy, 'FJL1', FJL1_SERVER_DICT)
        # Create call center
        _testprep.add_test_call_center(self.tdb,'FJL4')
        _testprep.add_test_call_center(self.tdb,'FJL1')
        # Create test client
        client_id = _testprep.add_test_client(self.tdb, 'MS0189', 'FJL1',
                    update_call_center='FJL4')
        # post a ticket
        tl = ticketloader.TicketLoader(os.path.join("../testdata",
             "sample_tickets","FJL4","TEST-2009-03-31-00030.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['MississippiKorterra'])
        for loc in t.locates:
            loc.status = '-P'
        t.ticket_format = 'FJL1'
        t.source = 'FJL4'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # update the locate with a new status so the responder_queue will be
        # populated
        kwargs = {
            'status': '%s' %(status,),
            'client_id':client_id,
        }
        for loc in t.locates:
            self.tdb.update('locate', 'locate_id', loc.locate_id, **kwargs)

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2009-03-31 09:39:00'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # Insert notes for each locate
        for loc in locates:
            # insert a locate note
            sql = """
             insert notes
             (foreign_type, foreign_id, entry_date, modified_date, uid, active,
              note)
             values (5, '%s', getdate(), getdate(), %s, 1,
                     '< > & A locate note for client %s')
            """ % (loc['locate_id'],uid,loc['client_id'])
            self.tdb.runsql(sql)

        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 7)

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(FJL1_SERVER_DICT)
            return resp

    def _test_FJL4(self, status=None, remarks1='< > & A locate note for client',
                   remarks2='MS0189', ismarked="", isclear="",
                   actcode=None, job='09033106420004', operator='UTILIQUEST'):
        # NOTE:
        # if actcode is None, we check that it ISN'T in the response.

        resp = self.FJL4_body(status)

        # what files are stored?
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        # inspect this file and the XML
        data = resp.server.get(stored_files[0])

        # Verify the xml
        doc = ET.fromstring(data)
        sd = et_tools.find(doc, 'Remarks').text
        self.assertTrue(sd.find(remarks1) > -1)
        self.assertTrue(sd.find(remarks2) > -1)
        sd = et_tools.find(doc, 'ismarked1').text
        self.assertEquals(sd, ismarked)
        sd = et_tools.find(doc, 'isclearexcavate1').text
        self.assertEquals(sd, isclear)

        if actcode is None:
            z = list(et_tools.findall(doc, 'actcode1'))
            self.assertEquals(z, [], "actcode1 should not be present")
        else:
            sd = et_tools.find(doc, 'actcode1').text
            self.assertEquals(sd, actcode)

        sd = et_tools.find(doc, 'Jobid').text
        self.assertEquals(sd, job)
        sd = et_tools.find(doc, 'Operatorid').text
        self.assertEquals(sd, operator)

    def test_FJL4_1(self):
        self._test_FJL4(status='C',
                        ismarked='0',
                        isclear='1',
                        actcode='NO GAS LINES')

    def test_FJL4_2(self):
        self._test_FJL4(status='M',
                        ismarked='1',
                        isclear='0',
                        actcode='MARKED')

    def test_FJL4_3(self):
        self._test_FJL4(status='O',
                        ismarked='0',
                        isclear='0',
                        actcode=None)

    def test_FJL4_4(self):
        self._test_FJL4(status='NC',
                        ismarked='0',
                        isclear='0',
                        actcode=None)

    def test_FJL4_5(self):
        self._test_FJL4(status='NP',
                        ismarked='0',
                        isclear='1',
                        actcode='NO GAS LINES')

    def test_FJL4_6(self):
        self._test_FJL4(status='NL',
                        ismarked='0',
                        isclear='1',
                        actcode='NO GAS LINES')

    # 1421 responder
    def test_1421(self):
        serverdict = {'login': 'login', 'password': 'password',
                      'server': 'server', 'name': '1421', 'temp_dir': '',
                      'file_ext': '', 'exclude_states': [],
                      'mappings': {'C':  ('CLEAR', 'NO GAS LINES'),
                                   'M':  ('LOCATED', 'MARKED'),
                                   'O':  ('LOCATE DELAYED', ''),
                                   'NC': ('CANNOT LOCATE', ''),
                                   'NP': ('CLEAR', 'NO GAS LINES'),
                                   'NL': ('CLEAR', 'NO GAS LINES')},
                      'outgoing_dir': '',
                      'clients': ['CC1421DD'],
                      'skip': ['ALW','B03','CK4','CM','MCI','SBU'],
                      'status_code_skip': [],
                      'translations': {},
                      'send_emergencies': 0,
                      'send_3hour': 0,
                      'parsed_locates_only': 1,
                      }
        self.add_responder_config(self.conf_copy, '1421', serverdict)

        status = 'NL'
        # Create test client
        client_id = _testprep.add_test_client(self.tdb, 'CC1421DD', '1421')

        # post a ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "1421", "1421-2008-12-17-01353.txt"))
        raw = tl.tickets[0]
        # make sure CC1421DD is on the ticket
        raw = raw.replace("SCGZ70", "CC1421DD")
        t = self.handler.parse(raw, ['SC1421'])
        for loc in t.locates:
            loc.status = '-P'
        t.ticket_format = '1421'
        t.source = '1421'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # update the locate with a new status so the responder_queue will be
        # populated
        for loc in t.locates:
            self.tdb.update('locate', 'locate_id', loc.locate_id,
              status=status, client_id=client_id)

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Fred')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2009-03-31 09:39:00'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'foo')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # Insert notes for each locate
        for loc in locates:
            self.tdb.insert_locate_note(loc['locate_id'], uid,
              date.Date().isodate(),
              '< > & A locate note for client %s' % loc['client_id'])

        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 12) # 12 term ids on ticket

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # what files are stored?
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 1)
            # inspect this file and the XML
            data = resp.server.get(stored_files[0])

        # verify XML
        self.assertTrue("<Jobid>0812171145</Jobid>" in data)
        self.assertTrue("<Membercode>CC1421DD</Membercode>" in data)
        self.assertTrue("<ismarked1>0</ismarked1>" in data)
        self.assertTrue("<isclearexcavate1>1</isclearexcavate1>")

    # note: it *looks* like this has much in common with FJL1 and 1422, but in
    # this case we use an update call center, so refactoring out the common
    # parts is harder than it seems. :(
    def test_NewJersey(self, status='NL'):
        # customize configuration
        serverdict = {'login': 'login', 'password': 'password',
                      'server': 'server', 'name': 'NewJersey', 'temp_dir': '',
                      'file_ext': '', 'exclude_states': [],
                      'mappings': {'C':  ('CLEAR', 'NO GAS LINES'),
                                   'M':  ('MARKED', 'MARKED'),
                                   'O':  ('LOCATE DELAYED', ''),
                                   'NC': ('CANNOT LOCATE', ''),
                                   'NP': ('CLEAR', 'NO GAS LINES'),
                                   'NL': ('CLEAR', 'NO GAS LINES')},
                      'outgoing_dir': '',
                      'clients': ['CCNJ2DD'],
                      'skip': ['ALW','B03','CK4','CM','MCI','SBU'],
                      'status_code_skip': [],
                      'translations': {},
                      'send_emergencies': 0,
                      'send_3hour': 0,
                      'parsed_locates_only': 1,
                      }
        self.add_responder_config(self.conf_copy, 'NewJersey', serverdict)

        # Create test client
        self.tdb.delete('client', oc_code='CCNJ2DD')
        client_id = _testprep.add_test_client(self.tdb, 'CCNJ2DD',
                    'NewJersey', update_call_center='NewJersey2')

        # post a ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "NewJersey2", "GSU2-2009-04-20-00968.txt"))
        raw = tl.tickets[0]
        # make sure CCNJ2DD is on the ticket
        raw = raw.replace("GPS", "CCNJ2DD")
        t = self.handler.parse(raw, ['NewJersey2'])
        for loc in t.locates:
            loc.status = '-P'
        t.ticket_format = 'NewJersey'
        t.source = 'NewJersey2'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # update the locate with a new status so the responder_queue will be
        # populated
        for loc in t.locates:
            self.tdb.update('locate', 'locate_id', loc.locate_id,
              status=status, client_id=client_id)

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'Annette')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2009-03-31 09:39:00'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assertTrue(locates)

        # Insert notes for each locate
        for loc in locates:
            self.tdb.insert_locate_note(loc['locate_id'], uid,
              date.Date().isodate(),
              '< > & A locate note for client %s' % loc['client_id'])

        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1) # 1 term id on ticket

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # what files are stored?
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 1)
            # inspect this file and the XML
            data = resp.server.get(stored_files[0])

        # QMAN-3445: extra tag for open locates
        if status == 'O':
            self.assertTrue("<completionlevel>T</completionlevel>" in data)
        else:
            self.assertFalse("<completionlevel>T</completionlevel>" in data)

        # verify XML
        self.assertTrue("<Jobid>091102430</Jobid>" in data)
        self.assertTrue("<Membercode>CCNJ2DD</Membercode>" in data)
        if status == 'NL':
            self.assertTrue("<ismarked1>0</ismarked1>" in data)
            self.assertTrue("<isclearexcavate1>1</isclearexcavate1>" in data)
            self.assertTrue("<reasonid>CLEAR</reasonid>" in data) # M3092
        elif status == 'M':
            self.assertTrue("<ismarked1>1</ismarked1>" in data)
            self.assertTrue("<isclearexcavate1>0</isclearexcavate1>" in data)
            self.assertTrue("<reasonid>MARKED</reasonid>" in data) # M3092

    def test_NewJersey_marked(self):
        self.test_NewJersey(status='M')

    def test_NewJersey_ongoing(self):
        self.test_NewJersey(status='O')

    # XXX in spite of my usual rules, this is a blatant copy & paste from
    # test_1421; at some point this code should be cleaned up, and specific
    # tests should be written that can be customized for several call centers.
    # right now I am too pressed for time to do it, and there are too many
    # different parts to do this trivially, grrr! >=(

    # 2011-12-06: there are now two versions of this, one for the original
    # responder, one for the new Korterra one, which has a different "job id".
    # (Mantis #2893)

    def test_1181(self):
        self._test_1181(dir="1181-2011-03-23",
                        filename="1181-2011-03-23-00003.txt",
                        format="WV1181",
                        term_id="VBV",
                        num_locates=8,
                        job_id="103280009")

    def test_1181_new(self):
        self._test_1181(dir="1181-2011-11-28",
                        filename="1182-2011-11-29-00286-serial.txt",
                        format="WV1181Korterra",
                        term_id="PEB",
                        num_locates=7,
                        job_id="1133300459A") # was: WV1133300459A

    def _test_1181(self, dir, filename, format, term_id, num_locates, job_id):
        serverdict = {'login': 'login', 'password': 'password',
                      'server': 'server', 'name': '1181', 'temp_dir': '',
                      'file_ext': '', 'exclude_states': [],
                      'mappings': {'C':  ('CLEAR/NOT INVOLVED', 'GAS LINES'),
                                   'M':  ('MARKED', 'MARKED'),
                                   'N':  ('NOT MARKED', ''),
                                   'NC': ('NOT MARKED', '')},
                      'outgoing_dir': '',
                      'clients': ['CC1181DD'],
                      'skip': ['ALW','B03','CK4','CM','MCI','SBU'],
                      'status_code_skip': [],
                      'translations': {},
                      'send_emergencies': 0,
                      'send_3hour': 0,
                      'parsed_locates_only': 1,
                      }
        self.add_responder_config(self.conf_copy, '1181', serverdict)

        status = 'C'
        # Create test client
        client_id = _testprep.add_test_client(self.tdb, 'CC1181DD', '1181')

        # post a ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", dir, filename))
        raw = tl.tickets[0]
        # make sure CC1421DD is on the ticket
        raw = raw.replace(term_id, "CC1181DD")
        t = self.handler.parse(raw, [format])
        for loc in t.locates:
            loc.status = '-P'
        t.ticket_format = '1181'
        t.source = '1181'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # update the locate with a new status so the responder_queue will be
        # populated
        for loc in t.locates:
            self.tdb.update('locate', 'locate_id', loc.locate_id,
              status=status, client_id=client_id)

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'John1181')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2011-03-31 09:39:00'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # Insert notes for each locate
        for loc in locates:
            self.tdb.insert_locate_note(loc['locate_id'], uid,
              date.Date().isodate(),
              '< > & A locate note for client %s' % loc['client_id'])

        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), num_locates) # term ids on ticket

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # what files are stored?
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 1)
            # inspect this file and the XML
            data = resp.server.get(stored_files[0])

        # QMAN-3445: 1181 should not have this tag, only NewJersey
        self.assertFalse("<completionlevel>T</completionlevel>" in data)

        # verify XML
        self.assertTrue("<Jobid>" + job_id + "</Jobid>" in data)
        self.assertTrue("<Membercode>CC1181DD</Membercode>" in data)
        self.assertTrue("<ismarked1>0</ismarked1>" in data)
        self.assertTrue("<isclearexcavate1>1</isclearexcavate1>" in data)
        self.assertTrue("<reasonid>CLEAR/NOT INVOLVED</reasonid>" in data)

        # test if XML can be parsed properly
        ET.fromstring(data)

    def test_FMB2(self):
        """ Mantis #2800: FMB2 is an update call center, so we must look for
            responses in the queue that have call center FMB1, but a client
            that falls under FMB2. """
        serverdict = {'login': 'login', 'password': 'password',
                      'server': 'server', 'name': 'FMB2', 'temp_dir': '',
                      'file_ext': '', 'exclude_states': [],
                      'mappings': {'C':  ('CLEAR', 'GAS LINES'),
                                   'M':  ('MARKED', 'MARKED'),
                                   'N':  ('NOT MARKED', ''),
                                   'NC': ('NOT MARKED', '')},
                      'outgoing_dir': '',
                      'clients': ['CCFMB2DD'],
                      'skip': ['ALW','B03','CK4','CM','MCI','SBU'],
                      'status_code_skip': [],
                      'translations': {},
                      'send_emergencies': 0,
                      'send_3hour': 0,
                      'parsed_locates_only': 1,
                      }
        self.add_responder_config(self.conf_copy, 'FMB2', serverdict)

        # make sure call center FMB2 exists, and that we have at least one
        # active client for it
        self.tdb.delete('client', oc_code='CCFMB2DD')
        client_id = _testprep.add_test_client(self.tdb, 'CCFMB2DD', 'FMB1',
                    update_call_center='FMB2')

        # post a ticket
        status = 'C'
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "FMB2-2011-04-22", "FMB2-2011-04-21-00224.txt"))
        raw = tl.tickets[0]
        # make sure CC1421DD is on the ticket
        raw = raw.replace("PTE01", "CCFMB2DD")
        t = self.handler.parse(raw, ['Baltimore2'])
        for loc in t.locates:
            loc.status = '-P'
        t.ticket_format = 'FMB1'
        #t.source = '1181'
        #print "?", t.ticket_format, t.source
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # update the locate with a new status so the responder_queue will be
        # populated
        for loc in t.locates:
            self.tdb.update('locate', 'locate_id', loc.locate_id,
              status=status, client_id=client_id)

        rrows = self.tdb.getrecords("responder_queue")
        self.assertEquals(len(rrows), 1)

        # make sure the special FMB2 query works
        rows = self.tdb.runsql_result("""
         exec get_pending_responses_ucc 'FMB2';
        """)
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['status'], 'C')
        self.assertEquals(rows[0]['ticket_format'], 'FMB1') # !
        self.assertEquals(rows[0]['client_code'], 'CCFMB2DD')

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # what files are stored?
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 1)
            # inspect this file and the XML
            data = resp.server.get(stored_files[0])

        self.assertTrue('<Jobid>11200349</Jobid>' in data, data)

    test_FMB2.has_error=1

    def test_FMW1_parsed_locates_only(self):
        # Mantis #2830
        serverdict = {
            'login': 'login', 'password': 'password',
            'server': 'server', 'name': 'FMW1', 'temp_dir': '',
            'file_ext': '', 'exclude_states': ['OH'],
            'mappings': {'-P': ('1', ''), 'C': ('2', '')},
            'outgoing_dir': '', 'clients': ['CAL01', 'XYZZY'], 'skip': [],
            'status_code_skip': [],
            'translations': {}, 'send_emergencies': 0, 'send_3hour': 0,
            'parsed_locates_only': 1,
            'method': ''}
        self.add_responder_config(self.conf_copy, 'FMW1', serverdict)

        # grab a ticket and post it
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "FMW1-2011-06", "fmw1-2011-05-12-02652.txt"))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ['Washington'])
        self.assertEquals(len(t.locates), 1)

        # manually add a locate that was not added by 'parser'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')
        newloc = locate.Locate('XYZZY')
        newloc.added_by = 'foobar'
        t.locates.append(newloc)
        t.update(self.tdb)

        # check that we have two locates in the db, one added by 'parser', one
        # added by 'foobar'
        locs = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assertEquals(len(locs), 2)
        for loc in locs:
            if loc['client_code'] == 'CAL01':
                self.assertEquals(loc['added_by'], 'parser')
            elif loc['client_code'] == 'XYZZY':
                self.assertEquals(loc['added_by'], 'foobar')

        # assign locates, set status to -R for now
        self.tdb.runsql("""
         update locate
         set assigned_to = 200, status = '-R'
         where ticket_id = %d
        """ % int(ticket_id))

        # set status to 'C' so responder queue will be populated
        self.tdb.runsql("""
         update locate
         set status = 'C'
         where ticket_id = %d
        """ % int(ticket_id))

        locs = self.tdb.getrecords('locate', ticket_id=ticket_id)

        # check that there are two locates in the responder queue
        rqrows = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(rqrows), 2)
        self.assertEquals(sorted([x['client_code'] for x in rqrows]),
                          ['CAL01', 'XYZZY'])

        # get_pending_responses, by default, excludes the locate that was not
        # added by 'parser'
        rq1 = self.tdb.runsql_result("""
         exec get_pending_responses 'FMW1'
        """)
        self.assertEquals(len(rq1), 1)

        # ...but with additional parameter 0, it does include it
        rq0 = self.tdb.runsql_result("""
         exec get_pending_responses 'FMW1', @ParsedLocatesOnly = 0
        """)
        self.assertEquals(len(rq0), 2)

        # fire up a responder
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # only client CAL01 should be processed
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 1)

        # change the configuration
        serverdict['parsed_locates_only'] = 0
        time.sleep(2)
        # force different filename by using different timestamp; kludgy :(

        # run the responder again with updated configuration
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # now it processes client XYZZY as well (CAL01 has already been
            # removed from the responder queue at this point); there should be
            # 2 files now
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 2)

    test_FMW1_parsed_locates_only.has_error=1

    def test_NewJersey_NJOC(self):
        # Mantis #2882.
        serverdict = {'login': 'login', 'password': 'password',
                      'server': 'server', 'name': 'NewJersey', 'temp_dir': '',
                      'file_ext': '', 'exclude_states': [],
                      'mappings': {'C':  ('1', ''),
                                   'M':  ('2', ''),
                                   'N':  ('0', ''),
                                   'NC': ('0', '')},
                      'outgoing_dir': '',
                      'clients': ['CCNEWJDD'],
                      'skip': ['ALW','B03','CK4','CM','MCI','SBU'],
                      'status_code_skip': [],
                      'translations': {},
                      'send_emergencies': 0,
                      'send_3hour': 0,
                      'parsed_locates_only': 1,
                      'kind': 'njoc',
                      }
        self.add_responder_config(self.conf_copy, 'NewJersey', serverdict)

        status = 'C'
        # Create test client
        client_id = _testprep.add_test_client(self.tdb, 'CCNEWJDD', 'NewJersey')

        # post a ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "NewJersey-2011-07-expiration",
             "GSU1-2011-06-30-01826.txt"))
        raw = tl.tickets[0]
        # make sure CCNEWJDD is on the ticket
        raw = raw.replace("GSC", "CCNEWJDD")
        t = self.handler.parse(raw, ['NewJersey2010'])
        for loc in t.locates:
            loc.status = '-P'
        t.ticket_format = 'NewJersey'
        t.source = 'NewJersey'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # update the locate with a new status so the responder_queue will be
        # populated
        for loc in t.locates:
            self.tdb.update('locate', 'locate_id', loc.locate_id,
              status=status, client_id=client_id)

        # add ticket notes; these must show up in <Remarks> section
        emp_id = _testprep.add_test_employee(self.tdb, 'JohnNJ')
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2011-03-31 09:39:00'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # Insert notes for each locate
        for loc in locates:
            self.tdb.insert_locate_note(loc['locate_id'], uid,
              date.Date().isodate(),
              '< > & A locate note for client %s' % loc['client_id'])

        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1) # 1 term id on ticket

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # what files are stored?
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 1)
            # inspect this file and the XML
            data = resp.server.get(stored_files[0])

        # pick NewFTPResponderData! [because id = 'njoc']
        self.assertTrue("<TicketNumber>111811531</TicketNumber>" in data)
        self.assertTrue("<DistrictCode>CCNEWJDD</DistrictCode>" in data)

        # test if XML can be parsed properly
        ET.fromstring(data)

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0')  # not flagged as success yet
        locate_id = logs[0]['locate_id']
        resp_id = logs[0]['response_id']

        z = self.tdb.getrecords('response_ack_context')
        self.assertEquals(len(z), 1) # there should be one record

        # pretend that the server writes a result file.
        # the expected file on the server will have the same name as the input
        # file, except s/.xml/.rxml:
        ackdata = open(os.path.join(_testprep.TICKET_PATH, "responder_ack",
                                    "NewJersey-NJOC", "NJOC1.xml")).read()
        result_file = os.path.splitext(stored_files[0])[0] + ".rxml"
        resp.server.put(result_file, ackdata)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        del resp
        resp2 = MockFTPResponder(dont_send=1, verbose=0)
        resp2.log.lock = 1

        resp2.run2(serverdict)

        z = self.tdb.getrecords('response_ack_context')
        self.assertEquals(len(z), 0) # there should be none left

        # there should still be *one* log record, with success=1
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '1')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 0)

    def test_FDE1_201203(self, call_center='FMB1', client='CCFMBDD',
                         format='Baltimore', old_client="BGEAA",
                         filename="FMB1-2011-06/FMB1-2011-05-12-03191.txt",
                         ticket_number="2202887"):

        # Mantis #2961.
        serverdict = {'login': 'login', 'password': 'password',
                      'server': 'server', 'name': call_center, 'temp_dir': '',
                      'file_ext': '', 'exclude_states': [],
                      'mappings': {'C':  ('1', ''),
                                   'M':  ('2', ''),
                                   'N':  ('0', ''),
                                   'NC': ('0', '')},
                      'outgoing_dir': '',
                      'clients': [client],
                      'skip': [],
                      'status_code_skip': [],
                      'translations': {},
                      'send_emergencies': 0,
                      'send_3hour': 0,
                      'parsed_locates_only': 1,
                      }
        self.add_responder_config(self.conf_copy, call_center, serverdict)

        status = 'C'
        # Create test client
        client_id = _testprep.add_test_client(self.tdb, client, call_center)

        # post a ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", filename))
        raw = tl.tickets[0]
        # make sure <client> is on the ticket
        raw = raw.replace(old_client, client)
        t = self.handler.parse(raw, [format])
        for loc in t.locates:
            loc.status = '-P'
        t.ticket_format = t.source = call_center
        self.assertEquals(len(t.locates), 1)
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # update the locate with a new status so the responder_queue will be
        # populated
        for loc in t.locates:
            self.tdb.update('locate', 'locate_id', loc.locate_id,
              status=status, client_id=client_id)

        # add ticket notes; these must show up in <Remarks> section
        emp_name = 'John' + call_center
        emp_id = _testprep.add_test_employee(self.tdb, emp_name)
        uid = _testprep.add_test_user(self.tdb, emp_id)
        d = '2011-03-31 09:39:00'
        self.tdb.insert_ticket_note(ticket_id, emp_id, d, 'bar')

        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        self.assert_(locates)

        # Insert notes for each locate
        for loc in locates:
            self.tdb.insert_locate_note(loc['locate_id'], uid,
              date.Date().isodate(),
              '< > & A locate note for client %s' % loc['client_id'])

        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 1) # 1 term id on ticket

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # what files are stored?
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 1)
            # inspect this file and the XML
            data = resp.server.get(stored_files[0])

        self.assertTrue("<TicketNumber>"+ticket_number+"</TicketNumber>"
                        in data)
        self.assertTrue("<DistrictCode>" + client + "</DistrictCode>" in data)
        self.assertTrue("<StateCode>MD</StateCode>" in data)
        self.assertTrue("<StatusComments>CCFMBDD:" in data)

        # test if XML can be parsed properly
        ET.fromstring(data)

        # there should be *one* log record
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '0') # not flagged as success yet
        locate_id = logs[0]['locate_id']
        resp_id = logs[0]['response_id']

        z = self.tdb.getrecords('response_ack_context')
        self.assertEquals(len(z), 1) # there should be one record

        # pretend that the server writes a result file.
        # the expected file on the server will have the same name as the input
        # file, except s/.xml/.rxml:
        ackdata = open(os.path.join(_testprep.TICKET_PATH, "responder_ack",
                                    call_center, call_center+".xml")).read()
        result_file = os.path.splitext(stored_files[0])[0] + ".rxml"
        resp.server.put(result_file, ackdata)

        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # create a new responder
        del resp
        resp2 = MockFTPResponder(dont_send=1, verbose=0)
        resp2.log.lock = 1

        resp2.run2(serverdict)

        # there should still be *one* log record, with success=1
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 1)
        self.assertEquals(logs[0]['success'], '1')

        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 0)

        z = self.tdb.getrecords('response_ack_context')
        self.assertEquals(len(z), 0) # there should be none left

    test_FDE1_201203.has_error=1

    # new
    def test_FDE1_201209(self, call_center='FMB1', client='CCFMBDD',
                         format='Baltimore', old_client="BGEAA",
                         filename="FMB1-2011-06/FMB1-2011-05-12-03191.txt",
                         ticket_number="2202887"):

        # Mantis #3135: response_ack_context had a primary key that was too
        # restrictive; one file can have multiple tickets. This test verifies
        # that that situation is supported.

        serverdict = {'login': 'login', 'password': 'password',
                      'server': 'server', 'name': call_center, 'temp_dir': '',
                      'file_ext': '', 'exclude_states': [],
                      'mappings': {'C':  ('1', ''),
                                   'M':  ('2', ''),
                                   'N':  ('0', ''),
                                   'NC': ('0', '')},
                      'outgoing_dir': '',
                      'clients': [client],
                      'skip': [],
                      'status_code_skip': [],
                      'translations': {},
                      'send_emergencies': 0,
                      'send_3hour': 0,
                      'parsed_locates_only': 1,
                      }
        self.add_responder_config(self.conf_copy, call_center, serverdict)

        status = 'C'
        # Create test client
        client_id = _testprep.add_test_client(self.tdb, client, call_center)

        # post two tickets
        for number in ["123456", "123457"]:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "tickets", filename))
            raw = tl.tickets[0]
            raw = raw.replace(ticket_number, number)
            # make sure <client> is on the ticket
            raw = raw.replace(old_client, client)
            t = self.handler.parse(raw, [format])
            for loc in t.locates:
                loc.status = '-P'
            t.ticket_format = t.source = call_center
            self.assertEquals(len(t.locates), 1)
            ticket_id = self.tdb.insertticket(t, whodunit='parser')

            # update the locate with a new status so the responder_queue will
            # be populated
            for loc in t.locates:
                self.tdb.update('locate', 'locate_id', loc.locate_id,
                  status=status, client_id=client_id)

        ticket_ids = self.tdb.getrecords('ticket')
        self.assertEquals(len(ticket_ids), 2)

        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(responses), 2) # 1 term id on each ticket

        # start a responder...
        def create_server(self, serverdict):
            self.server = MockCCFTPClient(self.config, serverdict, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
            resp.log.lock = 1
            resp.run2(serverdict)

            # what files are stored?
            stored_files = resp.server.getfilelist()
            self.assertEquals(len(stored_files), 1)
            # inspect this file and the XML
            data = resp.server.get(stored_files[0])

        # there should be 2 tickets in the XML
        self.assertTrue(data.count("<TicketNumber>"), 2)

        # test if XML can be parsed properly
        ET.fromstring(data)

        # there should be *two* log records
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), 2)
        self.assertEquals(logs[0]['success'], '0') # not flagged as success yet

        z = self.tdb.getrecords('response_ack_context')
        self.assertEquals(len(z), 2) # there should be two records

    test_FDE1_201209.has_error=1

    def _add_NewJersey_Rockland_client_locate(self, client_locates, client_code,
     notes, combined_notes, status, translated_status, method, closed_date,
     result, ftpresponderdata, ticket_number, state,
     public_improvement_status='0'):
        def template_data():
            return {'ticket_number': ticket_number, 'state': state,
             'company': client_code, 'status': translated_status,
             'method': method, 'completion_datetime': closed_date,
             'user': ftpresponderdata.USER, 'notes': combined_notes,
             'result': result,
             'public_improvement_status': public_improvement_status}
        client_locates[client_code] = {'notes': notes, 'status': status,
         'closed_date': closed_date, 'result': result,
         'expected_response': ftpresponderdata.TEMPLATE2 % template_data(),
         'ack': ftpresponderdata.TEMPLATE2.replace('End::',
         'Result:: %(result)s\nEnd::') % template_data()}
         
    def test_NewJersey_Rockland_1(self):
        """
        QMAN-3321: Test NewJersey Rockland with 1 locate
        """
        client_locates = {}
             
        # Set up call center
        call_center = 'NewJersey'
        update_call_center = 'NewJersey6'
        kind = 'rock'
        ftpresponderdata = ftpresponder_data.get_responderdata_class(
         call_center, kind)

        # Set up ticket
        format = 'NewJersey6'
        filename = 'NewJersey6-2013-11-01/NewJersey-2013-10-23-10-15-49-516-BEYQY.txt'
        ticket_number = "132350168"
        state = 'NJ'
        
        # Set up locate
        client_code = 'REC'
        notes = ['Note 1', 'Note 2', 'Note 3']
        combined_notes = 'REC: Note 1\nREC: Note 2\nREC: Note 3'
        status = 'XXW'
        translated_status = '2DW'
        method = '0'
        closed_date = date.Date().isodate().replace('-', '/')
        result = 'Success'
        
        # Add locate to client_locates
        self._add_NewJersey_Rockland_client_locate(client_locates, client_code,
         notes, combined_notes, status, translated_status, method, closed_date,
         result, ftpresponderdata, ticket_number, state)

        # Run test
        self._test_NewJersey_Rockland(call_center, update_call_center, format,
         filename, client_locates, kind)

    def test_NewJersey_Rockland_2(self):
        """
        QMAN-3321: Test NewJersey Rockland with 1 locate
        """
        client_locates = {}

        # Add test status
        _testprep.add_test_status(self.tdb, 'MRC', complete=1)
    
        # Set up call center
        call_center = 'NewJersey'
        update_call_center = 'NewJersey6'
        kind = 'rock'
        ftpresponderdata = ftpresponder_data.get_responderdata_class(
         call_center, kind)

        # Set up ticket
        format = 'NewJersey6'
        filename = 'NewJersey6-2013-11-01/NewJersey-2013-10-23-10-15-49-516-BEYQY.txt'
        ticket_number = "132350168"
        state = 'NJ'
        
        # Set up locate
        client_code = 'REC'
        note45 = 'This note should be 45 characters long    end' 
        notes = [note45, note45, note45, note45, note45, note45, note45,
         note45, note45, 'The End:: tag must be replaced with End_::  end']        
        combined_notes = ('REC: ' + note45 + '\n') * 9 +\
         'REC: The End_:: tag must be replaced with End_::  end'
        status = 'MRC'
        translated_status = 'MRC'
        method = '3'
        closed_date = date.Date().isodate().replace('-', '/')
        result = 'Fail'
        
        # Add locate to client_locates
        self._add_NewJersey_Rockland_client_locate(client_locates, client_code,
         notes, combined_notes, status, translated_status, method, closed_date,
         result, ftpresponderdata, ticket_number, state)

        # Run test
        self._test_NewJersey_Rockland(call_center, update_call_center, format,
         filename, client_locates, kind)
       
    def test_NewJersey4_1(self):
        """
        QMAN-3321: Test NewJersey4 with 1 locate
        """
        client_locates = {}

        # Add test status
        _testprep.add_test_status(self.tdb, 'MRG', complete=1)
        _testprep.add_test_status(self.tdb, '3', complete=1)
    
        # Set up call center
        call_center = 'NewJersey4'
        update_call_center = 'NewJersey7'
        kind = ''
        ftpresponderdata = ftpresponder_data.get_responderdata_class(
         call_center, kind)

        # Set up ticket
        format = 'NewJersey7'
        filename = 'NewJersey7-2013-11-01/NewJersey4-2013-10-23-10-16-42-822-0KF3C.txt'
        ticket_number = '08233-170-006-00'
        state = 'NY'
        
        # Set up locate 1
        client_code = 'RCK-ORNG'
        note40 = 'This note should be 40 chars long    end' 
        notes = [note40, note40, note40, note40, note40, note40, note40,
         note40, note40, 'The End:: must be replaced with End_::  end']        
        combined_notes = ('RCK-ORNG: ' + note40 + '\n') * 9 +\
         'RCK-ORNG: The End_:: must be replaced with E(More...)'
        status = 'C'
        translated_status = 'CLR'
        method = '0'
        closed_date = date.Date().isodate().replace('-', '/')
        result = 'Success'

        # Set up public improvement locate
        public_improvement_client = 'RCK-ORNG-P'
        public_improvement_status = '3'
        
        # Add locate to client_locates
        self._add_NewJersey_Rockland_client_locate(client_locates, client_code,
         notes, combined_notes, status, translated_status, method, closed_date,
         result, ftpresponderdata, ticket_number, state,
         public_improvement_status=public_improvement_status)

        # Run test
        self._test_NewJersey_Rockland(call_center, update_call_center, format,
         filename, client_locates, kind,
         public_improvement_client=public_improvement_client,
         public_improvement_status=public_improvement_status)
        
    def test_NewJersey4_2(self):
        """
        QMAN-3495: Test NewJersey4 with public improvement status mapping
        """
        client_locates = {}

        # Add test status
        _testprep.add_test_status(self.tdb, 'MRG', complete=1)
        _testprep.add_test_status(self.tdb, 'XPI2', complete=1)
    
        # Set up call center
        call_center = 'NewJersey4'
        update_call_center = 'NewJersey7'
        kind = ''
        ftpresponderdata = ftpresponder_data.get_responderdata_class(
         call_center, kind)

        # Set up ticket
        format = 'NewJersey7'
        filename = 'NewJersey7-2013-11-01/NewJersey4-2013-10-23-10-16-42-822-0KF3C.txt'
        ticket_number = '08233-170-006-00'
        state = 'NY'
        
        # Set up locate 1
        client_code = 'RCK-ORNG'
        note40 = 'This note should be 40 chars long    end' 
        notes = [note40, note40, note40, note40, note40, note40, note40,
         note40, note40, 'The End:: must be replaced with End_::  end']        
        combined_notes = ('RCK-ORNG: ' + note40 + '\n') * 9 +\
         'RCK-ORNG: The End_:: must be replaced with E(More...)'
        status = 'C'
        translated_status = 'CLR'
        method = '0'
        closed_date = date.Date().isodate().replace('-', '/')
        result = 'Success'

        # Set up public improvement locate
        public_improvement_client = 'RCK-ORNG-P'
        public_improvement_status = 'XPI2'
        public_improvement_mapped_status = '2'
        
        # Add locate to client_locates
        self._add_NewJersey_Rockland_client_locate(client_locates, client_code,
         notes, combined_notes, status, translated_status, method, closed_date,
         result, ftpresponderdata, ticket_number, state,
         public_improvement_status=public_improvement_mapped_status)

        # Run test
        self._test_NewJersey_Rockland(call_center, update_call_center, format,
         filename, client_locates, kind,
         public_improvement_client=public_improvement_client,
         public_improvement_status=public_improvement_status)

    def _test_NewJersey_Rockland(self, call_center, update_call_center, format,
     filename, client_locates, kind, public_improvement_client=None,
     public_improvement_status=None):
        # Create responder config
        raw_responderdata = {'login': 'login', 'password': 'password',
         'server': 'server', 'name': call_center, 'temp_dir': '', 'file_ext': '',
         'exclude_states': [], 'mappings': {
             'XXW': ('2DW', ''), 'O': ('COT', ''), 'C': ('CLR', ''),
             'DM': ('DR', ''), 'DME': ('DR', ''), 'DNC': ('DR', ''),
             'MRC': ('MRC', ''), 'MRE': ('MRE', ''), 'MRG': ('MRG', ''),
             'N': ('NC', ''), 'OS': ('OFC', ''), 'XD': ('CF', ''),
             'XPI0': ('0', ''), 'XPI1': ('1', ''), 'XPI2': ('2', ''),
             'XPI3': ('3', '')},
         'outgoing_dir': '', 'clients': client_locates.keys(), 'skip': [],
         'status_code_skip': [], 'translations': {}, 'send_emergencies': 0,
         'send_3hour': 0, 'parsed_locates_only': 1, 'kind': kind}

        # Add test clients
        for client in client_locates:
            client_locates[client]['client_id'] = _testprep.add_test_client(
             self.tdb, client, call_center,
             update_call_center=update_call_center)
        if public_improvement_client:
            _testprep.add_test_client(self.tdb, public_improvement_client,
             call_center, update_call_center=update_call_center)
              
        # Post a ticket
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", filename))
        raw = tl.tickets[0]
        t = self.handler.parse(raw, [format])
        locates = []
        public_improvement_locate = None
        for loc in t.locates:
            loc.status = '-P'
            if loc.client_code in client_locates:
                loc.client_id = client_locates[loc.client_code]['client_id']
                locates.append(loc)
            elif loc.client_code == public_improvement_client:
                public_improvement_locate = loc

        t.ticket_format = call_center
        t.source = update_call_center or call_center
        # Verify each expected client has a locate
        self.assertEquals(sorted([loc.client_code for loc in locates]),
         sorted(client_locates.keys()))
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        for loc in locates:
            # Add locate notes
            for note in client_locates[loc.client_code]['notes']:
                self.tdb.insert_locate_note(loc.locate_id, self.test_emp_id,
                 date.Date().isodate(), note)

            # Status the locates, to add them to the responder_queue
            self.tdb.update('locate', 'locate_id', loc.locate_id,
             status=client_locates[loc.client_code]['status'],
             closed_date=client_locates[loc.client_code]['closed_date']) 

        if public_improvement_locate:
            self.tdb.update('locate', 'locate_id',
             public_improvement_locate.locate_id,
             status=public_improvement_status) 
             
        # Verify 1 queued response per expected client
        responses = self.tdb.getrecords('responder_queue')
        self.assertEquals(set([response['client_code'] for response in responses
         if response['client_code'] <> public_improvement_client]),
         set(client_locates.keys()))

        # Create and run a responder, to post response files
        def create_server(self, raw_responderdata):
            self.server = MockCCFTPClient(self.config, raw_responderdata, MockLogger())
        MockFTPResponder.create_server = create_server
        with Mockery(config, 'getConfiguration', lambda: self.conf_copy):
            resp = MockFTPResponder(dont_retrieve=1, verbose=0)
        resp.log.lock = 1
        resp.run2(raw_responderdata)

        # Verify 1 file stored, and read it
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 1)
        data = resp.server.get(stored_files[0])
            
        # Verify expected responses in file data    
        for client in client_locates.keys():
            self.assertTrue(client_locates[client]['expected_response'] in data)

        # Verify 1 response_log record per expected client, with success = '0'
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), len(client_locates))
        for log in logs:        
          self.assertEquals(log['success'], '0')

        # Verify 1 response_ack_context record per expected client
        contexts = self.tdb.getrecords('response_ack_context')
        self.assertEquals(sorted([context['client_code'] for context in contexts]),
         sorted(client_locates.keys()))

        # Pretend that the server writes an ack file. This file will have the
        # same name as the response file, except the extension will be .RCT.
        ack_data = string.join([client_locates[client]['ack'] for client in client_locates], '')
        ack_file = os.path.splitext(stored_files[0])[0] + '.RCT'
        resp.server.put(ack_file, ack_data)
        
        # Verify 2 files stored (response and ack)
        stored_files = resp.server.getfilelist()
        self.assertEquals(len(stored_files), 2)

        # Create and run a new responder, to process ack files
        del resp
        resp2 = MockFTPResponder(dont_send=1, verbose=0)
        resp2.log.lock = 1
        resp2.run2(raw_responderdata)

        # Verify 1 response_log record per expected client, with success = '1' 
        # if result = 'Success', or '0' otherwise
        logs = self.tdb.getrecords('response_log')
        self.assertEquals(len(logs), len(client_locates))
        for log in logs:
            response_log_client = ''
            for loc in locates:
                if loc.locate_id == log['locate_id']:
                    response_log_client = loc.client_code
                    break
            self.assertTrue(response_log_client)
            if client_locates[response_log_client]['result'] == 'Success': 
                self.assertEquals(log['success'], '1')
            else:
                self.assertEquals(log['success'], '0')
                
        # Verify queue is empty, except for public improvement locate
        queue = self.tdb.getrecords('responder_queue')
        self.assertEquals(len(queue), 1 if public_improvement_client else 0)
        
        # Verify response_ack_context is empty
        z = self.tdb.getrecords('response_ack_context')
        self.assertEquals(len(z), 0)
    
if __name__ == "__main__":

    unittest.main()
