# test_main_FGV1.py

import copy
import glob
import os
import site; site.addsitedir('.')
import string
import StringIO
import unittest
#
import _testprep
import config
import filetools
import main
import ticket_db
import ticketloader
import ticketparser

from mockery import *
from mockery_tools import *

from test_main import drop
from test_archiver import create_test_dirs, clear, delete_test_dirs

class TestFGV1(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)
        sql = "select * from call_center where cc_code = 'FGV1'"
        rows = self.tdb.runsql_result(sql)
        if not rows:
            _testprep.add_test_call_center(self.tdb, 'FGV1')
            self.delete_call_center = True
        else:
            self.delete_call_center = False

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)
        # restore the getconfiguration method
        #restore()
        if self.delete_call_center:
            _testprep.delete_test_call_center(self.tdb, 'FGV1')

    def prepare(self):
        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'incoming': 'test_incoming_FGV1',
          'format': 'FGV1',
          'processed': 'test_processed_FGV1',
          'error': 'test_error_FGV1',
          'attachments': ''},]

        self.TEST_DIRECTORIES = []

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['incoming','processed', 'error']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)

    @with_config_copy
    @with_call_center_copy
    def test_FGV1(self):
        '''
        Test AT&T summary ticket
        '''
        self.prepare()

        with open(os.path.join("..", "testdata", "sample_tickets", "1422",
                  "FGV1-2009-07-07-00002.txt")) as f:
            raw = f.read()
        drop("FGV1_summary_1.txt", raw, dir='test_incoming_FGV1')

        f = open(os.path.join("..", "testdata", "sample_tickets", "1422",
            "FGV1-2009-07-09-00001.txt"))
        raw = f.read()
        f.close()
        drop("FGV1_summary_2.txt", raw, dir = 'test_incoming_FGV1')

        path = os.path.join("..", "testdata", "sample_tickets", "1422",
               "FGV1-2009-07-14-00002.txt")
        raw = filetools.read_file(path)
        drop("FGV1_summary_3.txt", raw, dir = 'test_incoming_FGV1')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='FGV1')
        m.log.logger.logfile = StringIO.StringIO()
        m.log.lock = 1
        m.run(runonce=1)
        log = m.log.logger.logfile.getvalue()
        m.log.logger.logfile.close()

        # what's the summary header id of the ticket we just posted?
        sh_id, sd_ids = m.ids[-1]

        self.assertTrue(sh_id > 0)
        self.assertEquals(len(sd_ids),709)

if __name__ == "__main__":
    unittest.main()

