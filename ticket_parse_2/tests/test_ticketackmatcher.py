# test_ticketackmatcher.py

import site; site.addsitedir('.')
import unittest
#
import _testprep
import ticket_db
import ticketackmatcher

class TestTicketAckMatcher(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()

        # insert some test records
        for call_center, match_string, work_priority in (
            ('XYZ', 'emergency', None),
            ('XYZ', 'CANCEL', None),
            ('ABC', 'NLOC', None),
            ('DEF', 'EMER',10),
            ('DEF', 'PRONTO', 5),
            ('DEF', 'CANCEL', 4),
        ):
            if work_priority is not None:
                _testprep.add_test_ticket_ack_match(self.tdb, call_center,
                 match_string, work_priority=work_priority)
            else:
                _testprep.add_test_ticket_ack_match(self.tdb, call_center,
                 match_string)

    def test_matching(self):
        matcher = ticketackmatcher.TicketAckMatcher(self.tdb)
        self.assertEquals(matcher.get_values('ABC'), ['nloc',])
        self.assertEquals(matcher.get_values('XYZ'), ['emergency','cancel'])
        self.assertEquals(matcher.get_values('DEF'), ['emer','pronto','cancel'])

        match = matcher.is_ticket_ack
        self.assertEquals(match('XYZ', 'Emergency Ticket'), 1)
        self.assertEquals(match('XYZ', 'DON"T CANCEL'), 1)
        self.assertEquals(match('XYZ', 'fubar'), 0)
        self.assertEquals(match('ABC', 'spaNlock'), 1)

        get_work_priority = matcher.get_work_priority
        self.assertEquals(get_work_priority('XYZ','EMER'),None)
        self.assertEquals(get_work_priority('DEF','EMER'),10)
        self.assertEquals(get_work_priority('DEF','emergency'),10)
        self.assertEquals(get_work_priority('DEF','PRONTO'),5)
        self.assertEquals(get_work_priority('DEF','CANCEL'),4)
        matcher2 = ticketackmatcher.TicketAckMatcher(self.tdb)
        self.assertEquals(matcher2.data,matcher2._data)



if __name__ == "__main__":

    unittest.main()
