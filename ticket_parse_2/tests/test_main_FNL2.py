# test_main_FNL2.py

import copy
import glob
import os
import site; site.addsitedir('.')
import string
import unittest
#
import _testprep
import call_centers
import config
import filetools
import main
import ticket_db
import ticketloader
import ticketparser
import tools

from mockery_tools import *
from test_main import drop
from test_archiver import create_test_dirs, clear, delete_test_dirs

class TestFNL2(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

    def prepare(self):
        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'incoming': 'test_incoming_FNL2',
          'format': 'FNL2',
          'processed': 'test_processed_FNL2',
          'error': 'test_error_FNL2',
          'attachments': ''},]
        _testprep.set_call_center_active(self.tdb, 'FNL2')

        self.TEST_DIRECTORIES = []

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['incoming','processed', 'error']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)

        call_centers.get_call_centers(self.tdb).reload()

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)

    @with_config_copy
    def test_FNL2_1(self):
        self.prepare()

        raw = filetools.read_file(os.path.join(_testprep.TICKET_PATH,
              "sample_tickets", "FNL2", "FNL2-2008-12-30-00001.txt"))

        drop("FNL2_1.txt", raw, dir='test_incoming_FNL2')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='FNL2')
        m.log.lock = 1
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(len(t2.locates), 3)
        self.assertEquals(t2.source, 'FNL2')
        self.assertEquals(t2.ticket_format, 'FNL2')

    @with_config_copy
    def test_FNL2_2(self):
        self.prepare()
        all_tickets = tools.safeglob(os.path.join("..", "testdata",
                      "sample_tickets", "FNL2", "FNL2-2008-12-30-*.txt"))

        # Drop all of the test tickets
        for i,ticket_file in enumerate(all_tickets):
            f = open(ticket_file)
            raw = string.join(f.readlines(),'\n')
            f.close()
            drop("FNL2_%d.txt" % (i,), raw, dir = 'test_incoming_FNL2')
        # Parse the lot

        m = main.Main(verbose=0, call_center='FNL2')
        m.log.lock = 1
        m.run(runonce=1)

        self.assertEquals(len(m.ids),25)
        for id in m.ids:
            # Get the ticket
            t2 = self.tdb.getticket(id)
            self.assertEquals(t2.parsed_ok, '1')

if __name__ == "__main__":
    unittest.main()

