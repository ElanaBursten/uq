# test_launcher.py

import os
import unittest
import site; site.addsitedir('.')
#
import launcher
import _testprep

class MockLauncher(launcher.Launcher):
    def _start(self):
        pass

class TestLauncher(unittest.TestCase):

    def test_001(self):
        la = launcher.Launcher()
        la.applications = [
         ("test", "launchertest.bat", "mutextest"),
        ]
        # problem is, this will pop up a new window :-/

    def test_read_configuration(self):
        import config; cfg = config.getConfiguration()
        cfg_centers = [p['format'] for p in cfg.processes if p.get('format')]
        print cfg_centers
        la = MockLauncher()
        apps = la.read_configuration(os.path.join(_testprep.TICKET_PATH,
               "config", "launcher_sample.xml"))
        self.assertEquals(apps[0],
         ('router', 'c:\\work\\uq\\ticket_parse\\runrouter.bat', 'router', ''))

        # centers in testdata/config/config.xml; may change
        for i, center in enumerate(cfg_centers, 1):
            self.assertEquals(apps[i][0], "parser " + center)
            self.assertEquals(apps[i][2], "parser")
            self.assertEquals(apps[i][3], center)

        self.assertEquals(la.wait, 5)
        self.assertEquals(la.runtime, 540)
        self.assertEquals(la.pythonpath, 'c:\\python23\\python.bat')

if __name__ == "__main__":

    unittest.main()

