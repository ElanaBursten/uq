# tst_config.py

import os
#
import config
import datadir

CONFIG_TEMPLATE = """\
<?xml version="1.0" ?>
<root>
    <ticketparser>
        <!-- ticketparser/ado_database --> 

        <schema name="uq_schema.sql" />
        <server>
            <timezone>EST/EDT</timezone>
        </server>

        <!-- ticketparser/process list --> 

        <!-- administrator info. multiple admins are possible. -->
        <admin name="admin" email="admin@test.com" />
        <admin name="admin" email="admin@test.com" ln="1" />
        <admin name="admin" email="admin@test.com" ln="0" />
        <smtp host="mail.test.net" from="logger@test.net" />
        <!-- smtp server name, and "from" address -->

        <!-- in this directory we log errors -->
        <logdir name="logs" />
    </ticketparser>

    <ticketrouter numrounds="3"/>

    <responder>
        <nodelete>
            <callcenter code="FCV1" />
            <callcenter code="FCV2" />
            <callcenter code="FDX1" />
            <callcenter code="6001" />
        </nodelete>

        <work_order>
            <!-- responder/work_order/callcenter list -->
        </work_order>

        <irth>
            <!-- responder/irth/callcenter list -->
        </irth>
        
        <ftp>
            <!-- responder/ftp/callcenter list -->
        </ftp>        
         
        <xmlhttp>
            <!-- responder/xmlhttp/callcenter list -->
        </xmlhttp>

        <email>
            <!-- responder/email/callcenter list -->
        </email>

        <soap>
            <!-- responder/soap/callcenter list -->
        </soap>
    </responder>

    <archiver 
        root="c:/work/uq/ticket_parse/temp/"
        archive_dir="c:\temp"
    />
    <!-- archive_dir must always use backslashes! -->

    <integrity_check emails="admin@test.com" />
    <polygon_dir name="c:\work\uq\ticket_parse\testdata\maps" />
    <event_log store_events="1" />

    <watchdog>
        <output filename="" />
        <old_responses threshold="60">
            <ignore callcenter="FCO1" />
        </old_responses>
    </watchdog>
</root>
"""

TEST_CONFIG_FILENAME = os.path.join(datadir.datadir.path, 'test_config.xml')

db_xml = ''


class TestConfig:
    def __init__(self):
        global db_xml
        self.config_xml = CONFIG_TEMPLATE
        if not db_xml:
            config_ = config.getConfiguration()
            db_element = config_._root.find('ticketparser/ado_database')
            db_xml = '<ado_database\n'
            for k, v in db_element.attrib.iteritems():
                db_xml += '            %s="%s"\n' % (k, v)
            db_xml += '        />'

        self.add_section('ticketparser/ado_database', db_xml)
        
    def add_section(self, name, xml):
        self.config_xml = self.config_xml.replace('<!-- ' + name + ' -->', xml)

    def save(self, filename=TEST_CONFIG_FILENAME):
        f = open(filename, 'w')
        f.write(self.config_xml)
        f.close()