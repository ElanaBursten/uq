# test_xmlhttpresponder_1421.py

from __future__ import with_statement
import os
import unittest
import xml.etree.ElementTree as ET

import _testprep
import callcenters.c1421 as c1421
import config
import dbinterface_ado
import locate
import ticket_db
import ticketloader
import ticketparser
import tools
import xmlhttpresponder

class MockXMLHTTPResponder(xmlhttpresponder.XMLHTTPResponder):
    sent = []
    def _send_response_xml(self, *args, **kwargs):
        self.sent.append((args, kwargs))
        # responder expects a COM object, so we mock one
        class Reply:
            Text = "250 ok"
            xml = "<status>success</status>"
        return Reply()

class TestXMLHTTPResponder(unittest.TestCase):

    def setUp(self):
        self.dbado = dbinterface_ado.DBInterfaceADO()
        self.tdb = ticket_db.TicketDB(dbado=self.dbado)
        _testprep.clear_database(self.tdb)
        self.test_emp_id = _testprep.add_test_employee(self.tdb, "TestXMLHTTP")
        self.cfg = config.getConfiguration(reload=True)

    def tearDown(self):
        config.getConfiguration(reload=True)

    def addTicket(self):
         # add tickets
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "1422-2013-02-12",
             "1422-2013-02-08-11-23-53-460-E62C7.txt"))
        raw = tl.tickets[0]

        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ['SC1421PUPS'])
        t.locates.append(locate.Locate('BSZN42'))
        return t

    def setup1421(self, z):
        try:
            del self.cfg.xmlhttpresponders['1421']
        except:
            pass
        self.cfg.xmlhttpresponders['1421'] = z
        self.cfg.responderconfigdata.add(z['name'], 'xmlhttp', z, unique=True)

    def test_1421_pups(self):
        # set up config
        z = {'ack_dir': '',
             'ack_proc_dir': '',
             'mappings': {'M': ('Marked', ''),
                          'N': ('Cleared', '')},
             'logins': [{'login': 'BSNE', 'password': '123'}],
             'clients': ['SCA88', 'SCAZ88', 'SCEDZ05', 'SCGZ02'],
             'all_versions': 0,
             'id': 'spam',
             'initials': 'UQ',
             'name': '1421',
             'post_url': 'http://example.com/whatgoeshere',
             'resp_url': 'http://example.com/whatgoeshere',
             'send_3hour': 1,
             'send_emergencies': 1,
             'skip': [],
             'status_code_skip': [],
             'translations': {},
             'kind': 'pups'
        }

        self.setup1421(z)
        t = self.addTicket()

        # find SCEDZ05 locate and mark it
        idx = tools.findfirst(t.locates, lambda l: l.client_code == 'SCEDZ05')
        assert idx > -1
        t.locates[idx].status = 'M'
        assert t.locates[idx].client_code == 'SCEDZ05'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add a SCEDZ05 locate to responder_multi_queue
        sql = """
         select * from locate
         where client_code = 'SCEDZ05' and ticket_id = %s""" % ticket_id
        locate = self.dbado.runsql_result(sql)
        sql = "exec insert_responder_multi_queue 'client', %s" % \
         locate[0]['locate_id']
        self.dbado.runsql(sql)

        # make sure we have one ticket in the database
        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # check responder queues
        rmq = self.tdb.getrecords('responder_multi_queue', ticket_format='1421')
        rq = self.tdb.getrecords('responder_queue', ticket_format='1421')
        self.assertEquals(len(rmq), 1)
        self.assertEquals(len(rq), 1)

        # run mock responder
        resp = MockXMLHTTPResponder(verbose=0)
        resp.sent = []
        resp.respond_all()

        # check output, response log
        self.assertEquals(len(resp.sent), 1)

        #grab and parse the XML to inspect the facility
        #make sure that it is set based upon the client table
        #use of lazy namespacing is OK for testing purposes
        sent_XML = resp.sent[0][0][0]
        root = ET.fromstring(sent_XML)
        for responses in root.findall('{ns0}responses'):
            for response in responses.findall('{ns0}response'):
                facility = response.find('{ns0}facilities').text
                self.assertEquals('Electric', facility)


        # responder_queue should be empty now
        rmq = self.tdb.getrecords('responder_multi_queue', ticket_format='1421')
        rq = self.tdb.getrecords('responder_queue', ticket_format='1421')
        self.assertEquals(len(rmq), 1)
        self.assertEquals(len(rq), 0)

        rl = self.tdb.getrecords('response_log')
        self.assertEquals(rl[0]['response_sent'], 'Marked')
        self.assertEquals(rl[0]['success'], '1')

        #Add another ticket but change the facility type

        t = self.addTicket()

        #change the facility type of the client to check the response mechanism
        sql = """
            update client set utility_type = 'pipl' where oc_code = 'SCEDZ05'"""
        self.dbado.runsql(sql)

         # find SCEDZ05 locate and mark it
        idx = tools.findfirst(t.locates, lambda l: l.client_code == 'SCEDZ05')
        assert idx > -1
        t.locates[idx].status = 'M'
        assert t.locates[idx].client_code == 'SCEDZ05'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add a SCEDZ05 locate to responder_multi_queue
        sql = """
         select * from locate
         where client_code = 'SCEDZ05' and ticket_id = %s""" % ticket_id
        locate = self.dbado.runsql_result(sql)
        sql = "exec insert_responder_multi_queue 'client', %s" % \
         locate[0]['locate_id']
        self.dbado.runsql(sql)

        # run mock responder
        resp.sent = []
        resp.respond_all()

        #grab and parse the XML to inspect the facility
        #make sure that it is set based upon the client table
        #use of lazy namespacing is OK for testing purposes
        sent_XML = resp.sent[0][0][0]
        root = ET.fromstring(sent_XML)
        for responses in root.findall('{ns0}responses'):
            for response in responses.findall('{ns0}response'):
                facility = response.find('{ns0}facilities').text
                self.assertEquals('Gas', facility)

    def test_1421_non_pups(self):
        # set up config
        z = {'ack_dir': '',
             'ack_proc_dir': '',
             'mappings': {'M': ('Marked', ''), 
                          'N': ('Cleared', '')},
             'logins': [{'login': 'BSNE', 'password': '123'}],
             'clients': ['SCA88', 'SCAZ88', 'SCEDZ05', 'SCGZ02'],
             'all_versions': 0,
             'id': 'spam',
             'initials': 'UQ',
             'name': '1421',
             'post_url': 'http://example.com/whatgoeshere',
             'resp_url': 'http://example.com/whatgoeshere',
             'send_3hour': 1,
             'send_emergencies': 1,
             'skip': [],
             'status_code_skip': [],
             'translations': {}
        }

        self.setup1421(z)
        t = self.addTicket()

        # find SCEDZ05 locate and mark it
        idx = tools.findfirst(t.locates, lambda l: l.client_code == 'SCEDZ05')
        assert idx > -1
        t.locates[idx].status = 'M'
        t.locates[idx].closed_date = '2014-08-27'
        assert t.locates[idx].client_code == 'SCEDZ05'
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # add a SCEDZ05 locate to responder_multi_queue
        sql = """
         select * from locate
         where client_code = 'SCEDZ05' and ticket_id = %s""" % ticket_id
        locate = self.dbado.runsql_result(sql)
        sql = "exec insert_responder_multi_queue 'client', %s" % \
         locate[0]['locate_id']
        self.dbado.runsql(sql)

        # make sure we have one ticket in the database
        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # check responder queues
        rmq = self.tdb.getrecords('responder_multi_queue', ticket_format='1421')
        rq = self.tdb.getrecords('responder_queue', ticket_format='1421')
        self.assertEquals(len(rmq), 1)
        self.assertEquals(len(rq), 1)

        # run mock responder
        resp = MockXMLHTTPResponder(verbose=0)
        resp.sent = []
        resp.respond_all()

        # check output, response log
        self.assertEquals(len(resp.sent), 1)

        # responder_multi_queue should be empty now
        rmq = self.tdb.getrecords('responder_multi_queue', ticket_format='1421')
        rq = self.tdb.getrecords('responder_queue', ticket_format='1421')
        self.assertEquals(len(rmq), 0)
        self.assertEquals(len(rq), 1)

        rl = self.tdb.getrecords('response_log')
        self.assertEquals(rl[0]['response_sent'], 'Marked')
        self.assertEquals(rl[0]['success'], '1')

    def test_1421_pups_skip(self):
        # QMAN-3683

        # set up config
        z = {'ack_dir': '',
             'ack_proc_dir': '',
             'mappings': {'M': ('Marked', ''),
                          'N': ('Cleared', '')},
             'logins': [{'login': 'BSNE', 'password': '123'}],
             'clients': ['SCA88', 'SCAZ88', 'SCEDZ05', 'SCGZ02'],
             'all_versions': 0,
             'id': 'spam',
             'initials': 'UQ',
             'name': '1421',
             'post_url': 'http://example.com/whatgoeshere',
             'resp_url': 'http://example.com/whatgoeshere',
             'send_3hour': 1,
             'send_emergencies': 1,
             'skip': ['BSZN42'],
             'status_code_skip': [],
             'translations': {},
             'kind': 'pups'
        }

        self.setup1421(z)

        t = self.addTicket()

        # make sure skip list contains BSZN42
        r = self.cfg.responderconfigdata.get_responders('1421','xmlhttp')[0]
        cc, rtype, rdata = r
        self.assertEquals(rdata['skip'], ['BSZN42'])

        # find SCEDZ05 and BSZN42 locates and mark them, so they will be added
        # to the responder queue
        for termid in ["SCEDZ05", "BSZN42"]:
            idx = tools.findfirst(t.locates, lambda l: l.client_code == termid)
            assert idx > -1
            t.locates[idx].status = 'M'
            assert t.locates[idx].client_code == termid
        ticket_id = self.tdb.insertticket(t, whodunit='parser')

        # make sure we have one ticket in the database
        tix = self.tdb.getrecords('ticket')
        self.assertEquals(len(tix), 1)

        # there should be two records in the nesponder queue
        rq = self.tdb.getrecords('responder_queue', ticket_format='1421')
        self.assertEquals(len(rq), 2)

        # run mock responder
        resp = MockXMLHTTPResponder(verbose=0)
        resp.sent = []
        resp.respond_all()

        # check output, response log
        self.assertEquals(len(resp.sent), 1)

        # responder_queue should be empty now
        rq = self.tdb.getrecords('responder_queue', ticket_format='1421')
        self.assertEquals(len(rq), 0)

        rl = self.tdb.getrecords('response_log')
        self.assertEquals(rl[0]['response_sent'], 'Marked')
        self.assertEquals(rl[0]['success'], '1')

if __name__ == "__main__":
    unittest.main()

