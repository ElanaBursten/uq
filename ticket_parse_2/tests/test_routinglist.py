# test_routinglist.py

from glob import glob
import os
import site;
import unittest
site.addsitedir('.')
#
import routinglist
import ticket_db
import ticketparser
import ticketloader
import tools

class TestRoutingList(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()

    def prepare(self):
        self.tdb.deleterecords("routing_list", call_center="ZZZ")
        sql_template = """
         insert routing_list (call_center, field_name, field_value, area_id)
         values ('%s', '%s', '%s', %d)
        """
        blurb = [
            ("ZZZ", "work_county", "MECKLENBURG", 200),
            ("ZZZ", "work_state", "/(NC|SC)", 201),
            # this invalid regex should be ignored
            ("ZZZ", "work_state", "/[x", 666),
            ("ZZZ", "work_state", "/A", 123),
            ('FNL1','company','AT&T',15839),
            ('FNL1','company','AT&T UTILITY OPERATIONS',15839),
            ('FNL1','company','CENTURY TEL',15840)
        ]
        for t in blurb:
            sql = sql_template % t
            self.tdb.runsql(sql)

    def test_literal_match(self):
        # test a literal match on a field.
        self.prepare()
        rl = routinglist.RoutingList(self.tdb, verbose=0)

        tl = ticketloader.TicketLoader("../testdata/NorthCarolina-1.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCarolina"])
        t.ticket_format = "ZZZ"

        _, area_id = rl.route(t)
        self.assertEquals(area_id, "200")

        # what happens if we change that field? we should then match the
        # work_state field...
        t.work_county = "SOMETHING ELSE"
        _, area_id = rl.route(t)
        self.assertEquals(area_id, "201")

        # then we change the state as well...
        t.work_state = "XX"

        area_id = rl.route(t)
        self.assertEquals(area_id, None)

        self.assertEquals(len(rl.data["ZZZ"]), 3)

        t.work_county = "I WENT TO MECKLENBURG YESTERDAY"
        area_id = rl.route(t) # does *not* match!
        self.assertEquals(area_id, None)

        # the /A regular expression matches any value containing a capital A:
        t.work_state = "AL"
        _, area_id = rl.route(t)
        self.assertEquals(area_id, '123')

        t.work_state = "LA"
        rule, area_id = rl.route(t)
        self.assertEquals(area_id, '123')
        self.assert_(isinstance(rule, routinglist.RoutingListRow))
        self.assertEquals(rule.field_name, 'work_state')
        self.assertEquals(rule.call_center, 'ZZZ')

    def test_Louisianna_routing_list(self):
        self.prepare()
        rl = routinglist.RoutingList(self.tdb, verbose=0)
        tickets = tools.safeglob(os.path.join("..","testdata","sample_tickets","FNL1","FNL1-2009-07-31*.txt"))
        tmp = {}
        for ticket_file in tickets:
            tl = ticketloader.TicketLoader(ticket_file)
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["BatonRouge2"])
            t.ticket_format = "FNL1"

            _, area_id = rl.route(t)
            self.assertTrue(area_id == "15839" or area_id == "15840")



if __name__ == "__main__":

    unittest.main()
