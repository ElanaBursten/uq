# NCA2-2016-03

# Test all possible fields on a single sample ticket
formats: NorthCaliforniaATT2016
source: tickets/NCA2-2016-03/NCA2-test1.txt

- active:
#- alert  varchar(1)  NULL
- call_date: 2016-03-17 11:30:00
- caller: CHRIS BRASSART
- caller_altcontact: UNKNOWN
#- caller_altphone: 614-348-4346
#- caller_cellular: 740-362-3355
#- caller_contact: LISA HART
- caller_email: CBRASSART@USAN.ORG
- caller_fax: 925-798-9506
- caller_phone: 925-798-9504
#- channel: WEB
- company: TEST
- con_address: 4005 PORT CHICAGO HWY
- con_city: CONCORD
- con_name: USA NORTH 811
- con_state: CA
#- con_type: CONT
- con_zip: 94520
#- do_not_mark_before  datetime  NULL
#- do_not_respond_before  datetime  NULL
#- due_date: 2016-03-02 12:37:00
#- duration  varchar(40)  NULL
- explosives: N
#- followup_type_id  int  NULL
#- geocode_precision  int  NULL
#- image  text  NOT NULL
- kind: NORMAL
#- legal_date  datetime  NULL
#- legal_due_date: 2016-03-02 12:37:00
#- legal_good_thru  datetime  NULL
#- legal_restake  varchar(40)  NULL
- map_page: 3800A12202C
#- map_ref  varchar(60)  NULL
#- modified_date  datetime  NOT NULL
#- operator  varchar(40)  NULL
#- parent_ticket_id  int  NULL
#- parse_errors:
~ 0 parsed_ok: 1
- priority: 2
#- recv_manager_id  int  NULL
#- respond_date: 2016-03-02 12:37:00
- revision: 00W
#- route_area_id  int  NULL
- serial_number: 70000008
#- service_area_code  varchar(40)  NULL
#- source: OUPS2
#- status  varchar(20)  NULL
#- ticket_id  int  IDENTITY(1000,1)  NOT NULL
#- ticket_format: OUPS2
- ticket_number: W607790005
- ticket_type: NORM NEW GRID LREQ RSEN-MEET
- transmit_date: 2016-03-18 19:04:37
#- update_of:
#- ward  varchar(10)  NULL
#- watch_and_protect  bit  NULL
- work_address_number:
- work_address_number_2:
- work_address_street: BATES AVE
- work_city: CONCORD
- work_county: CONTRA COSTA
- work_cross: NELSON AVE
- work_date: 2016-03-21 12:00:00
-start- work_description: Street Address: BATES AVE
-end- work_description: N/SI/O BATES AVE FR NELSON AVE TO INDUSTRIAL WAY
#- work_extent  varchar(100)  NULL
~ 6 work_lat: 38.01251
~ 6 work_long: -122.03959
- work_notc: 048 hrs
#- work_priority_id  int  NULL
- work_remarks:
- work_state: CA
#- work_subdivision: CORTONA
- work_type: TEST

#### grids

#### locates, base filters out exclude list
? [l.client_code for l in t.locates]
! ['NDPTST']

? [l._plat for l in t.locates]
! ['CNCRCA01']

# Test various fields on another ticket, modified for NCA2
formats: NorthCaliforniaATT2016
source: tickets/NCA1-2016-03/NCA1-2016-03-30-1-modified-for-NCA2.txt

- active:
#- alert  varchar(1)  NULL
- call_date: 2016-03-17 11:35:00
- caller: CHRIS BRASSART
#- caller_altcontact: TODD SMITH
#- caller_altphone: 614-348-4346
#- caller_cellular: 740-362-3355
#- caller_contact: LISA HART
- caller_email: CBRASSART@USAN.ORG
- caller_fax: 925-798-9506
- caller_phone: 925-798-9504
#- channel: WEB
#- company: ROMANELLI & HUGHES
- con_address: 4005 PORT CHICAGO HWY
- con_city: CONCORD
- con_name: USA NORTH 811
- con_state: CA
#- con_type: CONT
- con_zip: 94520
#- do_not_mark_before  datetime  NULL
#- do_not_respond_before  datetime  NULL
#- due_date: 2016-03-02 12:37:00
#- duration  varchar(40)  NULL
#- explosives: N
#- followup_type_id  int  NULL
#- geocode_precision  int  NULL
#- image  text  NOT NULL
- kind: NORMAL
#- legal_date  datetime  NULL
#- legal_due_date: 2016-03-02 12:37:00
#- legal_good_thru  datetime  NULL
#- legal_restake  varchar(40)  NULL
#- map_page: 4008C8307C
#- map_ref  varchar(60)  NULL
#- modified_date  datetime  NOT NULL
#- operator  varchar(40)  NULL
#- parent_ticket_id  int  NULL
#- parse_errors:
~ 0 parsed_ok: 1
- priority: 1
#- recv_manager_id  int  NULL
#- respond_date: 2016-03-02 12:37:00
- revision: 00W
#- route_area_id  int  NULL
#- serial_number  varchar(40)  NULL
#- service_area_code  varchar(40)  NULL
#- source: OUPS2
#- status  varchar(20)  NULL
#- ticket_id  int  IDENTITY(1000,1)  NOT NULL
#- ticket_format: OUPS2
- ticket_number: W607790008
- ticket_type: SHRT NEW GRID LREQ RSEN-MEET
- transmit_date: 2016-03-30 19:29:31
#- update_of:
#- ward  varchar(10)  NULL
#- watch_and_protect  bit  NULL
#- work_address_number: 4944
#- work_address_number_2:
#- work_address_street: PESARO WAY
#- work_city: DUBLIN
#- work_county: FRANKLIN
#- work_cross: MEMORIAL DR
- work_date: 2016-03-21 12:00:00
#- work_description: PLS MARK WITH PAINT AND FLAGS.  ENTIRE PROPERTY
#- work_extent  varchar(100)  NULL
#~ 6 work_lat: 40.140242
#~ 6 work_long: -83.124117
#- work_notc  varchar(40)  NULL
#- work_priority_id  int  NULL
#- work_remarks:
#- work_state: OH
#- work_subdivision: CORTONA
- work_type: TEST

#### grids

#### locates, base filters out exclude list
? [l.client_code for l in t.locates]
! ['XXXTST']

? [l._plat for l in t.locates]
! ['SNJSCA21']

? t._hp_info
! ['PACBEL: CONDUIT,FIBER,UNDERGROUND,FTTN,FIBER LANDMARK']


# Test some fields that are different from the previous ticket
# Test a modified ticket with some possible variations from sample tickets

# todo(dan) clean this up better than OUPS