# NewJersey8 format, QMAN-3490

formats: NewJersey8
source: tickets/NewJersey8-2014-02/NewJersey-2014-02-05-11-10-45-411-4BKVP.txt

- call_date: 2014-02-04 13:22:00
- transmit_date: 2014-02-04 13:22:00
- ticket_type: ROUTINE
- kind: NORMAL
- ticket_number: 140350942
- work_date: 2014-02-08 07:00:00
- work_county: OCEAN
- work_city: LAKEWOOD
- work_address_number: 0
- work_address_street: CEDAR BRIDGE AVE
- work_cross: GARDEN STATE PKWY
- work_type: ROAD RECONSTRUCTION
- company: NJTA
- caller: JEANETTE CHOJNACKI
- caller_phone: 732-223-9393
- con_name: MIDLANTIC CONSTRUCTION LLC
- con_address: 371 MAIN ST
- con_city: BARNEGAT
- con_state: NJ
- con_zip: 08005
- caller_fax: 732-223-2460
- caller_cellular: 
- caller_email: janaya@midlanticconstllc.com

? [x.client_code for x in t.locates]
! ['NJH']
