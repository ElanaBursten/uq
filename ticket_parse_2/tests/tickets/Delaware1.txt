# Delaware1

source: sample_tickets/FDE1/fde1-2009-11-04-00554.txt
formats: Delaware1

- call_date: 2009-11-02 10:56:00
- caller_altcontact: CELL
- caller_altphone: (302)245-4290
- caller_email: jerry@brakewaterfenceanddeck.com
- caller_phone: (302)684-3333
- company: JERRY LITTLE
- con_name: JERRY LITTLE
- con_state: DE
- kind: NORMAL
- map_page: NEWC 14 E9
- operator: witrai
- ticket_number: 102780036
- transmit_date: 2009-11-04 14:22:00
- work_address_number: 21
- work_address_street: RED SUNSET DR
- work_city: GLASGOW
- work_county: NEW CASTLE
- work_date: 2009-11-05 07:00:00

~ 5 work_lat: 39.60285825
~ 5 work_long: -75.7592163

#
# original source: test_Delaware1()
# (this is an older test)

source: Delaware1-1.txt

- ticket_number: 01069281
- ticket_type: EMERGENCY
- kind: EMERGENCY
- map_page: KENTD 006 J12

? len(t.locates)
! 2
? t._dup_fields
! []

source: fde1-new.txt

~ 5 work_lat: 38.914919
~ 5 work_long: -75.5669125
- map_page: *MORE 36 A10
