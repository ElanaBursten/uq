# Test new due date rule for FMW3.
# Mantis #2739.

formats: VUPSNewFMW3
source: tickets/FMW3-2011-02-25-due-date/FMW3-2011-02-24-00005.txt

- ticket_format: FMW3
- ticket_number: B105500451
- transmit_date: 2011-02-24 09:51:00
- due_date: 2011-03-01 07:00:00

source: tickets/FMW3-2011-02-25-due-date/FMW3-2011-02-24-00006.txt

- ticket_format: FMW3
- ticket_number: A105500505
- transmit_date: 2011-02-24 09:55:00
- due_date: 2011-03-01 07:00:00
