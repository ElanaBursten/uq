# Tests for Washington format
# Taken from test_ticketparser.py
# Also tests Washington4 (which is the same as Washington)

formats: Washington, Washington4
source: Washington-1.txt

- ticket_number: 00357291
- ticket_type: EMERGENCY
- kind: EMERGENCY
- transmit_date: 2002-05-14 10:29:00
- call_date: 2002-05-14 10:26:00
- work_date: 2002-05-14 10:00:00
- operator: ellen.
- work_city: NE
- work_address_number: 1732
- work_address_street: MONTELLO AV NE
- work_cross: MT OLIVE AV NE
- work_type: REPAIRING ELEC SVC
- work_remarks: EMERGENCY ** CREW ON SITE
- work_state: DC
- work_county: NE
- caller_fax: (301)699-7750
- con_name: B FRANK JOY
- caller_contact: ERICA WILSON
- caller_phone: (301)699-6000
- caller_altcontact: ERICA WILSON
- caller_altphone: (301)699-6029
- company: PEPCO
- con_state: DC
- map_page: NE 010 G11


? t.grids
! ["NE 010 G11"]
? t._dup_fields
! []

# parsing of locates changed in Mantis #2773
? [x.client_code for x in t.locates]
! ["TDC01", "WGL07"]

