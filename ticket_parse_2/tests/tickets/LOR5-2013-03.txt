# LOR5-2013-03
# Mantis #3271

formats: Oregon6
source: tickets/LOR5-2013-03-14/LOR5-2013-03-13-15-58-20-552-G2QEI.txt

- map_page: 1N1E32SE
- call_date: 2013-03-13 10:08:00
- caller:
- caller_contact: ERIC BROWN
- caller_phone: (503)781-2857
- company: LARRY THOMAS
- con_address:
- con_city:
- con_name: BOYD BROWN AND SONS INC
- con_state: OR
- con_zip:
- kind: NORMAL
- operator: ortyni
- ticket_number: 13046583
- ticket_type: 48 HOUR NOTICE
- transmit_date: 2013-03-13 10:14:00
- work_address_number: 261
- work_address_street: SW KINGSTON AVE
- work_city: PORTLAND
- work_county: MULTNOMAH
- work_cross: FAIRVIEW
- work_date: 2013-03-15 10:15:00
-start- work_description: ADD IS ON NW CORNER
-end- work_description: KINGSTON AVE.
- work_remarks:
- work_state: OR
- work_type: REPL WATER SRVC LINE

? len(t.locates)
! 1

? t.locates[0].client_code
! 'POTELOR01'

~ 5 work_lat: 45.52154
~ 5 work_long: -122.70689


