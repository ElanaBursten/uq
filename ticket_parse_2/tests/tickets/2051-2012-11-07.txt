# Mantis #3174

formats: SC2051
source: tickets/2051-2012-11-07/2051-2012-11-05-15-20-31-726-K009Q.txt

- ticket_number: 1211051981
- transmit_date: 2012-11-05 15:08:00
- ticket_type: Normal
- kind: NORMAL
- call_date: 2012-11-05 15:15:00
- work_date: 2012-11-08 23:59:00
- con_name: ANSCO & ASSOCIATES, LLC
- con_address: 43 SENTELL ROAD
- con_city: GREENVILLE
- con_state: SC
- con_zip: 29611
- caller_fax: (864) 295-8794
- caller: TAMMY STEGALL
- caller_email: TAMMY.STEGALL@ANSCOLLC.COM
- caller_phone: (864) 295-0235 Ext:
- work_state: SC
- work_county: GREENVILLE
- work_city: GREER
- work_address_number: 101,
- work_address_street: 103 PRESTON DR
- work_cross: LANDRUM DR
- explosives: N
- work_type: TELEPHONE, INSTALL SPLICE PIT
- company: ANSCO
#-start- work_remarks: FROM HWY 1 TO DEER ROAD
#-end- work_remarks: ON THE RIGH
- map_page: 3456B8215A

~ 5 work_lat: 34.94454
~ 6 work_long: -82.266618

? len(t.locates)
! 3

