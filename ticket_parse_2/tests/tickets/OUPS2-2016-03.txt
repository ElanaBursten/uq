# OUPS2-2016-03

# Test all possible fields on a single sample ticket
formats: OUPS2
source: tickets/OUPS2-2016-03/OUPS2-2016-03-09-15-12-43-776-8IJ5M (1).txt

#- active  bit  NOT NULL
#- alert  varchar(1)  NULL
- call_date: 2016-02-29 12:22:00
- caller: LISA HART
- caller_altcontact: TODD SMITH
- caller_altphone: 614-348-4346
- caller_cellular: 740-362-3355
- caller_contact: LISA HART
- caller_email: LHART@SMITHEXC.COM
- caller_fax: 740-362-3022
- caller_phone: 740-362-3355
- channel: WEB
- company: ROMANELLI & HUGHES
- con_address: 7257 ST RT 37 E
- con_city: SUNBURY
- con_name: SMITH & ASSOC. EXCAVATING INC.
- con_state: OH
- con_type: CONT
- con_zip: 43074
#- do_not_mark_before  datetime  NULL
#- do_not_respond_before  datetime  NULL
- due_date:
#- duration  varchar(40)  NULL
- explosives: N
#- followup_type_id  int  NULL
#- geocode_precision  int  NULL
#- image  text  NOT NULL
- kind: NORMAL
#- legal_date  datetime  NULL
- legal_due_date:
#- legal_good_thru  datetime  NULL
#- legal_restake  varchar(40)  NULL
- map_page: 4008C8307C
#- map_ref  varchar(60)  NULL
#- modified_date  datetime  NOT NULL
#- operator  varchar(40)  NULL
#- parent_ticket_id  int  NULL
- parse_errors:
~ 0 parsed_ok: 1
#- priority  varchar(40)  NULL
#- recv_manager_id  int  NULL
- respond_date: 2016-03-02 12:37:00
- revision: 00B
#- route_area_id  int  NULL
#- serial_number  varchar(40)  NULL
#- service_area_code  varchar(40)  NULL
- source: OUPS2
#- status  varchar(20)  NULL
#- ticket_id  int  IDENTITY(1000,1)  NOT NULL
- ticket_format: OUPS2
- ticket_number: B606000313
- ticket_type: ROUT NEW POLY LREQ
- transmit_date: 2016-02-29 12:23:08
- update_of:
#- ward  varchar(10)  NULL
#- watch_and_protect  bit  NULL
- work_address_number: 4944
- work_address_number_2:
- work_address_street: PESARO WAY
- work_city: DUBLIN
- work_county: FRANKLIN
- work_cross: MEMORIAL DR
- work_date: 2016-03-02 12:37:00
- work_description: PLS MARK WITH PAINT AND FLAGS.  ENTIRE PROPERTY
#- work_extent  varchar(100)  NULL
~ 6 work_lat: 40.140242
~ 6 work_long: -83.124117
#- work_notc  varchar(40)  NULL
#- work_priority_id  int  NULL
- work_remarks:
- work_state: OH
- work_subdivision: CORTONA
- work_type: INSTALL WATER LINE

#### grids

#### locates, base filters out exclude list
? [l.client_code for l in t.locates]
! ['CGE', 'AMTP', 'ATBP', 'CSP', 'DBW', 'DUB', 'OBF', 'ODT6', 'WCOP']


# Test some fields that are different from the previous ticket
formats: OUPS2
source: tickets/OUPS2-2016-03/OUPS2-2016-03-09-15-12-43-776-8IJ5M (9).txt

- due_date:
- legal_due_date:
- map_page: 3956B8250D
- parse_errors:
~ 0 parsed_ok: 1
- work_city: COLUMBUS
- work_county: FRANKLIN
- work_description: LOCATE ENTIRE PROPERTY AND ROAD AT THE ABOVE ADDRESS. INCLUDE 40 FEET OFF BACK OF CURB ON OPPOSITE SIDE OF ADDRESS. ADDRESS, PAINT AND FLAG ALL UTILITIES.
- work_state: OH


# Test a 1-line 'Comment'
formats: OUPS2
source: tickets/OUPS2-2016-03/OUPS2-2016-03-09-15-12-43-776-8IJ5M (12).txt

- due_date:
- legal_due_date:
- work_remarks: *AREA IS MARKED IN PINK PAINT


# Test a modified ticket with some possible variations from sample tickets
formats: OUPS2
source: tickets/OUPS2-2016-03/OUPS2-2016-03-09-15-12-43-776-8IJ5M (3) modified.txt

- due_date:
- legal_due_date:
- respond_date:
- update_of: A123456789
- work_address_number: 123
- work_address_number_2: 456789
- work_description:
~ 6 work_lat: 39.974693
~ 6 work_long: -83.816676
- work_remarks: test line 1 aaaaaaaaaaa test line 2 bbbbbbb  test line 4, line 3 was blank
- caller_altphone: (740) 331-0684
- caller_cellular: 937-206-2579
- caller_fax: 111-222-3333
- caller_phone: 777-888-9999

? [l.client_code for l in t.locates]
! ['CGR', 'USS', 'WCOP', 'A09*#-_/&Z', 'CCM', 'URBS', 'WS', 'OP']

formats: OUPS2
source: tickets/OUPS2-2016-03/OUPS2-2016-03-22-10-44-12-634-PFU26.txt

- due_date:
- legal_due_date:
- update_of: A608201262
- work_city: MIFFLIN TWP

? [l.client_code for l in t.locates]
! ['CGET', 'CGE', 'CGEO', 'CPOP', 'CSP', 'FCE', 'FSEP', 'OBF', 'SCL', 'WCOP']

? [l.alert for l in t.locates]
! [None, None, None, 'A', None, None, None, None, None, None]
