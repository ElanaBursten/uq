# OCC4
# Mantis #2865

formats: VUPSNewOCC4
source: tickets/OCC4-2011-09-30/OCC4-2011-09-28-00001.txt

- ticket_number: A127101272
- revision: 00A
- ticket_type: RMRK GRID NORM LREQ RSND
- transmit_date: 2011-09-28 14:31:00
- call_date: 2011-09-28 12:43:00
- work_county: MANASSAS CITY
- work_city: 
- work_state: VA
- work_address_number: 8758
- work_address_street: RICHMOND AVE
- work_cross: FAIRVIEW AVE
- work_type: WATER METER - INSTALL
- company: SAME
- con_name: CITY OF MANASSAS PUBLIC WORKS
- con_address: 8500 PUBLIC WORKS DR
- con_city: MANASSAS
- con_state: VA
- con_zip: 20110
- caller_phone: 703-257-8380
- caller_email: CURTIS.SHORT@CI.MANASSAS.VA.US
- map_page: 3844B7727A-03

? [x.client_code for x in t.locates]
! ['CGV930', 'CMC703', 'DOM400', 'MAN901', 'MAN902', 'MAN903', 'MAN904', 'VZN213']

