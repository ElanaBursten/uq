# New parser for SCA10 (based on SCA1)
# Mantis #3307

source: tickets/SCA10-2013-05-04/SCA1-2013-04-22-10-36-42-200-BNM3B.txt
formats: SouthCalifornia10

- ticket_number: A30290885
- update_of: A30290885
- ticket_type: NORM UPDT GRID
- kind: NORMAL
- ticket_format: SCA10
- transmit_date: 2013-04-22 07:32:52
- call_date: 2013-04-22 07:32:00
- work_state: CA
- work_county: SAN BERNARDINO
- work_city: ONTARIO
- work_address_number: 
- work_address_street: W 5TH ST
- work_cross: N ELDERBERRY AVE
- work_description: WORK CONTINUING
- work_date: 2013-04-22 07:32:00
- work_type: INSTALL WATRR PIPELINE/STORM DRAIN & SEWER
- company: S D DEACON CORP
- con_name: VCI
- caller: JON SAKAKIBARA
- con_address: 1921 W 11TH ST
- con_city: UPLAND
- con_state: CA
- con_zip: 91786
- caller_phone: 909-946-0905
- caller_fax: 909-946-0924
- caller_email: JSAKAKIBARA@VCICOM.COM
- map_page: 0601J053

~ 5 work_lat: 34.08148
~ 5 work_long: -117.67452

? len(t.locates)
! 7
? t.locates[0].client_code
! 'INLEMPUTA'

# Locat:
-start- work_remarks: BOTH SIDES (PROPERTY LINE
-end- work_remarks: APPROXIMATELY 300FT

