# PA7501Meet-2016-07-old-format
formats: PA7501Meet

# Test all possible fields on a 'NEW XCAV MTG' ticket
source: tickets/7501-2016-07-old-format/7501-2016-07-05-08-14-07-927-QJKFA.txt

- active: 
- alert: 
- call_date: 2016-07-05 08:12:00
- caller: FERNANDO VELOSO
#- caller_altcontact: 
- caller_altphone: 610-960-3912
#- caller_cellular: 
- caller_contact: ARTHUR CAUL
- caller_email: FERNANDO@PACTCONSTRUCTION.COM
- caller_fax: 908-788-5780
- caller_phone: 908-788-1985
- channel: 0751WEB 1242
- company: MIDDLETOWN TOWNSHIP
- con_address: PO BOX 74
- con_city: RINGOES
- con_name: PACT CONSTRUCTION INC
- con_state: NJ
#- con_type: 
- con_zip: 08551
#- do_not_mark_before: 
#- do_not_respond_before: 
#- do_not_route_until: 
- due_date: 
- duration: 
- explosives: N
#- followup_type_id: 
#- geocode_precision: 
-start- image: JY     00005 POCS 07/05/16 08:13:16 20161870255-000 NEW  XCAV MTG
-end- image: ========== Copyright (c) 2016 by Pennsylvania One Call System, Inc. ==========
- kind: NORMAL
#- legal_date: 
- legal_due_date: 
#- legal_good_thru: 
#- legal_restake: 
#- map_page: 
#- map_ref: 
#- modified_date: 
- operator: fcveloso
#- parent_ticket_id: 
- parse_errors: 
~ 0 parsed_ok: 1.0
#- priority: 
#- recv_manager_id: 
- respond_date: 2016-07-12 00:00:00
- revision: 000
#- route_area_id: 
- serial_number: 20161870255
#- service_area_code: 
- source: 7501
#- status: 
- ticket_format: 7501
#- ticket_id: 
- ticket_number: 20161870255
- ticket_type: NEW XCAV MTG
- transmit_date: 2016-07-05 08:13:16
#- update_of: 
- ward: 
#- watch_and_protect: 
#- work_address_number: 
#- work_address_number_2: 
- work_address_street: 
- work_city: BROOKHAVEN BORO
- work_county: DELAWARE
- work_cross: 
- work_date: 2016-07-13 08:00:00
- work_description: SANITARY SEWER FORCE MAIN INSTALLATION BETWEEN CHESTER CREEK ROAD AND STATE ROUTE 352
- work_extent: 
~ 6 work_lat: 0.0
~ 6 work_long: 0.0
#- work_notc: 
#- work_priority_id: 
-start- work_remarks: *************===ADDITIONAL MEETING INFORMATION===*************
-end- work_remarks:       RESPONSE CAN BE DETERMINED FROM THE SCOPE OF WORK.
- work_state: PA
- work_subdivision: 
- work_type: OPEN CUT/HORIZONTAL DIRECTIONAL DRILLING

? [l.client_code for l in t.locates]
! ['ACW', 'AML', 'BHH', 'CD', 'CR', 'HT', 'IA', 'JY', 'KE', 'MD1', 'SH', 'SP', 'SPE', 'WBC', 'YI', 'KEG']
