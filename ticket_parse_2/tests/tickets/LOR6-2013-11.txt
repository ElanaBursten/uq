# LOR6 tickets, Oregon/Portland
# Mantis #3438

formats: Oregon6
source: tickets/LOR6-2013-11-09/LOR1-2013-11-05-17-34-12-200-V4SEL.txt

- map_page: 1S2E16SWSE
- call_date: 2013-11-05 14:27:00
- caller:
- caller_contact: CAROL LAFATA
- caller_phone: (503)735-0445
- company: NW REALTY SIGN
- con_address:
- con_city:
- con_name: NW REALTY SIGN
- con_state: OR
- con_zip:
- kind: NORMAL
- operator: orandr
- ticket_number: 13242963
- ticket_type: 48 HOUR NOTICE
- transmit_date: 2013-11-05 14:33:00
- work_address_number: 8910
- work_address_street: SE CLAYBOURNE STREET
- work_city: PORTLAND
- work_county: MULTNOMAH
- work_cross: 
- work_date: 2013-11-07 14:30:00
-start- work_description: LOCATE ONLY WITHIN 3 FEET
-end- work_description: UNLESS CONFLICT FOUND.
- work_remarks:
- work_state: OR
- work_type: SIGN INSTALL

# we only read the term id at the top of the ticket
? len(t.locates)
! 1

? [x.client_code for x in t.locates]
! ['PGE01']

~ 5 work_lat: 45.47475
~ 5 work_long: -122.57121


