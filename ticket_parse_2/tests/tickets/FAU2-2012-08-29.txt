# FAU2 (AlabamaNew) format changes, scheduled for 2012-08-29

formats: AlabamaNew
source: tickets/FAU2-2012-08-29/TEST-2012-08-21-11-22-02-828-Z53WB.txt

- ticket_number: 122340001
- transmit_date: 2012-08-21 10:18:00
- ticket_type: Test
- kind: NORMAL
- call_date: 2012-08-21 10:20:00
- work_date: 2012-08-23 10:30:00
- con_name: Matt Brown
- con_address: 401 St charles St
- con_city: homewood
- con_state: AL
- con_zip: 35209
- caller_fax:
- caller: Matt Brown
- caller_email: mbrown@al811.com
- caller_phone: (251) 367-3613
- work_state: AL
- work_county: JEFFERSON
- work_city: BIRMINGHAM
- work_address_number: 3311
- work_address_street: Highland dr
- work_cross: Clairmont Ave
- explosives: N
- work_type: Other
- company: Matt Brown
- work_description: 
-start- work_remarks: Testing output formats
-end- work_remarks: please do not locate

? [x.client_code for x in t.locates]
! ['TEST01']

source: tickets/FAU2-2012-08-29/TEST-2012-08-21-11-26-02-851-SHKXM.txt

? [x.client_code for x in t.locates]
! ['APC1CL', 'BHWT01', 'SCBH01', 'CHAL01', 'FULG01', 'TEST01']


