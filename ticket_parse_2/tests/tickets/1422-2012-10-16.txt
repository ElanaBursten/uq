# Mantis #2977

formats: SC1422A
source: tickets/1422-2012-10-12/1422-2012-10-10-14-10-11-717-4H9KO.txt

- ticket_number: 1210100298
- transmit_date: 2012-10-10 13:31:00
- ticket_type: Normal
- kind: NORMAL
- call_date: 2012-10-10 14:08:00
- work_date: 2012-10-15 23:59:00
- con_name: MID-CAROLINA ELECTRIC CO-OP
- con_address: 254 LONGS POND RD
- con_city: LEXINGTON
- con_state: SC
- con_zip: 29072
- caller_fax:
- caller: LEW DUBOSE
- caller_email: dispatchgroup@mcecoop.com
- caller_phone: (803) 749-6440 Ext:
- work_state: SC
- work_county: RICHLAND
- work_city: COLUMBIA
- work_address_number: 810
- work_address_street: DUTCH SQUARE BLVD
- work_cross: UNKNOWN
- explosives: N
- work_type: ELECTRIC, RELOCATE SECONDARY
- company: MID-CAROLINA ELECTRIC CO-OP
- work_remarks: 
-start- work_description: MARK THE LEFT SIDE OF
-end- work_description: TESTING TESTING
- map_page: 3402D8105B
# derived from lat/long

? len(t.locates)
! 5

# Mantis #3151
source: tickets/1422-2012-10-12/1422-2012-10-22-16-13-58-125-V69U0.txt
? len(t.locates)
! 5

source: tickets/1422-2012-10-12/1422-2012-10-10-14-18-11-068-BW4AI.txt
- ticket_number: 1210100301
- update_of: 1210100298



