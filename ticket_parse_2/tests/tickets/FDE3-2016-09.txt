# FDE3-2016-09

source: tickets/FDE3-2016-09/FDE3-2016-09-29-13-28-02-980-M3PL5.txt.001
formats: Delaware3

- active: 
#- alert: 
#- boring: 
- call_date: 2016-09-29 12:54:00
- caller: BILL WEISEL
- caller_altcontact: FRANK CINI
- caller_altphone: (302)256-1108
#- caller_cellular: 
- caller_contact: BILL WEISEL
- caller_email: missutility@nccde.org
- caller_fax: (302)395-5860
- caller_phone: (302)395-5700
#- channel: 
- company: NEW CASTLE COUNTY
- con_address: 187 #A OLD CHURCHMANS RD
- con_city: NEW CASTLE
- con_name: NEW CASTLE PUBLIC WORKS
- con_state: DE
#- con_type: 
- con_zip: 19720
#- do_not_mark_before: 
#- do_not_respond_before: 
#- do_not_route_until: 
#- due_date: 
#- duration: 
- explosives: N
#- followup_type_id: 
#- geocode_precision: 
-start- image: KORTERRA JOB 00322-SMTP-1 GT162730390 Seq: 1 09/29/2016 13:26:59
#-end- image: 
- kind: NORMAL
#- legal_date: 
#- legal_due_date: 
#- legal_good_thru: 
#- legal_restake: 
- map_page: 3947C7528C
#- map_ref: 
#- modified_date: 
- operator: webusr
#- parent_ticket_id: 
- parse_errors: 
~ 0 parsed_ok: 1.0
#- priority: 
#- recv_manager_id: 
- respond_date: 2016-10-04 07:00:00
#- revision: 
#- route_area_id: 
#- serial_number: 
#- service_area_code: 
- source: FDE3
#- status: 
- ticket_format: FDE3
#- ticket_id: 
- ticket_number: 162730390
- ticket_type: -
- transmit_date: 2016-09-29 12:56:00
#- update_of: 
#- ward: 
#- watch_and_protect: 
#- wo_number: 
- work_address_number: 21
- work_address_number_2: 
- work_address_street: ROLLING RD
- work_city: CLAYMONT
- work_county: NEW CASTLE
- work_cross: GARRETT RD
- work_date: 2016-10-04 07:00:00
- work_description: LOCATE/MARK: MARK UTILITIES IN FRONT OF 21 ROLLING ROAD FOR CURB REMOVAL. AREA IS MARKED WITH WHITE PAINT. ANY QUESTIONS OR COMMENTS, CALL FRANK CINI AT 302-256-1108 CLICK HERE TO SEE THE TICKET MAPPING FOR DPUG09: http://de.itic.occinc.com/Z47Z-2ZH-R62-DZ9
#- work_extent: 
~ 6 work_lat: 39.788333
~ 6 work_long: -75.471666
#- work_notc: 
#- work_priority_id: 
- work_remarks: WO# 116430
- work_state: DE
- work_subdivision: ROLLING PARK
- work_type: REMOVING THE CURB

? [l.client_code for l in t.locates]
! ['DPUG09']
