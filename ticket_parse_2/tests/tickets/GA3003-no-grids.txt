# Mantis 3009
# When no grids are listed, map_page should be empty, not parse
# the next field ("Work")

formats: GA3003, GA3004

source: tickets/3003-2012-04-28-no-grids/3003-2012-04-26-00173.txt
- ticket_number: 04262-300-073
- ticket_type: NORMAL
- revision: 000
- update_of:
- map_page:

