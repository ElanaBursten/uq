# Mantis #2911: OCC4 switching to XML format

formats: OCC4XML

source: tickets/OCC4-2011-12-21-xml/OCC2-2011-12-14-01040.txt

- transmit_date: 2011-12-14 16:00:17
- ticket_number: B134801346
- work_state: VA
- call_date: 2011-12-14 15:56:05
- ticket_type: NORM NEW GRID LREQ
- kind: NORMAL
- work_date: 2012-01-04 23:59:59
- work_county: FAIRFAX
- work_city: 
- work_address_number: 7816
- work_address_street: WOLF RUN SHOALS RD
- work_cross: HENDERSON RD
- work_type: CONSTRUCTION - REPAIR FOUNDATION / WATERPROOFING
- company: HO ERIC
- explosives: N
- con_name: HOMELAND EXCAVATIONS
- con_address: 8501 MCGRATH RD
- con_city: MANASSAS
- con_state: VA
- con_zip: 20112
- caller_phone: 5719217683
- caller_fax: 
- caller_contact: DAN SMITH
- caller_email: homelandexcavations@msn.com
- work_remarks: CALLER MAP REF: NONE
- map_page: 3844B7720B-14
  # same as first grid


? len(t.grids)
! 7

? t.grids[0]
! '3844B7720B'

? len(t.locates)
! 5

? t.locates[0].client_code
! 'COX609'

? t.locates[-1].client_code
! 'WGL904'

-start- work_description: COME UP THE LONG DRIVEWAY
-end- work_description: SIDE OF THE HOUSE