# QMANTWO-122

formats: NC2101

# Test a cancellation notice, including all available fields on the ticket
source: tickets/2101-2016-01/2101-2016-01-12-16-49-43-775-7X0E2.txt

- ticket_number: C160121965
- update_of:
- call_date: 2016-01-12 16:46:00
- operator: BIJ
- channel: 999
- revision: 01C
- ticket_type: NORM CNCL GRID LR
- work_state: NC
- work_county: MECKLENBURG
- work_city: CHARLOTTE
- work_subdivision:
- work_address_number:
- work_address_street: MERTON WOOD LN
- work_cross: FRESHWELL RD
- work_type: INSTALL FIBER / LIGHT DIGGING
- work_date: 2016-01-16 00:00:00
- work_notc: 72/72
- priority: NORM
- duration: 2 DAYS
- company: ATT/ ANSCO
- con_name: BECLOP SERVICES LLC
- con_type: CONT
- con_address: 403 W JEFFERSON ST
- con_city: MONROE
- con_state: NC
- con_zip: 28112
- explosives: N
- caller: OSCAR LOPEZ
- caller_phone: 704-441-0442
- caller_email: osmarlo4@gmail.com
- caller_cellular:
- transmit_date: 2016-01-12 16:47:30
- caller_contact: SAME
- map_page: 3505A8058C
-start- work_description: LOCATE BY 11:59 PM ON 01/15/2016
-end- work_description: IS ABOUT 400 FT
-start- work_remarks: REASON FOR CANCEL: STREET IS INCORRECT
-end- work_remarks: PM CANCELLED BY BIJ
~ 6 work_lat: 35.1
~ 6 work_long: -80.970833
- kind: NORMAL

? [locate.client_code for locate in t.locates]
! ['ATT331', 'CHA01', 'CHA02', 'CLS01', 'CSR02', 'DPC1L', 'OCL01', 'PNG02', 'TWC02']

? [grid for grid in t.grids]
! ['3505A8058C', '3505A8058D', '3506D8058C', '3506D8058D']
