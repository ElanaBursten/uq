# test_emailresponder_with_receiver.py

import copy
import os
import unittest
#
import datadir
import backend_tst_case
import tst_sql


def setup_module():
    backend_tst_case.setup_module()


class TestEmailResponderWithReceiver(backend_tst_case.BackendTestCase):
    data_dir = os.path.join(datadir.datadir.path, '')

    process_config = """
        <process
            incoming="{datadir}in"
            processed="{datadir}proc"
            error="{datadir}error"
            format="NCA2"
        />
        <process
            incoming="{datadir}in_2"
            processed="{datadir}proc_2"
            error="{datadir}error_2"
            format="SCA6"
        />""".format(datadir=data_dir)

    email_responder_config = """
        <callcenter
            name="NCA1"
            resp_email="admin@test.com"
            sender="UQ Email Responder &lt;fred@bellsouth.net&gt;"
            subject="UQ Email Responses"
            tech_code="UTI"
            all_versions="0"
            ack_dir="{datadir}ack_in"
            ack_proc_dir="{datadir}ack_proc">
            <clients>
                <term name="PACBEL"/>
                <term name="DEF02"/>
            </clients>
            <skip>
                <term name="GHI03"/>
            </skip>
            <code_mappings>
                <mapping uq_code="C" response="3010"/>
                <mapping uq_code="M" response="3011"/>
            </code_mappings>
            <account
                server="test_pop3"
                username="nca2_user"
                password="xxxxxxxxxxx"
                ignore_attachments="1" />
        </callcenter>
        <callcenter
            name="SCA1"
            resp_email="admin@test.com"
            sender="UQ Email Responder &lt;fred@bellsouth.net&gt;"
            subject="UQ Email Responses"
            tech_code="302"
            all_versions="0"
            ack_dir="{datadir}ack_in_2"
            ack_proc_dir="{datadir}ack_proc_2">
            <clients>
                <term name="ATTDSOUTH" />
                <term name="DEF02" />
            </clients>
            <skip>
                <term name="GHI03" />
            </skip>
            <code_mappings>
                <mapping uq_code="C" response="3010" />
                <mapping uq_code="M" response="3011" />
            </code_mappings>
            <!-- sca1 account list -->
        </callcenter>""".format(datadir=data_dir)

    sca1_account_list = """
        <account
            server="test_pop3"
            username="sca6_user"
            password="xxxxxxxxxxx"
            ignore_attachments="1" />
        <account
            server="test_pop3_2"
            username="sca6_user_2"
            password="xxxxxxxxxxx"
            ignore_attachments="1" />"""

    expected_config_001 = {
        'SCA1': {
            'status_code_skip': [],
            'clients': ['ATTDSOUTH', 'DEF02'],
            'sender': 'UQ Email Responder <fred@bellsouth.net>',
            'all_versions': 0,
            'send_emergencies': 1,
            'translations': {},
            'parsed_locates_only': 1,
            'ack_dir': data_dir + 'ack_in_2',
            'tech_code': '302',
            'ack_proc_dir': data_dir + 'ack_proc_2',
            'accounts': [
                {'username': 'sca6_user',
                 'password': 'xxxxxxxxxxx',
                 'ignore_attachments': True,
                 'aggregate_file': '',
                 'destination': data_dir + 'ack_in_2',
                 'server': 'test_pop3',
                 'attachment_dir': '',
                 'full': False,
                 'store_attachments': False,
                 'use_ssl': False,
                 'file_ext': '',
                 'call_center': 'SCA1',
                 'type': 'email',
                 'port': 110},
                {'username': 'sca6_user_2',
                 'password': 'xxxxxxxxxxx',
                 'ignore_attachments': True,
                 'aggregate_file': '',
                 'destination': data_dir + 'ack_in_2',
                 'server': 'test_pop3_2',
                 'attachment_dir': '',
                 'full': False,
                 'store_attachments': False,
                 'use_ssl': False,
                 'file_ext': '',
                 'call_center': 'SCA1',
                 'type': 'email',
                 'port': 110}
            ],
            'resp_email': 'admin@test.com',
            'skip': ['GHI03'],
            'send_3hour': 1,
            'mappings': {
                'C': ('3010', ''),
                'M': ('3011', '')},
            'subject': 'UQ Email Responses',
            'name': 'SCA1'},
        'NCA1': {
            'status_code_skip': [],
            'clients': ['PACBEL', 'DEF02'],
            'sender': 'UQ Email Responder <fred@bellsouth.net>',
            'all_versions': 0,
            'send_emergencies': 1,
            'translations': {},
            'parsed_locates_only': 1,
            'ack_dir': data_dir + 'ack_in',
            'tech_code': 'UTI',
            'ack_proc_dir': data_dir + 'ack_proc',
            'accounts': [
                {'username': 'nca2_user',
                 'password': 'xxxxxxxxxxx',
                 'ignore_attachments': True,
                 'aggregate_file': '',
                 'destination': data_dir + 'ack_in',
                 'server': 'test_pop3',
                 'attachment_dir': '',
                 'full': False,
                 'store_attachments': False,
                 'use_ssl': False,
                 'file_ext': '',
                 'call_center': 'NCA1',
                 'type': 'email',
                 'port': 110}
            ],
            'resp_email': 'admin@test.com',
            'skip': ['GHI03'],
            'send_3hour': 1,
            'mappings': {
                'C': ('3010', ''),
                'M': ('3011', '')},
            'subject': 'UQ Email Responses', 'name': 'NCA1'
        }
    }

    expected_config_002 = copy.deepcopy(expected_config_001)
    expected_config_002['SCA1']['accounts'] = []

    def test_SCA1_NCA1_acks(self):
        """ Test various ack processing cases, using SCA1 and NCA1 """
        # Set up and verify config
        self.test_config.add_section('ticketparser/process list', self.process_config)
        self.test_config.add_section('responder/email/callcenter list',
                                     self.email_responder_config)
        self.test_config.add_section('sca1 account list', self.sca1_account_list)
        self.test_config.save()

        self.verify_email_responders_config(self.expected_config_001)

        # Add ticket files, and parse in. ticket_number/serial_number's:
        # NCA2: 0326829/26470173, 0288777/26470177, 0326832/26470179
        # SCA6: A22790737/25656255, A22790739/25656257, A22790740/25656259
        self.add_incoming_files('tickets/NCA2-2013-08-28-qtrmin',
                                ['NCA2-2013-08-22-17-49-03-211-QSOCV.txt',
                                 'NCA2-2013-08-22-17-52-27-448-HLPQT.txt',
                                 'NCA2-2013-08-22-17-52-28-073-03QRQ.txt'])
        self.add_incoming_files('tickets/SCA6-2012-10-17-hp',
                                ['sca6-2012-10-05-21-06-08-240-GJTD2.txt',
                                 'sca6-2012-10-05-21-06-08-272-FWEAI.txt',
                                 'sca6-2012-10-05-21-08-05-928-AY1JQ.txt'],
                                'in_2')

        self.run_parser('NCA2')
        self.run_parser('SCA6')

        # Status locates
        self.status_ticket('0326829', 'PACBEL', 'M')
        self.status_ticket('0288777', 'PACBEL', 'C')
        self.status_ticket('0326832', 'PACBEL', 'M')
        self.status_ticket('A22790737', 'ATTDSOUTH', 'M')
        self.status_ticket('A22790739', 'ATTDSOUTH', 'C')
        self.status_ticket('A22790740', 'ATTDSOUTH', 'M')

        # Add email accounts
        self.add_pop3_account('test_pop3', 'nca2_user')
        self.add_pop3_account('test_pop3', 'sca6_user')
        self.add_pop3_account('test_pop3_2', 'sca6_user_2')

        # Respond for tickets. This also tests 0 ack messages waiting.
        self.run_email_responders()

        # Add acks. serial_numbers:
        # NCA2 001: 26470173, 99993645, 26470177 (99993645 doesn't exist)
        # SCA6 001: 25656255; 002: spam; 003: 25656257; 00001: 25656259
        self.add_pop3_message_files('receiver\NCA2-2014-test',
                                    ['Close Out Response test-001.eml'],
                                    'test_pop3', 'nca2_user')
        self.add_pop3_message_files('receiver\SCA6-2014-test',
                                    ['Close Out Response test-001.eml',
                                     'Close Out Response test-002.eml'],
                                    'test_pop3', 'sca6_user')
        self.add_pop3_message_files('receiver\SCA6-2014-test',
                                    ['Close Out Response test-003.eml'],
                                    'test_pop3_2', 'sca6_user_2')

        self.add_incoming_files('responder_ack\SCA6', ['sca6-test-00001.txt'], 'ack_in_2')

        # Verify ack input
        self.verify_results(
            tst_sql.response_ack_sql,
            ['ticket_number', 'client_code', 'status', 'success', 'response_sent',
             'reply', 'reference_number'],
            [['0288777', 'PACBEL', 'C', '0', '3010', '(email waiting)', '26470177'],
             ['0326829', 'PACBEL', 'M', '0', '3011', '(email waiting)', '26470173'],
             ['0326832', 'PACBEL', 'M', '0', '3011', '(email waiting)', '26470179'],
             ['A22790737', 'ATTDSOUTH', 'M', '0', '3011', '(email waiting)', '25656255'],
             ['A22790739', 'ATTDSOUTH', 'C', '0', '3010', '(email waiting)', '25656257'],
             ['A22790740', 'ATTDSOUTH', 'M', '0', '3011', '(email waiting)', '25656259']])

        # Process acks
        self.run_email_responders()

        # Verify ack output
        self.verify_results(
            tst_sql.response_ack_sql,
            ['ticket_number', 'client_code', 'status', 'success', 'response_sent',
             'reply', 'reference_number'],
            [['0288777', 'PACBEL', 'C', '1', '3010', 'acknowledged', None],
             ['0326829', 'PACBEL', 'M', '1', '3011', 'acknowledged', None],
             ['0326832', 'PACBEL', 'M', '0', '3011', '(email waiting)', '26470179'],
             ['A22790737', 'ATTDSOUTH', 'M', '1', '3011', 'acknowledged', None],
             ['A22790739', 'ATTDSOUTH', 'C', '1', '3010', 'acknowledged', None],
             ['A22790740', 'ATTDSOUTH', 'M', '1', '3011', 'acknowledged', None]])

    def test_NCA1_acks(self):
        """ Test additional ack processing cases, using only NCA1 """
        # Set up and verify config
        self.test_config.add_section('ticketparser/process list', self.process_config)
        self.test_config.add_section('responder/email/callcenter list',
                                     self.email_responder_config)
        self.test_config.save()

        self.verify_email_responders_config(self.expected_config_002)

        # Add ticket files, and parse in. ticket_number/serial_number's:
        # 0326829/26470173, 0288777/26470177, 0326832/26470179
        self.add_incoming_files('tickets/NCA2-2013-08-28-qtrmin',
                                ['NCA2-2013-08-22-17-49-03-211-QSOCV.txt',
                                 'NCA2-2013-08-22-17-52-27-448-HLPQT.txt',
                                 'NCA2-2013-08-22-17-52-28-073-03QRQ.txt'])

        self.run_parser('NCA2')

        # Status locates
        self.status_ticket('0326829', 'PACBEL', 'M')
        self.status_ticket('0288777', 'PACBEL', 'C')
        self.status_ticket('0326832', 'PACBEL', 'M')

        # Add email accounts
        self.add_pop3_account('test_pop3', 'nca2_user')

        # Respond for tickets
        self.run_email_responders()

        # Add acks. serial_number's:
        # 001: 26470173, 99993645, 26470177 (99993645 doesn't exist); 002: blank message
        self.add_pop3_message_files('receiver\NCA2-2014-test',
                                    ['Close Out Response test-001.eml',
                                     'Close Out Response test-002.eml'],
                                    'test_pop3', 'nca2_user')

        # Verify ack input
        self.verify_results(
            tst_sql.response_ack_sql,
            ['ticket_number', 'client_code', 'status', 'success', 'response_sent',
             'reply', 'reference_number'],
            [['0288777', 'PACBEL', 'C', '0', '3010', '(email waiting)', '26470177'],
             ['0326829', 'PACBEL', 'M', '0', '3011', '(email waiting)', '26470173'],
             ['0326832', 'PACBEL', 'M', '0', '3011', '(email waiting)', '26470179']])

        # Process acks
        self.run_email_responders()

        # Verify ack output/input
        self.verify_results(
            tst_sql.response_ack_sql,
            ['ticket_number', 'client_code', 'status', 'success', 'response_sent',
             'reply', 'reference_number'],
            [['0288777', 'PACBEL', 'C', '1', '3010', 'acknowledged', None],
             ['0326829', 'PACBEL', 'M', '1', '3011', 'acknowledged', None],
             ['0326832', 'PACBEL', 'M', '0', '3011', '(email waiting)', '26470179']])

        # Add acks. serial_number's:
        # 00001: 26470173, 99993645, 26470177 (reprocessing acks from last run)
        # 00002: 26470179
        # 00003: 26470173, 99993645, 26470177 (typo in line 2)
        # 00004: empty file
        self.add_incoming_files('responder_ack\NCA2',
                                ['nca2-test-00001.txt', 'nca2-test-00002.txt',
                                 'nca2-test-00003.txt', 'nca2-test-00004.txt'],
                                'ack_in')

        # Process acks
        self.run_email_responders()

        # Verify ack output
        self.verify_results(
            tst_sql.response_ack_sql,
            ['ticket_number', 'client_code', 'status', 'success', 'response_sent',
             'reply', 'reference_number'],
            [['0288777', 'PACBEL', 'C', '1', '3010', 'acknowledged', None],
             ['0326829', 'PACBEL', 'M', '1', '3011', 'acknowledged', None],
             ['0326832', 'PACBEL', 'M', '1', '3011', 'acknowledged', None]])

if __name__ == "__main__":
    unittest.main()
