# test_ticketrouter_screening.py
# XXX try time zone code again with a different zone for the server, say,
# Central Time, and make sure everything is still correct

from __future__ import with_statement
import StringIO
import os
import unittest
#
import _testprep
import call_centers
import config
import datadir
import date
import locate_status
import mail2
import ticket_db
import ticketloader
import ticketparser
import ticketrouter
from mockery import *

class TestTicketRouterScreening(unittest.TestCase):

    def setUp(self):
        mail2.TESTING = 1
        mail2._test_mail_sent = []

        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        # empty tables so we won't have any accidental updates of existing
        # tickets where we expected an insert, etc.
        _testprep.clear_database(self.tdb)
        # the USW01 client is no longer active in the database, make it active
        sql = "select call_center "\
         "from client where oc_code = 'USW01'"
        self.result_USW01 = self.tdb.runsql_result(sql)[0]
        _testprep.set_client_active(self.tdb, self.result_USW01['call_center'],
          "USW01")
        # Delete the routing error file
        BUGFILE = "routing_error_skip.txt"
        if datadir.datadir.exists(BUGFILE):
            datadir.datadir.remove(BUGFILE)

        _testprep.set_cc_routing_file()
        _testprep.set_client_active(self.tdb, 'FCV2', 'ACR441')

        # set correct time zones for NCA1, NCA2, SCA1
        cc = call_centers.get_call_centers(self.tdb)
        for cc_name in ['NCA1', 'NCA2', 'SCA1']:
            d = cc.get(cc_name)
            d['timezone_code'] = 'PST/PDT'

    def tearDown(self):
        # Set it back inactive
        _testprep.set_client_inactive(self.tdb,
          self.result_USW01['call_center'], "USW01")

    #
    # Mantis 2319

    def test_screen_1(self, timezone='EST/EDT'):
        '''
        Tests screening process, Mantis 2319
        '''
        ref = self.tdb.getrecords('reference',ref_id='8')[0]
        modifier_orig = ref['modifier']
        if 'SCR' not in modifier_orig:
            self.tdb.updaterecord('reference', 'ref_id', 8,
             modifier=modifier_orig+',SCR')
        try:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "sample_tickets", "NCA1", "nca1-NORMAL NOTICE RENEWAL - NO.txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["NorthCalifornia"])
            ticket_id = self.tdb.insertticket(t)

            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.log.logger.logfile = StringIO.StringIO()

            tr.log.lock = 1
            tr.run(runonce=1)
            log = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()
            locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
            stats = [locate_status.not_a_customer,
                     locate_status.not_a_customer,
                     locate_status.screened,
                     locate_status.not_a_customer,
                     locate_status.not_a_customer,
                     locate_status.screened,
                     locate_status.not_a_customer,
                     locate_status.not_a_customer]

            for loc,stat in zip(locates,stats):
                self.assertEquals(loc['status'],stat)
                if stat == locate_status.screened:
                    locate_id = int(loc["locate_id"])
                    s = "%s: ticket %s/%s, locate %s/%s screened" % (
                        t.ticket_format, t.ticket_number, ticket_id,
                        loc["client_code"], locate_id)
                    self.assertTrue(s in log)
                    self.assertEquals(loc['closed'], '1')
                    self.assertEquals(loc['closed_how'], 'router')
                    self.assertEquals(loc['closed_by_id'], '9063')

                    diff = date.Date(loc['closed_date']).diff(date.Date())
                    # California; expecting a 3 hour diff, with an error
                    # margin of ~60s (Mantis #2856)
                    hours_diff = abs(diff / 3600)
                    self.assertTrue(abs(abs(diff) - 60*60*hours_diff) < 60.0)

                    Locate_Status = self.tdb.getrecords('locate_status', locate_id=locate_id)
                    diff = date.Date(Locate_Status[-1]['status_date']).diff(date.Date())
                    # California; expecting a 3 hour diff, with an error
                    # margin of ~60s (Mantis #2856)
                    hours_dff = diff/3600
                    self.assertTrue(abs(abs(diff) - 60*60*hours_diff) < 60.0)
                    self.assertEquals(Locate_Status[-1]['qty_marked'],'1')
                    self.assertEquals(Locate_Status[-1]['statused_by'], '9063')
                    Locate_Snap = self.tdb.getrecords('locate_snap', locate_id=locate_id)
                    diff = date.Date(Locate_Snap[-1]['first_close_date']).diff(date.Date())
                    # California; expecting a 3 hour diff, with an error
                    # margin of ~60s (Mantis #2856)
                    hours_dff = abs(diff / 3600)
                    self.assertTrue(abs(abs(diff) - 60*60*hours_diff) < 60.0)
                    self.assertEquals(Locate_Snap[-1]['first_close_emp_id'],
                     '9063')
                    #self.assertEquals(Locate_Snap[-1]['first_close_pc'], 'FFA')
                    tsnap = self.tdb.getrecords('ticket_snap', ticket_id=ticket_id)
                    diff = date.Date(tsnap[-1]['first_close_date']).diff(date.Date())
                    diff_hz = abs(diff / 3600)
                    # California; expecting a 3 hour diff, with an error
                    # margin of ~60s (Mantis #2856)
                    self.assertTrue(abs(abs(diff) - 60*60*diff_hz) < 60.0)
                    self.assertEquals(tsnap[-1]['first_close_emp_id'], '9063')
                    #self.assertEquals(Ticket_Snap[-1]['first_close_pc'], 'FFA')
        finally:
            if 'SCR' not in modifier_orig:
                self.tdb.updaterecord('reference', 'ref_id', 8,
                 modifier=modifier_orig)

    def test_screen_1a(self, timezone='CST/CDT'):
        self.test_screen_1(timezone)

    def test_screen_2(self):
        '''
        Tests screening process, Mantis 2319
        '''
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "NCA1", "nca1-NORMAL NOTICE RENEWAL - NO.txt"))
        raw = tl.getTicket()
        raw = raw.replace("To Re-mark Their Facilities: N",
                          "To Re-mark Their Facilities: Y")
        t = self.handler.parse(raw, ["NorthCalifornia"])
        ticket_id = self.tdb.insertticket(t)

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.log.logger.logfile = StringIO.StringIO()

        tr.log.lock = 1
        tr.run(runonce=1)
        log = tr.log.logger.logfile.getvalue()
        tr.log.logger.logfile.close()
        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        stats = [locate_status.not_a_customer,
                 locate_status.not_a_customer,
                 locate_status.assigned,
                 locate_status.not_a_customer,
                 locate_status.not_a_customer,
                 locate_status.assigned,
                 locate_status.not_a_customer,
                 locate_status.not_a_customer]
        for loc,stat in zip(locates,stats):
            self.assertEquals(loc['status'],stat)

    def test_screen_3(self):
        '''
        Tests screening process, Mantis 2319
        '''
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets","NCA1","nca1-NORMAL NOTICE - NO.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCalifornia"])
        ticket_id = self.tdb.insertticket(t)

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.log.logger.logfile = StringIO.StringIO()

        tr.log.lock = 1
        tr.run(runonce=1)
        log = tr.log.logger.logfile.getvalue()
        tr.log.logger.logfile.close()
        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        stats = [locate_status.not_a_customer,
                 locate_status.assigned,
                 locate_status.not_a_customer,
                 locate_status.not_a_customer,
                 locate_status.not_a_customer,
                 locate_status.assigned,
                 locate_status.not_a_customer]
        for loc,stat in zip(locates,stats):
            self.assertEquals(loc['status'],stat)

    def test_screen_4(self):
        '''
        Tests screening process, Mantis 2319
        '''
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets","NCA1","nca1-FOLLOW UP - NO.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["NorthCalifornia"])
        ticket_id = self.tdb.insertticket(t)

        tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
        tr.log.logger.logfile = StringIO.StringIO()

        tr.log.lock = 1
        tr.run(runonce=1)
        log = tr.log.logger.logfile.getvalue()
        tr.log.logger.logfile.close()
        locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
        stats = [locate_status.assigned,
                 locate_status.not_a_customer,
                 locate_status.assigned,
                 locate_status.not_a_customer]
        for loc,stat in zip(locates,stats):
            self.assertEquals(loc['status'],stat)

    def test_screen_5(self):
        '''
        Tests screening process, Mantis 2319
        '''
        ref = self.tdb.getrecords('reference',ref_id='8')[0]
        modifier_orig = ref['modifier']
        if 'SCR' not in modifier_orig:
            self.tdb.updaterecord('reference', 'ref_id', 8,
             modifier=modifier_orig+',SCR')
        try:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "sample_tickets","NCA1","nca1-NORMAL NOTICE EXTENSION - NO.txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["NorthCalifornia"])
            ticket_id = self.tdb.insertticket(t)

            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.log.logger.logfile = StringIO.StringIO()

            tr.log.lock = 1
            tr.run(runonce=1)
            log = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()
            locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
            stats = [locate_status.screened,
                     locate_status.not_a_customer,
                     locate_status.screened,
                     locate_status.screened,
                     locate_status.not_a_customer,
                     locate_status.not_a_customer,
                     locate_status.not_a_customer,
                     locate_status.screened,
                     locate_status.not_a_customer,
                     locate_status.not_a_customer]
            for loc,stat in zip(locates,stats):
                self.assertEquals(loc['status'],stat)
                if stat == locate_status.screened:
                    s = "%s: ticket %s/%s, locate %s/%s screened" % (
                        t.ticket_format, t.ticket_number, ticket_id,
                        loc["client_code"], loc["locate_id"])
                    self.assertTrue(s in log)
                    self.assertEquals(loc['closed'], '1')
                    self.assertEquals(loc['closed_how'], 'router')
                    self.assertEquals(loc['closed_by_id'], '2063')
                    diff = date.Date(loc['closed_date']).diff(date.Date())
                    diff_hz = abs(diff/3600)
                    # California; expecting a 3 hour diff, with an error
                    # margin of ~60s (Mantis #2856)
                    self.assertTrue(abs(abs(diff) - 60*60*diff_hz) < 60.0)
        finally:
            if 'SCR' not in modifier_orig:
                self.tdb.updaterecord('reference', 'ref_id', 8, modifier=modifier_orig)

    def test_screen_6(self, timezone="EST/EDT"):
        """ Tests screening process, Mantis 2319 """
        self.config = config.getConfiguration()
        with Mockery(self.config, 'server_timezone', timezone):
            _testprep.add_test_client(self.tdb, 'PBTMDO', 'NCA2', active=1)
            ref = self.tdb.getrecords('reference', ref_id='8')[0]
            modifier_orig = ref['modifier']
            if 'SCR' not in modifier_orig:
                self.tdb.updaterecord('reference', 'ref_id', 8,
                 modifier=modifier_orig+',SCR')
            try:
                tl = ticketloader.TicketLoader(os.path.join(
                     _testprep.TICKET_PATH, "sample_tickets", "NCA2",
                     "nca2-NORMAL NOTICE RENEWAL-NO.txt"))
                raw = tl.getTicket()
                t = self.handler.parse(raw, ["NorthCaliforniaATT"])
                ticket_id = self.tdb.insertticket(t)

                tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
                tr.log.logger.logfile = StringIO.StringIO()

                tr.log.lock = 1
                tr.run(runonce=1)
                log = tr.log.logger.logfile.getvalue()
                tr.log.logger.logfile.close()
                locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
                stats = [locate_status.screened]
                for loc, stat in zip(locates, stats):
                    self.assertEquals(loc['status'], stat)
                    if stat == locate_status.screened:
                        s = "%s: ticket %s/%s, locate %s/%s screened" % (
                            t.ticket_format, t.ticket_number, ticket_id,
                            loc["client_code"], loc["locate_id"])
                        self.assertTrue(s in log)
                        self.assertEquals(loc['closed'], '1')
                        self.assertEquals(loc['closed_how'], 'router')
                        self.assertEquals(loc['closed_by_id'], '2063')
                        diff = date.Date(loc['closed_date']).diff(date.Date())
                        diff_hours = abs(diff / 3600) # hour-diff between TZs
                        # California; expecting a 3 hour diff, with an error
                        # margin of ~60s (Mantis #2856)
                        self.assertTrue(abs(abs(diff) - 3600*diff_hours) < 60.0)
            finally:
                _testprep.delete_test_client(self.tdb, 'PBTMDO')
                if 'SCR' not in modifier_orig:
                    self.tdb.updaterecord('reference', 'ref_id', 8,
                     modifier=modifier_orig)

    def test_screen_6a(self, timezone="CST/CDT"):
        self.test_screen_6(timezone)

    def test_screen_7(self):
        '''
        Tests screening process, Mantis 2319
        '''
        _testprep.add_test_client(self.tdb, 'PBTMDO', 'NCA2', active=1)
        try:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "sample_tickets","NCA2","nca2-NORMAL NOTICE RENEWAL-NO.txt"))
            raw = tl.getTicket()
            raw = raw.replace("Operator(s) To Re-mark Their Facilities: N",
                              "Operator(s) To Re-mark Their Facilities: Y")
            t = self.handler.parse(raw, ["NorthCaliforniaATT"])
            ticket_id = self.tdb.insertticket(t)

            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.log.logger.logfile = StringIO.StringIO()

            tr.log.lock = 1
            tr.run(runonce=1)
            log = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()
            locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
            stats = [locate_status.assigned]
            for loc,stat in zip(locates,stats):
                self.assertEquals(loc['status'],stat)
        finally:
            _testprep.delete_test_client(self.tdb, 'PBTMDO')

    def test_screen_8(self):
        '''
        Tests screening process, Mantis 2319
        '''
        _testprep.add_test_client(self.tdb, 'PBTMDO', 'NCA2', active=1)
        try:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                "sample_tickets","NCA2","nca2-NORMAL NOTICE.txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["NorthCaliforniaATT"])
            ticket_id = self.tdb.insertticket(t)

            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.log.logger.logfile = StringIO.StringIO()

            tr.log.lock = 1
            tr.run(runonce=1)
            log = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()
            locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
            stats = [locate_status.assigned]
            for loc,stat in zip(locates,stats):
                self.assertEquals(loc['status'],stat)
        finally:
            _testprep.delete_test_client(self.tdb, 'PBTMDO')

    def test_screen_9(self):
        '''
        Tests screening process, Mantis 2319
        '''
        _testprep.add_test_client(self.tdb, 'PBTCHI', 'NCA2', active=1)
        try:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "sample_tickets","NCA2","nca2-NORMAL NOTICE FOLLOW-UP-NO.txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["NorthCaliforniaATT"])
            ticket_id = self.tdb.insertticket(t)

            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.log.logger.logfile = StringIO.StringIO()

            tr.log.lock = 1
            tr.run(runonce=1)
            log = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()
            locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
            stats = [locate_status.assigned]
            for loc,stat in zip(locates,stats):
                self.assertEquals(loc['status'],stat)
        finally:
            _testprep.delete_test_client(self.tdb, 'PBTCHI')

    def test_screen_10(self):
        '''
        Tests screening process, Mantis 2319
        '''
        _testprep.add_test_client(self.tdb, 'PBTNPA', 'NCA2', active=1)
        ref = self.tdb.getrecords('reference',ref_id='8')[0]
        modifier_orig = ref['modifier']
        if 'SCR' not in modifier_orig:
            self.tdb.updaterecord('reference', 'ref_id', 8, modifier=modifier_orig+',SCR')
        try:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "sample_tickets","NCA2","nca2-NORMAL NOTICE EXTENSION - NO.txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["NorthCaliforniaATT"])
            ticket_id = self.tdb.insertticket(t)

            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.log.logger.logfile = StringIO.StringIO()

            tr.log.lock = 1
            tr.run(runonce=1)
            log = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()
            locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
            stats = [locate_status.screened]
            for loc,stat in zip(locates,stats):
                self.assertEquals(loc['status'],stat)
                if stat == locate_status.screened:
                    s = "%s: ticket %s/%s, locate %s/%s screened" % (
                        t.ticket_format, t.ticket_number, ticket_id,
                        loc["client_code"], loc["locate_id"])
                    self.assertTrue(s in log)
                    self.assertEquals(loc['closed'], '1')
                    self.assertEquals(loc['closed_how'], 'router')
        finally:
            _testprep.delete_test_client(self.tdb, 'PBTNPA')
            if 'SCR' not in modifier_orig:
                self.tdb.updaterecord('reference', 'ref_id', 8, modifier=modifier_orig)

    #
    # SCA1

    def test_screen_SCA1(self):
        _testprep.add_test_client(self.tdb, 'UQSTSO', 'SCA1', active=1)
        ref = self.tdb.getrecords('reference',ref_id='8')[0]
        modifier_orig = ref['modifier']
        if 'SCR' not in modifier_orig:
            self.tdb.updaterecord('reference', 'ref_id', 8,
              modifier=modifier_orig + ',SCR')

        try:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
                 "routing", "screening", "SCA1", "sca1-2010-03-29-00012.txt"))
            raw = tl.getTicket()
            t = self.handler.parse(raw, ["SouthCalifornia"])
            t.ticket_format = 'SCA1'
            ticket_id = self.tdb.insertticket(t)
            locates = self.tdb.getrecords('locate', ticket_id=ticket_id)

            tr = ticketrouter.TicketRouter(1, verbose=0, debug=0)
            tr.log.logger.logfile = StringIO.StringIO()

            tr.log.lock = 1
            tr.run(runonce=1)
            log = tr.log.logger.logfile.getvalue()
            tr.log.logger.logfile.close()
            locates = self.tdb.getrecords('locate', ticket_id=ticket_id)
            stats = [locate_status.screened]
            for loc, stat in zip(locates, stats):
                self.assertEquals(loc['status'], stat)
                if stat == locate_status.screened:
                    s = "%s: ticket %s/%s, locate %s/%s screened" % (
                        t.ticket_format, t.ticket_number, ticket_id,
                        loc["client_code"], loc["locate_id"])
                    self.assertTrue(s in log) # brittle
                    self.assertEquals(loc['closed'], '1')
                    self.assertEquals(loc['closed_how'], 'router')

                    # the closed date should be in California time, so
                    # basically EST-3... so we check if the difference between
                    # the current time and the closed_date is ~3 hours, with a
                    # few seconds difference allowed (because some time might
                    # elapse between running the router and these tests)
                    now = date.Date()
                    closed_date = date.Date(loc['closed_date'])
                    diff = now.diff(closed_date)
                    three_hours = 60 * 60 * 3
                    self.assertTrue(abs(three_hours - diff) < 60)

        finally:
            _testprep.delete_test_client(self.tdb, 'UQSTSO')
            if 'SCR' not in modifier_orig:
                self.tdb.updaterecord('reference', 'ref_id', 8,
                  modifier=modifier_orig)


