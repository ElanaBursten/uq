# test_main_update_cc.py
# Test main.py with update call centers.
# XXX In need of rewriting; database changes must be cleaner

import copy
import httplib
import os
import site; site.addsitedir('.')
import unittest
#
import _testprep
import businesslogic
import call_centers
import config
import dbinterface_old as dbinterface
import mail2
import main
import ticket_db
import ticketloader
import ticketparser
import update_call_centers

TEST01_CLIENTS = ["ATT01", "DCC01", "MTV01"]
TEST02_CLIENTS = ["PEP05", "PEP06", "PEP07", "PEP08", "PEP09"]

class TestMainUpdateCallCenters(unittest.TestCase):

    def send(self,xml):
        self.xml = xml

    def setUp(self):
        self.config = config.getConfiguration(reload=True)
        # replace the real configuration with our test data
        self.config.processes = [
         # one set for general testing...
         {'incoming': 'test_incoming',
          'format': 'TEST01',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
         {'incoming': 'test_incoming',
          'format': 'TEST02',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
        ]
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        mail2.TESTING = 1
        mail2._test_mail_sent = []

        _testprep.create_test_dirs()
        _testprep.set_cc_routing_file()
        for process in self.config.processes:
            #incoming, name, processed, error, group = process
            _testprep.clear(process['incoming'])
            _testprep.clear(process['processed'])
            _testprep.clear(process['error'])

        self.cc = call_centers.get_call_centers(self.tdb)
        self.cc.reload()
        self.ucc_orig = copy.deepcopy(self.cc.ucc)
        self.cc2format_orig = copy.deepcopy(call_centers.cc2format)
        self.format2cc_orig = copy.deepcopy(call_centers.format2cc)

        """ Add fake call centers to client table. """
        _testprep.clear_database(self.tdb)

        self.config.processes = [
         # one set for general testing...
         {'incoming': 'test_incoming',
          'format': 'TEST01',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
         {'incoming': 'test_incoming',
          'format': 'TEST02',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
        ]
        _testprep.create_test_dirs()
        _testprep.set_cc_routing_file()

        call_centers.cc2format["TEST01"] = ["Washington"]
        call_centers.cc2format["TEST02"] = ["Washington2"]

        # insert fake call centers
        self.tdb.insertrecord("call_center", cc_code="TEST01", cc_name="TEST01")
        self.tdb.insertrecord("call_center", cc_code="TEST02", cc_name="TEST02")

        # insert fake clients
        for client in TEST01_CLIENTS[:-1]:
            self.tdb.insertrecord("client",
                                  client_name=client,
                                  oc_code=client,
                                  active=1,
                                  call_center="TEST01")
        # add a client with update_call_center == TEST01
        self.tdb.insertrecord("client", client_name=TEST01_CLIENTS[-1],
                              oc_code=TEST01_CLIENTS[-1],
                              active=1,
                              call_center="TEST01",
                              update_call_center="TEST01")
        # also add an inactive client, which should be ignored
        self.tdb.insertrecord("client",
                              client_name="D00K13",
                              oc_code="D00K13",
                              active=0,
                              call_center="TEST01")

        for client in TEST02_CLIENTS:
            self.tdb.insertrecord("client",
                                  client_name=client,
                                  oc_code=client,
                                  active=1,
                                  call_center="TEST01",
                                  update_call_center="TEST02")

        self.tdb.runsql("""
          update client
          set active = 1
          where update_call_center in ('TEST01', 'TEST02')
        """)

        from callcenters import FMW1, FMW2
        businesslogic.TEST01BusinessLogic = FMW1.BusinessLogic
        businesslogic.TEST02BusinessLogic = FMW2.BusinessLogic
        update_call_centers.TEST02UpdateData = FMW2.UpdateData

        call_centers.get_call_centers(self.tdb).reload()

    def tearDown(self):
        config.getConfiguration(reload=True)
        self.cc.ucc = self.ucc_orig

        # NOTE: if there's any tickets (etc) in the database for these clients,
        # delete those too

        _testprep.clear_database(self.tdb)

        for name in ("TEST01", "TEST02"):
            # delete clients first
            try:
                self.tdb.deleterecords("client", call_center=name)
            except:
                pass
            # then call center
            try:
                self.tdb.deleterecords("call_center", cc_code=name)
            except:
                pass

        del businesslogic.TEST01BusinessLogic
        del businesslogic.TEST02BusinessLogic

        self.tdb.runsql("""
          update client
          set active = 1
          where update_call_center in ('FMW1', 'FMW2')
        """)

        _testprep.delete_test_dirs()
        #_testprep.clear_database(self.tdb)

    def drop(self, filename, data):
        """ Write a file (containing one or more images) to the
            test_incoming directory. """
        fullname = os.path.join("test_incoming", filename)
        f = open(fullname, "wb")
        f.write(data)
        f.close()

    def test_001_check_data(self):
        """ Check data in database. """
        clients = self.tdb.get_clients_dict()
        self.assertEquals(len(clients["TEST01"]), 8)
        self.assertEquals(
         len([c for c in clients["TEST01"]
              if c.update_call_center == "TEST02"]),
         5)
        self.assertEquals(
         len([c for c in clients["TEST01"]
              if c.update_call_center == "TEST01"]),
         1)

    def test_002_main_exclude(self):
        m = main.Main(verbose=0, call_center="TEST01")
        m.log.lock = 1

        exclude = m.get_exclude_list("TEST01")
        self.assertEquals(exclude, TEST02_CLIENTS)

        exclude = m.get_exclude_list("TEST02")
        self.assertEquals(exclude, TEST01_CLIENTS)

    def test_003_updatedata(self):
        # this should work
        spam = update_call_centers.get_updatedata("FMW2")

        # and so should this
        eggs = update_call_centers.get_updatedata("FMW1")

    def test_004_post_TEST01(self):
        tl = ticketloader.TicketLoader("../testdata/fmw-updates.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Washington"]) # TEST01
        #self.assertEquals(t.ticket_format, "TEST01")

        # per Mantis #2773, parsing rules have changed; to make the test
        # return the same results, we add locates to "Send To" section:
        raw = raw.rstrip() + "\n"
        termids = 'ATT01 DCC01 LTC02 MCI03 PEP06 PEP08'.split()
        for termid in termids:
            raw += ("  " + termid + "\n")

        # put the ticket in the incoming directory so Main can find it
        self.drop("test01.txt", raw)

        m = main.Main(verbose=0, call_center='TEST01')
        m.log.lock = 1
        m.run(runonce=1)

        # there should be one ticket, TEST01
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_format"], "TEST01")
        self.assertEquals(rows[0].get("serial_number"), None)
        id = rows[0]["ticket_id"]

        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 5)
        # there's 8 locates on the raw ticket, but only 5 should have been
        # parsed (i.e. the PEP ones should be ignored)

    test_004_post_TEST01.has_error=1

    def test_005_post_TEST02(self):
        tl = ticketloader.TicketLoader("../testdata/fmw-updates.txt")
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["Washington"]) # TEST01
        #self.assertEquals(t.ticket_format, "TEST01")

        # per Mantis #2773, parsing rules have changed; to make the test
        # return the same results, we add locates to "Send To" section:
        raw = raw.rstrip() + "\n"
        termids = 'ATT01 DCC01 LTC02 MCI03 PEP06 PEP08'.split()
        for termid in termids:
            raw += ("  " + termid + "\n")

        self.drop("test01.txt", raw)

        m = main.Main(verbose=0, call_center='TEST01')
        m.log.lock = 1
        m.run(runonce=1)
        tl = ticketloader.TicketLoader("../testdata/fmw-updates.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Washington2"])
        #self.assertEquals(t.ticket_format, "TEST02")

        # update Send To section again
        raw = raw.rstrip() + "\n"
        termids = 'MTV01 TPG01 WGL06 WSS01 PEP07'.split()
        for termid in termids:
            raw += ("  " + termid + "\n")
        self.drop("test02.txt", raw)

        m = main.Main(verbose=0, call_center='TEST02')
        m.log.lock = 1
        m.run(runonce=1)

        # there should still be only one ticket, of call center TEST01, because
        # TEST02 is an update call center:
        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_format"], "TEST01")
        self.assertEquals(rows[0]["serial_number"], "MISUTIL2003010700661")
        id = rows[0]["ticket_id"]

        # most fields should be the same as the original TEST01 ticket, except
        # for service_area_code:
        self.assertEquals(rows[0]["work_type"], "REPAIRING ELEC SVC")
        self.assertEquals(rows[0]["work_cross"], "MT OLIVE AV NE")
        self.assertEquals(rows[0]["service_area_code"], "PG NON PIPELINE")

        # in addition to the 5 existing locates, there should now be 5 new ones
        # (6 on ticket, but MTV01 is ignored because it's FMW1)
        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 5+5)

    test_005_post_TEST02.has_error=1

    def test_006_TEST02_first(self):
        # now, we try the FMW2 ticket first...

        tl = ticketloader.TicketLoader("../testdata/fmw-updates.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Washington2"])
        #self.assertEquals(t.ticket_format, "TEST02")
        self.drop("test02a.txt", raw)

        m = main.Main(verbose=0, call_center='TEST02')
        m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_format"], "TEST01")
        id = rows[0]["ticket_id"]
        self.assertEquals(rows[0]["work_type"], "INST ALL UG UTIL")
        self.assertEquals(rows[0]["work_cross"], "")
        self.assertEquals(rows[0]["service_area_code"], "PG NON PIPELINE")
        self.assertEquals(rows[0]["serial_number"], "MISUTIL2003010700661")

        # there should be *5* locates... MTV01 is ignored, it's an FMW1 client
        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 5)

    test_006_TEST02_first.has_error=1

    def test_007_then_TEST01(self):
        tl = ticketloader.TicketLoader("../testdata/fmw-updates.txt")
        raw = tl.tickets[1]
        t = self.handler.parse(raw, ["Washington2"])
        #self.assertEquals(t.ticket_format, "TEST02")

        # update Send To section again
        raw = raw.rstrip() + "\n"
        termids = 'MTV01 TPG01 WGL06 WSS01 PEP07'.split()
        for termid in termids:
            raw += ("  " + termid + "\n")

        self.drop("test02a.txt", raw)

        m = main.Main(verbose=0, call_center='TEST02')
        m.log.lock = 1
        m.run(runonce=1)
        # post the original TEST01 while the TEST02 is still in the database
        tl = ticketloader.TicketLoader("../testdata/fmw-updates.txt")
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Washington"])
        #self.assertEquals(t.ticket_format, "TEST01")

        raw = raw.rstrip() + "\n"
        termids = 'ATT01 DCC01 LTC02 MCI03 PEP06 PEP08'.split()
        for termid in termids:
            raw += ("  " + termid + "\n")

        self.drop("test01a.txt", raw)

        m = main.Main(verbose=0, call_center='TEST01')
        m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]["ticket_format"], "TEST01")
        id = rows[0]["ticket_id"]
        # these fields should have been overwritten:
        self.assertNotEqual(rows[0]["work_type"], "REPAIRING ELEC SVC")
        self.assertNotEqual(rows[0]["work_cross"], "MONTELLO AV NE")
        # but these should still be there:
        self.assertEquals(rows[0]["service_area_code"], "PG NON PIPELINE")
        self.assertEquals(rows[0]["serial_number"], "MISUTIL2003010700661")

        # this behavior is currently expected, since FMW1's transmit date falls
        # before FMW2's -> no update

        # 5 on original ticket, 5 on this one (well, there are really 8
        # locates, but 3 of them are ignored, since they're FMW2 clients)
        locates = self.tdb.getrecords("locate", ticket_id=id)
        self.assertEquals(len(locates), 10)

    test_007_then_TEST01.has_error=1

    def test_009_adapters(self):
        # test how call_center_adapter works with main.py.

        cfg = config.getConfiguration()
        cfg.processes[0]['format'] = 'LQW1'
        # the 'test' dir now excepts LQW1 tickets

        formats = call_centers.cc2format["LQW1"]
        tl = ticketloader.TicketLoader("../testdata/qwest-all.txt")
        for index, raw in enumerate(tl.tickets):
            t = self.handler.parse(raw, formats)
            self.drop("lqw1-%d.txt" % (index,), raw)

        m = main.Main(verbose=0, call_center='LQW1')
        m.log.lock = 1
        m.run(runonce=1)

        tickets = self.tdb.getrecords("ticket")
        def get_number(call_center):
            return len([t for t in tickets if t['ticket_format'] == call_center])
        self.assertEquals(get_number("LID1"), 1)
        self.assertEquals(get_number("LOR1"), 1)
        self.assertEquals(get_number("LWA1"), 3)
        self.assertEquals(get_number("LQW1"), 0)

    def test_010_transmit_date(self):
        # Tests nasty bug with transmit dates < transmit_date of original ticket.
        # This causes updateticket() to be called with locates_only=1, which
        # prevents certain FMW2 files from being updated (most notably,
        # service_area_code).

        _testprep.clear_database(self.tdb)

        self.config.processes = [
         # one set for general testing...
         {'incoming': 'test_incoming',
          'format': 'FMW1',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
         {'incoming': 'test_incoming',
          'format': 'FMW2',
          'processed': 'test_processed',
          'error': 'test_error',
          'attachments': ''},
        ]

        # make sure FMW2 has active clients, otherwise it won't be recognized
        # as update call center
        self.tdb.runsql("""
          update client
          set active = 1
          where update_call_center in ('FMW1', 'FMW2')
        """)
        call_centers.get_call_centers(self.tdb).reload()

        tl = ticketloader.TicketLoader('../testdata/fmw-updates.txt')
        raw = tl.tickets[0]
        t = self.handler.parse(raw, ["Washington"])
        t.ticket_format = t.source = 'FMW1'
        self.drop("test02a.txt", raw)

        m = main.Main(verbose=0, call_center='FMW1')
        m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords("ticket")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0].get('service_area_code'), None)

        raw = tl.tickets[1]
        raw = raw.replace('12/07/02', '05/14/02')
        raw = raw.replace('16:45', '10:20')
        self.drop('test02b.txt', raw)
        m = main.Main(verbose=0, call_center='FMW2')
        m.log.lock = 1
        m.run(runonce=1)

        rows = self.tdb.getrecords('ticket')
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['ticket_format'], 'FMW1')
        self.assertEquals(rows[0].get('service_area_code'), 'PG NON PIPELINE')
        self.assertEquals(rows[0].get('serial_number'), 'MISUTIL2003010700661')

    test_010_transmit_date.has_error=1

if __name__ == "__main__":

    unittest.main()
