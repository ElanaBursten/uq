# test_watchdog.py
# Make sure the watchdog works and works correctly.

import os
import shutil
import site; site.addsitedir('.')
import time
import unittest
#
import config
import date
import datadir
import ticketloader
import ticketparser
import ticket_db
import watchdog
import _testprep

class TestWatchdog(unittest.TestCase):

    incoming_present = 0

    # all tests reuse the same Watchdog
    dawg = watchdog.Watchdog(watchdog.WatchdogOptions(verbose=0, testmode=1))

    def setUp(self):
        try:
            os.mkdir("test_dog")
        except:
            pass

        self.create_dirs()
        _testprep.create_test_dirs()
        _testprep.set_cc_routing_file()

        _testprep.clear_database(self.dawg.tdb)

    def tearDown(self):
        try:
            shutil.rmtree("test_dog")
        except:
            pass

        _testprep.delete_test_dirs()

    def create_dirs(self):
        cfg = config.getConfiguration()
        dirs = [p['incoming'] for p in cfg.processes]
        for dir in dirs:
            if not os.path.exists(dir):
                os.mkdir(dir)

    def delete_dirs(self):
        cfg = config.getConfiguration()
        dirs = [p['incoming'] for p in cfg.processes]
        for dir in dirs:
            if os.path.exists(dir):
                shutil.rmtree(dir)

    def test_001_old_files(self):
        # write file
        f = open("test_dog/oldcake.txt", "w")
        f.write("bla"*100)
        f.close()
        # set file modified date so it seems old
        t = time.mktime((1973, 9, 2, 16, 5, 0, 0, 0, 0))
        os.utime("test_dog/oldcake.txt", (t, t))
        # write another file, don't change the date
        f = open("test_dog/newcake.txt", "w")
        f.write("zzz\n"*100)
        f.close()

        # what does the watchdog find?
        self.dawg.dir_old_tickets = "test_dog"
        oldfiles = self.dawg.get_old_files()
        self.assertEquals(len(oldfiles), 1)

    def test_002_old_responses(self):
        # remove tracker file, so it won't affect the test results
        try:
            datadir.datadir.remove(watchdog.TRACKER_FILENAME)
        except OSError:
            pass

        # insert a ticket first
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["Atlanta"])
        # set verbose=1 to check output
        self.dawg.tdb.runsql("delete responder_queue")
        id = self.dawg.tdb.insertticket(t, whodunit="dog")

        # get the locates
        locates = self.dawg.tdb.getrecords("locate", ticket_id=id)
        self.assert_(len(locates) > 0)

        # add responses for these locates
        yesterday = date.Date() - 1
        for locate in locates:
            # use special SQL so yesterday's date is used rather than today's
            sql = """
             insert responder_queue
             (locate_id, insert_date, ticket_format, client_code)
             values (%s, '%s', '%s', '%s')
            """ % (locate['locate_id'], yesterday.isodate(), 'Atlanta',
                   locate['client_code'])
            self.dawg.tdb.runsql(sql)

        d = self.dawg.get_old_responses()
        self.assert_((d['Atlanta']))

        self.dawg.testmode = 1
        self.dawg.run()

    def test_incoming_item(self):
        # check that incoming_item table is empty
        rows = self.dawg.tdb.getrecords('incoming_item')
        self.assertEquals(len(rows), 0)

        # ...and that watchdog correctly reports it as empty
        self.assertEquals(self.dawg.get_incoming_tickets(), 0)

        # insert records in incoming_item
        d = date.Date() # current date/time
        d.dec()         # yesterday 
        _testprep.add_incoming_item(self.dawg.tdb, '3003', 1, 'x', d.isodate())
        _testprep.add_incoming_item(self.dawg.tdb, '3006', 2, 'y', d.isodate())
        _testprep.add_incoming_item(self.dawg.tdb, '3006', 3, 'z',
         date.Date().isodate())

        rows = self.dawg.tdb.getrecords('incoming_item')
        self.assertEquals(len(rows), 3)

        # there should be 3 records...
        self.assertEquals(self.dawg.get_incoming_tickets(), 3)
        # ...but only two records are considered "old"
        self.assertEquals(self.dawg.get_incoming_tickets_age(), 2)

    def test_pending_tickets_1(self):
        '''
        Test get_pending_tickets_age
        '''

        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["Atlanta"])
        t = handler.parse(raw, ["Atlanta"])

        _testprep.clear_database(self.dawg.tdb)

        id = self.dawg.tdb.insertticket(t, whodunit="dog")

        # get the locates
        locates = self.dawg.tdb.getrecords("locate", ticket_id=id)
        self.assert_(len(locates) > 0)
        now = date.Date()
        now.dec_time(hours=4)
        try:
            sql = 'ALTER TABLE locate DISABLE TRIGGER ALL'
            self.dawg.tdb.runsql(sql)
            for locate in locates:
                self.dawg.tdb.updaterecord('locate', 'locate_id',
                  locate['locate_id'], modified_date=now.isodate())
        finally:
            sql = 'ALTER TABLE locate ENABLE TRIGGER ALL'
            self.dawg.tdb.runsql(sql)
        num_old = self.dawg.get_pending_tickets_age()
        self.assertEquals(num_old, len(locates))

    def test_pending_tickets_2(self):
        '''
        Test get_pending_tickets_age
        '''

        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, ["Atlanta"])
        t = handler.parse(raw, ["Atlanta"])

        id = self.dawg.tdb.insertticket(t, whodunit="dog")

        # get the locates
        locates = self.dawg.tdb.getrecords("locate", ticket_id=id)
        self.assert_(len(locates) > 0)
        num_old = self.dawg.get_pending_tickets_age()
        self.assertEquals(num_old, 0)

    def test_pending_tickets_body(self):
        '''
        Test get_pending_tickets_age in message body
        '''
        body = self.dawg._make_warning_mail_body(0, 0, 5, 0, 0, 0, 0, 0, 3, 2)
        self.assert_('are 5 tickets older than 3 hours waiting to be' in body)
        self.assert_('2 items older than 9 minutes' in body)



if __name__ == "__main__":

    unittest.main()

