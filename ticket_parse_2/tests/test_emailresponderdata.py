# test_emailresponder.py
# Test retrieving of the EmailResponderData classes, and the methods of
# these classes.

import site; site.addsitedir('.')
import unittest
#
import callcenters
import emailresponder_data as erd
import responder_config_data as rcd

class TestEmailResponderData(unittest.TestCase):

    RESPONDERS = ['NCA1', 'SCA1', 'FWP1']

    # dummy config values to keep getresponderdata happy
    CONFIG = {
        'clients': [],
        'mappings': [],
        'name': "",
        'resp_email': "",
        'sender': '',
        'subject': '',
        "tech_code": "",
        'translations': [],
        'skip': [],
        'all_versions': 0,
        'send_emergencies': 0,
        'ack_dir': '',
        'ack_proc_dir': '',
        'accounts': []
    }

    def get_responderdata(self, name):
        class DummyConfig: pass
        config = DummyConfig()
        config.emailresponders = {}
        config.emailresponders[name] = self.CONFIG
        config.responderconfigdata = rcd.ResponderConfigData()
        # we don't use add() here, so ResponderConfigData is essentially empty
        config.responderconfigdata.gather_skip_data()
        # the following should not raise an error
        obj = erd.getresponderdata(config, name, self.CONFIG)
        return obj

    def test_get_emailresponderdata(self):
        for resp in self.RESPONDERS:
            obj = self.get_responderdata(resp)
            self.assert_(obj.__class__.__name__.endswith("ResponderData"))
            self.assert_(obj)



if __name__ == "__main__":

    unittest.main()

