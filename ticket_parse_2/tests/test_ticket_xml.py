# test_ticket_xml.py

import os
import site; site.addsitedir('.')
import unittest
import xml.etree.ElementTree as ET
#
import _testprep
import et_tools
import ticket
import ticket_db
import ticket_xml
import ticketloader
import ticketparser

class TestTicketXML(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

    def test_generate(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "3004-2011-08-agl", "TEST-2011-08-03-00004.txt"))
        raw = tl.getTicket()
        t = self.handler.parse(raw, ["GA3004"])
        t.con_name = '<the company>'

        t.insert(self.tdb)

        # ticket has not been routed at this point, but for testing purposes
        # it doesn't matter

        gen = ticket_xml.TicketXMLGenerator()
        tree = gen.generate(t)

        #print ET.tostring(tree.getroot())
        et_tools.write_tree(tree, 'ticket1.xml')
        xml = open('ticket1.xml').read()
        #print xml # DEBUG
        et_tools.validate(xml)

if __name__ == "__main__":
    unittest.main()

