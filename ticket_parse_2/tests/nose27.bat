rem  Set RUN_BROKEN_XXXX vars here (see full list in _testprep.py), e.g.:
rem  set RUN_BROKEN_FDE7=1
rem  set RUN_BROKEN_DELAWARE7=1

rem  Tests flagged with 'has_error' or 'fails', were broken after the final
rem  Oasis build (12236), and before we restarted maintaining the tests.
call c:\python27\scripts\nosetests -A "(not will_fail) and (not has_error) and (not fails)" %1 %2 %3 %4 %5 %6 %7 %8 %9
