# test_receiver.py

from __future__ import with_statement
import glob
import os
import re
import shutil
import string
import StringIO
import time
import unittest
import xml.etree.ElementTree as ET
import site
site.addsitedir('.')
#
import _testprep
import attachment
import config
import datadir
import emailtools
import filetools
import receiver
import ticketloader
import ticketparser
from mockery import Mockery

RO = receiver.ReceiverOptions  # shorthand


class TestReceiver(unittest.TestCase):
    def set_up_config(self, config_file='test_receiver.xml'):
        if config_file:
            self.config = config.Configuration(config_file)
            config.setConfiguration(self.config)
        else:
            self.config = config.getConfiguration()

    def setUp(self):
        for dir in ['test_logs', 'test_attachments']:
            try:
                os.mkdir(datadir.datadir.get_filename(dir))
            except:
                pass
        self.set_up_config()

    def tearDown(self):
        filetools.remove_file("stuff.txt")

        for dir in ['test_incoming', 'test_processed', 'test_error',
         'test_attachments', 'test_attachments_processed']:
            try:
                shutil.rmtree(dir)
            except:
                pass

        for dir in ['test_logs', 'test_attachments']:
            try:
                shutil.rmtree(datadir.datadir.get_filename(dir))
            except:
                pass
        config.setConfiguration(None)

    def test_001(self):
        cfg = receiver.ReceiverConfiguration(
          self.config, 'FHL2', self.config.call_center_process('FHL2').get('accounts', []))
        rec = receiver.Receiver(cfg, RO(verbose=0))

        f = open(os.path.join(_testprep.TICKET_PATH, "receiver", "FHL1-2003-06-26-0005.txt"))
        data = f.read()
        f.close()
        lines = data.split("\n")

        idx = emailtools.find_header_delimiter(lines)
        self.assert_(idx > -1)

        # for this ticket, we're only interested in the attachment.  The 'body'
        # doesn't contain anything useful.
        proc = receiver.EmailProcessor(rec.log)
        parts = proc.get_parts(lines, idx)
        self.assertEquals(parts[0][0], 'attachment')
        self.assertEquals(parts[0][2], 'UtiliQueJun202003.txt')

        try:
            g = open("stuff.txt", "w")
            g.write(parts[0][1])
            g.close()

            tl = ticketloader.TicketLoader("stuff.txt")
            self.assertEquals(len(tl.tickets), 85)

            # get a ticket and try to parse it... not the first one
            raw = tl.tickets[1]
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["Houston1"])

        finally:
            try:
                os.remove("stuff.txt")
            except:
                pass

    def test_002(self):
        cfg = receiver.ReceiverConfiguration(
            self.config, 'FMW2', self.config.call_center_process('FMW2').get('accounts', []))
        cfg.accounts[0]['file_ext'] = ''
        cfg.accounts[0]['full'] = False
        cfg.accounts[0]['store_attachments'] = False
        rec = receiver.Receiver(cfg, RO(verbose=0))

        path = os.path.join(_testprep.TICKET_PATH, "receiver", "fmw1-2003-06-26-0010.txt")
        f = open(path, "r")
        data = f.read()
        f.close()
        lines = data.split("\n")

        idx = emailtools.find_header_delimiter(lines)
        self.assert_(idx > -1)

        proc = receiver.EmailProcessor(rec.log)
        parts = proc.get_parts(lines, idx)
        self.assertEquals(parts[0][0], 'body')

        try:
            g = open("stuff.txt", "w")
            g.write(parts[0][1])
            g.close()

            tl = ticketloader.TicketLoader("stuff.txt")
            self.assertEquals(len(tl.tickets), 1)

            # get the ticket and try to parse it...
            raw = tl.tickets[0]
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["Washington"])

        finally:
            try:
                os.remove("stuff.txt")
            except:
                pass

    test_002.has_error=1

    def test_quoted_printable(self):
        """Test the "multipart" quoted-printable FCO2 tickets. """
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH, "qpmail.txt"))
        raw = tl.tickets[0] # not a ticket, but a raw message
        lines = raw.split("\n")
        log = []

        def mock_write_message(outfilename, data):
            log.append((outfilename, data))

        cfg = receiver.ReceiverConfiguration(
            self.config, 'FCO2', self.config.call_center_process('FCO2').get('accounts', []))

        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.write_message_raw = mock_write_message

        self.assertEquals(emailtools.body_is_quoted_printable(raw), True)

        rec.process_message(lines, cfg.accounts[0])

        newbody = log[0][1]
        self.assertEquals(newbody.count('=0D'), 0)
        self.assertEquals(newbody.count('=0A'), 0)
        self.assertEquals(newbody.count('--Mark='), 0)

        # test if ticket can be parsed now
        handler = ticketparser.TicketHandler()
        t = handler.parse(newbody, ["Colorado2"])
        self.assertEquals(t.ticket_number, "0126218")

    test_quoted_printable.has_error=1

    def test_quoted_printable_2(self):
        """Test the "multipart" quoted-printable FCO2 tickets. """
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "qpmail-2.txt"))
        raw = tl.tickets[0] # not a ticket, but a raw message
        lines = raw.split("\n")
        log = []

        def mock_write_message(outfilename, data):
            log.append((outfilename, data))

        cfg = receiver.ReceiverConfiguration(
            self.config, 'FCO2', self.config.call_center_process('FCO2').get('accounts', []))

        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.write_message_raw = mock_write_message

        self.assertEquals(emailtools.body_is_quoted_printable(raw), True)

        rec.process_message(lines, cfg.accounts[0])

        newbody = log[0][1]
        self.assertEquals(newbody.count('=0D'), 0)
        self.assertEquals(newbody.count('=0A'), 0)
        self.assertEquals(newbody.count('--Mark='), 0)
        self.assertEquals(newbody.count("multi-part"), 0)
        self.assertEquals(newbody.count("Content-Type"), 0)
        self.assertEquals(newbody.count("\r"), 0)

        # test if ticket can be parsed now
        handler = ticketparser.TicketHandler()
        t = handler.parse(newbody, ["Colorado2"])
        self.assertEquals(t.ticket_number, "0181888")

    test_quoted_printable_2.has_error=1

    def test_quoted_printable_3(self):
        # test quoted-printable as sent by GA [2007-01-12]

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "receiver", "GA-new-2007-01-12.txt"))
        raw = tl.tickets[0] # not a ticket, but a raw message
        lines = raw.split("\n")
        log = []

        def mock_write_message(outfilename, data):
            log.append((outfilename, data))

        cfg = receiver.ReceiverConfiguration(
            self.config, 'Atlanta', self.config.call_center_process('Atlanta').get('accounts', []))

        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.write_message_raw = mock_write_message

        self.assertEquals(emailtools.body_is_quoted_printable(raw), True)

        rec.process_message(lines, cfg.accounts[0])

        newbody = log[0][1]
        self.assertEquals(newbody.count('=0D'), 0)
        self.assertEquals(newbody.count('=0A'), 0)
        self.assertEquals(newbody.count('--Mark='), 0)
        self.assertEquals(newbody.count("multi-part"), 0)
        self.assertEquals(newbody.count("Content-Type"), 0)
        self.assertEquals(newbody.count("\r"), 0)

        self.assertEquals(newbody.count("Underground Notification"), 1)
        self.assertEquals(newbody.count("Crew on Site:"), 1)

        # test if ticket can be parsed now
        handler = ticketparser.TicketHandler()
        t = handler.parse(newbody, ["Atlanta"])
        self.assertEquals(t.ticket_number, "11276-032-051")

    def test_make_temp_filename(self):
        cfg = receiver.ReceiverConfiguration(
            self.config, 'FCO2', self.config.call_center_process('FCO2').get('accounts', []))

        rec = receiver.Receiver(cfg, RO(verbose=0))
        lt = time.localtime(time.time())
        filename = rec.make_temp_filename("FMW1")
        self.assertEquals(filename[:4], "FMW1")
        self.assertEquals(filename[-4:], ".txt")
        self.assertEquals(filename[5:9], str(lt[0]))
        self.assertEquals(int(filename[9:11]), lt[1])
        self.assertEquals(int(filename[11:13]), lt[2])

    def test_write_attachment_email(self):
        cfg = receiver.ReceiverConfiguration(
            self.config, 'FCO2', self.config.call_center_process('FCO2').get('accounts', []))

        rec = receiver.Receiver(cfg, RO(verbose=0))
        lines = [str(i) for i in range(100)]
        rec.write_original_email(lines, "FCO1")

        path = datadir.datadir.get_filename(cfg.attachment_dir)
        files = os.listdir(path)
        self.assertEquals(len(files), 1)

    def test_write_attachment_email_2(self):
        cfg = receiver.ReceiverConfiguration(
            self.config, 'FCO2', self.config.call_center_process('FCO2').get('accounts', []))

        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "FHL1-2003-06-26-0005.txt")
        f = open(path, 'r')
        data = f.read()
        f.close()
        lines = data.split("\n")
        lines.append("Content-Disposition: attachment; filename=2005170289.PNG")
        lines.append("blah-de-blah")

        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.process_message(lines, cfg.accounts[0])
        self.assertEquals(rec.may_have_attachments(lines), True)

        # since this email has attachments, a copy should have been written
        # to the attachment dir
        path = datadir.datadir.get_filename(cfg.attachment_dir)
        files = os.listdir(path)
        self.assertEquals(len(files), 1)

    def test_LAK1_attachments(self):
        cfg = receiver.ReceiverConfiguration(
            self.config, 'LAK1', self.config.call_center_process('LAK1').get('accounts', []))
        cfg.accounts[0]['store_attachments'] = False

        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "LAK1-20050503-154204-913.txt")
        f = open(path, 'r')
        data = f.read()
        lines = data.split("\n")
        f.close()

        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.check_directories(cfg.accounts[0])
        rec.process_message(lines, cfg.accounts[0])

        # check test_incoming directory
        files = os.listdir("test_incoming")
        # two files should have been written: the plain text (ticket) and
        # the attachment
        self.assertEquals(len(files), 2)
        # check if attachment filename matches the expected pattern
        re_attachment = re.compile(".*-A\d\d\.txt$")
        m = re_attachment.match(files[0])
        self.assert_(m, "Attachment filename does not match")

        # check that files start with the same date and number
        self.assertEquals(files[0][:20], files[1][:20])

        # do some more tests to check that we have the right files...
        # the first file is the ticket
        data = open(os.path.join('test_incoming', files[1])).read()
        self.assert_(data.find("ALASKA DIGLINE") >= 0)

    def test_LAK1_attachments_again(self):
        cfg = receiver.ReceiverConfiguration(
            self.config, 'LAK1', self.config.call_center_process('LAK1').get('accounts', []))

        f = open(os.path.join(_testprep.TICKET_PATH, "receiver",
            "LAK1-20050503-154204-913.txt"))
        data = f.read()
        lines = data.split("\n")
        f.close()

        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.check_directories(cfg.accounts[0])
        rec.process_message(lines, cfg.accounts[0])

        # check test_incoming directory
        files = os.listdir("test_incoming")
        # two files should have been written: the plain text (ticket) and
        # the attachment
        self.assertEquals(len(files), 1)

        attachments = os.listdir("test_attachments")
        self.assertEquals(len(attachments), 1)

        # check if attachment filename matches the expected pattern
        re_attachment = re.compile(".*-A\d\d\.txt$")
        m = re_attachment.match(attachments[0])
        self.assert_(m, "Attachment filename does not match")

        # check that files start with the same date and number
        self.assertEquals(files[0][:20], attachments[0][:20])

        # do some more tests to check that we have the right files...
        # the first file is the ticket
        data = open(os.path.join('test_incoming', files[0])).read()
        self.assert_(data.find("ALASKA DIGLINE") >= 0)

        # read the attachment and inspect it
        attfilename = os.path.join('test_attachments', attachments[0])
        filename, attdata = attachment.read_attachment(attfilename)
        self.assertEquals(filename, "2005190401.PCX")

    def test_ignore_attachments(self):
        cfg = receiver.ReceiverConfiguration(
            self.config, 'LAK1', self.config.call_center_process('LAK1').get('accounts', []))

        # write a file that has an attachment
        cfg.accounts[0]['store_attachments'] = False
        cfg.accounts[0]['ignore_attachments'] = True
        f = open(os.path.join(_testprep.TICKET_PATH, "receiver",
            "LAK1-20050503-154204-913.txt"))
        data = f.read()
        lines = data.split("\n")
        f.close()

        # let receiver process this "incoming email"
        rec = receiver.Receiver(cfg, RO(verbose=0))
        os.mkdir("test_attachments")
        rec.process_message(lines, cfg.accounts[0])

        # check test_incoming directory
        # one file should have been written: the plain text (ticket)
        # and no attachment!!
        files = os.listdir("test_incoming")
        self.assertEquals(len(files), 1)
        attachments = os.listdir("test_attachments")
        self.assertEquals(len(attachments), 0)

    def test_ignore_attachments_2(self):
        cfg = receiver.ReceiverConfiguration(
            self.config, 'FDX1', self.config.call_center_process('FDX1').get('accounts', []))

        f = open(os.path.join(_testprep.TICKET_PATH, "receiver", "fdx1-raw.txt"))
        data = f.read()
        lines = data.split("\n")
        f.close()

        # let receiver process this "incoming email"
        rec = receiver.Receiver(cfg, RO(verbose=0))
        os.mkdir("test_attachments")
        rec.process_message(lines, cfg.accounts[0])

        # check test_incoming directory
        # one file should have been written: the plain text (ticket)
        # and no attachment!!
        files = os.listdir("test_incoming")
        self.assertEquals(len(files), 1)
        attachments = os.listdir("test_attachments")
        self.assertEquals(len(attachments), 0)

        # yes, but...
        filename = os.path.join("test_incoming", files[0])
        f = open(filename, "r")
        data = f.read()
        f.close()
        self.assertEquals(data.count("Locate Request No. 053082371"), 1)

    def test_get_configuration_old(self):
        rc = receiver.ReceiverConfiguration(
            self.config, 'FMW2', self.config.call_center_process('FMW2').get('accounts', []))

    def test_SCA6_1(self):
        cfg = receiver.ReceiverConfiguration(
            self.config, 'FHL2', self.config.call_center_process('FHL2').get('accounts', []))

        rec = receiver.Receiver(cfg, RO(verbose=0))

        f = open(os.path.join("..", "testdata", "receiver",
            "USAS USA01 2008_07_16  #00001A A72470912-53A NORM UPDT.txt"), "r")
        data = f.read()
        f.close()
        lines = string.split(data, "\n")

        idx = emailtools.find_header_delimiter(lines)
        self.assert_(idx > -1)

        proc = receiver.EmailProcessor(rec.log)
        parts = proc.get_parts(lines, idx)
        self.assertEquals(parts[0][0], 'body')

        try:
            g = open("stuff.txt", "w")
            g.write(parts[0][1])
            g.close()

            tl = ticketloader.TicketLoader("stuff.txt")
            self.assertEquals(len(tl.tickets), 1)

            # get the ticket and try to parse it...
            raw = tl.tickets[0]
            handler = ticketparser.TicketHandler()
            t = handler.parse(raw, ["SouthCaliforniaATT"])

        finally:
            try:
                os.remove("stuff.txt")
            except:
                pass

    def test_SCA6_2(self):
        # replacement for the original test_SCA6_2, without using an actual POP
        # server. >.<  also, it tests Receiver.check_account, as it should.

        # path of file we're going to read
        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "USAS USA01 2008_07_16  #00001A A72470912-53A NORM UPDT.txt")

        # define a mock class so we don't actually need to check a POP server
        class MockPOP3:
            def list(self):
                return [1, [1], None]
            def retr(self, n):
                with open(path, 'rb') as f:
                    data = f.read()
                # POP3.retr returns a list of lines, w/o trailing newlines
                return ("blah", [s.rstrip('\r') for s in data.split("\n")], 0)
            def quit(self):
                pass
            def dele(self, n):
                pass

        class MockEmailReceiver(receiver.EmailReceiver):
            def connect(self, *args):
                self.conn = MockPOP3()
                return self.conn

        # set up custom configuration
        cfg = receiver.ReceiverConfiguration(
            self.config, 'SCA6', self.config.call_center_process('SCA6').get('accounts', []))
        self.assertEquals(len(cfg.accounts), 1)

        # run the MockReceiver with this configuration, suppressing output
        with filetools.Stdout(StringIO.StringIO()):
            with Mockery(receiver, 'EmailReceiver', MockEmailReceiver):
                rec = receiver.Receiver(cfg, RO(verbose=1))
                rec.check()

        # check the destination directory
        files = os.listdir('test_incoming')
        self.assertEquals(len(files), 1)
        with open(os.path.join('test_incoming', files[0]), "r") as f:
            data = f.read()
        self.assertEquals(len(data), 7660)  # size of original file

    def test_get_configuration(self):
        rc = receiver.ReceiverConfiguration(
          self.config, 'FMW2', self.config.call_center_process('FMW2').get('accounts', []))

        self.assertEquals(rc.accounts,
         [{'type': 'email',
           'attachment_dir': 'test_attachments',
           'call_center': 'FMW2',
           'destination': 'test_incoming',
           'full': True,
           'ignore_attachments': False,
           'password': 'emailtkts.FMW2',
           'server': '192.168.1.171',
           'store_attachments': True,
           'file_ext': '.xml',
           'aggregate_file': '',
           'use_ssl': False,
           'port': 110,
           'username': 'emailtkts.FMW2'}])

        self.assertEquals(rc.smtpinfo.host, 'smtp.earthlink.net')
        self.assertEquals(rc.smtpinfo.from_address, 'logger@earthlink.net')
        self.assertEquals(rc.smtpinfo.use_ssl, 0)
        self.assertEquals(rc.logdir, "test_logs")
        self.assertEquals(rc.attachment_dir, "test_attachments")
        self.assertEquals(rc.admins,
          [{'email': 'vaporeon@wanadoo.nl', 'name': 'Hans Nowak', 'ln': '1'}])

    def test_AGL_base64(self):
        """ Mantis #2682: Some AGL tickets are sent as base64. """

        # set up custom configuration
        cfg = receiver.ReceiverConfiguration(
            self.config, 'FAU5', self.config.call_center_process('FAU5').get('accounts', []))

        # get raw email
        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "AGL-2010-11-02.txt")
        data = filetools.read_file(path)
        lines = data.split('\n')

        # run Receiver.process_message(), which should produce a decoded base64
        # file
        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.process_message(lines, cfg.accounts[0])

        # what files did it produce?
        files = os.listdir('test_incoming')
        self.assertEquals(len(files), 1)
        self.assertEquals(files[0][:4], 'FAU5')

        # let's open this file
        fpath = os.path.join('test_incoming', files[0])
        data = filetools.read_file(fpath)

        # the original has odd characters in it
        self.assertTrue('\xe2' in data)
        # verify that the ticketloader gets rid of them
        tl = ticketloader.TicketLoader(fpath)
        raw = tl.tickets[0]
        self.assertFalse('\xe2' in raw)

    # XXX this doesn't work properly yet. technically the email has 2
    # attachments and an empty body. the first attachment should be stored as
    # a ticket (in XML format). the second one is an image and should be
    # stored in the attachment directory.
    def test_attachments_general(self):
        cfg = receiver.ReceiverConfiguration(
            self.config, 'LAM01', self.config.call_center_process('LAM01').get('accounts', []))
        cfg.accounts[0]['file_ext'] = ''
        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "test-2-attachments.txt")
        f = open(path, 'r')
        lines = f.read().split('\n')
        f.close()

        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.process_message(lines, cfg.accounts[0])

        #print os.listdir('test_incoming')
        #print os.listdir('test-stored-attachments')

        # XXX ERROR:
        # writes two files: one with the empty body
        # one with the (un-decoded) XML *and* the image clumped together.
        # >.<
        # needs fixed, but does LAM01 really send the emails like that?

        '''
        # check test_incoming directory
        files = os.listdir("test_incoming")
        # two files should have been written: the plain text (ticket) and
        # the attachment
        self.assertEquals(len(files), 2)
        # check if attachment filename matches the expected pattern
        re_attachment = re.compile(".*-A\d\d\.txt$")
        m = re_attachment.match(files[0])
        self.assert_(m, "Attachment filename does not match")

        # check that files start with the same date and number
        self.assertEquals(files[0][:20], files[1][:20])

        # do some more tests to check that we have the right files...
        # the first file is the ticket
        data = open(os.path.join('test_incoming', files[1])).read()
        self.assert_(data.find("ALASKA DIGLINE") >= 0)
        '''

    def test_LAM01(self):
        # Mantis #2782: LAM01 sends work orders as XML, email may contain
        # attachments as well (e.g. images).

        # set up custom configuration
        cfg = receiver.ReceiverConfiguration(
            self.config, 'FMW2', self.config.call_center_process('FMW2').get('accounts', []))

        # get raw email
        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "LAM01-attachment-1.txt")
        data = filetools.read_file(path)
        lines = data.split('\n')

        # run Receiver.process_message(), which should produce a decoded base64
        # file
        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.check_directories(cfg.accounts[0])
        for f in glob.glob("test_incoming\\*.txt"):
            os.remove(f)
        for f in glob.glob("test_attachments\\*.txt"):
            os.remove(f)
        rec.process_message(lines, cfg.accounts[0])

        tickets = os.listdir('test_incoming')
        atts = os.listdir('test_attachments')
        self.assertEquals(len(tickets), 1)
        self.assertEquals(len(atts), 1)
        self.assertTrue('A01' in atts[0])

        # check work order
        data2 = open(os.path.join('test_incoming', tickets[0]), 'rb').read()
        self.assertTrue("<?xml" in data2)
        ET.fromstring(data2) # verify that it's valid XML

        # check attachment file (still in base64)
        data3 = open(os.path.join('test_attachments', atts[0]),
                'r').read()
        lines3 = data3.split('\n')
        self.assertEquals(lines3[0], '[Attachment]')
        self.assertEquals(lines3[1], 'v0013991_WO_201105100000135D7E927.tif')

    def test_LAM01_2(self):
        # Mantis #2782: LAM01 sends work orders as XML, email may contain
        # attachments as well (e.g. images).

        cfg = receiver.ReceiverConfiguration(
            self.config, 'LAM01', self.config.call_center_process('LAM01').get('accounts', []))
        #print cfg.accounts
        self.assertEquals(len(cfg.accounts), 1)
        self.assertEquals(cfg.accounts[0]['file_ext'], '.xml')
        self.assertEquals(cfg.accounts[0]['store_attachments'], True)

        # get raw email
        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "LAM01-attachments-2.txt")
        data = filetools.read_file(path, 'rb')
        lines = data.split('\n')

        # run Receiver.process_message(), which should produce a decoded base64
        # file
        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.check_directories(cfg.accounts[0])
        rec.process_message(lines, cfg.accounts[0])

        # there should be one file in test_incoming, none in
        # test-stored-attachments
        tickets = os.listdir('test_incoming')
        atts = os.listdir('test_attachments')
        self.assertEquals(len(tickets), 1)
        self.assertEquals(len(atts), 0)

    def test_LAM01_3(self):
        # Mantis #2782: LAM01 sends work orders as XML, email may contain
        # attachments as well (e.g. images).
        # For unclear reasons, UQ receives emails in a slightly different
        # format than e.g. GMail or a vanilla POP server.

        cfg = receiver.ReceiverConfiguration(
            self.config, 'LAM01', self.config.call_center_process('LAM01').get('accounts', []))
        self.assertEquals(len(cfg.accounts), 1)
        self.assertEquals(cfg.accounts[0]['file_ext'], '.xml')
        self.assertEquals(cfg.accounts[0]['store_attachments'], True)

        # get raw email
        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "lambert.txt")
        data = filetools.read_file(path, 'rb')
        first = data.split(chr(12))[0] # get first email
        lines = first.split('\n')

        # run Receiver.process_message(), which should produce a decoded base64
        # file
        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.check_directories(cfg.accounts[0])
        rec.process_message(lines, cfg.accounts[0])

        # there should be one file in test_incoming, and one in
        # test-stored-attachments
        tickets = os.listdir('test_incoming')
        atts = os.listdir('test_attachments')
        self.assertEquals(len(tickets), 1)
        self.assertEquals(len(atts), 1)
        # check that ticket filename and attachment filename match up
        fn, ext = os.path.splitext(tickets[0])
        self.assertEquals(atts[0], fn + "-A01" + ext)

        # check if the file in test-stored-attachments contains a filename
        filename = 'V0019196_WO_201106030000235D7E918.tif'
        att_path = os.path.join('test_attachments', atts[0])
        with open(att_path, 'r') as f:
            data = f.read()
        att_lines = data.split('\n')
        self.assertEquals(att_lines[0], '[Attachment]')
        self.assertEquals(att_lines[1], filename)
        self.assertEquals(att_lines[2], 'begin 666 ' + filename)
        self.assertEquals(att_lines[3][:5], 'M24DJ') # base64

    def test_SCA11_sunesys(self):
        # QMAN-3533

        cfg = receiver.ReceiverConfiguration(
            self.config, 'SCA11', self.config.call_center_process('SCA11').get('accounts', []))
        self.assertEquals(len(cfg.accounts), 1)
        #self.assertEquals(cfg.accounts[0]['file_ext'], '.xml')
        self.assertEquals(cfg.accounts[0]['store_attachments'], True)

        # get raw email
        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "3533-sunesys", "DigAlert A40940218 Conflict Found")
        data = filetools.read_file(path, 'rb')
        #first = data.split(chr(12))[0] # get first email
        lines = data.split('\n')

        # run Receiver.process_message(), which should produce a decoded base64
        # file
        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.check_directories(cfg.accounts[0])
        rec.process_message(lines, cfg.accounts[0])

        # there should be one file in test_incoming, and one in
        # test-stored-attachments
        tickets = os.listdir('test_incoming')
        atts = os.listdir('test_attachments')
        self.assertEquals(len(tickets), 1)
        self.assertEquals(len(atts), 1)
        # check that ticket filename and attachment filename match up
        fn, ext = os.path.splitext(tickets[0])
        self.assertEquals(atts[0], fn + "-A01" + ext)

        # check if the file in test-stored-attachments contains a filename
        # and base64 data
        filename = 'CA_S_10515.pdf'
        att_path = os.path.join('test_attachments', atts[0])
        with open(att_path, 'r') as f:
            data = f.read()
        att_lines = data.split('\n')
        self.assertEquals(att_lines[0], '[Attachment]')
        self.assertEquals(att_lines[1], filename)
        self.assertEquals(att_lines[2], 'begin 666 ' + filename)
        self.assertEquals(att_lines[3][:5], 'M)5!$') # base64

    def test_SCA11_sunesys_2(self):
        # QMAN-3566

        cfg = receiver.ReceiverConfiguration(
            self.config, 'SCA11', self.config.call_center_process('SCA11').get('accounts', []))
        self.assertEquals(len(cfg.accounts), 1)
        self.assertEquals(cfg.accounts[0]['store_attachments'], True)

        # get raw email
        path = os.path.join(_testprep.TICKET_PATH, "receiver",
               "3566-sunesys", "KHector DigAlert A41410145.eml")
        data = filetools.read_file(path, 'rb')
        lines = data.split('\n')

        # run Receiver.process_message(), which should produce a decoded base64
        # file
        rec = receiver.Receiver(cfg, RO(verbose=0))
        rec.check_directories(cfg.accounts[0])
        rec.process_message(lines, cfg.accounts[0])

        # there should be one file in test_incoming, and one in
        # test-stored-attachments
        tickets = os.listdir('test_incoming')
        atts = os.listdir('test_attachments')
        self.assertEquals(len(tickets), 1)
        self.assertEquals(len(atts), 1)
        # check that ticket filename and attachment filename match up
        fn, ext = os.path.splitext(tickets[0])
        self.assertEquals(atts[0], fn + "-A01" + ext)

        # check if the file in test-stored-attachments contains a filename
        # and base64 data
        filename = 'CA_S_10684.pdf'
        att_path = os.path.join('test_attachments', atts[0])
        with open(att_path, 'r') as f:
            data = f.read()
        att_lines = data.split('\n')
        self.assertEquals(att_lines[0], '[Attachment]')
        self.assertEquals(att_lines[1], filename)
        self.assertEquals(att_lines[2], 'begin 666 ' + filename)
        self.assertEquals(att_lines[3][:5], 'M)5!$') # base64


if __name__ == "__main__":

    unittest.main()
