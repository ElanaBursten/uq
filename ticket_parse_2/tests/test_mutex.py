# test_mutex.py

from __future__ import with_statement
import unittest
#
import dbinterface_ado
from mutex import mutex_is_locked, MutexLock, SingleInstanceError

dbado = None
dbado2 = None

class TestMutex(unittest.TestCase):
    def setUp(self):
        global dbado, dbado2
        if not dbado:
            dbado = dbinterface_ado.DBInterfaceADO()
        if not dbado2:
            dbado2 = dbinterface_ado.DBInterfaceADO()

    def test_mutex_1(self):
        self.assertFalse(mutex_is_locked(dbado, "test"))
        self.assertFalse(mutex_is_locked(dbado2, "test"))

        with MutexLock(dbado, "test") as ml:
            self.assertEquals(ml.mutex_name, "test")
            self.assertTrue(mutex_is_locked(dbado, "test"))
            self.assertTrue(mutex_is_locked(dbado2, "test"))

        self.assertFalse(mutex_is_locked(dbado, "test"))
        self.assertFalse(mutex_is_locked(dbado2, "test"))

    def test_mutex_2(self):
        # try to acquire the same mutex twice
        with MutexLock(dbado, "test") as m1:
            self.assertEquals(mutex_is_locked(dbado, "test"), True)

            # try again with the same connection
            try:
                with MutexLock(dbado, "test") as m2:
                    self.assertFalse("this should never be executed")
            except SingleInstanceError, e:
                pass # ok
            except:
                self.assertFalse("SingleInstanceError expected")
            else:
                self.assertFalse("SingleInstanceError expected")

            # try again with a different connection
            try:
                with MutexLock(dbado2, "test") as m2:
                    self.assertFalse("this should never be executed")
            except SingleInstanceError, e:
                pass # ok
            except:
                self.assertFalse("SingleInstanceError expected")
            else:
                self.assertFalse("SingleInstanceError expected")

            # mutex should still be locked
            self.assertEquals(mutex_is_locked(dbado, "test"), True)

        self.assertEquals(mutex_is_locked(dbado, "test"), False)

if __name__ == "__main__":
    unittest.main()

