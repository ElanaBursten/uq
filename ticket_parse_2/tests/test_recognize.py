# test_recognize.py
# Created: 28 Jan 2002, Hans Nowak
# Test recognition of tickets and audits.
# NOTE: recognition of "good morning" tickets has been moved to
# test_recognize_gm.py.

import os
import site; site.addsitedir('.')
import sys
import unittest
#
import _testprep
import call_centers
import recognize
import ticketloader
import ticketparser
import tools

glob = tools.safeglob

log = []

class TestRecognize(unittest.TestCase):
    """ Test if all tickets are recognized as the correct type. """

    def recognizeMany(self, filename, name, fixed=0):
        # 'fixed' means that we have one suggestion for the format that must be
        # recognized. Non-fixed means that we leave it up to the recognizer to
        # come up with a format suggestion.
        counter = 0
        path = os.path.join(_testprep.TICKET_PATH, filename)
        tl = ticketloader.TicketLoader(path)
        for raw in tl:
            if fixed:
                type = recognize.recognize_as(raw, name)
                format = type and name or None
            else:
                format = recognize.recognize(raw)
            self.assertEquals(format, name,
             "Unrecognized ticket: #%d\n'''%s'''" % (counter+1, raw))
            counter = counter + 1
        log.append("%s %s tickets recognized in %s" % (counter, name, filename))

    def testRecognizeAtlanta(self):
        for x in ("1", "2", "3", "4"):
            self.recognizeMany("atlanta-"+x+".txt", "Atlanta", fixed=1)

    def testRecognizeRichmond_linenoise(self):
        # 22 Apr 2002: Because the Richmond2 recognizer must accept a certain
        # level of line noise, we want to test if such tickets are really
        # recognized:
        tl = ticketloader.TicketLoader("../testdata/richmond2-1.txt")
        raw = tl.tickets[0]
        raw = "CONNECT\n%$!" + raw   # insert junk
        format = recognize.recognize(raw)
        self.assertEquals(format, "Richmond2")

    def testRecognizeLanham(self):
        # ignore this test until Lanham is in production
        self.recognizeMany("lanham-1.txt", "Lanham", fixed=1)

    def test_xml_not_a_ticket_1(self):
        raw = '''<?xml version="1.0" encoding="UTF-8"?>
                <OccOutgoingMessage xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://or.itic.occinc.com/or-xml/OutboundTicket-20080815.xsd">
                <Ticket TicketNum="8200622" RevNum="0">
                <CallCenter>Not a ticket</CallCenter>
                </Ticket>
                </OccOutgoingMessage>'''
        _format = recognize.recognize(raw)
        self.assertNotEquals(_format, "OregonXML")

    def test_xml_not_a_ticket_2(self):
        raw = '''<NotATicket>Not a ticket</NotATicket>'''
        _format = recognize.recognize(raw)
        self.assertNotEquals(_format, "OregonXML")

    TICKET_TESTDATA = [
        # filename, format, fixed
        (os.path.join("sample_tickets","xml_tickets","TEST-2008-10-27-00001-A01.xml"), 'OregonXML', 1),
        (os.path.join("sample_tickets","NWN","LNG1-2009-02-11-00464-A01.xml"), 'OregonXML', 1),
        (os.path.join("sample_tickets","LUT1","LUT1-2008-04-03-00033.txt"), "Utah", 1),
        ('Alabama-1.txt', 'Alabama', 1),
        ('AlabamaMobile-1.txt', 'AlabamaMobile', 1),
        ('AlabamaNew-1.txt', 'AlabamaNew', 1),
        ('Alaska-1.txt', 'Alaska', 1),
        ('AlaskaNew-1.txt', 'AlaskaNew', 1),
        ('Albuquerque-1.txt', 'Albuquerque', 1),
        ('Albuquerque2-1.txt', 'Albuquerque2', 1),
        ('AlbuquerqueAZ-1.txt', 'AlbuquerqueAZ', 1),
        ('AlbuquerqueTX-1.txt', 'AlbuquerqueTX', 1),
        ('Arkansas-1.txt', 'Arkansas', 1),
        (os.path.join("..","testdata","sample_tickets","FNL2","FNL2-2008-12-30-00001.txt"), 'Arkansas', 1),
        ('AR1393-1.txt', 'AR1393', 1),
        ('Arizona-1.txt', 'Arizona', 1),
        ('Arizona-1.txt', 'AlbuquerqueAZ', 1),
        ('Atlanta-1.txt', 'Atlanta', 1),
        ('Atlanta-1.txt', 'TennesseeGA', 1),
        ('Atlanta2-1.txt', 'Atlanta2', 1),
        ('Baltimore-1.txt', 'Baltimore', 1),
        (os.path.join("..","testdata","sample_tickets","FMB1","FMB1-2009-04-03-01400.txt"), 'Baltimore', 1),
        ('BatonRouge1-1.txt', 'BatonRouge1', 1),
        ('BatonRouge1-1.txt', 'BatonRouge2', 1),
        ('BatonRouge3-1.txt', 'BatonRouge3', 1),
        ('Colorado-1.txt', 'Colorado', 1),
        ('Colorado2-1.txt', 'Colorado2', 1),
        ("ColoradoNE-1.txt", "ColoradoNE", 1),
        ('Dallas1-1.txt', 'Dallas1', 1),
        (os.path.join("sample_tickets","FDX1","FDX4-2008-07-30-01871.txt"), 'Dallas1', 1),
        ('Dallas1-1.txt', 'AlbuquerqueTX', 1),
        ('Dallas2-1.txt', 'Dallas2', 1),
        ('Dallas4-1.txt', 'Dallas4', 1),
        ('Dallas4-1.txt', 'TX6004', 1),
        ('Delaware1-1.txt', 'Delaware1', 1),
        ('Delaware3-1.txt', 'Delaware3', 1),
        ('Fairfax2-1.txt', 'Fairfax2', 1),
        ('Fairfax2-1.txt', 'Washington3', 1),
        ("FL9001-1.txt", "FL9001", 1),
        ("FL9002-1.txt", "FL9002", 1),
        ('GA3001-1.txt', 'GA3001', 1),
        ('GA3003-1.txt', 'GA3003', 1),
        ('Greenville-1.txt', 'Greenville', 1),
        ("GreenvilleNew-1.txt", "GreenvilleNew", 1),
        ("Greenville3-1.txt", "Greenville3", 1),
        ('Harlingen-1.txt', 'Harlingen', 1),
        ('Harlingen-2.txt', 'Harlingen', 1),
        ('Houston1-1.txt', 'Houston1', 1),
        ('Houston2-1.txt', 'Houston2', 1),
        ("HoustonKorterra-1.txt", "HoustonKorterra", 1),
        ('Idaho-1.txt', 'Idaho', 1),
        ('IdahoNew-1.txt', 'IdahoNew', 1),
        ('Illinois-1.txt', 'Illinois', 1),
        ('Jacksonville-1.txt', 'Jacksonville', 1),
        ('Jacksonville2-1.txt', 'Jacksonville2', 1),
        ('Kentucky-1.txt', "Kentucky", 1),
        ("KentuckyNew-1.txt", "KentuckyNew", 1),
        ('Mississippi-1.txt', 'Mississippi', 1),
        ('Mississippi2-1.txt', 'Mississippi2', 1),
        ('Mississippi2-1.txt', 'MS1392', 1),
        ('MississippiTN-1.txt', 'MississippiTN', 1),
        ('Nevada-1.txt', 'Nevada', 1),
        ('NewJersey-1.txt', 'NewJersey', 1),
        ('NewJersey2-1.txt', 'NewJersey2', 1),
        (os.path.join("sample_tickets","GPP","GSU2-2008-02-01-00024.txt"), 'NewJersey2', 1),
        ('NewJersey2-1.txt', 'NewJersey2a', 1),
        ('NewJersey3-1.txt', 'NewJersey3', 1),
        ('NM1101-1.txt', 'NM1101', 1),
        ('NorthCalifornia-1.txt', 'NorthCalifornia', 1),
        ('NorthCalifornia2-1.txt', 'NorthCalifornia2', 1),
        ('NorthCaliforniaATT-1.txt', 'NorthCaliforniaATT', 1),
        ('NorthCarolina-1.txt', 'NorthCarolina', 1),
        ('NorthCarolina-1.txt', 'RichmondNC', 1),
        ('NorthCarolina-1.txt', 'TennesseeNC', 1),
        ("Northwest-1.txt", "Northwest", 1),
        ('Ohio-1.txt', 'Ohio', 1),
        ('Ohio-2.txt', 'Ohio', 1),
        ("OhioNew-stub.txt", "OhioNew", 1),
        ('Oregon-1.txt', 'Oregon', 1),
        ('Oregon2-1.txt', 'Oregon2', 1),
        ('OregonEM-1.txt', 'OregonEM', 1),
        ("OregonFalcon-1.txt", "OregonFalcon", 1),
        ('Orlando-1.txt', 'AlabamaFL', 1),
        ('Orlando-1.txt', 'Orlando', 1),
        ('PA7501-1.txt', 'PA7501', 1),
        ('PA7501-new.txt', 'PA7501', 1),
        ('Pennsylvania-1.txt', 'Pennsylvania', 1),
        ('qwest-idl-1.txt', 'QWEST_IDL', 1),
        ('qwest-ieucc-1.txt', 'QWEST_IEUCC', 1),
        ('qwest-ooc-1.txt', 'QWEST_OOC', 1),
        ('qwest-uulc-1.txt', 'QWEST_UULC', 1),
        ('qwest-woc-1.txt', 'QWEST_WOC', 1),
        ('Richmond1-1.txt', 'Richmond1', 1),
        ('Richmond2-1.txt', 'Richmond2', 1),
        ('Richmond3-1.txt', 'Richmond3', 1),
        ('SanAntonio-1.txt', 'SanAntonio', 1),
        ('SouthCalifornia-1.txt', 'SouthCalifornia', 1),
        ('SouthCaliforniaATT-1.txt', 'SouthCaliforniaATT', 1),
        ('SouthCarolina-1.txt', 'SouthCarolina', 1),
        ('Tennessee-1.txt', 'Jackson', 1),
        ('Tennessee-1.txt', 'Tennessee', 1),
        ('Tennessee-1.txt', 'TN1391', 1),
        ('Tennessee-1.txt', 'TN3005', 1),
        (os.path.join("sample_tickets","TNOCS","TEST-2008-12-16-00001.txt"), 'Jackson', 1),
        (os.path.join("sample_tickets","TNOCS","TEST-2008-12-16-00001.txt"), 'Tennessee', 1),
        (os.path.join("sample_tickets","TNOCS","TEST-2008-12-16-00001.txt"), 'TN1391', 1),
        ('Tulsa-1.txt', 'Tulsa', 1),
        ('TX1103-1.txt', 'TX1103', 1),
        ('TX6001-1.txt', 'TX6001', 1),
        ('TX6002-1.txt', 'TX6002', 1),
        ('vupsnew-1.txt', 'VUPSNew', 1),
        ('vupsnew-2.txt', 'VUPSNew', 1),
        ('Washington-1.txt', 'Washington', 1),
        ('Washington2-1.txt', 'Washington2', 1),
        ("WashingtonState-1.txt", "WashingtonState", 1),
        ("WashingtonState2-1.txt", "WashingtonState2", 1),
        ('WestVirginia-1.txt', 'WestVirginia', 1),
        ('WestVirginia-2.txt', 'WestVirginia', 1),
        ('Wisconsin1.txt', 'Wisconsin1', 1),
        ('Wisconsin2.txt', 'Wisconsin2', 1),

        ('6003.txt', 'TX6003', 1),
        ('fdx2.txt', 'Dallas2New', 1),
        ('fwp4.txt', 'WestVirginiaAEP', 1),
        ('lqw1-new.txt', 'QWEST_IRTH', 1),
        ('sca1-cox.txt', 'SouthCaliforniaCOX', 1),

        ('GA3003-1.txt', 'Atlanta4', 1),
        ('GA3003-1.txt', 'Atlanta5', 1),
        ('Atlanta-1.txt', 'GA3003a', 1),
        ('Atlanta-1.txt', 'GA3004a', 1),
        ('Atlanta-1.txt', 'Atlanta4a', 1),
        ('Atlanta-1.txt', 'Atlanta5a', 1),

        (os.path.join("sample_tickets","FCT4","TEST-2008-09-23-00006_NORMAL.txt"), 'GA3004', 1),
        (os.path.join("sample_tickets","FJL4","TEST-2009-03-31-00030.txt"), 'MississippiKorterra', 1),
        (os.path.join("sample_tickets","1422","1422-2009-04-14-00001.txt"), 'SC1422', 1),
        (os.path.join("sample_tickets","1422","1422-2009-04-14-00002.txt"), 'SC1422', 1),
        (os.path.join("sample_tickets","SCA7","TEST-2009-04-14-00240.txt"), 'SouthCaliforniaSDGKorterra', 1),
        (os.path.join("sample_tickets","FNV3","FNV3-2009-04-01-00034.txt"), 'NashvilleKorterra', 1),
        (os.path.join("sample_tickets","FCL3","FCL3-2009-05-19-00007.txt"), 'NorthCarolina2', 1),
        (os.path.join("sample_tickets","FCL3","FCL3-2009-05-19-00008.txt"), 'NorthCarolina2', 1),
        (os.path.join("sample_tickets","FCL3","FCL3-2009-05-19-00009.txt"), 'NorthCarolina2', 1),
        (os.path.join("sample_tickets","FCL3","FCL3-2009-05-19-00010.txt"), 'NorthCarolina2', 1),
        (os.path.join("sample_tickets","FCL3","FCL3-2009-05-19-00011.txt"), 'NorthCarolina2', 1),
        (os.path.join("sample_tickets","FCL3","FCL3-2009-05-19-00012.txt"), 'NorthCarolina2', 1),
        (os.path.join("sample_tickets","1423","1423-2009-05-19-00001.txt"), 'SC1423', 1),
        (os.path.join("sample_tickets","1423","1423-2009-05-19-00002.txt"), 'SC1423', 1),
        (os.path.join("sample_tickets","1423","1423-2009-05-19-00003.txt"), 'SC1423', 1),
        (os.path.join("sample_tickets","1423","1423-2009-05-19-00004.txt"), 'SC1423', 1),
        (os.path.join("sample_tickets","1423","1423-2009-05-19-00005.txt"), 'SC1423', 1),
        (os.path.join("sample_tickets","1423","1423-2009-05-19-00006.txt"), 'SC1423', 1),
    ]

    def test_recognize_tickets_4(self):
        """ Blanket testing of testdata/tickets. (This only tests recognition,
            not parsing!) """
        data = [
            ('NewJersey2010',      'NewJersey-2010-02-15'),
            ('NewJersey4',         'NewJersey4-2010-03-15'),
            ('NewJersey5',         'NewJersey5-2010-03-15'),
            ('NorthCalifornia',    'NCA1-2010-03-26'), # NorthCalifornia_A?
            ('NC811',              'NC811-2010-05-04'),
            ('NC811',              'FCT3-2010-05-19'),
            ('TN811',              'FCT3-2010-05-19'),
            ('FairfaxXML',         'FairfaxXML-2010-05-06'),
            ('DelawareXML',        'FairfaxXML-2010-05-06'), # same format
            ('WashingtonXML',      'FairfaxXML-2010-05-06'), # ditto
            ('VUPSNewOCC3',        'OCC3-2010-05-18'), # contain \xA0 chars
            ('GA3004b',            '3004-2010-06-29'),
            ('Atlanta5b',          '3004-2010-06-29'), # same
            ('TennesseeNC',        'FCT3-2010-07-01'),
            ('WV1181',             '1181-2011-03-23'),
            ('WV1181Alt',          '1181-2012-02-15'),
            ('Baltimore2',         'FMB2-2011-04-22'), # same as FMB1
            ('PensacolaAL',        'FPL2-2011-04-29'),
            ('SC1422',             '1422-2011-06-01'),
            ('Baltimore',          'FMB1-2011-06'),
            ('Washington',         'FMW1-2011-06'),
            ('PA7501',             '7501-2011-07'), # fmt changes M#2808
            ('VUPSNewOCC4',        'OCC4-2011-09-30'),
            ('WV1181Korterra',     '1181-2011-11-28'),
            ('SouthCaliforniaCOX', 'SCA5-2011-12-20-cox'), # Mantis #2917
            ('OCC4XML',            'OCC4-2011-12-21-xml'), # Mantis #2911
            ('SC1421A',            '1421-2012-03-09'),     # Mantis #2977
            ('SC1425A',            '1421-2012-03-09'),     # ! Mantis #3470
            ('NorthCalifornia3',   'NCA3-2012-04-09'),     # Mantis #2995
            ('SouthCalifornia9',   'SCA9-2012-04-09'),     # Mantis #2995
            ('SouthCaliforniaNV',  'SCA8-2012-04-09'),     # Mantis #2995
            ('SC1444',             '1444-2012-09-27-ansco'), # Mantis #3125
            ('SC1424',             '1424-2012-10-05-ansco'),
            ('SC1421A',            '1421-2012-10-06'),
            ('SC1422A',            '1422-2012-10-12'),
            ('SC1425A',            '1421-2012-10-06'),     # !
            ('GA3006',             '3006-2012-11-01'),
            ('SC2051',             '2051-2012-11-07'),
            ('Nashville2013',      'FNV1-2013-01-08'),
            ('Oregon4',            'LOR3-2013-02'),
            ('WashingtonState2',   'LWA2-2013-02'),
            ('NC2101',             '2101-2013-03-01'),
            ('Oregon6',            'LOR5-2013-03-14'),
            ('WashingtonState4',   'LWA4-2013-03-14'),
            ('WashingtonState5',   'LWA5-2013-03-28'),
            ('SouthCalifornia10',  'SCA10-2013-05-04'),
            ('NewJersey6',         'NewJersey6-2013-11-01'),
            ('NewJersey7',         'NewJersey7-2013-11-01'),
            ('OregonPortland',     'LOR6-2013-11-09'),
            ('GeorgiaXML',         'GeorgiaXML-2013-11'),
            ('SC1421PUPS',         '1421-2014-02-pups'),
            ('NewJersey8',         'NewJersey8-2014-02'),
            ('NorthCalifornia4',   'NCA4-2014-03'),
            ('SouthCalifornia11',  'SCA11-2014-04'),
            ('NorthCalifornia5',   'NCA5-2014-04'),
            ('NewJersey9',         'NewJersey9-2014-08'),
            #('NorthCaliforniaATT', 'NCA2-2010-04-30'),
        ]

        for format, dirname in data:
            if not _testprep.RUN_BROKEN_BALTIMORE and format == 'Baltimore':
                continue
            if not _testprep.RUN_BROKEN_BALTIMORE2 and format == 'Baltimore2':
                continue
            if not _testprep.RUN_BROKEN_WASHINGTON and format == 'Washington':
                continue

            path = os.path.join(_testprep.TICKET_PATH, "tickets", dirname)
            filenames = glob(os.path.join(path, "*.txt"))
            for fn in filenames:
                tl = ticketloader.TicketLoader(fn)
                for raw in tl.tickets:
                    type = recognize.recognize_as(raw, format)
                    found = (type == 'ticket') and format or None
                    self.assertEquals(found, format,
                      "ticket not recognized as %s:\n%s" % (format, raw))

    def test_NewJersey2010(self):
        """ Test that old and new NewJersey formats are recognized properly. """

        # this should be recognized as NewJersey2010, but not NewJersey...
        p1 = os.path.join(_testprep.TICKET_PATH, "tickets",
             "NewJersey-2010-02-15", "TEST-2010-01-08-00003.txt")
        tl = ticketloader.TicketLoader(p1)
        for raw in tl.tickets:
            self.assertTrue(recognize.recognize_as(raw, "NewJersey2010"))
            self.assertFalse(recognize.recognize_as(raw, "NewJersey"))

        # ...and vice versa
        p2 = os.path.join(_testprep.TICKET_PATH, "sample_tickets",
                          "NewJersey-1.txt")
        tl = ticketloader.TicketLoader(p2)
        for raw in tl.tickets:
            self.assertTrue(recognize.recognize_as(raw, "NewJersey"))
            self.assertFalse(recognize.recognize_as(raw, "NewJersey2010"))

    def test_ticket_format(self):
        for filename, format, fixed in TestRecognize.TICKET_TESTDATA:
            if not _testprep.RUN_BROKEN_BALTIMORE and format == 'Baltimore':
                continue
            if not _testprep.RUN_BROKEN_DELAWARE1 and format == 'Delaware1':
                continue
            if not _testprep.RUN_BROKEN_DELAWARE3 and format == 'Delaware3':
                continue
            if not _testprep.RUN_BROKEN_WASHINGTON and format == 'Washington':
                continue
            if not _testprep.RUN_BROKEN_WASHINGTON2 and format == 'Washington2':
                continue
            fullname = os.path.join("..", "testdata", filename)
            tl = ticketloader.TicketLoader(fullname)
            for index, raw in enumerate(tl.tickets):
                if fixed:
                    type = recognize.recognize_as(raw, format)
                    found = (type == "ticket") and format or None
                else:
                    found = recognize.recognize_summary(raw)
                self.assertEquals(found, format,
                 "Ticket #%d is not of format %s:\n%s" % (index+1, format, raw))

    def test_recognize_summaries_2(self):
        names = [
            # (format, fixed)
            ("Utah", 1),
            ("Atlanta", 1),
            ("BatonRouge1", 1),
            ("Fairfax", 1),
            ("Lanham", 1),
            ("NewJersey", 1),
            ("NorthCarolina", 1),
            ("Ohio", 1),
            ("Orlando", 1),
            ("Pennsylvania", 1),
            ("Richmond1", 1),
            ("Richmond2", 1),
            ("SouthCarolina", 1),
            ("Washington", 1),
            ("WestVirginia", 1),
            ("Richmond3", 1),
            ("Fairfax", 1),
            ("Illinois", 1),
            ("NorthCalifornia", 1),
            ("SouthCalifornia", 1),
            ("Alabama", 1),
            ("Kentucky", 1),
            ("Dallas1", 1),
            ("Dallas2", 1),
            ("Arkansas", 1),
            ("Tulsa", 1),
            ("SanAntonio", 1),
            ("Harlingen", 1),
            ("Houston1", 1),
            ("AlabamaMobile", 1),
            ("AlabamaNew", 1),
            ("Mississippi", 1),
            ("Mississippi2", 1),
            ("Delaware1", 1),
            ("Delaware3", 1),
            ("TulsaWellsco", 1),
            ('Albuquerque', 1),
            ("Fairfax2", 1),
            ("Albuquerque2", 1),
            ("Oregon", 1),
            ("Wisconsin2", 1),
            ("Wisconsin2", 1),
            ("WashingtonState", 1),
            ("Alaska", 1),
            ("HarlingenTESS", 1),
            ("TX1103", 1),
            ("VUPSNewOCC3", 1),
            ("Nevada", 1),
            ("Alaska", 1),
            ("Greenville3", 1),
            ('IdahoNew', 1),
            #("WashingtonState2", 1), # OBSOLETE
            ("OregonFalcon", 1),
            ("Jacksonville2", 1),
            ("KentuckyNew", 1),
            ('NorthCalifornia2', 1),
            ("Northwest", 1),
            ("NewJersey2", 1),
            ("NewJersey2a", 1),
            ("HoustonKorterra", 1),
            ("SouthCaliforniaATT", 1),
            ("NewJersey3", 1),
            ("OregonEM", 1),
            ("NorthCaliforniaATT", 1),
            ("BatonRouge3", 1),
            ("Dallas4", 1),
        ]

        for format, fixed in names:
            tl = ticketloader.TicketLoader("../testdata/sum-%s.txt" % (format,))
            for raw in tl.tickets:
                if fixed:
                    type = recognize.recognize_as(raw, format)
                    found = (type == "summary") and format or None
                else:
                    found = recognize.recognize_summary(raw)
                self.assertEquals(found, format)

    def test_recognize_summaries_3(self):
        names = [
            # (format, filename)
            ('WestVirginiaAEP', 'fwp4-audit.txt'),
            ('TX6003', '6003-audit.txt'),
            ('Dallas2New', 'fdx2-audit.txt'),
            ('SouthCaliforniaCOX', 'sca1-cox-audit.txt'),
            ('FL9002', 'sum-FL9002-new.txt'),
            ('SC1421', os.path.join("sample_tickets","1421","1421-2009-07-09-00001.txt")),
            ('SC1422', os.path.join("sample_tickets","1422","SummaryAudit-1422.txt")),
            ('SC1422_ATT', os.path.join("sample_tickets","1422","FGV1-2009-07-09-00001.txt")),
            ('SC1422_ATT', os.path.join("sample_tickets","1422","FGV1-2009-07-07-00002.txt")),
            ('SouthCaliforniaSDGKorterra', os.path.join("sample_tickets","SCA7","TEST-2009-04-15-00034.txt")),
            ('SouthCaliforniaSCA2Korterra', os.path.join("sample_tickets","SCA7","TEST-2009-04-15-00034.txt")),
            ('NashvilleKorterra', os.path.join("sample_tickets","FNV3","FNV3-2009-04-02-00001.txt")),
            ('NorthCarolina2', os.path.join("sample_tickets","FCL3","FCL3-2009-05-20-00001.txt")),
            ('SC1423', os.path.join("sample_tickets","1423","1423-2009-05-20-00001.txt")),
            ('Washington4', os.path.join("audits", "FMW4-2009-12-15",
                                         "TEST-2009-12-16-00001.txt")),
            ('Atlanta5a', os.path.join("audits", "FAU5-2010-01-13",
                                       "FAU5-2010-01-08-00001.txt")),
            ('GA3004a', os.path.join("audits", "3004-2010-01-13",
                                       "3004-2010-01-08-00003.txt")),
        ]

        for format, filename in names:
            tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH, filename))
            for raw in tl.tickets:
                type = recognize.recognize_as(raw, format)
                found = (type == "summary") and format or None
                self.assertEquals(found, format)

    def test_recognize_audits_4(self):
        """ Blanket testing of testdata/audits """
        data = [
            # format             # subdir of testdata/audits
            ('Washington4',      'FMW4-2009-12-15'),
            ('Atlanta5a',        'FAU5-2010-01-13'),
            ('GA3004a',          '3004-2010-01-13'),
            ('NewJersey2010',    'NewJersey-2010-02-15'),
            ('NewJersey4',       'NewJersey4-2010-03-15'),
            ('NewJersey2010B',   'NewJersey2-2010-06-18'),
            ('FairfaxXML',       'OCC2-2010-06-11'),
            ('DelawareXML',      'OCC2-2010-06-11'), # same
            ('WashingtonXML',    'OCC2-2010-06-11'), # same
            ('GA3004b',          '3004-2010-06-29'), # SENTRi; contains \xA0
            ('Atlanta5b',        '3004-2010-06-29'), # same
            ('TennesseeNC',      'FTL3-2010-07-01'),
            ('WV1181',           '1181-2011-03-26'),
            ('WV1181Alt',        '1181-2011-04-01'),
            ('Baltimore2',       'FMB2-2011-04-22'), # same as FMB2
            ('PensacolaAL',      'FPL2-2011-04-29'),
            ('VUPSNewOCC4',      'OCC4-2011-09-30'),
            ('Arizona',          'SCA3-2011-11-06'),
            ('SC1421A',          '1421-2012-03-09'),
            ('SC1422A',          '1421-2012-03-09'),
            ('SC1425A',          '1421-2012-03-09'), # !
            ('GA3006',           '3006-2012-11-01'),
            ('Nashville2013',    'FNV1-2013-01-08'),
            ('Oregon4',          'LOR3-2013-03-09'),
            ('Oregon5',          'LOR4-2013-03-09'),
            ('WashingtonState2', 'LWA2-2013-03-09'),
            ('WashingtonState3', 'LWA3-2013-03-09'),
            ('Oregon6',          'LOR5-2013-03-14'),
            ('WashingtonState4', 'LWA4-2013-03-14'),
            ('SouthCalifornia10','SCA10-2013-05-04'),
            ('AL811',            'FAU2-2013-06-12'),
            ('NewJersey6',       'NewJersey6-2013-11-01'),
            ('NewJersey7',       'NewJersey7-2013-11-01'),
            ('NewJersey9',       'NewJersey9-2014-08'),
        ]

        for format, dirname in data:
            path = os.path.join(_testprep.TICKET_PATH, "audits", dirname)
            filenames = glob(os.path.join(path, "*.txt"))
            for fn in filenames:
                tl = ticketloader.TicketLoader(fn)
                for raw in tl.tickets:
                    type = recognize.recognize_as(raw, format)
                    found = (type == 'summary') and format or None
                    self.assertEquals(found, format, raw)

    def test_recognize_Dallas2_summaries(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Dallas2-2.txt")
        for raw in tl.tickets:
            type = recognize.recognize_as(raw, "Dallas2")
            found = (type == "summary") and "Dallas2" or None
            self.assertEquals(found, "Dallas2")

    def test_recognize_WestVirginia_summaries(self):
        # some summaries don't have a date, but should still be recognized.
        tl = ticketloader.TicketLoader("../testdata/sum-WestVirginia-2.txt")
        for raw in tl.tickets:
            type = recognize.recognize_as(raw, "WestVirginia")
            found = (type == "summary") and "WestVirginia" or None
            self.assertEquals(found, "WestVirginia", raw)

    def test_recognize_WestVirginiaAEP_summaries(self):
        tl = ticketloader.TicketLoader("../testdata/fwp4-audit.txt")
        for raw in tl.tickets:
            type = recognize.recognize_as(raw, "WestVirginiaAEP")
            found = (type == "summary") and "WestVirginiaAEP" or None
            self.assertEquals(found, "WestVirginiaAEP", raw)

    def test_recognize_Washington2_summaries(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Washington2-new.txt")
        for raw in tl.tickets:
            type = recognize.recognize_as(raw, "Washington2")
            found = (type == "summary") and "Washington2" or None
            self.assertEquals(found, "Washington2", raw)

    def test_Houston2_summaries(self):
        tl = ticketloader.TicketLoader("../testdata/FHL2.txt")
        raw = tl.tickets[0] # first one is the summary
        self.assertEquals(recognize.recognize_summary(raw), "Houston2")

    def test_QWEST_summaries(self):
        tl = ticketloader.TicketLoader("../testdata/sum-qwest-1.txt")
        for raw in tl.tickets:
            z = recognize.recognize_summary(raw)
            self.assert_(z in ["QWEST_IDL", "QWEST_OOC", "QWEST_IEUCC",
                               "QWEST_UULC", "QWEST_WOC"])

    def test_HarlingenTESS_summaries(self):
        tl = ticketloader.TicketLoader("../testdata/sum-HarlingenTESS.txt")
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, "HarlingenTESS")
            self.assertEquals(z, "summary")

    def test_Colorado_summaries_new(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Colorado-3.txt")
        for raw in tl.tickets:
            z = recognize.recognize_summary(raw)
            self.assert_(z in ['Colorado'])

    test_Colorado_summaries_new.fails=1

    def test_Arkansas_summaries(self):
        tl = ticketloader.TicketLoader("../testdata/sum-Arkansas.txt")
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'Arkansas')
            self.assertEquals(z, 'summary')

        tl = ticketloader.TicketLoader("../testdata/sum-Arkansas-2.txt")
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'Arkansas')
            self.assertEquals(z, 'summary')

    def test_NewAtlanta_Summaries(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","GA_summaries","3003-2008-12-01-00001.txt"))
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'Atlanta4')
            self.assertEquals(z, 'summary')

    def test_Tennessee_Summaries_1(self):
        tickets = glob(os.path.join("../testdata", "sample_tickets", "TNOCS",
                  "*-2009-01-20-*.txt"))
        for ticket in tickets:
            tl = ticketloader.TicketLoader(ticket)
            for raw in tl.tickets:
                z = recognize.recognize_as(raw, 'Tennessee')
                self.assertEquals(z, 'summary')
                z = recognize.recognize_as(raw, 'TN3005')
                self.assertEquals(z, 'summary')

    def test_Tennessee_Summaries_2(self):
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets","TNOCS","Audit-2008-12-19-00121.txt"))
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'Tennessee')
            self.assertEquals(z, 'summary')
            z = recognize.recognize_as(raw, 'TN3005')
            self.assertEquals(z, 'summary')

    def test_DIGGTESS_Summaries(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","FHL","FHL2-2009-01-13-00001.txt"))
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'DIGGTESS')
            self.assertEquals(z, 'summary')

    def test_VUPSNewOCC3_2_Summaries(self):
        tl = ticketloader.TicketLoader(os.path.join("..","testdata","sample_tickets","occ3","OCC3-2009-03-19-00918.txt"))
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'VUPSNewOCC3_2')
            self.assertEquals(z, 'summary')

        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets","occ3","OCC3-2009-03-19-00919.txt"))
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'VUPSNewOCC3_2')
            self.assertEquals(z, 'summary')

    def test_MississippiKorterra_Summaries(self):
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets","FJL4","TEST-2009-03-31-00026.txt"))
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'MississippiKorterra')
            self.assertEquals(z, 'summary')
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets","FJL4","TEST-2009-03-31-00027.txt"))
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'MississippiKorterra')
            self.assertEquals(z, 'summary')
        tl = ticketloader.TicketLoader(os.path.join("..", "testdata",
             "sample_tickets","FJL4","TEST-2009-03-31-00028.txt"))
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'MississippiKorterra')
            self.assertEquals(z, 'summary')

    def test_NCA_no_response(self):
        tl = ticketloader.TicketLoader(os.path.join("../testdata",
             "sample_tickets","NCA1","nca1-2009-06-17-01398.txt"))
        raw = tl.getTicket()
        x = recognize.recognize_gm_message(raw)
        self.assertEquals(x, 'NorthCalifornia')
        handler = ticketparser.TicketHandler()
        obj = handler.parse(raw, ['NorthCalifornia',])
        self.assertEquals(isinstance(obj, ticketparser.GMMessage), 1)
        self.assertEquals(obj.format, 'NorthCalifornia')

    def test_GeorgiaXML_message(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
         'messages', 'GeorgiaXML-2013-11', 'sample_message.xml'))
        raw = tl.getTicket()
        x = recognize.recognize_gm_message(raw)
        self.assertEquals(x, 'GeorgiaXML')
        handler = ticketparser.TicketHandler()
        obj = handler.parse(raw, ['GeorgiaXML',])
        self.assertTrue(isinstance(obj, ticketparser.GMMessage))
        self.assertEquals(obj.format, 'GeorgiaXML')
        self.assertTrue('This is a message we created for testing GeorgiaXML' in
         obj.image)

    def test_recognize_as(self):
        files = [
            ("sum-Atlanta.txt", ["Atlanta"]),
            ("Atlanta-1.txt", ["Atlanta"]),
            ("Pennsylvania-1.txt", ["Pennsylvania"]),
            #("FCL2.txt", call_centers.cc2format["FCL2"]),
            # FCL2 contains Atlanta tickets as well, which *used* to work, but
            # isn't necessary anymore now
        ]
        for (file, formats) in files:
            fullname = os.path.join("../testdata", file)
            tl = ticketloader.TicketLoader(fullname)
            for ticket in tl.tickets:
                for format in formats:
                    z = recognize.recognize_as(ticket, format)
                    if z:
                        break
                else:
                    self.fail("Failed: " + repr(formats))

        # also test that it fails when it should
        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        ticket = tl.tickets[0]  # Atlanta or SouthCarolina
        z = recognize.recognize_as(ticket, "Pennsylvania")  # should fail
        self.assertEquals(z, "")

    def test_spam_1(self):
        tickets = glob(os.path.join("../testdata","sample_tickets","NCA2","nca2-2009-06*.txt"))
        for ticket in tickets:
            tl = ticketloader.TicketLoader(ticket)
            for raw in tl.tickets:
                z = recognize.recognize_type(raw)
                if z is not None:
                    self.assertEquals(z, 'summary')

    def test_SC1421_Summaries(self):
        tickets = glob(os.path.join("../testdata","sample_tickets","1421","1421-2009-07-07-*.txt"))
        for ticket in tickets:
            tl = ticketloader.TicketLoader(ticket)
            for raw in tl.tickets:
                z = recognize.recognize_as(raw, 'SC1421')
                self.assertEquals(z, 'summary')

    def test_1392_Summary(self):
        ticket = os.path.join("../testdata","sample_tickets","1392","1392-2009-07-09-00002.txt")
        tl = ticketloader.TicketLoader(ticket)
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'MS1392')
            self.assertEquals(z, 'summary')

    def test_FJL1_Summary(self):
        ticket = os.path.join("../testdata","sample_tickets","FJL1","FJL1-2009-07-09-00002.txt")
        tl = ticketloader.TicketLoader(ticket)
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'Mississippi2')
            self.assertEquals(z, 'summary')

    def test_FMS1_Summary(self):
        ticket = os.path.join("../testdata","sample_tickets","FMS1","FMS1-2009-07-09-00002.txt")
        tl = ticketloader.TicketLoader(ticket)
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'Mississippi')
            self.assertEquals(z, 'summary')

    def test_1422_Summary(self):
        ticket = os.path.join("../testdata","sample_tickets","FMS1","FMS1-2009-07-09-00002.txt")
        tl = ticketloader.TicketLoader(ticket)
        for raw in tl.tickets:
            z = recognize.recognize_as(raw, 'Mississippi')
            self.assertEquals(z, 'summary')

    def test_xml_tickets(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "sample_tickets", "xml_tickets", "TEST-2008-10-27-00001-A01.xml"))
        raw = tl.tickets[0]
        # this XML should not have whitespace surrounding it
        self.assertEquals(raw, raw.strip())
        rec = recognize.XmlTicketRecognize("./Ticket/CallCenter",
              ["Oregon One Call", "Washington One Call"])
        self.assertTrue(rec.search(raw))

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "FairfaxXML-2010-05-06", "TEST-2010-03-23-00076.txt"))
        raw = tl.tickets[0]
        rec = recognize.XmlTicketRecognize("./delivery/center",
              ["VUPSa", "VUPSb", "VUPSc"])
        self.assertTrue(rec.search(raw))

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "FairfaxXML-2010-05-06", "OCC2-2010-06-04-00328.txt"))
        raw = tl.tickets[0]
        self.assertTrue(tools.is_xml(raw))
        rec = recognize.XmlTicketRecognize("./delivery/center",
              ["VUPSa", "VUPSb", "VUPSc"])
        self.assertTrue(rec.search(raw))



if __name__ == "__main__":
    unittest.main()


