# test_work_order_audit.py

import site; site.addsitedir('.')
import unittest
#
from work_order_audit import WorkOrderAuditHeader, WorkOrderAuditDetail
import _testprep
import ticket_db

class TestWorkOrderAudit(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

    def test_saving_etc(self):
        woa = WorkOrderAuditHeader()
        woa.set(summary_date='2011-01-01 00:00:00', wo_source='LAM01')

        # make up a few detail entries
        for i in range(5):
            woad = WorkOrderAuditDetail()
            woad.client_wo_number = 'V1234500' + str(i)
            woa.entries.append(woad)

        # store the audit
        woh_id, wod_ids = woa.insert(self.tdb)
        self.assertEquals(len(wod_ids), 5)

        woa2 = WorkOrderAuditHeader.load(self.tdb, woh_id)
        self.assertEquals(woa2.wo_audit_header_id, woh_id)
        self.assertEquals(len(woa2.entries), 5)
        woa2.entries.sort(key=lambda e: e.client_wo_number)
        for i in range(5):
            self.assertEquals(woa2.entries[i].client_wo_number,
                              woa.entries[i].client_wo_number)
            self.assertEquals(woa2.wo_audit_header_id, woh_id)

if __name__ == "__main__":
    unittest.main()

