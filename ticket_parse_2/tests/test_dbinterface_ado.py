# test_dbinterface_ado.py
# Created: 2002.10.10, HN

import os
import site; site.addsitedir('.')
import string
import ticket_db
import ticketparser
import time
import unittest
#
import dbinterface
import dbinterface_ado
import sqlmaptypes as smt
import ticket as ticketmod
import ticketloader
import tools
import _testprep

class TestDBInterfaceADO(unittest.TestCase):

    def setUp(self):
        self.dba = dbinterface_ado.DBInterfaceADO()

    def test_001_connection(self):
        """ Test if we can make a connection """
        mydba = dbinterface_ado.DBInterfaceADO()
        self.assertTrue(mydba._dbdata.has_key('host'))

    def test_002_simple_sql(self):
        """ Test simple SQL (ADO) """
        sql = "select top 10 * from client order by client_id"
        results = self.dba.runsql_result(sql)
        self.assertEquals(len(results), 10)
        self.assertEquals(results[0]["oc_code"], "USW01")
        self.assertEquals(results[1]["oc_code"], "YIP01")

    def test_003_tickets_to_route(self):
        """ Test results of tickets_to_route (ADO) """
        # for reliable results, I need to make sure there's a routable
        # ticket in the database
        tdb = ticket_db.TicketDB()
        _testprep.clear_database(tdb)

        tl = ticketloader.TicketLoader("../testdata/Atlanta-1.txt")
        raw = tl.getTicket()
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw)
        tdb.insertticket(t)

        sql = "exec tickets_to_route_noxml"
        results = self.dba.runsql_result(sql)
        self.assertEquals(len(results), 13)

        # test types of values -- should all be strings
        for key, value in results[0].items():
            # None is allowed, but the other values should be strings
            if value is not None:
                self.assertEquals(type(value), type(""))

        row = results[0]
        self.assertEquals(row["ticket_number"], "12311-109-056")
        self.assertEquals(row["transmit_date"], "2001-12-31 10:14:39")

    def test_004_stored_proc(self):
        """ Test calling of stored proc (ADO) """
        tdb = ticket_db.TicketDB()
        emp_id = _testprep.add_test_employee(tdb, 'Huub')
        area_id = _testprep.add_test_area(tdb, 'Test004', emp_id)
        tdb.delete('map_trs', township='01N', range='071W', section=10,
          basecode='CO')
        _testprep.add_test_township(tdb, '01N', '071W', '10', area_id, 'CO',
         'FCO1')

        rows = self.dba.call_result("find_township_baseline_2", "CO", "01N",
         "071W", 10, 'FCO1')
        self.assert_(rows, "No rows returned")
        self.assertEquals(rows[0]['area_id'], area_id)
        self.assertEquals(rows[0]['emp_id'], emp_id)
        self.assertEquals(rows[0]['short_name'], 'Huub')
        self.assertEquals(rows[0]["map_id"], '600') # default map

    def test_005_ticket(self):
        """ Test retrieval of ticket (ADO) """
        tdb = ticket_db.TicketDB()
        _testprep.clear_database(tdb)
        t = ticketmod.Ticket()
        t.image = "blah"
        t.ticket_number = "12345"
        t.ticket_format = "test005"
        tdb.insertticket(t)

        sql = """
         select * from ticket
         where ticket_format = 'test005'
        """
        # there are NULL values for dates, but that should not cause an error:
        rows = self.dba.runsql_result(sql)
        self.assert_(rows)
        #self.assertEquals(rows[0]["legal_good_thru"], "1900-01-01 00:00:00")
        # the legal_good_thru field is never used
        self.assertEquals(rows[0]["call_date"], None)
        # some values are coerced to 1900-01-01, some are just None

    def test_006_multiple_result_sets(self):
        # drop test_multiple_result_sets stored proc, then recreate it
        sql = """
            if object_id('dbo.test_multiple_result_sets') is not null
                drop procedure dbo.test_multiple_result_sets
        """
        self.dba.runsql(sql)

        sql = """
            CREATE Procedure dbo.test_multiple_result_sets as
            set nocount on

            select top 10 * from client
            select top 10 * from employee
            select top 10 * from ticket

            GO
        """
        self.dba.runsql(sql)

        sets = self.dba.runsql_result_multiple("exec test_multiple_result_sets")
        self.assertEquals(len(sets), 3)

        self.assertEquals(len(sets[0]), 10)
        self.assertEquals(len(sets[1]), 10)
        # the length of sets[2] depends on previous tests

    def test_007_runsql_with_parameters(self):
        # the USW01 client is no longer active in the database, make it active
        sql = "select call_center "\
         "from client where oc_code = 'USW01'"
        result_USW01 = self.dba.runsql_result(sql)[0]

        _testprep.set_client_active(self.dba, result_USW01['call_center'], "USW01")

        sql = "select * from client where call_center = ?"
        rows = self.dba.runsql_with_parameters(sql, ["FCO1"])
        self.assert_(len(rows))
        self.assertEquals(rows[0]['call_center'], 'FCO1')

        # this works as long as there is an active client USW01
        # if this test starts failing, create a client record yourself.
        sql = "select * from client where oc_code = ? and active = ?"
        rows = self.dba.runsql_with_parameters(sql, ["USW01", 1])
        self.assertEquals(len(rows[:1]), 1)
        # there should be at least one

        # Set it back inactive
        _testprep.set_client_inactive(self.dba, result_USW01['call_center'], "USW01")

        return

    def test_008_runsql_with_parameters_moeilijk(self):
        # delete old test records
        self.dba.deleterecords("client", oc_code="BLAH01")

        # try an insert
        sql = """
         insert client (client_name, oc_code, office_id, active, call_center)
         values (?, ?, ?, ?, ?)
        """
        #values = ["blah", "BLAH01", 100, 1, "FCO1"]
        values = [
            ("blah", smt.sqlstring(40), 'client_name'),
            ("BLAH01", smt.sqlstring(10), 'oc_code'),
            (100, smt.sqlint, 'office_id'),
            (1, smt.sqlbool, 'active'),
            ("FCO1", smt.sqlstring(20), 'call_center'),
        ]
        self.dba.runsql_with_parameters(sql, values)

        # check if the record was actually posted
        rows = self.dba.getrecords("client", oc_code="BLAH01")
        self.assertEquals(len(rows), 1)
        self.assertEquals(rows[0]['client_name'], "blah")
        self.assertEquals(rows[0]['active'], '1')
        self.assertEquals(rows[0]['office_id'], '100')
        self.assertEquals(rows[0]['call_center'], 'FCO1')

    def test_009_multiple_insert(self):
        # delete old test records
        self.dba.deleterecords("client", oc_code="BLAH01")

        sql = """
         insert client (client_name, oc_code, office_id, active, call_center)
         values (?, ?, ?, ?, ?)
         insert client (client_name, oc_code, office_id, active, call_center)
         values (?, ?, ?, ?, ?)
        """
        values = [
            ("blah", smt.sqlstring(40), 'client_name'),
            ("BLAH01", smt.sqlstring(10), 'oc_code'),
            (100, smt.sqlint, 'office_id'),
            (1, smt.sqlbool, 'active'),
            ("FCO1", smt.sqlstring(20), 'call_center'),
            ("blah", smt.sqlstring(40), 'client_name'),
            ("BLAH01", smt.sqlstring(10), 'oc_code'),
            (100, smt.sqlint, 'office_id'),
            (1, smt.sqlbool, 'active'),
            ("FCO1", smt.sqlstring(20), 'call_center'),
        ]
        self.dba.runsql_with_parameters(sql, values)

        rows = self.dba.getrecords("client", oc_code="BLAH01")
        self.assertEquals(len(rows), 2)

    def test_010_bogus_date_on_ticket(self):
        tdb = ticket_db.TicketDB()
        _testprep.clear_database(tdb)

        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "tickets", "broken", "FMB1-2012-08-27-00411.txt"))
        raw = tl.getTicket()
        handler = ticketparser.TicketHandler()
        t = handler.parse(raw, suggested_formats=['Baltimore'],
                          call_center='FMB1')
        self.assertEquals(t.work_date, '2000-12-13 20:19:00')
        self.assertEquals(t.call_date, '2096-09-05 16:29:00') # !
        tdb.insertticket(t)

        # retrieving the ticket should not fail
        rows = tdb.runsql_result("select * from ticket", as_strings=False)
        rows = tdb.runsql_result("select * from ticket")

    test_010_bogus_date_on_ticket.has_error=1

    def test_011_timeout(self):
        dba = dbinterface_ado.DBInterfaceADO()

        # Check if timeout gets set from config.xml
        self.assertEquals(dba.timeout, 581)

        # Check if timeout works
        dba.timeout = 1
        start_time = time.time()
        try:
            dba.runsql("WaitFor Delay '00:00:10'")
        except dbinterface.TimeoutError as e:
            elapsed_time = time.time() - start_time
            self.assertEquals(e.args[1]['timeout_seconds'], 1)
            self.assertTrue(e.args[1]['elapsed_seconds'] >= 1)
            self.assertTrue(elapsed_time >= 1)
        else:
            self.assert_(False, "TimeoutError not raised")

if __name__ == "__main__":

    unittest.main()

