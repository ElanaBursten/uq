# test_main_FHL3.py

import os
import site; site.addsitedir('.')
import string
import unittest
#
import _testprep
import call_centers
import config
import filetools
import main
import ticket_db
import ticketloader
import ticketparser

from mockery_tools import *
from test_main import drop
from test_archiver import create_test_dirs, clear, delete_test_dirs

class TestFHL3(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        self.handler = ticketparser.TicketHandler()
        _testprep.clear_database(self.tdb)

    def prepare(self):
        # replace the real configuration with our test data
        self.conf_copy.processes = [
         {'incoming': 'test_incoming_FHL3',
          'format': 'FHL3',
          'processed': 'test_processed_FHL3',
          'error': 'test_error_FHL3',
          'attachments': ''},]

        self.TEST_DIRECTORIES = []

        # Make sure the right directories are created
        for process in self.conf_copy.processes:
            for key in ['incoming','processed', 'error']:
                direc = process.get(key,'')
                if direc and not direc in self.TEST_DIRECTORIES:
                    self.TEST_DIRECTORIES.append(direc)
        create_test_dirs(self.TEST_DIRECTORIES)

        self.tdb.runsql("""
         update call_center
         set active = 1
         where cc_code in ('FHL1', 'FHL2', 'FHL3');
        """)

        ucc = call_centers.get_call_centers(self.tdb)
        ucc.reload()

    def tearDown(self):
        delete_test_dirs(self.TEST_DIRECTORIES)

    @with_config_copy
    def test_FHL3_1(self):
        self.prepare()

        # make sure FHL3 is recognized as an update call center
        self.tdb.runsql("""
          update client
          set active = 1
          where call_center in ('FHL1', 'FHL2')
          or update_call_center = 'FHL3'
        """)

        cc = call_centers.get_call_centers(self.tdb)
        cc.reload()

        raw = filetools.read_file(
              os.path.join("..", "testdata", "sample_tickets", "FHL",
                           "FHL3-2008-12-11-00001.txt"))

        drop("FHL3.txt", raw, dir='test_incoming_FHL3')

        # now let main.py run and see what happens...
        m = main.Main(verbose=0, call_center='FHL3')
        m.log.lock = 1
        m.run(runonce=1)

        # what's the id of the ticket we just posted?
        id = m.ids[-1]

        # Get the ticket
        t2 = self.tdb.getticket(id)
        self.assertEquals(t2.source, 'FHL3')
        self.assertEquals(t2.ticket_format, 'FHL2')

if __name__ == "__main__":
    unittest.main()


