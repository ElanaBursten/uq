# test_work_order.py

import os
import site; site.addsitedir('.')
import unittest
#
from formats import Lambert
import ticket_db
import ticketloader
import work_order
import _testprep

class TestWorkOrder(unittest.TestCase):

    def setUp(self):
        self.tdb = ticket_db.TicketDB()
        _testprep.clear_database(self.tdb)

    def test_dummy(self):
        wo = work_order.WorkOrder()

    def test_saving_empty(self):
        # try to store a WorkOrder with as few fields set as possible.
        wo = work_order.WorkOrder()
        wo.client_wo_number = '123456'
        wo.wo_number = '23456'
        wo.wo_source = 'LAM01'
        wo.kind = 'NORMAL'
        wo.status = 'X'
        wo.closed = 0
        wo.transmit_date = wo.due_date = '2011-01-01 13:00:00'
        wo.image = 'bah<x>\'"'
        wo.work_lat = 33.33
        tdb = ticket_db.TicketDB()
        wo_id = wo.insert(tdb)

        # check that we have a valid id
        self.assertTrue(wo_id)

        # retrieve the record and verify the contents
        wo2 = work_order.WorkOrder.load(tdb, wo_id)
        self.assertEquals(wo2.wo_number, '23456')

    def test_saving(self):
        tl = ticketloader.TicketLoader(os.path.join(_testprep.TICKET_PATH,
             "work-orders", "LAM01", "LAM01-2011-03-07-00009-A01.txt"))
        raw = tl.tickets[0]
        wop = Lambert.WorkOrderParser()
        wo = wop.parse(raw)

        tdb = ticket_db.TicketDB()
        #wo.debug = 1
        wo_id = wo.insert(tdb)

        self.assertTrue(wo_id)

        wo2 = work_order.WorkOrder.load(tdb, wo_id)
        self.assertEquals(wo2.wo_number, 'LAMV0002850')


if __name__ == "__main__":
    unittest.main()

