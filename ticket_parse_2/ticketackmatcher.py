# ticketackmatcher.py

import string

class TicketAckMatcher:
    """ Gets the contents of the ticket_ack_match table.  Allows to do
        matches on these data, to determine whether a ticket goes to
        the "emergency bucket" (ticket_ack table) or not. """
    _data = None
    def __init__(self, tdb):
        self.tdb = tdb

        # read the rows, creating a dictionary with keys for each call
        # center
        if TicketAckMatcher._data is None:
            self.get_data()
        self.data = TicketAckMatcher._data

    def get_data(self):
        TicketAckMatcher._data = {}
        rows = self.tdb.getrecords("ticket_ack_match")
        for row in rows:
            key = row['call_center']
            value = string.lower(row['match_string'])
            TicketAckMatcher._data[key] = TicketAckMatcher._data.get(key, []) + [value,]

    def get_values(self, call_center):
        try:
            return self.data[call_center]
        except KeyError:
            # Perhaps the call center has just been added
            self.get_data()
            self.data = TicketAckMatcher._data
            try:
                return self.data[call_center]
            except KeyError:
                return []

    def is_ticket_ack(self, call_center, ticket_type):
        #print "# trying to match:", `call_center`, `ticket_type`
        ticket_type = string.lower(ticket_type)
        values = self.get_values(call_center)
        for value in values:
            if string.find(ticket_type, value) > -1:
                return 1
        return 0

    def get_work_priority(self, call_center, ticket_type):
        """
        Get the work priority associated with this call center/ticket type
        """
        ticket_type = string.lower(ticket_type)
        values = self.get_values(call_center)
        for value in values:
            if string.find(ticket_type, value) > -1:
                rows = self.tdb.getrecords("ticket_ack_match",
                                           call_center=call_center,
                                           match_string=value)
                work_priority = rows[0]['work_priority']
                if work_priority is not None:
                    work_priority = int(work_priority)
                return work_priority
        return None
