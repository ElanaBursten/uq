# parser_tools.py
# Selected tools for parsing tickets.

# TO BE ADDED
# - separate number (if any) from street address
# - parse a date in the MM/DD/YY format
# - ...

def nca_isemergency(ticket):
    return ticket["kind"] == "EMERGENCY" \
        or ticket.get("priority", "") == 0 \
        or ticket["ticket_type"].find("PRIORITY NOTICE") > -1 \
        or ticket["ticket_type"].find("RUSH") > -1 \
        or ticket["ticket_type"].find("EMER") > -1
