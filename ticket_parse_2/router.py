# router.py
# Created: 2002.11.06
# XXX probably obsolete

# Mantis #945:
# We need an object that takes a Ticket and routes it, i.e. finds locators
# for the locates, sets their status, adds assignments, etc.
# All we have to do then to store the changes is a Ticket.update().
#
# This code is old and probably needs to be rewritten or at least
# refactored...

import assignment
import businesslogic
import locate_status
import sqlmap
import ticketparser
import tools

class Router:

    def __init__(self, tdb, log, holiday_list, clients, verbose=1):
        self.tdb = tdb
        self.log = log
        self.logic = {}
        self.clients = clients  # a dict with a list of Client instances per
                                # call center
        self.holiday_list = holiday_list
        self.verbose = verbose

    def getlogic(self, call_center):
        """ Get a BusinessLogic instance from the collection dict, or create
            and add one first if necessary. """
        # We use our own getlogic(), rather than relying on the caller's (if
        # any).
        if self.logic.has_key(call_center):
            return self.logic[call_center]
        else:
            logic = businesslogic.getbusinesslogic(call_center, self.tdb,
                    self.holiday_list)
            self.logic[call_center] = logic
            return logic

    ###
    ### the main routine...

    def route(self, ticket, whodunit=""):
        """ Route a ticket, i.e. inspect its locates, and if they're
            unassigned, attempt to assign a locator to them. We change the
            Ticket in-place.
        """
        call_center = ticket.ticket_format
        logic = self.getlogic(call_center)
        assigned_locator = self.find_existing_locator(ticket)

        for locate in ticket.locates:
            if not locate._assignment \
            and locate.status == locate_status.blank:
                if self.is_a_client(locate.client_code, call_center):
                    if assigned_locator:
                        # if we already assigned a locator, use that
                        self.assign(locate, assigned_locator, whodunit)
                        s = "Ticket #%s (%s), locate %s -> "\
                         "existing locator %s" % (
                         ticket.ticket_number, ticket.ticket_format,
                         locate.client_code, locate._assignment.locator_id)
                        self.log.log_event(s)
                    else:
                        # try to assign a locator
                        row = self.fake_row(ticket, locate)
                        rr = logic.locator(row)
                        self.assign(locate, rr.locator, whodunit)
                        s = "Ticket #%s (%s), locate %s -> locator %s (%s)" % (
                         ticket.ticket_number, ticket.ticket_format,
                         locate.client_code, locate._assignment.locator_id,
                         rr.routing_method or "default")
                        self.log.log_event(s, self.verbose)

                        # set route_area_id and map_page, if appropriate
                        if rr.area_id and not ticket.route_area_id:
                            ticket.route_area_id = rr.area_id
                        if rr.mapinfo and not ticket.map_page:
                            ticket.map_page = rr.mapinfo

                else:
                    # set status to 'not a client'
                    locate.status = locate_status.not_a_customer

    def find_existing_locator(self, ticket):
        """ Try to find an existing locator in the ticket's locates. """
        for locate in ticket.locates:
            if locate._assignment:
                return locate._assignment.locator_id
        return None

    def assign(self, locate, locator, whodunit):
        theassignment = assignment.Assignment(locator)
        theassignment.added_by = whodunit
        locate._assignment = theassignment
        locate.status = locate_status.assigned

    def fake_row(self, ticket, locate):
        """ Creates a fake "row" dictionary for use with businesslogic.locator.
            Mostly an ugly HACK to make this work with existing, old code.
            Should be refactored out later, when/if we don't need the old
            way of routing anymore.
        """
        d = {}
        if isinstance(ticket, sqlmap.SQLMap):
            ticket_fields = [f[0] for f in ticket.__fields__]
        else:
            ticket_fields = tools.get_fields(ticket)
        for attrname in ticket_fields:
            d[attrname] = getattr(ticket, attrname)

        if isinstance(locate, sqlmap.SQLMap):
            locate_fields = [f[0] for f in locate.__fields__]
        else:
            locate_fields = tools.get_fields(locate)
        for attrname in locate_fields:
            d[attrname] = getattr(locate, attrname)
        return d

    def prepare(self, ticket):
        """ Prepare a ticket (merged or not) for routing. This sets all the
            locates with status 'not a customer' to 'blank', and after that,
            sets the _existing attributes of all locates that are not blank.

            The router doesn't care about the _existing attribute, but the
            updateticket_2 code does.
        """
        for loc in ticket.locates:
            if loc.status == locate_status.not_a_customer:
                loc.status = locate_status.blank

        for loc in ticket.locates:
            if loc.status != locate_status.blank:
                loc._existing = 1

    def is_a_client(self, client_code, call_center):
        clients = self.clients.get(call_center, [])
        for client in clients:
            if client.client_code == client_code:
                return 1
        return 0

    def sync(self, ticket, oldticket):
        """ Check locates of ticket. If these exist in oldticket too, set the
            locate's locate_id to that of the appropriate locate in oldticket.
            <oldticket> is retrieved from the database and is supposed to have
            locates with locate_ids. <ticket> is a new ticket and is *not*
            supposed to have these.
        """
        for locate in ticket.locates:
            if not locate.locate_id:
                for oldlocate in oldticket.locates:
                    if oldlocate.client_code == locate.client_code:
                        locate.locate_id = oldlocate.locate_id
                        print "ni!"
                        break   # out of inner loop

