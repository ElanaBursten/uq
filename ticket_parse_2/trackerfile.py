# trackerfile.py

import datadir
import date

class TrackerFile:
    """ Manage a "tracker file", a file type similar to an INI file, but
        storing only keys and timestamps, as lines of the form name=value.
    """

    def __init__(self, filename):
        self.data = {}
        self.filename = filename
        self.read(filename)

    def read(self, filename):
        try:
            f = datadir.datadir.open(filename, 'r')
        except IOError:
            pass
        else:
            lines = f.readlines()
            for line in lines:
                if line.strip():
                    key, value = line.strip().split("=")
                    self.data[key] = value

    def get(self, key):
        """ Return the timestamp for a given key, or 1 Jan 1980 if not
            found. """
        return self.data.get(key, "1980-01-01 00:00:00")
        # (using a date that the time module can grok)

    def update(self, key, timestamp=None):
        """ Set the timestamp for a given key.  If no timestamp is specified,
            use the current date/time. """
        self.data[key] = timestamp or date.Date().isodate()

    def save(self):
        """ Write tracker changes back to file. """
        f = datadir.datadir.open(self.filename, 'w')
        for key, value in self.data.items():
            f.write("%s=%s\n" % (key, value))
        f.close()


if __name__ == "__main__":

    tf = TrackerFile("test-tracker.txt")
    tf.update("foo")
    tf.update("bar")
    tf.save()

    tf2 = TrackerFile("test-tracker.txt")
    print tf2.data

    import file_tools
    filetools.remove_file("test-tracker.txt")

