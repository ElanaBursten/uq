# ftpresponder_data.py

import traceback
import time
#
import abstractresponderdata
import callcenters
import errorhandling2
import tools

class FTPResponderError(Exception):
    pass

class FTPResponderAck(tools.Struct):
    """ Holds relevant data from an acknowledgement sent by a responder
        server.  These are the fields we actually need; any other fields in
        the ack file will be ignored (by our code). """
    __fields__ = ['locate_id', 'resp_id', 'result', 'raw', 'date', 'time',
     'ticket_number', 'term_id'] 

class FTPResponderData(abstractresponderdata.AbstractResponderData):
    """ Holds and defines ways to deal with data sent to and received from the
        FTP server. """

    # by default, we use methods that work for the "regular" FTP responder
    # (as used by FMW1, FMB1, etc)

    TEMPLATE1 = "###BODY###"

    TEMPLATE2 = "%(ticket_number)s%(status)s%(company)s\n"

    LEN_TICKET_NUMBER = 8

    store_resp_context = True
    delete_ack = True # delete ack file from server when done

    def template(self):
        """ Override if template is more complex than simple string subst. """
        return self.TEMPLATE2

    def make_line(self, namespace):
        """ Return a "section" containing a response.  Usually this is just
            a matter of string interpolation, but it can be overridden to
            handle more complex scenarios. """
        return self.template() % namespace

    def adapt_ticket_number(self, ticket_number):
        if not self.LEN_TICKET_NUMBER:
            return ticket_number # don't adapt
        while len(ticket_number) < self.LEN_TICKET_NUMBER:
            ticket_number = '0' + ticket_number
        if len(ticket_number) > self.LEN_TICKET_NUMBER:
            return None
        return ticket_number

    def extra_names(self, namespace):
        return {}

    def extra_data(self, namespace):
        """ Override to pass extra variables necessary for the template
            substitution. """
        return {}

    def gen_filename(self):
        """ Generate a unique filename for the file that will be stored on
            the server. """
        t9 = time.localtime(time.time())
        year, month, day, hours, minutes, seconds = t9[:6]
        username = self.login
        file_ext = self.file_ext or '.rsp'
        if not file_ext.startswith("."):
            file_ext = "." + file_ext
        name = '%s_%02d%02d%02d_%02d%02d%02d%s' % (
               username, year, month, day, hours, minutes, seconds, file_ext)
        return name

    @staticmethod
    def is_response_file(filename):
        return filename.endswith(".out")

    def get_acknowledgements(self, data, locates):
        """ Get a list of locates that were acknowledged by the server. <data>
            is the raw data of the acknowledgement file found on the server.
            <locates> is an optional list of locate dicts (only present when
            we use pickling to store state; otherwise empty).

            Returns a tuple (ok, errors) where <ok> is a list of FTPResponderAck
            objects, containing the acknowledgements, and <error> is a list
            of ErrorPackets containing caught exceptions, that can be logged
            by the caller.
        """
        lines = data.split('\n')
        lines = [l for l in lines if l.strip()]

        ok, errors = [], []

        for i in range(len(lines)):
            try:
                line = lines[i].strip()
                locate = locates[i]
                parts_before = line.split('\t')
                parts = map(tools.remove_crud_2, parts_before) # rmv line noise
                ticket_number, client_code, resp, date, time, result = parts[:6]

                if self.retry(result):
                    raise ValueError('Result indicates a retry is needed')

                line2 = tools.remove_crud_2(line.replace('\t', ''))
                ack = FTPResponderAck(locate_id=locate['locate_id'],
                                      resp_id=locate['resp_id'],
                                      result=result,
                                      raw=line2,
                                      date=date,
                                      time=time)
                ok.append(ack)
            except:
                ep = errorhandling2.errorpacket(parts=parts, locate=locate,
                     lines=repr(lines))
                errors.append(ep)
                continue

        return ok, errors

    @staticmethod
    def is_success(result_code):
        return result_code == '00'

    @staticmethod
    def retry(result_code):
        '''
        If result_code indicates that the acknowledgement failed
        and to re-try, return True
        '''
        return False

    def resend_response(self, result_code):
        '''
        If result_code indicates that the acknowledgement failed, and the
        response needs to be resent, return True.

        Note: This is not yet implemented in
        ftpresponder_data.FTPResponderData.get_acknowledgements, so it will not
        work yet for all FTP responders. See
        FDE1.FTPResponderData.get_acknowledgements for an example.
        '''
        return False

def get_responderdata_class(call_center, kind):
    """
    Return responderdata class. Used by tests.
    """
    
    # get 'prefix' version for numerical call centers (1181 -> c1181)
    prefix = call_center
    if prefix[0] in "01234567890":
        prefix = "c" + prefix

    # get responder registry for this call center
    reg = callcenters.get_registry(prefix)

    # find the right responder in the registry, taking into account ids that
    # might have been specified in the configuration info.
    klass = None
    for (_, type, rkind), rclass in reg.items():
        if type != 'ftp': continue
        if kind and kind != rkind: continue
        if rkind and kind != rkind: continue
        klass = rclass
        break

    return klass
        
def getresponderdata(config, call_center, raw_responderdata):
    """ Try to get and instantiate an FTPResponderData class for the given
        call center.  If not found, raise an FTPResponderError.  Looks
        for the given class in two ways: in the namespace of this module, and
        in the 'callcenters' package.
        NOTE: The first lookup method is obsolete now, but I'll leave it in
        because it's useful for testing.
    """
    # get 'prefix' version for numerical call centers (1181 -> c1181)
    prefix = call_center
    if prefix[0] in "01234567890":
        prefix = "c" + prefix

    # get responder registry for this call center
    reg = callcenters.get_registry(prefix)
    kind = raw_responderdata.get('kind', '') # is a 'kind' specified?


    '''
    prefix = call_center
    if prefix[0] in "01234567890":
        prefix = "c" + prefix
    klassname = prefix + "FTPResponderData"
    try:
        klass = globals()[klassname]
    except KeyError:
        try:
            klass = callcenters.get_ftpresponderdata(prefix)
        except:
            traceback.print_exc()
            raise FTPResponderError(
             "Could not find FTPResponderData class for %s" % call_center)
    '''

    # find the right responder in the registry, taking into account ids that
    # might have been specified in the configuration info.
    klass = None
    for (_, type, rkind), rclass in reg.items():
        if type != 'ftp': continue
        if kind and kind != rkind: continue
        if rkind and kind != rkind: continue
        klass = rclass
        break

    if klass is None: # should not happen, unless in tests!
        # old-style: only one FTPResponderData class per file
        klassname = prefix + "FTPResponderData"
        try:
            klass = globals()[klassname]
        except KeyError:
            try:
                klass = callcenters.get_ftpresponderdata(prefix)
            except:
                traceback.print_exc()
                raise FTPResponderError(
                 "Could not find FTPResponderData class for %s" % call_center)

    FIELDNAMES = ['exclude_states', 'file_ext', 'login', 'mappings', 'name',
                  'outgoing_dir', 'password', 'server', 'skip', 'temp_dir',
                  'translations', 'clients', 'send_emergencies', 'send_3hour',
                  'status_code_skip', 'method', 'parsed_locates_only']

    rd = klass()

    # inject data from configuration
    if not raw_responderdata:
        # get from config.xml -- for testing purposes only
        try:
            raw_responderdata = config.ftpresponders[call_center]
        except KeyError:
            raise KeyError, "config.xml: No FTP responder data for " + call_center

    for attr in FIELDNAMES:
        setattr(rd, attr, raw_responderdata.get(attr, ''))

    # override 'skip' to use values in ResponderConfigData
    rd.skip = config.responderconfigdata.get_skip_data(call_center)
    rd.status_code_skip = config.responderconfigdata.get_status_code_skip_data(call_center)

    return rd

