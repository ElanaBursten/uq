# call_centers.py

# call center -> format: A call center can have one or more formats associated
# with it. These formats will be used when a call center is fixed for a ticket
# (in the configuration file).
# NOTE: The first format in the list is used as key in format2cc.
cc2format = {

    "1101": ["NM1101"], # New Mexico STS
    "1102": ["AZ1102"], # ditto, although it's Arizona, actually :)
    "1103": ["TX1103"],

    "1181": ["WV1181Korterra", "WV1181", "WV1181Alt"],
            # Miss Utility of West Virginia

    "1391": ["TN1391"], # Memphis
    "1392": ["MS1392"],
    "1393": ["AR1393"], # Arkansas
    "1394": ["Kentucky", "KentuckyNew"],

    "1411": ["LA1411"], # Louisiana
    "1412": ["AR1412"], # based on FNL2/Arkansas

    "1421": ["SC1421PUPS", "SC1421", "SC1421A"], # South Carolina
    "1422": ["SC1422", "SC1422_ATT", "SC1422A"], # South Carolina
    "1423": ["SC1423"], # South Carolina
    "1424": ["SC1424"], # South Carolina/ANSCO
    "1425": ["SC1425", "SC1425A"],
    "1426": ["SC1426A"], # KORTERRA WINDSTREAM
    "1427": ["SC1427A"], # KORTERRA SCANA
    "1444": ["SC1444"], # South Carolina/ANSCO (prelim)

    "2051": ["SC2051"], # Greenville/ANSCO

    "2101": ["NC2101", "NC2101a"], # NC 811. Order is significant.

    "300":  ["GA3001"],
    "3002": ["GA3002"], # update call center
    "3003": ["GeorgiaXML", "GA3003", "GA3003a"], # MEET/DIG tickets
    "3004": ["GA3004", "GA3004a", "GA3004b"], # updates 3003
    "3005": ['TN3005'],
    "3006": ["GA3006"],
    "3007": ["GA3007"], # KORTERRA GA WINDSTREAM

    "6001": ["TX6001"],
    "6002": ["TX6002"],
    "6003": ["TX6003"],
    "6004": ["TX6004"],

    "7501": ["PA7501", "PA7501Meet"], # Pennsylvania

    "9001": ["FL9001"], # Florida STS
    "9002": ["FL9002"], # updates 9001
    "9003": ["FL9003"], # updates 9001

    "9101": ["NC9101"], # North Carolina STS
    "9102": ["NC9102"], # North Carolina Progressive Energy (update call center)

    "9301": ["VA9301"], # Virginia STS

    "9401": ["TN9401"], # Tennessee STS

    "Atlanta": ["Atlanta"],

    "FAM1": ["AlabamaMobile", "AlabamaMobileNew"],
    "FAM2": ["AlabamaFL"],

    "FAQ1": ["Albuquerque"],
    "FAQ2": ["Albuquerque2"],
    "FAQ3": ["AlbuquerqueAZ"],
    "FAQ4": ["AlbuquerqueTX"], # No longer a current call center

    "FAU2": ["Alabama", "AlabamaNew", "AL811"],
    "FAU3": ["Atlanta2"],
    "FAU4": ["Atlanta4", "Atlanta4a"],
    "FAU5": ["Atlanta5", "Atlanta5a", "Atlanta5b"],

    "FBL1": ["BatonRouge1"],
    "FBL2": ["BatonRouge3"],

    "FCL1": ["NorthCarolina", "NC811"],
    "FCL2": ["SouthCarolina"],
    "FCL3": ["NorthCarolina2"],

    "FCO1": ["Colorado", "ColoradoNE"], #"ColoradoAlt"],
    "FCO2": ["Colorado2"],

    "FCT1": ["Chattanooga"],
    "FCT2": ["ChattanoogaGA"],
    "FCT3": ["ChattanoogaNC", "TN811"],
    "FCT4": ["GA3004"],

    "FCV1": ["Richmond1", "Richmond3"],
    "FCV2": ["Richmond2"],
    "FCV3": ["Richmond3", "VUPSNewFCV3"],
    "FCV4": ["RichmondNC"],

    "FDE1": ["Delaware1"],
    "FDE2": ["Delaware2", "VUPSNewFDE2", "DelawareXML"],
    "FDE3": ["Delaware3"],
    "FDE5": ["Delaware5"],
    "FDE6": ["Delaware6"],
    "FDE7": ["Delaware7"],

    "FDX1": ["Dallas1", "DIGGTESS"],
    "FDX2": ["Dallas2New"],
    "FDX3": ["Arkansas"],
    "FDX4": ["Dallas4", "DIGGTESS"],

    "FEF1": ["Jacksonville"],
    "FEF2": ["Jacksonville2"],

    "FGV1": ["Greenville", "GreenvilleNew", "SC1422_ATT"],
    "FGV2": ["GreenvilleNC"],
    "FGV3": ["Greenville3"],

    # todo(dan) Is it ok to keep the old FHL1 formats?
    "FHL1": ["Houston2016", "Houston1", "Houston1Alt"],
    "FHL2": ["Houston3", "DIGGTESS"], # was: Houston2 (now deprecated)
    "FHL3": ["HoustonKorterra", "DIGGTESS"],
    "FJL1": ["Mississippi2"],
    "FJL2": ["MississippiLA"],
    "FJL3": ["MississippiTN"],
    "FJL4": ["MississippiKorterra"],

    "FJT1": ["Jackson"],    # Jackson, TN

    "FMB1": ["Baltimore"],
    "FMB2": ["Baltimore2"], # UCC used for Korterra audits
    "FMB3": ["Baltimore3"], # Verizon Screens tickets from Miss Utility
    "FMB4": ["Baltimore4"],
    "FMS1": ["Mississippi"],

    "FMW1": ["Washington"],
    "FMW2": ["Washington2", "WashingtonVA", "VUPSNewFMW2"],
    "FMW3": ["Washington3", "VUPSNewFMW3", "WashingtonXML"],
    "FMW4": ["Washington4"],
    "FMW5": ["Washington5"], # Verizon Screens tickets from Miss Utility
    "FMW6": ["Washington6"], # Verizon Screens MCI tickets from Miss Utility


    "FNL1": ["BatonRouge2"],

    "FNL2": ["Arkansas"],

    "FNV1": ["Nashville", "Nashville2013"],
    "FNV2": ["Kentucky", "KentuckyNew"],
    "FNV3": ["NashvilleKorterra"],
    "FNV4": ["NashvilleKorterra2013"],

    "FOK1": ["Tulsa", "TulsaWellsco"],

    "FOL1": ["Orlando"],

    "FOR1": ["Oregon"],

    "FPK1": ["Chesapeake1", "VUPSNewFPK1"],
    "FPK2": ["Chesapeake2"],

    "FPL1": ["Pensacola"],
    "FPL2": ["PensacolaAL"], # Alabama

    "FSA1": ["SanAntonio"],

    "FTL1": ["Tennessee"],
    "FTL2": ["TennesseeGA"],
    "FTL3": ["TennesseeNC"],
    "FTL4": ["Kentucky", "KentuckyNew"],
    "FTL5": ["TennesseeKorterra"],

    "FTS1": ["Harlingen", "HarlingenTESS"],

    "FWI1": ["Wisconsin1"],
    "FWI2": ["Wisconsin2", "Wisconsin2a"],

    "FWP1": ["WestVirginia"],
    "FWP2": ["Pennsylvania"],
    "FWP3": ["Ohio", "OhioNew"],
    'FWP4': ["WestVirginiaAEP"],

    "NCA1": ["NorthCalifornia", "NorthCalifornia2016"],
    "NCA2": ["NorthCaliforniaATT", "NorthCaliforniaATT2016"],
    "NCA3": ["NorthCalifornia3", "NorthCaliforniaATT2016_3"],
    "NCA4": ["NorthCalifornia4", "NorthCalifornia2016_4"],
    "NCA5": ["NorthCalifornia5", "NorthCalifornia2016_5"],

    "NewJersey":  ["NewJersey", "NewJersey2010"],
    "NewJersey2": ["NewJersey2", "NewJersey2a", "NewJersey2010B"],
    "NewJersey3": ["NewJersey3"],
    "NewJersey4": ["NewJersey4"],
    "NewJersey5": ["NewJersey5"],
    "NewJersey6": ["NewJersey6"],
    "NewJersey7": ["NewJersey7"],
    "NewJersey8": ["NewJersey8"],
    "NewJersey9": ["NewJersey9"],

    "OCC1": ["Fairfax"],
    "OCC2": ["Fairfax2", "VUPSNewOCC2", "FairfaxXML"],
    "OCC3": ["VUPSNewOCC3", "VUPSNewOCC3_2"],
    "OCC4": ["VUPSNewOCC4", "OCC4XML"],
    # todo(dan) Give OCC5 its own formats like OCC6, but may not need FairFax5
    "OCC5": ["Fairfax2", "VUPSNewOCC2", "FairfaxXML"],
    "OCC6": ["Fairfax6", "VUPSNewOCC6", "OCC6XML"],

    "OUPS1": ["OUPS1"],
    "OUPS2": ["OUPS2"],

    "SCA1": ["SouthCalifornia"],
    "SCA2": ["SanDiego", "SouthCaliforniaSCA2Korterra"],
    "SCA3": ["Arizona"],
    "SCA5": ["SouthCaliforniaCOX"],
    "SCA6": ["SouthCaliforniaATT"],
    "SCA7": ["SouthCaliforniaSDG", "SouthCaliforniaSDGKorterra"],
    "SCA8": ["SouthCaliforniaNV"],
    "SCA9": ["SouthCalifornia9"],
    "SCA10": ["SouthCalifornia10"],
    "SCA11": ["SouthCalifornia11"],

    #
    # LocInc

    "LAK1": ["Alaska", "AlaskaNew"],
    "LCA1": ["NorthCalifornia2", "NorthCalifornia2016_2"],
    "LEM1": ["OregonEM"],
    "LID1": ["IdahoNew"], # was: Idaho
    "LNG1": ["Northwest", "OregonXML"],
    "LNV1": ["Nevada"],
    "LOR1": ["Oregon2"],
    "LOR2": ["OregonFalcon"],
    "LOR3": ["Oregon4"],
    "LOR4": ["Oregon5"],
    "LOR5": ["Oregon6"],
    "LOR6": ["OregonPortland"],
    "LQW1": ["QWEST_IDL", "QWEST_IEUCC", "QWEST_OOC", "QWEST_UULC",
             "QWEST_WOC"], # , "QWEST_IRTH"],
    "LUT1": ["Utah"],
    "LWA1": ["WashingtonState"],
    "LWA2": ["WashingtonState2"],
    "LWA3": ["WashingtonState3"],
    "LWA4": ["WashingtonState4"],
    "LWA5": ["WashingtonState5"],
    "LCC1": ["Oregon3"],

    # NOTE: work order call centers have their own dictionary (see below).
}

#
# "call centers" (from the system's POV) that receive work orders are listed
# here.

# XXX replace with a list of names
work_order_centers = {
    'LAM01': ['Lambert'],
}

# if a call center has work orders, they must be listed here. multiple call
# centers can share the same WO call center.
work_orders = {
    'FMW1': ['LAM01'],
    'FMW4': ['LAM01'],
    'FMB1': ['LAM01'],
    'OCC2': ['LAM01'],
}

# format -> call center: A format can have one and only one call center
# associated with it. This is used when we need/try to determine the call
# center based on a ticket's format. In cases where this is not appropriate
# (e.g. FCL2), the entry in this dict is only a guess; use a fixed call center
# in the configuration instead.

format2cc = {}
for call_center, formats in sorted(cc2format.items()):
    for format in formats:
        if not format2cc.has_key(format):
            format2cc[format] = call_center


class CallCenters:
    """ A collection of call centers. """

    def __init__(self, tdb):
        self.tdb = tdb
        self.reload()

    def reload(self):
        # note: useful for testing
        self.cc = self.load()
        self.ucc = self.load_ucc()

    def load(self):
        cc = {}

        try:
            sql = """
              select c.*, r.code as timezone_code from call_center c
              left join reference r on c.time_zone = r.ref_id
              where c.active = 1;
            """
            rows = self.tdb.runsql_result(sql)
        except:
            # if call_center.time_zone does not exist, fall back to old way
            sql = "select * from call_center where active = 1"
            rows = self.tdb.runsql_result(sql)

        for row in rows:
            call_center = row['cc_code']
            cc[call_center] = row
        return cc

    def get_formats(self, call_center):
        return cc2format[call_center]

    def get_all_call_centers(self):
        return sorted(self.cc.keys())

    def __getitem__(self, call_center):
        return self.cc[call_center] # may raise a KeyError

    def get(self, call_center, default=None):
        try:
            return self.cc[call_center]
        except KeyError:
            return default

    def load_ucc(self):
        """ Fetch the update call centers from the database. """
        sql = """
         select distinct update_call_center, call_center
         from client
         where call_center is not null and
         update_call_center is not null and update_call_center != ''
         and active = 1
        """
        results = self.tdb.runsql_result(sql)
        d = {}
        for row in results:
            ucc = row["update_call_center"]
            cc = row["call_center"]
            # If ucc is the same as cc, that is an error, log and prevent
            if ucc.strip() != cc.strip():
                d[ucc] = cc
        return d

    # todo(dan) Rename this? It's confusing.
    def update_call_center(self, name):
        """ If <name> is an update call center, return the call center that
            it updates. Otherwise, return None. """
        try:
            return self.ucc[name]
        except KeyError:
            return None

    def work_order_centers(self, call_center):
        """ Return the WO centers, if any, associated with the given call
            center. """
        return work_orders.get(call_center, [])

    def is_work_order_center(self, name):
        return name in work_order_centers

_cc = None
def get_call_centers(tdb):
    global _cc
    if _cc is None:
        _cc = CallCenters(tdb)
    return _cc

if __name__ == "__main__":

    # simple display for testing purposes.

    import pprint
    import ticket_db
    cc = CallCenters(ticket_db.TicketDB())
    print "Call centers:"
    pprint.pprint(cc.get_all_call_centers())
    print
    print "Update call centers:"
    pprint.pprint(cc.ucc)

