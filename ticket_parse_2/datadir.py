# datadir.py
# One directory to bind them all.

r"""
A "data dir" is a directory containing all the configuration files
necessary for various parts of the system.  Files may be created by
the system here as well.

(The purpose is to allow several non-conflicting sets of configuration data
on the same system, e.g. one for testing, one for production, etc.)

A data dir can be specified in the following ways:

1. By using the --data command line parameter:

    <some program> --data <datadir>

    e.g.

    main.py --data c:\foo\mydatadir

(Note: Do not use: --data=datadir!)

2. By setting the QMDATA environment variable.

To have a program look for and use a data dir, importing this module suffices.

If a data dir is required but not found, then this is considered a fatal
error, and the program will halt.

Tests and demonstration programs can set the data dir "manually".
"""

import os
import sys
#
import filetools
import tools

datadir = None

DATADIR_ENVIRONMENT_VAR = "QMDATA"

strict = False
# if True, this will cause an error if no data dir is found;
# if False, "" is assumed as the data dir

class DataDirError(Exception):
    pass

def get_datadir():
    """ Attempt to determine the data dir from command line arguments and
        environment variable. """

    # if we already have a data dir, then return it; don't look for it again
    if datadir is not None:
        return datadir

    # try command line arguments
    lower = [s.lower() for s in sys.argv]
    idx = -1
    if "-data" in lower:
        idx = lower.index("-data")
    if idx == -1:
        if "--data" in lower:
            idx = lower.index("--data")
    if idx > -1 and idx < len(sys.argv)-1:
        dir = sys.argv[idx+1]
        # delete these arguments so getopt & co won't be confused later
        del sys.argv[idx:idx+2]
        return DataDir(dir)

    # nothing found in command line arguments; try environments variables
    dir = os.getenv(DATADIR_ENVIRONMENT_VAR)
    if dir:
        return DataDir(dir)

    # nothing found at all
    if strict:
        raise DataDirError, "Data dir not found in command line args or environment"
    else:
        return DataDir(".") # use current directory

class DataDir:
    """ Data dir representation.  Provides custom methods to open files in the
        data dir, check for their existence, create paths, etc. """

    def __init__(self, path):
        self.path = path

    def __repr__(self):
        return "DataDir(%r)" % self.path

    def open(self, filename, mode='r'):
        fullpath = self.get_filename(filename)
        path, filename = os.path.split(fullpath)
        if path and not os.path.exists(path):
            os.makedirs(path)
        f = open(fullpath, mode)
        # will raise an appropriate error if it still cannot be opened/created
        return f

    def exists(self, filename):
        fullpath = self.get_filename(filename)
        return os.path.exists(fullpath)

    def get_filename(self, filename):
        fullpath = os.path.normpath(os.path.join(self.path, filename))
        # XXX absolute or relative path should make a difference
        return fullpath

    def remove(self, filename, silent=0):
        fullpath = self.get_filename(filename)
        if silent:
            filetools.remove_file(fullpath)
        else:
            os.remove(fullpath)

    def get_id(self):
        try:
            f = self.open("id.txt", "r")
            # notice: self.open rather than open... looks in data dir!
            id = f.read().strip()
            f.close()
        except IOError:
            return ""
        else:
            return id

#
# determine the data dir when this module is imported

datadir = get_datadir()
print "Data dir:", `datadir`

if __name__ == "__main__":

    print __doc__

