# timezone.py

import sys
PYTZ_EGG = "pytz-2011n-py%d.%d.egg" % sys.version_info[:2]
# XXX might be different for IronPython

# some path trickery is necessary, apparently
import os
whereami = os.path.dirname(os.path.abspath(__file__))
lib_path = os.path.join(whereami, PYTZ_EGG)
sys.path.append(lib_path)

import date
import datetime
import pytz

# key: timezones as entered in the database (reference table);
# value: timezones as known by pytz
pytz_timezones = {
    "AKST/AKDT": "US/Alaska",
    "CST/CDT":   "CST6CDT",
    "EST/EDT":   "EST5EDT",
    "HST":       "HST",
    "MST":       "MST",
    "MST/MDT":   "MST7MDT",
    "PST/PDT":   "PST8PDT",
}
# others are possible as well; see pytz/zoneinfo/US

def convert(datestr, tz, target_tz):
    """ Convert a date/time, given as an ISO string (as used by date.py),
        from timezone <tz> to the new timezone <target_tz>. Both timezones are
        strings, either in the format used in the db, or the format used by
        pytz.

        convert("2011-11-19 20:22:00", "EST/EDT", "PST/PDT")
    """
    # create timezone objects for 'origin' and 'target' timezones
    tz1 = pytz.timezone(pytz_timezones.get(tz, tz))
    tz2 = pytz.timezone(pytz_timezones.get(target_tz, target_tz))

    dt = date.Date(datestr).as_tuple()[:6]  # timezone-agnostic date, as 6-tuple
    d1 = datetime.datetime(*dt)             # create a datetime from it
    d2 = tz1.localize(d1)                   # associate with origin timezone
    d3 = d2.astimezone(tz2)                 # convert to target timezone
    return d3.strftime("%Y-%m-%d %H:%M:%S") # return as ISO string

