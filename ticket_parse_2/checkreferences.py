# fixreferences.py
# Note: Uses config.xml for SMTP host data

import config
import datadir
import dbinterface_ado
import getopt
import mail2
import sys

#
# configuration data (placed here for now, can be moved to config file
# later if necessary)

uq_db = {
    'host': '10.1.1.183',
    'login': 'sa',
    'password': 'doggy183',
    'database': 'QM',
}

locinc_db = {
    'host': '10.1.1.24',
    'login': 'sa',
    'password': 'br38we',
    'database': 'LocQM',
}

test_db = {
    'host': '10.1.1.175',
    'login': 'sa',
    'password': 'doggy183',
    'database': 'QMTesting',
}

notification_emails = [
    "hans.nowak@oasisdigital.com",
    "erik.berry@oasisdigital.com",
]

cfg = config.getConfiguration()
smtpinfo = cfg.smtp_accs[0]

#
# options

__usage__ = """\
checkreferences.py [options]

Options:
    -q       quiet: Don't send notification emails.
    -t       test: Write to test db rather than to production one.
    -a type  add: Copy records of given type from UQ to LocInc.  Existing
             records (of the same type/code) will be skipped.
"""

quiet = test = 0
add_type = None
opts, args = getopt.getopt(sys.argv[1:], "a:qt?")
for o, a in opts:
    if o == "-q":
        quiet = 1
    elif o == "-t":
        test = 1
    elif o == "-a":
        add_type = a
    elif o == "-?":
        print >> sys.stderr, __usage__
        raise SystemExit

dbuq = dbinterface_ado.DBInterfaceADO(uq_db)
dblocinc = dbinterface_ado.DBInterfaceADO(locinc_db)
dbtest = dbinterface_ado.DBInterfaceADO(test_db)

def get_types(db):
    sql = "select distinct type from reference"
    rows = db.runsql_result(sql)
    types = [row['type'] for row in rows]
    types.sort()
    return types

sql = "select distinct type from reference"
uqtypes = get_types(dbuq)
locinctypes = get_types(dblocinc)

def differences(list1, list2):
    list1_only = []
    list2_only = []
    for item in list1:
        if item not in list2:
            list1_only.append(item)
    for item in list2:
        if item not in list1:
            list2_only.append(item)
    return list1_only, list2_only

uqonly, locinconly = differences(uqtypes, locinctypes)

print "Types in UQ but not LocInc:", uqonly
print "Types in LocInc but not in UQ:", locinconly

body = """
Comparing types in references table:

Types in UQ but not in LocInc: %s
Types in LocInc but not in UQ: %s
"""

if uqonly or locinconly:
    print "Sending notification email(s)...",
    subject = "References Notification Email"
    body = body % (uqonly, locinconly)
    if not quiet:
        mail2.sendmail(smtpinfo, notification_emails, subject, body)
        print "OK"
    else:
        print "nothing sent (quiet mode)"
else:
    print "No differences found; no notification email necessary"

#
# store differences if required

def remove_existing_rows(rows, existing):
    if not existing:
        return rows
    ok = []
    for row in rows:
        exists = 0
        for extrow in existing:
            # if type and code are the same, this is considered to be the
            # same record, even if description, sort order, etc, are different
            if row['type'] == extrow['type'] \
            and row['code'] == extrow['code']:
                print "Row", (row['type'], row['code']), "already exists, will be skipped"
                exists = 1
                break
        if not exists:
            ok.append(row)
    return ok

def store_differences(fromdb, todb, typename):
    existing = todb.getrecords("reference", type=typename)
    rows = fromdb.getrecords("reference", type=typename)
    sql = """
     insert reference (type, code, description, sortby, active_ind, modifier)
     values (%s, %s, %s, %s, %s, %s)
    """

    def p(fieldname):
        value = row[fieldname]
        if value is None:
            return 'null'
        else:
            return "'%s'" % (value,)

    def q(fieldname):
        value = row[fieldname]
        if value is None:
            return 'null'
        else:
            return "%s" % (value,)

    rows = remove_existing_rows(rows, existing)
    for row in rows:
        isql = sql % (p('type'), p('code'), p('description'),
                      q('sortby'), q('active_ind'), p('modifier'))
        print "Storing:", (row['type'], row['code'], row['description'])
        todb.runsql(isql)
    print "Records for type", typename, "stored (%d rows)" % (len(rows,))

if add_type:
    if test:
        todb = dbtest
    else:
        todb = dblocinc
    print "Storing types in",
    print (todb is dbtest) and "test database" or "LocInc"
    store_differences(dbuq, todb, add_type)
