# !makeranges.py

import tools

def inc_grid((number1, number2, letter)):
    if letter < 'D':
        letter = chr(ord(letter) + 1)
        return (number1, number2, letter)
    else:
        letter = 'A'
        number2 = number2 + 1
        if number2 >= 60:
            number2 = 0
            number1 = number1 + 1
        return (number1, number2, letter)

def split_grid(grid):
    return int(grid[:2]), int(grid[2:4]), grid[4:]

def join_grid((number1, number2, letter)):
    return "%02d%02d%s" % (number1, number2, letter)

def all_between(grid1, grid2):
    a = split_grid(grid1)
    b = split_grid(grid2)
    if a > b:
        a, b = b, a
    stuff = []
    c = a
    while c <= b:
        stuff.append(c)
        c = inc_grid(c)
    return [join_grid(x) for x in stuff]

def all_in_square(lat1, long1, lat2, long2):
    lats = all_between(lat1, lat2)
    longs = all_between(long1, long2)
    return [z for z in tools.cartesian(lats, longs)]


if __name__ == "__main__":

    def test1():
        print split_grid('3337A')
        a = split_grid('3359A')
        for i in range(10):
            print a
            a = inc_grid(a)
        print join_grid(a)

    def test2():
        a = all_between('3337A', '3341A')
        print a, len(a)
        a = all_between('8403A', '8359A')
        print a, len(a)

    def test3():
        all = all_in_square('3337A', '8403A', '3341A', '8359A')
        print all
        print len(all)

    test3()

