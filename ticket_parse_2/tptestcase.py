# tptestcase.py
# An extension of unittest.TestCase.
# The 'tp' in 'tptestcase' comes from 'ticket parser', because that's the
# project it's used for.

import unittest

class TPTestCase(unittest.TestCase):

    def assert_close_enough(self, value1, value2, eps=0.0001):
        """ Assert that the difference between value1 and value2 is less
            than 'eps'. In other words, this works like value1 == value2 for
            floating points, but allows for a certain error margin caused by
            rounding.
            Note: Should be called with floating point values. Other types
            may work but are not guaranteed.
        """
        diff = abs(value1 - value2)
        self.assert_(diff < eps, "Difference between %s and %s too high: %s" % (
         value1, value2, diff))

