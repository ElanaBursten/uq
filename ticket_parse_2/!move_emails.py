# !move_emails.py
# Move email addresses from call_center table to notification_email table.

import dbinterface_ado
import sys

FIELDNAMES = ["admin", "emergency", "summary", "message", "warning"]

dbado = dbinterface_ado.DBInterfaceADO()

if "-d" in sys.argv[1:]:
    dbado.delete("notification_email")

rows = dbado.getrecords("call_center")

for row in rows:
    call_center = row["cc_code"]
    for fieldname in FIELDNAMES:
        emailstring = row[fieldname + "_email"] or ""
        emailstring = emailstring.strip()
        if emailstring:
            emails = emailstring.split(";")
            for email in emails:
                print (call_center, fieldname, email)
                dbado.insert("notification_email", call_center=call_center,
                 notification_type=fieldname, email_address=email.strip())

