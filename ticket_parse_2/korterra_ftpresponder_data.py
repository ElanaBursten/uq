# korterra_ftpresponder_data.py

import time
import xml.etree.ElementTree as ET
#
import date
import errorhandling2
import et_tools
import ftpresponder_data

class BaseKorTerraFTPResponderData(ftpresponder_data.FTPResponderData):
    TEMPLATE1 = """\
<?xml version="1.0" encoding="UTF-8"?>
<LocateCollection>
###BODY###</LocateCollection>"""

    TEMPLATE2 = ''

    MARK_SETTINGS = {'CLEAR': (0, 1, ''), 'MARKED': (1, 0, '')}

    store_resp_context = False
    delete_ack = True
    is_xml = True

    def adapt_ticket_number(self, ticket_number):
        return ticket_number

    # todo(dan) refactor: move extra_names to here from subclasses
    def extra_names(self, namespace):
        # Use the closed_date, if available (should be)
        self.row = self.tdb.getrecords(
          "locate", locate_id=namespace['locate_id'])[0]
        now = self.row['closed_date']
        if now is None:
            # No closed date, set to now
            now = date.Date().isodate()

        extra = {}
        extra['remarks'] = 'Response by Utiliquest'
        extra['completion_date'] = now[:10]
        extra['completion_time'] = now[11:] + ".000"

        ms = self.MARK_SETTINGS.get(namespace['status'], (0, 0, ''))
        extra['ismarked'] = ms[0]
        extra['isclearexcavate'] = ms[1]
        extra['actcode'] = ms[2]

        return extra

    def gen_filename(self):
        """ Generate a unique filename to be posted on the server. """
        t = time.time()
        t9 = time.localtime(t)
        year, month, day, hours, minutes, seconds = t9[:6]
        milliseconds = int((t - int(t)) * 1000)
        name = "%04d%02d%02d%02d%02d%02d%03d.xml" % (year, month, day, hours,
               minutes, seconds, milliseconds)
        return name

    def is_response_file(self, filename):
        return filename.endswith("_ack.xml")

    def get_acknowledgements(self, data, locates):
        dom = ET.fromstring(data)
        ok, errors = [], []
        for ackcoll in et_tools.findall(dom, 'ackcollection'):
            for ack in et_tools.findall(ackcoll, 'ack'):
                a = ftpresponder_data.FTPResponderAck()
                try:
                    for ackcode in et_tools.findall(ack, 'ackCode'):
                        a.result = ackcode.text.strip()
                        if str(a.result) == '19' or str(a.result) == '14':
                            raise ValueError('Result indicates a retry is needed')
                        break
                    for acktext in et_tools.findall(ack, 'ackText'):
                        a.raw = acktext.text.strip()
                        if str(a.raw).find('Operation Timed Out') != -1:
                            raise ValueError('Result indicates a retry is needed')
                        break
                    completion_id = ack.get('completionid')
                    a.locate_id, a.resp_id = completion_id.split(';')[:2]
                except:
                    ep = errorhandling2.errorpacket()
                    errors.append(ep)
                    continue
                else:
                    ok.append(a)
        return ok, errors

    def is_success(self, result_code):
        return result_code == '0'

# todo(dan) deprecated, use CrownCastleKorTerraFTPResponderData2 instead
class CrownCastleKorTerraFTPResponderData(BaseKorTerraFTPResponderData):
    TEMPLATE2 = """\
    <Locate type="CUSTOM2" membercode="%(company)s" completionid="%(locate_id)s;%(resp_id)s">
        <customerid>CROWNCASTLE</customerid>
        <occid>CNOC</occid>
        <jobid>%(ticket_number)s</jobid>
        <membercode>%(company)s</membercode>
        <completiondtdate>%(completion_date)s</completiondtdate>
        <completiondttime>%(completion_time)s</completiondttime>
        <operatorid>UTILIQUEST</operatorid>
        <reasonid></reasonid>
        <faccode1>FIBER</faccode1>
        <ismarked1>%(ismarked)s</ismarked1>
        <isclearexcavate1>%(isclearexcavate)s</isclearexcavate1>
        <actcode1>%(actcode)s</actcode1>
        <isnotsendprs>0</isnotsendprs>
        <remarks>%(remarks)s</remarks>
    </Locate>\n"""

# todo(dan) replaces CrownCastleKorTerraFTPResponderData
class CrownCastleKorTerraFTPResponderData2(BaseKorTerraFTPResponderData):
    OCCID = ''

    TEMPLATE2 = """\
    <Locate type="CUSTOM2" membercode="%(company)s" completionid="%(locate_id)s;%(resp_id)s">
        <customerid>CROWNCASTLE</customerid>
        <occid>%(occid)s</occid>
        <jobid>%(ticket_number)s</jobid>
        <membercode>%(company)s</membercode>
        <completiondtdate>%(completion_date)s</completiondtdate>
        <completiondttime>%(completion_time)s</completiondttime>
        <operatorid>UTILIQUEST</operatorid>
        <reasonid></reasonid>
        <faccode1>FIBER</faccode1>
        <ismarked1>%(ismarked)s</ismarked1>
        <isclearexcavate1>%(isclearexcavate)s</isclearexcavate1>
        <actcode1>%(actcode)s</actcode1>
        <isnotsendprs>0</isnotsendprs>
        <remarks>%(remarks)s</remarks>
    </Locate>\n"""

    def extra_names(self, namespace):
        extra = BaseKorTerraFTPResponderData.extra_names(self, namespace)
        extra['occid'] = self.OCCID
        return extra

class ExelonKorTerraFTPResponderData(BaseKorTerraFTPResponderData):
    COMPLETION_SCREEN = 'CUSTOM1'
    CUSTOMER_ID = 'SYSTEM'
    ACT_CODE = 'UTL'

    OCC_ID = ''
    JOB_ID_PREFIX = ''

    MEMBER_CODES = {
      'DPCV28': ('CVMD', 'CV'),
      'DPCV29': ('CVMD', 'CV'),
      'DPCV30': ('CVMD', 'CV'),
      'DPML11': ('MLDE', 'ML'),
      'DPML12': ('MLDE', 'ML'),
      'DPSA15': ('SAMD', 'SA'),
      'DPSA16': ('SAMD', 'SA'),
      'DPU104': ('U1DE', 'U1'),
      'DPUG07': ('UGDE', 'UG'),
      'DPUG09': ('GTDE', 'GT')}

    TEMPLATE2 = """\
    <Locate type="%(completion_screen)s" membercode="%(company)s" completionid="%(locate_id)s;%(resp_id)s">
        <customerid>%(customer_id)s</customerid>
        <occid>%(occ_id)s</occid>
        <jobid>%(job_id)s</jobid>
        <membercode>%(company)s</membercode>
        <completiondtdate>%(completion_date)s</completiondtdate>
        <completiondttime>%(completion_time)s</completiondttime>
        <actcode1>%(act_code)s</actcode1>
        <notcode>%(status)s</notcode>
    </Locate>\n"""

    def extra_names(self, namespace):
        extra = BaseKorTerraFTPResponderData.extra_names(self, namespace)
        extra['completion_screen'] = self.COMPLETION_SCREEN
        extra['customer_id'] = self.CUSTOMER_ID
        extra['act_code'] = self.ACT_CODE

        member_code = namespace['company'].upper()
        if member_code in self.MEMBER_CODES:
            occ_id = self.MEMBER_CODES[member_code][0]
            job_id_prefix = self.MEMBER_CODES[member_code][1]
        else:
            occ_id = self.OCC_ID
            job_id_prefix = self.JOB_ID_PREFIX
        extra['occ_id'] = occ_id
        extra['job_id'] = job_id_prefix + namespace['ticket_number']

        return extra
