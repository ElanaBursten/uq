# trymanytimes.py

import dbinterface_old as dbinterface # for DBException, TimeoutError

class TryManyTimes:

    def __init__(self, times, log=None):
        self.times = times
        self.log = log

    def __call__(self, f, *args, **kwargs):
        for i in range(self.times):
            # if first attempt is OK, be silent; only start logging if the
            # first attempt fails
            if i > 0 and self.log:
                self.log.log_event("TryManyTimes %d/%d: %r args=%r kwargs=%r" % (
                 i+1, self.times, f, args, kwargs))
            try:
                result = f(*args, **kwargs)
            except dbinterface.TimeoutError:
                if self.log:
                    self.log.log_event("[TryManyTimes] Attempt %d/%d failed due to timeout." %
                     (i+1, self.times))
                pass # continue and possibly try again
            else:
                return result

        # if we're here, none of the attempts worked
        raise

