# ticketversion.py
# Object-relational mapping for the ticket_version table.  Basically
# write-only, since we don't load ticket versions in the Python code
# (except for testing).

import sqlmap
import sqlmaptypes as smt
import static_tables

class TicketVersion(sqlmap.SQLMap):

    __table__ = "ticket_version"
    __key__ = "ticket_version_id"
    __fields__ = static_tables.ticket_version

