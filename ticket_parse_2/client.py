# client.py

import sqlmap
import static_tables

class Client(sqlmap.SQLMap):
    __table__ = "client"
    __key__ = "client_id"
    __fields__ = static_tables.client

    def __init__(self, **kwargs):
        sqlmap.SQLMap.__init__(self)
        fieldnames = zip(*self.__fields__)[0]
        for key, value in kwargs.items():
            if key == 'client_code':
                key = 'oc_code'
            assert key in fieldnames, "Invalid field: %s" % (key,)
            setattr(self, key, value)

    def __getattr__(self, name):
        if name == 'client_code':
            return self.oc_code
        return sqlmap.SQLMap.__getattr__(self, name)

    def __setattr__(self, name, value):
        if name == 'client_code':
            setattr(self, 'oc_code', value)
        sqlmap.SQLMap.__setattr__(self, name, value)

