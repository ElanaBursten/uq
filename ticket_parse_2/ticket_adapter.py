# ticket_adapter.py
# Adapt parsed tickets according to their call center's rules.
# Note: TicketAdapter classes may be found in the 'callcenters' package.  They
# are *optional*; they may not be defined for a given call center, in which
# case no ticket adapting will be done.

# XXX This functionality is much the same as call_center_adapter.py, and may be
# merged with it.  Only difference seems to be the place where the ticket is
# adapted -- by the FeedHandler (TicketAdapter) or by main.py (call center
# adapter).

# XXX The object might benefit from having more information, e.g. a list of
# term ids for the given call center, etc. It should not have access to the
# database.

# API:
# adapt(ticket) -- change a ticket in-place

import callcenters # package
import string

class TicketAdapterError(Exception):
    pass

class TicketAdapter:
    def adapt(self, ticket):
        pass

def get_adapter(call_center):
    name = call_center
    if call_center[0] not in string.letters:
        name = 'c' + call_center

    try:
        return callcenters.get_adapter(name)
    except (ImportError, AttributeError):
        raise TicketAdapterError, "No TicketAdapter class for: " + call_center

