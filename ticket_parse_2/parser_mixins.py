# parser_mixins.py

from re_shortcut import R
import string

class FindSecondTownshipMixin:

    _re_map_page = R("Twp: (.*?) *Rng: (.*?) *Sect-Qtr: (.*?),? *$")
    # note that we can override this in the subclass if it's different for
    # a certain format

    '''
    def _f_map_page(self, match):
        try:
            township, range, section = match.groups()
            section = string.split(section, ",")[0]
            section = string.replace(section, "-", "")
            return "%s%s%s" % (township, range, section)
        except (ValueError, IndexError):
            return string.join(match.groups())
    '''

    def find_map_page(self):
        # get all matches
        g = self._re_map_page.finditer(self.data)
        matches = [m for m in g]

        # take the second one; if not possible, take the first one
        if not matches:
            return ""
        elif len(matches) == 1:
            index = 0
        else:
            index = 1

        match = matches[index]
        try:
            township, range, section = match.groups()
            section = section.replace(" ", "")
            section = string.split(section, ",")[0]
            section = string.replace(section, "-", "")
            return "%s%s%s" % (township, range, section)
        except (ValueError, IndexError):
            return string.join(match.groups())
