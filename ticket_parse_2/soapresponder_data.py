# soapresponder_data.py

import abstractresponderdata
import callcenters
import traceback

class SOAPResponderError(Exception):
    pass

class SOAPResponderData(abstractresponderdata.AbstractResponderData):
    client_based_login = 0

    acceptable_codes = ["invalid id/cdc combination",
                        "invalid serial number",
                        "member not on ticket",
                        "invalid response code",
                        "response past due",
                        "response posted"]

    codes_ok = ["response posted", "response past due"]
    codes_reprocess = []
    codes_error = ["invalid id/cdc combination", "invalid serial number",
                   "member not on ticket", "invalid response code"]

    sender = 'UQ'

    def get_xml(self, names):
        raise NotImplementedError, "Override get_xml method in subclasses"
        #return self.XML_TEMPLATE % locals()

    def process_response(self, reply):
        """ Overload in call center if necessary. """
        raise NotImplementedError

    def response_is_success(self, row, resp):
        raise NotImplementedError

    def filter_versions(self, versions):
        return versions

def getresponderdata(config, call_center, raw_responderdata):
    """ Try to get and instantiate an SOAPResponderData class for the given
        call center.  If not found, raise an SOAPResponderDataError.  Looks
        for the given class in two ways: in the namespace of this module, and
        in the 'callcenters' package.
        NOTE: The first lookup method is obsolete now, but I'll leave it in
        because it's useful for testing.
    """
    prefix = call_center
    if prefix[0] in "01234567890":
        prefix = "c" + prefix
    klassname = prefix + "SOAPResponderData"
    try:
        klass = globals()[klassname]
    except KeyError:
        try:
            klass = callcenters.get_soapresponderdata(prefix)
        except:
            traceback.print_exc()
            raise SOAPResponderError, "Could not find SOAPResponderData class for %s" % (call_center,)

    rd = klass()

    # inject data provided or from configuration
    if not raw_responderdata:
        # only for testing purposes
        try:
            raw_responderdata = config.soap_responders[call_center]
        except KeyError:
            raise KeyError, "No responder data for " + call_center

    rd.response_codes = raw_responderdata["mappings"]
    for attr in ('name', 'url', 'mappings', 'logins',
                 'translations', 'skip', 'all_versions', 'clients',
                 'send_emergencies', 'send_3hour'):
        setattr(rd, attr, raw_responderdata[attr])

    #try:
    #    rd.client_based_login = int(raw_responderdata["clientbased"])
    #    # do client-based SOAP responders even exist?
    #except (ValueError, KeyError):
    #    rd.client_based_login = 0

    # override 'skip' to use values in ResponderConfigData
    rd.skip = config.responderconfigdata.get_skip_data(call_center)
    rd.status_code_skip = config.responderconfigdata.get_status_code_skip_data(call_center)

    return rd
