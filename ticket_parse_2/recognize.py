# recognize.py
# Inspect a raw ticket and determine its type, e.g. Atlanta, Baton Rouge,
# etc.
# Created: 26 Jan 2002, Hans Nowak

import re
import re_tools
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

from re_tools2 import header_value_re as hdr_val
from re_tools2 import ALL_0_N, START

def R(regex, flags=0):
    return re.compile(regex, re.MULTILINE | flags)

class XmlTicketRecognize(object):
    # TODO: we need to be able to look for multiple nodes and their values

    def __init__(self, pattern, values):
        self.pattern = pattern
        self.values = values

    def search(self, raw):
        raw = raw.strip() # we want no whitespace around XML
        cstr_file = StringIO(raw)
        try:
            tree = ET.parse(cstr_file)
            root = tree.getroot()
            pattern_match = root.find(self.pattern)
            for value in self.values:
                if pattern_match.text == value:
                    return True
            return False
        except:
            # blanket exception; sometimes non-XML is fed to the recognizer, it
            # should not choke on that, but just return False
            #import traceback; traceback.print_exc()
            return False

class TicketRecognizeSpecial(object):
    def __init__(self, func):
        self.func = func

    def search(self,raw):
        return self.func(raw)

def recognize_SouthCalifornia(raw):
    regex = R("[A-Z0-9]+\s+\d+[A-Z]+ USAS \d\d")
    m = regex.search(raw)
    if m and 'KORTERRA' not in raw:
        return 'SouthCalifornia'

# Regular expressions that determine the type of ticket. It's very well
# possible that not all tickets can be determined through regular
# expressions.
regexen = {
    "Atlanta": R("^[A-Z0-9]+ +\d+ +GAUPC.*Ticket\s*:", re.DOTALL|re.IGNORECASE),
    "GA3003": R("^[A-Z0-9]+ +\d+ +GAUPC.*Notice\s*:", re.DOTALL|re.IGNORECASE),
    "GA3007": R("KORTERRA JOB GA*", re.DOTALL),
    "NorthCarolina": R("^North Carolina One Call"),
    "NC811": R("^North Carolina 811"),
    "NC2101": R("[A-Z0-9*]+ +\d+ +NCOC[a-z].+^North Carolina 811", re.DOTALL),
    "TennesseeNC": R("^North Carolina (One Call|811)"),

    # NewJersey old and new formats. Notice the difference in time format...
    "NewJersey": R("(^|^ )New Jersey One Call System +SEQUENCE NUMBER.*?At: \d{4}", re.DOTALL|re.MULTILINE),
    "NewJersey2010": R("(^|^ )New Jersey One Call System +SEQUENCE NUMBER.*?At: \d\d:\d\d", re.DOTALL|re.MULTILINE),
    "NewJersey8": R("New Jersey One Call System +SEQUENCE NUMBER.*?At: \d\d:\d\d", re.DOTALL|re.MULTILINE),

    # this "family" of formats all look the same, and must therefore have
    # designed directories with call centers associated with them, and special
    # recognize tests
    "Lanham": R("NOTICE OF INTENT TO EXCAVATE.*State: MD", re.DOTALL),
    "Fairfax": R("NOTICE OF INTENT TO EXCAVATE.*State: (VA|MD)", re.DOTALL),
    "BatonRouge1": R("NOTICE OF INTENT TO EXCAVATE.*State: LA", re.DOTALL),
    "BatonRouge2": R("NOTICE OF INTENT TO EXCAVATE.*State: LA", re.DOTALL),
    "Richmond1": R("NOTICE OF INTENT TO EXCAVATE.*State: (VA|MD)", re.DOTALL),
    "Baltimore": R("NOTICE OF INTENT TO EXCAVATE.*State      : (MD|DE)", re.DOTALL),
    "Baltimore4": R("NOTICE OF INTENT TO EXCAVATE"),
    "Washington": R("NOTICE OF INTENT TO EXCAVATE.*State      : (MD|DC|VA|DE)",
                  re.DOTALL),
    "Houston1": R("NOTICE OF INTENT TO EXCAVATE.*State: TX", re.DOTALL),
    "Houston2016": R('LONE ?STAR 811' + ALL_0_N + 'NOTICE OF INTENT TO EXCAVATE' +
                     ALL_0_N + hdr_val('State', 'TX')),
    "Delaware1": R("NOTICE OF INTENT TO EXCAVATE.*State      : (DE|MD|VA)",
                 re.DOTALL),
    "Richmond2": R("UTIL11 +\d+ +ONISI"),
    "Richmond3": R("[0-9A-Z]+ \d+ VUPS[a-z] \d\d/.*?Ticket :", re.DOTALL),
    "WestVirginia": R("Miss Utility of West Virginia Notice"),
    "Pennsylvania": R("PENNSYLVANIA ONE CALL SYSTEM - ACT"),
    "PA7501": R("PENNSYLVANIA (UNDERGROUND UTILITY LINE PROTECTION|ONE CALL LOCATE) REQUEST.*Serial Number", re.DOTALL),
    "PA7501Meet": R("PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION.*Meeting Request Number", re.DOTALL),
    "Ohio": R("^ ticket +[0-9-]+ +xref-\d+"),
    "SouthCarolina": R("SOUTH CAROLINA PUPS NOTICE"),
    "Orlando": R(" +\d+ CALL SUNSHINE \d"),
    "Jacksonville": R(" +\d+ CALL SUNSHINE \d"),
    "Pensacola": R(" +\d+ CALL SUNSHINE \d"),
    "Tennessee": R("TNOCS\sLOCATE\sREQUEST.*Ticket Number--\[", re.DOTALL|re.I),
    "Illinois": R("[A-Z0-9]+ +\d+ JULIE \d\d"), # is it always 'JULIE'?
    "Colorado": R("[A-Z0-9]+ +\d+ UNCC[a-z]? \d\d"),   # UNCC, always
    "SouthCalifornia": TicketRecognizeSpecial(recognize_SouthCalifornia),
    "SouthCalifornia11": R("[A-Z0-9]+\s+\d+[A-Z]+ USAS \d\d"),
    "SouthCaliforniaCOX": R("COX\S+\s+\d+[A-Z]+\s+USAS \d\d"),
    "NorthCalifornia": R("[A-Z0-9]+\s+\d+\s+USAN \d\d"),
    #"NorthCalifornia_A": R("[A-Z0-9]+\s+\d+ +USAN \d\d"),
    "NorthCaliforniaATT": R("[A-Z0-9]+ \d+ +USAN \d\d"), # FIXME
    "NorthCalifornia2016": re_tools.usan_transmission_line_re(),
    "NorthCalifornia2016_4": R(r'Message Number:.*?Rev:.*?Received by USAN at'),
    "Alabama": R("[A-Z0-9]+ +\d+ +AOC +\d\d"),
    "Kentucky": R("KENTUCKY URG PRO INC - KY.*COUNTY\.+", re.DOTALL),
    "Dallas1": R("Texas Excavation Safety System"),
    "DIGGTESS": R("DIG-TESS Locate Request For"),
    "Arkansas": R("// +AOC TICKET +//"),
    "Tulsa": R("// +OOCSI LOCATE REQUEST"),
    "SanAntonio": R("Texas One Call System"),
    "Harlingen": R(
     "(Texas One Call System|Texas Excavation Safety System|LONE STAR|"\
     "Locate Request No)"),
    "AlabamaNew": R("// +ALOCS? LOCATE REQUEST"),
    "Mississippi": R("// +MS?OCS LOCATE REQUEST"),
    "Albuquerque": R("New Mexico One Call\s*$"),
    "Oregon": R("Ticket No:.*State: OR.*Twp:", re.DOTALL),
    "WashingtonState": R("Ticket No:.*State: (WA|MT).*Twp:", re.DOTALL),
    "Greenville": R("SOUTH CAROLINA PUPS NOTICE"),
    "VUPSNew": R("\d+\s+VUPS[a-z]\s+\d+.*?Ticket No:", re.DOTALL),
    "Arizona": R("Arizona Blue Stake.*?Ticket No:", re.DOTALL),
    "Wisconsin1": R("Diggers Hotline, Inc\. \(Wisconsin\)"),
    "Wisconsin2": R("Ticket no\.:.*Field Rep\.:.*Work for:.*Place:", re.DOTALL),
    "GreenvilleNew": R("\w+ \d+ PUPS\s+(.*?)\s+\d+/\d+/\d+"),
    "AlbuquerqueTX": R("Texas Excavation Safety System|Texas One Call System"),
    "QWEST_IDL": R("Idaho Dig Line"),
    "QWEST_IEUCC": R("IEUCC\d+.*Twp:", re.DOTALL),
    "QWEST_OOC": R("OOC\d+.*OUNC", re.DOTALL),
    "QWEST_UULC": R("UULC\d+.*Twp:", re.DOTALL),
    "QWEST_WOC": R("WOC\d+.*NUNC", re.DOTALL),
    "QWEST_IRTH": R("IRTH\d+"),
    "ColoradoNE": R("DHON\d+\s+\d+\s+20\d\d/.*Ticket No:", re.DOTALL),
    "OhioNew": R("Dig Request from OUPS for"),
    "Alaska": R("ALASKA DIGLINE.*NOTICE OF INTENT TO EXCAVATE\s+Header Code",
     re.DOTALL),
    "AlaskaNew": R("ALASKA DIGLINE.*NOTICE OF INTENT TO EXCAVATE\s+Ticket No",
     re.DOTALL),
    "Utah": R("BAJA +? \d+ UTAH[a-z] \d"),
    "Nevada": R("[A-Z0-9]+ \d+ +USA[NS] \d\d"),
    "Idaho": R("NOTICE OF INTENT TO EXCAVATE"),
    "KentuckyNew": R("[A-Z0-9]+ +\d+ +KUPI[a-zA-Z] +\d\d\/"),
    "Northwest": R("^Ticket No:.*Notification Type:", re.DOTALL),
    "HoustonKorterra": R("KORTERRA JOB.*Texas Excavation Safety", re.DOTALL),
    "MississippiKorterra": R("MOCS LOCATE REQUEST", re.DOTALL),
    "OregonEM": R("Ticket No:.*State: (OR|WA).*Twp:", re.DOTALL),
    "SC1422": R("From: IRTHNet  At: .* Seq No: \d+.*(?:Facility:)?.*Ticket Number:", re.DOTALL),
    "SC1422A": R("From: IRTHNet  At: .* Seq No: \d+.*(?:Facility:)?.*Notice Number:", re.DOTALL),
    "SC1421A": R("(Ticket|Notice) Number:.*?Caller Information.*?Drilling/Boring", re.DOTALL),
    "SC1444": R("Ticket Number.*ANSCO", re.DOTALL),
    "SouthCaliforniaSDGKorterra": R("KORTERRA JOB.*USAS", re.DOTALL),
    "NashvilleKorterra": R("KORTERRA JOB.*TNOCS", re.DOTALL),
    "NewJersey4": R("DIG REQUEST from"),
    "WV1181": R("Miss Utility of West Virginia Locate Request"),
    "WV1181Korterra": R("KORTERRA JOB UTILIQUEST"),
    "WV1181Alt": R("Locate Request For \S+"),
    "Nashville2013": R("TNOCS LOCATE REQUEST.*Ticket Number:", re.DOTALL|re.I),
    "NewJersey6": R("Assigned to ORUUNJ"),
    "NewJersey7": R("Assigned to ORUUNY"),

    # todo(dan) review these
    "OUPS1": R('[ \t]+OUPS[a-z]?[ \t]+[\d/]+ +[\d:]+'),
    "OUPS2": R('[ \t]+OUPS[a-z]?[ \t]+[\d/]+ +[\d:]+'),

    # XML tickets
    "OregonXML": XmlTicketRecognize("./Ticket/CallCenter",
                 ["Oregon One Call", "Washington One Call"]),
    "FairfaxXML": R("<center>VUPS[abc]</center>.*?"\
                  + "<transmission_type>TKT</transmission_type>", re.S|re.M),
    "GeorgiaXML": R("<TICKET>.*?<One_Call_Center>", re.S|re.M),
}

regexen["TennesseeGA"] = regexen["Atlanta"]
regexen["Nashville"] = regexen["Tennessee"]
regexen["NashvilleKorterra2013"] = regexen["Nashville2013"]
regexen["Dallas2"] = regexen["Dallas1"]
regexen["Houston2"] = regexen["Houston1"]
regexen["AlabamaMobile"] = regexen["Alabama"]
regexen["Mississippi2"] = regexen['Mississippi']
regexen["MississippiLA"] = regexen['BatonRouge1']
regexen["Chesapeake1"] = regexen["Richmond3"]
regexen["Chesapeake2"] = regexen["NorthCarolina"]
regexen["Washington2"] = regexen["Washington"]
regexen["AlabamaMobileNew"] = regexen["AlabamaNew"]
regexen["RichmondNC"] = regexen["Chesapeake2"]
regexen["AlabamaFL"] = regexen["Pensacola"]
regexen["TulsaWellsco"] = regexen["Tulsa"]
regexen["Jackson"] = regexen["Tennessee"]
regexen["Chattanooga"] = regexen["Tennessee"]
regexen["ChattanoogaGA"] = regexen["TennesseeGA"]
regexen["ChattanoogaNC"] = regexen["TennesseeNC"]
regexen["Fairfax2"] = regexen["Richmond3"]
regexen["Fairfax6"] = regexen["Fairfax2"]
regexen["Delaware2"] = regexen["Richmond3"]
regexen['Washington3'] = regexen['Richmond3']
regexen["Albuquerque2"] = regexen['Albuquerque']
regexen['WashingtonVA'] = regexen['Richmond3']
regexen["VUPSNewFCV3"] = regexen["VUPSNew"]
regexen["VUPSNewFPK1"] = regexen["VUPSNew"]
regexen["VUPSNewOCC2"] = regexen["VUPSNew"]
regexen["VUPSNewOCC6"] = regexen["VUPSNewOCC2"]
regexen["VUPSNewFDE2"] = regexen["VUPSNew"]
regexen["VUPSNewFMW3"] = regexen["VUPSNew"]
regexen["VUPSNewFMW2"] = regexen["VUPSNew"]
regexen["VUPSNewOCC3"] = regexen["VUPSNew"]
regexen["VUPSNewOCC3_2"] = regexen["VUPSNew"]
regexen["VUPSNewOCC4"] = regexen["VUPSNew"]
regexen["Colorado2"] = regexen["Colorado"]
regexen["AlbuquerqueAZ"] = regexen["Arizona"]
regexen["Wisconsin2a"] = regexen["Wisconsin1"]
regexen["Houston1Alt"] = regexen["Houston1"]
regexen["Oregon2"] = regexen["Oregon"]
regexen["OregonFalcon"] = regexen["Oregon2"]
regexen["GA3001"] = regexen["Atlanta"]
regexen["GA3002"] = regexen["GA3001"]
regexen["GA3004"] = regexen["GA3003"]
regexen["TX6001"] = regexen["Harlingen"]
regexen["TX6002"] = regexen["Albuquerque2"]
regexen["TN1391"] = regexen["Nashville"]
regexen["MS1392"] = regexen["Mississippi2"]
regexen["GreenvilleNC"] = regexen["NorthCarolina"]
regexen["FL9001"] = regexen["Jacksonville"]
regexen["FL9002"] = regexen["FL9001"]
regexen["FL9003"] = regexen["FL9001"]
regexen["NC9101"] = regexen["NorthCarolina"]
regexen["NC9102"] = regexen["NC9101"]
regexen["VA9301"] = regexen["VUPSNewFCV3"]
regexen["TN9401"] = regexen["Nashville"]
regexen["NM1101"] = regexen["Albuquerque2"]
regexen["AZ1102"] = regexen["Arizona"]
regexen["Washington4"] = regexen["Washington"]
regexen["Washington5"] = regexen["Washington"]
regexen["Washington6"] = regexen["Washington"]
regexen["HarlingenTESS"] = regexen["Harlingen"]
regexen["Delaware3"] = regexen["Delaware1"]
regexen["Delaware5"] = regexen["Delaware1"]
regexen["Delaware6"] = regexen["Delaware1"]
regexen["Delaware7"] = regexen["Delaware1"]
regexen["TX1103"] = regexen["TX6001"]
regexen["AR1393"] = regexen["Arkansas"]
regexen["WestVirginiaAEP"] = regexen["WestVirginia"]
regexen["TX6003"] = regexen["Houston1"]
regexen["Dallas2New"] = regexen["Houston1"]
regexen["MississippiTN"] = regexen["Tennessee"]
regexen["Greenville3"] = regexen["GreenvilleNew"]
regexen["IdahoNew"] = regexen["Idaho"]
regexen["WashingtonState2"] = regexen["WashingtonState"]
regexen["Jacksonville2"] = regexen["Jacksonville"]
regexen["NorthCalifornia2"] = regexen["NorthCalifornia"]
regexen["Houston3"] = regexen["Dallas1"]
regexen["SC1421"] = regexen["GreenvilleNew"]
regexen["BatonRouge3"] = regexen["BatonRouge1"]
regexen["Atlanta2"] = regexen["GA3002"]
regexen["LA1411"] = regexen["BatonRouge1"]
regexen["NewJersey2"] = regexen["NewJersey"]
regexen["NewJersey2a"] = regexen["NewJersey"]
regexen["NewJersey3"] = regexen["NewJersey"]
regexen["Atlanta4"] = regexen["GA3003"]
regexen["Atlanta5"] = regexen["GA3004"]
regexen["GA3003a"] = regexen["Atlanta"]
regexen["GA3004a"] = regexen["Atlanta"]
regexen["GA3004b"] = regexen["GA3004"]
regexen["GA3006"] = regexen["GA3003"]
regexen["GA3007"] = regexen["GA3003"]
regexen["Atlanta4a"] = regexen["Atlanta"]
regexen["Atlanta5a"] = regexen["Atlanta"]
regexen["Atlanta5b"] = regexen["Atlanta5"]
regexen["Dallas4"] = regexen["Dallas1"]
regexen["TX6004"] = regexen["Dallas1"]
regexen["Oregon3"] = regexen["OregonEM"]
regexen["SouthCaliforniaATT"] = regexen["SouthCalifornia"]
regexen["SanDiego"] = regexen["SouthCalifornia"]
regexen["SouthCaliforniaSDG"] = regexen["SouthCalifornia"]
regexen["TennesseeKorterra"] = regexen["Tennessee"]
regexen["SouthCaliforniaSCA2Korterra"] = regexen["SouthCaliforniaSDGKorterra"]
regexen["NorthCarolina2"] = regexen["NorthCarolina"]
regexen["SC1423"] = regexen["SC1421"]
regexen["SC1422_ATT"] = regexen["SC1422"]
regexen["AR1412"] = regexen["Arkansas"]
regexen["NewJersey2010B"] = regexen["NewJersey2010"]
regexen["NewJersey5"] = regexen["PA7501"]
regexen["TN811"] = regexen["NC811"]
regexen["DelawareXML"] = regexen["FairfaxXML"]
regexen["WashingtonXML"] = regexen["DelawareXML"]
regexen["OCC5XML"] = regexen["FairfaxXML"]
regexen["TN3005"] = regexen["Tennessee"]
regexen["Baltimore2"] = regexen["Baltimore"]
regexen["Baltimore3"] = regexen["Baltimore"]
regexen["PensacolaAL"] = regexen["AlabamaNew"]
regexen["OCC4XML"] = regexen["FairfaxXML"]
regexen["OCC6XML"] = regexen["FairfaxXML"]
regexen["SouthCalifornia9"] = regexen["SouthCaliforniaATT"]
regexen["NorthCalifornia3"] = regexen["NorthCaliforniaATT"]
regexen["SouthCaliforniaNV"] = regexen["NorthCaliforniaATT"] # sic
regexen["SC1424"] = regexen["SC1421"]
regexen["SC1425"] = regexen["SC1421"]
regexen["SC2051"] = regexen["SC1421A"]
regexen["Oregon4"] = regexen["Oregon2"]
regexen["Oregon5"] = regexen["Oregon4"]
regexen["Oregon6"] = regexen["Oregon4"]
regexen["WashingtonState3"] = regexen["WashingtonState2"]
regexen["WashingtonState4"] = regexen["WashingtonState2"]
regexen["WashingtonState5"] = regexen["WashingtonState2"]
regexen["NC2101a"] = regexen["NC811"]
regexen["SouthCalifornia10"] = regexen["SouthCalifornia"]
regexen["AL811"] = regexen["AlabamaNew"]
regexen["OregonPortland"] = regexen["Oregon2"]
regexen["SC1425"] = regexen["SC1421"]
regexen["SC1426A"] = regexen["SC1421A"]
regexen["SC1425A"] = regexen["SC1421A"]
regexen["SC1427A"] = regexen["SC1421A"]
regexen["SC1421PUPS"] = regexen["SC1421A"]
regexen["NorthCalifornia4"] = regexen["NorthCalifornia"]
regexen["NorthCalifornia5"] = regexen["NorthCalifornia"]
regexen["NewJersey9"] = regexen["NewJersey2010"]
regexen["NorthCaliforniaATT2016"] = regexen["NorthCalifornia2016"]
regexen["NorthCaliforniaATT2016_3"] = regexen["NorthCaliforniaATT2016"]
regexen["NorthCalifornia2016_5"] = regexen["NorthCalifornia2016"]
regexen["NorthCalifornia2016_2"] = regexen["NorthCalifornia2016"]

def recognize(raw):
    """ Inspect raw ticket data and return a string indicating its type.
        This string is the start of the name of the appropriate parser, e.g.
        "Atlanta" for AtlantaParser, "BatonRouge" for BatonRougeParser, etc.
        Returns None if no parser was found.
    """
    # first, try all regular expressions in the 'regexen' dict
    for name, regex in regexen.items():
        m = regex.search(raw)
        if m:
            return name

    # if we're here, then apparently nothing was found
    return None

###
### summary tickets

CLICKSCREENER_AUDIT = R("ClickScreener EOD for", re.DOTALL|re.MULTILINE)

s_regexen = {
    "Atlanta": R("^[A-Z0-9]+ \*SUM\* GAUPC"),
    "Atlanta4": R("^[A-Z0-9]+\s+\*SUM\* UPCA"),
    "NorthCarolina": R("\S+ +\*(EOD|SUM)\* NCOC[a-z]"),
    "NewJersey": R("(NJ1C audit|^SEQUENCE NUMBER \d\d\d\d +CDC =)"),
    "Pennsylvania": R("END-OF-DAY SUMMARY AUDIT.*PENNSYLVANIA ONE CALL",
     re.DOTALL),

    # these are all the same:
    "Richmond1": R("^MISSU.*\nSUMM", re.DOTALL),  # same as Lanham
    "Lanham": R("^MISSU.*\nSUMM", re.DOTALL),
    "Baltimore": R("^MISSU.*\nSUMM", re.DOTALL),
    "Baltimore3": R("^MISSU.*\nSUMM", re.DOTALL),
    "Fairfax": R("^MISSU.*\nSUMM", re.DOTALL),
    "Washington": R("^MISSU.*\nSUMM", re.DOTALL),
    "Washington5": R("^MISSU.*\nSUMM", re.DOTALL),
    "Washington6": R("^MISSU.*\nSUMM", re.DOTALL),
    "Delaware1": R("^MISSU.*\nSUMM", re.DOTALL),
    "Delaware5": R("^MISSU.*\nSUMM", re.DOTALL),
    "Delaware6": R("^MISSU.*\nSUMM", re.DOTALL),
    "Delaware7": R("^MISSU.*\nSUMM", re.DOTALL),
    "Houston1": R("^LONESTAR.*\nSUMM", re.DOTALL),
    "Houston2016": R(START + 'LONE ?STAR 811' + ALL_0_N + START + 'SUMM'),
    "Wisconsin1": R("TELDIG SYSTEMS ONECALL.*THE FOLLOWING MESSAGE", re.DOTALL),
    "Wisconsin2": R("^Diggers Hotline.*\nSUMM", re.DOTALL),
    "NewJersey2010": R("New Jersey One Call.*SUMM.*DAILY AUDIT", re.DOTALL),

    "Richmond2": R("(^TICKET SUMMARY REPORT.*O N I S I|"\
     "TICKET SUMMARY REPORT AT)", re.DOTALL),
    "BatonRouge1": R("Louisiana One Call\nSUMM\nDAILY AUDIT OF TICKETS",
     re.DOTALL),
    "BatonRouge2": R("Louisiana One Call\nSUMM\nDAILY AUDIT OF TICKETS",
     re.DOTALL),
    "WestVirginia": R("SEQUENCE NUMBER.*Audit for CDC(.*Last Audit was)?",
     re.DOTALL),
    "Ohio": R("OHIO UTILITIES PROTECTION.*MESSAGE LIST BY STATION",
     re.DOTALL),
    "SouthCarolina": R("PUPS DAILY AUDITS for \S+ on"),
    "Tennessee": R("From (TOCS|TNOCS).*Audit For.*TOTAL FOR",
                 re.DOTALL|re.IGNORECASE),
    "Richmond3": R("[A-Z0-9]+ \*(SUM|EOD)\* VUPS[a-z] \d\d"),
    "Nashville2013": R("From TNOCS.*Audit For.*UTILNASH.*Compliant", re.DOTALL),

    # Orlando and Jacksonville are the same
    "Orlando": R("CALL SUNSHINE.*NOW ENDING TRANSMISSION", re.DOTALL),
    "Jacksonville": R("CALL SUNSHINE.*NOW ENDING TRANSMISSION", re.DOTALL),
    "Pensacola": R("CALL SUNSHINE.*NOW ENDING TRANSMISSION", re.DOTALL),

    "Colorado": R("This is your End of Day Audit from IRTHNet for items sent"),
    "Colorado2": R("This is your End of Day Audit from IRTHNet for items sent"),
    "Illinois": R("[A-Z0-9]+ +\*(EOD|SUM)\* JULIE \d\d"),

    "SouthCalifornia": R("[A-Z0-9]+ +\*(EOD|SUM)\*[A-Z]? USAS \d\d"),
    "SouthCalifornia11": R('KORTERRA AUDIT'),
    "NorthCalifornia": R("[A-Z0-9]+ +USAN +\d\d.*DAILY TICKET RECAP",
     re.DOTALL),
    "NorthCalifornia2016": R(r'[A-Z0-9]+ +\*(EOD|SUM)\* +USAN[A-Z] +[0-9/]+'),
    "NorthCalifornia2016_5": R('KORTERRA AUDIT'),
    "Alabama": R("[A-Z0-9]+ +\*(SUM|EOD)\* AOC \d\d"),
    "Kentucky": R("MESSAGE NUMBERS WERE TRANSMITTED TO YOU TODAY"),
    "Dallas1": R("(TESS audit|^SEQUENCE NUMBER \d+ +CDC =)"),
    "DIGGTESS": R("From TESS.*Audit For", re.DOTALL),
    "Dallas2": R("(TESS audit|WELLSCO audit|^SEQUENCE NUMBER \d+ +CDC =)"),
    "Arkansas": R("From AROCS.*Audit for", re.DOTALL|re.IGNORECASE),
    "Tulsa": R("FROM OKOCS.*AUDIT FOR", re.DOTALL),
    "Harlingen": R("(TESS audit for|Audit for.*Last Audit|Audit for CDC|^SEQUENCE NUMBER \d+ +CDC =)"),
    "AlabamaNew": R("FROM ALOCS?.*AUDIT FOR", re.DOTALL),
    "Mississippi": R("From MSOCS.*Audit For", re.DOTALL|re.IGNORECASE),
    "TulsaWellsco": R("(WELLSCO audit|^SEQUENCE NUMBER \d+ +CDC =)"),
    "Washington2": R(
     "AUDIT.*\d\d\d\d\/\d\d\/\d\d.*This is your End of Day Audit", re.DOTALL),
    "Albuquerque": R("End of Day Audit from (US West|Qwest Local)"),
    "Houston2": R("Attached Tickets To Locate for Utili"),
    "Albuquerque2": R("NEW MEXICO ONE CALL GOOD NIGHT"),
    "Oregon": R("Oregon One Call.*SUMM", re.DOTALL),  # FIXME
    #"Arizona": R("MISSU.*\nSUMM", re.DOTALL),   # FIXME -- probably not correct
    "Arizona": R("Arizona Blue Stake.*DESTINATION AUDIT", re.DOTALL),
    "GreenvilleNew": R("PUPS AUDIT.*UTILIQUEST", re.DOTALL),
    "SC1421": R("PUPS AUDIT.*(UTILIQUEST|SCEG GAS|SCEG ELECTRIC|SCE&G ELECTRIC|CONSOLIDATED UTILITY SERVICES)", re.DOTALL),
    "SC1422_ATT": R("PUPS AUDIT.*(UTILIQUEST|CONSOLIDATED UTILITY SERVICES)", re.DOTALL),
    "SC1422": R("This is your End of Day Audit from IRTHNet for items sent.+?(\(PUPS\))",flags=re.DOTALL),
    "SC1421A": R("Audit For [0-9/]+"),
    "Greenville3": R("PUPS AUDIT.*DUKE ENERGY", re.DOTALL),
    "WashingtonState": R("Washington One Call.*SUMM.*DAILY AUDIT", re.DOTALL),
    "QWEST_IDL": R("End of Day Audit from (US West|Qwest Local|QWEST COMM)"),
    #"OhioNew": R("MESSAGE FROM OUPS.*Ticket summary"),
    "GA3002": R("Notified Date.*Total Tickets Sent:", re.DOTALL),
    "AZ1102": R("Arizona Blue Stake.*DESTINATION AUDIT FOR", re.DOTALL),
    "Alaska": R("TELDIG SYSTEMS ONECALL.*GOOD NIGHT.*MESSAGE NUMBERS WERE TRANSMITTED", re.DOTALL),
    "HarlingenTESS": R("End of Day Audit from"),
    "Delaware3": R("KORTERRA AUDIT \S+ \*EOD AUDIT\* FOR TICKETS"),
    "Utah": R("BAJA +?\*(SUM|EOD)\* UTAH[a-z] \d"),
    "Nevada": R("[A-Z0-9]+ +USA[NS] +\d\d.*DAILY TICKET RECAP", re.DOTALL),
    "Idaho": R("TELDIG SYSTEMS ONECALL.*GOOD NIGHT.*MESSAGE NUMBERS WERE TRANSMITTED", re.DOTALL),
    "SouthCaliforniaCOX": R("This is your End of Day Audit from IRTHNet"),
    "KentuckyNew": R("[A-Z0-9]+ \*EOD\* KUPI[a-z] \d\d\/"),
    "Northwest": R("Total Tickets:"),
    "NewJersey2a": R("This is your End of Day Audit from First Energy"),
    "HoustonKorterra": R("KORTERRA AUDIT.*FOR TICKETS SENT ON"),
    "MississippiKorterra" : R("KORTERRA AUDIT.*(JCKSN|SOUTH|TNC)"),
    "SouthCaliforniaATT": R("Daily Summary Report (\d\d\d\d\d\d)"),
    "NewJersey3": R('"Sequence","ID",'),
    "FL9002": R('"Sequence","ID",'), # new since Mar 2007
    "OregonEM": R("(Oregon|Washington) One Call.*SUMM", re.DOTALL),
    "Dallas4": R("This is your End of Day Audit from IRTHNet for items sent.+?(\(TESS\))",flags=re.DOTALL),
    "VUPSNewOCC4": R("This is your End of Day Audit from IRTHNet for items sent.+?(\(VUPS\))",flags=re.DOTALL),
    "Oregon3": R("This is your End of Day Audit from IRTHNet for items sent.+?(\(WOC\))",flags=re.DOTALL),
    "NorthCarolina2": R("This is your End of Day Audit from IRTHNet for items sent.+?(\(NCOC\))",flags=re.DOTALL),
    "Oregon4": R("This is your End of Day Audit from IRTHNet for items sent"),
    "VUPSNewOCC3_2":R("ClickScreener EOD for"),
    "SouthCaliforniaSDGKorterra": R("KORTERRA AUDIT \S+ \*EOD AUDIT\* FOR TICKETS SENT ON"),
    "Washington4": CLICKSCREENER_AUDIT,
    "NewJersey4": R("FROM (UFPO|DSNY).*TICKET AUDIT FOR TRANSMISSIONS",
                  re.DOTALL|re.MULTILINE),
    "GA3004b": R("\*(SUM|EOD)\*.*?SENTRi CENTER SIGNING OFF",
               re.DOTALL|re.MULTILINE),
    "Atlanta5b": R("\*(SUM|EOD)\*.*?SENTRi CENTER SIGNING OFF",
                 re.DOTALL|re.MULTILINE),
    "WV1181": R("Miss Utility.*Audit For", re.MULTILINE|re.DOTALL),
    "GA3006": R("EOD ATTGA.*Sequence.*Sent", re.DOTALL|re.MULTILINE),
    "AL811": R("From AL811.*Audit For", re.DOTALL|re.MULTILINE),

    # todo(dan) finish these
    "OUPS1": R("This is your End of Day Audit from IRTHNet"),
    "OUPS2": R("This is your End of Day Audit from IRTHNet"),

    # XML tickets
    "FairfaxXML": R("<center>VUPS[abc]</center>.*?"\
                  + "<transmission_type>EOD</transmission_type>", re.S|re.M),
    "GeorgiaXML": R("<AUDIT>", re.S|re.M), # tentative
}

s_regexen["TennesseeGA"] = s_regexen["Atlanta"]
s_regexen["TennesseeNC"] = s_regexen["NorthCarolina"]
s_regexen["Nashville"] = s_regexen["Tennessee"]
s_regexen["NashvilleKorterra"] = s_regexen["SouthCaliforniaSDGKorterra"]
s_regexen["NashvilleKorterra2013"] = s_regexen["SouthCaliforniaSDGKorterra"]
s_regexen["GA3007"] = s_regexen["SouthCaliforniaSDGKorterra"]
s_regexen["SanAntonio"] = s_regexen["Dallas1"]
s_regexen["AlabamaMobile"] = s_regexen["Alabama"]
s_regexen['Mississippi2'] = s_regexen['Mississippi']
s_regexen['MississippiLA'] = s_regexen['BatonRouge1']
s_regexen["Chesapeake1"] = s_regexen["Richmond3"]
s_regexen["Chesapeake2"] = s_regexen["NorthCarolina"]
s_regexen["AlabamaMobileNew"] = s_regexen["AlabamaNew"]
s_regexen["RichmondNC"] = s_regexen["Chesapeake2"]
s_regexen["AlabamaFL"] = s_regexen["Pensacola"]
s_regexen["Jackson"] = s_regexen["Tennessee"]
s_regexen["Chattanooga"] = s_regexen["Tennessee"]
s_regexen["ChattanoogaGA"] = s_regexen["TennesseeGA"]
s_regexen["ChattanoogaNC"] = s_regexen["TennesseeNC"]
s_regexen["Fairfax2"] = s_regexen["Richmond3"]
s_regexen["Fairfax6"] = s_regexen["Fairfax2"]
s_regexen["Delaware2"] = s_regexen["Richmond3"]
s_regexen['Washington3'] = s_regexen['Richmond3']
s_regexen['WashingtonVA'] = s_regexen['Richmond3']
s_regexen["Greenville"] = s_regexen["SouthCarolina"]
s_regexen["AlbuquerqueAZ"] = s_regexen["Arizona"]
s_regexen["AlbuquerqueTX"] = s_regexen["Dallas1"]
s_regexen["Wisconsin2a"] = s_regexen["Wisconsin1"]
s_regexen["Houston1Alt"] = s_regexen["Houston2"]
s_regexen["GA3001"] = s_regexen["Atlanta"]
s_regexen["GA3003"] = s_regexen["Atlanta4"] # we assume this for now
s_regexen["GA3004"] = s_regexen["Atlanta4"]
s_regexen["TX6001"] = s_regexen["Harlingen"]
s_regexen["TX6002"] = s_regexen["Albuquerque2"]
s_regexen["ColoradoNE"] = s_regexen["Oregon"]
s_regexen["OhioNew"] = s_regexen["Ohio"]
s_regexen["TN1391"] = s_regexen["Nashville"]
s_regexen["MS1392"] = s_regexen["Mississippi2"]
s_regexen["PA7501"] = s_regexen["Pennsylvania"]
s_regexen["GreenvilleNC"] = s_regexen["NorthCarolina"]
s_regexen["FL9001"] = s_regexen["Jacksonville"]
s_regexen["NC9101"] = s_regexen["NorthCarolina"]
s_regexen["NC9102"] = s_regexen["NC9101"]
s_regexen["TN9401"] = s_regexen["Nashville"]
s_regexen["NM1101"] = s_regexen["Albuquerque2"]
s_regexen["TX1103"] = s_regexen["TX6001"]
s_regexen["AR1393"] = s_regexen["Arkansas"]
s_regexen["MississippiTN"] = s_regexen["Tennessee"]
s_regexen["IdahoNew"] = s_regexen["Idaho"]
s_regexen["WashingtonState2"] = s_regexen["Oregon4"]
s_regexen["Jacksonville2"] = s_regexen["Jacksonville"]
s_regexen["NorthCalifornia2"] = s_regexen["NorthCalifornia"]
s_regexen["Houston3"] = s_regexen["Dallas1"]
s_regexen["AlaskaNew"] = s_regexen["Alaska"] # for now
s_regexen["Atlanta2"] = s_regexen["GA3002"]
s_regexen["LA1411"] = s_regexen["BatonRouge1"]
s_regexen["NewJersey2"] = s_regexen["NewJersey"]
s_regexen["BatonRouge3"] = s_regexen["NewJersey3"]
s_regexen["Atlanta4"] = s_regexen["GA3003"]
s_regexen["Atlanta5"] = s_regexen["Atlanta4"]
s_regexen["GA3003a"] = s_regexen["Atlanta4"]
s_regexen["GA3004a"] = CLICKSCREENER_AUDIT
s_regexen["Atlanta4a"] = s_regexen["Atlanta4"]
s_regexen["Atlanta5a"] = CLICKSCREENER_AUDIT
s_regexen["TX6004"] = s_regexen["Dallas4"]
s_regexen["VUPSNew"] = s_regexen["Richmond3"]
s_regexen["VUPSNewFCV3"] = s_regexen["VUPSNew"]
s_regexen["VUPSNewFPK1"] = s_regexen["VUPSNew"]
s_regexen["VUPSNewOCC2"] = s_regexen["VUPSNew"]
s_regexen["VUPSNewOCC6"] = s_regexen["VUPSNewOCC2"]
s_regexen["VUPSNewOCC3"] = s_regexen["VUPSNew"]
s_regexen["VUPSNewFDE2"] = s_regexen["VUPSNew"]
s_regexen["VUPSNewFMW3"] = s_regexen["VUPSNew"]
s_regexen["VUPSNewFMW2"] = s_regexen["VUPSNew"]
s_regexen["QWEST_IEUCC"] = s_regexen["QWEST_IDL"]
s_regexen["QWEST_OOC"] = s_regexen["QWEST_IDL"]
s_regexen["QWEST_UULC"] = s_regexen["QWEST_IDL"]
s_regexen["QWEST_WOC"] = s_regexen["QWEST_IDL"]
s_regexen["QWEST_IRTH"] = s_regexen["QWEST_IDL"]
s_regexen["Oregon2"] = s_regexen["Oregon"]
s_regexen["OregonFalcon"] = s_regexen["Oregon2"]
s_regexen["VA9301"] = s_regexen["VUPSNewFCV3"]
s_regexen["WestVirginiaAEP"] = s_regexen["Washington2"]
s_regexen["TX6003"] = s_regexen["Houston1"] # for now
s_regexen["Dallas2New"] = s_regexen["Houston1"] # for now
s_regexen["NorthCaliforniaATT"] = s_regexen["SouthCaliforniaATT"]
s_regexen["FL9003"] = s_regexen["FL9002"]
s_regexen["SanDiego"] = s_regexen["SouthCalifornia"]
s_regexen["SouthCaliforniaSDG"] = s_regexen["SouthCalifornia"]
s_regexen["OregonXML"] = s_regexen["Oregon"]
s_regexen["TennesseeKorterra"] = s_regexen["HoustonKorterra"]
s_regexen["SouthCaliforniaSCA2Korterra"] = s_regexen["SouthCaliforniaSDGKorterra"]
s_regexen["SC1423"] = s_regexen["SC1422"]
s_regexen["AR1412"] = s_regexen["Arkansas"]
s_regexen["NewJersey2010B"] = s_regexen["HoustonKorterra"]
#s_regexen["NorthCalifornia_A"] = s_regexen["NorthCalifornia"]
s_regexen["NewJersey5"] = s_regexen["PA7501"]
s_regexen["NC811"] = s_regexen["NorthCarolina"] # assume this for now
s_regexen["TN811"] = s_regexen["NC811"]
s_regexen["DelawareXML"] = s_regexen['FairfaxXML']
s_regexen["OCC5XML"] = s_regexen['FairfaxXML']
s_regexen["WashingtonXML"] = s_regexen["FairfaxXML"]
s_regexen["TN3005"] = s_regexen["Tennessee"]
s_regexen["WV1181Alt"] = s_regexen["HoustonKorterra"]
s_regexen["Baltimore2"] = s_regexen["HoustonKorterra"] # FIXME
s_regexen["Baltimore4"] = s_regexen["Baltimore"]
s_regexen["PensacolaAL"] = s_regexen["AlabamaNew"]
s_regexen["WV1181Korterra"] = s_regexen["WV1181Alt"]
s_regexen["OCC4XML"] = s_regexen["FairfaxXML"]
s_regexen["OCC6XML"] = s_regexen["FairfaxXML"]
s_regexen["PA7501Meet"] = s_regexen["PA7501"]
s_regexen["SC1422A"] = s_regexen["SC1421A"]
s_regexen["SouthCalifornia9"] = s_regexen["SouthCaliforniaATT"]
s_regexen["NorthCalifornia3"] = s_regexen["NorthCaliforniaATT"]
s_regexen["SouthCaliforniaNV"] = s_regexen["NorthCaliforniaATT"] # sic
s_regexen["SC1444"] = s_regexen["SC1421"]
s_regexen["SC1424"] = s_regexen["SC1421"]
s_regexen["SC2051"] = s_regexen["SC1421A"]
s_regexen["Oregon5"] = s_regexen["Oregon4"]
s_regexen["Oregon6"] = s_regexen["Oregon4"]
s_regexen["WashingtonState3"] = s_regexen["WashingtonState2"]
s_regexen["WashingtonState4"] = s_regexen["WashingtonState2"]
s_regexen["WashingtonState5"] = s_regexen["WashingtonState2"]
s_regexen["NC2101"] = s_regexen["NC811"]
s_regexen["NC2101a"] = s_regexen["NC811"]
s_regexen["SouthCalifornia10"] = s_regexen["SouthCalifornia"]
s_regexen["NewJersey6"] = s_regexen["Kentucky"]
s_regexen["NewJersey7"] = s_regexen["Kentucky"]
s_regexen["OregonPortland"] = s_regexen["Oregon2"]
s_regexen["SC1425"] = s_regexen["SC1421"]
s_regexen["SC1425A"] = s_regexen["SC1421A"]
s_regexen["SC1421PUPS"] = s_regexen["SC1421A"]
s_regexen["SC1426A"] = s_regexen["SouthCaliforniaSDGKorterra"]
s_regexen["SC1427A"] = s_regexen["SouthCaliforniaSDGKorterra"]
s_regexen["NewJersey8"] = s_regexen["NewJersey2010"]
s_regexen["NorthCalifornia4"] = s_regexen["NorthCalifornia"]
s_regexen["NorthCalifornia5"] = s_regexen["NorthCalifornia"]
s_regexen["NewJersey9"] = s_regexen["NewJersey2010"]
s_regexen["NorthCaliforniaATT2016"] = s_regexen["NorthCaliforniaATT"]
s_regexen["NorthCaliforniaATT2016_3"] = s_regexen["NorthCaliforniaATT2016"]
s_regexen["NorthCalifornia2016_4"] = s_regexen["NorthCalifornia2016"]
s_regexen["NorthCalifornia2016_2"] = s_regexen["NorthCalifornia2016"]

def recognize_summary(raw):
    """ Inspect raw summary data and return a string indicating its type.
        Works like recognize(). """
    for name, regex in s_regexen.items():
        m = regex.search(raw)
        if m:
            return name
    return None

###
### "good morning" message regexen
### Can also be used for other types of messages/tickets that need to be
### ignored.

gm_regexen = {
    "Alabama": (
        R("[A-Z0-9]+ +\*(SOD|MSG)\* +AOC \d\d"),
    ),
    "AlabamaNew": (
        R("MESSAGE FROM ALOCS"),
        R("GeoCall could not create the text of the audit"),
    ),
    "Albuquerque": (
        R("New Mexico One Call <FREEFORMS>"),
    ),
    "Arizona": (
        R("Message From: Arizona Blue Stake"),
    ),
    "Arkansas": (
        R("MESSAGE FROM AROCS"),
    ),
    "Atlanta": (
        R("Message From: Utility Protection Center"),
        ),
    "BatonRouge1": (
        R("(BRDC|MESG).*FREE FORMAT MESSAGE SENT ON", re.DOTALL),
    ),
    "ChattanoogaGA": (
        R("Message From: Utility Protection Center"),
    ),
    "Colorado": (
        R("[A-Z0-9]+ +\*(ASR|MSG)\* UNCC"),
    ),
    "GeorgiaXML": (
        R("<MESSAGE>.*?<One_Call_Center>", re.S|re.M),
    ),
    "Greenville": (
        R("MESSAGE FROM PUPS"),
    ),
    "GreenvilleNew": (
        R("MESSAGE FROM SCOCS"),
        R("MESSAGE FROM PUPS"),
        R("Message From PUPS"),
    ),
    "Houston1": (
        R("MESG.*FREE FORMAT MESSAGE SENT ON", re.DOTALL),
        R("BRDC.*FREE FORMAT MESSAGE SENT ON", re.DOTALL),
    ),
    "Kentucky": (
        R("MESSAGE FROM KENTUCKY UNDERGROUND PROTECTION"),
        R("MESSAGE FROM KENTUCKY DIG SAFELY"),
        R("MESSAGE FROM KY DIG SAFELY"),
        R("Message From: KY Dig Safely"),
    ),
    "Nevada": (
        R("Message From: USAN"),
        #R("Good morning all"),
    ),
    "NorthCalifornia": (
        R("Message From: USAN"),
        R("NO RESPONSE NOTICE.*USA North", re.DOTALL),
        R("^\s*Center Message\s*$"),
    ),
    "NorthCarolina": (
        R("Good morning from the North Carolina One-Call Center"),
        R("[A-Z0-9\*]+ +\*MSG\* NCOC."),
        R("MESSAGE FROM PUPS"),
        ),
    "Ohio": (
        R("[A-Z0-9]+ +\*ASR\* UNCC"),
        R("Message From: OUPS"),
    ),
    "OhioNew": (
        R("MESSAGE FROM OUPS.*Seq"),
    ),
    "Orlando": (
        R("REPORT CALL SUNSHINE.*PAPER SUPPLY", re.DOTALL),
        R("Message From: Sunshine One Call Center"),
        R("CALL SUNSHINE.*NOW BEGINNING TICKET TRANSMISSION", re.DOTALL),
        ),
    "Oregon": (
        R("MESG.*FREE FORMAT MESSAGE", re.DOTALL),
    ),
    "Pennsylvania": (
        R("\*MSG\* POCS"),
        ),
    "Richmond1": (
        R("MESG.*FREE FORMAT.*GOOD MORNING - Miss Utility", re.DOTALL),
        R("ONISI.*GOOD MORNING.*MISS UTILITY", re.DOTALL),  # !
        R("MESG.*FREE FORMAT MESSAGE.*Miss Utility", re.DOTALL),
        R("MESG.*FREE FORMAT MESSAGE SENT ON.*Receiving Station", re.DOTALL),
        R("BRDC.*FREE FORMAT MESSAGE SENT ON", re.DOTALL),
        ),
    "Richmond2": (
        R("ONISI.*GOOD MORNING.*MISS UTILITY", re.DOTALL),
        R("UTIL11 ONISI.*(MEMO|NOTICE) TO ALL MISS UTILITY MEMBERS", re.DOTALL),
        ),
    "Richmond3": (
        R("[A-Z0-9]+ +\*MSG\* VUPS[a-z] \d\d"),
    ),
    "SanDiego": (
        R("[A-Z0-9]+ +\*(SOD|MSG)\*[AB]? +USAS"),
        R("ICU Message.*from SDGE"),
    ),
    "SouthCalifornia": (
        R("[A-Z0-9]+ +\*(SOD|MSG)\*[AB]? +USAS"),
    ),
    "Tennessee": (
        R("MESSAGE FROM TNOCS"),
    ),
    "Tulsa": (
        R("MESSAGE FROM OKOCS"),
    ),
    "Utah": (
        R("BLUE STAKES IS NOW STARTING TRANSMISSION FOR TODAY."),
        R("CHARTRT? \*MSG\* UTAH[a-z] \d"),
    ),
    "WestVirginia": (
        R("Message From: OUPS"),
    ),
    "Wisconsin2": (
        R("TELDIG SYSTEMS ONECALL <GOOD NIGHT> - DIGGERS HOTLINE"),
        R("MESG.*FREE FORMAT MESSAGE", re.DOTALL),
    ),
}
gm_regexen["AlabamaFL"] = gm_regexen["Orlando"]
gm_regexen["AlabamaMobile"] = gm_regexen["Alabama"]
gm_regexen["AlabamaMobileNew"] = gm_regexen["AlabamaNew"]
gm_regexen["Albuquerque2"] = gm_regexen["Albuquerque"]
gm_regexen["AR1393"] = gm_regexen["Arkansas"]
gm_regexen["AR1412"] = gm_regexen["Arkansas"]
gm_regexen["Atlanta4"] = gm_regexen["Atlanta"]
gm_regexen["Atlanta5"] = gm_regexen["Atlanta"]
gm_regexen["Baltimore"] = gm_regexen["Richmond1"]
gm_regexen["Baltimore3"] = gm_regexen["Richmond1"]
gm_regexen["Baltimore4"] = gm_regexen["Richmond1"]
gm_regexen["BatonRouge2"] = gm_regexen["BatonRouge1"]
gm_regexen["BatonRouge3"] = gm_regexen["BatonRouge1"]
gm_regexen["ChattanoogaNC"] = gm_regexen["NorthCarolina"]
gm_regexen["Chesapeake1"] = gm_regexen["Richmond3"]
gm_regexen["Chesapeake2"] = gm_regexen["NorthCarolina"]
gm_regexen["Colorado2"] = gm_regexen["Colorado"]
gm_regexen["Dallas2New"] = gm_regexen["Houston1"]
gm_regexen["Delaware1"] = gm_regexen["Richmond1"]
gm_regexen["Delaware2"] = gm_regexen["Richmond3"]
gm_regexen["Delaware3"] = gm_regexen["Delaware1"]
gm_regexen["Delaware5"] = gm_regexen["Delaware1"]
gm_regexen["Delaware6"] = gm_regexen["Delaware1"]
gm_regexen["Delaware7"] = gm_regexen["Delaware1"]
gm_regexen["Fairfax"] = gm_regexen["Richmond1"]
gm_regexen["Fairfax2"] = gm_regexen["Richmond3"]
gm_regexen["Fairfax6"] = gm_regexen["Fairfax2"]
gm_regexen["FL9001"] = gm_regexen["Orlando"]
gm_regexen["FL9002"] = gm_regexen["FL9001"]
gm_regexen["GA3001"] = gm_regexen["Atlanta"]
gm_regexen["GA3002"] = gm_regexen["GA3001"]
gm_regexen["GA3003"] = gm_regexen["Atlanta"]
gm_regexen["GA3004"] = gm_regexen["GA3003"]
gm_regexen["GA3006"] = gm_regexen["GA3003"]
gm_regexen["GreenvilleNC"] = gm_regexen["NorthCarolina"]
gm_regexen["HoustonAlt"] = gm_regexen["Houston1"]
gm_regexen["Jackson"] = gm_regexen["Tennessee"]
gm_regexen["Jacksonville"] = gm_regexen["Orlando"]
gm_regexen["Jacksonville2"] = gm_regexen["Jacksonville"]
gm_regexen["KentuckyNew"] = gm_regexen["Kentucky"]
gm_regexen["LA1411"] = gm_regexen["BatonRouge1"]
gm_regexen["MississippiLA"] = gm_regexen["BatonRouge1"]
gm_regexen["NC811"] = gm_regexen["NorthCarolina"] # assume this for now
gm_regexen["NC9101"] = gm_regexen["NorthCarolina"]
gm_regexen["NC9102"] = gm_regexen["NC9101"]
gm_regexen["NM1101"] = gm_regexen["Albuquerque2"]
gm_regexen["NorthCalifornia2"] = gm_regexen["NorthCalifornia"]
gm_regexen["AZ1102"] = gm_regexen["Arizona"]
gm_regexen["PA7501"] = gm_regexen["Pennsylvania"]
gm_regexen["Pensacola"] = gm_regexen["Orlando"]
gm_regexen["RichmondNC"] = gm_regexen["Chesapeake2"]
gm_regexen["TennesseeGA"] = gm_regexen["Atlanta"]
gm_regexen["TennesseeNC"] = gm_regexen["NorthCarolina"]
gm_regexen["TN811"] = gm_regexen["NC811"]
gm_regexen['VA9301'] = gm_regexen['Richmond3']
gm_regexen["Washington"] = gm_regexen["Richmond1"]
gm_regexen["Washington2"] = gm_regexen["Washington"]
gm_regexen['Washington3'] = gm_regexen['Richmond3']
gm_regexen['Washington4'] = gm_regexen['Washington']
gm_regexen['Washington5'] = gm_regexen['Washington']
gm_regexen['Washington6'] = gm_regexen['Washington']
gm_regexen["WashingtonState"] = gm_regexen["Oregon"]
gm_regexen["Wisconsin1"] = gm_regexen["Wisconsin2"]
gm_regexen["Wisconsin2a"] = gm_regexen["Wisconsin1"]
gm_regexen["Oregon2"] = gm_regexen["Oregon"]
gm_regexen["OregonFalcon"] = gm_regexen["Oregon"]
gm_regexen["TX6002"] = gm_regexen["Albuquerque"]
gm_regexen["TX6003"] = gm_regexen["Houston1"]
gm_regexen["SC1421"] = gm_regexen["GreenvilleNew"]
gm_regexen["Atlanta2"] = gm_regexen["GA3002"]
gm_regexen["TN3005"] = gm_regexen["Tennessee"]
gm_regexen["PensacolaAL"] = gm_regexen["AlabamaNew"]
gm_regexen["SC1424"] = gm_regexen["SC1421"]
gm_regexen["SC1425"] = gm_regexen["SC1421"]
gm_regexen["NorthCalifornia4"] = gm_regexen["NorthCalifornia"]
gm_regexen["SouthCalifornia11"] = gm_regexen["SouthCalifornia"]
gm_regexen["NorthCalifornia5"] = gm_regexen["NorthCalifornia"]

# Any file containing one of these strings will be considered to be a "good
# morning" message. (This check happens after testing for tickets and audits.)
GM_MARKERS = [
    #"Good morning all",
]

def recognize_gm_message(raw):
    """ Inspects raw data and returns the name of the call center if this is
        a ("good morning") message.
    """
    for call_center, regexen in sorted(gm_regexen.items()):
        for regex in regexen:
            m = regex.search(raw)
            if m:
                return call_center
    return None

#
# work orders

# Note: The call center (or equivalent) will always be specified, so all we
# need to be able to do is tell work orders apart from audits.

wo_regexen = {
    "Lambert": R(r'<XML Type="WorkOrder">'),
}

wo_audit_regexen = {
    "Lambert": R(r'<XML Type="Audit">'),
}

#
# general recognize functions

def recognize_as(data, format, work_order=False):
    """ If the given raw ticket (data) can be recognized as the given
        format, return a string "ticket" or "summary". Return an empty
        string if it wasn't recognized as such.
    """
    if work_order:
        rwo = wo_regexen.get(format, "")
        if rwo:
            if rwo.search(data):
                return "work_order"
        rwoa = wo_audit_regexen.get(format, "")
        if rwoa:
            if rwoa.search(data):
                return "work_order_audit"

    else:

        rt = regexen.get(format, "")
        if rt:
            m = rt.search(data)
            if m:
                return "ticket"

        rs = s_regexen.get(format, "")
        if rs:
            m = rs.search(data)
            if m:
                return "summary"

        rgm = gm_regexen.get(format, [])
        for regex in rgm:
            if regex.search(data):
                return "gm_message"
        for gm_marker in GM_MARKERS:
            if gm_marker in data:
                return "gm_message"

    # not recognized
    return ""

# XXX Too vague; in practice, we always have a call centers and a format
# suggestion (or multiple) to go by.
# TO BE DEPRECATED.
def recognize_type(data):
    """ Integrate the two recognize functions to recognize both normal
        tickets and summaries. Return None if no match was found.
    """
    # is it a regular ticket?
    format = recognize(data)
    if format:
        return ("ticket", format)

    # is it a summary?
    format = recognize_summary(data)
    if format:
        return ("summary", format)

    # is it a good morning message?
    format = recognize_gm_message(data)
    if format:
        return ("gm_message", format)

    return None

