@echo off
rem deploy.bat
rem Requires environment variables:
rem DEPLOY_USER
rem DEPLOY_PASSWORD
rem DEPLOY_SERVER
rem Also see sample_deploy_config.bat.

call deploy_config.bat
rem This should contain the environment variables needed for configuration

rem delete python.bat
del /q python.bat

@echo Checking out version %1...
rem Check out revision %1 into the staging directory
call python.exe svn_deploy.py %1
rem If svn returns with an error code, something went wrong, so we should not
rem proceed.
IF %ERRORLEVEL% NEQ 0 goto SvnError


rem quit all Python processes, killing them if necessary
rem if not exist python.bat xcopy /y staging\ticket_parse\python.bat .
call python.exe quitall.py -k

rem If everything was checked out/updated correctly, copy some files...
@echo Copying Python code...

rem Delete all *.pyc files
del /q *.pyc
mkdir callcenters
mkdir formats
mkdir lib
xcopy /y staging\ticket_parse\*.py .
xcopy /y staging\ticket_parse\*.exe .
xcopy /y staging\ticket_parse\*.egg .
xcopy /y staging\ticket_parse\clean.bat .
xcopy /y staging\ticket_parse\restart.bat .
xcopy /y staging\ticket_parse\run.bat .
xcopy /y staging\ticket_parse\sample_deploy_config.bat .
xcopy /y staging\ticket_parse\zip-upload.bat .
xcopy /y staging\callcenters\*.py callcenters
xcopy /y staging\formats\*.py formats
xcopy /y /e staging\lib\*.* lib
del /q test*.py

rem restart pycron / Python processes
call restart.bat

@echo Version %1 has been successfully deployed.

rem copy deploy.bat (this script) last
xcopy /y staging\ticket_parse\deploy.bat .

goto TheEnd

:SvnError
@echo ** AN ERROR OCCURRED WHILE DOWNLOADING CODE **
goto TheEnd

:VerError
@echo ** AN ERROR OCCURRED WHILE STAMPING THE SVN VERSION **
goto TheEnd

:TheEnd
