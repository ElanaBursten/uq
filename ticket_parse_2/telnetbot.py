# telnetbot.py
# Created: 13 Apr 2002, Hans Nowak
# Last updated: 14 Apr 2002, Hans Nowak

import telnetlib
import sys
#
import tools

# constant for TelnetBot.log()
SENT, RECEIVED, EVENT = "sent", "received", "event"

class TelnetBot:

    def __init__(self, host, port=23, timeout=60):
        self.verbose = 1
        self.logs = []
        self.timeout = timeout
        self.name = "telnetbot"
        self.file = sys.stdout

        self.show("Connecting to %s:%s\n" % (host, port))
        self.conn = telnetlib.Telnet(host, port)
        self.show("Connected\n")
        self.log(EVENT, "Connected to %s:%s\n" % (host, port))

    def log(self, who, text):
        self.logs.append((who, text))

    def show(self, text):
        if self.verbose:
            self.file.write(text)
            if hasattr(self.file, "flush"):
                self.file.flush()
    display = show

    def display_prompt(self, text):
        s = ">>> %s: %s" % (self.name, text)
        self.display(s)

    def read_until(self, text):
        try:
            z = self.conn.read_until(text, self.timeout)
        except:
            # probably a timeout -- XXX handle this appropriately
            print "An error occurred..."
            raise
        self.show(z)
        self.log(RECEIVED, z)
        # XXX How do I flush the queue?
        return z    # for convenience

    def read_until_regex(self, regexlist):
        """ Pass a list of regexen. (A single regex will be considered to be a
            list with that regex as the one element.) If text read matches one
            of these regexen, return a 3-tuple (idx, match, text), containing
            the index/number of the regex in the list (starting at 0), the
            match object, and the text read.
        """
        if not isinstance(regexlist, list):
            regexlist = [regexlist]
        try:
            (idx, match, text) = self.conn.expect(regexlist, self.timeout)
        except:
            raise
        self.show(text)
        self.log(RECEIVED, text)
        return (idx, match, text)

    def read_all(self):
        z = self.conn.read_all()
        self.show(z)
        self.log(RECEIVED, z)
        return z    # for convenience

    def write(self, text):
        text = tools.udecode(text) # never pass an unicode string
        self.conn.write(text)
        self.log(SENT, text)
        self.display_prompt(text)
    send = write

    def close(self):
        self.log(EVENT, "Connection closed")
        self.conn.close()


if __name__ == "__main__":

    bot = TelnetBot("tatum", 2323)
    bot.read_until("QUIT ]")
    bot.read_until("---\r\n")
    bot.write("QUIT\n")
    bot.read_all()
    bot.close()

    print len(bot.logs), "actions logged"
