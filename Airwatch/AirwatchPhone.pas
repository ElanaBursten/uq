unit AirwatchPhone;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Data.Win.ADODB, Vcl.StdCtrls, System.Actions, Vcl.ActnList, Vcl.ComCtrls, uADSI;

type
  TfrmAirWatch = class(TForm)
    UpdatePhoneCo: TADOQuery;
    ADOConn: TADOConnection;
    FileADOConnection: TADOConnection;
    qryAirwatchPhone: TADOQuery;
    dsAirwatchPhone: TDataSource;
    DBGrid1: TDBGrid;
    btnOpen: TButton;
    btnClose: TButton;
    btnSave: TButton;
    OpenDialog1: TOpenDialog;
    ActionList1: TActionList;
    Action1: TAction;
    Memo1: TMemo;
    StatusBar1: TStatusBar;
    qryRefPhoneCo: TADOQuery;
    UpdateEmail: TADOQuery;
    edtAtlas: TEdit;
    Label1: TLabel;
    qryEmpActive: TADOQuery;
    procedure btnOpenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Action1Update(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    AppPath, ErrFile, AirwatchFile, Errpath, ProcPath: String;
    AirwatchPath:string;
    Processed: Boolean;
    PhoneCoList: TStringList;
    ADSI:TADSI;
    procedure OpenFile;
    function connectDB: boolean;
    procedure SaveFile;
    function ExtractPhoneNumber(InputStr: String): String;
    procedure HandleException(Sender: TObject; E: Exception);
    procedure LogErrors(Filename: TFileName; ErrMsg: String);
    procedure OpenFileConsoleMode;
    procedure LogAppException(ErrorMsg:String);
    function IsValidEmail(const Value: string): boolean;

    { Private declarations }
  public
    { Public declarations }
    procedure CoreLogic;

  end;

   EFileNotFound = class(Exception);
  var
  frmAirWatch: TfrmAirWatch;

implementation
uses
  Inifiles, System.StrUtils, odMiscUtils;

{$R *.dfm}

procedure TfrmAirWatch.CoreLogic;
begin
  try
    ConnectDB;
    ADOConn.Open();
    OpenFileConsoleMode;
    SaveFile;
    Self.Close;
  except
    On E:Exception do
    begin
      Self.Close;
      LogAppException(E.Message);
    end;
  end;
end;

function TfrmAirWatch.ExtractPhoneNumber(InputStr: String): String;
var
  I: Integer;
  InputStr2:string;
const
  COUNTRY_CODE='+1';
begin
  Result := '';

  InputStr2:=ReplaceStr(InputStr,COUNTRY_CODE,'');

  for I := 1 to length(InputStr2) do
  begin
    if InputStr2[I] in ['0'..'9'] then
      Result := Result + InputStr2[I];
  end;

  Result := AnsiRightStr(Result, 10);
  if Length(Result) <> 10  then Result := 'ERROR'
  else
  begin
    Insert('-', Result, 4);
    Insert('-',Result, 8);
  end;
end;

procedure TfrmAirWatch.Action1Update(Sender: TObject);
begin
 //for buttons in GUI mode
  btnClose.Enabled := qryAirwatchPhone.Active;
  btnSave.Enabled  := qryAirwatchPhone.Active and (Trim(edtAtlas.Text) <> '');
end;

procedure TfrmAirWatch.btnCloseClick(Sender: TObject);
begin
  qryAirwatchPhone.Close;
end;

procedure TfrmAirWatch.btnOpenClick(Sender: TObject);
var
 Msg: String;
begin
  try
    OpenFile;
  except
    on E:Exception do
    begin
      Msg :=  'connection to file error: ' + E.Message;
      raise Exception.Create(Msg);
    end;
  end;
end;

procedure TfrmAirWatch.btnSaveClick(Sender: TObject);
begin
  if qryAirwatchPhone.Active then SaveFile;
end;

Procedure TfrmAirWatch.LogErrors(Filename: TFilename; ErrMsg: String);
var
  LogFile: TextFile;
begin
  AssignFile(LogFile, Filename);
  if FileExists(FileName) then
    Append (LogFile)
  else
    Rewrite(LogFile);
  try
    Writeln(LogFile, DateTimeToStr(Now) + ': ' + ErrMsg);
  finally
    CloseFile (LogFile);
  end;
end;

procedure TfrmAirWatch.HandleException(Sender: TObject; E: Exception);
begin;
  LogAppException(E.Message);
  if ParamCount > 0 then
    Application.ShowException(E);
end;

procedure TfrmAirWatch.LogAppException(ErrorMsg: String);
var
  Filename: TFileName;
  LogFile: TextFile;
begin
  Filename := ErrPath + 'ApplicationError.log';
  try
    AssignFile(LogFile, Filename);
    Rewrite(LogFile);
    Writeln(LogFile, DateTimeToStr(Now) + ': ' + ErrorMsg);
  finally
    CloseFile(LogFile);
  end;
end;

procedure TfrmAirWatch.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PhoneCoList.Free;
  ADSI.Free;
  qryAirwatchPhone.Close;
  ADOConn.Close;
  FileADOConnection.Close;
  if FileExists(AirwatchFile) and Processed then
   MoveFile(PChar(AirwatchFile),PChar(ProcPath+ExtractFileName(AirwatchFile)));
end;

procedure TfrmAirWatch.FormCreate(Sender: TObject);
var
  VersionString:string;
  ErrMsg: string;
  VerMatch, ATTMatch, TMobileMatch: Boolean;
begin

  Application.OnException := HandleException;
  Processed := False;
  PhoneCoList:= TStringList.Create;
  // BP QM-590
  ADSI := TADSI.Create(Self);


  if ParamCount > 0 then
  begin
    if GetFileVersionNumberString(Application.ExeName, VersionString) then
      StatusBar1.Panels[3].Text := VersionString;
    If not(connectDB) then
    begin
      showmessage('Cannot connect to database');
      btnOpen.Enabled := False;
    end;
  end;
end;

procedure TFrmAirWatch.OpenFileConsoleMode;
var
  Connstr: String;
  searchResult : TSearchRec;
  SheetList: TStringList;
begin
  try
    Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0;Data Source=';
    //find file matching *.xlsx
    SetCurrentDir(AirwatchPath);
    if Findfirst('*.xlsx', faAnyFile, searchResult) = 0 then
    begin
      AirwatchFile := AirwatchPath + searchResult.Name;
      FindClose(SearchResult);
    end else
      raise EFilenotFound.Create('Airwatch File Not Found');

    ErrFile := ErrPath + ChangeFileExt(ExtractFileName(AirwatchFile), '') + '.log';
    FileADOConnection.Close;
    Connstr := Connstr + AirWatchFile;
    try
      FileADOConnection.ConnectionString := Connstr;
      FileADOConnection.Open;
      SheetList := TStringlist.Create;
      FileADOConnection.GetTableNames(SheetList);
      qryAirwatchPhone.SQL.Clear;
      qryAirwatchPhone.SQL.Add('Select * from [' + SheetList[0] + ']');
      SheetList.Free;
      qryAirwatchPhone.Open;
    except
      on E:Exception do
        raise Exception.Create('Connection to File Error: ' + E.Message);
    end;
  except
    raise;
  end;
end;

procedure TfrmAirWatch.SaveFile;
var
  RowsAffected: Integer;
  InsRows: Integer;
  PhoneNbr: String;
  ErrMsg: String;
  ADName: String;
  PhoneCo: String;
  EMail: String;
  Character: Char;
  CharCount: Integer;
  Idx: Integer;
  UserName: String;
begin
  try
    if ParamCount > 0 then
      btnSave.Enabled := false;
    InsRows := 0;
    qryAirwatchPhone.First;
    while not qryAirwatchPhone.EOF do
    begin
      With DBGrid1.DataSource.DataSet do
      begin
        if Pos('iPad',FieldByName('Model').AsString) <> 0 then
        begin
          qryAirwatchPhone.Next;
          continue;
        end;
        PhoneNbr := ExtractPhoneNumber(Trim(FieldbyName('Phone').AsString));
        if PhoneNbr = 'ERROR' then
        begin
          ErrMsg := 'Invalid Phone Number for ADName: ' + ADName;
          if ParamCount > 0 then
            Memo1.Lines.Add(ErrMsg);
          LogErrors(ErrFile, ErrMsg);
          qryAirwatchPhone.Next;
          Continue;
        end;
        Email :=   Trim(FieldbyName('Email').AsString);
        ADName :=  Trim(FieldbyName('UserName').AsString);
        PhoneCo := Trim(FieldbyName('Current Carrier').AsString);

        // BP QM-590
        qryEmpActive.Parameters.ParamByName('adname').Value := ADName;
        qryEmpActive.Open;
        if qryEmpActive.EOF then
        begin
          qryEmpActive.Close;
          ErrMsg :=  'AD Name: ' + ADName + ' not found';
          if ParamCount > 0 then
            Memo1.Lines.Add(ErrMsg);
          LogErrors(ErrFile, ErrMsg);
          qryAirwatchPhone.Next;
          Continue;
        end else
        if qryEmpActive.FieldByName('Active').AsBoolean = False then
        begin
          qryEmpActive.Close;
          ErrMsg :=  'Employee: ' + ADName + ' is inactive';
          if ParamCount > 0 then
            Memo1.Lines.Add(ErrMsg);
          LogErrors(ErrFile, ErrMsg);
          qryAirwatchPhone.Next;
          Continue;
        end;

        Idx := PhoneCoList.IndexOfName(PhoneCo);
        if Idx  > -1 then
         PhoneCo := PhoneCoList.Values[PhoneCo];

        qryRefPhoneCo.Parameters.ParamByName('code').Value := PhoneCo;
        qryRefPhoneCo.Open;
        RowsAffected := -1;
        if qryRefPhoneCo.Eof then
        begin
          qryRefPhoneCo.Close;
          ErrMsg := 'Phone co ' + PhoneCo + ' for AD Name: ' +
             ADName + ', ' + 'Phone Nbr: ' + PhoneNbr + ' not in Reference table';
              if ParamCount > 0 then
            Memo1.Lines.Add(ErrMsg);
          LogErrors(ErrFile, ErrMsg);
          RowsAffected := 0;
        end else
        begin
          UserName := ADSI.CurrentUserName;
          UpdatePhoneCo.Parameters.ParamByName('ADName').Value := ADName;
          UpdatePhoneCo.Parameters.ParamByName('contactphone').Value := PhoneNbr;
          UpdatePhoneCo.Parameters.ParamByName('phoneco').Value := qryRefPhoneCo.FieldByName('ref_id').AsInteger;
          // BP QM-590
          if ParamCount > 0 then
            UpdatePhoneCo.Parameters.ParamByName('atlas_number').Value := edtAtlas.Text else
          UpdatePhoneCo.Parameters.ParamByName('atlas_number').Value := '1111';
          UpdatePhoneCo.Parameters.ParamByName('change_by').Value := UserName;
          RowsAffected := UpdatePhoneCo.ExecSQL;
          qryRefPhoneCo.Close;
        end;

        if RowsAffected < 1 then
        begin
          ErrMsg := 'Error writing phone nbr/phone company to Employee table for ADName: ' + ADName;
          if ParamCount > 0 then
            Memo1.Lines.Add(ErrMsg);
          LogErrors(ErrFile, ErrMsg);
          qryAirwatchPhone.Next;
          Continue;
        end;

        if Email.ToLower.Contains('no_email') or not IsValidEmail(Email) then
        begin
         ErrMsg := 'Invalid email: ' + EMail + ' for ADName: ' + ADName;
         LogErrors(ErrFile, ErrMsg);
        end else
        begin
          UpdateEmail.Parameters.ParamByName('email').Value := Email;
          UpdateEmail.Parameters.ParamByName('adname').Value := ADName;
          UpdateEmail.ExecSQL;
          if UpdateEmail.RowsAffected < 1 then
          begin
            ErrMsg := 'Email update failed. Check for missing or duplicate emp id in Users: ' +
              EMail + ' for ADName: ' + ADName;
            LogErrors(ErrFile, ErrMsg);
          end;
        end;

        Inc(InsRows);
        qryAirwatchPhone.Next;
      end;
    end;

    Processed := True;
    if FileExists(AirwatchFile) then
    begin
      fileADOConnection.Close;
      MoveFile(PChar(AirwatchFile),PChar(ProcPath+ExtractFileName(AirwatchFile)));
    end;

    if ParamCount > 0 then
    begin
      ShowMessage(InttoStr(InsRows) + ' updated rows');
      btnSave.Enabled := False;
    end;
  except
    raise;
  end;
end;

function TfrmAirWatch.IsValidEmail(const Value: string): boolean;
  function CheckAllowed(const s: string): boolean;
  var
    i: integer;
  begin
    Result:= False;
    for i:= 1 to Length(s) do
    begin
      // illegal char - no valid address
      if not (s[i] in ['a'..'z','A'..'Z','0'..'9','_','-','.','+']) then
        Exit;
    end;
    Result:= True;
  end;
var
  i: integer;
  namePart, serverPart: string;
begin
  Result:= False;

  i:= Pos('@', Value);
  if (i = 0) then
    Exit;
  if(pos('..', Value) > 0) or (pos('@@', Value) > 0) or (pos('.@', Value) > 0)then
    Exit;
  if(pos('.', Value) = 1) or (pos('@', Value) = 1) then
    Exit;

  namePart:= Copy(Value, 1, i - 1);
  serverPart:= Copy(Value, i + 1, Length(Value));
  if (Length(namePart) = 0)  or (Length(serverPart) < 5)    then
    Exit;                      // too short

  i:= Pos('.', serverPart);
  // must have dot and at least 3 places from end
  if (i=0) or (i>(Length(serverPart)-2)) then
    Exit;
  Result:= CheckAllowed(namePart) and CheckAllowed(serverPart);
end;

procedure TfrmAirWatch.OpenFile;
var
 Connstr: String;
 Sheetlist: TStringlist;
begin
  Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0;Data Source=';
  OpenDialog1.InitialDir := AirwatchPath;
  openDialog1.Filter := 'Excel files|*.xlsx';
  if OpenDialog1.Execute then
  begin
    If (AirwatchFile = '') or
     (Processed and (AirwatchFile <> OpenDialog1.FileName)) then
    begin
      Processed := False;
      Memo1.Clear;
      AirwatchFile := OpenDialog1.FileName;
      ErrFile := ErrPath + ChangeFileExt(ExtractFileName(AirwatchFile), '') + '.log';
    end;
    FileADOConnection.Close;
    Connstr := Connstr + OpenDialog1.FileName;
    try
      FileADOConnection.ConnectionString := Connstr;
      FileADOConnection.Open;
      SheetList := TStringlist.Create;
      FileADOConnection.GetTableNames(SheetList);
      qryAirwatchPhone.SQL.Clear;
      qryAirwatchPhone.SQL.Add('Select * from [' + SheetList[0] + ']');
      SheetList.Free;
      qryAirwatchPhone.Open;
    except
      raise;
    end;
    btnSave.Enabled := True;
  end;
end;

function TfrmAirWatch.connectDB: boolean;
var
  myPath:string;
  connStr: String;
  aDatabase: String;
  aServer: String;
  ausername: string;
  apassword: string;
  AirWatchINI: TIniFile;
begin
  Result := True;
  myPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
  try
    AirWatchINI := TIniFile.Create(myPath + 'AirWatch.ini');
    AirwatchFile := '';
    AirwatchPath := AirWatchIni.ReadString('AirwatchPaths', 'InPath', '');
    ErrPath := AirWatchIni.ReadString('AirwatchPaths', 'ErrPath', '');
    ForceDirectories(ErrPath);
    ProcPath := AirWatchIni.ReadString('AirwatchPaths', 'ProcPath', '');
    ForceDirectories(ProcPath);
    AirWatchINI.ReadSectionValues('PhoneCo', PhoneCoList);
    aServer := AirWatchINI.ReadString('Database', 'Server', '');
    aDatabase := AirWatchINI.ReadString('Database', 'DB', 'QM');
    ausername := AirWatchINI.ReadString('Database', 'UserName', '');
    apassword := AirWatchINI.ReadString('Database', 'Password', '');
    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword +';Persist Security Info=True;User ID=' + ausername +';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;
    if ParamCount > 0 then
      Memo1.Lines.Add(connStr);
    ADOConn.ConnectionString := connStr;
    try
      ADOConn.Open;
      if (ParamCount > 0) then
      begin
        StatusBar1.Panels[1].Text :=  aServer;
        Memo1.Lines.Add('Connected to database');
      end;

    except
      on E:Exception do
      begin
        Result := False;
        if (ParamCount > 0) then
        begin
          LogAppException('Could not connect to database: ' + E.Message);
            Memo1.Lines.Add('Could not connect to DB');
        end else
         raise exception.Create('Could not connect to database: ' + E.Message);
      end;
    end;
  finally
    FreeAndNil(AirWatchINI);
  end;
end;

end.

