program AirQ;
{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

uses
  Vcl.Forms,
  Windows,
  SysUtils,
  AirwatchPhone in 'AirwatchPhone.pas' {frmAirWatch},
  GlobalSU in '..\common\GlobalSU.pas';

var
  MyInstanceName: string;

procedure TestParams;
begin
  if ParamCount = 0 then
  begin
    frmAirWatch.CoreLogic();
    Application.ShowMainForm := False;
  end;
end;

begin
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TfrmAirWatch, frmAirWatch);
    ReportMemoryLeaksOnShutdown := DebugHook <> 0;
    TestParams();
    Application.Run;
  end
  else
    Application.Terminate;

end.
