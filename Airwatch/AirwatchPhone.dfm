object frmAirWatch: TfrmAirWatch
  Left = 0
  Top = 0
  Caption = 'Airwatch Phone Number Processor'
  ClientHeight = 382
  ClientWidth = 737
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 192
    Top = 304
    Width = 35
    Height = 13
    Caption = 'Atlas #'
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 8
    Width = 695
    Height = 225
    DataSource = dsAirwatchPhone
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object btnOpen: TButton
    Tag = 1
    Left = 46
    Top = 260
    Width = 112
    Height = 25
    Caption = 'Open File'
    TabOrder = 1
    OnClick = btnOpenClick
  end
  object btnClose: TButton
    Left = 201
    Top = 260
    Width = 112
    Height = 25
    Action = Action1
    Caption = 'Close File'
    TabOrder = 2
    OnClick = btnCloseClick
  end
  object btnSave: TButton
    Left = 46
    Top = 299
    Width = 112
    Height = 25
    Action = Action1
    Caption = 'Save to Employee'
    TabOrder = 3
    OnClick = btnSaveClick
  end
  object Memo1: TMemo
    Left = 344
    Top = 246
    Width = 350
    Height = 92
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 358
    Width = 737
    Height = 24
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Width = 200
      end
      item
        Text = ' Version'
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object edtAtlas: TEdit
    Left = 240
    Top = 301
    Width = 73
    Height = 21
    TabOrder = 6
  end
  object UpdatePhoneCo: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'contactphone'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'phoneco'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'atlas_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'change_by'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'ADName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end>
    SQL.Strings = (
      'update employee'
      'set contact_phone = :contactphone,'
      'phoneco_ref = :phoneco,'
      'atlas_number = :atlas_number,'
      'changeby = :change_by'
      'where ad_username = :ADName')
    Left = 484
    Top = 132
  end
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=TestDB;Data Source=BRIAN-PC;'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 579
    Top = 80
  end
  object FileADOConnection: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.ACE.OLEDB.12.0;Data Source= C:\Trunk1\Airwatc' +
      'hnew\AirwatchDump0621.xlsx;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.ACE.OLEDB.12.0'
    Left = 479
    Top = 24
  end
  object qryAirwatchPhone: TADOQuery
    Connection = FileADOConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from [Sheet1$]')
    Left = 483
    Top = 73
  end
  object dsAirwatchPhone: TDataSource
    AutoEdit = False
    DataSet = qryAirwatchPhone
    Left = 587
    Top = 24
  end
  object OpenDialog1: TOpenDialog
    Left = 547
    Top = 186
  end
  object ActionList1: TActionList
    Left = 627
    Top = 186
    object Action1: TAction
      Caption = 'Action1'
      OnUpdate = Action1Update
    end
  end
  object qryRefPhoneCo: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select ref_id from reference '
      'where'
      'type = '#39'phoneco'#39
      'and code like :code')
    Left = 459
    Top = 185
  end
  object UpdateEmail: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'email'
        Size = -1
        Value = Null
      end
      item
        Name = 'adname'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'update users'
      'set email_address = :email'
      'where emp_id in ('
      'select u.emp_id from users u join employee e'
      'on e.emp_id = u.emp_id'
      'and e.ad_username = :adname'
      'group by u.emp_id'
      'having count(*) = 1)'
      '')
    Left = 588
    Top = 140
  end
  object qryEmpActive: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'adname'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end>
    SQL.Strings = (
      'Select active from employee where ad_username = :adname')
    Left = 384
    Top = 112
  end
end
