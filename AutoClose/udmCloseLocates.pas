unit udmCloseLocates;

interface

uses
  System.SysUtils, System.Classes, Winapi.Windows, Winapi.Messages, VCL.Forms,
  Data.DB, Data.Win.ADODB;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg : String;
    DataStream : String;
  end;


type
  TdmCloseLocates = class(TDataModule)
    adoConn: TADOConnection;
    adoCloseAllLocatesForLocator: TADOStoredProc;
    qryCountToClose: TADOQuery;
    qryEmployee: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure clearLogRecord;
    function GetAppVersionStr: string;
    procedure ProcessINI;

    { Private declarations }
  public
    { Public declarations }
    locatorName:string;
    sConnStr:string;
    APP_VER, ACLog, ExcludeClients: String;
    LogResult: TLogResults;
    procedure WriteLog;
  end;

implementation
uses inifiles, dateUtils;
{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

procedure TdmCloseLocates.DataModuleCreate(Sender: TObject);
begin
  ProcessINI;
  try
      ADOConn.Close;
      APP_VER := GetAppVersionStr;
      ADOConn.ConnectionString := sConnStr;
      ADOConn.Open();
  except on E: Exception do
    begin
        LogResult.LogType := ltError;
        LogResult.MethodName := 'DataModuleCreate';
        LogResult.DataStream := sConnStr;
        LogResult.ExcepMsg := E.Message;
        WriteLog;
    end;
  end;
end;

procedure TdmCloseLocates.ProcessINI;
var
  IniFile: TIniFile;
  spath: String;
begin
  try
    try
      spath := IncludeTrailingBackslash(ExtractFilePath(application.ExeName));
      IniFile := TIniFile.Create(spath + 'AutoClose.ini');
      sConnStr  := IniFile.ReadString('Database', 'ADOConnectionString', '');
      ExcludeClients  := IniFile.ReadString('Exclusions', 'ClientCodes', '');  //QMANTWO-799
      ACLog := IncludeTrailingBackslash(IniFile.ReadString( 'logFile','DataPath', spath+'Logs'));

    finally
      IniFile.Free;
    end;
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'Processed INI';
      LogResult.DataStream := IniFile.ToString;
      LogResult.ExcepMsg := E.Message;
      WriteLog;
    end;
  end;
end;

procedure TdmCloseLocates.WriteLog;
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;

const
  PRE_PEND = '[yyyy-mm-dd hh:nn:ss] ';
  SEP = '\';
  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  LogName := ACLog+'AutoClose '+locatorName+'_' + FormatDateTime('yyyy-mm-dd', Date)
    + FILE_EXT;

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);
  if not(FileExists(LogName)) then
    WriteLn(myFile, 'AuToClose Version: ' + APP_VER);
  if LogResult.MethodName <> '' then
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  if LogResult.ExcepMsg <> '' then
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);// +' '+ SysErrorMessage(GetLastError()));
  if LogResult.Status <> '' then
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  if LogResult.DataStream <> '' then
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);



  CloseFile(myFile);
  clearLogRecord;
end;

procedure TdmCloseLocates.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TdmCloseLocates.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

end.
