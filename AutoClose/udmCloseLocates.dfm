object dmCloseLocates: TdmCloseLocates
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 206
  Width = 264
  object adoConn: TADOConnection
    CommandTimeout = 120
    ConnectionString = 
      'Provider=SQLNCLI11;Initial Catalog=QM;Database=QM;Server=SSDS-UT' +
      'Q-QM-01-DV,1433;User ID=uqweb;Application Intent=READWRITE;'
    ConnectionTimeout = 600
    LoginPrompt = False
    Provider = 'SQLNCLI11'
    Left = 40
    Top = 32
  end
  object adoCloseAllLocatesForLocator: TADOStoredProc
    Connection = adoConn
    ProcedureName = 'close_all_locates_for_locator'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@LocatorID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Status'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Billed'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Excludes'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@OverRide'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@StatusNotToClose'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@TimeOffset'
        DataType = ftInteger
        Value = 0
      end>
    Prepared = True
    Left = 40
    Top = 96
  end
  object qryCountToClose: TADOQuery
    Connection = adoConn
    CommandTimeout = 120
    Parameters = <
      item
        Name = 'Excludes'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'LocatorID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'declare @exclsTab table ('
      #9'excludeClients varchar(16)'
      ')'
      ''
      'insert into @exclsTab'
      'select * from [dbo].StringSplit (:Excludes,'#39','#39')'
      ''
      ''
      'select count(distinct locate.locate_id) as QtyToClose'
      ' from assignment'
      ' inner join locate on locate.locate_id=assignment.locate_id'
      ' where locator_id=:LocatorID'
      '  and locate.closed=0'
      '  and assignment.active=1'
      
        '  and locate.client_code not in (select excludeClients from @exc' +
        'lsTab)')
    Left = 136
    Top = 32
  end
  object qryEmployee: TADOQuery
    Connection = adoConn
    CommandTimeout = 120
    Parameters = <
      item
        Name = 'empID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Select short_name'
      'from employee'
      'where emp_id = :empID')
    Left = 160
    Top = 96
  end
end
