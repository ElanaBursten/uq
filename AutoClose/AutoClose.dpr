program AutoClose;

{$APPTYPE CONSOLE}
 {$R '..\QMIcon.res'}
 {$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  System.SysUtils,
  System.StrUtils, //qm-733 sr
  System.Classes,
  WinAPI.ActiveX,
  Variants,

  udmCloseLocates in 'udmCloseLocates.pas' {dmCloseLocates: TDataModule};

var
  dmCloseLocates: TdmCloseLocates;
  LocatorID, Return: integer;
  closeStatus, locatorName: string;
  StatusNotToClose: string;   //qm-514  sr
  sTimeDiff:string;  //qm-733 sr
  TimeDiff:integer;  //qm-733 sr
  Billed: boolean;
  sBilled, sToClose, sOverRide: string; //qm-262
  procedure ShowHelpScreen;
  begin
    WriteLn('---------------------------------------------------------------');
    WriteLn;
    WriteLn('Closes locates for a specific locate status and employee ID');
    WriteLn;
    WriteLn('AutoClose takes four arguments');
    WriteLn('An AutoClose.ini file expected to be in the same path as this program');
    WriteLn('the locator ID as EmpId');
    WriteLn('the locator status to filter for e.g. -R');
    WriteLn('whether this locate should or should not be billed');
    WriteLn('The Billed argument can be either 0 or 1 ');
    WriteLn('or');
    WriteLn('Billed or NotBilled');
    WriteLn('Calling convention is AutoClose.exe AutoClose.ini EmpId Status Billed');
    WriteLn('AutoClose.ini 14441 -R 1');
    WriteLn('');
    WriteLn('A fifth argument can be added as a status override.  AutoClose.ini 14441 -R 1 BSCA=OS');  //qm-262
    WriteLn('The syntax is ClintCode=Status e.g.  BSCA=OS');     //qm-262
    WriteLn('There CANNOT be any spaces in the ''ClintCode=Status'' ');   //qm-262
    WriteLn('');
    WriteLn('A Sixth argument is a StatusNotToClose.  This permits a status change ');   //qm-514  sr
    WriteLn('without cl0sing the locate.');  //qm-514  sr
    WriteLn('A Seventh argument is a the hours different between the Call center and Utiliquest.');   //qm-514  sr
    WriteLn('This is a value pair of the hours difference between the call center and east coast. TO=3');  //qm-733  sr

    WriteLn('Additional parameters are passed in through the AutoClose.ini');
    WriteLn;
    WriteLn('You can view this Help by running AutoClose with no parmeters or');
    WriteLn(' passing a /? as the only argument');
    WriteLn;
    WriteLn;
    WriteLn('            Happy Closings              ');
    WriteLn;
  end;


  function HelpSwitch(HelpIfNoParams: Boolean=False): Boolean;
  begin
    if (HelpIfNoParams and (ParamCount < 4)) or (ParamStr(1) = '/?') then
      Result := True
    else
      Result := False;
  end;  //locatorName

  function GetLocatorName: string;
  begin
    try
      dmCloseLocates.qryEmployee.Parameters.ParamByName('empID').Value :=
        LocatorID;
      dmCloseLocates.qryEmployee.open;
      Result := dmCloseLocates.qryEmployee.FieldByName('short_name').asString;
      dmCloseLocates.locatorName:=Result;
    finally
      dmCloseLocates.qryEmployee.close;
    end;
  end;

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  sTimeDiff:='';
  TimeDiff:=0; //Call center East Coast
  if HelpSwitch(True) then
  begin
    ShowHelpScreen;
    Exit;
  end;
  try
    coInitialize(nil);
    dmCloseLocates := TdmCloseLocates.Create(nil);
    try
      LocatorID   := StrToInt(paramStr(2));
      closeStatus := paramStr(3);
      sBilled     := UpperCase(paramStr(4));
      sOverRide   := UpperCase(paramStr(5));
      StatusNotToClose := UpperCase(paramStr(6));


      sTimeDiff := trim(paramStr(7));   //qm-733 sr
      if sTimeDiff<>'' then                       //qm-733 sr
      TimeDiff:=StrToInt(RightStr(sTimeDiff,1));  //qm-733 sr



      If sBilled = '0' then
      Billed := False
      else If sBilled = '1' then
      Billed := True;
//      sToClose:= IntToStr(GetHowManyToClose); //QM-426
      locatorName := GetLocatorName;
//      dmCloseLocates.LogResult.logtype := ltInfo;
//      dmCloseLocates.LogResult.MethodName:= '************Process Starting*******************';
//      dmCloseLocates.LogResult.Status := 'Found '+sToClose+' to Auto Close for '+locatorName;
//      dmCloseLocates.WriteLog;

      with dmCloseLocates.adoCloseAllLocatesForLocator do
      begin
        Parameters.ParamByName('@LocatorID').Value := LocatorID;
        Parameters.ParamByName('@Status').Value :=   closeStatus;
        Parameters.ParamByName('@Billed').Value :=   Billed;
        if dmCloseLocates.ExcludeClients <> '' then
        Parameters.ParamByName('@Excludes').Value :=   dmCloseLocates.ExcludeClients
        else
        Parameters.ParamByName('@Excludes').Value := NULL;//QMANTWO-799

        if sOverRide <> '' then  //qm-262
        Parameters.ParamByName('@OverRide').Value :=   sOverRide
        else
        Parameters.ParamByName('@OverRide').Value := NULL;
        Parameters.ParamByName('@StatusNotToClose').Value := StatusNotToClose;  //qm-514  sr
        Parameters.ParamByName('@TimeOffset').Value := TimeDiff;  //qm-514  sr
      end;
      dmCloseLocates.adoCloseAllLocatesForLocator.ExecProc;
      Return := dmCloseLocates.adoCloseAllLocatesForLocator.Parameters.ParamByName('@RETURN_VALUE').Value;
    except
      on E: Exception do
      begin
        Writeln(E.ClassName, ': ', E.Message);
        dmCloseLocates.LogResult.logtype := ltError;      //AutoClose.ini 22618 M 1
        dmCloseLocates.LogResult.MethodName:= 'adoCloseAllLocatesForLocator.ExecProc';
        dmCloseLocates.LogResult.DataStream:='IniFile '+paramStr(1)+' Empid: '+paramStr(2)+' Status: '+paramStr(3)+' Billing Flag: '+paramStr(4)+'  '+paramStr(5)+' Exclude Clients: '+dmCloseLocates.ExcludeClients;
        dmCloseLocates.LogResult.ExcepMsg:=E.Message;
        dmCloseLocates.WriteLog;
      end;
    end;
    dmCloseLocates.LogResult.LogType := ltInfo;     //AutoClose.ini 14441 -R 1 BSCA=OS
    dmCloseLocates.LogResult.MethodName := 'AutoClose';
    dmCloseLocates.LogResult.Status :='Return Value SP: '+IntToStr(Return)+ ' AutoClosed on '+sToClose+' tickets for EmpID: '+IntToStr(LocatorID)+' for Status: '+closeStatus+' with Billed set to '+sBilled;
    dmCloseLocates.LogResult.DataStream:='IniFile '+paramStr(1)+' Locator to close: '+locatorName+' Empid: '+paramStr(2)+' Status: '+paramStr(3)+' Billing Flag: '+paramStr(4)+'  '+paramStr(5)+' Exclude Clients: '+dmCloseLocates.ExcludeClients;
    dmCloseLocates.WriteLog;
  finally
    dmCloseLocates.LogResult.LogType := ltInfo;
    dmCloseLocates.LogResult.MethodName := 'AutoClose';
    dmCloseLocates.LogResult.Status := '***************Process complete***************';
    dmCloseLocates.WriteLog;
    dmCloseLocates.adoConn.Close;
    FreeAndNil(dmCloseLocates);
    CoUnInitialize;
  end;

end.
