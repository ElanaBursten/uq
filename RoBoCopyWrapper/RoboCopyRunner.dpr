program RoboCopyRunner;

uses
  Vcl.Forms,
  uRoboDM in 'uRoboDM.pas' {roboDM: TDataModule},
  uRoboMain in 'uRoboMain.pas' {frmRoBoMain};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := false;
  Application.CreateForm(TfrmRoBoMain, frmRoBoMain);
  reportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Run;
end.
