object roboDM: TroboDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 278
  Width = 451
  object qryCustomerLocator: TDBISAMQuery
    AutoCalcFields = False
    DatabaseName = 'DB3'
    SessionName = 'Default'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      '  Select RoBoSource'
      '      ,RoBoDest'
      '      ,RoBoOptions'
      '      ,RoBoFiles'
      '      ,RoBoLogs'
      '  FROM customer_locator'
      '  where loc_emp_id =:EmpID')
    Params = <
      item
        DataType = ftInteger
        Name = 'EmpID'
      end>
    ReadOnly = True
    Left = 144
    Top = 22
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EmpID'
      end>
  end
  object dbIsamRoboCopy: TDBISAMDatabase
    EngineVersion = '4.44 Build 3'
    DatabaseName = 'DB3'
    SessionName = 'Default'
    Left = 39
    Top = 22
  end
end
