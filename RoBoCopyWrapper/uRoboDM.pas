unit uRoboDM;

interface

uses
  System.SysUtils, System.Classes, Data.DB, dbisamtb,
  WinApi.ActiveX, WinApi.ShlObj, WinApi.Windows, WinApi.KnownFolders, uRoboMain;

type
  TroboDM = class(TDataModule)
    qryCustomerLocator: TDBISAMQuery;
    dbIsamRoboCopy: TDBISAMDatabase;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    MyAppPath: string;

    function connectDB: Boolean;

    { Private declarations }
  public
    { Public declarations }

    procedure GetRoboCopyRequests;
  end;

var
  roboDM: TroboDM;
  AppLocal: integer;
  locEmpId: integer;
implementation

uses dialogs, forms;

{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}

// LocalAppDataFolder := GetKnownFolderPath(FOLDERID_LocalAppData);
function GetKnownFolderPath(const folder: KNOWNFOLDERID): string;
var
  Path: LPWSTR;
begin
  if SUCCEEDED(SHGetKnownFolderPath(folder, 0, 0, Path)) then
  begin
    try
      result := Path;
    finally
      CoTaskMemFree(Path);
    end;
  end
  else
    result := '';
end;
// ExecuteProcess(        APP_ROBOCOPY, sParams,        sFolder,        false,                False,         true,       );
function ExecuteProcess(const FileName, Params: string; folder: string; WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean;
  var ErrorCode: integer): Boolean;
var
  CmdLine: string;
  WorkingDirP: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  result := true;
  CmdLine := '"' + FileName + '" ' + Params;
  UniqueString(CmdLine);
  if folder = '' then
    folder := ExcludeTrailingPathDelimiter(ExtractFilePath(FileName));
  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(StartupInfo);
  if RunMinimized then
    StartupInfo.dwFlags := CREATE_NO_WINDOW;
  if folder <> '' then
    WorkingDirP := PChar(folder)
  else
    WorkingDirP := nil;
  if not CreateProcess(nil, PChar(CmdLine), nil, nil, False, 0, nil, WorkingDirP, StartupInfo, ProcessInfo) then
  begin
    result := False;
    ErrorCode := GetLastError;
    exit;
  end;
  with ProcessInfo do
  begin
    CloseHandle(hThread);
    if WaitUntilIdle then WaitForInputIdle(hProcess, INFINITE);
    if WaitUntilTerminated then
    repeat
      Application.ProcessMessages;
    until MsgWaitForMultipleObjects(1, hProcess, False, INFINITE, QS_ALLINPUT) <> WAIT_OBJECT_0 + 1;
    CloseHandle(hProcess);
  end;
end;

function TroboDM.connectDB: Boolean;
const
  MY_DATA = 'Data';
  BASE_DATA_PATH = '\Q Manager\QMServer\Data';
var
  myDataPath: string;
begin
  AppLocal := 0;

  result := False;
  dbIsamRoboCopy.connected := False;
  if AppLocal = 1 then
    myDataPath := MyAppPath + BASE_DATA_PATH // data cache
  else
    myDataPath := MyAppPath + MY_DATA; // local data

  dbIsamRoboCopy.Directory := myDataPath;
  dbIsamRoboCopy.connected := true;
  result := dbIsamRoboCopy.connected;

end;

procedure TroboDM.DataModuleCreate(Sender: TObject);
begin

  if AppLocal = 1 then
    MyAppPath := GetKnownFolderPath(FOLDERID_LocalAppData)
  else
    MyAppPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));

    if connectDB then
      GetRoboCopyRequests;

end;

procedure TroboDM.DataModuleDestroy(Sender: TObject);
begin
  dbIsamRoboCopy.Close;
end;

procedure TroboDM.GetRoboCopyRequests;
const
  APP_ROBOCOPY = 'robocopy';
  SEP = ' ';

var
  sParams: string;
  iErrorCode: integer;
  sSource, sDest, sOptions, sFiles, sLogs: string;
  sFolder : string;
begin
  sSource:=''; sDest:=''; sOptions:=''; sFiles:='';  sLogs:='';
  sFolder := ExtractFileDir(paramstr(0));
  try
    with qryCustomerLocator do
    begin
      ParamByName('EmpID').Value := locEmpId;
      Open;
      qryCustomerLocator.First;
      while not eof do
      begin
        sSource := fieldByName('RoBoSource').AsString;
        sDest := fieldByName('RoBoDest').AsString;
        sOptions := fieldByName('RoBoOptions').AsString;
        sFiles := fieldByName('RoBoFiles').AsString;
        sLogs :=  fieldByName('RoBoLogs').AsString;
        if sLogs<>'' then sLogs:= '/log:'+sLogs;

        sParams := sSource + SEP + sDest + SEP + sOptions + SEP + sFiles + SEP + sLogs;
                                              //WaitUntilTerm  WaitUntitlIdle  RunMin
        If ExecuteProcess(APP_ROBOCOPY, sParams, sFolder, true, false, true, iErrorCode)=true then
        Next
        else
        showmessage(IntToStr(iErrorCode));

      end; // while
    end; // with
  finally
    qryCustomerLocator.Close;
  end;

end;

end.
