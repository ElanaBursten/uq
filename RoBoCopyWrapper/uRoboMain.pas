unit uRoboMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs;

type
  TfrmRoBoMain = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRoBoMain: TfrmRoBoMain;

implementation
uses uRoboDM;
{$R *.dfm}

procedure TfrmRoBoMain.FormCreate(Sender: TObject);
begin
  if ParamCount < 1 then
    application.Terminate
    else
      uRoBoDM.locEmpId := StrToInt(paramstr(1));
  roboDM:= TroboDM.Create(self);
  application.Terminate;
end;

end.
