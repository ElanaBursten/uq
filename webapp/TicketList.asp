<%@ Language=VBScript %>
<!-- #INCLUDE file="includes/PageHeader.asp" -->
<%
' Page Name:	TicketList.asp
' Author:		Steve Brooks
' Date:			3/24/2002
' Description:	Ticket Listing Page
' 
' Revisions:
' mm/dd/yyyy	FML - Description
'Dimension base variables
Dim BaseFolder, BaseProgram
BaseFolder = ""
BaseProgram = ""
%>
<!-- #INCLUDE file="includes/dbConn.asp" -->
<!-- #INCLUDE file="includes/ValidUser.asp" -->
<!-- #INCLUDE file="includes/Replace.asp" -->
<!-- #INCLUDE file="includes/Format.asp" -->
<%
'Load variables passed from querystring or hidden form value
Dim TicketNumber, Address, BeginDate, EndDate, UserId
TicketNumber = Request("TicketNumber")
Address = Request("Address")
BeginDate = Request("BeginDate")
EndDate = Request("EndDate")
UserId = Session("UserId")

'Dimension variables
Dim MenuPage, NextPage, PrevPage, RtnPage
MenuPage = BaseFolder & "Menu.asp"
NextPage = "TicketDisplay.asp?Mode=D&TicketId="
PrevPage = "TicketSearch.asp?TicketNumber=" & TicketNumber & _
           "&Address=" & Address & "&BeginDate=" & BeginDate & _
           "&EndDate=" & EndDate
RtnPage = Server.URLEncode("TicketList.asp?TicketNumber=" & TicketNumber & _
           "&Address=" & Address & "&BeginDate=" & BeginDate & _
           "&EndDate=" & EndDate)

'Dimension form variables
Dim Bgcolor, ii, NoRecs, arrForm, RowClass, AddressLine
NoRecs = False

Call LoadRecords()

%>
<!-- #INCLUDE file="StandardHTML.asp" -->
<%
ocnConn.Close
Set ocnConn = Nothing
'-------------------------------------------------------------------------
Sub PageHTML
%>
<br>
<table border="0" cellpadding="2" cellspacing="1" width="100%" align="center">
	<tr>
		<td width="25%">
		<a href="<%=MenuPage%>">Menu</a>&nbsp;&nbsp;
		<a href="<%=PrevPage%>">Previous</a>&nbsp;&nbsp;
		<td class="PageHeading">Ticket Listing</td>
		<td width="25%">&nbsp;</td>
	</tr>
</table>
<br>
<table border="1" cellpadding="2" cellspacing="1" width="100%">
	<tr>
		<td class="ColHeading">Ticket#</td>
		<td class="ColHeading">Type</td>
		<td class="ColHeading">Status</td>
		<td class="ColHeading">Received</td>
		<td class="ColHeading">Due</td>
		<td class="ColHeading">Address</td>
		<td class="ColHeading">City, State</td>
 	</tr>	
<%	If NoRecs Then %>
	<tr>
		<td align="center" colspan="7"><br>
		<em>There are no Ticket(s) matching the search criteria.</em></td>
	</tr>
<%	Else 
		For ii = 0 to UBound(arrForm,2) 
			If ii Mod 2 = 1 Then RowClass = "EvenRow" Else RowClass = "OddRow" 
			AddressLine = arrForm(6,ii)
			If SetBlank(arrForm(7,ii)) <> "" Then
				AddressLine = AddressLine & " - " & arrForm(7,ii)
			End If
			AddressLine = AddressLine & " " & arrForm(8,ii) %>
			<tr class="<%=RowClass%>">
				<td align="center" nowrap rowspan="2">
				<a href="<%=NextPage%><%=arrForm(0,ii)%>&RtnPage=<%=RtnPage%>">
				<%= arrForm(1,ii)%></a></td>
				<td><%= arrForm(2,ii) %>&nbsp;</td>
				<td align="center"><%= arrForm(3,ii) %></td>
				<td align="center"><%= FormatDate(arrForm(4,ii),1) %></td>
				<td align="center"><%= FormatDate(arrForm(5,ii),1) %></td>
				<td><%= AddressLine %></td>
				<td><%= arrForm(9,ii) %>, <%= arrForm(10,ii) %></td>
			</tr>
			<tr class="<%=RowClass%>">
				<td colspan="6"><b>Work:</b> <%= arrForm(11,ii) %></td>
			</tr>
<%		Next
	End If %>
</table>
<%
End Sub
'-------------------------------------------------------------------------
Sub LoadRecords
'Load records first time

	Dim qmEng, repConn
	Set qmEng = CreateObject("QMDBEng.QMDBEngine")
	qmEng.GetReportingConnection "ReportEngineCGI.ini", repConn
	'ocnConn
	
	Dim ocmdProc, RtnCde, Param, orsData

	Set ocmdProc = Server.CreateObject("ADODB.Command")
	Set ocmdProc.ActiveConnection = repConn

	ocmdProc.CommandText = "SEL_TicketList" 
	ocmdProc.CommandType = adCmdStoredProc
	
	ocmdProc.Parameters("@UserId")			= UserId
	ocmdProc.Parameters("@TicketNumber")	= SetNull(TicketNumber)
	ocmdProc.Parameters("@Address")			= SetNull(Address)
	ocmdProc.Parameters("@BeginDate")		= SetNull(BeginDate)
	ocmdProc.Parameters("@EndDate")			= SetNull(EndDate)

	ocmdProc.CommandTimeout = 180
	
	Set orsData = ocmdProc.Execute
				
	If orsData.EOF Then
		NoRecs = True
	Else
		arrForm = orsData.GetRows
		If UBound(arrForm,2) = 0 Then
			Set orsData = Nothing
			Set ocnConn = Nothing
			Response.Redirect NextPage & arrForm(0,0) & "&RtnPage=" & _
				 Server.URLEncode(PrevPage)
		End If
	End If

	orsData.Close
	Set orsData = Nothing
	
End Sub
'-------------------------------------------------------------------------
%>