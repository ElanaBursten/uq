<%@ Language=VBScript %>
<!-- #INCLUDE file="includes/PageHeader.asp" -->
<%
' Page Name:	TicketDisplay.asp

' Heavily modified June 2003 by Kyle Cordes

Dim BaseFolder, BaseProgram
BaseFolder = ""
BaseProgram = ""
%>
<!-- #INCLUDE file="includes/dbConn.asp" -->
<!-- #INCLUDE file="includes/ValidUser.asp" -->
<!-- #INCLUDE file="includes/Replace.asp" -->
<%

'Load variables passed from querystring or hidden form value
Dim Mode, TicketId, RtnPage
Mode = Request("Mode")
TicketId = Request("TicketId")
RtnPage = Request("RtnPage")

'Dimension variables
Dim MenuPage, NextPage, PrevPage
MenuPage = BaseFolder & "Menu.asp"
PrevPage = RtnPage
NextPage = Request("SCRIPT_NAME") + "?TicketId=" & TicketId & "&Mode="
RtnPage = Server.URLEncode(RtnPage)

Dim TicketData, ClientData, ResponseData, NotesData, HoursData, VersionData
Dim AssignmentData, LocateStatusData, BillingData, UsersData

Dim NoRecs, ii
Dim CanSeeNotes, CanSeeHistory

Call LoadData
%>
<!-- #INCLUDE file="StandardHTML.asp" -->
<%

ocnConn.Close


'-------------------------------------------------------------------------
Sub PageHTML
%>
<br>
<table border="0" cellpadding="2" cellspacing="1" width="100%" align="center">
	<tr>
		<td width="25%">
		<a href="<%=MenuPage%>">Menu</a>&nbsp;&nbsp;
		<a href="<%=PrevPage%>">Previous</a>&nbsp;&nbsp;
		<td class="PageHeading">Ticket Detail</td>
		<td width="25%">&nbsp;</td>
	</tr>
</table>
<br>
<table border="0" cellpadding="2" cellspacing="1">
	<tr>
		<td><b>Ticket #:</b></td>
		<td><%= TicketData("ticket_number") %></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><%= TicketData("kind") %></td>
	</tr>
</table>
<br>

<table border="1" cellpadding="0" cellspacing="0" width="100%" ID="Table2">
<%
Call ShowTabs
%><tr><td><%
If Mode = "D" Then Call DetailHTML
If Mode = "I" Then Call ImageHTML

If Mode = "H" and CanSeeHistory Then Call HistoryHTML
If Mode = "N" and CanSeeNotes Then Call NotesHTML
%></td></tr>
</table>
<%
End Sub 
'-------------------------------------------------------------------------
Sub LoadData
	' Get all the data from one SP call, many result sets

	Dim GetTicketDataSP, RtnCde, Param, Result
	
	Set GetTicketDataSP = Server.CreateObject("ADODB.Command")
	Set GetTicketDataSP.ActiveConnection = ocnConn

	GetTicketDataSP.CommandText = "get_ticket_all" 
	GetTicketDataSP.CommandType = adCmdStoredProc
	
	ocnConn.CursorLocation = adUseClient
	' GetTicketDataSP.CursorType = adOpenStatic
	'GetTicketDataSP.
	
	GetTicketDataSP.Parameters("@TicketId") = TicketId
	GetTicketDataSP.Parameters("@UserId")	 = Session("UserId")
	
	Set Result = GetTicketDataSP.Execute
	Set TicketData = Result.Clone

	Set Result = Result.NextRecordset
	Set ClientData = Result.Clone
	
	Set Result = Result.NextRecordset
	Set ResponseData = Result.Clone

	Set Result = Result.NextRecordset
	Set NotesData = Result.Clone

	Set Result = Result.NextRecordset
	Set HoursData = Result.Clone

	Set Result = Result.NextRecordset
	Set VersionData = Result.Clone

	Set Result = Result.NextRecordset
	Set AssignmentData = Result.Clone

	Set Result = Result.NextRecordset
	Set LocateStatusData = Result.Clone

	Set Result = Result.NextRecordset
	Set BillingData = Result.Clone

	Set Result = Result.NextRecordset
	Set UsersData = Result.Clone

	CanSeeNotes = UsersData("can_view_notes")
	CanSeeHistory = UsersData("can_view_history")

	' Get some data in an array, the prexisting code wants it there.
	If ClientData.EOF Then
		NoRecs = True
	End If
End Sub
'-------------------------------------------------------------------------

sub ShowTab(CurrentTabCode, TabCode, Label)
	Dim BColor
	if CurrentTabCode = TabCode then
		BColor = "#add8e6"
	else
		BColor = "#eeeeee"
	end if
	
	%>
	<td bgcolor="<%= BColor %>" width="100" align="center"><a href="<%=NextPage%><%= TabCode %>&RtnPage=<%=RtnPage%>"><%= Label %></a></td>
	<%
end sub

Sub ShowTabs
%>
<tr><td bgcolor="#cccccc">
<table border="0" cellpadding="2" cellspacing="2" >
	<tr>
<%
ShowTab Mode, "D", "Detail"
ShowTab Mode, "I", "Image"

If CanSeeHistory Then
	ShowTab Mode, "H", "History"
End If

If CanSeeNotes Then
	ShowTab Mode, "N", "Notes"
End If
%>
</table>
</td></tr>
<%
end sub

Sub DetailHTML
%>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td class="Prompt">Due Date:</td>
		<td><%= TicketData("due_date") %></td>
 	</tr>
 	<tr>
 		<td class="Prompt" nowrap>Work to be Done:</td>
 		<td colspan="5"><%= TicketData("work_type") %></td>
 	</tr>
 	<tr>
 		<td class="Prompt">&nbsp;</td>
 		<td colspan="5"><%= TicketData("work_description") %></td>
 	</tr>
 	<tr>
 		<td class="Prompt" colspan="6">&nbsp;</td>
 	</tr>
 	<tr>
 		<td class="Prompt">Work Address:</td>
 		<td>
 			<%= TicketData("work_address_number") %>&nbsp;
<%			If Trim(TicketData("work_address_number_2")) <> "" Then
				Response.Write "- " & TicketData("work_address_number_2") & "&nbsp;"
			End If %>				
 			<%= TicketData("work_address_street") %>
 		
 		</td>
 		<td class="Prompt">Cross Street:</td>
 		<td colspan="5"><%= TicketData("work_cross") %></td>
 	</tr>
 	<tr>
 		<td class="Prompt">&nbsp;</td>
 		<td>
 			<%= TicketData("work_city") %>&nbsp;&nbsp;
 			<%= TicketData("work_state") %>
 		</td>
 		<td class="Prompt">County:</td>
 		<td colspan="5"><%= TicketData("work_county") %></td>
 	</tr>
 	<tr>
 		<td class="Prompt" colspan="6">&nbsp;</td>
 	</tr>
 	<tr>
 		<td class="Prompt" nowrap>Work Done For:</td>
 		<td><%= TicketData("company") %></td>
 		<td class="Prompt">Work Date/Time:</td>
 		<td colspan="5" nowrap><%= TicketData("work_date") %></td>
 	</tr>
 	<tr>
 		<td class="Prompt">Contractor:</td>
 		<td><%= TicketData("con_name") %></td>
 		<td class="Prompt">Ticket Received:</td>
 		<td colspan="5"><%= TicketData("transmit_date") %></td>
 	</tr>
 	<tr>
 		<td class="Prompt">Contact:</td>
 		<td>
 			<%= TicketData("caller_contact") %>&nbsp;
 		    <%= TicketData("caller_phone") %></td>
 		<td class="Prompt">Lattitude:</td>
 		<td><%= TicketData("work_lat") %></td>
 	</tr>
 	<tr>
 		<td class="Prompt">Alt Contact:</td>
 		<td>
 			<%= TicketData("caller_altcontact") %>&nbsp;
 		    <%= TicketData("caller_altphone") %></td>
 		<td class="Prompt">Longitude:</td>
 		<td><%= TicketData("work_long") %></td>
 	</tr>
 	<tr>
 		<td class="Prompt" colspan="2">&nbsp;</td>
 		<td class="Prompt">Map Page:</td>
 		<td><%= TicketData("map_page") %></td>
 	</tr>
 	<tr>
 		<td class="Prompt">Remarks:</td>
 		<td colspan="7"><%= TicketData("work_remarks") %></td>
 	</tr>
</table>
</td></tr>

<tr><td bgcolor="#cccccc">
<table border="0" cellpadding="2" cellspacing="1" width="60%">
	<tr>
		<td class="Prompt">Client</td>
		<td class="Prompt">Status</td>
		<td class="Prompt">Last Changed</td>
	</tr>
<%	If NoRecs Then %>	
	<tr>
		<td bgcolor="#ffffff">&nbsp;</td>
		<td bgcolor="#ffffff">&nbsp;</td>
		<td bgcolor="#ffffff">&nbsp;</td>
	</tr>
<%	Else
		while Not ClientData.EOF  %>
	<tr>
		<td bgcolor="#ffffff"><%= ClientData("client_code") %></td>
		<td bgcolor="#ffffff"><%= ClientData("status") %></td>
		<td bgcolor="#ffffff"><%= ClientData("closed_date") %></td>
	</tr>
<%		ClientData.MoveNext
		wend
	End If %>				
</table>
<%
End Sub
'-------------------------------------------------------------------------
Sub ImageHTML
%>
<pre><%= TicketData("image") %></pre>
<%
End Sub

'-------------------------------------------------------------------------
Sub NotesHTML
%>
<table border="0" cellpadding="2" cellspacing="1" ID="Table1" width="100%">
	<tr>
		<td class="Prompt" width="20%">Date</td>
		<td class="Prompt" width="80%">Note</td>
	</tr>
	<% while not NotesData.EOF  %>
	<tr>
		<td bgcolor="#ffffff"><%= NotesData("entry_date") %></td>
		<td bgcolor="#ffffff"><%= NotesData("note") %></td>
	</tr>
	<% 
	NotesData.MoveNext
	Wend %>
</table>
<%
End Sub

'-------------------------------------------------------------------------
Sub HistoryHTML
%>
<table border="0" cellpadding="2" cellspacing="1" ID="Table4" width="100%">
	<h4>Locate Status History:</h4>
	<tr>
		<td class="Prompt" width="20%">Client</td>
		<td class="Prompt" width="20%">Status</td>
		<td class="Prompt" width="20%">Date/Time</td>
		<td class="Prompt" width="20%">Locator</td>
	</tr>
	<%
	While not LocateStatusData.EOF
	%>
	<tr>
		<td bgcolor="#ffffff"><%= LocateStatusData("client_code") %></td>
		<td bgcolor="#ffffff"><%= LocateStatusData("status") %></td>
		<td bgcolor="#ffffff"><%= LocateStatusData("status_date") %></td>
		<td bgcolor="#ffffff"><%= LocateStatusData("short_name") %></td>
	</tr>
	<% 
	LocateStatusData.MoveNext
	Wend %>
</table>

<table border="0" cellpadding="2" cellspacing="1" ID="Table3" width="100%">
	<h4>Responses:</h4>
	<tr>
		<td class="Prompt" width="15%">Client</td>
		<td class="Prompt" width="25%">Date/Time</td>
		<td class="Prompt" width="15%">Response</td>
		<td class="Prompt" width="10%">Success</td>
		<td class="Prompt" width="20%">Reply</td>
	</tr>
	<%
	While not ResponseData.EOF
	%>
	<tr>
		<td bgcolor="#ffffff"><%= ResponseData("client_code") %></td>
		<td bgcolor="#ffffff"><%= ResponseData("response_date") %></td>
		<td bgcolor="#ffffff"><%= ResponseData("response_sent") %></td>
		<td bgcolor="#ffffff"><%= ResponseData("success") %></td>
		<td bgcolor="#ffffff"><%= ResponseData("reply") %></td>
	</tr>
	<% 
	ResponseData.MoveNext
	Wend %>
</table>

<table border="0" cellpadding="2" cellspacing="1" ID="Table5" width="100%">
	<h4>Ticket Versions:</h4>
	<tr>
		<td class="Prompt" width="20%">Transmit Date</td>
		<td class="Prompt" width="20%">Arrival Date</td>
		<td class="Prompt" width="20%">Process Date</td>
		<td class="Prompt" width="20%">Type</td>
		<td class="Prompt" width="10%">Revision</td>
	</tr>
	<%
	While not VersionData.EOF
	%>
	<tr>
		<td bgcolor="#ffffff"><%= VersionData("transmit_date") %></td>
		<td bgcolor="#ffffff"><%= VersionData("arrival_date") %></td>
		<td bgcolor="#ffffff"><%= VersionData("processed_date") %></td>
		<td bgcolor="#ffffff"><%= VersionData("ticket_type") %></td>
		<td bgcolor="#ffffff"><%= VersionData("ticket_revision") %></td>
	</tr>
	<% 
	VersionData.MoveNext
	Wend %>
</table>


<%
End Sub
%>
