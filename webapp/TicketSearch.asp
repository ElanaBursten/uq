<%@ Language=VBScript %>
<!-- #INCLUDE file="includes/PageHeader.asp" -->
<%
' Page Name:	TicketSearch.asp
' Author:		Steve Brooks
' Date:			3/24/2002
' Description:	Ticket Selection Criteria Page
' 
' Revisions:
' mm/dd/yyyy	FML - Description
%>
<!-- #INCLUDE file="includes/dbConn.asp" -->
<!-- #INCLUDE file="includes/clsFormInput.asp" -->
<%
'Dimension base variables
Dim BaseFolder, PrevPage
BaseFolder = ""
PrevPage = BaseFolder & "Menu.asp"

Dim TicketNumber, Address, BeginDate, EndDate
Dim NumberErr, AddressErr, BeginErr, EndErr

TicketNumber = Trim(Request("TicketNumber"))
Address = Trim(Request("Address"))
BeginDate = Trim(Request("BeginDate"))
EndDate = Trim(Request("EndDate"))

If Request.Form("AddressSrch") = "Search" Then
	If TicketNumber = "" and  Address = "" Then
		NumberErr = True
	End If
	If BeginDate = "" Or Not IsDate(BeginDate) then
		BeginErr = True
	End If
	if EndDate = "" or Not IsDate(EndDate) Then
		EndErr = True
	End If
	If Not NumberErr and Not BeginErr And Not EndErr Then
		Response.Redirect "TicketList.asp?TicketNumber=" & TicketNumber & "&Address=" & Address &_
			                  "&BeginDate=" & BeginDate & "&EndDate=" & EndDate
	End If
End If

Dim objInput
Set objInput = New clsFormInput
%>
<!-- #INCLUDE file="StandardHTML.asp" -->
<%
Set objInput = Nothing
Set ocnConn = Nothing
'-------------------------------------------------------------------------
Sub PageHTML
%>
<form method="post" action="TicketSearch.asp" name="form1" id="form1">
<br><p>
<table border="0" width="100%">
	<tr>
		<td width="25%">
		<a href="<%=BaseFolder%>Menu.asp">Menu</a>&nbsp;&nbsp;
		</td>
		<td class="PageHeading">Ticket Search</td>
		<td width="25%">&nbsp;</td>
	</tr>
</table>
</p>
<%	If NumberErr Then %>
		<font color="red">Please enter a Ticket Number or Address!</font>
<%	End If %>		
<%	If BeginErr Then %>
		<font color="red"><br>The Begin Date is not a valid date. Please re-enter!</font>
<%	End If %>		
<%	If EndErr Then %>
		<font color="red"><br>The End Date is not a valid date. Please re-enter!</font>
<%	End If %>		

<p>
<table border="1" width="100%" cellpadding="0" cellspacing="1">
<tr><td>
<table border="0" width="100%" cellpadding="0" cellspacing="2" bgcolor="#eeeeee">
	<tr>
		<td colspan="3"><b>Enter either Ticket Number or Address to search.</b></td>
	</tr>
	<tr>
		<td width="15%">Ticket Number:</td>
		<td width="35%"><% Call TextBox ("TicketNumber", TicketNumber, 20, 30, 1) %></td>
	</tr>
	<tr>
		<td width="15%">Partial Address:</td>
		<td colspan="2"><% Call TextBox ("Address", Address, 40, 40, 2) %></td>
	</tr>
	<tr>
		<td>Begin Date:</td>
		<td width="35%"><% Call TextBox ("BeginDate", BeginDate, 15, 15, 3) %> (required)</td>
		<td rowspan="2"><% Call SubmitButton ("AddressSrch", "Search", 0) %></td>
	</tr>
	<tr>
		<td>End Date:</td>
		<td><% Call TextBox ("EndDate", EndDate, 15, 15, 4) %> (required)</td>
	</tr>
</table>
</td></tr>
</table>
</form>
<%
End Sub
'-------------------------------------------------------------------------
Sub TextBox (InputName, Value, Size, MaxLen, TabIdx)
' Local sub to write text boxes

	With objInput
		.Initialize
		.InputName = InputName
		.Value = Value
		.Size = Size
		.MaxLength = MaxLen
		.TabIndex = TabIdx
		.SelectValue = True
	End With
	Response.Write objInput.TextBox
	 
End Sub
'-------------------------------------------------------------------------
Sub SubmitButton (InputName, Value, TabIdx)
' Local sub to write submit button

	With objInput
		.Initialize
		.InputName = InputName
		.Value = Value
		.TabIndex = TabIdx
	End With
	Response.Write objInput.SubmitButton
	 
End Sub
'-------------------------------------------------------------------------
%>
