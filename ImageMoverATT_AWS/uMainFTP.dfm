object frmMainFTP: TfrmMainFTP
  Left = 0
  Top = 0
  Caption = 'Image Mover'
  ClientHeight = 596
  ClientWidth = 928
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -18
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 22
  object Label1: TLabel
    Left = 415
    Top = 16
    Width = 79
    Height = 22
    Caption = 'Start Date'
  end
  object Label2: TLabel
    Left = 422
    Top = 47
    Width = 72
    Height = 22
    Caption = 'End Date'
  end
  object Label3: TLabel
    Left = 480
    Top = 495
    Width = 108
    Height = 22
    Caption = 'Total Records'
  end
  object Label4: TLabel
    Left = 442
    Top = 531
    Width = 146
    Height = 22
    Caption = 'Records processed'
  end
  object Label5: TLabel
    Left = 24
    Top = 8
    Width = 82
    Height = 22
    Caption = 'Time Start'
  end
  object btnRun: TButton
    Left = 111
    Top = 485
    Width = 107
    Height = 50
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Run'
    TabOrder = 0
    OnClick = btnRunClick
  end
  object Memo1: TMemo
    Left = 3
    Top = 112
    Width = 554
    Height = 341
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object cbCloseDone: TCheckBox
    Left = 753
    Top = 488
    Width = 164
    Height = 38
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Close when done'
    Checked = True
    State = cbChecked
    TabOrder = 2
  end
  object btnAbort: TButton
    Left = 283
    Top = 485
    Width = 97
    Height = 46
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Abort'
    TabOrder = 3
    OnClick = btnAbortClick
  end
  object cbSendTSV: TCheckBox
    Left = 18
    Top = 64
    Width = 127
    Height = 38
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Send TSV'
    TabOrder = 4
    OnClick = cbSendTSVClick
  end
  object cbSendXML: TCheckBox
    Left = 177
    Top = 65
    Width = 120
    Height = 37
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Send XML'
    TabOrder = 5
    OnClick = cbSendXMLClick
  end
  object cbSendImages: TCheckBox
    Left = 329
    Top = 65
    Width = 152
    Height = 37
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Send Images'
    TabOrder = 6
    OnClick = cbSendImagesClick
  end
  object settingsList: TCheckListBox
    Left = 576
    Top = 112
    Width = 351
    Height = 341
    ItemHeight = 22
    TabOrder = 7
  end
  object dpEnd: TcxDateEdit
    Left = 500
    Top = 44
    Properties.DateButtons = [btnClear, btnNow, btnToday]
    Properties.Kind = ckDateTime
    Properties.OnChange = dpEndPropertiesChange
    TabOrder = 8
    Width = 229
  end
  object dpStart: TcxDateEdit
    Left = 500
    Top = 13
    Properties.DateButtons = [btnClear, btnNow, btnToday]
    Properties.Kind = ckDateTime
    Properties.OnChange = dpStartPropertiesChange
    TabOrder = 9
    Width = 229
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 580
    Width = 928
    Height = 16
    Align = alBottom
    TabOrder = 10
  end
  object edtRecordCount: TEdit
    Left = 594
    Top = 492
    Width = 121
    Height = 30
    NumbersOnly = True
    TabOrder = 11
    Text = '0'
  end
  object edtProcessed: TEdit
    Left = 594
    Top = 528
    Width = 121
    Height = 30
    NumbersOnly = True
    TabOrder = 12
    Text = '0'
  end
  object edtTimeStart: TEdit
    Left = 24
    Top = 32
    Width = 209
    Height = 30
    TabOrder = 13
  end
  object MainMenu1: TMainMenu
    Left = 880
    Top = 8
    object Settings1: TMenuItem
      Caption = 'Settings'
      object InitExportSettings1: TMenuItem
        Caption = 'Init Export Settings'
        OnClick = InitExportSettings1Click
      end
      object ReadSettings1: TMenuItem
        Caption = 'Read Settings'
        OnClick = ReadSettings1Click
      end
      object SavetoINI1: TMenuItem
        Caption = 'Save to INI'
        OnClick = SavetoINI1Click
      end
    end
  end
end
