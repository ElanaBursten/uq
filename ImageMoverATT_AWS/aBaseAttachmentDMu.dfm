inherited imageDM: TimageDM
  OldCreateOrder = True
  Height = 384
  Width = 540
  object qryAPI_storage_credentials: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [id]'
      '      ,[storage_type]'
      '      ,[enc_access_key]'
      '      ,[enc_secret_key]'
      '      ,[unc_date_created]'
      '      ,[modified_date]'
      '      ,[unc_date_expired]'
      '      ,[api_storage_constants_id]'
      '      ,[description]'
      '  FROM [dbo].[api_storage_credentials]'
      '  where id = 1')
    Left = 304
    Top = 284
  end
  object qryAPI_storage_constants: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [id]'
      '      ,[host]'
      '      ,[rest_url]'
      '      ,[service]'
      '      ,[bucket]'
      '      ,[bucket_folder]'
      '      ,[region]'
      '      ,[algorithm]'
      '      ,[signed_headers]'
      '      ,[content_type]'
      '      ,[accepted_values]'
      '      ,[environment]'
      '      ,[description]'
      '      ,[unc_date_modified]'
      '      ,[modified_date]'
      '  FROM [dbo].[api_storage_constants]'
      '  where id = 1')
    Left = 96
    Top = 284
  end
  object spClientAttachments: TADOStoredProc
    Connection = ADOConn
    CursorType = ctStatic
    CommandTimeout = 3000
    ProcedureName = 'RPT_ClientAttachments'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CallCenters'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end
      item
        Name = '@ClientIDs'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end
      item
        Name = '@AttachUploadStartDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@AttachUploadEndDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ForeignTypes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@IncludeFileTypes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end
      item
        Name = '@ExcludeFileNames'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end>
    Prepared = True
    Left = 416
    Top = 80
  end
  object qryStructure: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      '  declare @ClientAttachment table ('
      '    attachment_id int not null primary key,'
      '    ticket_number varchar(20) not null,'
      '    serial_number varchar(40) null,'
      '    filename varchar(100) not null,'
      '    orig_filename varchar(100) not null,'
      '    upload_date datetime not null)'
      '  '
      
        '  select ticket_number, serial_number, filename, orig_filename, ' +
        'upload_date, attachment_id'
      '  from @ClientAttachment')
    Left = 256
    Top = 176
  end
  object spClientAttachmentsInActive: TADOStoredProc
    Connection = ADOConn
    CommandTimeout = 3000
    ProcedureName = 'RPT_ClientAttachmentsInactive'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CallCenters'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end
      item
        Name = '@ClientIDs'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end
      item
        Name = '@AttachModifyStartDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@AttachModifyEndDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ForeignTypes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@IncludeFileTypes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end
      item
        Name = '@ExcludeFileNames'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end>
    Left = 416
    Top = 144
  end
  object spDataSet: TADODataSet
    Connection = ADOConn
    CommandTimeout = 60
    Parameters = <>
    Left = 416
    Top = 216
  end
  object ADOConn: TADOConnection
    CommandTimeout = 900
    ConnectionTimeout = 3000
    DefaultDatabase = 'QM'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 388
    Top = 8
  end
end
