unit miniCVUtils;
{this is NOT the same as the same named file for QM.
This module has been modified to support EPR.}
interface

uses Classes, StrUtils, uMainFTP;

function ReplaceCVs(const CVFileName, sTicketID, IniName: string): boolean;

procedure GetFilesFromCV(const CVFile, sTicketID: string);

implementation

uses Windows, SysUtils, Variants, XSuperObject, OdMiscUtils,
  WinHttp_TLB, IniFiles, QMConst, BaseAttachmentDMu;

var
  CVServer: string;
  CertLocation: string;

const
  MANIFEST =  'manifest';

function FileIsCV(const FileName: string): Boolean;
begin
  Result := SameText(ExtractFileExt(FileName), '.@CV');
end;

function GetCVFileList(const CVFile: string): ISuperObject;
var
  ListRequest: IWinHttpRequest;
begin
  ListRequest := CoWinHttpRequest.Create;

  try
    try
      ListRequest.open('POST', CVServer, False);
      ListRequest.SetRequestHeader('Content-Type', 'binary/octet-stream');
    except
      on E: Exception do begin
        E.Message := 'Cannot connect to CV web service at: ' + CVServer + ': ' + E.Message;
        raise;
      end;
    end;

    //Establish Credentials
    try
      ListRequest.SetClientCertificate(CertLocation);
    except
      on E: Exception do begin
        E.Message := 'Invalid Cert or Cert Location for: ' + CertLocation + ': ' + E.Message;
        raise;
      end;
    end;

    //Post the file to dycom server
    ListRequest.Send(FileToOleVariant(CVFile));
    Assert(ListRequest.Status = HttpOK, Format('%s responded with code: %s (%s) '+
      'for @CV File: %s.', [CVServer, IntToStr(ListRequest.Status), ListRequest.StatusText, CVFile]));

    Result := SO(ListRequest.ResponseText);
  except
    on E: Exception do begin
      E.Message := 'Unable to retrieve CV content list from server: '+ E.Message;
      frmMainFTP.Memo1.Lines.Add( 'Unable to retrieve CV content list from server: '+ E.Message);
      raise;
    end;
  end;
end;

procedure GetContentFileFromCV(const CVFile, FileUrl, FileName, OutFileName: string);
var
  FileRequest: IWinHttpRequest;
begin
  //Needs less error handling as the cv service will have already been contacted
  //once
  try
    FileRequest := CoWinHttpRequest.Create;
    FileRequest.Open('GET', FileUrl, False);
    FileRequest.SetRequestHeader('Content-Type', 'binary/octet-stream');
    FileRequest.SetClientCertificate(CertLocation);
    FileRequest.Send(EmptyParam);
    Assert(FileRequest.Status = HttpOK, 'Server responded with '+
      IntToStr(FileRequest.Status) + '(' + FileRequest.StatusText + ')');
    OleVariantToFile(FileRequest.ResponseBody, OutFileName);
  except
    on E: Exception do begin
      E.Message := 'Failed to retrieve: ' + FileName + ' from CV: '+
        ExtractFileName(CVFile) + ': ' + E.Message;
      raise;
    end;
  end;
end;

procedure GetFilesFromCV(const CVFile, sTicketID: string);
var
  JsonData: ISuperObject;
  FileName: string;
  OutFileName: string;
  I, J: Integer;
begin
  Assert(FileIsCV(CVFile), Format('Non-CV file %s cannot be sent to the CV web service', [CVFile]));
  Assert(CanWriteToDirectory(ExtractFilePath(CVFile)), 'Unable to write to download folder: ' + ExtractFilePath(CVFile));

    JsonData := GetCVFileList(CVFile);
    for I := 0 to JsonData.AsArray.Length - 1 do begin
      //Extract the image file name from the url
        FileName := Copy(JsonData.AsArray.S[I], LastDelimiter('/', JsonData.AsArray.S[i])+1,
        Length(JsonData.AsArray.S[I])-1);
        if not((pos(MANIFEST, filename)) > 0) then continue;

       J := pos('-',FileName);
       Delete(FileName, 1, J);

    OutFileName := BuildFileName(ExtractFilePath(CVFile), FileName);
    GetContentFileFromCV(CVFile, JsonData.AsArray.S[I], FileName, OutFileName);
    end;

    DeleteFile (CVFile);
end;

procedure GetIniValuesForCV(IniName: string);
const
  IniSection = 'CVService';
  CertFromIni = 'Cert';
  ServerFromIni = 'Server';

var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(IniName);
  try
    CertLocation := Ini.ReadString(IniSection, CertFromIni, '');
    CVServer := Ini.ReadString(IniSection, ServerFromIni, '');
  finally
    FreeAndNil(Ini);
  end;
end;

function ReplaceCVs(const CVFileName, sTicketID, IniName: string): boolean;
begin
  Result := false;
  GetIniValuesForCV(IniName);
  GetFilesFromCV(CVFileName, sTicketID);
  Result := true;
end;

end.
