unit aBaseAttachmentDMu;

interface

uses
  System.SysUtils, System.Classes, BaseAttachmentDMu, Data.DB, Data.Win.ADODB;

type
  TimageDM = class(TBaseAttachment)
    qryAPI_storage_credentials: TADOQuery;
    qryAPI_storage_constants: TADOQuery;
    spClientAttachments: TADOStoredProc;
    qryStructure: TADOQuery;
    spClientAttachmentsInActive: TADOStoredProc;
    spDataSet: TADODataSet;
    ADOConn: TADOConnection;
  private

    { Private declarations }
  public
    { Public declarations }
    function connectToDB(connStr: string): Boolean;
    function GetAWSConstants: Boolean;
    function GetAWSCredentials: Boolean;
  end;

var
  imageDM: TimageDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uMainFTP, Vcl.Forms, OdUqInternet, dialogs;

{$R *.dfm}

function TimageDM.connectToDB(connStr: string): Boolean;
begin

  try
    ADOConn.close;
    try
      ADOConn.ConnectionString := connStr;
      ADOConn.open();
    except
      on E: Exception do
      begin
        frmMainFTP.LogResult.LogType := ltError;
        frmMainFTP.LogResult.MethodName := 'connectToDB';
        frmMainFTP.LogResult.DataStream := ADOConn.ConnectionString;
        frmMainFTP.LogResult.ExcepMsg := 'Connection failed ' + E.Message;
        ADOConn.ConnectionString;
        frmMainFTP.WriteLog(frmMainFTP.LogResult);
      end;
    end;
  finally
    If ADOConn.Connected then
    begin

      frmMainFTP.LogResult.LogType := ltInfo;
      frmMainFTP.LogResult.MethodName := 'connectToDB';
      frmMainFTP.LogResult.DataStream := 'ADOConn.ConnectionString: ' +
        ADOConn.ConnectionString;
      frmMainFTP.WriteLog(frmMainFTP.LogResult);
      result := true;
    end
    else
      result := false;
  end;

end;

function TimageDM.GetAWSConstants: Boolean; // QMANTWO-555 sr
begin
  if qryAPI_storage_constants.Active then
    qryAPI_storage_constants.close;
  qryAPI_storage_constants.open;
  if qryAPI_storage_constants.RecordCount > 0 then
  begin
    myAttachmentData.Constants.Host := qryAPI_storage_constants.FieldByName
      ('host').AsString;
    myAttachmentData.Constants.RestURL := qryAPI_storage_constants.FieldByName
      ('rest_url').AsString;
    myAttachmentData.Constants.Service := qryAPI_storage_constants.FieldByName
      ('service').AsString;
    myAttachmentData.Constants.StagingBucket :=
      qryAPI_storage_constants.FieldByName('bucket').AsString;
    myAttachmentData.Constants.StagingFolder :=
      qryAPI_storage_constants.FieldByName('bucket_folder').AsString;
    myAttachmentData.Constants.Region := qryAPI_storage_constants.FieldByName
      ('region').AsString;
    myAttachmentData.Constants.Algorithm := qryAPI_storage_constants.FieldByName
      ('algorithm').AsString;
    myAttachmentData.Constants.SignedHeaders :=
      qryAPI_storage_constants.FieldByName('signed_headers').AsString;
    myAttachmentData.Constants.ContentType :=
      qryAPI_storage_constants.FieldByName('content_type').AsString;
    myAttachmentData.Constants.AcceptedValues :=
      qryAPI_storage_constants.FieldByName('accepted_values').AsString;
    if myAttachmentData.Constants.Host <> '' then
      result := true
    else
      result := false
  end
  else
    result := false;
end;

function TimageDM.GetAWSCredentials: Boolean; // QMANTWO-555 sr
begin
  myAttachmentData.AWSKey := '';
  myAttachmentData.AWSSecret := '';
  if qryAPI_storage_credentials.Active then
    qryAPI_storage_credentials.close;

  qryAPI_storage_credentials.open;
  if qryAPI_storage_credentials.RecordCount > 0 then
  begin
    myAttachmentData.AWSKey := qryAPI_storage_credentials.FieldByName
      ('enc_access_key').AsWideString;
    myAttachmentData.AWSSecret := qryAPI_storage_credentials.FieldByName
      ('enc_secret_key').AsWideString;
    if (myAttachmentData.AWSKey <> '') and (myAttachmentData.AWSSecret <> '')
    then
      result := true
    else
      result := false;
  end
  else
  begin
    myAttachmentData.AWSKey := '';
    myAttachmentData.AWSSecret := '';
    result := false;
  end;
end;

end.
