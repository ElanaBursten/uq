unit uMainFTP;   //  AT&T

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,  OdUqInternet,  ShellAPI,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB,
  Winapi.SHFolder, Winapi.SHLObj, Winapi.KnownFolders, Winapi.ActiveX,
  System.IOUtils, System.Types,   Vcl.Grids, Vcl.ValEdit,
  Vcl.ComCtrls, aBaseAttachmentDMu, Vcl.CheckLst,
  xqCbListBoxHelper, Vcl.Menus, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxDBEdit, dxCore, cxDateUtils;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[90];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;

type
  TfrmMainFTP = class(TForm)
    btnRun: TButton;
    Memo1: TMemo;
    cbCloseDone: TCheckBox;
    btnAbort: TButton;
    cbSendTSV: TCheckBox;
    cbSendXML: TCheckBox;cbSendImages: TCheckBox;
    settingsList: TCheckListBox;
    MainMenu1: TMainMenu;
    Settings1: TMenuItem;
    InitExportSettings1: TMenuItem;
    ReadSettings1: TMenuItem;
    SavetoINI1: TMenuItem;
    Label1: TLabel;
    Label2: TLabel;
    dpEnd: TcxDateEdit;
    dpStart: TcxDateEdit;
    ProgressBar1: TProgressBar;
    Label3: TLabel;
    edtRecordCount: TEdit;
    edtProcessed: TEdit;
    Label4: TLabel;
    edtTimeStart: TEdit;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ADOConnAfterConnect(Sender: TObject);
    procedure btnAbortClick(Sender: TObject);
    procedure cbSendTSVClick(Sender: TObject);
    procedure cbSendXMLClick(Sender: TObject);
    procedure cbSendImagesClick(Sender: TObject);
    procedure InitExportSettings1Click(Sender: TObject);
    procedure ReadSettings1Click(Sender: TObject);
    procedure SavetoINI1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure dpStartPropertiesChange(Sender: TObject);
    procedure dpEndPropertiesChange(Sender: TObject);
  private
    fAttachUploadStartDate: TDateTime;
    fAttachUploadEndDate: TDateTime;
    fForeignTypes: string;
    fIncludeFileTypes: string;
    fExcludeFileNames: string;
    fPathForFTP: string;
    fWinScpPath: string;
    fLocalFileDrive: string;
    fLocalRoot: string;
    fConnString: string;
    fActiveDoFile: string;
    fLastAttID: string;
      attFile: TextFile;
    function ReadINI: Boolean;
    function connectToDB: Boolean;
    procedure ExportTSV;
    procedure ExportToXML;
    procedure clearLogRecord;

    function getAppVer: String;

    procedure RunProcess(Sender: TObject);
    function downloadAttachmentsForTicket(attID: integer; FileName, origFilename,tempTktPath:string): integer;

    procedure InitData;
    function GetTicketList:boolean;
    procedure copyAttachments;

    procedure ProcessCV(tempTktPath, sTicket, sLeadIn: string);
    procedure RenameFiles(tempTktPath, sLeadIn: string);


    procedure primeCbListBox;
    procedure ProcessParams;
    procedure CreateDOfile;
    { Private declarations }
  public
    { Public declarations }
    lastAttachmentID: integer;
    LogResult: TLogResults;
    LocalAppDataFolder: string;
    fActiveCallCenters: string;
    fActiveCallClients: string;
    fProcessDate: TDate;

    fDBType: string;
    fFtpAttLog: string;
    canRun: Boolean;
    fremFtpDir: string;
    fsendTSV: Boolean;
    fsendXML: Boolean;
    fsendImages: Boolean;
    IniName: string;
    imageDM: TimageDM;
    property LastAttID: string read fLastAttID write fLastAttID;
    property ConnString: string read fConnString write fConnString;
    property WinScpPath: string read fWinScpPath write fWinScpPath;
    property PathForFTP: string read fPathForFTP write fPathForFTP;
    property sendImages: Boolean read fsendImages write fsendImages;
    property sendTSV: Boolean read fsendTSV write fsendTSV;
    property sendXML: Boolean read fsendXML write fsendXML;
    property appVer: String read getAppVer;
    property DBType: string read fDBType write fDBType;

    property FtpAttLog: string read fFtpAttLog write fFtpAttLog;
    property remFtpDir: string read fremFtpDir write fremFtpDir;

    property ActiveCallCenters: string read fActiveCallCenters
      write fActiveCallCenters;
    property ActiveCallClients: string read fActiveCallClients
      write fActiveCallClients;
    property ActiveDoFile: string read fActiveDoFile
      write fActiveDoFile;
    property AttachUploadStartDate :TDateTime read fAttachUploadStartDate write fAttachUploadStartDate;
    property AttachUploadEndDate   :TDateTime read fAttachUploadEndDate write fAttachUploadEndDate;
    property ForeignTypes          :string read fForeignTypes write fForeignTypes;
    property IncludeFileTypes      :string read fIncludeFileTypes write fIncludeFileTypes;
    property ExcludeFileNames      :string read fExcludeFileNames write fExcludeFileNames;
    property LocalFileDrive        :string read fLocalFileDrive  write fLocalFileDrive;
    property LocalRoot             :string read fLocalRoot write fLocalRoot;
    property ProcessDate: TDate read fProcessDate write fProcessDate;
    procedure WriteLog(LogResult: TLogResults);
  end;

function GetKnownFolderPath(const folder: KNOWNFOLDERID): string;
function GetAppVersionStr: string;

//const
//  BASE_PATH_UTL_ATT = '\\dynutil.com\utq\Qmanager\ftp\california\ATT\';

var
  frmMainFTP: TfrmMainFTP;
  ABORT_PROCESS: Boolean;

implementation

uses DateUtils, inifiles, miniCVUtils;
{$R *.dfm}

procedure TfrmMainFTP.ADOConnAfterConnect(Sender: TObject);
begin
  lastAttachmentID := 0;
  if DBType = 'dbQM' then
    memo1.lines.add('Connected to: QM on QMCluster')
  else
  memo1.lines.add('Connection failed');

  LogResult.LogType := ltInfo;
  LogResult.Status := 'Connect: '+BoolToStr(imageDM.ADOConn.Connected);
  LogResult.MethodName := 'ADOConnAfterConnect';
  WriteLog(LogResult);

  ABORT_PROCESS := false;
end;

procedure TfrmMainFTP.btnAbortClick(Sender: TObject);
begin
  ABORT_PROCESS := true;
  cbCloseDone.Checked:=true;
end;

procedure TfrmMainFTP.btnRunClick(Sender: TObject);
var
  aPath : string;
const
  myLastAttID = 'LastAttID.txt';
begin
  aPath := PathForFTP + IncludeTrailingBackslash(FormatDateTime('yyyy-mm-dd',AttachUploadStartDate));
  if not(FileExists(myLastAttID)) then
  begin
    AssignFile(attFile, myLastAttID);
    Rewrite(attFile);
    CloseFile(attFile);
    if DirectoryExists(aPath) then
      TDirectory.Delete((aPath), true);
  end
  else
   begin
     AssignFile(attFile, myLastAttID);
     reset(attFile);
     readLn(attFile, fLastAttID);
     CloseFile(attFile);
     if LastAttID = '' then
       DeleteFile(myLastAttID);

   end;


  If TDirectory.Exists(aPath) then
  begin
    if (MessageDlg('Do you want to Delete folder '+aPath+' with Att ID of '+LastAttID
      +#10#13+'All files in it will be deleted and you will be starting over.'
      +#10#13+'Choosing Yes will restart from the beginning'
      +#10#13+'No will start from last Attachment ID.', mtWarning, [mbNo, mbYes], 0) = mrYes) then
    begin
      TDirectory.Delete((aPath), true);
      ForceDirectories(aPath);
      DeleteFile(myLastAttID);
      LogResult.LogType := ltInfo;
      LogResult.MethodName := 'Cleaned up temp folder';
      WriteLog(LogResult);

    end;
  end
  else
  begin
    DeleteFile(myLastAttID);
  end;
  IniName := IncludeTrailingBackSlash(ExtractFileDir(application.ExeName)) +
  'eprCertusViewConn.ini';
  btnRun.Enabled := false;
  edtTimeStart.text := DateTimeToStr(Now);
  RunProcess(Sender);
end;

procedure TfrmMainFTP.RunProcess(Sender: TObject);
begin
  CreateDOfile;

  btnRun.Enabled := false;
  canRun := true;
  Memo1.lines.add('Run GetTicketList');

  if GetTicketList then
  begin
    Memo1.lines.add('preparing copyAttachments');
    copyAttachments;
    Memo1.lines.add('Run copyAttachments');
  end
  else
  begin
    Memo1.lines.add('get tickets failed');
    LogResult.LogType := ltError;
    LogResult.Status := 'get tickets failed to return any tickets';
    LogResult.MethodName := 'get tickets failed';
    WriteLog(LogResult);
  end;
end;

function TfrmMainFTP.GetTicketList:boolean;
var
  cnt: integer;
begin
  cnt := 0;
  try
    try
      imageDM.spClientAttachments.close;
      self.Cursor := crSQLWait;
      with imageDM.spClientAttachments do
      begin
        Parameters.ParamByName('@CallCenters').Value := fActiveCallCenters;
        Parameters.ParamByName('@ClientIDs').Value := ActiveCallClients;
        Parameters.ParamByName('@AttachUploadStartDate').Value :=
          DateTimeToStr(AttachUploadStartDate);
        Parameters.ParamByName('@AttachUploadEndDate').Value :=
          DateTimeToStr(AttachUploadEndDate);
        Parameters.ParamByName('@ForeignTypes').Value := ForeignTypes;
        Parameters.ParamByName('@IncludeFileTypes').Value := IncludeFileTypes;
        Parameters.ParamByName('@ExcludeFileNames').Value := ExcludeFileNames;
      end;
      Memo1.lines.add('spClientAttachments initiated: ' + TimeToStr(Now));
      imageDM.spClientAttachments.open;
      Memo1.lines.add('spClientAttachments opened: ' + TimeToStr(Now));
      if not(imageDM.spClientAttachments.IsEmpty) then
      begin
        Memo1.lines.add('assigning to recordset : ' + TimeToStr(Now));
        imageDM.spDataset.Recordset := imageDM.spClientAttachments.Recordset;

        cnt := imageDM.spDataset.RecordCount;

        edtRecordCount.text := IntToStr(cnt);
        ProgressBar1.Max := 1000;

        Memo1.lines.add('opening spDataSet: ' + TimeToStr(Now));
        imageDM.spDataset.open;
        Memo1.lines.add('imageDM.spDataSet.opened: ' + TimeToStr(Now));
        Memo1.lines.add(InttoStr(cnt) +
          ' records added to spDataSet.Recordset');

      end
      else
      begin
        Memo1.lines.add('Stored proc returned no records');
      end;
    finally
      imageDM.spClientAttachments.close;
      result := (cnt>0);
    end;
  except
    on E: Exception do
    begin
      Memo1.lines.add('Ticket list query failed with: ' + E.Message);
      LogResult.LogType := ltError;
      LogResult.MethodName := 'GetTicketList';
      LogResult.ExcepMsg := 'Ticket list query failed with: ' + E.Message;
      WriteLog(LogResult);
      Memo1.lines.add(LogResult.ExcepMsg);
    end;

  end;
  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'GetTicketList';
  Memo1.lines.add('Running query spClientAttachments.sql with parameters:');
  Memo1.lines.add('SyncDateFrom: ' + DateTimeToStr(AttachUploadStartDate));
  Memo1.lines.add('SyncDateTo: ' + DateTimeToStr(AttachUploadEndDate));
  Memo1.lines.add('ClientCodes: ' + ActiveCallClients);
  Memo1.lines.add('callCenters: ' + ActiveCallCenters);
  Memo1.lines.add(' ');
  LogResult.DataStream := (Memo1.text);

  btnRun.Cursor := crDefault;
end;


procedure TfrmMainFTP.copyAttachments;
var
  ticketNo: string;
  serialNumber: string;
  FileName: string;
  origFilename: string;
  uploadDate: TDateTime;
  attachmentID: integer;
  i: integer;
  tempTktPath: string;
  LeadIn: string;

  hasError: Boolean;
  SearchOptions: TLocateOptions;
begin
  i := 0;
  attachmentID := 0;

  hasError := false;
  SearchOptions := [loPartialKey];
  try
    try
      if sendImages then
      begin
        with imageDM do
        begin
          spDataset.open;
          if LastAttID <> '' then
          begin
            attachmentID := StrToInt(LastAttID);
            spDataset.Locate('attachment_id', attachmentID, SearchOptions);
            memo1.lines.add('restarting from attachment id: '+LastAttID);
            LogResult.LogType := ltInfo;
            LogResult.Status := 'restarting from attachment id: '+LastAttID;
            LogResult.MethodName := 'AttmentID Restart';
            WriteLog(LogResult);
          end
          else
            spDataset.first;
          while not spDataset.eof do
          begin
            application.ProcessMessages;
            if ABORT_PROCESS then
            begin
              canRun := false;
              break;
            end;
            ticketNo := spDataset.FieldByName('ticket_number').AsString;
            serialNumber := spDataset.FieldByName('serial_number').AsString;
            FileName := spDataset.FieldByName('filename').AsString;
            origFilename := spDataset.FieldByName('orig_filename').AsString;
            uploadDate := spDataset.FieldByName('upload_date').AsDateTime;
            attachmentID := spDataset.FieldByName('attachment_id').AsInteger;
            lastAttachmentID := attachmentID;
            tempTktPath := PathForFTP + FormatDateTime('yyyy-mm-dd',
              AttachUploadStartDate);
            Memo1.lines.add('calling downloadAttachmentsForTicket');

            downloadAttachmentsForTicket(attachmentID, FileName, origFilename,
              tempTktPath);
            Rewrite(attFile);
            WriteLn(attFile, InttoStr(lastAttachmentID));
            CloseFile(attFile);

            Memo1.lines.add('attachmentID: ' + InttoStr(attachmentID));
            Memo1.lines.add('filename: ' + FileName);
            Memo1.lines.add('origFilename: ' + origFilename);
            Memo1.lines.add('tempTktPath: ' + tempTktPath);
            spDataset.Next;
            inc(i);
            edtProcessed.text := InttoStr(i);
            LogResult.LogType := ltInfo;
            LogResult.Status := 'Processed attachments for ticket ' + ticketNo;
            LogResult.MethodName := 'ExportTSV';
            WriteLog(LogResult);
            ProgressBar1.StepIt;
          end; // while not qryTicketList.eof do
        end; // with imageDM do
      end; // if sendImages then
    except
      on E: Exception do
      begin
        Memo1.lines.add(E.Message);
        dec(i);
        hasError := true;
      end;

    end;
  finally
    if sendTSV then
      ExportTSV;
    if sendXML then
      ExportToXML;

    ChDir(WinScpPath);
    Memo1.lines.add('Change Dir to WinScpPath: ' + WinScpPath);
    LogResult.LogType := ltInfo;
    LogResult.Status := 'Running WinSCP.bat ';
    LogResult.MethodName := 'ShellExecute';
    WriteLog(LogResult);
    if I  >0 then
    begin
      ShellExecute(0, 'open', 'WinSCP.bat', pchar(ActiveDoFile), nil, SW_HIDE) ;
      if not hasError then deletefile('LastAttID.txt');
    end
    else
    begin
      TDirectory.Delete((tempTktPath), true);
      Memo1.lines.add('No attachments to FTP');
    end;

    imageDM.spDataset.close;
    Memo1.lines.add('Completed transferring images for ' + ActiveCallCenters +
      ' client ' + ActiveCallClients);

    btnRun.Enabled := true;
    canRun := false;


    if cbCloseDone.Checked then
      application.Terminate;
    application.ProcessMessages;
  end;
end;


procedure TfrmMainFTP.cbSendImagesClick(Sender: TObject);
begin
  sendImages := cbSendImages.Checked;
end;

procedure TfrmMainFTP.cbSendTSVClick(Sender: TObject);
begin
  sendTSV := cbSendTSV.Checked;
end;

procedure TfrmMainFTP.cbSendXMLClick(Sender: TObject);
begin
  sendXML := cbSendXML.Checked;
end;

procedure TfrmMainFTP.ProcessCV(tempTktPath, sTicket, sLeadIn: string);
var
  searchResult: TSearchRec;
  oldJpegName, newJpegName: string;
begin
  Memo1.Lines.Add('Looking for @CV to crack in ' + tempTktPath);
  setCurrentDir(tempTktPath);

  if findFirst('*.@CV', faAnyFile, searchResult) = 0 then
  begin
    repeat
      Memo1.Lines.Add(searchResult.Name + ' to crack');

      If ReplaceCVs(tempTktPath + '\' + searchResult.Name, sTicket, IniName)
      then
      begin
        LogResult.LogType := ltInfo;
        LogResult.Status := 'Cracked ' + searchResult.Name;
        LogResult.MethodName := 'ProcessCV';
      end
      else
      begin
        LogResult.LogType := ltError;
        LogResult.Status := 'Failed to Crack ' + searchResult.Name;
        LogResult.MethodName := 'ProcessCV';
      end;
      WriteLog(LogResult);
    until FindNext(searchResult) <> 0;
    FindClose(searchResult);
  end;
end;

procedure TfrmMainFTP.RenameFiles(tempTktPath, sLeadIn: string);
var
  searchResult: TStringDynArray; // TSearchRec;
  oldJpegName, newJpegName: string;
  i: integer;
  ftpFile: string;
begin
  setCurrentDir(tempTktPath);

  searchResult := TDirectory.GetFiles(tempTktPath, '*.JPG');
  for i := Low(searchResult) to High(searchResult) do
  begin
    oldJpegName := ExtractFileName(searchResult[i]);
    newJpegName := sLeadIn + oldJpegName;
    RenameFile(oldJpegName, newJpegName);
    ftpFile := ExtractFileName(newJpegName);
  end;
  searchResult := nil;
end;

procedure TfrmMainFTP.SavetoINI1Click(Sender: TObject);
begin
  settingsList.SaveToINI;
end;

procedure TfrmMainFTP.FormActivate(Sender: TObject);
begin
  settingsList.ReadFromINI;
end;

procedure TfrmMainFTP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ABORT_PROCESS := true;
  Action := caFree;
end;

procedure TfrmMainFTP.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if imageDM.ADOConn.Connected then
    imageDM.ADOConn.close;
  CanClose := cbCloseDone.Checked;
end;

procedure TfrmMainFTP.FormCreate(Sender: TObject);
begin
  fLastAttID := '';
  memo1.Lines.Add('FormCreate');
  self.Caption := self.Caption + '       ' + appVer;
  imageDM := TimageDM.Create(self);
  memo1.Lines.Add('Created ImageDM');
  ReadINI;
  memo1.Lines.Add('ReadINI');
  ProcessParams;
  memo1.Lines.Add('ProcessParams');
  InitData;
  memo1.Lines.Add('InitData');
  if imageDM.GetAWSConstants then
  memo1.Lines.Add('GetAWSConstants succeeded')
  else
  memo1.Lines.Add('GetAWSConstants failed');

  if imageDM.GetAWSCredentials then
  memo1.Lines.Add('GetAWSCredentials succeeded')
  else
  memo1.Lines.Add('GetAWSCredentials failed');

  imageDM.GetAWSCredentials;

//{$IFNDEF DEBUG}
//  if ParamCount > 1 then
//  begin
//    RunProcess(nil);
//  end
//  else
//  begin
//    show;
//  end;
//{$ENDIF}
end;

procedure TfrmMainFTP.ProcessParams;
var
  i: integer;
begin
  if ParamCount > 0 then
  begin
    for i := 1 to ParamCount do
    begin
      case i of
        1:
          AttachUploadStartDate := StrToDateTime(paramstr(i));
        // 2: AttachUploadEndDate   := StrToDateTime(paramstr(i));
      end;
    end;
  end
  else
  begin
    AttachUploadStartDate := today - 7;
  end;
  AttachUploadEndDate := AttachUploadStartDate + 1;
  dpStart.Date := AttachUploadStartDate;
  dpEnd.Date := AttachUploadEndDate;
  dpStart.Refresh;
  dpEnd.Refresh;
end;

procedure TfrmMainFTP.primeCbListBox;
begin
    with imageDM.qryStructure do
    begin
      close;
    end;
   SettingsList.InitializeFromDataset(imageDM.qryStructure);
end;

function TfrmMainFTP.getAppVer: String;
begin
  result := GetAppVersionStr;
end;

function TfrmMainFTP.downloadAttachmentsForTicket(attID: integer; FileName, origFilename,tempTktPath:string): integer;
begin
  myAttachmentData.AttachmentID  := attID;
  myAttachmentData.LocalFilename := fileName;
  myAttachmentData.OrigFilename  := origFilename;
  ForceDirectories(tempTktPath);
  try
      imageDM.DownloadAttachmentToFileAWS(myAttachmentData, tempTktPath+'\'+myAttachmentData.LocalFilename);
  except on E: Exception do
  //
  end;
end;

procedure TfrmMainFTP.dpEndPropertiesChange(Sender: TObject);
begin
  AttachUploadEndDate := dpEnd.Date;
end;

procedure TfrmMainFTP.dpStartPropertiesChange(Sender: TObject);
begin
  AttachUploadStartDate := dpStart.Date;
  dpEnd.Date :=  dpStart.Date+1;
end;

procedure TfrmMainFTP.InitData;
var
  i: integer; // C:\Users\svc.utl.qmanager\AppData\Local\Temp
begin
  If not connectToDB then
  begin
    Memo1.Lines.Add('Failed to connect to database.  exiting');
    application.Terminate;
  end;

end;

procedure TfrmMainFTP.InitExportSettings1Click(Sender: TObject);
begin
  primeCbListBox;
end;

function TfrmMainFTP.connectToDB: Boolean;
begin
 if imageDM.connectToDB(connString) then
  Memo1.lines.Add('connectToDB result: succeeded')
  else
  Memo1.lines.Add('connectToDB result: failed');

result := imageDM.ADOConn.Connected;
end;

function TfrmMainFTP.ReadINI: Boolean;
var
  IniFile: TIniFile;
  Path: String;
begin
  try
    Path := ExtractFilePath(application.ExeName);
    IniFile := TIniFile.Create(Path + 'imageMoverATT.ini');

    DBType := IniFile.ReadString('remoteFTP', 'database', 'dbQM');
    FtpAttLog := IniFile.ReadString('remoteFTP', 'ftpAttLog', 'E:\QM\Logs');
    remFtpDir := IniFile.ReadString('remoteFTP', 'remDir', 'LocateCompletions');
    sendXML := IniFile.ReadBool('remoteFTP', 'sendXML', true);
    sendTSV := IniFile.ReadBool('remoteFTP', 'sendTSV', true);
    sendImages := IniFile.ReadBool('remoteFTP', 'sendImages', true);
    PathForFTP := IniFile.ReadString('remoteFTP', 'PathForFTP', '\\dynutil.com\utq\Qmanager\ftp\california\ATT\');
    WinScpPath := IniFile.ReadString('remoteFTP', 'WinScpPath', 'C:\Trunk1\ImageMoverATT_AWS\winscp');
    LocalRoot  := IniFile.ReadString('remoteFTP', 'LocalRoot ', 'utq');
    ConnString := IniFile.ReadString('remoteFTP', 'ConnString', 'Provider=SQLOLEDB.1;Password=ihjidojo3;Persist Security Info=True;User ID=QMParserUTL;Initial Catalog=QM;Data Source=DYATL-PENTDB02');
    ActiveCallCenters  := IniFile.ReadString('remoteFTP', 'callCenters', 'SCA1,NCA1');
    ActiveCallClients  := IniFile.ReadString('remoteFTP', 'clients', '4084,4097');
    ForeignTypes      := IniFile.ReadString('remoteFTP', 'ForeignTypes', '1');
    IncludeFileTypes  := IniFile.ReadString('remoteFTP', 'IncludeFileTypes', '.jpg');
    ExcludeFileNames  := IniFile.ReadString('remoteFTP', 'ExcludeFileNames', 'Screenshot*');


    PathForFTP := IncludeTrailingBackslash(PathForFTP);
    WinScpPath := IncludeTrailingBackslash(WinScpPath);
    LocalFileDrive := ExtractFileDrive(PathForFTP);
    cbSendTSV.Checked := sendTSV;
    cbSendXML.Checked := sendXML;
    cbSendImages.Checked := sendImages;
  finally
    Memo1.lines.add('IniFile '+IniFile.ToString);
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ReadINI';
    LogResult.Status := 'IniFile processed';
    WriteLog(LogResult);
    FreeAndNil(IniFile);
  end;
end;

procedure TfrmMainFTP.ReadSettings1Click(Sender: TObject);
begin
  settingsList.ReadFromINI;
end;

procedure TfrmMainFTP.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;

const
  PRE_PEND = '[yyyy-mm-dd hh:nn:ss] ';

  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  if ActiveCallCenters <> '' then
    LogName := FtpAttLog + '\' + ActiveCallCenters + '_' + ActiveCallClients + '_'
      + FormatDateTime('yyyy-mm-dd', Date) + FILE_EXT
  else
    LogName := FtpAttLog + '\' + 'StartUpLog' + '_' +
      FormatDateTime('yyyy-mm-dd', Date) + FILE_EXT;
  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);
  if not(FileExists(LogName)) then
    begin
    WriteLn(myFile, 'Image Att Version: ' + appVer);
      memo1.lines.add('Image Att Version: ' + appVer);
  end;

  if LogResult.MethodName <> '' then
    begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
      memo1.lines.add(Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
    begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
      memo1.lines.add(Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
    begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
      memo1.lines.add(Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
    begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
    memo1.lines.add(Leader + 'Data : ' + LogResult.DataStream);
  end;

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmMainFTP.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := paramStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function GetKnownFolderPath(const folder: KNOWNFOLDERID): string;
var
  Path: LPWSTR;
begin
  if SUCCEEDED(SHGetKnownFolderPath(folder, 0, 0, Path)) then
  begin
    try
      result := Path;
    finally
      CoTaskMemFree(Path);
    end;
  end
  else
    result := '';
end;

procedure TfrmMainFTP.ExportToXML;
var  // calls Helper class
  xmlFile: TextFile;
  xmlStr, xmlName, sDate: string;

  ftpPath: string;
  sClient, sCallCenter: string;
const
  EXT = '.xml';
  TSV_REPORT = '%s-Closed Tickets (%s) %s %s';
  ROOT = 'Tickets';
  ITEM_TAG = 'Ticket';
begin
  try
    xmlStr := settingsList.SaveToXML(ROOT,ITEM_TAG,imageDM.spDataSet);

    xmlName := 'ClientAttachments';
    forcedirectories(PathForFTP + IncludeTrailingBackslash(FormatDateTime('yyyy-mm-dd',AttachUploadStartDate)));
    ftpPath :=  PathForFTP + IncludeTrailingBackslash(FormatDateTime('yyyy-mm-dd',AttachUploadStartDate)) + xmlName + EXT;
    AssignFile(xmlFile, ftpPath);
    Rewrite(xmlFile);
    WriteLn(xmlFile, xmlStr);
    CloseFile(xmlFile);

  finally
    LogResult.LogType := ltInfo;
    LogResult.Status := 'Sent ' + xmlName +  ' to Client FTP server';
    LogResult.MethodName := 'ExportXML';
    WriteLog(LogResult);
  end;
end;

procedure TfrmMainFTP.ExportTSV;
var // consider passing log into Helper Class
  tsvName, sDate: string;
  ftpPath: string;
  sClient, sCallCenter: string;
const
  EXT = '.tsv';
  TSV_REPORT = '%s-Closed Tickets (%s) %s %s';
begin
  try
    try
      tsvName := 'ClientAttachments';
      forcedirectories(PathForFTP + IncludeTrailingBackslash(FormatDateTime('yyyy-mm-dd',AttachUploadStartDate)));
      ftpPath := PathForFTP + IncludeTrailingBackslash(FormatDateTime('yyyy-mm-dd',AttachUploadStartDate))+ tsvName + EXT;   //add date
      LogResult.LogType := ltInfo;
      LogResult.Status := 'SaveToTSV ';
      LogResult.MethodName := 'settingsList.SaveToTSV(imageDM.spDataSet, ftpPath)';
      WriteLog(LogResult);
      settingsList.SaveToTSV(imageDM.spDataSet, ftpPath);
    finally
      LogResult.LogType := ltInfo;
      LogResult.Status := 'Sent ' + tsvName + ' to Client FTP server';
      LogResult.MethodName := 'ExportTSV';
      WriteLog(LogResult);
    end;
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'ExportTSV';
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
    end;
  end;
end;

procedure TfrmMainFTP.CreateDOfile;
const
  LINE1 = 'option batch on';
  LINE2 = 'option confirm off';
  LINE3 = 'open sftp://UTQ-EXT-CA-ATT:mW61Qgg39TKrSYMv@files.mcview.com:22';
  LINE4 = 'lcd ';
  LINE5 = 'cd /';
  LINE6 = 'put -delete ';
  LINE7 = 'exit';

  DO_FILE = 'do';
var
  doLIst : TStringList;
begin
  try
    ActiveDoFile:= DO_FILE+'_'+FormatDateTime('yyyy-mm-dd',AttachUploadStartDate);
    doLIst := TStringList.Create;
    doLIst.Add(LINE1);
    doLIst.Add(LINE2);
    doLIst.Add(LINE3);
    doLIst.Add(LINE4+LocalFileDrive);
    doLIst.Add(LINE5);
    doLIst.Add(LINE6+PathForFTP +FormatDateTime('yyyy-mm-dd',AttachUploadStartDate));
    doLIst.Add(LINE7);
    doLIst.SaveToFile(WinScpPath+ActiveDoFile);
    memo1.Lines.add('-----------------do File start-------------------------');
    memo1.Lines.add(doLIst.Text);
    memo1.Lines.add('-----------------do File end-------------------------');
  finally
    freeandnil(doLIst);
    LogResult.LogType := ltInfo;
    LogResult.Status := 'Path: '+WinScpPath;
    LogResult.MethodName := 'Create Do file';
      WriteLog(LogResult);
    memo1.Lines.add( 'WinScpPath Path: '+WinScpPath);

  end;



end;

end.
