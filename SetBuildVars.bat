rem These environment vars should be injected by Jenkins before building QM.
rem SetBuildVars.bat should only be used when running BuildQM.bat outside of Jenkins.

rem Customize these settings as needed to match your build environment

rem Location of the project source
rem set WORKSPACE=D:\Projects\uq-qm
set WORKSPACE=C:\TRUNK1

rem Set vars needed by the Delphi compiler
::call "%ProgramFiles(x86)%\Embarcadero\Studio\19.0\bin\rsvars.bat"
set BDS2007=%ProgramFiles(x86)%\CodeGear\RAD Studio\5.0
set BDSCOMMONDIR2007=%PUBLIC%\Documents\RAD Studio\5.0
rem MRH 20150811 NEED TO CHANGE TO THIS = set BDSCOMMONDIR2007=%PUBLIC%\Public Documents\RAD Studio\5.0

rem Locations of 3rd party components
rem SLH Changed this one set DBISAM2007DIR=%Program Files (x86)%\DBISAM 4 VCL-STD\RAD Studio 2007 (Delphi)\code

set DBISAM2007DIR=C:\Delphi Tools\DBISAM 4 VCL-STD\RAD Studio 2007 (Delphi)\code
::set DBISAMDIR=C:\Program Files (x86)\DBISAM 4 VCL-STD\RAD Studio 10S (Delphi Win32)\code

set JCL2007DIRS=C:\Delphi Tools\jcl-2.4.1.4571\source\include;C:\Delphi Tools\jcl-2.4.1.4571\lib\d11
::set JCLDIRS=C:\DelphiXE10\JCL\source\include;C:\DelphiXE10\JCL\lib\d23\win32

set HTMLCOMP=C:\Delphi Tools\Installs\HTMLComp\Source;C:\Delphi Tools\Installs\HTMLComp\Source\editor;C:\Delphi Tools\Installs\HTMLComp\Source\hunspell;C:\Delphi Tools\Installs\HTMLComp\Source\Reports;C:\Delphi Tools\Installs\HTMLComp\Source\sql

rem set DEVEXDIR=D:\Projects\Components\DevExQM.VCL
rem set DEVEXDIR=C:\Delphi Tools\DevEx14211
::set ExpressBars=C:\Delphi Tools\DevEx14211\ExpressBars\Sources
::set ExpressCommonLibrary=C:\Delphi Tools\DevEx14211\ExpressCommon Library\Sources
::set ExpressCoreLibrary=C:\Delphi Tools\DevEx14211\ExpressCore Library\Sources
::set ExpressDataController=C:\Delphi Tools\DevEx14211\ExpressDataController\Sources
::set ExpressDBTreeSuite=C:\Delphi Tools\DevEx14211\ExpressDBTree Suite\Sources
::set ExpressDockingLibrary=C:\Delphi Tools\DevEx14211\ExpressDocking Library\Sources
::set ExpressEditorsLibrary=C:\Delphi Tools\DevEx14211\ExpressEditors Library\Sources
::set ExpressExportLibrary=C:\Delphi Tools\DevEx14211\ExpressExport Library\Sources
::set ExpressFlowChart=C:\Delphi Tools\DevEx14211\ExpressFlowChart\Sources
::set ExpressGDIPlusLibrary=C:\Delphi Tools\DevEx14211\ExpressGDI+ Library\Sources
::set ExpressLayoutControl=C:\Delphi Tools\DevEx14211\ExpressLayout Control\Sources
::set ExpressLibrary=C:\Delphi Tools\DevEx14211\ExpressLibrary\Sources
::set ExpressMemData=C:\Delphi Tools\DevEx14211\ExpressMemData\Sources
::set ExpressNavBar=C:\Delphi Tools\DevEx14211\ExpressNavBar\Sources
::set ExpressOrgChart=C:\Delphi Tools\DevEx14211\ExpressOrgChart\Sources
::set ExpressPageControl=C:\Delphi Tools\DevEx14211\ExpressPageControl\Sources
::set ExpressPivotGrid=C:\Delphi Tools\DevEx14211\ExpressPivotGrid\Sources
::set ExpressPrintingSystem=C:\Delphi Tools\DevEx14211\ExpressPrinting System\Sources
::set ExpressQuantumGrid=C:\Delphi Tools\DevEx14211\ExpressQuantumGrid\Sources
::set ExpressQuantumTreeList=C:\Delphi Tools\DevEx14211\ExpressQuantumTreeList\Sources
::set ExpressScheduler=C:\Delphi Tools\DevEx14211\ExpressScheduler\Sources
::set ExpressSpellChecker=C:\Delphi Tools\DevEx14211\ExpressSpellChecker\Sources
::set ExpressSpreadSheetDeprecated=C:\Delphi Tools\DevEx14211\ExpressSpreadSheet (deprecated)\Sources
::set ExpressTileControl=C:\Delphi Tools\DevEx14211\ExpressTile Control\Sources
::set ExpressVerticalGrid=C:\Delphi Tools\DevEx14211\ExpressVerticalGrid\Sources
::set ExpressWizardControl=C:\Delphi Tools\DevEx14211\ExpressWizard Control\Sources
set RS11=C:\Delphi Tools\DevEx14211\Library\RS11

::@ set DEVEXDIR=%ExpressEditorsLibrary%;%ExpressBars%;%ExpressCommonLibrary%
::@ set DEVEXDIR=%DEVEXDIR%;%ExpressCoreLibrary%;%ExpressDataController%;%ExpressDBTreeSuite%;%ExpressDockingLibrary%
::@ set DEVEXDIR=%DEVEXDIR%;%ExpressExportLibrary%;%ExpressFlowChart%;%ExpressGDIPlusLibrary%;%ExpressLayoutControl%
::@ set DEVEXDIR=%DEVEXDIR%;%ExpressLibrary%;%ExpressMemData%;%ExpressNavBar%;%ExpressOrgChart%;%ExpressPageControl%
::@ set DEVEXDIR=%DEVEXDIR%;%ExpressPivotGrid%;%ExpressPrintingSystem%;%ExpressQuantumGrid%;%ExpressQuantumTreeList%
::@ set DEVEXDIR=%DEVEXDIR%;%ExpressScheduler%;%ExpressSpellChecker%;%ExpressSpreadSheetDeprecated%;%ExpressTileControl%
::@ set DEVEXDIR=%DEVEXDIR%;%ExpressVerticalGrid%;%ExpressWizardControl%;
@ set DEVEXDIR=%RS11%

::set ::DEVEXDIR=%ExpressEditorsLibrary%;%ExpressBars%;%ExpressCommonLibrary%;%ExpressCoreLibrary%;%ExpressDataController%;%ExpressDBTreeSuite%;%ExpressDockingLibrary%;%ExpressExportLibrary%;%ExpressFlowChart%;			
::%ExpressGDIPlusLibrary%;%ExpressLayoutControl%;%ExpressLibrary%;%ExpressMemData%;%ExpressNavBar%;%ExpressOrgChart%;%ExpressPageControl%;%ExpressPivotGrid%;%ExpressPrintingSystem%;%ExpressQuantumGrid%;			
::%ExpressQuantumTreeList%;%ExpressScheduler%;%ExpressSpellChecker%;%ExpressTileControl%;%ExpressVerticalGrid%;%ExpressWizardControl%;%RS11%;

rem MRH 20150812 THE ABOVE WAS USED BY OASIS DIGITAL TO MANAGE THE UPGRADE OF THE DEVEXRPRESS COMPONENTS (APPARENTLY) = WE WILL NEED TO CREATE OUR OWN UTILITY
rem changed from 19.0  8/28 sr
set RBDIR=%ProgramFiles(x86)%\Embarcadero\Studio\22.0\RBuilder\Lib\Win32  
set RB2007DIR=%ProgramFiles(x86)%\CodeGear\RAD Studio\5.0\RBuilder\Lib

set FIREDACDIRS=C:\Program Files (x86)\Embarcadero\FireDAC\Dcu\D17\win32

rem Location of any extra unit search paths
rem set OTHERDIRS=

rem Conditional defines
set OTHERDEFINES= 

rem Test MSSQL DB location - WARNING: This gets dropped and recreated as part of the build!
set SVR=DYBOC-DQMGAS01\QM_GAS
set DB=QM
rem set
pause;


