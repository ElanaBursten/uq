unit AttachmentImportData;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, SysUtils, Classes, ADODB, DB, MSXML2_TLB, OdLog, oduqinternet,
  ServerAttachmentDMu;

type
  TCommandLineModeStage = (clmsStart, clmsSetupLogger, clmsSetupForUploading,
    clmsAttachFile, clmsCleanUpFiles, clmsFinish);

  ECommandLineModeException = class(Exception)
  private
    FStageCompleted: TCommandLineModeStage;
    FMessageLogged: Boolean;
  public
    property StageCompleted: TCommandLineModeStage read FStageCompleted;
    property MessageLogged: Boolean read FMessageLogged write FMessageLogged;
    constructor CreateWithParams(AMessage: string;
      AStageCompleted: TCommandLineModeStage; AMessageLogged: Boolean);
  end;

  ECommandLineModeInvalidSyntaxException = class(ECommandLineModeException);

  TAttachmentImportDM = class(TDataModule)
    Conn: TADOConnection;
    Ticket: TADODataSet;
    Employee: TADODataSet;
    UploadLocation: TADODataSet;
    qryAPI_storage_constants: TADOQuery;
    qryAPI_storage_credentials: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure ConnAfterConnect(Sender: TObject);
  private
    FLogger: TOdLog;
    FCountAdded, FCountDeleted: Integer;
    FCountXMLProcessed, FCountXMLFailed: Integer;
    FLocationID: Integer;
    FImportFolderName: string;
    FIniFilename: string;
    FServerAttachment: TServerAttachment;
    procedure Log(const Msg: string);
    procedure LogError(const Msg: string);
    procedure AddSummaryToLog;
    procedure ImportAttachmentFromXml(Doc: IXMLDomDocument);
    function GetTicketIDForNumberCenterAndDate(const TicketNum, CallCenter: string): Integer;
    function GetEmpIDForEmpNumber(const EmpNumber: string): Integer;
    function GetLocationIDForLocationName(const LocationName: string): Integer;
    procedure ProcessXMLAttachment(const FileName: string);
    procedure LogAttachMessage(Sender: TObject; const Msg: string);
    procedure DeleteImportedAttachment(const XMLFileName: string; Doc: IXMLDomDocument);
    procedure UploadAndRecordAttachment(const ForeignType, ForeignID: Integer;
      const Path, Filename: string; const EmpID: Integer; const Comment,
      AttachmentSource: string);
    procedure SetupLogger;
    procedure SetupForUploading;
    function WorkItemName(ForeignType: Integer;
      CapitalizeFirstLetter: Boolean=False): string;
    procedure AddSummaryToLogForCommandLine(Filename: string; ForeignType,
      ForeignID: Integer; StageCompleted: TCommandLineModeStage);
    function GetIniFileName(const ExplicitIniFile: string=''): string;
    function GetAWSConstants: Boolean;
    function GetAWSCredentials: Boolean;
  public
    procedure ExecuteForXML(const IniFileName, ImportFolderName: string);
    procedure ExecuteForCommandLine(out StageCompleted: TCommandLineModeStage;
      const IniFilename, ForeignTypeStr, ForeignIDStr, Filename, EmpIDStr,
      Comment: string);
  end;

implementation

uses
  Variants, OdAdoUtils, OdIsoDates, OdMSXMLUtils, OdDbUtils, QMConst, OdMiscUtils,
  JclFileUtils, BaseAttachmentDMu;

{$R *.dfm}

const
  ImportFileMask = '*.attachmentimport.xml';
  UploadToLocation = 'Download'; //TODO: maybe this should come from the ini file instead?
  DefaultAttachmentSource = '';
  DefaultIniFilename = 'qmserver.ini';

procedure TAttachmentImportDM.ExecuteForXML(const IniFileName,
  ImportFolderName: string);
var
  Files: TStringList;
  I: Integer;
begin
  FCountAdded := 0;
  FCountDeleted := 0;
  FImportFolderName := ImportFolderName;
  FIniFilename := GetIniFileName(IniFileName);

  if not DirectoryExists(ImportFolderName) then
    raise Exception.CreateFmt('The specified attachment import folder %s does not exist.',
      [ImportFolderName]);

  SetupLogger;
  try
    Files := TStringList.Create;
    try
      AdvBuildFileList(IncludeTrailingBackslash(ImportFolderName) + ImportFileMask,
        faArchive, Files, amSubSetOf, [flFullNames]);

      if Files.Count = 0 then // no files to import, so just quit
        Exit;

      // start writing to the log file
      Log('QMAttachmentImport process starting ' + DateTimeToStr(Now));
      SetupForUploading;
      Log('Using import folder: ' + ImportFolderName);
      for i := 0 to Files.Count - 1 do begin
        ProcessXMLAttachment(Files[i]);
      end;
      AddSummaryToLog;
    finally
      FreeAndNil(Files);
    end;
  except on E: Exception do
    LogError(E.Message);
  end;
  // Log('Deleting temporary files');
//  FServerAttachment.CleanAttachmentDownloads;
  Log('QMAttachmentImport process complete ' + DateTimeToStr(Now));
end;


function TAttachmentImportDM.GetIniFileName(const ExplicitIniFile: string): string;
begin
  if NotEmpty(ExplicitIniFile) then
    Result := ExplicitIniFile
  else
    Result := DefaultIniFilename;

  if not FileExists(Result) then
    raise Exception.CreateFmt('Could not find configuration file %s.', [Result]);
end;

procedure TAttachmentImportDM.ExecuteForCommandLine(
  out StageCompleted: TCommandLineModeStage; const IniFilename, ForeignTypeStr,
  ForeignIDStr, Filename, EmpIDStr, Comment: string);
var
  ForeignType: Integer;
  ForeignID: Integer;
  EmpID: Integer;

  procedure ValidateAndConvertParameters;
  begin
    if ForeignTypeStr = '1' then
      ForeignType := qmftTicket
    else if ForeignTypeStr = '7' then
      ForeignType := qmftWorkOrder
    else
      raise ECommandLineModeInvalidSyntaxException.CreateFmt('Foreign type ' +
        'must be ''1'' or ''7'', but ''%s'' found.', [ForeignTypeStr]);

    if not IsInteger(ForeignIDStr) then
      raise ECommandLineModeInvalidSyntaxException.CreateFmt('%s id must be ' +
        'an integer, but ''%s'' found.', [WorkItemName(ForeignType, True),
        ForeignIDStr]);
    ForeignID := StrToInt(ForeignIDStr);

    if IsEmpty(Filename) then
      raise ECommandLineModeInvalidSyntaxException.Create('Attachment ' +
        'filename cannot be blank.');

    if not IsInteger(EmpIDStr) then
      raise ECommandLineModeInvalidSyntaxException.CreateFmt('Employee id ' +
        'must be an integer, but ''%s'' found.', [EmpIDStr]);
    EmpID := StrToInt(EmpIDStr);
  end;

  procedure HandleException(ExceptionRaised: Exception);
  var
    Msg: string;
    MsgLogged: Boolean;
  begin
    Msg := ExceptionRaised.Message;
    MsgLogged := False;
    try
      case StageCompleted of
        clmsSetupForUploading: Msg := 'Attachment not processed - ' + Msg;
        clmsAttachFile: Msg := 'File cleanup not completed - ' + Msg;
      end;
      if StageCompleted >= clmsSetupLogger then begin
        LogError(Msg);
        MsgLogged := True;
      end;
    except
      // Ignore failed logging
    end;

    if ExceptionRaised is ECommandLineModeInvalidSyntaxException then
      raise ECommandLineModeInvalidSyntaxException.CreateWithParams(Msg,
        StageCompleted, MsgLogged)
    else
      raise ECommandLineModeException.CreateWithParams(Msg, StageCompleted,
        MsgLogged);
  end;
begin
  try
    StageCompleted := clmsStart;

    FIniFilename := GetIniFileName(IniFilename);
    SetupLogger;
    StageCompleted := clmsSetupLogger;

    Log('--- START ---' + CRLF + 'QMAttachmentImport command line process ' +
      'starting - ' + DateTimeToStr(Now));
    ValidateAndConvertParameters;
    SetupForUploading;
    StageCompleted := clmsSetupForUploading;

    Log(CRLF + 'Processing attachment');
    Conn.BeginTrans;
    try
      UploadAndRecordAttachment(ForeignType, ForeignID,
        ExtractFilePath(FileName), ExtractFileName(FileName), EmpID, Comment,
        DefaultAttachmentSource);
      Conn.CommitTrans;
      StageCompleted := clmsAttachFile;
      Log('Attachment uploaded and recorded');
    except
      on Exception do begin
        if Conn.InTransaction then
          Conn.RollbackTrans;
        raise;
      end;
    end;

    DeleteFile(Filename);
    Log('Attachment file: ' + Filename + ' deleted');
//    FServerAttachment.CleanAttachmentDownloads;
    StageCompleted := clmsCleanUpFiles;
    Log('Temporary files deleted');
    Log('Processed OK');

    AddSummaryToLogForCommandLine(Filename, ForeignType, ForeignID,
      StageCompleted);
    Log(CRLF + 'QMAttachmentImport command line process complete - ' +
      DateTimeToStr(Now));

    StageCompleted := clmsFinish;
  except
    on E: Exception do
      HandleException(E);
  end;
end;

procedure TAttachmentImportDM.ProcessXMLAttachment(const FileName: string);
var
  ImportDoc: IXMLDomDocument;
begin
  ImportDoc := LoadXmlFile(FileName);
  try
    if not Assigned(ImportDoc) then
      raise Exception.CreateFmt('Unable to create a Dom Document using the specified ' +
        'file. Verify %s is a valid xml document.', [FileName]);
    Log(CRLF + 'Processing file ' + ExtractFileName(FileName));
    ImportAttachmentFromXml(ImportDoc);
    // Log('Deleting attached files');
    DeleteImportedAttachment(FileName, ImportDoc);
    Inc(FCountXMLProcessed);
  except
    on E: Exception do begin
      Inc(FCountXMLFailed);
      LogError('Not processed - ' + E.Message);
    end;
  end;
end;

procedure TAttachmentImportDM.ImportAttachmentFromXml(Doc: IXMLDomDocument);
const
  Root = '/QManager/Ticket';
  NewAttachmentPath = 'Attachments/NewAttachment';
  AttachmentFileName = 'FileName';
var
  TicketNode: IXMLDOMNode;
  AttachmentList: IXMLDOMNodeList;
  I: Integer;
  TicketID: Integer;
  EmpID: Integer;
  TicketIDStr: string;
  TicketNum: string;
  CallCenter: string;
  TransmitDate: string;
  FileName: string;
  AttachedByEmpNumber: string;
  DeleteFlag: string;
  Comment: string;
  CountThisFileAdded: Integer;
  CountThisFileDeleted: Integer;
begin
//  Assert(Assigned(FServerAttachment), 'ServerAttachment object is not assigned in ImportAttachmentFromXML');
  Conn.BeginTrans;
  CountThisFileAdded := 0;
  CountThisFileDeleted := 0;
  try
    TicketNode := Doc.selectSingleNode(Root);
    TicketIDStr := '';
    TicketNum := '';
    CallCenter := '';
    TransmitDate := '';
    FileName := '';
    GetAttributeTextFromNode(TicketNode, 'ID', TicketIDStr);
    if IsEmpty(TicketIDStr) then begin
      if not GetAttributeTextFromNode(TicketNode, 'Number', TicketNum) then
        raise Exception.Create('Ticket Number is required when ID is not supplied.');
      if not GetAttributeTextFromNode(TicketNode, 'CallCenter', CallCenter) then
        raise Exception.Create('Ticket CallCenter is required when ID is not supplied.');
      if not GetAttributeTextFromNode(TicketNode, 'TransmitDate', TransmitDate) then
        raise Exception.Create('Ticket TransmitDate is required when ID is not supplied.');
      if not IsoIsValidDateTime(TransmitDate) then
        raise Exception.CreateFmt('Ticket TransmitDate %s must be in ISO format (yyyy-mm-ddThh:mm:ss.nnn)', [TransmitDate]);
      TicketID := GetTicketIDForNumberCenterAndDate(TicketNum, CallCenter);
    end else
      TicketID := StrToIntDef(TicketIDStr, -1);

    AttachmentList := TicketNode.selectNodes(NewAttachmentPath);
    for I := 0 to AttachmentList.length - 1 do
    begin
      if not GetAttributeTextFromNode(AttachmentList.item[I], 'AttachedByEmpNumber', AttachedByEmpNumber) then
        raise Exception.Create('Attachment AttachedByEmpNumber is required.');
      if not GetAttributeTextFromNode(AttachmentList.item[I], 'FileName', FileName) then
        raise Exception.Create('Attachment FileName is required.');
      DeleteFlag := '0';
      GetAttributeTextFromNode(AttachmentList.item[I], 'Delete', DeleteFlag);
      Comment := '';
      GetAttributeTextFromNode(AttachmentList.item[I], 'Comment', Comment);
      EmpID := GetEmpIdForEmpNumber(AttachedByEmpNumber);
      if DeleteFlag = '1' then begin
        Log('Removing attachment ' + FileName + ' from ticket id ' + IntToStr(TicketID));
        FServerAttachment.RemoveAttachment(qmftTicket, TicketID, FileName, WhiteLineAttachmentSource);
        Inc(CountThisFileDeleted);
      end else begin
        UploadAndRecordAttachment(qmftTicket, TicketID, FImportFolderName,
          FileName, EmpID, Comment, WhiteLineAttachmentSource);
        Inc(CountThisFileAdded);
      end;
    end;
    Conn.CommitTrans;
    Log('Processed OK');
    Inc(FCountAdded, CountThisFileAdded);
    Inc(FCountDeleted, CountThisFileDeleted);
  except
    on Exception do begin
      if Conn.InTransaction then
        Conn.RollbackTrans;
      raise;
    end;
  end;
end;

procedure TAttachmentImportDM.AddSummaryToLog;
begin
  Log(CRLF + '---------  SUMMARY  ---------');
  Log(Format('XML Files Processed OK: %5d', [FCountXMLProcessed]));
  Log(Format('XML Files with Errors : %5d', [FCountXMLFailed]));
  Log(Format('Attachments Added     : %5d', [FCountAdded]));
  Log(Format('Attachments Removed   : %5d', [FCountDeleted]));
  Log('-----------------------------');
end;

function TAttachmentImportDM.GetAWSConstants: Boolean;  //QMANTWO-555 sr
begin
   if qryAPI_storage_constants.Active then
      qryAPI_storage_constants.Close;
   qryAPI_storage_constants.Open;
   if qryAPI_storage_constants.RecordCount > 0 then begin
     myAttachmentData.Constants.Host := qryAPI_storage_constants.FieldByName('host').AsString;
     myAttachmentData.Constants.RestURL :=  qryAPI_storage_constants.FieldByName('rest_url').AsString;
     myAttachmentData.Constants.Service := qryAPI_storage_constants.FieldByName('service').AsString;
     myAttachmentData.Constants.StagingBucket := qryAPI_storage_constants.FieldByName('bucket').AsString;
     myAttachmentData.Constants.StagingFolder := qryAPI_storage_constants.FieldByName('bucket_folder').AsString;
     myAttachmentData.Constants.Region := qryAPI_storage_constants.FieldByName('region').AsString;
     myAttachmentData.Constants.Algorithm := qryAPI_storage_constants.FieldByName('algorithm').AsString;
     myAttachmentData.Constants.SignedHeaders := qryAPI_storage_constants.FieldByName('signed_headers').AsString;
     myAttachmentData.Constants.ContentType := qryAPI_storage_constants.FieldByName('content_type').AsString;
     myAttachmentData.Constants.AcceptedValues := qryAPI_storage_constants.FieldByName('accepted_values').AsString;
     if myAttachmentData.Constants.Host <> '' then
       Result := True
     else
       Result := False
   end
   else
     Result := False;
end;

function TAttachmentImportDM.GetAWSCredentials: Boolean;   //QMANTWO-555 sr
begin
    myAttachmentData.AWSKey := '';
    myAttachmentData.AWSSecret := '';
  if qryAPI_storage_credentials.Active then
    qryAPI_storage_credentials.Close;

  qryAPI_storage_credentials.Open;
  if qryAPI_storage_credentials.RecordCount > 0 then begin
    myAttachmentData.AWSKey := qryAPI_storage_credentials.FieldByName('enc_access_key').AsWideString;
    myAttachmentData.AWSSecret := qryAPI_storage_credentials.FieldByName('enc_secret_key').AsWideString;
    if (myAttachmentData.AWSKey <> '') and (myAttachmentData.AWSSecret <> '') then
      Result := True
    else
      Result := False;
  end
  else begin
    myAttachmentData.AWSKey := '';
    myAttachmentData.AWSSecret := '';
    Result := False;
  end;
end;

procedure TAttachmentImportDM.DataModuleDestroy(Sender: TObject);
begin
  if Assigned(FLogger) then
    FLogger.CloseLog;
  FreeAndNil(FLogger);
  FreeAndNil(FServerAttachment);
  UnInitializeHandler;
end;

procedure TAttachmentImportDM.DeleteImportedAttachment(const XMLFileName: string; Doc: IXMLDomDocument);
const
  NewAttachmentPath = '/QManager/Ticket/Attachments/NewAttachment';
var
  I: Integer;
  Nodes: IXMLDomNodeList;
  FileToDelete: string;
begin
  // Log('Deleting imported attachments for ' + ExtractFileName(XMLFileName));
  Nodes := Doc.selectNodes(NewAttachmentPath);
  for I := 0 to Nodes.length - 1 do begin
    GetAttributeTextFromNode(Nodes.item[i], 'FileName', FileToDelete);
    FileToDelete := BuildFileName(FImportFolderName, FileToDelete);
    DeleteFile(FileToDelete);
  end;
  DeleteFile(XMLFileName);
end;

function TAttachmentImportDM.GetTicketIDForNumberCenterAndDate(const TicketNum, CallCenter: string): Integer;
var
  StartDate, EndDate: TDateTime;
begin
  EndDate := Now;
  StartDate := EndDate - 10;

  Result := -1;
  Ticket.Parameters.ParamByName('ticket_number').Value := TicketNum;
  Ticket.Parameters.ParamByName('ticket_format').Value := CallCenter;
  Ticket.Parameters.ParamByName('start_date').Value := StartDate;
  Ticket.Parameters.ParamByName('end_date').Value := EndDate;
  Ticket.Open;
  try
    if Ticket.IsEmpty then
      raise Exception.CreateFmt('Could not locate a ticket using Ticket # / Call Center / Trans Date Range: %s / %s / %s to %s',
        [TicketNum, CallCenter, IsoDateTimeToStr(StartDate), IsoDateTimeToStr(EndDate)]);
    if Ticket.RecordCount > 1 then
      raise Exception.CreateFmt('Multiple tickets found using Ticket # / Call Center / Trans Date Range: %s / %s / %s to %s',
        [TicketNum, CallCenter, IsoDateTimeToStr(StartDate), IsoDateTimeToStr(EndDate)]);
    Result := Ticket.FieldByName('ticket_id').Value;
  finally
    Ticket.Close;
  end;
end;

function TAttachmentImportDM.GetEmpIDForEmpNumber(const EmpNumber: string): Integer;
begin
  Result := -1;
  Employee.Parameters.ParamByName('emp_number').Value := EmpNumber;
  Employee.Open;
  try
    if Employee.IsEmpty then
      raise Exception.Create('Cannot find an employee with emp_number = ' + EmpNumber);
    Result := Employee.FieldByName('emp_id').Value;
  finally
    Employee.Close;
  end;
end;

function TAttachmentImportDM.GetLocationIDForLocationName(const LocationName: string): Integer;
begin
  Result := -1;
  UploadLocation.Parameters.ParamByName('description').Value := LocationName;
  UploadLocation.Open;
  try
    if UploadLocation.IsEmpty then
      raise Exception.Create('Cannot find an upload_location named ' + LocationName);
    Assert(UploadLocation.RecordCount = 1, 'Found more than one upload_location named ' + LocationName);
    Result := UploadLocation.FieldByName('location_id').Value;
  finally
    UploadLocation.Close;
  end;
end;

procedure TAttachmentImportDM.LogAttachMessage(Sender: TObject; const Msg: string);
begin
  Log(Msg);
end;

procedure TAttachmentImportDM.Log(const Msg: string);
begin
  FLogger.Write(Msg);
  {$IFNDEF TEST_MODE}
  WriteLn(Msg);
  {$ENDIF}
end;

procedure TAttachmentImportDM.LogError(const Msg: string);
begin
  FLogger.Write('* Error: ' + Msg);
  {$IFNDEF TEST_MODE}
  WriteLn('* Error: ' + Msg);
  {$ENDIF}
end;

procedure TAttachmentImportDM.UploadAndRecordAttachment(const ForeignType,
  ForeignID: Integer; const Path, Filename: string; const EmpID: Integer;
  const Comment, AttachmentSource: string);
var
  PathAndFilename: string;
  UploadableFile: string;
  ABaseAttachment:TBaseAttachment;
begin
  PathAndFilename := BuildFileName(Path, Filename);

  FServerAttachment.AttachedByEmpID := EmpID;
  if not FileExists(PathAndFilename) then
    raise Exception.CreateFmt('Attachment file %s does not exist.', [PathAndFilename]);
  Log('Adding attachment ' + Filename + ' to ' +
    WorkItemName(ForeignType) + ' id ' + IntToStr(ForeignID) +
    ' as emp id ' + IntToStr(EmpID));
  UploadableFile := FServerAttachment.PrepareFileForAttachment(ForeignType, ForeignID, PathAndFilename, AttachmentSource);
//  FServerAttachment.UploadAttachment(FLocationID, ForeignType, ForeignID, UploadableFile);
      { TODO : breakout to oduqInternet }
  myAttachmentData.LocalFilename :=  PathAndFilename;
  myAttachmentData.ServerFilename := UploadableFile;
  myAttachmentData.AttachmentID := FServerAttachment.AttachUploadedFileToRecordAWS(ForeignType, ForeignID, FileName, UploadableFile, Comment, AttachmentSource);
  Upload_Attachment(myAttachmentData, ABaseAttachment);
end;

procedure TAttachmentImportDM.SetupLogger;
var
  LogFileName: string;
begin
  Assert(NotEmpty(FIniFilename), 'Config filename is missing');
  LogFileName := 'QMAttachmentImport-' + FormatDateTime('yyyy-mm-dd-hhnnss', Now) + '.log';
  InitializeHandler(LogFileName, FIniFilename);
  FLogger := TOdLog.Create(LogFileName, FIniFilename);
  FLogger.OpenLog;
end;

procedure TAttachmentImportDM.SetupForUploading;
begin
  Assert(NotEmpty(FIniFilename), 'Config filename is missing');
  Log('Connecting to database');
  ConnectAdoConnectionWithIni(Conn, FIniFilename);
  FServerAttachment := TServerAttachment.Create(Conn);
  FServerAttachment.OnLogMessage := LogAttachMessage;
  FLocationID := GetLocationIDForLocationName(UploadToLocation);
end;

{ todo: Consider moving to common library }
function TAttachmentImportDM.WorkItemName(ForeignType: Integer;
  CapitalizeFirstLetter: Boolean=False): string;
begin
  Assert(ForeignType in [1..7], 'Invalid foreign type');
  Result := ForeignTypeDescription[ForeignType];
  if not CapitalizeFirstLetter then
    Result := LowerCase(Result);
end;

procedure TAttachmentImportDM.AddSummaryToLogForCommandLine(Filename: string;
  ForeignType, ForeignID: Integer; StageCompleted: TCommandLineModeStage);
begin
  Log(CRLF + '--- SUMMARY ---');

  if StageCompleted < clmsAttachFile then
    Log(Format('File %s NOT attached to %s # %d', [Filename,
      WorkItemName(ForeignType), ForeignID]))
  else begin
    Log(Format('File %s attached to %s # %d', [Filename,
      WorkItemName(ForeignType), ForeignID]));
    if StageCompleted < clmsCleanUpFiles then
      Log('File cleanup NOT completed')
    else
      Log('File cleanup completed');
  end;
end;

procedure TAttachmentImportDM.ConnAfterConnect(Sender: TObject);
begin
  GetAWSConstants;
  GetAWSCredentials;
end;

{ ECommandLineModeException }

constructor ECommandLineModeException.CreateWithParams(AMessage: string;
  AStageCompleted: TCommandLineModeStage; AMessageLogged: Boolean);
begin
  inherited Create(AMessage);
  FStageCompleted := AStageCompleted;
  FMessageLogged := AMessageLogged;
end;

end.
