object AttachmentImportDM: TAttachmentImportDM
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 243
  Width = 316
  object Conn: TADOConnection
    IsolationLevel = ilReadCommitted
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AfterConnect = ConnAfterConnect
    Left = 8
    Top = 16
  end
  object Ticket: TADODataSet
    CacheSize = 500
    Connection = Conn
    CommandText = 
      'select ticket_id, ticket_number, ticket_format, transmit_date '#13#10 +
      'from ticket '#13#10'where ticket_number = :ticket_number'#13#10'  and ticket' +
      '_format = :ticket_format'#13#10'  and transmit_date >= :start_date'#13#10'  ' +
      'and transmit_date <= :end_date'
    Parameters = <
      item
        Name = 'ticket_number'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ticket_format'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'start_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'end_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 80
    Top = 18
  end
  object Employee: TADODataSet
    CacheSize = 500
    Connection = Conn
    CommandText = 'select emp_id from employee where emp_number = :emp_number'
    Parameters = <
      item
        Name = 'emp_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    Left = 141
    Top = 18
  end
  object UploadLocation: TADODataSet
    CacheSize = 500
    Connection = Conn
    CommandText = 
      'select location_id from upload_location where active=1 and descr' +
      'iption=:description'
    Parameters = <
      item
        Name = 'description'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end>
    Left = 203
    Top = 18
  end
  object qryAPI_storage_constants: TADOQuery
    Connection = Conn
    Parameters = <>
    SQL.Strings = (
      'SELECT [id]'
      '      ,[host]'
      '      ,[rest_url]'
      '      ,[service]'
      '      ,[bucket]'
      '      ,[bucket_folder]'
      '      ,[region]'
      '      ,[algorithm]'
      '      ,[signed_headers]'
      '      ,[content_type]'
      '      ,[accepted_values]'
      '      ,[environment]'
      '      ,[description]'
      '      ,[unc_date_modified]'
      '      ,[modified_date]'
      '  FROM [dbo].[api_storage_constants]'
      '  where id = 1')
    Left = 72
    Top = 104
  end
  object qryAPI_storage_credentials: TADOQuery
    Connection = Conn
    Parameters = <>
    SQL.Strings = (
      'SELECT [id]'
      '      ,[storage_type]'
      '      ,[enc_access_key]'
      '      ,[enc_secret_key]'
      '      ,[unc_date_created]'
      '      ,[modified_date]'
      '      ,[unc_date_expired]'
      '      ,[api_storage_constants_id]'
      '      ,[description]'
      '  FROM [dbo].[api_storage_credentials]'
      '  where id = 1')
    Left = 208
    Top = 104
  end
end
