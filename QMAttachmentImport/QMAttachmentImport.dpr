program QMAttachmentImport;

{$APPTYPE CONSOLE}

{$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  SysUtils,
  ActiveX,
  AttachmentImportData in 'AttachmentImportData.pas' {AttachmentImportDM: TDataModule},
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  OdIsoDates in '..\common\OdIsoDates.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  MSXML2_TLB in '..\common\MSXML2_TLB.pas',
  OdMSXMLUtils in '..\common\OdMSXMLUtils.pas',
  OdLog in '..\common\OdLog.pas' {OdExceptionDialog},
  QMConst in '..\client\QMConst.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  OdVclUtils in '..\common\OdVclUtils.pas',
  OdDBISAMUtils in '..\common\OdDBISAMUtils.pas',
  BaseAttachmentDMu in '..\common\BaseAttachmentDMu.pas' {BaseAttachment: TDataModule},
  OdUqInternet in '..\common\OdUqInternet.pas',
  OdSecureHash in '..\common\OdSecureHash.pas',
  OdFtp in '..\common\OdFtp.pas',
  CommandLine in '..\common\CommandLine.pas',
  Hashes in '..\thirdparty\Hashes.pas',
  UQDbConfig in '..\common\UQDbConfig.pas',
  CodeLookupList in '..\common\CodeLookupList.pas',
  ServerAttachmentDMu in '..\server\ServerAttachmentDMu.pas' {ServerAttachment: TDataModule};

type
  TApplicationMode = (amUnknown, amXML, amCommandLine);

var
  DM: TAttachmentImportDM;
  CommandLineModeStageCompleted: TCommandLineModeStage;
  NextParam, ParamsLeft: Integer;
  INIFilename: string;
  AppMode: TApplicationMode;

  procedure ShowHelpScreen;
  begin
    Writeln('XML file mode -----------------------------------------------------------------');
    Writeln('Version: ' + AppVersionShort);
    Writeln;
    Writeln('Adds and deletes attachments for tickets. Ticket and attachment parameters are');
    Writeln('read from XML files.');
    Writeln;
    Writeln('  QMAttachmentImport [/ini INIFilename] ImportFolderName');
    Writeln;
    Writeln('/ini INIFilename    Configuration filename (defaults to QMServer.ini, in the');
    Writeln('                    current directory). The database and log file');
    Writeln('                    configurations come from this file.');
    Writeln('ImportFolderName    Folder to scan for new attachments for importing');
    Writeln;
    Writeln('ERRORLEVEL will be set as follows, when the utility exits:');
    Writeln('0 - Running in XML file mode was attempted. Success or failure is not');
    Writeln('    indicated.');
    Writeln('4 - Running in XML file mode was NOT attempted. An invalid number of command');
    Writeln('    line parameters is a probable cause.');
    Writeln;
    Writeln('Command line mode -------------------------------------------------------------');
    Writeln;
    Writeln('Adds an attachment. All parameters are entered on the command line.');
    Writeln;
    Writeln('  QMAttachmentImport [/ini INIFilename] WorkItemType WorkItemID');
    Writeln('    AttachmentFilename EmpID Comment');
    Writeln;
    Writeln('/ini INIFilename    Configuration filename (defaults to QMServer.ini, in the');
    Writeln('                    current directory). The database and log file');
    Writeln('                    configurations come from this file.');
    Writeln('WorkItemType        "1" = ticket, "7" = work order.');
    Writeln('WorkItemID          The ticket id or work order id');
    Writeln('AttachmentFilename  The path and filename of the file to attach');
    Writeln('EmpID               The employee id assigned to the attachment');
    Writeln('Comment             The comment for the attachment. Enclose in quotes');
    Writeln('                    if spaces embedded, e.g. "This comment has spaces".');
    Writeln;
    Writeln('ERRORLEVEL will be set as follows, when the utility exits:');
    Writeln('0 - No errors encountered. The attachment and file cleanup processes were');
    Writeln('    successful.');
    Writeln('1 - Error encountered, but the attachment and file cleanup processes were');
    Writeln('    successful.');
    Writeln('2 - Error encountered. The attachment process was successful, but there was a');
    Writeln('    problem cleaning up files.');
    Writeln('3 - Error encountered. The attachment process failed.');
    Writeln('4 - Running in command line mode was not attempted. An invalid number of');
    Writeln('    command line parameters is a probable cause.');
  end;

  procedure SetCommandLineModeExitCode(StageCompleted: TCommandLineModeStage);
  begin
    case StageCompleted of
      clmsFinish:
        { The attachment and cleanup processes were successful, and there were
          no errors }
        ExitCode := 0;
      clmsCleanUpFiles:
        { There was an error, but the attachment and cleanup processes were
          successful }
        ExitCode := 1;
      clmsAttachFile:
        { The attachment process was successful, but there was a problem
          cleaning up files }
        ExitCode := 2;
    else
      ExitCode := 3; // The attachment process failed
    end;
  end;
begin
  ExitCode := 4; // Indicates XML or command line mode not detected

  if HelpSwitch(True) then begin
    ShowHelpScreen;
    Exit;
  end;

  NextParam := 1;
  INIFilename := ParamStrIfNext('/ini', NextParam);
  ParamsLeft := ParamCount - NextParam + 1;

  if ParamsLeft = 1 then begin
    AppMode := amXML;
    ExitCode := 0; // XML mode. Always return 0 after XML mode is detected.
  end
  { TODO : ParamCount fails when comment is "", so currently allowing 4 or 5
    params, instead of exactly 5. Consider trying CmdLineHelper to fix this. }
  else if (ParamsLeft = 4) or (ParamsLeft = 5) then begin
    AppMode := amCommandLine;
    SetCommandLineModeExitCode(clmsStart)
  end else begin
    Writeln('ERROR: Invalid number of parameters');
    Writeln('0 attachments processed');
    Writeln;
    ShowHelpScreen;
    Exit;
  end;

  try
    CoInitialize(nil);
    DM := TAttachmentImportDM.Create(nil);
    try
      if AppMode = amXML then
        DM.ExecuteForXML(INIFilename, ParamStr(NextParam))
      else begin
        DM.ExecuteForCommandLine(CommandLineModeStageCompleted, INIFilename,
          ParamStr(NextParam), ParamStr(NextParam + 1), ParamStr(NextParam + 2),
          ParamStr(NextParam + 3), ParamStr(NextParam + 4));
        SetCommandLineModeExitCode(CommandLineModeStageCompleted);
      end;
    finally
      FreeAndNil(DM);
      CoUninitialize;
    end;
  except
    on E: ECommandLineModeInvalidSyntaxException do begin
      SetCommandLineModeExitCode(E.StageCompleted);
      if not E.MessageLogged then
        Writeln('ERROR: Invalid syntax. ' + E.Message);
      Writeln;
      ShowHelpScreen;
    end;
    on E: ECommandLineModeException do begin
      SetCommandLineModeExitCode(E.StageCompleted);
      if not E.MessageLogged then
        Writeln('ERROR:' + E.Message);
    end;
    on E: Exception do
      Writeln('ERROR:' + E.Message);
  end;

end.
