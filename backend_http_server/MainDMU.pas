unit MainDMU;

interface

uses
  System.SysUtils, System.Classes, HTTPServerDMU, HTTPClientDMU, ConfigU;

type
  TMainDM = class(TDataModule)
  private
    FHTTPServerDM: THTTPServerDM;
    FHTTPClientDM: THTTPClientDM;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function InitializeConfig(out ErrorMessage: string): Boolean;
    procedure StartServer;
    procedure StopServer;
    procedure RestartServer;
    procedure PauseServer;
    procedure ResumeServer;
    procedure OpenBrowser;
    function ServerActive: Boolean;
    function ServerPort: Integer;
    function Configuration: TConfig;
    property HTTPClientDM: THTTPClientDM read FHTTPClientDM;
  end;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

uses
  Winapi.ShellApi, Winapi.Windows, LoggerU, FireDAC.Comp.Client;

function TMainDM.Configuration: TConfig;
begin
  Result := Config;
end;

constructor TMainDM.Create(AOwner: TComponent);
begin
  inherited;
  FDManager.Active := True;
  Config := TConfig.Create;
  Logger := TLogger.Create;
  FHTTPServerDM := THTTPServerDM.Create(nil);
  FHTTPClientDM := THTTPClientDM.Create(nil);
end;

destructor TMainDM.Destroy;
begin
  FreeAndNil(FHTTPClientDM);
  FreeAndNil(FHTTPServerDM);
  FreeAndNil(Logger);
  FreeAndNil(Config);
  inherited;
end;

function TMainDM.InitializeConfig(out ErrorMessage: string): Boolean;
begin
  Result := False;
  Logger.Log('(Re)initializing and validating configuration');
  if Config.InitializeAndValidate(ErrorMessage) then
    Result := True;

  // Reinitialize logger to use new log file
  FreeAndNil(Logger);
  Logger := TLogger.Create;

  if Result then
    Logger.Log('Configuration (re)initialized. No errors found.')
  else
    Logger.Log('Configuration (re)initialized.' + sLineBreak + ErrorMessage);
end;

procedure TMainDM.StartServer;
var
  ErrorMessage: string;
begin
  // Don't reinitialize config, if the server is already running
  if ServerActive then
    raise Exception.Create('Error: Server already running.');
  if InitializeConfig(ErrorMessage) then
    FHTTPServerDM.StartServer
  else
    raise Exception.Create(ErrorMessage);
end;

procedure TMainDM.StopServer;
begin
  FHTTPServerDM.StopServer;
end;

procedure TMainDM.RestartServer;
begin
  StopServer;
  StartServer;
end;

procedure TMainDM.PauseServer;
begin
  StopServer;
end;

procedure TMainDM.ResumeServer;
begin
  // Don't reinitialize config, since resuming from a pause
  if not Config.AllSettingsValid then
    // This shouldn't happen
    raise Exception.Create('Error: Invalid configuration');
  FHTTPServerDM.StartServer;
end;

function TMainDM.ServerActive: Boolean;
begin
  Result := FHTTPServerDM.ServerActive;
end;

procedure TMainDM.OpenBrowser;
var
  LURL: string;
begin
  LURL := Format('http://localhost:%d', [ServerPort]);
  ShellExecute(0,
        nil,
        PChar(LURL), nil, nil, SW_SHOWNOACTIVATE);
end;

function TMainDM.ServerPort: Integer;
begin
  if Config.Initialized then
    Result := Config.ServerPort
  else
    Result := InvalidPort;
end;

end.
