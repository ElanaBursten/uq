unit HTTPServerDMU;

interface

uses
  System.SysUtils, System.Classes, IdHTTPWebBrokerBridge;

type
  THTTPServerDM = class(TDataModule)
  private
    FServer: TIdHTTPWebBrokerBridge;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure StartServer;
    procedure StopServer;
    function ServerActive: Boolean;
  end;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

uses
  ConfigU, LoggerU;

constructor THTTPServerDM.Create(AOwner: TComponent);
begin
  inherited;
  Assert(Assigned(Logger), 'Logger not assigned');
  FServer := TIdHTTPWebBrokerBridge.Create(Self);
end;

destructor THTTPServerDM.Destroy;
begin
  FreeAndNil(FServer);
  inherited;
end;

procedure THTTPServerDM.StartServer;
begin
  { Starting the server implies the server will be using the latest config
    settings. So require the caller to stop the server first, if necessary. }
  if FServer.Active then
    raise Exception.Create('Error: Server already running.');

  Logger.Log('Starting HTTP server');
  FServer.Bindings.Clear;
  FServer.DefaultPort := Config.ServerPort;
  FServer.Active := True;
  if not FServer.Active then
    // This shouldn't happen. An exception should have been raised already.
    raise Exception.Create('Error: HTTP server failed to start');
  Logger.Log('HTTP server started');
end;

procedure THTTPServerDM.StopServer;
begin
  if FServer.Active then begin
    Logger.Log('Stopping HTTP server');
    FServer.Active := False;
    FServer.Bindings.Clear;
    if FServer.Active then
      // This shouldn't happen
      raise Exception.Create('Error: HTTP server failed to stop');
    Logger.Log('HTTP server stopped');
  end;
end;

function THTTPServerDM.ServerActive: Boolean;
begin
  Result := FServer.Active;
end;

end.
