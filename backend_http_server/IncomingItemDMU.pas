unit IncomingItemDMU;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

const
  { 32767 is the max for FireDac string params. Memo params can be larger, but
    will be converted to AnsiString, with potential data loss. But the varchar
    fields won't handle unicode anyway. }
  MaxItemTextLength = 50000;

type
  TIncomingItemType = (itTicket, itMessage, itAudit);

  TIncomingItemDM = class(TDataModule)
    ADQuery: TFDQuery;
  private
  public
    constructor Create(AOwner: TComponent); override;
    procedure Save(const CallCenter: string; const ItemText: string;
      const ItemType: TIncomingItemType);
  end;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

uses
  DateUtils, IOUtils, OdMiscUtils, ConfigU, DBConnectionDMU, LoggerU;

const
  TicketItemType = 1;
  AuditItemType = 2;
  MessageItemType = 3;

constructor TIncomingItemDM.Create(AOwner: TComponent);
begin
  inherited;
  if not Assigned(Logger) then
    raise Exception.Create('Error: Logger not assigned');
end;

procedure TIncomingItemDM.Save(const CallCenter: string; const ItemText: string;
  const ItemType: TIncomingItemType);
var
  ItemTypeCode: Integer;
  DBConnectionDM: TDBConnectionDM;
begin
  case ItemType of
    itTicket: ItemTypeCode := TicketItemType;
    itAudit: ItemTypeCode := AuditItemType;
    itMessage: ItemTypeCode := MessageItemType;
  else
    raise Exception.Create('Error: Unexpected item type');
  end;

  ADQuery.ParamByName('item_text').Size := MaxItemTextLength;

  ADQuery.ParamByName('cc_code').AsString := CallCenter;
  ADQuery.ParamByName('item_type').AsInteger := ItemTypeCode;
  ADQuery.ParamByName('item_text').AsMemo := AnsiString(ItemText);

  DBConnectionDM := TDBConnectionDM.Create(nil);
  try
    ADQuery.Connection := DBConnectionDM.FDConnection;
    DBConnectionDM.Connect;
    ADQuery.Execute;
    DBConnectionDM.Disconnect;
  finally
    FreeAndNil(DBConnectionDM);
  end;
end;

end.
