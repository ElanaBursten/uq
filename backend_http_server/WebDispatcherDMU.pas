unit WebDispatcherDMU;

interface

uses System.SysUtils, System.Classes, Web.HTTPApp, IncomingItemDMU;

type
  TWebDispatcherDM = class(TWebModule)
    procedure WebDispatcherDMDefaultHandlerAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModuleException(Sender: TObject; E: Exception;
      var Handled: Boolean);
  private
    function HasCorrectFields(Request: TWebRequest;
      ExpectedFieldNames: array of string): Boolean;
    function IsValidXML(XML: string): Boolean;
    procedure HandleIncomingItem(Request: TWebRequest; Response: TWebResponse;
      const CallCenter: string; const ItemType: TIncomingItemType);
    function GetWSDLFromResource(const WSDLResourceName: string): string;
    procedure HandleWSDL(Request: TWebRequest; Response: TWebResponse);
  public
    constructor Create(AOwner: TComponent); override;
  end;

var
  WebModuleClass: TComponentClass = TWebDispatcherDM;

implementation

{$R *.dfm}

uses
  OdMiscUtils, StrUtils, Winapi.Windows, ConfigU, LoggerU, OdMSXMLUtils, ActiveX;

const
  Status400Message = '400 - Bad Request';
  Status404Message = '404 - The requested URL was not found on this server';
  Status405Message = '405 - Method not allowed';
  Status422Message = '422 - Unprocessable entity';
  Status500Message = '500 - Internal Server Error';
  SuccessMessage = 'successful';
  XMLTicketField = 'xmlTicket';
  XMLAuditField = 'xmlAudit';
  XMLMessageField = 'xmlMessage';
  C3003Code = '3003';

function TWebDispatcherDM.HasCorrectFields(Request: TWebRequest;
  ExpectedFieldNames: array of string): Boolean;
var
  I: Integer;
begin
  Result := True;

  if Request.ContentFields.Count <> Length(ExpectedFieldNames) then
    Result := False
  else
    for I := 0 to Request.ContentFields.Count - 1 do begin
      if not MatchText(Request.ContentFields.Names[I], ExpectedFieldNames) then
      begin
        Result := False;
        Break;
      end;
    end;
end;

function TWebDispatcherDM.IsValidXML(XML: string): Boolean;
begin
  CoInitialize(nil);
  try
    Result := OdMSXMLUtils.IsValidXML(XML);
  finally
    CoUninitialize;
  end;
end;

procedure TWebDispatcherDM.HandleIncomingItem(Request: TWebRequest;
  Response: TWebResponse; const CallCenter: string;
  const ItemType: TIncomingItemType);
var
  FieldName: string;
  IncomingItemDM: TIncomingItemDM;
begin
  case ItemType of
    itTicket: FieldName := XMLTicketField;
    itMessage: FieldName := XMLMessageField;
    itAudit: FieldName := XMLAuditField;
  end;

  if Request.MethodType <> mtPost then begin
    Response.Content := Status405Message + '. POST expected.';
    Response.StatusCode := 405;
  end else if not HasCorrectFields(Request, [FieldName]) then begin
    Response.Content := Status422Message + '. One field named ' + FieldName +
      ' expected.';
    Response.StatusCode := 422;
  end else if Length(Request.ContentFields.Values[FieldName]) >
    MaxItemTextLength then begin
    { TODO -odan -c3442 : Should this message include details? }
    Response.Content := Status500Message;
    Response.StatusCode := 500;
  end else if not IsValidXML(Request.ContentFields.Values[FieldName]) then begin
    Response.Content := Format(Status400Message + '. Invalid XML in %s.',
      [FieldName]);
    Response.StatusCode := 400;
  end else begin
    IncomingItemDM := TIncomingItemDM.Create(nil);
    try
      IncomingItemDM.Save(CallCenter, Request.ContentFields.Values[FieldName],
        ItemType);
      Response.Content := SuccessMessage;
      Response.StatusCode := 202;
    finally
      FreeAndNil(IncomingItemDM);
    end;
  end;

  Response.ContentType := 'text/plain';
end;

function TWebDispatcherDM.GetWSDLFromResource(const WSDLResourceName: string):
  string;
var
  Stream: TResourceStream;
  StringList: TStringList;
begin
  try
    StringList := TStringList.Create;
    Stream := TResourceStream.Create(HInstance, WSDLResourceName, RT_RCDATA);
    StringList.LoadFromStream(Stream);
    Result := StringList.Text;
  finally
    FreeAndNil(Stream);
    FreeAndNil(StringList);
  end;
end;

procedure TWebDispatcherDM.HandleWSDL(Request: TWebRequest;
  Response: TWebResponse);
begin
  if HasCorrectFields(Request, ['']) and SameText(Request.ContentFields[0],
    'wsdl') then begin
    Response.Content := GetWSDLFromResource('DEFAULT_WSDL');
    Response.ContentType := 'text/xml';
    Response.StatusCode := 200;
  end else begin
    Response.Content := Status422Message + '. No data fields expected.';
    Response.ContentType := 'text/plain';
    Response.StatusCode := 422;
  end;
end;

procedure TWebDispatcherDM.WebDispatcherDMDefaultHandlerAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
  Path: string;
  Query: string;
begin
  Path := LowerCase(string(Request.PathInfo));
  if Path = '' then
    Path := '/'
  else if Path[Length(Path)] <> '/' then
    Path := Path + '/';
  Query := LowerCase(string(Request.Query));

  // 3003
  if (Path = '/3003/') and (Query = 'sendticket') then
    HandleIncomingItem(Request, Response, C3003Code, itTicket)
  else if (Path = '/3003/') and (Query = 'sendmessage') then
    HandleIncomingItem(Request, Response, C3003Code, itMessage)
  else if (Path = '/3003/') and (Query = 'sendaudit') then
    HandleIncomingItem(Request, Response, C3003Code, itAudit)
  else if (Path = '/3003/') and (Query = 'wsdl') then
    HandleWSDL(Request, Response)

  // Default
  else begin
    Response.StatusCode := 404;
    Response.ContentType := 'text/plain';
    Response.Content := Status404Message;
  end;
end;

procedure TWebDispatcherDM.WebModuleException(Sender: TObject; E: Exception;
  var Handled: Boolean);
begin
  Logger.Log(E.Message);
  Response.StatusCode := 500;
  Response.Content := Status500Message;
  Response.ContentType := 'text/plain';
end;

constructor TWebDispatcherDM.Create(AOwner: TComponent);
begin
  inherited;
  if not Assigned(Logger) then
    raise Exception.Create('Error: Logger not assigned');
end;

end.
