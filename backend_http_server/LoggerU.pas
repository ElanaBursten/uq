unit LoggerU;

interface

uses
  ThreadSafeLoggerU;

type
  TLogger = class(TObject)
  private
    FThreadSafeLogger: TThreadSafeLogger;
  public
    constructor Create;
    destructor Destroy; override;
    { TODO -odan -c3442 : Add LogError(Fmt), and prefix those messages with
      '[Error]'? }
    procedure Log(const Message: string);
    procedure LogFmt(const MessageFormat: string;
      const MessageArgs: array of const);
  end;

var
  Logger: TLogger;

implementation

uses
  SysUtils, ConfigU, OdMiscUtils;

constructor TLogger.Create;
var
  Fn: string;
begin
  inherited;
  Assert(Assigned(Config));

  if Config.Initialized and (Config.ServerLogFilePath <> '') then begin
    ForceDirectories(Config.ServerLogFilePath);
    Fn := ChangeFileExt(ExtractFileName(ParamStr(0)), '') + '.log';
    FThreadSafeLogger := TThreadSafeLogger.Create(Config.ServerLogFilePath, Fn, Config.ServerInstanceID);
  end;
end;

destructor TLogger.Destroy;
begin
  FreeAndNil(FThreadSafeLogger);
  inherited;
end;

procedure TLogger.Log(const Message: string);
begin
  if Assigned(FThreadSafeLogger) then
    FThreadSafeLogger.LogWithTime(Message);
end;

procedure TLogger.LogFmt(const MessageFormat: string;
  const MessageArgs: array of const);
begin
  if Assigned(FThreadSafeLogger) then
    Log(Format(MessageFormat, MessageArgs));
end;

end.
