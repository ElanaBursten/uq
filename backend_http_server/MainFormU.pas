unit MainFormU;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.AppEvnts, Vcl.StdCtrls, MainDMU, Vcl.ActnList, Vcl.ComCtrls, Vcl.ExtCtrls,
  System.Actions, RequestFieldFrameU;

type
  TMainForm = class(TForm)
    StartButton: TButton;
    StopButton: TButton;
    PortEdit: TEdit;
    Label1: TLabel;
    ApplicationEvents: TApplicationEvents;
    ButtonOpenBrowser: TButton;
    ActionList: TActionList;
    StartServerAction: TAction;
    StopServerAction: TAction;
    OpenBrowserAction: TAction;
    Panel1: TPanel;
    OpenDataFileAction: TAction;
    OpenFileDialog: TOpenDialog;
    Label5: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Field1Frame: TRequestFieldFrame;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    RequestURLComboBox: TComboBox;
    Panel2: TPanel;
    Button1: TButton;
    SendRequestAction: TAction;
    Field2Frame: TRequestFieldFrame;
    TabSheet3: TTabSheet;
    Field3Frame: TRequestFieldFrame;
    procedure ApplicationEventsIdle(Sender: TObject; var Done: Boolean);
    procedure StartServerActionExecute(Sender: TObject);
    procedure StopServerActionExecute(Sender: TObject);
    procedure OpenBrowserActionExecute(Sender: TObject);
    procedure OpenDataFileActionExecute(Sender: TObject);
    procedure SendRequestActionExecute(Sender: TObject);
  private
    FMainDM: TMainDM;
    FInitialized: Boolean;
    FServerPort: Integer;
    F3003ProxyServiceURL: string;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.ApplicationEventsIdle(Sender: TObject; var Done: Boolean);
var
  ErrorMessage: string;
  ServerPortStr: string;

  function WithTrailingSlash(const URL: string): string;
  begin
    if URL = '' then
      Result := '/'
    else if URL[Length(URL)] <> '/' then
      Result := URL + '/'
    else
      Result := URL;
  end;
begin
  if not FInitialized then begin
    FServerPort := -999;
    { TODO -odan -c3442 : Get this from an edit box }
    F3003ProxyServiceURL := 'http://localhost/gaupc';
    if not FMainDM.InitializeConfig(ErrorMessage) then
      MessageDlg(ErrorMessage, mtError, [mbOK], 0);
    FInitialized := True;
  end;

  StartServerAction.Enabled := not FMainDM.ServerActive;
  StopServerAction.Enabled := FMainDM.ServerActive;
  OpenBrowserAction.Enabled := FMainDM.ServerActive;

  if FServerPort <> FMainDM.ServerPort then begin
    FServerPort := FMainDM.ServerPort;
    if FServerPort > 0 then begin
      PortEdit.Text := IntToStr(FServerPort);
      ServerPortStr := PortEdit.Text;
    end else begin
      PortEdit.Text := '';
      ServerPortStr := '<port>';
    end;

    RequestURLComboBox.Items.Clear;
    RequestURLComboBox.Items.Add('');

    RequestURLComboBox.Items.Add(Format('http://localhost:%s/3003?sendticket',
      [ServerPortStr]));
    RequestURLComboBox.Items.Add(Format('http://localhost:%s/3003?sendmessage',
      [ServerPortStr]));
    RequestURLComboBox.Items.Add(Format('http://localhost:%s/3003?sendaudit',
      [ServerPortStr]));
    RequestURLComboBox.Items.Add(Format('http://localhost:%s/3003?wsdl',
      [ServerPortStr]));

    RequestURLComboBox.Items.Add(Format('%s?sendticket', [F3003ProxyServiceURL]));
    RequestURLComboBox.Items.Add(Format('%s?sendmessage', [F3003ProxyServiceURL]));
    RequestURLComboBox.Items.Add(Format('%s?sendaudit', [F3003ProxyServiceURL]));
    RequestURLComboBox.Items.Add(Format('%s?wsdl', [F3003ProxyServiceURL]));
  end;
end;

procedure TMainForm.SendRequestActionExecute(Sender: TObject);
begin
  { TODO -odan -c3442 : Change this? Show response on form? Limit length in
    dialog? }
  ShowMessage(FMainDM.HTTPClientDM.SendPostRequest(RequestURLComboBox.Text,
    [Field1Frame.FieldNameComboBox.Text, Field2Frame.FieldNameComboBox.Text,
    Field3Frame.FieldNameComboBox.Text], [Field1Frame.FieldDataMemo.Lines.Text,
    Field2Frame.FieldDataMemo.Lines.Text,
    Field3Frame.FieldDataMemo.Lines.Text]));
end;

procedure TMainForm.StartServerActionExecute(Sender: TObject);
begin
  FMainDM.StartServer;
end;

procedure TMainForm.StopServerActionExecute(Sender: TObject);
begin
  FMainDM.StopServer;
end;

procedure TMainForm.OpenBrowserActionExecute(Sender: TObject);
begin
  FMainDM.OpenBrowser;
end;

procedure TMainForm.OpenDataFileActionExecute(Sender: TObject);
begin
  if OpenFileDialog.Execute then
    ShowMessage(OpenFileDialog.FileName);{ TODO -odan -c3442 : Move to frame? }
end;

constructor TMainForm.Create(AOwner: TComponent);
begin
  inherited;
  FMainDM := TMainDM.Create(nil);
end;

destructor TMainForm.Destroy;
begin
  FreeAndNil(FMainDM);
  inherited;
end;

end.
