program QMCollectorService;

{$R 'QMCollectorService.res' 'QMCollectorService.rc'}
{$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  Vcl.SvcMgr,
  Web.WebReq,
  ConfigU in 'ConfigU.pas',
  DBConnectionDMU in 'DBConnectionDMU.pas' {DBConnectionDM: TDataModule},
  HTTPServerDMU in 'HTTPServerDMU.pas' {HTTPServerDM: TDataModule},
  HTTPServerServiceU in 'HTTPServerServiceU.pas' {QMDataCollectorService: TService},
  IncomingItemDMU in 'IncomingItemDMU.pas' {IncomingItemDM: TDataModule},
  LoggerU in 'LoggerU.pas',
  MainDMU in 'MainDMU.pas' {MainDM: TDataModule},
  WebDispatcherDMU in 'WebDispatcherDMU.pas' {WebDispatcherDM: TWebModule};

begin
  // Windows 2003 Server requires StartServiceCtrlDispatcher to be
  // called before CoRegisterClassObject, which can be called indirectly
  // by Application.Initialize. TServiceApplication.DelayInitialize allows
  // Application.Initialize to be called from TService.Main (after
  // StartServiceCtrlDispatcher has been called).
  //
  // Delayed initialization of the Application object may affect
  // events which then occur prior to initialization, such as
  // TService.OnCreate. It is only recommended if the ServiceApplication
  // registers a class object with OLE and is intended for use with
  // Windows 2003 Server.
  //
  // Application.DelayInitialize := True;
  //

  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;

  if not Application.DelayInitialize or Application.Installing then
    Application.Initialize;
  Application.CreateForm(TQMDataCollectorService, QMDataCollectorService);
  Application.Run;
end.
