object MainForm: TMainForm
  Left = 271
  Top = 114
  Caption = 'MainForm'
  ClientHeight = 428
  ClientWidth = 546
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 546
    Height = 57
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 25
      Top = 22
      Width = 20
      Height = 13
      Caption = 'Port'
    end
    object ButtonOpenBrowser: TButton
      Left = 438
      Top = 17
      Width = 85
      Height = 25
      Action = OpenBrowserAction
      TabOrder = 0
    end
    object PortEdit: TEdit
      Left = 51
      Top = 19
      Width = 121
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object StartButton: TButton
      Left = 268
      Top = 17
      Width = 75
      Height = 25
      Action = StartServerAction
      TabOrder = 2
    end
    object StopButton: TButton
      Left = 349
      Top = 17
      Width = 75
      Height = 25
      Action = StopServerAction
      TabOrder = 3
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 57
    Width = 546
    Height = 371
    Align = alClient
    TabOrder = 1
    DesignSize = (
      546
      371)
    object Label5: TLabel
      Left = 24
      Top = 22
      Width = 19
      Height = 13
      Caption = 'URL'
    end
    object PageControl1: TPageControl
      Left = 271
      Top = 50
      Width = 266
      Height = 312
      ActivePage = TabSheet1
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Field 1'
        inline Field1Frame: TRequestFieldFrame
          Left = 0
          Top = 0
          Width = 258
          Height = 284
          Align = alClient
          TabOrder = 0
          inherited Panel1: TPanel
            Top = 244
            Width = 258
            inherited LoadFromFileButton: TButton
              Left = 135
            end
          end
          inherited Panel2: TPanel
            Width = 258
            Height = 244
            inherited FieldDataMemo: TMemo
              Width = 174
              Height = 195
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Field 2'
        ImageIndex = 1
        inline Field2Frame: TRequestFieldFrame
          Left = 0
          Top = 0
          Width = 258
          Height = 284
          Align = alClient
          TabOrder = 0
          inherited Panel1: TPanel
            Top = 244
            Width = 258
            inherited LoadFromFileButton: TButton
              Left = 135
            end
          end
          inherited Panel2: TPanel
            Width = 258
            Height = 244
            inherited FieldDataMemo: TMemo
              Width = 174
              Height = 195
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Field 3'
        ImageIndex = 2
        inline Field3Frame: TRequestFieldFrame
          Left = 0
          Top = 0
          Width = 258
          Height = 284
          Align = alClient
          TabOrder = 0
          inherited Panel1: TPanel
            Top = 244
            Width = 258
            inherited LoadFromFileButton: TButton
              Left = 135
            end
          end
          inherited Panel2: TPanel
            Width = 258
            Height = 244
            inherited FieldDataMemo: TMemo
              Width = 174
              Height = 195
            end
          end
        end
      end
    end
    object ComboBox1: TComboBox
      Left = 50
      Top = 253
      Width = 190
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 1
      Text = 'application/x-www-form-urlencoded'
      Visible = False
      Items.Strings = (
        'application/x-www-form-urlencoded')
    end
    object ComboBox2: TComboBox
      Left = 51
      Top = 226
      Width = 190
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 2
      Text = 'POST'
      Visible = False
      Items.Strings = (
        'POST')
    end
    object RequestURLComboBox: TComboBox
      Left = 51
      Top = 19
      Width = 486
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 3
      Items.Strings = (
        ''
        'http://localhost:6003/3003/sendticket'
        'http://localhost:6003/3003/sendmessage'
        'http://localhost:6003/3003/sendaudit')
    end
    object Button1: TButton
      Left = 51
      Top = 50
      Width = 89
      Height = 25
      Action = SendRequestAction
      TabOrder = 4
    end
  end
  object ApplicationEvents: TApplicationEvents
    OnIdle = ApplicationEventsIdle
    Left = 112
    Top = 368
  end
  object ActionList: TActionList
    Left = 16
    Top = 368
    object StartServerAction: TAction
      Caption = 'Start Server'
      OnExecute = StartServerActionExecute
    end
    object StopServerAction: TAction
      Caption = 'Stop Server'
      OnExecute = StopServerActionExecute
    end
    object OpenBrowserAction: TAction
      Caption = 'Open Browser'
      OnExecute = OpenBrowserActionExecute
    end
    object OpenDataFileAction: TAction
      Caption = 'Select POST Field Data File'
      OnExecute = OpenDataFileActionExecute
    end
    object SendRequestAction: TAction
      Caption = 'Send Request'
      OnExecute = SendRequestActionExecute
    end
  end
  object OpenFileDialog: TOpenDialog
    Left = 208
    Top = 368
  end
end
