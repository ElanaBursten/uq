unit DBConnectionDMU;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.MSSQL, Data.DB, FireDAC.Comp.Client,
  FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait, FireDAC.Phys.ODBCBase;

type
  TDBConnectionDM = class(TDataModule)
    FDConnection: TFDConnection;
    ADPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink;
  private
  public
    procedure Connect;
    procedure Disconnect;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

uses
  ConfigU, FDUtils;

procedure TDBConnectionDM.Connect;
begin
  if Config.FDConnectionString = '' then
    raise Exception.Create('Error: FDConnectionString not configured.');
  ConnectFDConnection(FDConnection, Config.FDConnectionString);
end;

procedure TDBConnectionDM.Disconnect;
begin
  FDConnection.Close;
end;

end.
