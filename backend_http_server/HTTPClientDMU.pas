unit HTTPClientDMU;

interface

uses
  System.SysUtils, System.Classes, IdHTTP;

type
  THTTPClientDM = class(TDataModule)
  private
  public
    function SendPostRequest(const URL: string;
      const FieldNames: array of string;
      const FieldValues: array of string): string;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

uses IdURI;

function THTTPClientDM.SendPostRequest(const URL: string;
  const FieldNames: array of string;
  const FieldValues: array of string): string;
var
  Client: TIdHTTP;
  Fields: TStringStream;
  FieldStr: string;
  FirstField: Boolean;
  I: Integer;
begin
  Assert(Length(FieldNames) = Length(FieldValues),
    'Different number of FieldNames and FieldValues');
  Fields := TStringStream.create('');
  Client := TIdHTTP.create(nil);
  try
    FirstField := True;
    for I := 0 to Length(FieldNames) - 1 do begin
      if FieldNames[I] = '' then
        Continue;
      // ParamsEncode doesn't handle '&'
      FieldStr := FieldNames[I] + '=' + StringReplace(
        TIdURI.ParamsEncode(FieldValues[I]), '&', '%26', [rfReplaceAll]);
      if FirstField then
        FirstField := False
      else
        FieldStr := '&' + FieldStr;
      Fields.WriteString(FieldStr);
    end;

    Client.Request.ContentType := 'application/x-www-form-urlencoded';
    try
      Result := Client.Post(URL, Fields);
    except
      on E: EIdHTTPProtocolException do
        Result := E.ErrorMessage;
    end;
  finally
    FreeAndNil(Client);
    FreeAndNil(Fields);
  end;
end;

end.
