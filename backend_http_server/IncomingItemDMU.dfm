object IncomingItemDM: TIncomingItemDM
  OldCreateOrder = False
  Height = 150
  Width = 215
  object ADQuery: TFDQuery
    SQL.Strings = (
      'insert incoming_item (cc_code, item_type, item_text)'
      'values (:cc_code, :item_type, :item_text)')
    Left = 80
    Top = 56
    ParamData = <
      item
        Name = 'CC_CODE'
        ParamType = ptInput
      end
      item
        Name = 'ITEM_TYPE'
        ParamType = ptInput
      end
      item
        Name = 'ITEM_TEXT'
        ParamType = ptInput
      end>
  end
end
