@echo off
rem
rem This test script needs curl (http://curl.haxx.se/download.html)
rem and expects it to be in the c:\curl dir. It also expects the
rem web service to be on port 5055.
rem

SETLOCAL enabledelayedexpansion
echo Add all the sample tickets from test_data...
FOR %%F IN (test_data\*Notice.xml) DO (
  c:\curl\curl --verbose --data-urlencode xmlTicket@"%%F" http://localhost:5055/3003?sendTicket
  if !ERRORLEVEL! neq 0 exit /b !ERRORLEVEL! 
)
ENDLOCAL

SETLOCAL enabledelayedexpansion
echo Add 300 dummy messages...
FOR /L %%I IN (1, 1, 300) DO (
  c:\curl\curl --verbose --data-ascii xmlMessage="<Content><Message>Hello QM web service</Message></Content>" http://localhost:5055/3003?sendMessage
  if !ERRORLEVEL! neq 0 exit /b !ERRORLEVEL! 
)
ENDLOCAL

echo Add an Audit
c:\curl\curl --verbose --data-ascii xmlAudit="<Content><Audit>Just a dummy audit</Audit></Content>" http://localhost:5055/3003?sendAudit
if ERRORLEVEL 1 exit /b 1

echo Send an invalid message
c:\curl\curl --verbose --data-ascii xmlMessage="Hello QMBackendHTTPServer" http://localhost:5055/3003?sendMessage

echo Send an invalid audit
c:\curl\curl --verbose --data-ascii xmlAudit="Just a dummy audit" http://localhost:5055/3003?sendAudit

SETLOCAL enabledelayedexpansion
echo Ask for the WSDL 10 times...
FOR /L %%I IN (1, 1, 10) DO (
  c:\curl\curl -verbose http://localhost:5055/3003?wsdl
  if !ERRORLEVEL! neq 0 exit /b !ERRORLEVEL! 
)
ENDLOCAL

echo Success!
Exit

