unit RequestFieldFrameU;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TRequestFieldFrame = class(TFrame)
    Label1: TLabel;
    Label2: TLabel;
    FieldDataMemo: TMemo;
    LoadFromFileButton: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    FieldNameComboBox: TComboBox;
  private
  public
  end;

implementation

{$R *.dfm}

end.
