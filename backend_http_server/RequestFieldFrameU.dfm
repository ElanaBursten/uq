object RequestFieldFrame: TRequestFieldFrame
  Left = 0
  Top = 0
  Width = 451
  Height = 304
  Align = alClient
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 264
    Width = 451
    Height = 40
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      451
      40)
    object LoadFromFileButton: TButton
      Left = 328
      Top = 6
      Width = 113
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Load Data From File'
      Enabled = False
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 451
    Height = 264
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 1
    DesignSize = (
      451
      264)
    object Label2: TLabel
      Left = 20
      Top = 40
      Width = 48
      Height = 13
      Caption = 'Field Data'
    end
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 52
      Height = 13
      Caption = 'Field Name'
    end
    object FieldDataMemo: TMemo
      Left = 74
      Top = 40
      Width = 367
      Height = 215
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
    end
    object FieldNameComboBox: TComboBox
      Left = 74
      Top = 13
      Width = 145
      Height = 21
      TabOrder = 1
      Items.Strings = (
        ''
        'xmlTicket'
        'xmlMessage'
        'xmlAudit')
    end
  end
end
