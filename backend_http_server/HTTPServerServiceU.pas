unit HTTPServerServiceU;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs, MainDMU;

type
  TQMDataCollectorService = class(TService)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
  private
    FMainDM: TMainDM;
  public
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0); override;
    destructor Destroy; override;
    function GetServiceController: TServiceController; override;
  end;

var
  QMDataCollectorService: TQMDataCollectorService;

implementation

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  QMDataCollectorService.Controller(CtrlCode);
end;

constructor TQMDataCollectorService.CreateNew(AOwner: TComponent;
  Dummy: Integer);
begin
  inherited;
  FMainDM := TMainDM.Create(nil);
end;

destructor TQMDataCollectorService.Destroy;
begin
  FreeAndNil(FMainDM);
  inherited;
end;

function TQMDataCollectorService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TQMDataCollectorService.ServiceStart(Sender: TService;
  var Started: Boolean);
begin
  { This will reinitialize the config. The server shouldn't be running. But use
    RestartServer, instead of StartServer, in case it is. }
  FMainDM.RestartServer;
  Started := FMainDM.ServerActive;
end;

{ There may be a problem when shutting down the server in ServiceStop and
  ServicePause, if there's a request thread running. This will be addressed in
  a new JIRA item. }

procedure TQMDataCollectorService.ServiceStop(Sender: TService;
  var Stopped: Boolean);
begin
  FMainDM.StopServer;
  Stopped := not FMainDM.ServerActive;
end;

procedure TQMDataCollectorService.ServicePause(Sender: TService;
  var Paused: Boolean);
begin
  FMainDM.PauseServer;
  Paused := not FMainDM.ServerActive;
end;

procedure TQMDataCollectorService.ServiceContinue(Sender: TService;
  var Continued: Boolean);
begin
  { Resuming won't reinitialize the config }
  FMainDM.ResumeServer;
  Continued := FMainDM.ServerActive;
end;

end.
