unit ConfigU;

interface

uses
  System.IniFiles;

const
  InvalidPort = -2;

type
  TConfig = class
  private
    FInitialized: Boolean;
    FAllSettingsValid: Boolean;
    FServerPort: Integer;
    FFDConnectionString: string;
    FServerLogFilePath: string;
    FServerInstanceID: Integer;
  public
    function InitializeAndValidate(out ErrorMessage: string): Boolean;
    function ServerPort: Integer;
    function FDConnectionString: string;
    function ServerLogFilePath: string;
    function ServerInstanceID: Integer;
    property Initialized: Boolean read FInitialized;
    property AllSettingsValid: Boolean read FAllSettingsValid;
  end;

var
  Config: TConfig;

implementation

uses
  SysUtils, Classes, OdMiscUtils;

const
  INIFileName = 'QMServer.ini';
  INIDatabaseSection = 'Database';
  INIFDConnectionStringName = 'FDConnectionString';
  INIServerSection = 'QMCollectorService';
  INIServerPortName = 'Port';
  INILogFileSection = 'LogFile';
  INILogFilePathName = 'Path';
  MinPort = 1;
  MaxPort = 65535;
  ConfigNotInitializedMessage = 'Error: Configuration not initialized';

function TConfig.InitializeAndValidate(out ErrorMessage: string): Boolean;
var
  INIFullFileName: string;
  INIFile: TIniFile;

  function SetAndValidateFields(out ErrorMessage: string): Boolean;
  begin
    ErrorMessage := '';

    FServerPort := INIFile.ReadInteger(INIServerSection, INIServerPortName,
      InvalidPort);
    if (FServerPort < MinPort) or (FServerPort > MaxPort) then begin
      ErrorMessage := ErrorMessage + sLineBreak +
        Format('* [%s] %s is missing or invalid', [INIServerSection,
        INIServerPortName]);
      FServerPort := InvalidPort;
    end;

    FFDConnectionString := INIFile.ReadString(INIDatabaseSection,
      INIFDConnectionStringName, '');
    if FFDConnectionString = '' then
      ErrorMessage := ErrorMessage + sLineBreak +
        Format('* [%s] %s is missing', [INIDatabaseSection,
        INIFDConnectionStringName]);

    FServerLogFilePath := INIFile.ReadString(INILogFileSection,
      INILogFilePathName, '');

    Randomize;
    FServerInstanceID := Random(8999) + 1000;

    if ErrorMessage = '' then
      Result := True
    else begin
      ErrorMessage := Format('Error(s) in INI file, %s:', [INIFullFileName]) +
        ErrorMessage;
      Result := False;
    end;
  end;
begin
  Result := False;
  ErrorMessage := '';
  FInitialized := False;
  FAllSettingsValid := False;

  INIFullFileName := ExtractFilePath(ParamStr(0)) + INIFileName;

  INIFile := TIniFile.Create(INIFullFileName);
  try
    if SetAndValidateFields(ErrorMessage) then begin
      Result := True;
      FAllSettingsValid := True;
    end else if not FileExists(INIFullFileName) then
      ErrorMessage := Format('Error: INI file not found: %s.', [INIFullFileName]);
  finally
    FreeAndNil(INIFile);
  end;

  { Some settings may not come from the ini, so set to True, even if ini file
    not found. }
  FInitialized := True;
end;

function TConfig.ServerPort: Integer;
begin
  Assert(FInitialized, ConfigNotInitializedMessage);
  Result := FServerPort;
end;

function TConfig.FDConnectionString: string;
begin
  Assert(FInitialized, ConfigNotInitializedMessage);
  Result := FFDConnectionString;
end;

function TConfig.ServerLogFilePath: string;
begin
  Assert(FInitialized, ConfigNotInitializedMessage);
  Result := FServerLogFilePath;
end;

function TConfig.ServerInstanceID: Integer;
begin
  Assert(FInitialized, ConfigNotInitializedMessage);
  Result := FServerInstanceID;
end;

end.
