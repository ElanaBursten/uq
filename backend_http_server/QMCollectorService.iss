; -- QMCollectorService.iss --
; This installs the QM ticket web service, QMBackendHttpServer.exe
; Use Inno Setup 5.4.2+
#define Version GetEnv("VERSION")
#define MyAppName "Q Manager Collector Service"
#define GitCommitID GetEnv("GIT_COMMIT")
#define GitCommitIDShort copy(GitCommitID,1,7) 
#if GitCommitIDShort == ""
 #define GitCommitIDWithDash= "-DevTest"
#else
 #define GitCommitIDWithDash= "-" + GitCommitIDShort
#endif

[Setup]
AppCopyright=Copyright {#GetDateTimeString('yyyy', '#0', '#0')} UtiliQuest, LLC
AppName={#MyAppName} 
AppVerName={#MyAppName} {#Version} 
UninstallDisplayName = {#MyAppName} {#Version}
Compression=lzma/max
SolidCompression=yes
DefaultDirName={pf}\UtiliQuest\{#MyAppName}
DefaultGroupName={#MyAppName}
AppPublisher=UtiliQuest, LLC
AppPublisherURL=http://www.utiliquest.com/
AppMutex=UtiliQuestCollectorService
AppVersion={#Version}{#GitCommitIDWithDash}
OutputDir=..\build
OutputBaseFilename=QMCollectorServiceSetup-{#Version}{#GitCommitIDWithDash}
DisableProgramGroupPage=yes
PrivilegesRequired=none
OutputManifestFile=QMCollectorServiceSetup-Manifest.txt
SetupLogging=yes
UsePreviousLanguage=no

[Files]
Source: QMCollectorService.exe; DestDir: {app}; Flags: ignoreversion;
Source: ../server/SampleQMServer.ini; DestDir: {app}; DestName: QMServer.ini; Flags: onlyifdoesntexist uninsneveruninstall

;Delphi BPLs
Source: "..\lib\rtl170.bpl";    DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcl170.bpl";    DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclx170.bpl";   DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcldb170.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclie170.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclimg170.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\dbrtl170.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\xmlrtl170.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\AnyDAC_ComI_D17.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\AnyDAC_Comp_D17.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\AnyDAC_Phys_D17.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\AnyDAC_PhysMSSQL_D17.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\AnyDAC_PhysODBC_D17.bpl";   DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\AnyDAC_PhysSQLite_D17.bpl"; DestDir: "{sys}"; Flags: sharedfile

[Run]
Filename: "{app}\QMCollectorService.exe"; Parameters: "/install /silent"; StatusMsg: "Registering Collector service..."; Flags: runhidden;
Filename: "{sys}\net.exe"; Parameters: "start ""Q Manager Collector Service"""; StatusMsg: "Starting Collector service..."; Flags: runhidden;

[UninstallRun]
Filename: "{sys}\net.exe"; Parameters: "stop Q ""Manager Collector Service"""; StatusMsg: "Stopping Collector service..."; Flags: runhidden
Filename: "{app}\QMCollectorService.exe"; Parameters: "/uninstall /silent"; StatusMsg: "Unregistering Collector service..."; Flags: runhidden;
