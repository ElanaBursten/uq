import requests
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('url')
parser.add_argument('data_field_name', choices=['xmlTicket', 'xmlMessage', 'xmlAudit'])
parser.add_argument('data_file_name')
args = parser.parse_args()

try:
    f = open(args.data_file_name, 'rb')
    data_text = f.read()
    f.close
except IOError:
    print 'Error reading data file: ', args.data_file_name
else:
    data = {args.data_field_name: data_text}
    r = requests.post(args.url, data=data)
    print r.text
    