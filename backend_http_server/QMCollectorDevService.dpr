program QMCollectorDevService;

{$R 'QMCollectorService.res' 'QMCollectorService.rc'}
{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

uses
  Vcl.Forms,
  Web.WebReq,
  ConfigU in 'ConfigU.pas',
  DBConnectionDMU in 'DBConnectionDMU.pas' {DBConnectionDM: TDataModule},
  HTTPClientDMU in 'HTTPClientDMU.pas' {HTTPClientDM: TDataModule},
  HTTPServerDMU in 'HTTPServerDMU.pas' {HTTPServerDM: TDataModule},
  IncomingItemDMU in 'IncomingItemDMU.pas' {IncomingItemDM: TDataModule},
  LoggerU in 'LoggerU.pas',
  MainDMU in 'MainDMU.pas' {MainDM: TDataModule},
  WebDispatcherDMU in 'WebDispatcherDMU.pas' {WebDispatcherDM: TWebModule},
  RequestFieldFrameU in 'RequestFieldFrameU.pas' {RequestFieldFrame: TFrame},
  MainFormU in 'MainFormU.pas' {MainForm};

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
