program HoursFix;

{$APPTYPE CONSOLE}

{$R '..\QMIcon.res'}
{$R 'QMVersion.res' '..\QMVersion.rc'}

uses
  SysUtils,
  ActiveX,
  HoursSplitAdjuster in 'HoursSplitAdjuster.pas' {HoursSplitAdjusterDM: TDataModule},
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  HoursCalc in '..\common\HoursCalc.pas',
  HoursDataset in '..\common\HoursDataset.pas',
  OdIsoDates in '..\common\OdIsoDates.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  QMConst in '..\client\QMConst.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  UQDbConfig in '..\common\UQDbConfig.pas';

var
  HoursSplitAdjusterDM: THoursSplitAdjusterDM;

begin
  try
    CoInitialize(nil);
    HoursSplitAdjusterDM := THoursSplitAdjusterDM.Create(nil);
    try
      HoursSplitAdjusterDM.Go;
    finally
      HoursSplitAdjusterDM.Free;
      CoUninitialize;
    end;
  except
    on E: Exception do begin
      Writeln('ERROR:' + E.Message);
    end;
  end;

end.
