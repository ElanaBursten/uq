unit HoursSplitAdjuster;

interface

uses
  SysUtils, Classes, ADODB, DB, HoursCalc, HoursDataset;

type
  EInconsistantDataException = class(Exception);

  THoursSplitAdjusterDM = class(TDataModule)
    Conn: TADOConnection;
    HoursData: TADODataSet;
    UpdateHours: TADOCommand;
    procedure DataModuleDestroy(Sender: TObject);
  private
    FStartDate: TDateTime;
    FEndDate: TDateTime;
    FSaveChanges: Boolean;
    FEmpId: Integer;
    HC: TBaseHoursCalculator;
    TseIds: array[0..6] of Integer;
    FCountCorrect, FCountInCorrect: Integer;
    FCorrections: TList;
    procedure ClearWeek;
    procedure AddRow;
    procedure Process;
    procedure CorrectData(EntryId: Integer; RT, OT, DT, CO: HoursType);
    procedure ApplyCorrections;
  public
    procedure Go;
  end;

implementation

uses
  OdAdoUtils, OdIsoDates, OdMiscUtils;

{$R *.dfm}

type
  TCorrection = class
    EntryID: Integer;
    RT,OT,DT,CO: HoursType;
  end;

{ THoursSplitAdjusterDM }

procedure THoursSplitAdjusterDM.Go;
var
  EmpsProcessed: Integer;
  NextEmpID: Integer;
  EmpLimit: Integer;
begin
  if (ParamCount = 1) and (ParamStr(1) = '/v') then begin
    WriteLn('Version: ' + AppVersionShort);
    Exit;
  end;

  if ParamCount <> 3 then begin
    WriteLn('Usage: [/test|/save] WeekEnding EmpLimit');
    WriteLn('  WeekEnding should be in YYYY-MM-DD format');
    WriteLn('  EmpLimit is the maximum number of employees to look at,');
    WriteLn('    Enter a value (for testing) or 0 for All');
    Exit;
  end;

  if ParamStr(1) = '/test' then
    FSaveChanges := False
  else if ParamStr(1) = '/save' then
    FSaveChanges := True
  else
    raise Exception.Create('First parameter must be /test or /save');

  FEndDate := IsoStrToDate(ParamStr(2));

  if DayOfWeek(FEndDate) <> 7 then
    raise Exception.Create('Ending date must be a Saturday');

  FStartDate := FEndDate - 6;

  EmpLimit := StrToInt(ParamStr(3));

  Writeln('Connecting');
  ConnectAdoConnectionWithIni(Conn, ChangeFileExt(ParamStr(0), '.ini'));

  Writeln('Opening query');
  HoursData.Parameters.ParamValues['StartDate'] := FStartDate;
  HoursData.Parameters.ParamValues['EndDate'] := FEndDate;
  HoursData.Open;

  FCorrections := TList.Create;
  try
    Writeln('Processing');

    if FSaveChanges then
      Writeln('Will post needed changes to database.')
    else
      Writeln('Test Mode - will list needed changes to screen only.');

    EmpsProcessed := 0;
    FEmpId := -999;
    while (not HoursData.EOF) do begin
      NextEmpID := HoursData.FieldByName('work_emp_id').AsInteger;
      if NextEmpID <> FEmpId then begin
        if FEmpId<>-999 then begin
          Process;
          Inc(EmpsProcessed);
        end;

        if (EmpLimit>0) and (EmpsProcessed>=EmpLimit) then
          Break;

        FEmpId := NextEmpID;
        ClearWeek;
      end;

      AddRow;
      HoursData.Next;
    end;
    if HoursData.EOF then
      Process;  // to get the last week of data
    HoursData.Close;

    Writeln('Emps Processed:    ', EmpsProcessed);
    Writeln('Already Correct:   ', FCountCorrect);
    Writeln('Needed Correction: ', FCountInCorrect);

    if FSaveChanges then
      ApplyCorrections;
  finally
    Conn.Close;
  end;
end;

procedure THoursSplitAdjusterDM.AddRow;
var
  WorkingHours: Double;
  CallOutHours: Double;
  DayIndex: Integer;
  WorkDate: TDateTime;
begin
  inherited;
  WorkingHours := GetWorkHours(HoursData);
  CallOutHours := GetCalloutHours(HoursData);

  WorkDate := HoursData.FieldByName('work_date').AsDateTime;
  DayIndex := Round(WorkDate - FStartDate);

  if (DayIndex<0) or (DayIndex>6) then
    raise Exception.Create('Internal error');

  if TseIds[DayIndex] > 0 then begin
    WriteLn(Format('Error, multiple active rows for person %d date %s, ignoring entry_id %d',
      [FEmpID, IsoDateToStr(WorkDate), HoursData.FieldByName('entry_id').AsInteger]));
    Exit;
  end;

  TseIds[DayIndex] := HoursData.FieldByName('entry_id').AsInteger;

  HC.Worked[DayIndex] := WorkingHours;
  HC.Callout[DayIndex] := CallOutHours;

  // load up results with the stored results
  HC.Regular[DayIndex] := HoursData.FieldByName('reg_hours').AsFloat;
  HC.Overtime[DayIndex] := HoursData.FieldByName('ot_hours').AsFloat;
  HC.DoubleTime[DayIndex] := HoursData.FieldByName('dt_hours').AsFloat;
  HC.CalloutCalc[DayIndex] := HoursData.FieldByName('callout_hours').AsFloat;
end;

function Fmt(Hours: Double): string;
const
  FmtString = '%6.2f';
begin
  if Abs(Hours) < 0.01 then
    Result := '      '
  else
    Result := Format(FmtString, [Hours]);
end;

procedure THoursSplitAdjusterDM.ClearWeek;
var
  I: Integer;
  Code: string;
begin
  inherited;
  Code := HoursData.FieldByName('Code').AsString;
  HC := CreateHoursCalculator(Code);
  HC.ClearResults;

  for I := 0 to 6 do begin
    TseIds[I] := 0;
  end;
end;

procedure THoursSplitAdjusterDM.Process;
var
  I: Integer;
  Before: array[0..6] of string;
  After: array[0..6] of string;
  TimesMatch: Boolean;
begin
  // build strinngs of the stored results
  for I := 0 to 6 do begin
    Before[I] := Fmt(HC.Regular[I]) +
                   Fmt(HC.Overtime[I]) +
                   Fmt(HC.DoubleTime[I]) +
                   Fmt(HC.CalloutCalc[I]);
  end;

  try
    HC.Calculate;
  except
    HC.WriteStateToFile('HoursFixErrorState.txt');
    raise;
  end;

  TimesMatch := True;
  // build strings of the calculated results
  for I := 0 to 6 do begin
    After[I] := Fmt(HC.Regular[I]) +
                   Fmt(HC.Overtime[I]) +
                   Fmt(HC.DoubleTime[I]) +
                   Fmt(HC.CalloutCalc[I]);

    if After[I] <> Before[I] then
      TimesMatch := False;
  end;

  if TimesMatch then begin
    Inc(FCountCorrect);
    //WriteLn('Breakdowns verified for emp ', FEmpID);
  end else begin
    Inc(FCountInCorrect);
    WriteLn('Emp:    ', FEmpId, ' - ', HC.Identity);
    WriteLn('        STORED                  CORRECT');
    WriteLn('Day     RT    OT    DT    CO    RT    OT    DT    CO    ');

    for I := 0 to 6 do begin
      WriteLn('RT:  ', Before[I], After[I]);
    end;
    WriteLn;

    for I := 0 to 6 do
      if After[I] <> Before[I] then
        CorrectData(TseIds[I], HC.Regular[I], HC.Overtime[I], HC.DoubleTime[I], HC.CalloutCalc[I]);
    WriteLn;
    WriteLn;
  end;
end;

procedure THoursSplitAdjusterDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(HC);
end;

procedure THoursSplitAdjusterDM.CorrectData(EntryId: Integer; RT, OT, DT,
  CO: HoursType);
var
  C: TCorrection;
begin
  WriteLn(Format('Correct entry %6d to %6.2f%6.2f%6.2f%6.2f', [EntryId, RT, OT, DT, CO]));

  C := TCorrection.Create;
  C.EntryID := EntryId;
  C.RT := RT;
  C.OT := OT;
  C.DT := DT;
  C.CO := CO;
  FCorrections.Add(C);
end;

procedure THoursSplitAdjusterDM.ApplyCorrections;
var
  I: Integer;
  C: TCorrection;
begin
  UpdateHours.Prepared := True;

  WriteLn('Saving to DB');
  Conn.BeginTrans;
  try
    for I := 0 to FCorrections.Count-1 do begin
      WriteLn(I);
      C := TCorrection(FCorrections[I]);

      UpdateHours.Parameters.ParamValues['rt'] := C.RT;
      UpdateHours.Parameters.ParamValues['ot'] := C.OT;
      UpdateHours.Parameters.ParamValues['dt'] := C.DT;
      UpdateHours.Parameters.ParamValues['co'] := C.CO;
      UpdateHours.Parameters.ParamValues['id'] := C.EntryId;
      UpdateHours.Execute;
    end;
    WriteLn('Commiting');
    Conn.CommitTrans;
    WriteLn('Done Saving');
  except
    Conn.RollbackTrans;
    raise;
  end;
end;

end.
