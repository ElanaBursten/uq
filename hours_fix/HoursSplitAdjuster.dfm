object HoursSplitAdjusterDM: THoursSplitAdjusterDM
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 533
  Width = 420
  object Conn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=doggy183;Persist Security Info=True' +
      ';User ID=sa;Initial Catalog=QMTesting;Data Source=10.1.1.175'
    IsolationLevel = ilReadCommitted
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 64
    Top = 24
  end
  object HoursData: TADODataSet
    CacheSize = 500
    Connection = Conn
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select tse.*, reference.code'#13#10'from timesheet_entry tse'#13#10'   left ' +
      'join employee emp on tse.work_emp_id = emp.emp_id'#13#10'   left join ' +
      'reference on reference.ref_id = emp.timerule_id'#13#10'where (tse.stat' +
      'us='#39'ACTIVE'#39' or tse.status='#39'SUBMIT'#39')'#13#10' and tse.work_date between ' +
      ':StartDate and :EndDate'#13#10'order by tse.work_emp_id, tse.work_date' +
      #13#10
    Parameters = <
      item
        Name = 'StartDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'EndDate'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 144
    Top = 24
  end
  object UpdateHours: TADOCommand
    CommandText = 
      'update timesheet_entry'#13#10' set reg_hours=:rt,'#13#10'     ot_hours=:ot,'#13 +
      #10'     dt_hours=:dt,'#13#10'     callout_hours=:co'#13#10' where entry_id=:id' +
      #13#10
    Connection = Conn
    Parameters = <
      item
        Name = 'rt'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'ot'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'dt'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'co'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 5
        Size = 19
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 232
    Top = 24
  end
end
