object MetricsDM: TMetricsDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 150
  Width = 215
  object Conn: TADConnection
    Params.Strings = (
      'Server=localhost'
      'OSAuthent=Yes'
      'Database=QMMetricsTest'
      'LoginTimeout=5'
      'MARS=yes'
      'DriverID=MSSQL')
    FormatOptions.AssignedValues = [fvMapRules]
    FormatOptions.OwnMapRules = True
    FormatOptions.MapRules = <
      item
        SourceDataType = dtDateTimeStamp
        TargetDataType = dtDateTime
      end>
    ResourceOptions.AssignedValues = [rvKeepConnection]
    ResourceOptions.KeepConnection = False
    LoginPrompt = False
    Left = 24
    Top = 16
  end
  object AddMetricData: TADStoredProc
    Connection = Conn
    Left = 128
    Top = 16
  end
  object AddLocate: TADStoredProc
    Connection = Conn
    StoredProcName = 'PRJ_LocateReceived'
    Left = 128
    Top = 80
  end
end
