program QMProjectionMetrics;

{$APPTYPE CONSOLE}

uses
  System.SysUtils,
  MainDMu in 'MainDMu.pas' {MainDM: TDataModule},
  AdUtils in '..\common\AdUtils.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  UQDbConfig in '..\common\UQDbConfig.pas',
  EventSource in '..\server2\EventSource.pas',
  MSSqlEventSource in '..\server2\MSSqlEventSource.pas',
  QMLogic2ServiceLib in '..\server2\gen-delphi\QMLogic2ServiceLib.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  MetricsDMu in 'MetricsDMu.pas' {MetricsDM: TDataModule},
  QMThriftUtils in '..\common\QMThriftUtils.pas',
  ProjMetricsCL in 'ProjMetricsCL.pas',
  EventUtils in '..\server2\EventUtils.pas',
  ConfigDMu in 'ConfigDMu.pas' {ConfigDM: TDataModule};

{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

var
  CLP: TCommandLineProjector;
begin
  CLP := TCommandLineProjector.Create;
  try
    CLP.Exec;
  finally
    FreeAndNil(CLP);
  end;
end.
