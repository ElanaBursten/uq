program QMProjectionAdmin;

{$APPTYPE CONSOLE}

{$R 'QMProjAdminResources.res' 'QMProjAdminResources.rc'}

uses
  System.SysUtils,
  ProjAdminDMu in 'ProjAdminDMu.pas' {ProjAdminDM: TDataModule},
  AdUtils in '..\common\AdUtils.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas';

{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

begin
  try
    Writeln('This utility deletes and refreshes data in the Metrics DB. ');
    if (ParamCount < 1) then
      Writeln('Command Syntax is: QMProjectionAdmin.exe reload ')
    else if SameText(ParamStr(1), 'reload') then begin
      ProjAdminDM := TProjAdminDM.Create(nil);
      try
        ProjAdminDM.ReloadMetricAndHierarchyData;
      finally
        ProjAdminDM.Free;
      end;
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
  Writeln('Press <Enter> key to exit.');
  ConsoleWaitForEnterKey(15, 100);//Wait 15 secs for a response, checking every tenth of a second. If none, then close.
end.
