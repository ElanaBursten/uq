unit ProjMetricsServiceU;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs, ProjMetricsThreadU, ThreadSafeLoggerU,
  uADStanIntf, uADStanOption, uADStanError, uADGUIxIntf, uADPhysIntf,
  uADStanDef, uADPhysManager, uADCompClient;

type
  TQMProjMetricsService = class(TService)
    // Multi-threaded apps and pooled db connections need TADManager
    ADManager: TADManager;
    procedure ServiceShutdown(Sender: TService);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceDestroy(Sender: TObject);
    procedure ServiceCreate(Sender: TObject);
  private
    MaxIdleMinutes: Integer;
    Logger: TThreadSafeLogger;
    WorkerThread: TProjMetricsThread;
    function StopProjectionThread: Boolean;
    function CreateFileLogger: TThreadSafeLogger;
  public
    function GetServiceController: TServiceController; override;
  end;

var
  QMProjMetricsService: TQMProjMetricsService;

implementation

uses
  System.IniFiles, OdMiscUtils, QMConst;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  QMProjMetricsService.Controller(CtrlCode);
end;

function TQMProjMetricsService.CreateFileLogger: TThreadSafeLogger;
var
  LogDir: string;
  LogEnabled: Boolean;
  InstanceID: Integer;
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'QMServer.ini');
  try
    LogDir := AddSlash(GetLocalAppDataFolder) + ApplicationFamilyName + '\logs\';
    LogDir := AddSlash(Ini.ReadString('LogFile', 'Path', LogDir));
    LogEnabled := (Ini.ReadInteger('LogFile', 'LogicLogEnabled', 1) = 1);
    MaxIdleMinutes := Ini.ReadInteger('ProjectionMetrics', 'MaxIdleMinutes', 10);
  finally
    FreeAndNil(Ini);
  end;

  Randomize;
  InstanceID := Random(8999) + 1000;
  ForceDirectories(LogDir);
  Result := TThreadSafeLogger.Create(LogDir, 'QMProjMetricsService.log', InstanceID);
  Result.Enabled := LogEnabled;
end;

function TQMProjMetricsService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TQMProjMetricsService.ServiceCreate(Sender: TObject);
begin
  Logger := nil;
end;

procedure TQMProjMetricsService.ServiceDestroy(Sender: TObject);
begin
  FreeAndNil(Logger);
end;

procedure TQMProjMetricsService.ServiceShutdown(Sender: TService);
begin
  StopProjectionThread;
end;

procedure TQMProjMetricsService.ServiceStart(Sender: TService;
  var Started: Boolean);
begin
  Logger := CreateFileLogger;
  Logger.LogWithTime('Starting ' + Name + ' Version: ' + AppVersionShort);

  WorkerThread := TProjMetricsThread.Create(Logger, MaxIdleMinutes);
  WorkerThread.Start;
  Started := not WorkerThread.Suspended;
  if Started then
    Logger.LogWithTime('Started ' + Name);
end;

procedure TQMProjMetricsService.ServiceStop(Sender: TService;
  var Stopped: Boolean);
begin
  Logger.LogWithTime('Stopping ' + Name);
  Stopped := StopProjectionThread;
  if Stopped then
    Logger.LogWithTime('Stopped ' + Name);
end;

function TQMProjMetricsService.StopProjectionThread: Boolean;
begin
  WorkerThread.Terminate;
  WorkerThread.WaitFor;
  FreeAndNil(WorkerThread);
  Result := True;
end;

end.
