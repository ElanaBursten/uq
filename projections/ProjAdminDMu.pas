unit ProjAdminDmu;

interface

uses
  System.SysUtils, System.Classes, uADStanIntf, uADStanOption, uADStanError,
  uADGUIxIntf, uADPhysIntf, uADStanDef, uADStanPool, uADStanAsync,
  uADPhysManager, Data.DB, uADCompClient, uADPhysMSSQL;

type
  TProjAdminDM = class(TDataModule)
    Conn: TADConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
    IniFileName: string;
    QMDBName: string;
    procedure ConnectToDB;
    procedure LogToCmdLine(Sender: TComponent; const ALogMessage: string);
  public
    procedure ReloadMetricAndHierarchyData(const RowLimit: Integer = 0);
  end;

var
  ProjAdminDM: TProjAdminDM;

implementation

uses UQDbConfig, AdUtils, IniFiles, OdMiscUtils, StrUtils;

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

procedure TProjAdminDM.DataModuleCreate(Sender: TObject);
begin
  IniFileName := ExtractFilePath(ParamStr(0)) + 'QMServer.ini';
  if not FileExists(IniFileName) then
    raise Exception.Create('QMServer.ini is required in the same directory as this utility for DB settings.');
end;

{ TProjAdminDM }

procedure TProjAdminDM.ConnectToDB;
var
  Ini: TIniFile;
  MainDBC: TODBCDatabaseConfig;
  MetricsDBC: TODBCDatabaseConfig;
begin
  MainDBC := nil;
  MetricsDBC := nil;
  Ini := TIniFile.Create(IniFileName);
  try
    // Get the QM db name
    MainDBC := TODBCDatabaseConfig.CreateFromIni(Ini, 'Database', 'MainDB');
    QMDBName := MainDBC.DB;
    // Now get the Metrics db config
    MetricsDBC := TODBCDatabaseConfig.CreateFromIni(Ini, 'MetricsDatabase', 'MetricsDB');
    ConnectADConnectionWithConfig(Conn, MetricsDBC);
  finally
    FreeAndNil(MainDBC);
    FreeAndNil(MetricsDBC);
    FreeAndNil(Ini);
  end;
end;

procedure TProjAdminDM.LogToCmdLine(Sender: TComponent; const ALogMessage: string);
begin
{$IFDEF CONSOLE}
  WriteLn(FormatDateTime('yyyy-mm-dd hh:MM:ss ', Now), ALogMessage);
{$ENDIF}
end;

procedure TProjAdminDM.ReloadMetricAndHierarchyData(const RowLimit: Integer = 0);
const
  ReloadMsg = 'Could not reload Entity and Employee Hierarchy data. ';
  RecCountMsg = 'Table %s contains %d rows';
var
  RecCount: Integer;

  function GetSQLFromResource(const ResName: string): string;
  begin
    Result := GetResourceString(ResName);
    Result := StringReplace(Result, 'TestDB', QMDBName, [rfReplaceAll]);
    Result := StringReplace(Result, 'TOP N', IfThen(RowLimit > 0,
      'TOP ' + IntToStr(RowLimit), ''), [rfReplaceAll]);
  end;

begin
  ConnectToDB;
  Assert(NotEmpty(QMDBName));

  Conn.StartTransaction;
  try
    //Populate hierarchy, hclos, employee, eclos, emp_allowed_node
    LogToCmdLine(Self, 'Refreshing hierarchy');
    Conn.ExecSQL(GetSQLFromResource('ADD_HIERARCHY_SQL'));
    //Populate metric_entity
    LogToCmdLine(Self, 'Refreshing entities');
    Conn.ExecSQL(GetSQLFromResource('ADD_ENTITIES_SQL'));

{$IFDEF CONSOLE}
    RecCount := Conn.ExecSQLScalar('select COUNT(*) from metric_entity');
    LogToCmdLine(Self, Format(RecCountMsg, ['metric_entity', RecCount]));
    RecCount := Conn.ExecSQLScalar('select COUNT(*) from hierarchy');
    LogToCmdLine(Self, Format(RecCountMsg, ['hierarchy', RecCount]));
    RecCount := Conn.ExecSQLScalar('select COUNT(*) from hclos');
    LogToCmdLine(Self, Format(RecCountMsg, ['hclos', RecCount]));
    RecCount := Conn.ExecSQLScalar('select COUNT(*) from employee');
    LogToCmdLine(Self, Format(RecCountMsg, ['employee', RecCount]));
    RecCount := Conn.ExecSQLScalar('select COUNT(*) from eclos');
    LogToCmdLine(Self, Format(RecCountMsg, ['eclos', RecCount]));
    RecCount := Conn.ExecSQLScalar('select COUNT(*) from emp_allowed_node');
    LogToCmdLine(Self, Format(RecCountMsg, ['emp_allowed_node', RecCount]));
{$ENDIF}
    Conn.Commit;
  except
    on E: Exception do begin
      Conn.Rollback;
      E.Message := ReloadMsg + E.Message;
      raise;
    end;
  end;
end;

end.
