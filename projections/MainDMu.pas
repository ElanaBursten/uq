unit MainDMu;

interface

uses
  System.SysUtils, System.Classes, uADStanIntf, uADStanOption, uADStanError,
  uADGUIxIntf, uADPhysIntf, uADStanDef, uADStanPool, uADStanAsync,
  uADPhysManager, Data.DB, uADCompClient, uADPhysMSSQL, uADStanParam,
  uADDatSManager, uADDAptIntf, uADDAptManager, uADCompDataSet, OdDbUtils,
  QMThriftUtils, QMLogic2ServiceLib, MetricsDMu, ConfigDMu;

type
  TServerLogNotifyEvent = procedure (Sender: TComponent; const ALogMessage: string) of object;

  TMainDM = class(TDataModule)
    EventConn: TADConnection;
    EventQ: TADQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    IniFileName: string;
    MetricsDM: TMetricsDM;
    ConfigDM: TConfigDM;
    FOnLogMessage: TServerLogNotifyEvent;
    procedure ConnectToDB(DB: TADConnection; DBName: string='Database');
    procedure DisconnectDB(DB: TADConnection);
    procedure ProcessNewEvents;
    function GetMetricsDM: TMetricsDM;
    procedure WaitForNewEvents;
  public
    EventAddedCount: Integer;
    procedure ProjectEvents;
    procedure AddLogMessage(const AMessage: string; const AIsSuccess: Boolean=True);
    procedure SetStatus(const Status: string);
    function GetConfigDM: TConfigDM;

    property OnLogMessage: TServerLogNotifyEvent read FOnLogMessage write FOnLogMessage;
  end;

var
  MainDM: TMainDM;

implementation

uses IniFiles, StrUtils, AdUtils, OdMiscUtils, MSSQLEventSource, EventSource;

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

{ TMainDM }

procedure TMainDM.ProjectEvents;
begin
  GetMetricsDM;
  SetStatus('STARTING');
  ConnectToDB(EventConn);
  try
    ProcessNewEvents;
    SetStatus('INACTIVE');
  finally
    DisconnectDB(MetricsDM.Conn);
    DisconnectDB(EventConn);
  end;
end;

procedure TMainDM.WaitForNewEvents;
const
  // Waits up to 60 seconds for new notifications to arrive
  SQL = 'WAITFOR (RECEIVE CONVERT(VARCHAR(100), message_body) new_event_id ' +
    'FROM NewEventReceiveQueue), TIMEOUT 60000';
begin
  AddLogMessage('Waiting up to 60 seconds for new events...');
  EventQ.Close;
  EventQ.SQL.Text := SQL;
  EventQ.Open;
end;

procedure TMainDM.ProcessNewEvents;
var
  EventStore: IEventStore;
  Events: IEventList;
  LastEventID: string;
begin
  EventAddedCount := 0;
  EventStore := TMSSQLEventStore.Create(nil, EventConn);
  repeat
    SetStatus('ACTIVE');
    EventConn.StartTransaction;
    try
      try
        LastEventID := GetMetricsDM.GetLastProjectedEventID;
        Events := EventStore.GetEventsSince(LastEventID, 3000);
        if Events.Items.Count > 0 then begin
          AddLogMessage('Found ' + IntToStr(Events.Items.Count) + ' to process');
          GetMetricsDM.HandleEvents(Events, EventStore);
          EventAddedCount := EventAddedCount + Events.Items.Count;
        end;
      except
        on E: Exception do begin
          EventConn.Rollback;
          SetStatus('ERROR ' + E.ClassName + '. Check the log for details.');
          raise;
        end;
      end;
    finally
      EventConn.Commit;
      SetStatus('WAITING FOR NEW EVENTS');
      WaitForNewEvents;
      if not EventQ.HasRecords then
        AddLogMessage('No pending events');
    end;
  until not EventQ.HasRecords;
end;

procedure TMainDM.AddLogMessage(const AMessage: string; const AIsSuccess: Boolean);
var
  Mess: string;
begin
  Mess := IfThen(not AIsSuccess, '[Error] ', '') + AMessage;
  if Assigned(FOnLogMessage) then
    FOnLogMessage(Self, Mess);
end;

procedure TMainDM.ConnectToDB(DB: TADConnection; DBName: string);
var
  Ini: TIniFile;
begin
  Assert(Assigned(DB), 'DB must be assigned.');
  Ini := TIniFile.Create(IniFileName);
  try
    if IsEmpty(DBName) then
      DBName := 'Database';
    ConnectADConnectionWithIni(DB, Ini, DBName);
  finally
    FreeAndNil(Ini);
  end;
end;

procedure TMainDM.DataModuleCreate(Sender: TObject);
begin
  IniFileName := ExtractFilePath(ParamStr(0)) + 'QMServer.ini';
end;

procedure TMainDM.DisconnectDB(DB: TADConnection);
begin
  DB.Close;
end;

function TMainDM.GetMetricsDM: TMetricsDM;
begin
  if not Assigned(MetricsDM) then
    MetricsDM := TMetricsDM.Create(Self);
  if not MetricsDM.Conn.Connected then
    ConnectToDB(MetricsDM.Conn, 'MetricsDatabase');
  Result := MetricsDM;
end;

function TMainDM.GetConfigDM: TConfigDM;
begin
  if not Assigned(ConfigDM) then
    ConfigDM := TConfigDM.Create(Self, MetricsDM.Conn);
  Result := ConfigDM;
end;

procedure TMainDM.SetStatus(const Status: string);
begin
  GetConfigDM.ProjectorStatus := Status;
end;

end.
