object ProjAdminDM: TProjAdminDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 365
  Width = 499
  object Conn: TADConnection
    Params.Strings = (
      'Server=localhost'
      'OSAuthent=Yes'
      'Database=QMMetricsTest'
      'LoginTimeout=5'
      'MARS=yes'
      'DriverID=MSSQL')
    FormatOptions.AssignedValues = [fvMapRules]
    FormatOptions.OwnMapRules = True
    FormatOptions.MapRules = <
      item
        SourceDataType = dtDateTimeStamp
        TargetDataType = dtDateTime
      end>
    ResourceOptions.AssignedValues = [rvKeepConnection]
    ResourceOptions.KeepConnection = False
    LoginPrompt = False
    Left = 24
    Top = 16
  end
end
