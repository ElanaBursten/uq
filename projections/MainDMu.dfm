object MainDM: TMainDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 194
  Width = 215
  object EventConn: TADConnection
    Params.Strings = (
      'Server=localhost'
      'OSAuthent=Yes'
      'Database=TestDB'
      'LoginTimeout=5'
      'MARS=yes'
      'DriverID=MSSQL')
    FormatOptions.AssignedValues = [fvMapRules]
    FormatOptions.OwnMapRules = True
    FormatOptions.MapRules = <
      item
        SourceDataType = dtDateTimeStamp
        TargetDataType = dtDateTime
      end>
    ResourceOptions.AssignedValues = [rvKeepConnection]
    ResourceOptions.KeepConnection = False
    LoginPrompt = False
    Left = 32
    Top = 16
  end
  object EventQ: TADQuery
    Connection = EventConn
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate]
    UpdateOptions.EnableDelete = False
    UpdateOptions.EnableInsert = False
    UpdateOptions.EnableUpdate = False
    Left = 32
    Top = 80
  end
end
