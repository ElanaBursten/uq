program QMProjectionMetricsService;

uses
  Vcl.SvcMgr,
  ProjMetricsServiceU in 'ProjMetricsServiceU.pas' {QMProjMetricsService: TService},
  ProjMetricsThreadU in 'ProjMetricsThreadU.pas',
  QMLogic2ServiceLib in '..\server2\gen-delphi\QMLogic2ServiceLib.pas',
  EventSource in '..\server2\EventSource.pas',
  MSSqlEventSource in '..\server2\MSSqlEventSource.pas',
  MainDMu in 'MainDMu.pas' {MainDM: TDataModule},
  MetricsDMu in 'MetricsDMu.pas' {MetricsDM: TDataModule},
  QMConst in '..\client\QMConst.pas',
  EventUtils in '..\server2\EventUtils.pas';

{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

begin
  // Windows 2003 Server requires StartServiceCtrlDispatcher to be
  // called before CoRegisterClassObject, which can be called indirectly
  // by Application.Initialize. TServiceApplication.DelayInitialize allows
  // Application.Initialize to be called from TService.Main (after
  // StartServiceCtrlDispatcher has been called).
  //
  // Delayed initialization of the Application object may affect
  // events which then occur prior to initialization, such as
  // TService.OnCreate. It is only recommended if the ServiceApplication
  // registers a class object with OLE and is intended for use with
  // Windows 2003 Server.
  //
  // Application.DelayInitialize := True;
  //
  if not Application.DelayInitialize or Application.Installing then
    Application.Initialize;
  Application.CreateForm(TQMProjMetricsService, QMProjMetricsService);
  Application.Run;
end.
