
-- NOTE: THIS IS TEMPORARY CODE THAT IS ONLY USED UNTIL EMPLOYEE EVENTS ARE COMPLETED

set nocount on
-- The columns of this intermediate table must match those returned from QM's hier_display3 sp
declare @hier table (
  tname varchar(50) not null,
  emp_tree_id varchar(15) not null,
  emp_id integer not null,
  short_name varchar(30) not null,
  emp_number varchar(15) null, 
  node_name varchar(50) not null,
  type_id integer not null,
  report_level integer not null,
  report_to varchar(10) null, 
  sort integer not null,
  under_me bit not null,
  is_manager bit not null,
  work_status_now varchar(3) not null,
  contact_phone varchar(20) null 
)

declare @dist int
declare @hier_root_id varchar(10) = '2676'

-- Reload emp hierarchy from QM's hier_display3 stored proc results
insert into @hier exec TestDB..hier_display3 @hier_root_id, 0
delete from hierarchy
insert hierarchy
select TOP N
  convert(integer, emp_tree_id) as node_id,
  short_name as node_name,
  case
    when report_to is null or report_to='NULL' then 0
    else convert(integer, report_to)
  end as report_to_node_id,
  convert(integer, report_level) as [depth]
from @hier
where is_manager = 1

-- Recreate transitive closure on the hierarchy
delete from hclos
insert into hclos
  select node_id, node_id, 0 from hierarchy

set @dist = 1
while @dist < 20 begin
  insert into hclos
  select uc.pid, u.node_id, @dist
  from hclos uc, hierarchy u
  where uc.cid = u.report_to_node_id
    and uc.distance = @dist - 1
  set @dist = @dist + 1
end

-- bring in employees
delete from employee
insert employee
select TOP N
  convert(integer, emp_id) emp_id,
  short_name,
  report_to
from TestDB..employee
where type_id is not null
 and active = 1

-- Recreate transitive closure on employees
delete from eclos
insert into eclos
  select emp_id, emp_id, 0 from employee

set @dist = 1
while @dist < 20 begin
  insert into eclos
  select uc.pid, u.emp_id, @dist
    from eclos uc, employee u
    where uc.cid = u.report_to
      and uc.distance = @dist - 1
  set @dist = @dist + 1
end

-- Recreate allowed employee node access (based on the TicketManagement right)
delete from emp_allowed_node
declare @TMRight varchar(30) = 'TicketManagement'
declare @EmpID int
declare @MgrLimit varchar(200)
declare @Results table (emp_id int not null, mgr_id int not null)

declare TMEmpsCursor cursor for
select e.emp_id,
  case when coalesce(TestDB.dbo.get_emp_right_limitation(e.emp_id, @TMRight), '') in ('', '-') then convert(varchar(200), e.emp_id)
  else TestDB.dbo.get_emp_right_limitation(e.emp_id, @TMRight) end allowed_hier_nodes
from employee e
where TestDB.dbo.emp_has_right(e.emp_id,
    (select right_id
    from TestDB..right_definition
    where entity_data=@TMRight)) = 1

OPEN TMEmpsCursor
FETCH NEXT FROM TMEmpsCursor into @EmpID, @MgrLimit
WHILE @@FETCH_STATUS = 0
BEGIN
  -- get the managers each employee is allowed to access
  delete from @hier
  insert into @hier exec TestDB..hier_display3 @MgrLimit, 0
  insert emp_allowed_node (emp_id, node_id, distance, can_manage)
  select
    @EmpID,
    convert(integer, emp_tree_id) as node_id,
    convert(integer, report_level) as distance,
    under_me as can_manage
  from @hier
  where is_manager = 1

  -- now add the children of the allowed managers
  insert into emp_allowed_node
    select distinct @EmpID,
      e.emp_id as node_id,
      ea.distance + 1,
      1 as can_manage
    from emp_allowed_node ea
    inner join employee e on e.report_to = ea.node_id
    where ea.emp_id = @EmpID
      and ea.can_manage = 1
      and e.emp_id not in (select node_id
      from emp_allowed_node
      where emp_id = @EmpID)
  
  FETCH NEXT FROM TMEmpsCursor into @EmpId, @MgrLimit
END

CLOSE TMEmpsCursor
DEALLOCATE TMEmpsCursor
