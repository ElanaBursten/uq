unit MetricsDMu;

interface

uses
  System.SysUtils, System.Classes, uADStanIntf, uADStanOption, uADStanError,
  uADGUIxIntf, uADPhysIntf, uADStanDef, uADStanPool, uADStanAsync,
  uADPhysManager, Data.DB, uADCompClient, uADStanParam, uADDatSManager,
  uADDAptIntf, uADDAptManager, uADCompDataSet, QMLogic2ServiceLib, QMThriftUtils,
  EventSource, Thrift.Protocol, Generics.Collections, Rtti,
  System.TypInfo, Variants;

type
  EProjectionError = class(Exception);

  TEventHandler = class(TObject)
  private
    FAfterSetParams: TNotifyEvent;
    function GetStoredProcName: string;
  public
    EventName: string;
    constructor Create(Name: string; ExtraParamSetter: TNotifyEvent); overload;
    procedure Execute(SP: TADStoredProc; Event: IEvent; EventObj: IBase);
    property StoredProcName: string read GetStoredProcName;
    property AfterSetParams: TNotifyEvent read FAfterSetParams write FAfterSetParams;
  end;

  TMetricsDM = class(TDataModule)
    Conn: TADConnection;
    AddMetricData: TADStoredProc;
    AddLocate: TADStoredProc;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    EventHandlers: TObjectDictionary<string,TEventHandler>;
    FEventStore: IEventStore;
    procedure RegisterEventHandlers;
    procedure ProjectEvent(Event: IEvent);
    procedure HandleEvent(Event: IEvent);
    function GetTicketID(Event: IEvent): Integer;
    procedure SetTicketReceivedParams(Sender: TObject);
    procedure SetTicketScheduledParams(Sender: TObject);
  public
    function GetLastProjectedEventID: string;
    procedure HandleEvents(Events: IEventList; EventStore: IEventStore);
  end;

implementation

uses
  AdUtils, OdMiscUtils, MainDMu, EventUtils, OdIsoDates;

const
  PROJ_SP_PREFIX = 'PRJ_';

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TMetricsDM }

procedure TMetricsDM.DataModuleCreate(Sender: TObject);
begin
  SetConnections(Conn, Self);
end;

procedure TMetricsDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(EventHandlers);
end;

procedure TMetricsDM.RegisterEventHandlers;
// Populate a list of the names of all the events we know how to handle.
// We can project any event from IEvent Thrift event union that has a stored
// procedure in the Metrics database named like PRJ_[EventName]. The param names
// for the stored procedure must match the property names on the Event object for
// automatic value mapping.

  function EventHasProjectionSP(const EventName: string): Boolean;
  const
    SQL = 'select count(*) N from INFORMATION_SCHEMA.ROUTINES ' +
      'where routine_name = ';
  begin
    Result := Conn.ExecSQLScalar(SQL + QuotedStr(PROJ_SP_PREFIX + EventName)) > 0;
  end;

  // Assign a optional proc to set extra param values
  function GetExtraParamSetter(const EventName: string): TNotifyEvent;
  begin
    Result := nil;
    if EventName = 'TicketReceived' then
      Result := SetTicketReceivedParams
    else if EventName = 'TicketScheduled' then
      Result := SetTicketScheduledParams;
  end;

var
  CTX: TRttiContext;
  RT: TRttiType;
  PropArray: TArray<TRttiProperty>;
  Prop: TRttiProperty;
begin
  FreeAndNil(EventHandlers);
  EventHandlers := TObjectDictionary<string,TEventHandler>.Create;
  RT := CTX.GetType(TEventUnionImpl.ClassInfo);
  PropArray := RT.GetProperties;
  for Prop in PropArray do begin
    if IsValueProperty(Prop, TEventUnionImpl) then begin
      if EventHasProjectionSP(Prop.Name) then
        EventHandlers.Add(Prop.Name, TEventHandler.Create(Prop.Name,
          GetExtraParamSetter(Prop.Name)));
    end;
  end;
end;

procedure TMetricsDM.HandleEvents(Events: IEventList; EventStore: IEventStore);
var
  Ev: IEvent;
  I: Integer;
begin
  FEventStore := EventStore;
  MainDM.AddLogMessage('Register Event Handlers');
  MainDM.SetStatus('PROJECTING EVENTS');
  RegisterEventHandlers;
  for I := 0 to Events.Items.Count - 1 do begin // the order matters, so don't use for..in
    Ev := Events.Items[I];
    HandleEvent(Ev);
  end;
end;

procedure TMetricsDM.HandleEvent(Event: IEvent);
begin
  Conn.StartTransaction;
  try
    ProjectEvent(Event);
    MainDM.GetConfigDM.LastProjectedEventID := Event.EventID;
    Conn.Commit;
  except
    on E: Exception do begin
      Conn.Rollback;
      raise;
    end;
  end;
end;

procedure TMetricsDM.ProjectEvent(Event: IEvent);
var
  Handler: TEventHandler;
  EventTypeName: string;
  EventType: IBase;
begin
  EventType := GetEventType(Event, EventTypeName);
  if EventHandlers.TryGetValue(EventTypeName, Handler) then begin
    MainDM.AddLogMessage('Projecting ' + EventTypeName + ' event ' + Event.EventID);
    Handler.Execute(AddMetricData, Event, EventType);
  end else
    MainDM.AddLogMessage('Unhandled ' + EventTypeName + ' event ' + Event.EventID);
end;

function TMetricsDM.GetLastProjectedEventID: string;
begin
  Result := MainDM.GetConfigDM.LastProjectedEventID;
end;

function TMetricsDM.GetTicketID(Event: IEvent): Integer;
begin
  Assert(Event.AggregateKey.AggrType = TAggregateType.atTicket, 'Expected a Ticket event.');
  Result := Event.AggregateKey.AggrID;
end;

procedure TMetricsDM.SetTicketReceivedParams(Sender: TObject);
var
  Ev: IEvent;
  Locate: ILocateSummary;
begin
  Assert(Sender is TEventImpl, 'Expected a TEventImpl object');
  Ev := TEventImpl(Sender);
  Assert(Ev.Union.__isset_TicketReceived, 'Only TicketReceived events are supported.');
  AddMetricData.ParamByName('@TicketID').AsInteger := GetTicketID(Ev);

  // Add all the locates for the ticket
  AddLocate.Prepare;
  for Locate in Ev.Union.TicketReceived.LocateList do begin
    AssignParamsFromThriftObj(AddLocate.Params, Ev.Union.TicketReceived);
    AddLocate.ParamByName('@TicketID').AsInteger := GetTicketID(Ev);
    AddLocate.ParamByName('@LocateID').AsInteger := Locate.ID;
    AddLocate.ParamByName('@ClientID').AsInteger := Locate.ClientID;
    AddLocate.ParamByName('@ClientCode').AsString := Locate.ClientCode;
    AddLocate.ParamByName('@ClientName').AsString := Locate.ClientName;
    AddLocate.ParamByName('@Status').AsString := Locate.Status;
    AddLocate.ParamByName('@Closed').AsBoolean := Locate.Closed;
    if Locate.__isset_StatusDate then
      AddLocate.ParamByName('@StatusDate').AsDateTime := IsoStrToDateTime(Locate.StatusDate)
    else
      AddLocate.ParamByName('@StatusDate').Value := Null;
    AddLocate.ExecProc;
  end;
end;

procedure TMetricsDM.SetTicketScheduledParams(Sender: TObject);
var
  Ev: IEvent;
begin
  Assert(Sender is TEventImpl, 'Expected a TEventImpl object');
  Ev := TEventImpl(Sender);
  Assert(Ev.Union.__isset_TicketScheduled, 'Only TicketScheduled events are supported.');
  AddMetricData.ParamByName('@TicketID').AsInteger := GetTicketID(Ev);
end;

{ TEventHandler }

constructor TEventHandler.Create(Name: string; ExtraParamSetter: TNotifyEvent);
begin
  EventName := Name;
  AfterSetParams := ExtraParamSetter;
end;

function TEventHandler.GetStoredProcName: string;
begin
  Result := PROJ_SP_PREFIX + EventName;
end;

procedure TEventHandler.Execute(SP: TADStoredProc; Event: IEvent; EventObj: IBase);
begin
  SP.StoredProcName := StoredProcName;
  SP.Prepare;
  AssignParamsFromThriftObj(SP.Params, EventObj);
  if Assigned(AfterSetParams) then
    AfterSetParams(TEventImpl(Event));
  SP.Execute;
end;

end.
