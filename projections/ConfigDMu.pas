unit ConfigDMu;

interface

uses
  System.SysUtils, System.Classes, uADCompClient, uADStanIntf, uADStanOption,
  uADStanParam, uADStanError, uADDatSManager, uADPhysIntf, uADDAptIntf,
  uADStanAsync, uADDAptManager, Data.DB, uADCompDataSet, AdUtils;

type
  TConfigDM = class(TDataModule)
    ConfigData: TADQuery;
  private
    FLastProjectedEventID: string;
    FProjectorStatus: string;
    function GetLastProjectedEvent: string;
    procedure SetLastProjectedEventID(const Value: string);
    function GetProjectorStatus: string;
    procedure SetProjectorStatus(const Value: string);
    procedure SaveData(const Name, Value: string);
    function ReadData(const Name: string): string;
  public
    constructor Create(Owner: TComponent; Conn: TADConnection); reintroduce; virtual;

    property LastProjectedEventID: string read GetLastProjectedEvent write SetLastProjectedEventID;
    property ProjectorStatus: string read GetProjectorStatus write SetProjectorStatus;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TConfigDM }
const
  ConfigNameLastEventID = 'LastProjectedEventID';
  ConfigNameProjStatus  = 'ProjectorStatus';

constructor TConfigDM.Create(Owner: TComponent; Conn: TADConnection);
begin
  inherited Create(Owner);
  Assert(Assigned(Conn), 'Connection is undefined.');
  SetConnections(Conn, Self);
end;

function TConfigDM.GetLastProjectedEvent: string;
begin
  try
    Result := ReadData(ConfigNameLastEventID);
    FLastProjectedEventID := Result;
  finally
    ConfigData.Close;
  end;
end;

procedure TConfigDM.SetLastProjectedEventID(const Value: string);
begin
  if Value <> FLastProjectedEventID then begin
    SaveData(ConfigNameLastEventID, Value);
    FLastProjectedEventID := Value;
  end;
end;

procedure TConfigDM.SetProjectorStatus(const Value: string);
begin
  if Value <> FProjectorStatus then begin
    SaveData(ConfigNameProjStatus, Value);
    FProjectorStatus := Value;
  end;
end;

function TConfigDM.GetProjectorStatus: string;
begin
  try
    Result := ReadData(ConfigNameProjStatus);
    FProjectorStatus := Result;
  finally
    ConfigData.Close;
  end;
end;

function TConfigDM.ReadData(const Name: string): string;
begin
  ConfigData.SQL.Text := 'select * from configuration_data where name=' + QuotedStr(Name);
  ConfigData.Open;
  if ConfigData.Eof then
    Result := ''
  else
    Result := ConfigData.FieldByName('value').AsString;
end;

procedure TConfigDM.SaveData(const Name, Value: string);
begin
  ReadData(Name);
  try
    if ConfigData.Eof then begin
      ConfigData.Insert;
      ConfigData.FieldByName('name').AsString := Name;
    end else
      ConfigData.Edit;
    ConfigData.FieldByName('value').AsString := Value;
    ConfigData.FieldByName('modified_date').AsDateTime := Now;
    ConfigData.Post;
  finally
    ConfigData.Close;
  end;
end;

end.
