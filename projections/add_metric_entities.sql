
-- NOTE: THIS IS TEMPORARY CODE THAT IS ONLY USED UNTIL EMPLOYEE AND CALL CENTER EVENTS ARE COMPLETED

-- Populate entities from Call Centers in the QM OLTP db
insert into metric_entity
  select TOP N 'CC' as ent_cat_code,
    'Call Center' as ent_cat_name,
    cc_code as ent_code,
    cc_name as ent_name
  from TestDB..call_center
  where active = 1
    and cc_code not in (
      select ent_code from metric_entity
      where ent_cat_code = 'CC')

-- Populate entities from Employees
insert into metric_entity
  select TOP N 'EMP' as ent_cat_code,
    'Employee' as ent_cat_name,
    convert(varchar(20), employee.emp_id) as ent_code,
    employee.short_name as ent_name
  from employee
  where short_name is not null
    and convert(varchar(20), employee.emp_id) not in (
      select ent_code from metric_entity
      where ent_cat_code = 'EMP')
  order by 2
