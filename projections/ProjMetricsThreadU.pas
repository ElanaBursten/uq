unit ProjMetricsThreadU;

interface

uses
  System.SysUtils, System.Classes, MainDMu, ThreadSafeLoggerU;

type
  TProjMetricsThread = class(TThread)
  private
    FileLogger: TThreadSafeLogger;
    MaxIdleMS: Integer;
    procedure LogToFile(Sender: TComponent; const ALogMessage: string);
    procedure WaitAWhile(const TimesPaused: Integer);
  protected
    procedure Execute; override;
  public
    constructor Create(ALogger: TThreadSafeLogger; const AMaxIdleMinutes: Integer); overload;
  end;

implementation

uses
  System.DateUtils, System.Math;

{ TProjMetricsThread }

constructor TProjMetricsThread.Create(ALogger: TThreadSafeLogger; const AMaxIdleMinutes: Integer);
begin
  Assert(Assigned(ALogger), 'No logger assigned.');
  Assert(AMaxIdleMinutes > 0, 'MaxIdleMinutes must be > 0');

  inherited Create(True); // start suspended
  FreeOnTerminate := False;
  FileLogger := ALogger;
  MaxIdleMS := AMaxIdleMinutes * SecsPerMin * 1000;
end;

procedure TProjMetricsThread.WaitAWhile(const TimesPaused: Integer);
var
  TotalWaitTime: Integer;
  StartTime: TDateTime;
begin
  TotalWaitTime := Min(MaxIdleMS,
    (Trunc(Power(2, TimesPaused) - 1) * (Random(1000) + 1000) +
    (TimesPaused * 14000)));
  StartTime := Now;
  repeat
    Sleep(100); // only 0.1 sec sleep to keep the service responsive
  until Terminated or (MilliSecondsBetween(Now, StartTime) > TotalWaitTime);
end;

procedure TProjMetricsThread.Execute;
var
  TimesPaused: Integer;
begin
  TimesPaused := 0;
  while not Terminated do begin
    try
      try
        MainDM := TMainDM.Create(nil);
        MainDM.OnLogMessage := LogToFile;
        MainDM.ProjectEvents;
        if MainDM.EventAddedCount > 0 then
          TimesPaused := 0;
      except
        on E: Exception do
          LogToFile(nil, E.ClassName + ': ' + E.Message);
      end;
    finally
      FreeAndNil(MainDM);
    end;
    Inc(TimesPaused);
    WaitAWhile(TimesPaused);
  end;
end;

procedure TProjMetricsThread.LogToFile(Sender: TComponent;
  const ALogMessage: string);
begin
  Assert(Assigned(FileLogger), 'No logger assigned.');
  FileLogger.LogWithTime(ALogMessage);
end;

end.
