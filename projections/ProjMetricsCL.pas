unit ProjMetricsCL;

interface

uses
  SysUtils, Classes, MainDMu;

type
  TCommandLineProjector = class(TObject)
  private
    procedure LogToCmdLine(Sender: TComponent; const ALogMessage: string);
  public
    procedure Exec;
  end;

implementation

{ TCommandLineProjector }

procedure TCommandLineProjector.Exec;
begin
  try
    try
      MainDM := TMainDM.Create(nil);
      MainDM.OnLogMessage := LogToCmdLine;
      MainDM.ProjectEvents;
    except
      on E: Exception do
        LogToCmdLine(nil, E.ClassName + ': ' + E.Message);
    end;
  finally
    MainDM.Free;
  end;
end;

procedure TCommandLineProjector.LogToCmdLine(Sender: TComponent; const ALogMessage: string);
begin
  WriteLn(FormatDateTime('yyyy-mm-dd hh:MM:ss ', Now), ALogMessage);
end;

end.
