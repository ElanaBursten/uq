unit QueryDMu;

interface

uses
  Windows, Messages, SysUtils, Classes, Db, DBISAMTb, ADODB, Dialogs, OdAdoUtils;

type
  TQueryDM = class(TDataModule)
    Database: TDBISAMDatabase;
    Table: TDBISAMTable;
    SyncStatus: TDBISAMTable;
    Conn: TADOConnection;
    Query: TADODataSet;
    EmpHierarchy: TDBISAMTable;
  private
    FDir: string;
    procedure DeleteMatchingWildcard(const Wildcard: string);
    procedure CreateSyncStatusTable;
    procedure CreateEmpHierarchyTable;
    procedure Process(const LocalTableName, PrimaryKey, SQL: string);
    procedure DeleteLocalTables;
    procedure Generate(Dir: string);
    procedure AddTable(const TableName, PrimaryKey: string);
  public
    procedure Go;
    procedure SetDataDirectory(Dir: string);
  end;

var
  QueryDM: TQueryDM;

implementation

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// NOTE: IFDEFS here are temporary only until we convert to
//   BDS 2006.  The code is duplicated to make diffs trivial
//   and that ensures Delphi 6 functionality does not change.
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

{$R *.DFM}

uses IniFiles, OdDBISAMXml, OdDBISAMjson, LocalDataDef;

{ TQueryDM }

type
  TDestinationFormat = (fmt_xml, fmt_json);

function DestFile(const aDestFmt: TDestinationFormat): string;
begin
  case aDestFmt of
    fmt_xml : Result := '..\client\ClientSchema.xml';
    fmt_json: Result := '..\client\ClientSchema.json';
  end;
  if ParamCount >= 1 then
    Result := ParamStr(1);
end;

procedure TQueryDM.Go;
begin   //Provider=SQLNCLI11.1;Integrated Security=SSPI;Persist Security Info=False;User ID="";Initial Catalog=QM;Data Source=DYBOC-DQMGAS01\QM_GAS;Initial File Name="";Server SPN=""
  try
    Generate('..\client\data');

    WriteLn(Output, 'Writing ClientSchema.xml');
    SaveDBISAMSchemaToXML(Database, DestFile(fmt_xml));

    WriteLn(Output, 'Writing ClientSchema.json');
    SaveDBISAMSchemaToJSON(Database, DestFile(fmt_json));

    Generate('..\tests\data1');
    Generate('..\tests\data2');
    Generate('..\tests\data3');
    Generate('..\tests\data4');

    WriteLn(Output, 'Complete!');
    ExitCode := 0;  // Build can continue
  except
    on E: Exception do begin
      ExitCode := 2;
      WriteLn(Output, 'Fatal Error: ' + E.Message);
      Sleep(2000);
    end;
  end;
end;

procedure TQueryDM.CreateSyncStatusTable;
begin
  with SyncStatus do begin
    Close;
    Exclusive := True;
    with FieldDefs do begin
      Clear;
      Add('TableName',    ftString,  25,  True);
      Add('PrimaryKey',   ftString,  45,  False);
      Add('LastSyncDown', ftString,  25,  False);
    end;
    with IndexDefs do begin
      Clear;
      Add('', 'TableName', [ixPrimary, ixUnique]);
    end;
    CreateTable;
  end;
end;

procedure TQueryDM.CreateEmpHierarchyTable;
begin
  with EmpHierarchy do begin
    Exclusive := True;
    with FieldDefs do begin
      Clear;
      Add('emp_id',  ftInteger, 0, True);
      Add('lft',     ftInteger, 0, True);
      Add('rght',    ftInteger, 0, True);
      Add('depth',   ftInteger, 0, True);
      Add('pc_code', ftString, 15, False);
    end;
    with IndexDefs do begin
      Clear;
      Add('', 'emp_id', [ixPrimary, ixUnique]);
      Add('lft_idx', 'lft', [ixUnique]);
    end;
    CreateTable;
  end;
end;

procedure TQueryDM.Generate(Dir: string);
var
  SettingsFile: string;
  i: Integer;
  Row: TSyncStatusRow;
begin
  SetDataDirectory(Dir);

//  SettingsFile := ChangeFileExt(ParamStr(0), '.ini');
//
//  ConnectAdoConnectionWithIni(Conn, SettingsFile);
  DeleteLocalTables;

  // TODO: Create a development DB with some sample records?
  CreateSyncStatusTable;
  SyncStatus.Open;

  CreateEmpHierarchyTable;
  // The SyncStatusRows and the local indexes are defined in LocalDataDef.pas
  for i := Low(SyncStatusRows) to High(SyncStatusRows) do begin
    Row := SyncStatusRows[i];
    if Trim(Row.CustomSQL) = '' then
      AddTable(Row.TableName, Row.PrimaryKey)
    else begin
      SyncStatus.AppendRecord([Row.TableName, Row.PrimaryKey]);
      Process(Row.TableName, Row.PrimaryKey, Row.CustomSQL);
    end;
  end;

  Table.Close;

  for i := Low(LocalIndexes) to High(LocalIndexes) do begin
    with LocalIndexes[i] do begin
      Table.TableName := TableName;
      Table.AddIndex(IndexName, Fields, Options);
    end;
  end;

  Database.Close;
end;

procedure TQueryDM.AddTable(const TableName, PrimaryKey: string);
var
  PopulateQuery: string;
begin
  PopulateQuery := 'select * from ' + TableName + ' where 1=0';

  SyncStatus.AppendRecord([TableName, PrimaryKey]);
  Process(TableName, PrimaryKey, PopulateQuery);
end;

procedure TQueryDM.DeleteLocalTables;
begin
  DeleteMatchingWildcard('*.dat');
  DeleteMatchingWildcard('*.idx');
  DeleteMatchingWildcard('*.blb');
end;

procedure TQueryDM.DeleteMatchingWildcard(const Wildcard: string);
var
  sr: TSearchRec;

  procedure DeleteOrFail(FileName: string);
  begin
    if not DeleteFile(FDir + '\' + FileName) then
      raise Exception.Create('Could not delete file: ' + FDir + '\' + sr.name);
  end;

begin
  if FindFirst(FDir + '\' + Wildcard, faAnyFile, sr) = 0 then begin
    DeleteOrFail(sr.Name);
    while FindNext(sr) = 0 do begin
      DeleteOrFail(sr.Name);
    end;
  end;
  FindClose(sr);
end;

procedure CopyFieldDefsWithRules(Src: TFieldDefs; Dest: TDBISAMFieldDefs);
var
 i: Integer;
begin
  Dest.Clear;
  for i := 0 to Src.Count-1 do begin
    with Dest.AddFieldDef do begin
      Name := Src[i].Name;
      DisplayName := Src[i].DisplayName;
      Attributes := Src[i].Attributes;

      if Src[i].DataType = ftAutoInc then
        DataType := ftInteger
      else begin
        // DBISAM can't handle length ftFixedChar fields, so make them ftString
        if Src[i].DataType = ftFixedChar then
          DataType := ftString
        else if Src[i].DataType = ftWideMemo then    //QMANTW0-432 EB - AWS tables for api_storage_credentials
          DataType := ftMemo
        else if Src[i].DataType = ftFMTBcd then
          DataType := ftBcd
        else
          DataType := Src[i].DataType;
      end;

      //DBISAM 4 now supports string of 512, and memo size is now required to be 0:
      //http://www.elevatesoft.com/manual?action=viewtopic&id=dbisam4&product=rsdelphi&version=2007&topic=data_types_null_support

      //TODO: Changing the size to 512 (since DBISAM now supports it) created an issue for us
      //with columns like highlight_rule.word_regex (varchar(300)). This causes a datatype change
      //from ftmemo to ftstring- which DBISAM does not handle well - it clears the column data
      //during the conversion from memo to string, regardless of size- it does not even attempt to keep the data and truncate it.
      if (Src[i].DataType = ftString) and (Src[i].Size>250) then begin
        DataType := ftMemo;
        Size := 0;
      end
      else
        Size := Src[i].Size;

      Required := Src[i].Required;

      if CompareStr(Src[i].Name, 'modified_date') = 0 then
        Required := False;

      if CompareStr(Src[i].Name, 'active') = 0 then
        Required := True;
    end;
  end;
end;

procedure TQueryDM.Process(const LocalTableName, PrimaryKey, SQL: string);
begin
  Query.CommandText := SQL;
  Query.Open;
  try
    // Set up FieldDefs from query
    with Table do begin
      TableName := LocalTableName;

      CopyFieldDefsWithRules(Query.FieldDefs, FieldDefs);

      // Local tables always have a DeltaStatus column to find inserts/edits/deletes
      with FieldDefs.AddFieldDef do begin
        Name     := 'DeltaStatus';
        DataType := ftString;
        Size := 1;
      end;

      // LocalKey allows the client to store a value that can be used to
      // find a specific row after an identify fetch is done on the server
      with FieldDefs.AddFieldDef do begin
        Name     := 'LocalKey';
        DataType := ftDateTime;
      end;

      with FieldDefs.AddFieldDef do begin
        Name     := 'LocalStringKey';
        DataType := ftString;
        Size := 20;
      end;

      CreateTable;
      Close;
      AddIndex('', PrimaryKey, [ixPrimary, ixUnique]);
    end;
  finally
    Query.Close;
  end;
end;

procedure TQueryDM.SetDataDirectory(Dir: string);
begin
  CreateDir(Dir);

  FDir := Dir;
  Database.Close;
  Database.Directory := Dir;
  WriteLn(Output, Dir);
end;

end.

