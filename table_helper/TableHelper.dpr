program TableHelper;

{$R 'QMVersion.res' '..\QMVersion.rc'}

// Packages: vcl;rtl;dbrtl;vcldb;adortl;BetterADO;dbisamr

uses
  ActiveX,
  QueryDMu in 'QueryDMu.pas' {QueryDM: TDataModule},
  OdDBISAMXml in '..\common\OdDBISAMXml.pas',
  OdDBISAMjson in '..\common\OdDBISAMjson.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdMSXMLUtils in '..\common\OdMSXMLUtils.pas',
  LocalDataDef in '..\client\LocalDataDef.pas',
  MSXML2_TLB in '..\common\MSXML2_TLB.pas',
  QMConst in '..\client\QMConst.pas',
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  OdIsoDates in '..\common\OdIsoDates.pas',
  UQDbConfig in '..\common\UQDbConfig.pas',
  OdExceptions in '..\common\OdExceptions.pas';

{$APPTYPE CONSOLE}

begin
  ExitCode := 1;
  CoInitialize(nil);
  TQueryDM.Create(nil).Go;
end.


