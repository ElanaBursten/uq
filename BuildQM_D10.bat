@echo off
rem this runs first in the build process 
set BUILD_NUMBER=%1
set GIT_COMMIT=%2

if "%TARGET%"=="" set TARGET=ReBuild
if "%CONFIG%"=="" set CONFIG=Release

set WORKSPACE=C:\TRUNK1
rem changed from 19.0  8/28 sr
call "%ProgramFiles(x86)%\Embarcadero\Studio\22.0\bin\rsvars.bat" 

rem set DBISAMDIR=C:\Program Files (x86)\DBISAM 4 VCL-STD\RAD Studio 10S (Delphi Win32)\code

rem set JCLDIRS=C:\DelphiXE10\JCL\source\include;C:\DelphiXE10\JCL\lib\d23\win32

rem set RBDIR=%ProgramFiles(x86)%\Embarcadero\Studio\17.0\RBuilder\Lib\Win32

rem set FIREDACDIRS=C:\Program Files (x86)\Embarcadero\FireDAC\Dcu\D17\win32 

rem Uncomment SKIP_NEWDB to skip recreating TestDB
 set SKIP_NEWDB=True
rem Uncomment SKIP_PYTHON to skip the post build Python tests
 set SKIP_PYTHON=True


cd %WORKSPACE%
REM Get the Version information from version.ini
for /f "tokens=1,2 delims==" %%a in (version.ini) do (
  if %%a==VERSION_MAJOR set VERSION_MAJOR=%%b
  if %%a==VERSION_MINOR set VERSION_MINOR=%%b
  if %%a==VERSION_RELEASE set VERSION_RELEASE=%%b
)
set VERSION_PREFIX=%VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_RELEASE%.

REM GIT_COMMIT, GIT_BRANCH, and BUILD_NUMBER variables are set by Jenkins. Default for dev purposes if missing.

if "%GIT_BRANCH%"=="" set GIT_BRANCH=origin/master
set BUILD_BRANCH=
if NOT "%GIT_BRANCH%"=="origin/master" set BUILD_BRANCH=-%GIT_BRANCH:origin/=%  


set GIT_COMMIT_SHORT=%GIT_COMMIT:~0,7%
set VERSION=%VERSION_PREFIX%%BUILD_NUMBER%
set PRODUCT_VERSION=%VERSION% %GIT_COMMIT_SHORT%
set BUILD_NAME=QM-%VERSION%-%GIT_COMMIT_SHORT%%BUILD_BRANCH%

echo %TARGET% %GIT_COMMIT under %WORKSPACE% using %CONFIG% configuration Set Version %VERSION% %GIT_COMMIT_SHORT%

setlocal 
REM GOTO EndCommentBlock
:: Acts a comment block
set OTHERDIRS=%BDSCOMMONDIR%\Dcp;%OTHERDIRS%
set THIRDPARTYPATH=%INDYDIRS%;%JCLDIRS%;%RBDIR%;%FIREDACDIRS%;%DBISAMDIR%;%OTHERDIRS%
set DCC_Quiet=True
set DCC_MapFile=3
set DCC_UnitSearchPath=
%WORKSPACE%\client;
%WORKSPACE%\server;
%WORKSPACE%\reportengine;
%WORKSPACE%\common;
%WORKSPACE%\thirdparty;
%WORKSPACE%\thirdparty\GPSTools;
%WORKSPACE%\thirdparty\PerlRegEx;
%WORKSPACE%\thirdparty\zlib;
%WORKSPACE%\thirdparty\zlib\zlib-1.1.4;
%WORKSPACE%\thirdparty\BetterADO;
%WORKSPACE%\thirdparty\SuperObjectv1.2.4;%THIRDPARTYPATH%
set DCC_Define=%OTHERDEFINES%
set DCC_IntegerOverflowCheck=True
set DCC_RangeChecking=True
set DCC_GenerateStackFrames=True
set DCC_WriteableConstants=True
set LibraryPath=%BDS%\lib\win32\release;%BDS%\Imports;%BDSCOMMONDIR%\Dcp;%DCC_UnitSearchPath%  

C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild /Nologo /fl /flp:LogFile=%WORKSPACE%\BuildQM_D10.log;Verbosity=Normal /t:%TARGET% /v:n /p:Configuration=%CONFIG% QMD10.msbuild || goto :Failed
REM :EndCommentBlock

echo Build the Delphi Seattle projects Complete

endlocal


echo Preparing to Build Delphi2007 Projects

call SetBuildVars.bat
set GIT_COMMIT_SHORT=%GIT_COMMIT:~0,7%
set VERSION=%VERSION_PREFIX%%BUILD_NUMBER%
set PRODUCT_VERSION=%VERSION% %GIT_COMMIT_SHORT%
set BUILD_NAME=QM-%VERSION%-%GIT_COMMIT_SHORT%%BUILD_BRANCH%

REM Build the Delphi 2007 projects
setlocal
set BDS=%BDS2007%
set BDSCOMMONDIR=%BDSCOMMONDIR2007%
set INDYDIRS=%WORKSPACE%\thirdparty\Indy10\Protocols;%WORKSPACE%\thirdparty\Indy10\System;%WORKSPACE%\thirdparty\Indy10\Core
set OTHERDIRS=%BDSCOMMONDIR%\Dcp;%OTHERDIRS%;%DEVEXDIR%
set THIRDPARTYPATH=%INDYDIRS%;%DBISAM2007DIR%;%JCL2007DIRS%;%RB2007DIR%;%OTHERDIRS%;%HTMLCOMP%
set DCC_Quiet=True
set DCC_MapFile=3
set DCC_UnitSearchPath=%WORKSPACE%\client;%WORKSPACE%\server;%WORKSPACE%\reportengine;%WORKSPACE%\common;%WORKSPACE%\thirdparty;%WORKSPACE%\thirdparty\RemObjects;%WORKSPACE%\thirdparty\GPSTools;%WORKSPACE%\thirdparty\PerlRegEx;%WORKSPACE%\thirdparty\zlib;%WORKSPACE%\thirdparty\zlib\zlib-1.1.4;%WORKSPACE%\thirdparty\BetterADO;%WORKSPACE%\thirdparty\SuperObjectv1.2.4;%THIRDPARTYPATH%
set DCC_Define=%OTHERDEFINES%
set DCC_IntegerOverflowCheck=True
set DCC_RangeChecking=True
set DCC_GenerateStackFrames=True
set DCC_WriteableConstants=True
set PATH=%FrameworkDir%;%PATH%
set LibraryPath=%BDS%\lib;%BDS%\Imports;%BDSCOMMONDIR%\Dcp;%DCC_UnitSearchPath%

REM print the vars so we can track down problems:
set

rem Use this for more verbose output > MSBuild /Nologo /fl /flp:LogFile=%WORKSPACE%\BuildQM.log;Verbosity=Detailed /t:%TARGET% /p:Configuration=%CONFIG% QM.msbuild
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild /Nologo /fl /flp:LogFile=%WORKSPACE%\BuildQM_D10.log;Verbosity=Normal;Append /t:%TARGET% /v:n /p:Configuration=%CONFIG%;Win32LibraryPath="%LibraryPath%" QM2007.msbuild || goto :Failed
endlocal

echo Build the Delphi 2007 projects Complete
exit

:Failed
PAUSE
exit 1





