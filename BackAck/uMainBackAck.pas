unit uMainBackAck;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB, MSXML,
  Vcl.ComCtrls, Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc, Vcl.ExtCtrls;
type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);
  EBadAckData = class(Exception);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[40];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;
type
  TForm2 = class(TForm)
    dtBegin: TDateTimePicker;
    dtEnd: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    ADOConn: TADOConnection;
    insAcks: TADOQuery;
    btnAckBack: TButton;
    Label3: TLabel;
    Memo1: TMemo;
    StatusBar1: TStatusBar;
    ComboBox1: TComboBox;
    ProgressBar1: TProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure btnAckBackClick(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure dtBeginChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    LogResult: TLogResults;
    flogDir: wideString;
    fcallCenter: wideString;
    fDateBegin  : TDate;
    fDateEnd    : TDate;
    fSource : string;
    fClientCode : string;
    HotStop : boolean;
    function ConfigDB: boolean;

    procedure clearLogRecord;

    function GetAppVersionStr: string;
    procedure ProcessLoop;

    { Private declarations }
  public
    procedure WriteLog(LogResult: TLogResults);
    property callCenter: wideString read fcallCenter;
    property logDir: wideString read flogDir;

    { Public declarations }
  end;

var
  Form2: TForm2;

    implementation
uses System.DateUtils;

{$R *.dfm}

function TForm2.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

procedure TForm2.Memo1DblClick(Sender: TObject);
begin
  memo1.Clear;
end;

procedure TForm2.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  CALL_CENTER_LOG = 'BACK_Acker_%s';
  MISS_ACK_LOG = 'MISSING_ACK_%s';
  PRE_Pend = '[yyyy-mm-dd hh:nn:ss] ';

  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_Pend, NOW);
  if LogResult.LogType <> ltMissing then

    LogName := logDir + '\' + callCenter + '-' + format(CALL_CENTER_LOG,
      [FormatDateTime('yyyy-mm-dd', Date)]) + FILE_EXT
  else
    LogName := logDir + '\' + callCenter + '-' + format(MISS_ACK_LOG,
      [FormatDateTime('yyyy-mm-dd', Date)]) + FILE_EXT;
  Memo1.Lines.add('Log path ' + LogName);

  // callCenter
  case LogResult.LogType of
    ltError:
      begin
        EntryType := '**************** ERROR ****************';
      end;
    ltInfo:
      begin
        EntryType := '****************  INFO  ****************';
      end;
    ltNotice:
      begin
        EntryType := '**************** NOTICE ****************';
      end;
    ltWarning:
      begin
        EntryType := '**************** WARNING ****************';
      end;
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;
  WriteLn(myFile, EntryType);
  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
    Memo1.Lines.add(Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;
  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
    Memo1.Lines.add(Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;
  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
    Memo1.Lines.add(Leader + 'Status : ' + LogResult.Status);
  end;
  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
    Memo1.Lines.add(Leader + 'Data : ' + LogResult.DataStream);
  end;

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TForm2.btnAckBackClick(Sender: TObject);

begin
  HotStop := false;
  fDateBegin := dtBegin.Date;
  fDateEnd := dtEnd.Date;
  btnAckBack.Enabled := false;
//  Tthread.CreateAnonymousThread(procedure begin
//      while not Application.Terminated  do begin
      ProcessLoop;
//    end;
//    end).Start;
end;

procedure TForm2.ProcessLoop;
var
  processDate : TDate;
  cnt, totalCnt:integer;
begin
  cnt:= 0;
  totalCnt := 0;
  while fDateBegin < fDateEnd do
  begin

    processDate :=fDateBegin;
    insAcks.Parameters.ParamByName('BeginDate').Value:= processDate;
    insAcks.Parameters.ParamByName('EndDate').Value:=   processDate+1;
    insAcks.Parameters.ParamByName('callCenter').Value:= fCallcenter;  //NCA1 or SCA1
    insAcks.Parameters.ParamByName('source').Value:= fSource; // 'NCA2' or 'SCA6'
    insAcks.Parameters.ParamByName('clientCode').Value:= fClientCode; //ATTDSOUTH or PACBEL
    insAcks.Parameters.ParamByName('ticketFormat').Value :=  fCallcenter;  // //NCA1 or SCA1
    StatusBar1.panels[5].Text := TimeToStr(time);
    cnt:= insAcks.ExecSQL;
    totalCnt :=  totalCnt + cnt;
    ProgressBar1.Max:= totalCnt;
    ProgressBar1.StepBy(cnt);
    memo1.Lines.Add(IntToStr(cnt)+' Acks restored for '+DateToStr(processDate));
    memo1.Lines.Add(IntToStr(totalCnt)+' Acks totalled restored');
    StatusBar1.Panels[1].Text := IntToStr(cnt);
    StatusBar1.Panels[3].Text := IntToStr(totalCnt);
    StatusBar1.Refresh;
    fDateBegin := fDateBegin+1;
    dtBegin.Date := fDateBegin;
    dtBegin.Refresh;
    LogResult.MethodName := 'ProcessLoop';
    LogResult.DataStream :=  IntToStr(cnt)+' Acks restored for '+DateToStr(processDate);
    LogResult.LogType := ltinfo;
    WriteLog(LogResult);
    application.ProcessMessages;

    sleep(1000);
  end;
  memo1.Lines.Add('All Acks are Back, Jack');
  btnAckBack.Enabled := true;
end;

procedure TForm2.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TForm2.ComboBox1Change(Sender: TObject);
begin
  case ComboBox1.ItemIndex of
    0:  begin
           fCallcenter := 'SCA1';
           fSource     := 'SCA6';
           fClientCode := 'ATTDSOUTH';
        end;
    1:  begin
           fCallcenter := 'NCA1';
           fSource     := 'NCA2';
           fClientCode := 'PACBEL';
        end;
  end;
  memo1.Lines.Add('Configured for Call Center '+fCallcenter+ ' and Source '+fSource);
end;

function TForm2.ConfigDB: boolean;
begin
  result := false;
  if ADOConn.Connected then
    ADOConn.Close;
  try
    with ADOConn do
    begin

      ConnectionString := 'Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParserUTL;Password=ihjidojo3;Initial Catalog=QM;Data Source=SSDS-UTQ-QM-02;Initial File Name="";Server SPN=""';

      ADOConn.Open();
      Result := ADOConn.Connected;
      LogResult.MethodName := 'ConfigDB';
      LogResult.DataStream := 'ConnectionString: ' + ConnectionString;
      LogResult.LogType := ltInfo;
      Memo1.Lines.add('Connected to ' + ConnectionString);
    end;
    assert(ADOConn.Connected, 'Failed to connect to database');

  except
    on e: Exception do
    begin
      if e is EAssertionFailed then
      begin
        LogResult.MethodName := 'ConfigDB';
        LogResult.ExcepMsg := e.message;
        LogResult.DataStream := ADOConn.ConnectionString;
        LogResult.LogType := ltError;
        WriteLog(LogResult);
      end
      else
      begin
        LogResult.MethodName := 'ConfigDB';
        LogResult.ExcepMsg := e.message;
        LogResult.DataStream := ADOConn.ConnectionString;
        LogResult.LogType := ltError;
        WriteLog(LogResult);
      end;
    end;
  end;
end;

procedure TForm2.dtBeginChange(Sender: TObject);
begin
  dtEnd.DateTime := EndOfTheMonth(dtBegin.DateTime);
end;

procedure TForm2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  application.Terminate;
end;

procedure TForm2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
//  canClose := application.Terminated;
end;

procedure TForm2.FormCreate(Sender: TObject);
var
  i: integer;
begin
  dtBegin.DateTime := Today;
  dtBeginChange(Sender);
  flogDir := 'c:\QM\Logs';

//  if ParseXML(ExtractFilePath(ParamStr(0))+'\config.XML') then
//    Memo1.Lines.add('ParseXML(configXML) succeeded');

  if ConfigDB then
    Memo1.Lines.add('ConfigDB succeeded ');

  begin

    Memo1.Lines.add
      ('******************************* Let''s Make Those Acks Bass Ackwards!!!! *******************************');

  end;
  self.Caption := self.Caption + '     ' + ' Back Acking ' + callCenter +
    ' in the Free World';
  self.Caption := self.Caption + '     ' + GetAppVersionStr;
end;



end.
