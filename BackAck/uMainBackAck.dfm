object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Back Off-'
  ClientHeight = 328
  ClientWidth = 474
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 12
  object Label1: TLabel
    Left = 39
    Top = 5
    Width = 46
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Begin'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 346
    Top = 5
    Width = 30
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'End'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 189
    Top = 5
    Width = 91
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Call Center'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dtBegin: TDateTimePicker
    Left = 19
    Top = 26
    Width = 97
    Height = 20
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Date = 0.380698819441022400
    Time = 0.380698819441022400
    TabOrder = 0
    OnChange = dtBeginChange
  end
  object dtEnd: TDateTimePicker
    Left = 326
    Top = 26
    Width = 97
    Height = 20
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Date = 43035.380698819440000000
    Time = 43035.380698819440000000
    TabOrder = 1
  end
  object btnAckBack: TButton
    Left = 182
    Top = 83
    Width = 108
    Height = 27
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Get Some'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = btnAckBackClick
  end
  object Memo1: TMemo
    Left = 0
    Top = 145
    Width = 474
    Height = 148
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alBottom
    ScrollBars = ssBoth
    TabOrder = 3
    OnDblClick = Memo1DblClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 309
    Width = 474
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Panels = <
      item
        Text = 'Ack Cnt'
        Width = 80
      end
      item
        Width = 100
      end
      item
        Text = 'Total Cnt'
        Width = 50
      end
      item
        Width = 100
      end
      item
        Text = 'Query Time'
        Width = 110
      end
      item
        Width = 50
      end>
  end
  object ComboBox1: TComboBox
    Left = 202
    Top = 26
    Width = 64
    Height = 20
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    TabOrder = 5
    OnChange = ComboBox1Change
    Items.Strings = (
      'SCA6'
      'NCA1')
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 293
    Width = 474
    Height = 16
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alBottom
    TabOrder = 6
  end
  object ADOConn: TADOConnection
    CommandTimeout = 3000
    ConnectionTimeout = 3000
    LoginPrompt = False
    Provider = 'C:\SoftwareProjects\ticketAck(8)\BackAck\ADOConnQM.UDL'
    Left = 49
    Top = 122
  end
  object insAcks: TADOQuery
    Connection = ADOConn
    Filtered = True
    CommandTimeout = 3000
    Parameters = <
      item
        Name = 'BeginDate'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'EndDate'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'callCenter'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'clientCode'
        Size = -1
        Value = Null
      end
      item
        Name = 'source'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'ticketFormat'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Insert Into [dbo].[response_ack_waiting]'
      'SELECT distinct [response_id]'
      ' '#9'  ,tv.serial_number as [reference_number]'
      #9'  ,call_center'
      '  FROM [QM].[dbo].[response_log] rl'
      '  inner join locate L on (rl.locate_id = L.locate_id)'
      '  inner join ticket_version tv on (L.ticket_id = tv.ticket_id)'
      '  where ((rl.response_date >= :BeginDate)'
      '  and (rl.response_date< :EndDate))'
      '  and rl.call_center = :callCenter'
      '  and l.client_code = :clientCode'
      '  and tv.serial_number is not null'
      '  and rl.success = 0'
      '  and rl.reply = '#39'(email waiting)'#39
      '  and tv.source = :source'
      '  and tv.ticket_format = :ticketFormat'
      '  and 0 = (select count(*)'
      #9#9'   from [dbo].[response_ack_waiting]'
      #9#9'   where [response_id] = rl.[response_id]'
      '       and call_center = rl.call_center'
      #9#9')'
      '')
    Left = 329
    Top = 82
  end
end
