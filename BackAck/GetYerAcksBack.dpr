program GetYerAcksBack;
{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}
uses
  Vcl.Forms,
  uMainBackAck in 'uMainBackAck.pas' {Form2};

begin
  Application.Initialize;
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
