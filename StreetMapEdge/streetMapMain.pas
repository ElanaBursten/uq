unit streetMapMain;
//QM-553
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, StdCtrls, ExtCtrls, ComCtrls, MSHTML,
  Buttons, uWVBrowserBase, uWVBrowser, uWVWinControl, uWVWindowParent,
  uWVTypes, uWVConstants, uWVTypeLibrary,
  uWVLibFunctions, uWVLoader, uWVInterfaces, uWVCoreWebView2Args, uWVCoreWebView2DownloadOperation;
type
  TfrmStreetMapMain = class(TForm)
    PanelHeader: TPanel;
    CheckBoxTraffic: TCheckBox;
    CheckBoxStreeView: TCheckBox;
    BitBtn1: TBitBtn;
    WVWindowParent1: TWVWindowParent;
    WVBrowser1: TWVBrowser;
    Timer1: TTimer;
    StatusBar1: TStatusBar;
    edtLat: TEdit;
    Label2: TLabel;
    edtLng: TEdit;
    Label3: TLabel;
    Button2: TButton;

    procedure CheckBoxTrafficClick(Sender: TObject);

    procedure CheckBoxStreeViewClick(Sender: TObject);
    procedure ButtonClearMarkersClick(Sender: TObject);
    procedure ButtonGotoLocationClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure WVBrowser1DownloadStarting(Sender: TObject;
      const aWebView: ICoreWebView2;
      const aArgs: ICoreWebView2DownloadStartingEventArgs);
    procedure WVBrowser1DownloadStateChanged(Sender: TObject;
      const aDownloadOperation: ICoreWebView2DownloadOperation;
      aDownloadID: Integer);
    procedure WVBrowser1AfterCreated(Sender: TObject);
    procedure WVBrowser1BytesReceivedChanged(Sender: TObject;
      const aDownloadOperation: ICoreWebView2DownloadOperation;
      aDownloadID: Integer);
    procedure WVBrowser1InitializationError(Sender: TObject;
      aErrorCode: HRESULT; const aErrorMessage: wvstring);
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }

    fUtc: string;
    fLat: string;
    fLng: string;
    fLocatDT: string;
    fShortName: string;
    fMulti: Boolean;

    procedure CreateMap(Sender: TObject);
    function GetNextDownloadID: integer;
    procedure UpdateDownloadInfo(aDownloadID: integer);
    procedure LoadFromFileAsString(const aFileName: string);
  protected
    FDownloadOperation : TCoreWebView2DownloadOperation;
    FDownloadIDGen     : integer;
    procedure WMMove(var aMessage : TWMMove); message WM_MOVE;
    procedure WMMoving(var aMessage : TMessage); message WM_MOVING;
  public
    property  LocatDT : string Read fLocatDT write fLocatDT;
    property  Lat : string read fLat write fLat;
    property  Lng : string read fLng write fLng;
    property  Utc : string read fUtc write fUtc;
    property  ShortName : string read fShortName write fShortName;
    property Multi: Boolean read fMulti write fMulti;

    constructor Create(AOwner:TComponent; aLat, aLng, aUtc, aShortName:string);reintroduce;
//    function MapAllBreadcrumbs: Boolean;
  end;

var
  frmStreetMapMain: TfrmStreetMapMain;
  GoogleAPIKey: AnsiString;
  SystemDir: string;
implementation

uses
   DateUtils,  ShellAPI;

{$R *.dfm}


constructor TfrmStreetMapMain.Create(AOwner: TComponent; aLat, aLng, aUtc, aShortName: string);
begin
  inherited Create(AOwner);
  Lat  := aLat;
  Lng  := aLng;
  Utc  :=  aUtc;
end;


procedure TfrmStreetMapMain.CreateMap(Sender: TObject);
begin
   LoadFromFileAsString('StreetMapSimple.htm');
end;

procedure TfrmStreetMapMain.LoadFromFileAsString(const aFileName : string);
var
  TempLines : TStringList;
begin
  TempLines := nil;
  try
    try
      if (length(aFileName) > 0) and FileExists(aFileName) then
        begin
          TempLines := TStringList.Create;
          TempLines.LoadFromFile(aFileName);
          WVBrowser1.NavigateToString(TempLines.Text);
        end;
    except
      {$IFDEF DEBUG}
      on e : exception do
        OutputDebugString(PWideChar('TfrmStreetMap.LoadFromFileAsString error: ' + e.message + chr(0)));
      {$ENDIF}
    end;
  finally
    if assigned(TempLines) then
      FreeAndNil(TempLines);
  end;
end;

procedure TfrmStreetMapMain.FormActivate(Sender: TObject);



  procedure DriveLinkCreate(const Drive: Char; const Path: String);
  var
    Param: String;
  begin
    (* format the call parameter *)
    Param := Format('%s: "%s"', [Drive, Path]);
    (* and bang! we get a new drive *)
    ShellExecute(1, 'open', 'subst', PChar(Param),
      PChar(SystemDir), 0);
  end;


begin
  DriveLinkCreate('Q', 'D:\EdgeDownloadsII');
end;

procedure TfrmStreetMapMain.FormClose(Sender: TObject; var Action: TCloseAction);
  procedure DriveLinkRemove(const Drive: Char);
  var
    Param: String;
  begin
    (* format the call parameter with the /d option
       which stands for delete *)
    Param := Format('%s: /d', [Drive]);
    (* now we remove the virtual drive *)
    ShellExecute(1, 'open', 'subst', PChar(Param),
      PChar(SystemDir), 0);
  end;
begin
  DriveLinkRemove('Q');
  Action := caFree;
end;

procedure TfrmStreetMapMain.FormCreate(Sender: TObject);
  function sSystemDir: string;
    begin
      (* get system32 folder *)
      SetLength(Result, MAX_PATH);
      GetSystemDirectory(@Result[1], MAX_PATH);
    end;
begin
  SystemDir:= sSystemDir;
  FDownloadIDGen          := 0;
  FDownloadOperation      := nil;
//  CreateMap(Sender);
end;

procedure TfrmStreetMapMain.FormShow(Sender: TObject);
begin
  if GlobalWebView2Loader.InitializationError then
    showmessage(GlobalWebView2Loader.ErrorMessage)
   else
    if GlobalWebView2Loader.Initialized then
      WVBrowser1.CreateBrowser(WVWindowParent1.Handle)
     else
      Timer1.Enabled := True;

end;

//{QMANTWO-302 - Added}
//function TfrmStreetMap.MapAllBreadcrumbs: Boolean;
////var
////  fLat, fLng,
////  fLabel: string;
//  //fActivity: string;
//begin
//  WVBrowser1.ExecuteScript('ShowAllMarkers()');
//end;

procedure TfrmStreetMapMain.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if GlobalWebView2Loader.Initialized then
    WVBrowser1.CreateBrowser(WVWindowParent1.Handle)
   else
    Timer1.Enabled := True;
end;


procedure TfrmStreetMapMain.WMMove(var aMessage: TWMMove);
begin
  inherited;
  if (WVBrowser1 <> nil) then
    WVBrowser1.NotifyParentWindowPositionChanged;
end;

procedure TfrmStreetMapMain.WMMoving(var aMessage: TMessage);
begin
  inherited;
  if (WVBrowser1 <> nil) then
    WVBrowser1.NotifyParentWindowPositionChanged;
end;

procedure TfrmStreetMapMain.WVBrowser1AfterCreated(Sender: TObject);
begin
  WVWindowParent1.UpdateSize;
    CreateMap(Sender);
end;

procedure TfrmStreetMapMain.WVBrowser1BytesReceivedChanged(Sender: TObject;
  const aDownloadOperation: ICoreWebView2DownloadOperation;
  aDownloadID: Integer);
begin
  UpdateDownloadInfo(aDownloadID);
end;

procedure TfrmStreetMapMain.WVBrowser1DownloadStarting(Sender: TObject;
  const aWebView: ICoreWebView2;
  const aArgs: ICoreWebView2DownloadStartingEventArgs);
var
  TempArgs : TCoreWebView2DownloadStartingEventArgs;
begin
  TempArgs := TCoreWebView2DownloadStartingEventArgs.Create(aArgs);     //%USERPROFILE%\Downloads
  TempArgs.ResultFilePath:='Q:\EdgeDownloads\testVirtual.txt';
  if assigned(TempArgs.DownloadOperation) and not(assigned(FDownloadOperation)) then
    begin
      FDownloadOperation := TCoreWebView2DownloadOperation.Create(TempArgs.DownloadOperation, GetNextDownloadID);
      FDownloadOperation.AddAllBrowserEvents(WVBrowser1);
      // We hide the download window
      TempArgs.Handled := true;
    end;
  FreeAndNil(TempArgs);
end;

function TfrmStreetMapMain.GetNextDownloadID : integer;
begin
  inc(FDownloadIDGen);
  Result := FDownloadIDGen;
end;

procedure TfrmStreetMapMain.WVBrowser1DownloadStateChanged(Sender: TObject;
  const aDownloadOperation: ICoreWebView2DownloadOperation;
  aDownloadID: Integer);
begin
  if assigned(FDownloadOperation) and (FDownloadOperation.DownloadID = aDownloadID) then
    begin
      case FDownloadOperation.State of
        COREWEBVIEW2_DOWNLOAD_STATE_IN_PROGRESS :
          UpdateDownloadInfo(aDownloadID);
        COREWEBVIEW2_DOWNLOAD_STATE_INTERRUPTED :
          begin
            StatusBar1.Panels[0].Text := 'Download was interrupted';
            FreeAndNil(FDownloadOperation);
          end;
        COREWEBVIEW2_DOWNLOAD_STATE_COMPLETED :
          begin
            StatusBar1.Panels[0].Text := 'Download complete!';
            FreeAndNil(FDownloadOperation);
          end;
      end;
    end;
end;

procedure TfrmStreetMapMain.WVBrowser1InitializationError(Sender: TObject;
  aErrorCode: HRESULT; const aErrorMessage: wvstring);
begin
  showmessage(aErrorMessage);
end;

procedure TfrmStreetMapMain.UpdateDownloadInfo(aDownloadID : integer);
var
  TempStatus : string;
  TempPct : double;
begin
  if assigned(FDownloadOperation) and (FDownloadOperation.DownloadID = aDownloadID) then
    begin
      TempStatus := 'Downloading...';
      if (FDownloadOperation.TotalBytesToReceive <> 0) then
        begin
          TempPct    := (FDownloadOperation.BytesReceived / FDownloadOperation.TotalBytesToReceive) * 100;
          TempStatus := TempStatus + ' ' + inttostr(round(TempPct)) + '%';
        end
       else
        TempStatus := TempStatus + ' ' + inttostr(FDownloadOperation.BytesReceived) + ' bytes';
      TempStatus := TempStatus + ' - Estimated end time : ' + TimeToStr(FDownloadOperation.EstimatedEndTime);
      StatusBar1.Panels[0].Text := TempStatus;
    end;
end;

procedure TfrmStreetMapMain.BitBtn1Click(Sender: TObject);
begin
  close;
end;

procedure TfrmStreetMapMain.Button1Click(Sender: TObject);
begin
  CreateMap(Sender);
end;

procedure TfrmStreetMapMain.ButtonClearMarkersClick(Sender: TObject);
begin
  WVBrowser1.ExecuteScript('ClearMarkers()');
end;

procedure TfrmStreetMapMain.ButtonGotoLocationClick(Sender: TObject);
begin
  Lat:=  edtLat.Text;
  Lng:=  edtLng.Text;
   WVBrowser1.ExecuteScript(Format('GotoLatLng(%s,%s)',[Lat,Lng]));
end;

procedure TfrmStreetMapMain.CheckBoxStreeViewClick(Sender: TObject);
begin
    if CheckBoxStreeView.Checked then
     WVBrowser1.ExecuteScript('StreetViewOn()')
    else
     WVBrowser1.ExecuteScript('StreetViewOff()');

end;

procedure TfrmStreetMapMain.CheckBoxTrafficClick(Sender: TObject);
begin
    if CheckBoxTraffic.Checked then
     WVBrowser1.ExecuteScript('TrafficOn()')
    else
     WVBrowser1.ExecuteScript('TrafficOff()');
 end;

initialization
  GlobalWebView2Loader                := TWVLoader.Create(nil);
  GlobalWebView2Loader.UserDataFolder := ExtractFileDir(Application.ExeName) + '\CustomCache';
  GlobalWebView2Loader.StartWebView2;


end.
