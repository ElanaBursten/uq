unit uMainAce;

interface
uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  Vcl.ComCtrls, dxCore, cxDateUtils, dxSkinsCore, dxSkinGlassOceans, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, cxGroupBox, cxRadioGroup, Vcl.StdCtrls,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, Data.Win.ADODB, Data.DB, Vcl.ExtCtrls, Vcl.Menus, cxButtons, Vcl.Grids,
  Vcl.DBGrids, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxTL, dxDateRanges, dxSkinTheBezier;

type
  TForm2 = class(TForm)
    pnlTop: TPanel;
    pnlMid: TPanel;
    pnlBottom: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    dsAssetTracker: TDataSource;
    AdoConn: TADOConnection;
    qryPayPeriod: TADOQuery;
    spAssetTracker: TADOStoredProc;
    cxEndDate: TcxDateEdit;
    lblWeekEnding: TLabel;
    cxRadioGroup1: TcxRadioGroup;
    StatusBar1: TStatusBar;
    cbCompany: TComboBox;
    Label1: TLabel;
    btnRetrieve: TcxButton;
    btnExport: TcxButton;
    DBGrid1DBTableView1: TcxGridDBTableView;
    DBGrid1Level1: TcxGridLevel;
    DBGrid1: TcxGrid;
    DBGrid1DBTableView1Level1: TcxGridDBColumn;
    DBGrid1DBTableView1projectID: TcxGridDBColumn;
    DBGrid1DBTableView1taskID: TcxGridDBColumn;
    DBGrid1DBTableView1acctCat: TcxGridDBColumn;
    DBGrid1DBTableView1transDate: TcxGridDBColumn;
    DBGrid1DBTableView1units: TcxGridDBColumn;
    DBGrid1DBTableView1Amount: TcxGridDBColumn;
    DBGrid1DBTableView1Descrip: TcxGridDBColumn;
    DBGrid1DBTableView1company: TcxGridDBColumn;
    DBGrid1DBTableView1last_name: TcxGridDBColumn;
    DBGrid1DBTableView1charge_cov: TcxGridDBColumn;
    DBGrid1DBTableView1EarningsType: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxGridTableViewStyleSheet1: TcxGridTableViewStyleSheet;
    TreeListStyleSheetRoseLarge: TcxTreeListStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle12: TcxStyle;
    btnSaveAmounts: TButton;
    lblAsset: TLabel;
    pnlAmounts: TPanel;
    edtTruckAmt: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    edtLossAmt: TEdit;
    edtPremiumAmt: TEdit;
    edtEquipAmt: TEdit;
    Label2: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure cxEndDatePropertiesChange(Sender: TObject);
    procedure btnRetrieveClick(Sender: TObject);
    procedure cbCompanyChange(Sender: TObject);
    procedure edtTruckAmtChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtPremiumAmtChange(Sender: TObject);
    procedure edtEquipAmtChange(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
    procedure cxRadioGroup1Click(Sender: TObject);
    procedure btnSaveAmountsClick(Sender: TObject);
    procedure edtLossAmtChange(Sender: TObject);
  private
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    aTrusted: string;


    processDate: Tdate;
    payPeriod: string;
    company: string;
    localCompany : string;
    charge_cov: boolean;
    AcctCat: String;
    Amount: string;
    Descrip: string;
    DescripRev:string;
    TransDate: string;

    Level0Title:string;
    Level0TitleRev:string;

    TruckAmt     : string;
    PremiumAmt   : string;
    PremiumLoss  : string;
    EquipmentAmt : string;

    LastSelected : integer;
    aceDataOut : string;
      myPath: string;
    function connectDB
    : boolean;
    function EnDeCrypt(const Value: String): String;
    function ReadINI: boolean;
    function GetPayPeriod:boolean;
    procedure ExportEquipment;
    procedure ExportInsurance;
    procedure ExportTruck;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

const
  SEP1 = ',';
  SEP2 = ',,';
  SEP3 = ',,,';
  SEP4 = ',,,,';
  EXT ='.txt';
implementation
uses
  System.IniFiles, System.StrUtils, DateUtils;
{$R *.dfm}

function TForm2.ReadINI: boolean;
var
  AceIni: TIniFile;
  aPath:string;
begin
  Result := false;
  aPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
  try
    AceIni := TIniFile.Create(aPath + 'Ace.ini');

    aServer := AceIni.ReadString('Database', 'Server', '');
    aDatabase := AceIni.ReadString('Database', 'DB', 'QM');
    ausername := AceIni.ReadString('Database', 'UserName', '');
    apassword := AceIni.ReadString('Database', 'Password', '');
    aTrusted  := AceIni.ReadString('Database', 'Trusted', '1');

    edtTruckAmt.Text := AceIni.ReadString('AMOUNTS', 'Truck', '');
    edtPremiumAmt.Text :=  AceIni.ReadString('AMOUNTS', 'Premium', '');
    edtLossAmt.Text    :=  AceIni.ReadString('AMOUNTS', 'PremiumLoss', '');
    edtEquipAmt.Text :=   AceIni.ReadString('AMOUNTS', 'Equipment', '');

    aceDataOut:=  AceIni.ReadString('Data', 'DataOutput', '');
    myPath:=aPath+IncludeTrailingBackslash(aceDataOut);
    ForceDirectories(myPath);
  finally
    freeandnil(AceIni);
    Result := True;
  end;
end;

procedure TForm2.cbCompanyChange(Sender: TObject);
begin
  case cbCompany.ItemIndex of
    0:
      begin
        company := 'P3H';
        localCompany := 'UTQ';
      end;
    1:
      begin
        company := 'M1T';
        localCompany := 'LOC';
      end;
  end;
end;

function TForm2.connectDB: boolean;
const
  DECRYPT = 'DEC_';
var
  connStr, LogConnStr: String;
begin
  Result := false;
  try

    LogConnStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
    if LeftStr(apassword, 4) = DECRYPT then
    begin
      apassword := copy(apassword, 5, maxint);
      apassword := EnDeCrypt(apassword);
    end
    else
    begin
      Showmessage('You are using a Clear Text Password.  Please contact IT for the correct Password');
    end;

    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
    ADOConn.ConnectionString := connStr;
    ADOConn.Open;
  finally
     Result := ADOConn.Connected;
    if Result then
      StatusBar1.Panels[1].Text :=  aServer;
  end;
end;

procedure TForm2.btnExportClick(Sender: TObject);
begin
  case LastSelected of
    0: //truck
      begin
        ExportTruck;
      end;
    1: // Insurance
      begin
        ExportInsurance;
      end;
    2: // Equipment
      begin
        ExportEquipment;
      end;
  end;
  btnRetrieve.Enabled := false;
  btnExport.Enabled := false;
end;

procedure TForm2.ExportTruck;
var
  lineText, lineTextRev, OutputFile: string;
  myFile: TextFile;
  cnt : integer;
const
  FILE_NAME = '-PC Charge Entry - Trucks ';
  SUP_LOC_REM='000000000000';
begin
  try
    cnt:= 0;
    StatusBar1.Panels[6].text := 'Trucks';
    btnExport.Cursor := crHourGlass;
    OutputFile := myPath + localCompany + FILE_NAME + ' ' +
      FormatDateTime('mm_dd_yyyy', processDate) + EXT;
    AssignFile(myFile, OutputFile);
    ReWrite(myFile);
    WriteLn(myFile, Level0Title + ' ' + localCompany + ' ' + payPeriod + SEP1 + 'O');
    with spAssetTracker do
    begin
      first;
      while not eof do
      begin
        lineText := FieldByName('Level1').Value + SEP1 + FieldByName('projectID').Value + SEP1 +
          FieldByName('taskID').Value + SEP1 + FieldByName('acctCat').Value + SEP1 +
          FieldByName('transDate').Value + SEP1 + FieldByName('units').Value + SEP1 +
          FieldByName('Amount').Value + SEP4 + FieldByName('Descrip').Value;
        WriteLn(myFile, lineText);

        lineTextRev := FieldByName('Level1').Value + SEP1 + FieldByName('projectID').Value + SEP1 +
          LeftStr(FieldByName('taskID').Value,4)+SUP_LOC_REM+ SEP1 + FieldByName('acctCat').Value + SEP1 +
          FieldByName('transDate').Value + SEP1 + FieldByName('units').Value + SEP1 +'-'+
          FieldByName('Amount').Value + SEP4 + FieldByName('Descrip').Value;
        WriteLn(myFile, lineTextRev);
        next;
        inc(cnt);
        StatusBar1.Panels[5].text := IntToStr(cnt);
        StatusBar1.Refresh;
      end; // while not eof do
    end; // with spAssetTracker do
  finally
    spAssetTracker.Close;
    CloseFile(myFile);
    btnExport.Cursor := crDefault;
  end;
end;


procedure TForm2.ExportInsurance;
var
  lineText, lineTextRev, OutputFile, OutputFileRev: string;
  myFile,myFileRev : TextFile;
  cnt : integer;
  GL_Prem,GL_Loss:string;
const
  FILE_NAME = '-PC Charge Entry - Auto Ins Premium ';
  FILE_NAME_LOSS = '-PC Charge Entry - Auto Ins Loss ';
  SUP_LOC_REM='000000000000'; //not used in Loss version
  DESCRIP_PREMIUM_LOSS = 'Auto Insurance Loss';  //QMANTWO-744
  ACCT_CAT_LOSS='AUTO INS LOSS'; //QMANTWO-744

  GL_CODE_PREM_LO = '53520';   //QMANTWO-744
  GL_CODE_PREM_HI = '46560';   //QMANTWO-744

  GL_CODE_LOSS_LO = '53520';  //QMANTWO-744
  GL_CODE_LOSS_HI = '46500';  //QMANTWO-744
begin
  try
    cnt:= 0;
    StatusBar1.Panels[6].text := 'Premiums';
    btnExport.Cursor := crHourGlass;
    OutputFile := myPath + localCompany + FILE_NAME + ' ' +
      FormatDateTime('mm_dd_yyyy', processDate) + EXT;
    OutputFileRev := myPath + localCompany + FILE_NAME_LOSS + ' ' +
      FormatDateTime('mm_dd_yyyy', processDate) + EXT;
    AssignFile(myFile, OutputFile);
    AssignFile(myFileRev, OutputFileRev);
    ReWrite(myFile);
    ReWrite(myFileRev);
    WriteLn(myFile, Level0Title + ' ' + localCompany + ' ' + payPeriod + SEP1 + 'O');
    WriteLn(myFileRev, Level0TitleRev + ' ' + localCompany + ' ' + payPeriod + SEP1 + 'O');
    with spAssetTracker do
    begin
      first;
      while not eof do
      begin
        if StrToInt(LeftStr(FieldByName('taskID').AsString,3))< 13 then
        begin   //  low PC
          GL_Prem := GL_CODE_PREM_LO;
          GL_Loss := GL_CODE_LOSS_LO;
        end
        else
        begin    //  hi PC
          GL_Prem := GL_CODE_PREM_HI;
          GL_Loss := GL_CODE_LOSS_HI;
        end;

        lineText := FieldByName('Level1').Value + SEP1 + FieldByName('projectID').Value + SEP1 +
          FieldByName('taskID').Value + SEP1 + GL_Prem+ SEP1 +     //QMANTWO-744
          FieldByName('transDate').Value + SEP1 + FieldByName('units').Value + SEP1 +
          FieldByName('Amount').Value + SEP4 + FieldByName('Descrip').Value;
        WriteLn(myFile, lineText);

        lineTextRev := FieldByName('Level1').Value + SEP1 + FieldByName('projectID').Value + SEP1 +
          FieldByName('taskID').Value + SEP1 + GL_Loss+ SEP1 +
          FieldByName('transDate').Value + SEP1 + FieldByName('units').Value + SEP1 +
          //FieldByName('Amount').Value + SEP4 + DESCRIP_PREMIUM_LOSS;   //QMANTWO-744
          FieldByName('LossAmt').Value + SEP4 + DESCRIP_PREMIUM_LOSS;   //QMANTWO-744
        WriteLn(myFileRev, lineTextRev);
        next;
        inc(cnt);
        StatusBar1.Panels[5].text := IntToStr(cnt);
        StatusBar1.Refresh;
      end; // while not eof do
    end; // with spAssetTracker do
  finally
    spAssetTracker.Close;
    CloseFile(myFile);
    CloseFile(myFileRev);
    btnExport.Cursor := crDefault;
  end;
end;

procedure TForm2.ExportEquipment;
var
  lineText, lineTextRev, OutputFile: string;
  myFile : TextFile;
  cnt : integer;
const
  FILE_NAME = '-PC Charge Entry - Equipment ';
  SUP_LOC_REM='000000000000';
begin
  try
    cnt:= 0;
    StatusBar1.Panels[6].text:='Equipment';
    btnExport.Cursor := crHourGlass;
    OutputFile := myPath + localCompany + FILE_NAME + ' ' +
      FormatDateTime('mm_dd_yyyy', processDate) + EXT;
    AssignFile(myFile, OutputFile);
    ReWrite(myFile);
    WriteLn(myFile, Level0Title + ' ' + localCompany + ' ' + payPeriod + SEP1 + 'O');
    with spAssetTracker do
    begin
      first;
      while not eof do
      begin
        lineText := FieldByName('Level1').Value + SEP1 + FieldByName('projectID').Value + SEP1 +
          FieldByName('taskID').Value + SEP1 + FieldByName('acctCat').Value + SEP1 +
          FieldByName('transDate').Value + SEP1 + FieldByName('units').Value + SEP1 +
          FieldByName('Amount').Value + SEP4 + FieldByName('Descrip').Value;
        WriteLn(myFile, lineText);

        lineTextRev := FieldByName('Level1').Value + SEP1 + FieldByName('projectID').Value + SEP1 +
          LeftStr(FieldByName('taskID').Value,4)+SUP_LOC_REM+ SEP1 + FieldByName('acctCat').Value + SEP1 +
          FieldByName('transDate').Value + SEP1 + FieldByName('units').Value + SEP1 +'-'+
          FieldByName('Amount').Value + SEP4 + FieldByName('Descrip').Value;
        WriteLn(myFile, lineTextRev);
        next;
        inc(cnt);
        StatusBar1.Panels[5].text := IntToStr(cnt);
        StatusBar1.Refresh;
      end; // while not eof do
    end; // with spAssetTracker do

  finally
    spAssetTracker.Close;
    CloseFile(myFile);
    btnExport.Cursor := crDefault;
  end;
end;

procedure TForm2.btnRetrieveClick(Sender: TObject);
var
  N: integer;
  NULL: Variant;
begin
  try
    if ((LastSelected < 0) or (cbCompany.ItemIndex < 0)) then
    begin
      Showmessage('You must select an Asset To Export and a Company  first');
      cxRadioGroup1.SetFocus;
      exit;
    end;
    try
      btnRetrieve.Enabled := False;
      with spAssetTracker do
      begin
        Close;
        Parameters.ParamByName('@company').Value := company;
        if ((AcctCat = 'TRUCK') or (AcctCat = 'AUTO INS PREMIUM')) then
          Parameters.ParamByName('@charge_cov').Value := charge_cov
        else
          Parameters.ParamByName('@charge_cov').Value := NULL;
        Parameters.ParamByName('@AcctCat').Value := AcctCat;
        Parameters.ParamByName('@Amount').Value := Amount;
        If AcctCat = 'AUTO INS PREMIUM' then
          Parameters.ParamByName('@LossAmt').Value := PremiumLoss
        else Parameters.ParamByName('@LossAmt').Value := '';
        Parameters.ParamByName('@Descrip').Value := Descrip;
        Parameters.ParamByName('@TransDate').Value := TransDate;
        btnRetrieve.Cursor := crSQLWait;
        Open;
        btnRetrieve.Cursor := crDefault;
      end;
    except
      on E: Exception do
      begin
        btnExport.Enabled := false;
        ShowMessage(E.Message);
      end;
    end;
  btnExport.Enabled := True;
  finally
    cxRadioGroup1.ItemIndex := -1;
  end;
end;

procedure TForm2.btnSaveAmountsClick(Sender: TObject);
var
  AceIni: TIniFile;
  aPath: string;
begin
  aPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));

  try
    AceIni := TIniFile.Create(aPath + 'Ace.ini');
    AceIni.WriteString('AMOUNTS', 'Truck', TruckAmt);
    AceIni.WriteString('AMOUNTS', 'Premium', PremiumAmt);
    AceIni.WriteString('AMOUNTS', 'PremiumLoss', PremiumLoss);
    AceIni.WriteString('AMOUNTS', 'Equipment', EquipmentAmt);
  finally
    AceIni.Free;
  end;
end;

procedure TForm2.cxEndDatePropertiesChange(Sender: TObject);
begin
  processDate := cxEndDate.Date;
  TransDate := FormatDateTime('mm/dd/yyyy',processDate);
  If not(GetPayPeriod) then
  begin
      ShowMessage('There is no pay period for this date.  See IT support to add more periods.');
      PayPeriod := 'NONE';
  end;

  StatusBar1.panels[3].text := PayPeriod;
end;

procedure TForm2.cxRadioGroup1Click(Sender: TObject);
const
  DESCRIP_TRUCK = 'Truck (equipment) Charges';
  DESCRIP_PREMIUM = 'Auto Insurance Premium';

  DESCRIP_EQUIP = 'Equipment Charges (excl. Trucks)';
  ACCT_CAT_TRUCK = 'TRUCK';
  ACCT_CAT_PREMIUM = 'AUTO INS PREMIUM';
  ACCT_CAT_EQUIP =  'EQUIPMENT';

  LEVEL0_TRUCK = 'Truck Charges';
  LEVEL0_PREMIUM ='Auto Ins Premium';
  LEVEL0_PREMIUM_LOSS ='Auto Ins Loss';
  LEVEL0_EQUIP = 'Equipment Charges';

  LEVEL0='Level0';
begin
  case cxRadioGroup1.ItemIndex of
    0: //truck
      begin
        charge_cov := true;
        Descrip:= DESCRIP_TRUCK;
        AcctCat:= ACCT_CAT_TRUCK;
        Amount :=  TruckAmt;
        Level0Title:= LEVEL0+Sep1+LEVEL0_TRUCK;
        LastSelected:= 0;
        lblAsset.Caption := 'Active Asset: Truck Charges';
      end;
    1: // Insurance
      begin
        Descrip:= DESCRIP_PREMIUM;
        AcctCat:= ACCT_CAT_PREMIUM;
        Amount :=   PremiumAmt;
        Level0Title:= LEVEL0+Sep1+LEVEL0_PREMIUM;
        Level0TitleRev := LEVEL0+Sep1+LEVEL0_PREMIUM_LOSS;
        LastSelected:= 1;
        lblAsset.Caption := 'Active Asset: AUTO INS PREMIUM';
      end;
    2: // Equipment
      begin
        Descrip:= DESCRIP_EQUIP;
        AcctCat:= ACCT_CAT_EQUIP;
        Amount :=  EquipmentAmt;
        Level0Title:= LEVEL0+Sep1+LEVEL0_EQUIP;
        LastSelected:= 2;
        lblAsset.Caption := 'Active Asset: EQUIPMENT';
      end;
  end;
  btnRetrieve.Enabled := true;
end;

procedure TForm2.edtEquipAmtChange(Sender: TObject);
begin
  EquipmentAmt:= edtEquipAmt.text;
end;

procedure TForm2.edtLossAmtChange(Sender: TObject);
begin
   PremiumLoss := edtLossAmt.Text;
end;

procedure TForm2.edtPremiumAmtChange(Sender: TObject);
begin
  PremiumAmt:= edtPremiumAmt.text;
end;

procedure TForm2.edtTruckAmtChange(Sender: TObject);
begin
  TruckAmt:= edtTruckAmt.Text;
end;

function TForm2.EnDeCrypt(const Value : String) : String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;


procedure TForm2.FormActivate(Sender: TObject);
begin
  if ReadINI then
    connectDB;
  cxEndDate.Date := Today();

  processDate := cxEndDate.Date;
  GetPayPeriod;
  StatusBar1.panels[3].text := PayPeriod;
end;

procedure TForm2.FormCreate(Sender: TObject);
  function GetAppVersionStr: string;
  var
    Exe: string;
    Size, Handle: DWORD;
    Buffer: TBytes;
    FixedPtr: PVSFixedFileInfo;
  begin
    Exe := ParamStr(0);
    Size := GetFileVersionInfoSize(PChar(Exe), Handle);
    if Size = 0 then
      RaiseLastOSError;
    SetLength(Buffer, Size);
    if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
      RaiseLastOSError;
    if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
      RaiseLastOSError;
    Result := Format('%d.%d.%d.%d',
      [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
       LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
       LongRec(FixedPtr.dwFileVersionLS).Hi,  //release
       LongRec(FixedPtr.dwFileVersionLS).Lo]) //build
  end;

begin
  if AdoConn.Connected then  AdoConn.Close;
    StatusBar1.Panels[7].Text :=  GetAppVersionStr;
end;

function TForm2.GetPayPeriod:boolean;
begin
  result := false;
  qryPayPeriod.Parameters.ParamByName('ProcessDate').Value := processDate;
  try
    qryPayPeriod.Open;
    payPeriod := qryPayPeriod.FieldByName('PayPeriod').AsString;
    result := not(qryPayPeriod.FieldByName('PayPeriod').IsNull);
  finally
    qryPayPeriod.close;
  end;

end;

end.
