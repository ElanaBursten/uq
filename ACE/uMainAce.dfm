object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Welcome to A.C.E.  --  Asset Cost Exporter'
  ClientHeight = 585
  ClientWidth = 933
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 113
    Width = 933
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object Splitter2: TSplitter
    Left = 0
    Top = 469
    Width = 933
    Height = 12
    Cursor = crVSplit
    Align = alTop
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 933
    Height = 113
    Align = alTop
    TabOrder = 0
    object lblWeekEnding: TLabel
      Left = 56
      Top = 13
      Width = 63
      Height = 13
      Caption = 'Process Date'
    end
    object Label1: TLabel
      Left = 656
      Top = 13
      Width = 45
      Height = 13
      Caption = 'Company'
    end
    object cxEndDate: TcxDateEdit
      Left = 41
      Top = 30
      Properties.SaveTime = False
      Properties.ShowTime = False
      Properties.OnChange = cxEndDatePropertiesChange
      TabOrder = 0
      Width = 112
    end
    object cxRadioGroup1: TcxRadioGroup
      Left = 200
      Top = 0
      Caption = '    Asset To Export   '
      Properties.Items = <
        item
          Caption = 'Trucks Equipment Charges'
        end
        item
          Caption = 'Auto Insurance Premiums'
        end
        item
          Caption = 'Equipment Charges'
        end>
      Style.BorderColor = clDefault
      Style.BorderStyle = ebsNone
      TabOrder = 1
      OnClick = cxRadioGroup1Click
      Height = 107
      Width = 185
    end
    object cbCompany: TComboBox
      Left = 656
      Top = 30
      Width = 121
      Height = 21
      TabOrder = 2
      OnChange = cbCompanyChange
      Items.Strings = (
        'Utiliquest'
        'Locating Inc')
    end
    object btnSaveAmounts: TButton
      Left = 800
      Top = 28
      Width = 89
      Height = 25
      Caption = 'Save Amounts'
      TabOrder = 3
      OnClick = btnSaveAmountsClick
    end
    object pnlAmounts: TPanel
      Left = 368
      Top = 0
      Width = 249
      Height = 110
      TabOrder = 4
      object Label3: TLabel
        Left = 31
        Top = 54
        Width = 40
        Height = 13
        Caption = 'Premium'
      end
      object Label4: TLabel
        Left = 150
        Top = 54
        Width = 21
        Height = 13
        Caption = 'Loss'
      end
      object Label2: TLabel
        Left = 103
        Top = 2
        Width = 42
        Height = 13
        Caption = 'Amounts'
      end
      object edtTruckAmt: TEdit
        Left = 8
        Top = 21
        Width = 65
        Height = 21
        NumbersOnly = True
        TabOrder = 0
        OnChange = edtTruckAmtChange
      end
      object edtLossAmt: TEdit
        Left = 176
        Top = 51
        Width = 65
        Height = 21
        NumbersOnly = True
        TabOrder = 1
        OnChange = edtLossAmtChange
      end
      object edtPremiumAmt: TEdit
        Left = 77
        Top = 51
        Width = 65
        Height = 21
        NumbersOnly = True
        TabOrder = 2
        OnChange = edtPremiumAmtChange
      end
      object edtEquipAmt: TEdit
        Left = 8
        Top = 80
        Width = 65
        Height = 21
        NumbersOnly = True
        TabOrder = 3
        OnChange = edtEquipAmtChange
      end
    end
  end
  object pnlMid: TPanel
    Left = 0
    Top = 116
    Width = 933
    Height = 353
    Align = alTop
    TabOrder = 1
    object DBGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 931
      Height = 351
      Align = alClient
      TabOrder = 0
      object DBGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsAssetTracker
        DataController.DetailKeyFieldNames = 'taskID'
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Kind = skCount
            FieldName = 'Level1'
            Column = DBGrid1DBTableView1Level1
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skCount
            FieldName = 'Level1'
            Column = DBGrid1DBTableView1Level1
            DisplayText = 'N='
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.FooterAutoHeight = True
        object DBGrid1DBTableView1Level1: TcxGridDBColumn
          DataBinding.FieldName = 'Level1'
        end
        object DBGrid1DBTableView1projectID: TcxGridDBColumn
          DataBinding.FieldName = 'projectID'
        end
        object DBGrid1DBTableView1taskID: TcxGridDBColumn
          DataBinding.FieldName = 'taskID'
        end
        object DBGrid1DBTableView1acctCat: TcxGridDBColumn
          DataBinding.FieldName = 'acctCat'
        end
        object DBGrid1DBTableView1transDate: TcxGridDBColumn
          DataBinding.FieldName = 'transDate'
        end
        object DBGrid1DBTableView1units: TcxGridDBColumn
          DataBinding.FieldName = 'units'
        end
        object DBGrid1DBTableView1Amount: TcxGridDBColumn
          DataBinding.FieldName = 'Amount'
        end
        object DBGrid1DBTableView1Descrip: TcxGridDBColumn
          DataBinding.FieldName = 'Descrip'
        end
        object DBGrid1DBTableView1company: TcxGridDBColumn
          DataBinding.FieldName = 'company'
        end
        object DBGrid1DBTableView1last_name: TcxGridDBColumn
          DataBinding.FieldName = 'last_name'
        end
        object DBGrid1DBTableView1charge_cov: TcxGridDBColumn
          DataBinding.FieldName = 'charge_cov'
        end
        object DBGrid1DBTableView1EarningsType: TcxGridDBColumn
          DataBinding.FieldName = 'EarningsType'
        end
      end
      object DBGrid1Level1: TcxGridLevel
        GridView = DBGrid1DBTableView1
      end
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 481
    Width = 933
    Height = 85
    Align = alClient
    TabOrder = 2
    object lblAsset: TLabel
      Left = 25
      Top = 32
      Width = 280
      Height = 33
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnRetrieve: TcxButton
      Left = 328
      Top = 32
      Width = 113
      Height = 25
      Caption = 'Retrive Records'
      Enabled = False
      TabOrder = 0
      OnClick = btnRetrieveClick
    end
    object btnExport: TcxButton
      Left = 568
      Top = 32
      Width = 89
      Height = 25
      Caption = 'Export Data'
      Enabled = False
      TabOrder = 1
      OnClick = btnExportClick
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 566
    Width = 933
    Height = 19
    Panels = <
      item
        Text = 'Connected to'
        Width = 80
      end
      item
        Width = 120
      end
      item
        Text = 'Pay Period'
        Width = 70
      end
      item
        Width = 100
      end
      item
        Text = 'Exported'
        Width = 60
      end
      item
        Width = 70
      end
      item
        Text = 'Version'
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object dsAssetTracker: TDataSource
    DataSet = spAssetTracker
    Left = 80
    Top = 308
  end
  object AdoConn: TADOConnection
    CommandTimeout = 60
    ConnectionTimeout = 30
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 80
    Top = 380
  end
  object qryPayPeriod: TADOQuery
    Connection = AdoConn
    Parameters = <
      item
        Name = 'ProcessDate'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Declare @ProcessDate Date;'
      'set @ProcessDate = :ProcessDate'
      'SELECT '#39'P'#39'+[short_period] as PayPeriod'
      '  FROM [dbo].[dycom_period]'
      '  where @ProcessDate between [starting] and [ending]')
    Left = 80
    Top = 252
  end
  object spAssetTracker: TADOStoredProc
    Connection = AdoConn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 60
    ProcedureName = 'asset_export;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@company'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@charge_cov'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@AcctCat'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = '@Amount'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@LossAmt'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@Descrip'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@TransDate'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 80
    Top = 196
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 792
    Top = 521
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clWhite
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 13158655
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      TextColor = clMaroon
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 13158655
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      TextColor = clMaroon
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 15461375
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 15461375
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12103888
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Microsoft Sans Serif'
      Font.Style = []
      TextColor = clMaroon
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 9211088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12103888
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Microsoft Sans Serif'
      Font.Style = []
      TextColor = 7364768
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 5855675
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxGridTableViewStyleSheet1: TcxGridTableViewStyleSheet
      Styles.ContentEven = cxStyle12
      Styles.ContentOdd = cxStyle10
      Styles.Footer = cxStyle5
      BuiltIn = True
    end
    object TreeListStyleSheetRoseLarge: TcxTreeListStyleSheet
      Caption = 'Rose (large)'
      Styles.Content = cxStyle4
      Styles.Inactive = cxStyle8
      Styles.Selection = cxStyle11
      Styles.BandBackground = cxStyle1
      Styles.BandHeader = cxStyle2
      Styles.ColumnHeader = cxStyle3
      Styles.ContentEven = cxStyle5
      Styles.ContentOdd = cxStyle6
      Styles.Footer = cxStyle7
      Styles.Indicator = cxStyle9
      Styles.Preview = cxStyle10
      BuiltIn = True
    end
  end
  object cxStyleRepository2: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle12: TcxStyle
    end
  end
end
