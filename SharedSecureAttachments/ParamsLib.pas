unit ParamsLib;

{$ASSERTIONS ON}

interface

uses
  System.SysUtils
  , System.RegularExpressions
  , System.Generics.Collections
  ;

type
  TParamConfig = class;
  TBaseCommandConfig = class;
  TCommandConfig = class;
  TRootCommandConfig = class;
  TBaseClass = class;

  TOnParamHelpPrinter = reference to procedure(const AHelpStr:string='');
  TOnParamConfig = reference to function(AParamConfig:TParamConfig; var AError:string):boolean;
  TOnCommandConfig = reference to function(ACommandConfig:TBaseCommandConfig; var AError:string):boolean;

  TParamConfigOption = (poHidden,poHasRequiredValue,poHasNoValue);
  TParamConfigOptions = set of TParamConfigOption;

  TCommandConfigOption = (coDefault);
  TCommandConfigOptions = set of TCommandConfigOption;

  TParamConfigHelper = class helper for TBaseClass
    function AddParam(const AParamName, AHelpString, ADefaultValue:string;
      AParamChar:char = #0; AOptions:TParamConfigOptions = [poHasNoValue]; AValidator:TOnParamConfig = nil):TParamConfig;
    function AddCommand(const ACommandName:string; AExecutor:TOnCommandConfig; AOptions:TCommandConfigOptions = []):TCommandConfig;
  end;

  EParamConfig = class(Exception);

  TBaseClass = class
  public
  end;

  TParamConfig = class(TBaseClass)
  private
    function GetBooleanValue: boolean;
    function GetIntegerValue: integer;
    procedure SetBooleanValue(const Value: boolean);
    procedure SetIntegerValue(const Value: integer);
  protected
    FOptions: TParamConfigOptions;
    FCommandConfig:TBaseCommandConfig;
    FValue:string;
    FDefaultValue: string;
    FParamChar: char;
    FHelpString: string;
    FParamName: string;
    FHasValue: boolean;
    FOnValidate:TOnParamConfig;
    procedure SetValue(const Value: string);
    function GetValue: string; virtual;
    constructor create; virtual;
  public
    procedure Clear;
    property Options:TParamConfigOptions read FOptions write FOptions default [];
    property ParamName:string read FParamName write FParamName;
    property DefaultValue:string read FDefaultValue write FDefaultValue;
    property HelpString:string read FHelpString write FHelpString;
    property ParamChr:char read FParamChar write FParamChar default #0;
    property HasValue:boolean read FHasValue write FHasValue default false;
    property Command:TBaseCommandConfig read FCommandConfig;
    property OnValidate:TOnParamConfig read FOnValidate;

    property Value:string read GetValue write SetValue; 
    property asBoolean:boolean read GetBooleanValue write SetBooleanValue;
    property asInteger:integer read GetIntegerValue write SetIntegerValue;
  end;

  TBaseCommandConfig = class(TBaseClass)
  protected
    FExecuteCount:integer;
    FParamList:TObjectList<TParamConfig>;
    FOnExecute:TOnCommandConfig;
    function GetParamConfig(AParamName: variant): TParamConfig; virtual;
    function NewParamConfig:TParamConfig; virtual;
    function GetRootCommand: TRootCommandConfig; virtual; abstract;
    function GetCommandName: string; virtual; abstract;
    constructor create; virtual;
  public
    destructor Destroy; override;

    procedure Execute; virtual;

    property RootCommand:TRootCommandConfig read GetRootCommand;
    property CommandName:string read GetCommandName;
    property Param[AParamName:variant]:TParamConfig read GetParamConfig; default;
    property OnExecute:TOnCommandConfig read FOnExecute;
  end;

  TCommandConfig = class(TBaseCommandConfig)
  protected
    FRoot: TRootCommandConfig;
    FCommandName:string;
    FOptions: TCommandConfigOptions;
    function GetRootCommand: TRootCommandConfig; override;
    function GetCommandName: string; override;
    function GetParamConfig(AParamName: variant): TParamConfig; override;
  public
    procedure Execute; override;
  end;

//  TRootCommandConfig = class;
  TRootCommandInitializer = reference to procedure(ARootCommand:TRootCommandConfig);

  TRootCommandConfig = class(TBaseCommandConfig)
  protected
    FCommandList:TObjectList<TBaseCommandConfig>;
    FEXeName, FCopyright, FDescription:string;
    FInitializer:TRootCommandInitializer;
    function NewCommandConfig:TCommandConfig; virtual;
    function GetCommandConfig(ACommandName: string): TBaseCommandConfig;
    function GetRootCommand: TRootCommandConfig; override;
    function GetCommandName: string; override;
  public
    constructor create; override;
    destructor Destroy; override;

    procedure Init(const ACopyright, ADescription:string; AInitializer:TRootCommandInitializer);
    function Clone:TRootCommandConfig;
    procedure Execute; override;

    procedure Parse(ADefaultExecute:TOnCommandConfig; ACommandLine:string = ''); virtual;
    procedure PrintHelp(AHelpPrinter:TOnParamHelpPrinter); virtual;

    property Command[ACommandName:string]:TBaseCommandConfig read GetCommandConfig;
    
    property EXEName:string read FEXEName write FEXEName;
    property Copyright:string read FCopyright write FCopyright;
    property Description:string read FDescription write FDescription;
  end;

function ParamHelper:TRootCommandConfig;

implementation

uses
  Winapi.windows
  , System.ioutils
  , System.Math
  ;

threadvar
  FParamHelper:TRootCommandConfig;

function ParamHelper:TRootCommandConfig;
begin
  if FParamHelper = nil then
    FParamHelper := TRootCommandConfig.create;
  result := FParamHelper
end;

{ TParamConfig }

procedure TParamConfig.Clear;
begin
  FHasValue := false;
  FValue := ''
end;

constructor TParamConfig.create;
begin
  inherited create;
  clear;
  FParamChar := #0;
  FOptions := []
end;

function TParamConfig.GetBooleanValue: boolean;
begin
  try
    result := not (
      (GetValue = '')
      or (GetValue = '0')
      or (CompareText(GetValue, 'false') = 0)
      )
  except
    result := false
  end;
end;

function TParamConfig.GetIntegerValue: integer;
begin
  try
    result := StrToInt(GetValue)
  except
    result := 0
  end;
end;

function TParamConfig.GetValue: string;
begin
  if (FValue <> '') and HasValue then
    result := FValue
  else
    result := FDefaultValue
end;

procedure TParamConfig.SetBooleanValue(const Value: boolean);
begin
  if not value then
    SetValue('')
  else
  if not AsBoolean then
    AsInteger := 1;
  FHasValue := true
end;

procedure TParamConfig.SetIntegerValue(const Value: integer);
begin
  SetValue(IntToStr(value))
end;

procedure TParamConfig.SetValue(const Value: string);
begin
  FValue := value;
  FHasValue := true
end;

{ TBaseCommandConfig }

constructor TBaseCommandConfig.create;
begin
  inherited create;
  FParamList := TObjectList<TParamConfig>.create
end;

destructor TBaseCommandConfig.destroy;
begin
  freeandnil(FParamList);
  inherited;
end;

procedure TBaseCommandConfig.Execute;
var
  AParamConfig:TParamConfig;
  error:string;
begin
  if FParamList.count > 0 then
    for AParamConfig in FParamList do
  //    if (poRequired in AParamconfig.Options) and not AParamConfig.HasValue then
  //      raise EParamConfig.CreateFmt('Parameter (%s) required for %s command',[AParamConfig.ParamName,CommandName])
  //    else
//      if (poHasRequiredValue in AParamconfig.Options) and (not AParamConfig.HasValue) then
//        raise EParamConfig.CreateFmt('Parameter (%s) for %s command does not have a value',[AParamConfig.ParamName,CommandName])
//      else
//      if (poHasNoValue in AParamconfig.Options) and AParamConfig.HasValue then
//        raise EParamConfig.CreateFmt('Parameter (%s) for %s command unexpectedly has a value',[AParamConfig.ParamName,CommandName])
      ;
  if assigned(FOnExecute) then
  begin
    inc(FExecuteCount);
    if not FOnExecute(self,error) then
      raise EParamConfig.CreateFmt('Command (%s) returned false: %s',[CommandName,error])
  end;
end;

function TBaseCommandConfig.GetParamConfig(AParamName:variant): TParamConfig;
begin
  for result in FParamList do
  begin
    if SameText(result.ParamName, AParamName) or (SameText(result.FParamChar, AParamName)) then
      exit
  end;
  result := nil
//  raise EParamConfig.CreateFmt('Paramenter not found: %s',[AParamName]);
end;

//function TBaseCommandConfig.GetRootCommand: TRootCommandConfig;
//begin
//
//end;

function TBaseCommandConfig.NewParamConfig: TParamConfig;
begin
  result := TParamConfig.create
end;

{ TBaseClass }

function TParamConfigHelper.AddCommand(
  const ACommandName:string
  ; AExecutor:TOnCommandConfig
  ; AOptions:TCommandConfigOptions):TCommandConfig;
var
  ARootCommand:TRootCommandConfig;
begin
  ARootCommand := nil;
  if self is TBaseCommandConfig then
    ARootCommand := TBaseCommandConfig(self).RootCommand
  else
  if self is TParamConfig then
    ARootCommand := TParamConfig(self).Command.RootCommand;
  Assert(ARootCommand <> nil,'ARootCommand = nil');
  result := ARootCommand.NewCommandConfig;
  result.FRoot := ARootCommand;
  result.FOnExecute := AExecutor;
  result.FCommandName := ACommandName;
  result.FOptions := AOptions;
  ARootCommand.FCommandList.Add(result)
end;

function TParamConfigHelper.AddParam(const AParamName, AHelpString, ADefaultValue:string;
      AParamChar:char = #0; AOptions:TParamConfigOptions = [poHasNoValue]; AValidator:TOnParamConfig = nil):TParamConfig;
var
  ACommandConfig:TBaseCommandConfig;
begin
  ACommandConfig := nil;
  if self is TRootCommandConfig then
    if TRootCommandConfig(self).FCommandList.Count = 0 then
      ACommandConfig := TRootCommandConfig(self)
    else
      ACommandConfig := TRootCommandConfig(self).FCommandList.Last
  else
  if self is TCommandConfig then
    ACommandConfig := TCommandConfig(self)
  else
  if self is TParamConfig then
    ACommandConfig := TParamConfig(self).FCommandConfig;
  if (ADefaultValue <> '1') and (ADefaultValue <> '') then
    Exclude(AOptions, poHasNoValue);
  Assert(ACommandConfig <> nil,'ACommandConfig = nil');
  result := ACommandConfig.NewParamConfig;
  result.FDefaultValue := ADefaultValue;
  result.FParamChar := AParamChar;
  result.FHelpString := AHelpString;
  result.FParamName := AParamName;
  result.FOptions := AOptions;
  result.FOnValidate := AValidator;
  result.FCommandConfig := ACommandConfig;
  result.FHasValue := false;// ADefaultValue <> '';
  if poHasNoValue in AOptions then
  begin
    result.Value := ADefaultValue;
    result.FHasValue := false;
  end;
  ACommandConfig.FParamList.Add(result)
end;

{ TRootCommandConfig }

function TRootCommandConfig.Clone: TRootCommandConfig;
begin
  result := TRootCommandConfig.create;
  result.Init(FCopyright, FDescription, FInitializer);
end;

constructor TRootCommandConfig.create;
begin
  inherited;
  FCommandList := TObjectList<TBaseCommandConfig>.Create
end;

destructor TRootCommandConfig.destroy;
begin
  freeandnil(FCommandList);
  inherited;
end;

procedure TRootCommandConfig.Execute;
var
  ABaseCommand:TBaseCommandConfig;
  ACommand:TCommandConfig absolute ABaseCommand;
begin
  inherited;
//  if FExecuteCount = 0 then
//  for ABaseCommand in FCommandList do
//    if (ABaseCommand is TCommandConfig) and (coDefault in ACommand.FOptions) and (ACommand.FExecuteCount = 0) then
//      ACommand.DoOnCommandExecute
end;

function TRootCommandConfig.GetCommandConfig(ACommandName: string): TBaseCommandConfig;
begin
  for result in FCommandList do
    if CompareText(result.CommandName, ACommandName) = 0 then
      exit;
   raise EParamConfig.CreateFmt('Command not found: %s',[ACommandName]);
end;

function TRootCommandConfig.GetCommandName: string;
begin
  result := FEXeName
end;

function TRootCommandConfig.GetRootCommand: TRootCommandConfig;
begin
  result := self
end;

procedure TRootCommandConfig.Init;
begin
  FCopyright := ACopyright;
  FDescription := ADescription;
  FInitializer := AInitializer;
  FInitializer(self)
end;

function TRootCommandConfig.NewCommandConfig: TCommandConfig;
begin
  result := TCommandConfig.create
end;

// -abc --firstparam --secondparam=paramvalue firstcommand -p 1

procedure TRootCommandConfig.Parse(ADefaultExecute:TOnCommandConfig; ACommandLine:string = '');
var
  LRegEx: TRegEx;
//  AWorkParamStr:string;
//  AMatches:TMatchCollection;
  AMatch:TMatch;
//  AGroup:TGroup;
  paramIndex:integer;
  currCommand:TBaseCommandConfig;
  currParamType, currParamStr:string;
  lastParam:TParamConfig;
  currParamChar:char;
//  error:string;
//  group:TGroup;
begin
  lastParam := nil;
  if ACommandLine = '' then
    ACommandLine := GetCommandLine;

  FOnExecute := ADefaultExecute;
  currCommand := self;
  paramIndex := 0;
  LRegEx := TRegEx.create('^((?<type>=|--|-|)("(?<p1>[^"]+)"|''(?<p2>[^'']+)''|(?<p3>[^ =]+)))( *|(?<rest1>[=]{1}.*)| +(?<rest2>.*))$',[roExplicitCapture]);
  repeat
    AMatch := LRegEx.Match(ACommandLine);

    if AMatch.Groups.Count = 0 then
      raise EParamConfig.CreateFmt('Error parsing "%s"',[ACommandline]);

//    Writeln(AMatch.Value);
//    Writeln(AMatch.Length);
//    for group in AMatch.Groups do
//      writeln(group.value);
    currParamStr := AMatch.Groups['p1'].Value;
    if AMatch.Groups.Count > 3 then
      currParamStr := currParamStr + AMatch.Groups['p2'].Value;
    if AMatch.Groups.Count > 4 then
      currParamStr := currParamStr + AMatch.Groups['p3'].Value;
    if paramIndex = 0 then
      FEXeName := TPath.GetFileNameWithoutExtension(currParamStr)
    else
    begin
      currParamType := AMatch.Groups['type'].Value;

      if (lastParam = nil) and (currParamType = '') then
      begin
        currCommand.Execute;
        currCommand := Command[currParamStr]
      end
      else
      if (lastParam = nil) and (currParamType = '=') then
        raise EParamConfig.CreateFmt('Unexpected value: %s',[currParamStr])
      else
      if currParamType = '' then
        if (poHasNoValue in lastParam.Options) then
        begin
          currCommand.Execute;
          currCommand := Command[currParamStr]
        end
        else
        begin
          lastParam.Value := currParamStr;
          lastParam := nil
        end
      else
      if AMatch.Groups['type'].Value = '=' then
      begin
        lastParam.Value := currParamStr;
        lastParam := nil
      end
      else
      if AMatch.Groups['type'].Value = '-' then
        for currParamChar in currParamStr do
        begin
          lastParam := currCommand.Param[currParamChar];
          if lastParam=nil then
            raise EParamConfig.CreateFmt('Unexpected param type (%s) for parameter (%s)',[currParamType,currParamChar])
          else
            lastParam.asBoolean := true
        end
      else
      if currParamType = '--' then
      begin
        lastParam := currCommand.Param[currParamStr];
          if lastParam=nil then
            raise EParamConfig.CreateFmt('Unexpected param type (%s) for parameter (%s)',[currParamType,currParamStr])
          else
            lastParam.asBoolean := true;
      end
      else
        raise EParamConfig.CreateFmt('Unexpected param type (%s) for parameter (%s)',[currParamType,currParamStr])
    end;

    if AMatch.Groups.Count = 7 then
      ACommandLine := AMatch.Groups['rest2'].Value
    else
    if AMatch.Groups.Count = 6 then
      ACommandLine := AMatch.Groups['rest1'].Value
    else
      break;
    inc(paramIndex)
  until false;
//  if currCommand is TRootCommandConfig then
    currCommand.Execute
end;

procedure TRootCommandConfig.PrintHelp(AHelpPrinter: TOnParamHelpPrinter);
var
  ACommandConfig:TBaseCommandConfig;

  procedure OutDent(LeftMargin, RightMargin:integer; const AMsg:string; Args:array of const);
  var
    s:string;
    tmpRight:integer;
  begin
    s := format(AMsg, Args);
    while Length(s) > 0 do
    begin
      tmpRight := RightMargin;
      if Length(s) > RightMargin then
        for tmpRight := Min(RightMargin, Length(s)) downto Min(10, length(s)) do
          if Copy(s, tmpRight, 1) = ' ' then
            break;

      AHelpPrinter(Copy(s, 1, tmpRight));
      Delete(s, 1, tmpRight);
      if Trim(s) <> '' then
        s := format('%*s%s',[LeftMargin,' ',s])
    end;
  end;

  function CheckParamChar(AChar:char):string;
  begin
    if AChar = #0 then
      Result := ''
    else
      result := format('-%s |',[AChar])
  end;

  function CheckDefault(const ADef:string):string;
  begin
    if ADef = '' then
      Result := ''
    else
      result := format(' [Default: %s]',[ADef])
  end;

  procedure PrintCommandHelp(ACommand:TBaseCommandConfig);
  var
    AParamConfig:TParamConfig;
  begin
    if ACommand is TCommandConfig then
    begin
      AHelpPrinter;
      AHelpPrinter(Format('  Command %s', [ACommand.CommandName]));
    end;

    for AParamConfig in ACommand.FParamList do
    begin
      if poHidden in AParamConfig.FOptions then
        continue;
      OutDent(40, 120, '    %5s --%-25s   %s%s', [
        CheckParamChar(AParamConfig.FParamChar)
        , AParamConfig.FParamName
        , AParamConfig.FHelpString
        , CheckDefault(AParamConfig.DefaultValue)
      ]);
      AHelpPrinter;
    end;
  end;

begin
  AHelpPrinter(self.Description);
  AHelpPrinter(self.Copyright);
  AHelpPrinter('Global Parameters');
  PrintCommandHelp(self);
  for ACommandConfig in FCommandList do
    PrintCommandHelp(ACommandConfig)
end;

{ TCommandConfig }

procedure TCommandConfig.Execute;
begin
  inherited;
end;

function TCommandConfig.GetCommandName: string;
begin
  result := FCommandName
end;

function TCommandConfig.GetParamConfig(AParamName: variant): TParamConfig;
begin
  result := inherited;
  if result = nil then
    Result := RootCommand[AParamName];
  if result = nil then
    raise EParamConfig.CreateFmt('Param not found: %s',[string(AParamName)]);
end;

function TCommandConfig.GetRootCommand: TRootCommandConfig;
begin
  result := FRoot
end;

end.
