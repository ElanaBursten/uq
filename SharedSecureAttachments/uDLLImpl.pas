unit uDLLImpl;

{$ASSERTIONS ON}

interface

uses
  System.classes
  , System.JSON
  , System.SyncObjs
  , System.SysUtils
  , System.Threading
  , Data.Cloud.AmazonAPI
  , awslib
  , uLoggerLib
  ;

type
  ILog = interface(IUnknown)
    ['{0398EAC9-0249-4B2F-AF20-14AEF4BF1322}']
    function DoLog(APriority:TLogPriority; const ALogMsg, AMsg:widestring):widestring;
  end;

procedure SetLog(ALog:ILog); stdcall; export;

function PutImage(
  const
    Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
    , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
    , Service                                           // execute-api
    , StagingBucket                                     // dii-utq-attachments-prod
    , StagingFolder                                     // api/LOC_UTL/
    , Region                                            // us-east-1

    , Algorithm                                         // AWS4-HMAC-SHA256
    , SignedHeaders                                     // accept;content-type;host;x-amz-date
    , ContentType                                       // application/json
    , AcceptedValues                                    // application/octet-stream,application/json

    , awskey
    , awssecret:widestring
  ; AttachmentID:integer
  ; const AOrigFilepath, AOrigFilename:widestring
  ; var ErrorMsg:widestring
):boolean; stdcall; export;

function GetImage(
  const
    Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
    , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
    , Service                                           // execute-api
    , StagingBucket                                     // dii-utq-attachments-prod
    , StagingFolder                                     // api/LOC_UTL/
    , Region                                            // us-east-1

    , Algorithm                                         // AWS4-HMAC-SHA256
    , SignedHeaders                                     // accept;content-type;host;x-amz-date
    , ContentType                                       // application/json
    , AcceptedValues                                    // application/octet-stream,application/json

    , awskey
    , awssecret
    , ASaveFilename:widestring
  ; AttachmentID:integer
  ; var AAWSFilename:widestring
  ; var ErrorMsg:widestring
):boolean; stdcall; export;

function GetImageurl(
  const
    Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
    , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
    , Service                                           // execute-api
    , StagingBucket                                     // dii-utq-attachments-prod
    , StagingFolder                                     // api/LOC_UTL/
    , Region                                            // us-east-1

    , Algorithm                                         // AWS4-HMAC-SHA256
    , SignedHeaders                                     // accept;content-type;host;x-amz-date
    , ContentType                                       // application/json
    , AcceptedValues                                    // application/octet-stream,application/json

    , awskey
    , awssecret:widestring
  ; AttachmentID:integer
  ; var AImageURL:widestring
  ; var ErrorMsg:widestring
):boolean; stdcall; export;

procedure Finalize; stdcall; export;

//function Encrypt(const input:string):string; export;
//function Decrypt(const input:string):string; export;

exports
  PutImage
  , GetImage
  , GetImageURL
  , SetLog
  , Finalize
//  , Encrypt
//  , Decrypt
  ;

implementation

uses
  System.IOUtils
  , Winapi.ShellAPI
  , Winapi.Windows
  , REST.Types
  , REST.Client
  , uKeyDecryption
  , dxCore
  ;

procedure Finalize;
begin
  dxCore.dxFinalize;
  SetLog(nil)
end;

function Encrypt(const input:string):string;
begin
  result := _EncryptText(input)
end;

function Decrypt(const input:string):string;
begin
  result := _DecryptText(input)
end;

procedure SetLog(ALog:ILog);
begin
  if ALog = nil then
    SetLogger(
      TNullLogger.create
    )
  else
    SetLogger(
      TSimpleLogger.Create(
        function(APriority:TLogPriority; const ALogMsg, AMsg:string):string
        begin
          result := ALog.DoLog(APriority, widestring(ALogMsg), widestring(AMsg))
        end
        , 'AWSAPI'
        , lpVerbose
        , lpVerbose
        )
      );
end;

procedure Buildup(
  const
    Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
    , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
    , Service                                           // execute-api
    , StagingBucket                                     // dii-utq-attachments-prod
    , StagingFolder                                     // api/LOC_UTL/
    , Region                                            // us-east-1

    , Algorithm                                         // AWS4-HMAC-SHA256
    , SignedHeaders                                     // accept;content-type;host;x-amz-date
    , ContentType                                       // application/json
    , AcceptedValues                                    // application/octet-stream,application/json

    , awskey
    , awssecret:string
    ; var amzCI:TCustomAmazonConnectionInfo
    ; var amzAPI:TCustomAmazonService
  );
var
  _key, _secret:string;
begin
  amzAPI := nil;

  // create amazon connection
  amzCI := TCustomAmazonConnectionInfo.Create(
    Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
    , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
    , Service                                           // execute-api
    , StagingBucket                                     // dii-utq-attachments-prod
    , StagingFolder                                     // api/LOC_UTL/
    , Region                                            // us-east-1

    , Algorithm                                         // AWS4-HMAC-SHA256
    , SignedHeaders                                     // accept;content-type;host;x-amz-date
    , ContentType                                       // application/json
    , AcceptedValues                                    // application/octet-stream,application/json
  );

  amzCI.AccountName := Decrypt(awskey);
  amzCI.AccountKey := Decrypt(awssecret);
  amzCI.Protocol:='HTTPS';   //qm-766 sr

//  _key := '8JhNY8dczQP/OTxjIeHRatKuid/j4wuwq73Azrof7KM=';
//  _secret := 'WjBtHbKwjbkhfs1yLS64HXC71WhfdwKZFN60Sb0lDXb0PqzlkOTHcDn9WanYI4ZQ';
//
//  amzCI.AccountName := Decrypt(_key);
//  amzCI.AccountKey := Decrypt(_secret);

  Log(lpVerbose, '%s'#13#10'%s', [amzCI.AccountName, amzCI.AccountKey]);

  amzAPI := TCustomAmazonService.Create(amzCI)
end;

procedure Cleanup(
  var amzCI:TCustomAmazonConnectionInfo
  ; var amzAPI:TCustomAmazonService
);
begin
  freeandnil(amzAPI);
  freeandnil(amzCI)
end;

function PUTIMAGE;
var
  AFileStream:TStream;
  amzCI:TCustomAmazonConnectionInfo;
  amzAPI:TCustomAmazonService;
  APIRslt, AFilehash:string;
  AttachmentFilesize:Int64;
begin
  AFileStream := nil;
  try
    Buildup(
      Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
      , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
      , Service                                           // execute-api
      , StagingBucket                                     // dii-utq-attachments-prod
      , StagingFolder                                     // api/LOC_UTL/
      , Region                                            // us-east-1

      , Algorithm                                         // AWS4-HMAC-SHA256
      , SignedHeaders                                     // accept;content-type;host;x-amz-date
      , ContentType                                       // application/json
      , AcceptedValues                                    // application/octet-stream,application/json
      , awskey
      , awssecret
      , amzCI
      , amzAPI
    );
    try
      TCustomAmazonService.GetFileHash(AOrigFilepath, AFilehash, AttachmentFilesize, AFileStream);
      Assert(amzAPI.PutAttachment(
        attachmentid
        , AOrigFilename
        , AFilehash
        , AFileStream
        , APIRslt
        , true
      ), format('PUTIMAGE: PutAttachment %d %s: %s', [
        attachmentid
        , AOrigFilename
        , APIRslt ]));

      Log(lpInfo,'PUTIMAGE: Successful %d %s: %s', [
        attachmentid
        , AOrigFilename
        , APIRslt ]);
    finally
      FreeAndNil(AFileStream);
      Cleanup(
        amzCI
        , amzAPI
      )
    end;
    Result := true;
  except
    on e:exception do
    begin
      ErrorMsg := Format('!!%s: %s', [e.ClassName, e.Message]);
      result := false
    end;
  end;
end;

function GETIMAGE;
var
  AttachmentDir:string;
  AFileStream:TMemoryStream;
  AFileSize:int64;
  amzCI:TCustomAmazonConnectionInfo;
  amzAPI:TCustomAmazonService;
  APIRslt:string;
  _AAWSFilename:string;
begin
  try
    Buildup(
      Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
      , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
      , Service                                           // execute-api
      , StagingBucket                                     // dii-utq-attachments-prod
      , StagingFolder                                     // api/LOC_UTL/
      , Region                                            // us-east-1

      , Algorithm                                         // AWS4-HMAC-SHA256
      , SignedHeaders                                     // accept;content-type;host;x-amz-date
      , ContentType                                       // application/json
      , AcceptedValues                                    // application/octet-stream,application/json

      , awskey
      , awssecret
      , amzCI
      , amzAPI
      );
    try
      Assert(
        amzAPI.GETIMAGE(
          AttachmentID
          , APIRslt
          , AFileSize
          , _AAWSFilename
          , AFileStream
          , true)
        , format('GETIMAGE: %d failed!!'#13#10'%s',[AttachmentID,APIRslt]));
      AAWSFilename := _AAWSFilename;

      assert(AFileStream.Size > 0, 'GETIMAGE: Filesize = 0');

      if TFile.Exists(ASaveFilename) then
        TFile.Delete(ASaveFilename);

      AttachmentDir := TPath.GetDirectoryName(ASaveFilename);
      if not TDirectory.Exists(AttachmentDir) then
        ForceDirectories(AttachmentDir);

      AFileStream.SaveToFile(ASaveFilename);

      Log(lpInfo, 'GETIMAGE: Successful');
    finally
      Cleanup(
        amzCI
        , amzAPI
      )
    end;
    result := true
  except
    on e:exception do
    begin
      ErrorMsg := Format('!!%s: %s', [e.ClassName, e.Message]);
      result := false
    end;
  end;
end;

function GETIMAGEURL;
var
//  AImageURL,
//    ErrorMsg:string;
  amzCI:TCustomAmazonConnectionInfo;
  amzAPI:TCustomAmazonService;
  APIRslt, AFilehash:string;
  AttachmentFilesize:Int64;
  _AImageURL:string;
begin
  ErrorMsg := '';
  try
    Buildup(
      Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
      , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
      , Service                                           // execute-api
      , StagingBucket                                     // dii-utq-attachments-prod
      , StagingFolder                                     // api/LOC_UTL/
      , Region                                            // us-east-1

      , Algorithm                                         // AWS4-HMAC-SHA256
      , SignedHeaders                                     // accept;content-type;host;x-amz-date
      , ContentType                                       // application/json
      , AcceptedValues                                    // application/octet-stream,application/json

      , awskey
      , awssecret
      , amzCI
      , amzAPI
      );
    try
      Assert(amzAPI.GetImageURL(
        attachmentid
        , 30000
        , _AImageURL), 'OPENIMAGEURL: failed');
      AImageURL := _AImageURL;

      Log(lpInfo,'OPENIMAGEURL: Successful',[]);
    finally
      Cleanup(
        amzCI
        , amzAPI
      )
    end;
    result := true
  except
    on e:exception do
    begin
      ErrorMsg := Format('!!%s: %s', [e.ClassName, e.Message]);
      result := false
    end;
  end;
end;

initialization
  SetLog(nil)

end.
