library DSSS;

{$R 'QMVersion.res' '..\QMVersion.rc'}
   {$R '..\QMIcon.res'}

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  System.SysUtils,
  System.Classes,
  AWSLib in 'AWSLib.pas',
  uDLLImpl in 'uDLLImpl.pas',
  uLoggerLib in 'uLoggerLib.pas',
  uSecLib in 'uSecLib.pas',
  DCPcrypt2 in '..\thirdparty\DCPcrypt\DCPcrypt2.pas',
  DCPsha1 in '..\thirdparty\DCPcrypt\Hashes\DCPsha1.pas',
  DCPconst in '..\thirdparty\DCPcrypt\DCPconst.pas',
  DCPbase64 in '..\thirdparty\DCPcrypt\DCPbase64.pas',
  uKeyDecryption in 'uKeyDecryption.pas';



begin
end.
