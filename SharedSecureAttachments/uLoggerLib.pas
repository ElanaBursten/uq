unit uLoggerLib;

interface

uses
  classes
  , System.Sysutils
  , System.Generics.Collections
  , System.Syncobjs
  , System.Threading
  ;


type
  TLogPriority = (lpError,lpWarning,lpInfo,lpDebug,lpVerbose);

  ILogger = interface(IUnknown)
  ['{B1A25AF5-8C05-489F-B6D8-934B7806872D}']
    procedure DefLog(const AMsg:string);
    procedure DefLogFmt(const AFmtStr:string; const Args:array of const);
    procedure Log(APriority:TLogPriority; const AMsg:string);
    procedure LogFmt(APriority:TLogPriority;  const AFmtStr:string; const Args:array of const);
    procedure LogDebug(const AMsg:string);
    procedure LogDebugFmt(const AFmtStr:string; const Args:array of const);
    procedure LogError(Ex:Exception;const AMsg:string);
    procedure LogErrorFmt(Ex:Exception;const AFmtStr:string; const Args:array of const);
//    procedure FlushLog;
    procedure StopLog;
    procedure WaitForLogStop;
  end;

  TOnLog = reference to function(APriority:TLogPriority; const ALogMsg, AMsg:string):string;

  TNullLogger = class(TInterfacedObject, ILogger)
  protected
    procedure DefLog(const AMsg:string);
    procedure DefLogFmt(const AFmtStr:string; const Args:array of const);
    procedure Log(APriority:TLogPriority; const AMsg:string);
    procedure LogFmt(APriority:TLogPriority;  const AFmtStr:string; const Args:array of const);
    procedure LogDebug(const AMsg:string);
    procedure LogDebugFmt(const AFmtStr:string; const Args:array of const);
    procedure LogError(Ex:Exception;const AMsg:string);
    procedure LogErrorFmt(Ex:Exception;const AFmtStr:string; const Args:array of const);
    procedure FlushLog;
    procedure StopLog;
    procedure WaitForLogStop;
  public
    constructor create;
  end;

  TSimpleLogger = class(TInterfacedObject, ILogger)
  protected
    FPriorityThreshold, FDefaultPriority: TLogPriority;
    FOnLog:TOnLog;
    FModulename:string;
    class function FormatLog(APriority:TLogPriority; ADate:TDateTime; AThreadID:integer; const AModuleName, AMsg:string):string; virtual;

    procedure DoLog(APriority:TLogPriority; const AMsg:string); virtual;

    procedure DefLog(const AMsg:string);
    procedure DefLogFmt(const AFmtStr:string; const Args:array of const);
    procedure Log(APriority:TLogPriority; const AMsg:string);
    procedure LogFmt(APriority:TLogPriority;  const AFmtStr:string; const Args:array of const);
    procedure LogDebug(const AMsg:string);
    procedure LogDebugFmt(const AFmtStr:string; const Args:array of const);
    procedure LogError(Ex:Exception;const AMsg:string);
    procedure LogErrorFmt(Ex:Exception;const AFmtStr:string; const Args:array of const);
    procedure FlushLog; virtual;
    procedure StopLog; virtual;
    procedure WaitForLogStop; virtual;
  public
    constructor create(AOnLog:TOnLog; const AModulename:string; AThresholdPriority:TLogPriority =
{$IFDEF DEBUG}
      lpDebug
{$ELSE}
      lpInfo
{$ENDIF}
      ; ADefaultPriority:TLogPriority =
{$IFDEF DEBUG}
      lpDebug
{$ELSE}
      lpInfo
{$ENDIF}
      );

    property ThresholdPriority:TLogPriority read FPriorityThreshold write FPriorityThreshold;
    property DefaultPriority:TLogPriority read FDefaultPriority write FDefaultPriority;
  end;

  TLogMsgRec = record
    msg:string;
    module:string;
    dt:TDateTime;
    p:TLogPriority;
    threadid:integer;
  end;

  TAsyncLogger = class(TSimpleLogger)
  protected
    FThreadPool:TThreadPool;
    FLogMsg:TList<TLogMsgRec>;
    FLogEvent, FLogActive, FLogStopped:TSimpleEvent;
    procedure DoLog(APriority:TLogPriority; const AMsg:string); override;
  public
    constructor create(AThreadPool:TThreadPool;AOnLog:TOnLog;const AModulename:string; AThresholdPriority:TLogPriority =
{$IFDEF DEBUG}
      lpDebug
{$ELSE}
      lpInfo
{$ENDIF}
      ; ADefaultPriority:TLogPriority =
{$IFDEF DEBUG}
      lpDebug
{$ELSE}
      lpInfo
{$ENDIF}
    );

    procedure FlushLog; override;
    procedure StopLog; override;
    procedure WaitForLogStop; override;
  end;

  TSynchroObjectHelper = class helper for TSynchroObject
    function IsSet(ATimeout:Cardinal = INFINITE):boolean;
  end;

  TSyncHelper = class helper for TObject
  public
    procedure lock(AProc:TProc);
  end;

procedure sync(AProc:TThreadProcedure);      overload;
function sync(AProc:TFunc<Boolean>):boolean; overload;
function sync(AProc:TFunc<string>):string; overload;

procedure SetLogger(ALogger:ILogger);
procedure FlushLog;

procedure DefLog(const AMsg:string); overload;
procedure DefLog(const AFmtStr:string; const Args:array of const); overload;
procedure Log(APriority:TLogPriority; const AMsg:string); overload;
procedure Log(APriority:TLogPriority; const AFmtStr:string; const Args:array of const); overload;
procedure LogDebug(const AMsg:string); overload;
procedure LogDebug(const AFmtStr:string; const Args:array of const); overload;
procedure LogError(Ex:Exception;const AMsg:string); overload;
procedure LogError(Ex:Exception;const AFmtStr:string; const Args:array of const); overload;

implementation

//uses
//  Winapi.Windows
//  ;

var
  DefLogger:ILogger = nil;

procedure SetLogger(ALogger:ILogger);
begin
  DefLogger := ALogger
end;

procedure FlushLog;
begin
  DefLogger.StopLog
end;

procedure DefLog(const AMsg:string);
begin
  DefLogger.DefLog(AMsg)
end;

procedure DefLog(const AFmtStr:string; const Args:array of const);
begin
  DefLogger.DefLogFmt(AFmtStr,Args)
end;

procedure Log(APriority:TLogPriority; const AMsg:string); overload;
begin
  DefLogger.Log(APriority,AMsg)
end;

procedure Log(APriority:TLogPriority;  const AFmtStr:string; const Args:array of const);
begin
  DefLogger.LogFmt(APriority, AFmtStr, Args)
end;

procedure LogDebug(const AMsg:string);
begin
  DefLogger.LogDebug(AMsg)
end;

procedure LogDebug(const AFmtStr:string; const Args:array of const);
begin
  DefLogger.LogDebugFmt(AFmtStr,Args)
end;

procedure LogError(Ex:Exception;const AMsg:string);
begin
  DefLogger.LogError(Ex,AMsg)
end;

procedure LogError(Ex:Exception;const AFmtStr:string; const Args:array of const);
begin
  DefLogger.LogErrorFmt(Ex,AFmtStr,Args)
end;

procedure sync(AProc:TThreadProcedure);
begin
  TThread.Synchronize(
    nil
    , AProc
    );
end;

function sync(AProc:TFunc<Boolean>):boolean; overload;
var rslt:boolean;
begin
  TThread.Synchronize(
    nil
    , procedure
      begin
        rslt := AProc;
      end
    );
  result := rslt
end;

function sync(AProc:TFunc<string>):string; overload;
var rslt:string;
begin
  TThread.Synchronize(
    nil
    , procedure
      begin
        rslt := AProc;
      end
    );
  result := rslt
end;

{ TSyncHelper }

procedure TSyncHelper.lock(AProc: TProc);
begin
  TMonitor.Enter(Self);
  try
    AProc
  finally
    TMonitor.Exit(self)
  end
end;

{ TSynchroObjectHelper }

function TSynchroObjectHelper.IsSet;
begin
  result := WaitFor(ATimeout) = TWaitResult.wrSignaled
end;

{ TSimpleLogger }

procedure TSimpleLogger.Log(APriority:TLogPriority; const AMsg: string);
begin
  DoLog(APriority,AMsg)
end;

procedure TSimpleLogger.LogFmt(APriority:TLogPriority; const AFmtStr: string; const Args: array of const);
begin
  Log(APriority,Format(AFmtStr,Args))
end;

procedure TSimpleLogger.StopLog;
begin

end;

procedure TSimpleLogger.WaitForLogStop;
begin

end;

procedure TSimpleLogger.LogDebug(const AMsg: string);
begin
  Log(lpDebug,AMsg)
end;

procedure TSimpleLogger.LogDebugFmt(const AFmtStr: string;
  const Args: array of const);
begin
  LogDebug(format(AFmtStr,Args))
end;

procedure TSimpleLogger.LogError(Ex: Exception; const AMsg: string);
begin
  if ex <> nil then
    Log(lpError,format('%s:%s!! %s',[Ex.classname,Ex.message,AMsg]))
  else
    Log(lpError,format('!! %s',[AMsg]))
end;

procedure TSimpleLogger.LogErrorFmt(Ex: Exception; const AFmtStr: string;
  const Args: array of const);
begin
  LogError(Ex,format(AFmtStr,Args))
end;


constructor TSimpleLogger.create;
begin
  inherited create;
  FOnLog := AOnLog;
  FPriorityThreshold := AThresholdPriority;
  FDefaultPriority := ADefaultPriority;
  FModulename := AModulename;
end;

var
  CLogPrefix:array[TLogPriority] of string =
    ('!','?',':','#','.');

procedure TSimpleLogger.DefLog(const AMsg: string);
begin
  Log(FDefaultPriority, AMsg)
end;

procedure TSimpleLogger.DefLogFmt(const AFmtStr: string;
  const Args: array of const);
begin
  LogFmt(FDefaultPriority, AFmtStr, Args)
end;

threadvar
  ThreadID:integer;

procedure TSimpleLogger.DoLog(APriority: TLogPriority; const AMsg: string);
begin
  if assigned(FOnLog) and (APriority <= FPriorityThreshold) then
  begin
    if ThreadID = 0 then
      ThreadID := TThread.Current.ThreadID;
    FOnLog(APriority,FormatLog(APriority,Now,ThreadID,FModuleName,AMsg),AMsg)
  end
end;

procedure TSimpleLogger.FlushLog;
begin

end;

class function TSimpleLogger.FormatLog(APriority: TLogPriority; ADate:TDateTime; AThreadID:integer; const AModuleName, AMsg: string): string;
begin
//  if AModuleName = '' then
//    result := format('%s%s-%10s-%8d-%s',[
//      CLogPrefix[APriority]
//      , formatdatetime('yyyymmdd"T"hhnnss.zzz',ADate)
//      , ''
//      , ThreadID
//      , AMsg
//      ])
//  else
    result := format('%s%s-%10s-%8d-%s',[
      CLogPrefix[APriority]
      , formatdatetime('yyyymmdd"T"hhnnss.zzz',ADate)
      , AModuleName
      , AThreadID
      , AMsg
      ])
end;

{ TAsyncLogger }

constructor TAsyncLogger.create;
begin
  inherited create(AOnLog,AModulename,AThresholdPriority,ADefaultPriority);
  FThreadPool := AThreadPool;
  FLogMsg := TList<TLogMsgRec>.create;
  FLogEvent := TSimpleEvent.Create(nil, false, false, '');
  FLogActive := TSimpleEvent.Create(nil, True, True, '');
  FLogStopped := TSimpleEvent.Create(nil, True, false, '');

  FThreadPool.QueueWorkItem(
    procedure
    begin
      FlushLog
    end
  );
end;

procedure TAsyncLogger.FlushLog;
var msgrec:TLogMsgRec;
begin
  while FLogEvent.IsSet do
  try
//        TThread.Synchronize(
//          nil
//          , procedure
//          begin
        TMonitor.Enter(FLogMsg);
        try
          while FLogMsg.Count > 0 do
          begin
            msgrec := FLogMsg.Items[0];
            FLogMsg.Delete(0);
            TMonitor.Exit(FLogMsg);
            try
              FOnLog(msgrec.p,FormatLog(msgrec.p,msgrec.dt,msgrec.threadid,msgrec.module,msgrec.msg),msgrec.msg)
            finally
              TMonitor.Enter(FLogMsg)
            end
          end
        finally
          TMonitor.Exit(FLogMsg);
        end;
//          end
//          );
    if not FLogActive.IsSet(0) then
      break;
  finally
//    FLogEvent.ResetEvent
  end;
  FLogStopped.SetEvent
end;

procedure TAsyncLogger.StopLog;
begin
  FLogActive.ResetEvent;
  FLogEvent.SetEvent;
  WaitForLogStop
end;

procedure TAsyncLogger.WaitForLogStop;
begin
  FLogStopped.IsSet
end;

threadvar
  ASyncThreadID:integer;

procedure TAsyncLogger.DoLog(APriority: TLogPriority; const AMsg: string);
var
  AMsgRec:TLogMsgRec;
begin
  if APriority > FPriorityThreshold then
    exit;
  TMonitor.Enter(FLogMsg);
  try
    if ASyncThreadID = 0 then
      ASyncThreadID := TThread.Current.ThreadID;
    AMsgRec.p := APriority;
    AMsgRec.dt := now;
    AMsgRec.msg := AMsg;
    AMsgRec.module := FModulename;
    AMsgRec.threadid := ASyncThreadID;
    FLogMsg.Add(AMsgRec);
//    if FLogMsg.Count > 1024 then
//      FlushBuffer
  finally
    TMonitor.Exit(FLogMsg);
    FLogEvent.SetEvent
  end;
end;

{ TNullLogger }

constructor TNullLogger.create;
begin
  inherited create
end;

procedure TNullLogger.DefLog(const AMsg: string);
begin

end;

procedure TNullLogger.DefLogFmt(const AFmtStr: string;
  const Args: array of const);
begin

end;

procedure TNullLogger.FlushLog;
begin

end;

procedure TNullLogger.Log(APriority: TLogPriority; const AMsg: string);
begin

end;

procedure TNullLogger.LogDebug(const AMsg: string);
begin

end;

procedure TNullLogger.LogDebugFmt(const AFmtStr: string;
  const Args: array of const);
begin

end;

procedure TNullLogger.LogError(Ex: Exception; const AMsg: string);
begin

end;

procedure TNullLogger.LogErrorFmt(Ex: Exception; const AFmtStr: string;
  const Args: array of const);
begin

end;

procedure TNullLogger.LogFmt(APriority: TLogPriority; const AFmtStr: string;
  const Args: array of const);
begin

end;

procedure TNullLogger.StopLog;
begin

end;

procedure TNullLogger.WaitForLogStop;
begin

end;

initialization
  SetLogger(TNullLogger.create)

end.
