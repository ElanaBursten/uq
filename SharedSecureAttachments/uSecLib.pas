unit uSecLib;

interface

uses
  System.SysUtils
  , System.Classes
  , dcpcrypt2
  , DCPsha1
  , Math
  ;

function PBKDF2(const pass, salt: ansistring; count, kLen: Integer; hash: TDCP_hashclass): ansistring; overload;
function PBKDF2(const pass, salt: ansistring; count, kLen: Integer): ansistring; overload;

function String2Bytes(const Buffer: AnsiString): TBytes;
function Bytes2String(const Buffer: TBytes):AnsiString;

function String2Hex(const Buffer: AnsiString): string;
function TBytes2Hex(const Buffer: TBytes): string;
function UTF82Hex(const Buffer: UTF8String): string;

implementation

uses
  System.netencoding
  ;

function RPad(const x: ansistring; c: Char; s: Integer): ansistring;
var
  i: Integer;
begin
  Result := x;
  if Length(x) < s then
    for i := 1 to s-Length(x) do
        Result := Result + c;
end;

function XorBlock(const s, x: ansistring): ansistring; inline;
var
  i: Integer;
begin
  SetLength(Result, Length(s));
  for i := 1 to Length(s) do
    Result[i] := AnsiChar(Byte(s[i]) xor Byte(x[i]));
end;

function CalcDigest(const text: ansistring; dig: TDCP_hashclass):ansistring;
var
  x: TDCP_hash;
begin
  x := dig.Create(nil);
  try
    x.Init;
    x.UpdateStr(text);
    SetLength(Result, x.GetHashSize div 8);
    x.Final(Result[1]);
  finally
    x.Free;
  end;
end;

function CalcHMAC(const message:string; key: ansistring; hash: TDCP_hashclass): ansistring;
const
  blocksize = 64;
begin
  // Definition RFC 2104
  if Length(key) > blocksize then
    key := CalcDigest(key, hash);
  key := RPad(key, #0, blocksize);

  Result := CalcDigest(XorBlock(key, RPad('', #$36, blocksize)) + message, hash);
  Result := CalcDigest(XorBlock(key, RPad('', #$5c, blocksize)) + result, hash);
end;

function PBKDF1(const pass, salt: ansistring; count: Integer; hash: TDCP_hashclass): ansistring;
var
  i: Integer;
begin
  Result := pass+salt;
  for i := 0 to count-1 do
    Result := CalcDigest(Result, hash);
end;

function PBKDF2(const pass, salt: ansistring; count, kLen: Integer): ansistring; overload;
begin
  Result := PBKDF2(pass, salt, count, kLen, TDCP_sha1)
end;

function PBKDF2(const pass, salt: ansistring; count, kLen: Integer; hash: TDCP_hashclass): ansistring;

  function IntX(i: Integer): ansistring; inline;
  begin
    Result := Char(i shr 24) + Char(i shr 16) + Char(i shr 8) + Char(i);
  end;

var
  D, I, J: Integer;
  T, F, U: ansistring;
begin
  T := '';
  D := Ceil(kLen / (hash.GetHashSize div 8));
  for i := 1 to D do
  begin
    F := CalcHMAC(salt + IntX(i), pass, hash);
    U := F;
    for j := 2 to count do
    begin
      U := CalcHMAC(U, pass, hash);
      F := XorBlock(F, U);
    end;
    T := T + F;
  end;
  Result := Copy(T, 1, kLen);
end;

function UTF82Hex(const Buffer: UTF8String): string;
var c:ansichar;
begin
  result := '';
  for c in Buffer do
    result := result + format('%2.2x',[Ord(c)])
end;

function TBytes2Hex(const Buffer: TBytes): string;
begin
  SetLength(Result, Length(Buffer)*2);
  BinToHex(PChar(Buffer), PChar(Result), Length(Buffer)*2);
end;

function String2Hex(const Buffer: AnsiString): string;
begin
  SetLength(Result, Length(Buffer) * 2);
  BinToHex(PAnsiChar(Buffer), PChar(Result), Length(Buffer));
end;

function String2Bytes(const Buffer: AnsiString): TBytes;
var
  i:integer;
begin
  SetLength(Result, Length(buffer));
  for i := 1 to Length(buffer) do
    Result[i-1] := Byte(buffer[i]);
end;

function Bytes2String(const Buffer: TBytes):AnsiString;
var
  i:integer;
begin
  SetLength(Result, Length(buffer));
  for i := 1 to Length(buffer) do
    Result[i] := AnsiChar(buffer[i-1]);
end;

end.
