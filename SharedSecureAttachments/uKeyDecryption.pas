unit uKeyDecryption;

{.$DEFINE USEOPENSSL}

interface

function _EncryptText(const input:string):string;
function _DecryptText(const input:string):string;

implementation

uses
  Classes
  , System.sysutils
  , System.NetEncoding
  , uSecLib
{$IFDEF USEOPENSSL}
  , IdSSLOpenSSL
  , IdSSLOpenSSLHeaders
{$ENDIF}
  , dxCrypto
  , dxHash
  , dxCryptoAPI
  ;

var
  saltBytes:TBytes;
  pb:TBytes;

const
  Iterations =
//    1;
    1000;
  KeyLen = 256 div 8;
  IVLen = 128 div 8;

function AES_Crypt(input, passwordBytes:TBytes; encrypt:boolean):TBytes;
var
  AlgID, ProvID:Integer;
  key, iv:TBytes;
  rslt:string;
  provider:IdxCryptoProvider;
  cryptKey:IdxCryptoKey;
  irslt:integer;
  _pass, _salt, _rslt:TBytes;
begin
  TdxCryptoAlgorithms.GetInfo('AES', 256, AlgID, ProvID);
  provider := TdxCryptoProvider.Create(CryptoProviderEnhancedRSA_AES, provid);

  SetLength(key, KeyLen + IVLen);
{$IFDEF USEOPENSSL}
  Assert(PKCS5_PBKDF2_HMAC_SHA1(PAnsiChar(passwordBytes), length(passwordBytes), PAnsiChar(saltBytes), Length(saltBytes), Iterations, Length(key), PAnsiChar(key))=1,'PKCS5_PBKDF2_HMAC_SHA1 failed!!');
{$ELSE}
  key := String2Bytes(PBKDF2(Bytes2String(passwordBytes), Bytes2String(saltBytes), Iterations, KeyLen + IVLen));
{$ENDIF}
  SetLength(iv, IVLen);
  move(key[KeyLen], iv[0], IVLen);
  SetLength(key, KeyLen);
//  writeln('key = ',TNetEncoding.Base64.EncodeBytesToString(key));
//  writeln('iv = ',TNetEncoding.Base64.EncodeBytesToString(iv));

  cryptKey := TdxCryptoKey.Create(provider, AlgID, key);

  cryptKey.SetIV(iv);
  cryptKey.SetChainingMode(CRYPT_MODE_CBC);

  if encrypt then
    result := TdxCipher.Encrypt(cryptKey, input, True)
  else
    result := TdxCipher.Decrypt(cryptKey, input, True);
//  writeln('result = ',TBytes2Hex(result));
end;

function CryptText(const input:string; encrypt:boolean):string;
var
  bytesInput:TBytes;
  b:Byte;
  c:char;
begin
  if encrypt then
    bytesInput := TEncoding.UTF8.GetBytes(input)
  else
    bytesInput := TNetEncoding.Base64.DecodeStringToBytes(input);
  bytesInput := AES_Crypt(bytesInput, pb, false);
  if encrypt then
    result := TNetEncoding.Base64.EncodeBytesToString(bytesInput)
  else
    Result :=
//      TEncoding.UTF8.GetString(bytesInput)
      Bytes2String(bytesInput)
end;

function _EncryptText(const input:string):string;
begin
  result := CryptText(input, true)
end;

function _DecryptText(const input:string):string;
begin
  result := CryptText(input, false)
end;

initialization
  saltBytes := TBytes.create(145, 24, 32, 45, 85, 6, 17, 48, 9, 170);
  with TdxSHA256HashAlgorithm.Create do
  try
    pb := Calculate(TBytes.create(15, 24, 32, 45, 85, 67, 17, 48, 9, 110, 95, 34, 37, 45))
  finally
    free
  end;
{$IFDEF USEOPENSSL}
  LoadOpenSSLLibrary
{$ENDIF}

end.
