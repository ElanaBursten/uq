program DSSSCLI;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  System.Threading,
  System.SyncObjs,
  System.TimeSpan,
  Winapi.windows,
  ParamsLib in 'ParamsLib.pas',
  uLogFile in 'uLogFile.pas',
  uDLLIntf in 'uDLLIntf.pas',
  uLoggerLib in 'uLoggerLib.pas';

var
  rslt:integer = 0;
  iTests, iErrors:integer;
  iKBTests:Int64 = 0;
  start:TDateTime;
  elapsed:TTimeSpan;
  threadWaiter:TMultiThreadWaiter;
  signaled:THandle;
begin

  try
    start := now;
    threadWaiter := TMultiThreadWaiter.create;
    iTests := 0;

    uDLLIntf.SetLog(TLogger.Create(
      function (APriority:TLogPriorityIntf; const ALogMsg, AMsg:widestring):widestring
      begin
        result := ALogMsg;
        Log(TLogPriority(Ord(APriority)), AMsg)
      end
    ));

//    Writeln('Attach debugger, then press ENTER');
//    Readln;

    repeat
      threadWaiter.AddThread(TThreadPool.Current, True,
        procedure(AThreadDone, AThreadStarted:TSimpleEvent)
        var
          i:integer;
        begin
          try
            try
              try
                ParamHelper.Init('', 'SSSCLI 1.2 - Console companion for DSSS.DLL'
                  , procedure(ParamHelper:TRootCommandConfig)
                    begin
                      ParamHelper
//                      // GLOBAL PARAMETERS
                        .AddParam('pause','Pause after completing','','p',[poHidden,poHasNoValue])
                        .AddParam('verbose','Verbose output','','v',[poHidden,poHasNoValue])
                        .AddParam('logfile','Log file','DSSSLOG.log','l', [poHasRequiredValue])
                        .AddParam('logfilesize','Max Log file size','1G',#0, [poHasRequiredValue])
                        .AddParam('logfilerollovers','Log file rollover file count','5',#0, [poHasRequiredValue])
                        .AddParam('repeat','To test performance', '1', #0, [poHidden, poHasRequiredValue])
                        .AddParam('threads','To test performance', '1', #0, [poHidden, poHasRequiredValue])

                        .AddParam('Host','Host paramater'
//                          , ''
                          , 'p8bchtkc9h.execute-api.us-east-1.amazonaws.com'
                          , #0, [poHasRequiredValue])
                        .AddParam('RestURL','RestURL paramater'
//                          , ''
                          , '/develop/qmattach/v1/attachments/LOC_UTL/'
                          ,#0, [poHasRequiredValue])
                        .AddParam('Service','Service paramater'
//                          , ''
                          , 'execute-api'
                          ,#0, [poHasRequiredValue])
                        .AddParam('StagingBucket','StagingBucket paramater'
//                          , ''
                          , 'dii-utq-attachments-dev'
                          ,#0, [poHasRequiredValue])
                        .AddParam('StagingFolder','StagingFolder paramater'
//                          , ''
                          , 'develop/LOC_UTL/'
                          ,#0, [poHasRequiredValue])
                        .AddParam('Region','Region paramater'
//                          , ''
                          , 'us-east-1'
                          ,#0, [poHasRequiredValue])

                        .AddParam('Algorithm','Algorithm paramater'
//                          , ''
                          ,'AWS4-HMAC-SHA256'
                          ,#0, [poHasRequiredValue])
                        .AddParam('SignedHeaders','SignedHeaders paramater'
//                          , ''
                          ,'accept;content-type;host;x-amz-date'
                          ,#0, [poHasRequiredValue])

                        .AddParam('ContentType','ContentType paramater'
//                          , ''
                          ,'application/json'
                          ,#0, [poHasRequiredValue])
                        .AddParam('AcceptedValues','AcceptedValues paramater'
//                          , ''
                          ,'application/octet-stream,application/json'
                          ,#0, [poHasRequiredValue])

                        .AddParam('awskey', 'AWS key'
//                          , ''
                          , '8BXUnsehvmhWwx29mKlueFy16GHoSeT7LoaZ+MWhHKM='
                          , #0, [poHasRequiredValue])
                        .AddParam('awssecret', 'AWS secret'
//                          , ''
                          , 'MAK+fyWGiSRPmiM++nxXgCREuR4wVjmUl4BCQla9dWtRvbhtxK2Ceq54vUPwbtu1'
                          , #0, [poHasRequiredValue])

                        .AddParam('AttachmentID', 'AttachmentID param', '', #0, [poHasRequiredValue])

                        .AddCommand('PUTIMAGE',
                          function (ACommandConfig:TBaseCommandConfig; var AError:string):boolean
                          var _AError:widestring;
                          begin
                            result := uDLLIntf.PutImage(
                              PChar(ACommandConfig.Param['Host'].Value)
                              , PChar(ACommandConfig.Param['RestURL'].Value)
                              , PChar(ACommandConfig.Param['Service'].Value)
                              , PChar(ACommandConfig.Param['StagingBucket'].Value)
                              , PChar(ACommandConfig.Param['StagingFolder'].Value)
                              , PChar(ACommandConfig.Param['Region'].Value)

                              , PChar(ACommandConfig.Param['Algorithm'].Value)
                              , PChar(ACommandConfig.Param['SignedHeaders'].Value)
                              , PChar(ACommandConfig.Param['ContentType'].Value)
                              , PChar(ACommandConfig.Param['AcceptedValues'].Value)

                              , PChar(ACommandConfig.Param['awskey'].Value)
                              , PChar(ACommandConfig.Param['awssecret'].Value)

                              , ACommandConfig.Param['AttachmentID'].asInteger
                              , PChar(ACommandConfig.Param['OrigFilepath'].Value)
                              , PChar(ACommandConfig.Param['OrigFilename'].Value)
                              , _AError
                            );
                            AError := _AError;
                            if result then
                              Log( lpInfo, 'PUTIMAGE: Success')
                            else
                              Log(lpError, 'PUTIMAGE: ERROR = %s', [AError])
                          end)
                          .AddParam('OrigFilepath', 'OrigFilepath param', '', #0, [poHasRequiredValue])
                          .AddParam('OrigFilename', 'OrigFilename param', '', #0, [poHasRequiredValue])

                        .AddCommand('GETIMAGE',
                          function (ACommandConfig:TBaseCommandConfig; var AError:string):boolean
                          var AAWSFilename:widestring;
                            _AError:widestring;
                          begin
                            result := uDLLIntf.GetImage(
                              PChar(ACommandConfig.Param['Host'].Value)
                              , PChar(ACommandConfig.Param['RestURL'].Value)
                              , PChar(ACommandConfig.Param['Service'].Value)
                              , PChar(ACommandConfig.Param['StagingBucket'].Value)
                              , PChar(ACommandConfig.Param['StagingFolder'].Value)
                              , PChar(ACommandConfig.Param['Region'].Value)

                              , PChar(ACommandConfig.Param['Algorithm'].Value)
                              , PChar(ACommandConfig.Param['SignedHeaders'].Value)
                              , PChar(ACommandConfig.Param['ContentType'].Value)
                              , PChar(ACommandConfig.Param['AcceptedValues'].Value)

                              , PChar(ACommandConfig.Param['awskey'].Value)
                              , PChar(ACommandConfig.Param['awssecret'].Value)

                              , PChar(ACommandConfig.Param['SaveFilename'].value)
                              , ACommandConfig.Param['AttachmentID'].asInteger
                              , AAWSFilename
                              , _AError
                            );
                            AError := _AError;
                            if result then
                              Log(lpInfo, 'GETIMAGE: Success AWSFilename = %s', [AAWSFilename])
                            else
                              Log(lpError, 'GETIMAGE: ERROR = %s', [AError])
                          end)
                          .AddParam('SaveFilename', 'SaveFilename param', '', #0, [poHasRequiredValue])

                        .AddCommand('GETIMAGEURL',
                          function (ACommandConfig:TBaseCommandConfig; var AError:string):boolean
                          var AURL:widestring;
                            _AError:widestring;
                          begin
                            result := uDLLIntf.GetImageURL(
                              PChar(ACommandConfig.Param['Host'].Value)
                              , PChar(ACommandConfig.Param['RestURL'].Value)
                              , PChar(ACommandConfig.Param['Service'].Value)
                              , PChar(ACommandConfig.Param['StagingBucket'].Value)
                              , PChar(ACommandConfig.Param['StagingFolder'].Value)
                              , PChar(ACommandConfig.Param['Region'].Value)

                              , PChar(ACommandConfig.Param['Algorithm'].Value)
                              , PChar(ACommandConfig.Param['SignedHeaders'].Value)
                              , PChar(ACommandConfig.Param['ContentType'].Value)
                              , PChar(ACommandConfig.Param['AcceptedValues'].Value)

                              , PChar(ACommandConfig.Param['awskey'].Value)
                              , PChar(ACommandConfig.Param['awssecret'].Value)

                              , ACommandConfig.Param['AttachmentID'].asInteger
                              , AURL
                              , _AError
                            );
                            AError := _AError;
                            if result then
                              Log(lpInfo, 'GETIMAGEURL: URL = %s', [AURL])
                            else
                              Log(lpError, 'GETIMAGEURL: ERROR = %s', [AError])
                          end)

//                        .AddCommand('ENCRYPT', uTestSethSecure.CommandDispatcher)
//                          .AddParam('input','String to Encrypt','','i',[poHasRequiredValue])
//
//                        .AddCommand('DECRYPT', uTestSethSecure.CommandDispatcher)
//                          .AddParam('input','String to Decrypt','','i',[poHasRequiredValue])
//
                        .AddCommand('HELP',
                          function (ACommandConfig:TBaseCommandConfig; var AError:string):boolean
                          begin
                            ParamHelper.PrintHelp(
                              procedure (const AHelpStr:string)
                              begin
                                Writeln(AHelpStr)
                              end
                            );
                            result := true
                          end)
                    end
                  );

                Assert(ParamCount > 0,'ParamCount = 0');

                ParamHelper.Parse(uLogFile.CommandDispatcher);
              finally
                AThreadStarted.SetEvent;
                InterlockedAdd(iTests, 1)
              end;

//              if ALastFileStream <> nil then
//                InterlockedAdd64(iKBTests, ALastFileStream.Size);

              try
                for i := 2 to RepeatCnt do
                try
                  try
                    elapsed := TTimeSpan.Subtract(Now, start);
                    Log(lpInfo, #13#10#13#10'==========Repeated %d times in %f secs (%.2f sec/test, %.2f/min)',[
                      i-1
                      , elapsed.totalseconds
                      , elapsed.totalseconds / (i-1)
                      , (i-1) / elapsed.totalseconds * 60
                    ]);
                    ParamHelper.Parse(nil);
//                    if ALastFileStream <> nil then
//                      InterlockedAdd64(iKBTests, ALastFileStream.Size);
                  finally
                    InterlockedAdd(iTests, 1)
                  end
                except
                  on E: Exception do
                  begin
                    LogError(e, 'DSSSCLI');
                    InterlockedAdd(iErrors, 1);
              //      Writeln(E.ClassName, ': ', E.Message);

                    rslt := 1;
                  end
                end;
              finally
//                uTestAPI.Finalize
              end
            except
              on E: Exception do
              begin
                LogError(e, 'DSSSCLI');
                InterlockedAdd(iErrors, 1);
          //      Writeln(E.ClassName, ': ', E.Message);

                rslt := 1;
              end
            end;
          finally
            AThreadDone.SetEvent
          end
        end
      );
    until (ThreadCnt = threadWaiter.ThreadCnt);

    threadWaiter.WaitForAll(signaled)
  except
    on E: Exception do
    begin
      LogError(e, 'DSSSCLI');
//      Writeln(E.ClassName, ': ', E.Message);

      rslt := 1;
    end;
  end;

  try
    elapsed := TTimeSpan.Subtract(Now, start);
    Log(lpInfo, #13#10#13#10'==========Repeated %d times (%d errors) in %f secs (%.2f sec/test, %.2f tests/min, %.2n KB/sec, %.2n MB/sec, %.2n MB/min)',[
      iTests
      , iErrors
      , elapsed.totalseconds
      , elapsed.totalseconds / iTests
      , iTests / elapsed.totalseconds * 60
      , iKBTests / 1024 / elapsed.totalseconds
      , iKBTests / 1024 / 1024 / elapsed.totalseconds
      , iKBTests / 1024 / 1024 / elapsed.totalseconds * 60
    ]);

    Log(lpInfo, 'Done');
//    CloseLog;

    if
//      true or
      (DebugHook <> 0)
      or
      PauseCMD
//      or
//      (rslt <> 0)
    then
    begin
      Writeln('Press ENTER to end (',rslt,')');
      Readln;
    end;

    uDLLIntf.Finalize
  finally
    halt(rslt)
  end

end.
