unit uDLLIntf;

interface

uses
{$IFDEF  VER320 || VER310 || VER300}
  System.SysUtils
{$ELSE}
  SysUtils
{$ENDIF}  
  ;

const
  SharedDLLFilename = 'DSSS.dll';

type
  TLogPriorityIntf = (lpError,lpWarning,lpInfo,lpDebug,lpVerbose);

  ILog = interface(IUnknown)
    ['{0398EAC9-0249-4B2F-AF20-14AEF4BF1322}']
    function DoLog(APriority:TLogPriorityIntf; const ALogMsg, AMsg:widestring):widestring;
  end;

  TOnDoLog = 
{$IFDEF VER320 || VER310 || VER300}
    reference to
{$ENDIF}    
    function (APriority:TLogPriorityIntf; const ALogMsg, AMsg:widestring):widestring;

  TLogger = class(TInterfacedObject, ILog)
  protected
    FOnLog:TOnDoLog;
    function DoLog(APriority:TLogPriorityIntf; const ALogMsg, AMsg:widestring):widestring;
  public
    constructor Create(OnLog:TOnDoLog);
    destructor destroy; override;
  end;

function PutImage(
  const
    Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
    , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
    , Service                                           // execute-api
    , StagingBucket                                     // dii-utq-attachments-prod
    , StagingFolder                                     // api/LOC_UTL/
    , Region                                            // us-east-1

    , Algorithm                                         // AWS4-HMAC-SHA256
    , SignedHeaders                                     // accept;content-type;host;x-amz-date
    , ContentType                                       // application/json
    , AcceptedValues                                    // application/octet-stream,application/json

    , awskey
    , awssecret:widestring
  ; AttachmentID:integer
  ; const AOrigFilepath, AOrigFilename:widestring
  ; var ErrorMsg:widestring
):boolean; stdcall; external SharedDLLFilename;

function GetImage(
  const
    Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
    , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
    , Service                                           // execute-api
    , StagingBucket                                     // dii-utq-attachments-prod
    , StagingFolder                                     // api/LOC_UTL/
    , Region                                            // us-east-1

    , Algorithm                                         // AWS4-HMAC-SHA256
    , SignedHeaders                                     // accept;content-type;host;x-amz-date
    , ContentType                                       // application/json
    , AcceptedValues                                    // application/octet-stream,application/json

    , awskey
    , awssecret
    , ASaveFilename:widestring
  ; AttachmentID:integer
  ; var AAWSFilename:widestring
  ; var ErrorMsg:widestring
):boolean; stdcall; external SharedDLLFilename;

function GetImageurl(
  const
    Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
    , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
    , Service                                           // execute-api
    , StagingBucket                                     // dii-utq-attachments-prod
    , StagingFolder                                     // api/LOC_UTL/
    , Region                                            // us-east-1

    , Algorithm                                         // AWS4-HMAC-SHA256
    , SignedHeaders                                     // accept;content-type;host;x-amz-date
    , ContentType                                       // application/json
    , AcceptedValues                                    // application/octet-stream,application/json

    , awskey
    , awssecret:widestring
  ; AttachmentID:integer
  ; var AImageURL:widestring
  ; var ErrorMsg:widestring
):boolean; stdcall; external SharedDLLFilename;

procedure SetLog(ALog:ILog); stdcall; external SharedDLLFilename;
procedure Finalize; stdcall; external SharedDLLFilename;

implementation

{ TLogger }

constructor TLogger.Create;
begin
  inherited create;
  FOnLog := OnLog;
end;

destructor TLogger.destroy;
begin
  inherited;
end;

function TLogger.DoLog(APriority:TLogPriorityIntf; const ALogMsg, AMsg:widestring):widestring;
begin
  if Assigned(FOnLog) then
    Result := FOnLog(APriority, ALogMsg, AMsg)
end;

initialization
  SetLog(nil)

finalization
//  Finalize

end.
