unit uLogFile;

interface

uses
  System.Sysutils
  , System.SyncObjs
  , System.Threading
  , System.Classes
  , uLoggerLib
  , ParamsLib
  ;

var
  LogFileName:string = '';
  LogFileSize:Int64;
  LogFileRollovers:integer = 5;
  LogPriority:TLogPriority = lpInfo;
  RepeatCnt:integer = 1;
  ThreadCnt:integer = 1;
  PauseCMD:boolean = false;

type
  TMultiThreadWaiter = class
  private
    function GetThreadCnt: integer;
  protected
    FThreads:array of THandle;
    FThreadCnt:integer;
  public
    constructor create;
    destructor destroy; override;

    procedure AddThread(AThreadPool:TThreadPool; waitForStart:boolean; threadAdder:TProc<TSimpleEvent,TSimpleEvent>);
    function WaitForAll(var ASignaled:THandle; ATimeout: integer = INFINITE):TWaitResult;

    property ThreadCnt:integer read GetThreadCnt;
  end;

function CommandDispatcher(ACommandConfig:TBaseCommandConfig;var error:string):boolean;
procedure CloseLog;

implementation

uses
  System.IOUtils
  , System.Math
  , System.Win.ComObj
  , Winapi.Windows
  ;

function ConvertFilesize2(AParam, ASuffix:string; AMultiplier:integer; var AValue:int64 ):Boolean;
begin
  result := false;
  AValue := pos(ASuffix, AParam);
  if AValue > 0 then
  begin
    Delete(AParam, AValue, Length(ASuffix));
    AValue := StrToIntDef(AParam, -1);
    if AValue <> -1 then
    begin
      result := true;
      AValue := AValue * AMultiplier
    end
  end
end;

function ConvertFilesize(const AParam:string):Int64;
begin
  if not (
    ConvertFilesize2(AParam, 'KB', 1024, result)
    or ConvertFilesize2(AParam, 'K', 1024, result)
    or ConvertFilesize2(AParam, 'GB', 1024 * 1024, result)
    or ConvertFilesize2(AParam, 'G', 1024 * 1024, result)
    )
  then
    result := StrToIntDef(AParam, 1024)
end;

var
  ATextWriter:TStreamWriter = nil;
  ALogSync:TCriticalSection = nil;//TCriticalSection = nil;
  ALogMutex:TMutex = nil;
  bNeedsInit:boolean = true;

function CommandDispatcher(ACommandConfig:TBaseCommandConfig;var error:string):boolean;
begin
  result := true;

  if not bNeedsInit then
    exit;

  bNeedsInit := false;

  PauseCMD := ACommandConfig.Param['pause'].asBoolean;
  LogFileName := ACommandConfig.Param['logfile'].Value;
  LogFileSize := ConvertFilesize(ACommandConfig.Param['logfilesize'].Value);
  LogFileRollovers := Max(1, StrToIntDef(ACommandConfig.Param['logfilerollovers'].Value, 1));
  RepeatCnt := Max(1, StrToIntDef(ACommandConfig.Param['repeat'].Value, 1));
  ThreadCnt := Max(1, StrToIntDef(ACommandConfig.Param['threads'].Value, 1));
  ATextWriter := nil;

//  TThreadPool.Current.SetMaxWorkerThreads(ThreadCnt+1);

  if ACommandConfig.Param['verbose'].asBoolean then
    LogPriority := lpVerbose;

  SetLogger(
    TSimpleLogger.create(
//    TAsyncLogger.create(
//    TThreadPool.Current,
    function(APriority:TLogPriority; const ALogMsg, AMsg:string):string
    var
      i:Integer;
      logfilenamecurrroll, logfilenameprevroll:string;
    begin
      Writeln(ALogMsg);
      if ALogSync = nil then
        exit;
      if (LogFileName <> '') then
      try
        ALogSync.Enter;
        try
          ALogMutex.Acquire;
          if ATextWriter = nil then
            ATextWriter := TStreamWriter.Create(LogFilename, FileExists(LogFilename));
          if (ATextWriter.BaseStream.Size > LogFileSize) then
          begin
            ATextWriter.free;
            // if LogFileRollovers = 1
            logfilenameprevroll := Format('%s.%d',[LogFilename,0]);
            for I := LogFileRollovers-1 downto 1 do
            begin
              logfilenamecurrroll := Format('%s.%d',[LogFilename,i]);
              logfilenameprevroll := Format('%s.%d',[LogFilename,i-1]);
              if FileExists(logfilenameprevroll) then
                TFile.Copy(logfilenameprevroll, logfilenamecurrroll, true);
            end;
            TFile.Copy(LogFileName,logfilenameprevroll,true);
  //          TFile.Delete(LogFilename);
            ATextWriter := TStreamWriter.Create(LogFilename, false);
          end;
          ATextWriter.WriteLine(ALogMsg);
          ATextWriter.Flush
        finally
          ATextWriter.close;
          freeandnil(ATextWriter);
          ALogMutex.Release
        end
      finally
        ALogSync.Leave
      end
    end
    , 'DSSSCLI'
    , LogPriority
    , LogPriority
//    ,
    ));

  Log(lpInfo, CmdLine);
  result := true
end;

procedure CloseLog;
var
  tmpLogSync:TCriticalSection;
begin
  FlushLog;
  ALogSync.Enter;
  tmpLogSync := ALogSync;
  try
    if ATextWriter <> nil then
      ATextWriter.close;
    FreeAndNil(ATextWriter)
  finally
    ALogSync := nil;
    tmpLogSync.Leave;
    tmpLogSync.free
  end;
end;

{ TMultiThreadWaiter }

procedure TMultiThreadWaiter.AddThread;
var
  AThreadDoneEvent, AThreadStartedEvent:TSimpleEvent;
begin
  AThreadDoneEvent := TSimpleEvent.Create(nil, True, False, '');
  AThreadStartedEvent := TSimpleEvent.Create(nil, True, False, '');
  self.lock(
    procedure
    begin
      inc(FThreadCnt);
      SetLength(FThreads, FThreadCnt);
      FThreads[FThreadCnt-1] := AThreadDoneEvent.Handle;

      AThreadPool.QueueWorkItem(
        procedure
        begin
          threadAdder(AThreadDoneEvent, AThreadStartedEvent);
          // safeguard since optimally should have been called in thread
          AThreadStartedEvent.SetEvent;
          AThreadDoneEvent.SetEvent
        end
      );

      if waitForStart then
        AThreadStartedEvent.IsSet
    end
    );
end;

constructor TMultiThreadWaiter.create;
begin
  FThreadCnt := 0
end;

destructor TMultiThreadWaiter.destroy;
begin

  inherited;
end;

function TMultiThreadWaiter.GetThreadCnt: integer;
var
  rslt:Integer;
begin
  self.lock(
    procedure
    begin
      rslt := FThreadCnt
    end
  );
  Result := rslt
end;

function TMultiThreadWaiter.WaitForAll;
var
  wr:TWaitResult;
  sig:THandle;
begin
  self.lock(
    procedure
    var
      WaitResult: DWORD;
    begin
      WaitResult := WaitForMultipleObjectsEx(FThreadCnt, @FThreads[0], true, INFINITE, False);
      case WaitResult of
        WAIT_ABANDONED_0..WAIT_ABANDONED_0 + MAXIMUM_WAIT_OBJECTS - 1:
          begin
            wr := wrAbandoned;
            sig := FThreads[WaitResult - WAIT_ABANDONED_0];
          end;
        WAIT_TIMEOUT: wr := wrTimeout;
        WAIT_FAILED: wr := wrError;
        WAIT_IO_COMPLETION: wr := wrIOCompletion;
        WAIT_OBJECT_0..WAIT_OBJECT_0 + MAXIMUM_WAIT_OBJECTS - 1:
          begin
            wr := wrSignaled;
            sig := FThreads[WaitResult - WAIT_OBJECT_0];
          end;
        else
          wr := wrError;
      end;
    end
  );
  Result := wr;
  ASignaled := sig
end;

initialization
  ALogSync := TCriticalSection.Create;
  ALogMutex := TMutex.Create(nil, False, 'DSSSCLILog')

finalization
//  Writeln('Finalizing uLogFile..');

end.
