unit AWSLib;

{$ASSERTIONS ON}

(********************************************************

    AWSLib
      written by Michael Mast, 2017-11-14
      implements client interfacing with API written by Steve Dillon

    This library leverages existing AWS component functionality,
    only overriding functionality that addresses
    custom header requirements introduced by the
    API author.  Generally, these customizations have to do with
    how the attachment filehash is included in creating
    the header signature.  It also encapsulates the 2
    stage process of posting an attachment, whereby it is
    first uploaded to a staging S3 bucket, then moved into
    a DynamoDB bucket.  It also implements a multipart
    file upload which is required by Amazon if a file upload
    is attempted > 100MB



 *******************************************************)

{*******************************************************}
{             Win32 API Interface Unit                  }
{*******************************************************}

interface

uses
  System.SysUtils,
  System.DateUtils,
  Data.Cloud.CloudAPI,
  Data.Cloud.AmazonAPI,
  System.Classes,
  System.IOUtils,
  System.StrUtils,
  System.Hash,
  System.Net.URLClient,
  System.JSON,
  REST.Json,
  REST.Utils
  ;

type
  TCustomAmazonAuthenticator = class(TAmazonAWS4Authentication)
  protected
    function BuildAuthorizationString(const StringToSign, DateISO, Region, AServiceName, AProtocol, SignedStrHeaders: string): string; reintroduce;
  end;

  TCustomAmazonConnectionInfo = class(TAmazonConnectionInfo)
  protected
    FHost                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
    , FRestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
    , FService                                           // execute-api
    , FStagingBucket                                     // dii-utq-attachments-prod
    , FStagingFolder                                     // api/LOC_UTL/
    , FRegion                                            // us-east-1
    , FAlgorithm                                         // AWS4-HMAC-SHA256
    , FSignedHeaders                                     // accept;content-type;host;x-amz-date
    , FContentType                                       // application/json
    , FAcceptedValues                                    // application/octet-stream,application/json
    , FProtocol
    :string
    ; FLogVerbose:boolean;
  public
    constructor create(
      const
        Host                                                // o9mwuetv47.execute-api.us-east-1.amazonaws.com
        , RestURL                                           // /prod/qmattach/v1/attachments/LOC_UTL
        , Service                                           // execute-api
        , StagingBucket                                     // dii-utq-attachments-prod
        , StagingFolder                                     // api/LOC_UTL/
        , Region                                            // us-east-1

        , Algorithm                                         // AWS4-HMAC-SHA256
        , SignedHeaders                                     // accept;content-type;host;x-amz-date
        , ContentType                                       // application/json
        , AcceptedValues                                    // application/octet-stream,application/json
        :UTF8string; ALogVerbose:Boolean = false); reintroduce;
  end;

  EAWSException = class(Exception) end;
  EAWSGetImage = class(EAWSException) end;
  EAWSPutAttachment = class(EAWSException) end;
  TCustomAmazonService = class;

  TProcessFilehash = function(const AFilename:string; out AFileHash:string; out AFilesize:Int64; out AStream:TStream):boolean of object;
  TProcessFilehash2 = function(AStream:TStream; out AFileHash:string; out AFilesize:Int64):boolean of object;
  TPreUploadCallback = reference to function(const AFilename, AFileHash:string; AFilesize:Int64; out AAttachmentID:Integer; out AAttachmentFilename:string):boolean;
  TPreUploadCallback2 = reference to function(ci:TCustomAmazonConnectionInfo; var Svc:TCustomAmazonService; const AFilename, AFileHash:string; AFilesize:Int64; out AAttachmentID:Integer; out AAttachmentFilename:string):boolean;

  TCustomAmazonService =  class(
    TAmazonStorageService
    )
  private
    function GetConnectionInfo: TCustomAmazonConnectionInfo;
   { private declarations }
  protected
   { protected declarations }
    FRegularService:boolean;
    FContentHash: string;
    FAuth:TCustomAmazonAuthenticator;

    function GetURI: string; virtual;

    function BuildStringToSign(const HTTPVerb: string; Headers, QueryParameters: TStringList;
                               const QueryPrefix, URL: string): string; override;
    function GetRequiredHeaderNames(out InstanceOwner: Boolean): TStrings; override;
    function CreateAuthInstance(const ConnectionInfo: TAmazonConnectionInfo): TCloudAuthentication; override;
    procedure PrepareRequestSignature(const HTTPVerb: string;
                                      const Headers, QueryParameters: TStringList;
                                      const StringToSign: string;
                                      var URL: string; Request: TCloudHTTP; var Content: TStream); override;

    function InitiateMultipartUpload(const BucketName, ObjectName: string; Metadata, Headers: TStrings;
      ACL: TAmazonACLType; ResponseInfo: TCloudResponseInfo): string;

  public
   { public declarations }
//      const ADeployment, ASubsidiary, AServiceName, AProtocol, ARegion, AHost, AStagingPath, AStagingBucket:string);

    // returns true if attachment is successfully uploaded.
    // AttachmentID is generate/created by the call to preUploadCallback and keeps this unit free of all DB references.
    // It also avoids having to open the file multiple times.
    // The reason for this is because the file must be "processed" before inserting a record:
    //
    // PutAttachment                preUploadCallback
    // =============                =================
    // 1) Open file
    // 2) Get filesize
    // 3) Calculate filehash
    // 4) ---------------------->   1) Generate new filename
    //                              2) Insert record, including new filename/filesize/filehash
    //                              3) Capture and return auto generated attachmentid
    //    <------------------------ 4)
    // 5) Upload file to Staging bucket with associated attachmentid
    // 6) Move file to DynamoDB    (move fails if record inserted by preUploadCallback does not exist with matching filehash)
    //

    class procedure GetFileHash(const AFilename:string; var AFilehash:string; var AFilesize:Int64; var AFilestream:TStream); overload;
    class procedure GetFileHash(AFilestream:TStream; var AFilehash:string; var AFilesize:Int64); overload;

    function GetMetaData(const AFilename: string; out AMetaData: TStrings): boolean;

    function PutAttachment(AttachmentID:Integer; AFilename:string; const AFilehash:string; AFileStream:TStream; var APIResult:string; forceUpload:boolean = true; LogVerbose:Boolean = false):Boolean; overload;
    function PutAttachment(var AttachmentID:Integer; const AOrigFilepath:string; var AFileStream:TStream; out APIResult:string; out AFilesize:int64; out AFilehash:string; out AFilename:string; preUploadCallback:TPreUploadCallback = nil; forceUpload:boolean = true; LogVerbose:Boolean = false):Boolean; overload;

    class function PutAttachment(
      Ci:TCustomAmazonConnectionInfo
      ; var Svc:TCustomAmazonService
      ; var AttachmentID:Integer
      ; const AOrigFilepath:string
      ; var AFileStream:TStream
      ; var APIResult:string
      ; var AFilesize:int64
      ; out AFilename:string
      ; preUploadCallback:TPreUploadCallback2
      ; forceUpload:boolean = true
      ; LogVerbose:Boolean = false
      ):Boolean; overload;

    // pretty self explanatory
    function GetImage(AttachmentID:Integer; const AOutDir:string; var APIResult:string; var AFilesize:int64; out AFilename:string; LogVerbose:Boolean = false):Boolean; overload;
    function GetImage(AttachmentID:Integer; var APIResult:string; var AFilesize:int64; out AFilename:string; out AStream:TMemoryStream; LogVerbose:Boolean = false):Boolean; overload;

    // also self explanatory
    function GetImageURL(AttachmentID:Integer; ASeconds:Integer; var AURL:string; LogVerbose:Boolean = false):Boolean;

    property ConnectionInfo:TCustomAmazonConnectionInfo read GetConnectionInfo;
  end;

implementation

uses
  Xml.XMLIntf
  , Xml.XMLDoc
  , System.Math
  , System.generics.collections
  , uLoggerLib
  ;

type
  TStringsHelper = class helper for TStrings
    function SafeText:string;
  end;

function TStringsHelper.SafeText:string;
begin
  if Self = nil then
    result := ''
  else
    result := self.Text
end;

{ TMyAmazonService }

function TCustomAmazonService.InitiateMultipartUpload(const BucketName, ObjectName: string; Metadata,
                                                       Headers: TStrings; ACL: TAmazonACLType;
                                                       ResponseInfo: TCloudResponseInfo): string;
var
  xml: string;
  xmlDoc: IXMLDocument;
  RootNode, IDNode: IXMLNode;
begin
  xml := InitiateMultipartUploadXML(BucketName, ObjectName, Metadata, Headers, ACL, ResponseInfo);

  if xml <> EmptyStr then
  begin
    xmlDoc := TXMLDocument.Create(nil);

    try
      xmlDoc.LoadFromXML(xml);
    except
      on e:exception do
        Exit(EmptyStr);
    end;

    RootNode := xmlDoc.DocumentElement;

    if not AnsiSameText(RootNode.NodeName, 'InitiateMultipartUploadResult') then
      Exit(EmptyStr);

    IDNode := RootNode.ChildNodes.FindNode('UploadId');

    if (IDNode <> nil) and IDNode.IsTextElement then
      Result := IDNode.Text
  end;
end;

function TCustomAmazonService.GetConnectionInfo: TCustomAmazonConnectionInfo;
begin
  result := TAmazonService(self).ConnectionInfo as TCustomAmazonConnectionInfo
end;

class procedure TCustomAmazonService.GetFileHash(const AFilename: string;
  var AFilehash: string; var AFilesize: Int64; var AFilestream: TStream);
begin
  if AFileStream = nil then
    AFileStream :=
      TFileStream.
        Create(AFilename, fmOpenRead OR fmShareDenyNone);
  GetFileHash(AFileStream, AFileHash, AFileSize)
end;

class procedure TCustomAmazonService.GetFileHash(AFilestream: TStream;
  var AFilehash: string; var AFilesize: Int64);
begin
  Assert(AFileStream.size > 0, 'FileStream size = 0');
  AFilesize := AFileStream.Size;
  AFilehash :=
    UpperCase(THashSHA1.GetHashString(AfileStream));
end;

function TCustomAmazonService.GetImage(AttachmentID: Integer;
  var APIResult: string; var AFilesize: int64; out AFilename:string;
  out AStream: TMemoryStream; LogVerbose:Boolean = false): Boolean;
var
  AHeaders:TStringList;
  AQueryParameters:TStringList;
  ARespInfo:TCloudResponseInfo;
  ARespStream:TMemoryStream absolute AStream;
  AResp: TCloudHTTP;
  ARespText:TStringStream;
  AURL:string;
  ARespJson:TJSONObject;
begin
  AHeaders := nil;
  AQueryParameters := nil;
  ARespInfo := nil;
  ARespStream := nil;
  ARespJson := nil;
  AResp := nil;
  try
    try
      APIResult := '';
      result := false;
      ARespJson := nil;
      ARespText := TStringStream.Create;
      ARespStream := TMemoryStream.Create;
      AQueryParameters := TStringList.Create;
      AHeaders := TStringList.create;
      ARespInfo := TCloudResponseInfo.Create;

      AHeaders.Values['host'] := ConnectionInfo.FHost;
      AHeaders.Values['content-type'] := ConnectionInfo.FContentType;
      AHeaders.Values['x-amz-date'] := self.ISODateTime_noSeparators;
      AHeaders.Values['accept'] := ConnectionInfo.FAcceptedValues;
      FContentHash := TCloudSHA256Authentication.GetHashSHA256Hex('');

      AURL := format('https://%s%s%d',[
        AHeaders.Values['host']
        , GetURI
        , AttachmentID
        ]);

      Log(lpInfo, 'AWSLIB:GetImage %d', [AttachmentID]);
      Log(lpVerbose, 'AWSLIB:GetImage Headers'#13#10'%s',[AHeaders.Text]);
      Log(lpVerbose, 'AWSLIB:GetImage AQueryParameters'#13#10'%s',[AQueryParameters.Text]);
      Log(lpVerbose, 'AWSLIB:GetImage URL'#13#10'%s',[AURL]);

      AResp := IssueGetRequest(
        AURL
        , AHeaders
        , AQueryParameters
        , ''
        , ARespInfo, ARespStream);

      case AResp.ResponseCode of
        200:
        begin
          AHeaders.Delimiter := ';';
          AHeaders.DelimitedText := ARespInfo.Headers.Values['Content-Disposition'];
          AFilename := AHeaders.Values['filename'];
          if StrToInt(ARespInfo.Headers.Values['Content-Length']) <> ARespStream.size then
            raise Exception.CreateFmt('Unexpected file length: %s <> %d', [
              ARespInfo.Headers.Values['Content-Length'],
              ARespStream.size
          ]);
          AFilesize := ARespStream.size;
          Log(lpInfo, 'AWSLIB:GetImage Filename:%s Filesize:%d',[AFilename, AFilesize]);
          result := True
        end;
        else
        begin
          ARespText.CopyFrom(ARespStream, ARespStream.Size);
          ARespJson := TJSONObject.ParseJSONValue(ARespText.DataString) as TJSONObject;
          APIResult := ReplaceStr(TJson.Format(ARespJson),'\n',#13#10);
          Log(lpInfo, 'AWSLIB:GetImage APIResult'#13#10'%s',[APIResult]);
        end;
      end;

    except
      on e:exception do
        raise EAWSGetImage.CreateFmt('%s: %s', [e.classname, e.message]);
    end;
  finally
    ARespJson.Free;
    AQueryParameters.Free;
    AHeaders.Free;
    ARespInfo.Free;
//    ARespStream.Free;
    AResp.free
  end;
end;

function TCustomAmazonService.GetImage(AttachmentID:Integer; const AOutDir:string; var APIResult:string; var AFilesize:int64; out AFilename:string; LogVerbose:Boolean = false):Boolean;
var
  AStream:TMemoryStream;
begin
  result := GetImage(AttachmentID, APIResult, AFilesize, AFilename, AStream, LogVerbose);
  try
    AFilename := TPath.Combine(AOutDir, AFilename);
    if TFile.Exists(AFilename) then
      TFile.Delete(AFilename);
    AStream.SaveToFile(AFilename)
  finally
    AStream.free
  end;
end;

function TCustomAmazonService.GetImageURL(AttachmentID, ASeconds: Integer; var AURL:string; LogVerbose:Boolean = false):boolean;
var
  AHeaders:TStringList;
  AQueryParameters:TStringList;
  ARespInfo:TCloudResponseInfo;
  ARespStream:TMemoryStream;
  AResp: TCloudHTTP;
  ARespText:TStringStream;
//  AURL:string;
  ARespJson:TJSONObject;
begin
  AHeaders := nil;
  AQueryParameters := nil;
  ARespInfo := nil;
  ARespStream := nil;
  ARespJson := nil;
  AResp := nil;
  try
    result := false;
    ARespText := TStringStream.Create;
    ARespStream := TMemoryStream.Create;
    AQueryParameters := TStringList.Create;
    AHeaders := TStringList.create;
    ARespJson := nil;
    ARespInfo := TCloudResponseInfo.Create;

    AHeaders.Values['host'] := ConnectionInfo.FHost;
    AHeaders.Values['content-type'] := ConnectionInfo.FContentType;
    AHeaders.Values['x-amz-date'] := self.ISODateTime_noSeparators;
    AHeaders.Values['accept'] := ConnectionInfo.FAcceptedValues;
    FContentHash := TCloudSHA256Authentication.GetHashSHA256Hex('');

    AURL :=format('https://%s%s%d/urls',[
      AHeaders.Values['host']
      , GetURI
      , AttachmentID
      ]);

    AQueryParameters.Values['Seconds'] := IntToStr(ASeconds);

    Log(lpInfo, 'AWSLIB:GetImageURL %d', [AttachmentID]);
    Log(lpVerbose, 'AWSLIB:GetImageURL Headers'#13#10'%s',[AHeaders.Text]);
    Log(lpVerbose, 'AWSLIB:GetImageURL AQueryParameters'#13#10'%s',[AQueryParameters.Text]);
    Log(lpVerbose, 'AWSLIB:GetImageURL URL'#13#10'%s',[AURL]);

    AResp := IssueGetRequest(
      AURL
      , AHeaders
      , AQueryParameters
      , ''
      , ARespInfo
      , ARespStream);

    case AResp.ResponseCode of
      200:
      begin
        AHeaders.Delimiter := ';';
        AHeaders.DelimitedText := ARespInfo.Headers.Values['Content-Disposition'];
        ARespText.CopyFrom(ARespStream, ARespStream.Size);
        AURL := ARespText.DataString;
        ARespJson := TJSONObject.ParseJSONValue(AURL) as TJSONObject;
        Log(lpInfo, 'AWSLIB:GetImageURL Resp'#13#10'%s',[ReplaceStr(TJson.Format(ARespJson),'\n',#13)]);
        AURL := ARespJson.values['url'].value;
        result := true;
        exit
      end;
      else
      begin
        ARespText.CopyFrom(ARespStream, ARespStream.Size);
        ARespJson := TJSONObject.ParseJSONValue(ARespText.DataString) as TJSONObject;
        Log(lpError, 'AWSLIB:GetImageURL Resp'#13#10'%s',[ReplaceStr(TJson.Format(ARespJson),'\n',#13)]);
        AURL := ReplaceStr(TJson.Format(ARespJson),'\n',#13);
      end;
    end;
  finally
    FreeAndNil(ARespJson);
    AQueryParameters.Free;
    AHeaders.Free;
    ARespInfo.Free;
    ARespStream.Free;
    AResp.free
  end;
end;

function TCustomAmazonService.GetMetaData(const AFilename:string; out AMetaData:TStrings):boolean;
var
  AObjectName: string;
  ARespInfo:TCloudResponseInfo;
begin
  AMetaData := nil;
  AObjectName := URLEncode(Format('%s%s',[ConnectionInfo.FStagingFolder,AFilename]));
  AObjectName := StringReplace(AObjectName,'@','%40',[rfReplaceAll]);
  ARespInfo := TCloudResponseInfo.Create;
  AMetaData := GetObjectMetadata(ConnectionInfo.FStagingBucket,AObjectName,ARespInfo);
  result := AMetaData <> nil;
end;

function TCustomAmazonService.PutAttachment(AttachmentID:Integer; AFilename:string; const AFilehash:string; AFileStream:TStream; var APIResult:string; forceUpload:boolean = true; LogVerbose:Boolean = false):Boolean;
var
  AObjectName: string;
  APartIndex:integer;
  sizeLeft, partSize, partDone:Int64;
  AFileBuffer:TArray<System.Byte>;
  mpuID:string;
  AURL:string;
  ARestText:string;
  ARespJson:TJSONObject;
  AHeaders:TStringList;
  AQueryParameters:TStringList;
  AResponse: TCloudHTTP;
  ARespInfo:TCloudResponseInfo;
  AMetaData:TStrings;
  APartResp:TAmazonMultipartPart;
  APartList:TList<TAmazonMultipartPart>;
begin
  ARespJson := nil;
  AQueryParameters := nil;
  AResponse := nil;
  AHeaders := TStringList.create;
  AQueryParameters := TStringList.create;
  AFilename := TPath.GetFileName(AFilename);
  AObjectName := URLEncode(Format('%s%s',[ConnectionInfo.FStagingFolder,AFilename]));
  AObjectName := StringReplace(AObjectName,'@','%40',[rfReplaceAll]);

  ARespInfo := TCloudResponseInfo.Create;

  AMetaData := nil;
  try
    try
      FRegularService := true;
      AMetaData := GetObjectMetadata(ConnectionInfo.FStagingBucket,AObjectName,ARespInfo);
      Log(lpInfo, 'AWSLIB:PutAttachment %d %s (%0.0n %s)'#13#10'%s', [AttachmentID
        , AFilename
        , AFileStream.Size/1
        , AFilehash, AMetadata.safetext]);

      if (AMetaData <> nil) and (AMetaData.Values['sha1']=AFilehash) and not forceUpload then
        Log(lpInfo, 'AWSLIB:PutAttachment File already in bucket')
        // already in bucket
      else
      try
        Log(lpInfo, 'AWSLIB:PutAttachment Uploading to bucket..');
        APartList := TList<TAmazonMultipartPart>.Create;
        FreeAndNil(AMetaData);
        AMetaData := TStringList.Create;
        AMetaData.Values['attachmentid'] := IntToStr(AttachmentID);
        AMetaData.Values['filename'] := AFilename;
        AMetaData.Values['sha1'] := AFilehash;

        APartIndex := 0;
        partDone := 0;
        AFileStream.seek(0,TSeekOrigin.soBeginning);
        sizeLeft := AFileStream.Size;
        try
          repeat
            inc(APartIndex);
            partSize := Min(sizeLeft
              ,1024*1024
              *5 // minimum part size
              //*100
              ); // use multipart upload for files > 100MB

            Dec(sizeLeft,partSize);
            SetLength(AFileBuffer,partSize);
            AFileStream.Read64(AFileBuffer, 0, partSize);

            if (APartIndex=1)and(sizeLeft>0) then
            begin
              mpuID := InitiateMultipartUpload(
                ConnectionInfo.FStagingBucket,
                AObjectName,
                AMetaData,
                nil,
                amzbaNotSpecified,
                ARespInfo);

//              ARespJson := TJSONObject.ParseJSONValue(ARespInfo.StatusMessage) as TJSONObject;
//              if ARespJson <> nil then
//                APIResult := ReplaceStr(TJson.Format(ARespJson),'\n',#13)
//              else
//                APIResult := ARespInfo.StatusMessage;
//              Assert((ARespInfo.StatusCode = 200), Format('AWSLIB:PutAttachment (%d) S3 Upload Failed upload of %s part %d'#13#10'%s', [ARespInfo.StatusCode, AObjectName,APartIndex,APIResult]));
//
//              FreeAndNil(ARespInfo);
              ARespInfo := TCloudResponseInfo.Create;
            end;

            if (APartIndex>1)or(sizeLeft>0) then
            begin
              UploadPart(
                ConnectionInfo.FStagingBucket, // BucketName
                AObjectName,        // ObjectName
                mpuID,
                APartIndex,
                AFileBuffer,
                APartResp,// partResp,
                '',
                ARespInfo);
//                and (ARespInfo.StatusCode = 200) then
//              begin
//                ARespJson := TJSONObject.ParseJSONValue(ARespInfo.StatusMessage) as TJSONObject;
//                if ARespJson <> nil then
//                  APIResult := ReplaceStr(TJson.Format(ARespJson),'\n',#13)
//                else
//                  APIResult := ARespInfo.StatusMessage;
//                Assert(False, Format('AWSLIB:PutAttachment (%d) S3 Upload Failed upload of %s part %d'#13#10'%s', [ARespInfo.StatusCode, AObjectName,APartIndex,APIResult]));
//              end;

              APartList.Add(APartResp);
            end
            else
            begin
              UploadObject(
                ConnectionInfo.FStagingBucket, // BucketName
                AObjectName,        // ObjectName
                AFileBuffer,     // Content
                False,          // ReducedRedundancy
                AMetaData,
                nil,            // Headers
                amzbaNotSpecified,// ACL
                ARespInfo);        // ResponseInfo
            end;

            case ARespInfo.StatusCode of
              200:
              begin
                ARespJson := TJSONObject.ParseJSONValue(ARespInfo.StatusMessage) as TJSONObject;
                if ARespJson <> nil then
                  APIResult := ReplaceStr(TJson.Format(ARespJson),'\n',#13)
                else
                  APIResult := ARespInfo.StatusMessage;
                Log(lpDebug, 'AWSLIB:PutAttachment (%d) S3 Upload %d, %d left to bucket..'#13#10'%s',[ARespInfo.StatusCode, partSize, sizeLeft, APIResult]);
                FreeAndNil(ARespInfo);
                ARespInfo := TCloudResponseInfo.Create;
              end;
              else
              begin
                ARespJson := TJSONObject.ParseJSONValue(ARespInfo.StatusMessage) as TJSONObject;
                if ARespJson <> nil then
                  APIResult := ReplaceStr(TJson.Format(ARespJson),'\n',#13)
                else
                  APIResult := ARespInfo.StatusMessage;
                Log(lpError, 'AWSLIB:PutAttachment (%d) S3 Upload %d, %d left to bucket..'#13#10'%s',[ARespInfo.StatusCode, partSize, sizeLeft, APIResult]);
                FreeAndNil(ARespInfo);
                ARespInfo := TCloudResponseInfo.Create;
                exit
              end;
            end;
            inc(partDone,partSize)
          until sizeLeft = 0;

          if (APartIndex>1)or(sizeLeft>0) then
            if not (
              CompleteMultipartUpload(
                ConnectionInfo.FStagingBucket, // BucketName
                AObjectName,        // ObjectName
                mpuID,
                APartList,
                ARespInfo) and (ARespInfo.StatusCode = 200)) then
            begin
              ARespJson := TJSONObject.ParseJSONValue(ARespInfo.StatusMessage) as TJSONObject;
              if ARespJson <> nil then
                APIResult := ReplaceStr(TJson.Format(ARespJson),'\n',#13)
              else
                APIResult := ARespInfo.StatusMessage;
              Assert(false, Format('AWSLIB:PutAttachment (%d) S3 Error completing multipart upload of %s'#13#10'%s',[ARespInfo.StatusCode, AObjectName, APIResult]));
            end;

            FreeAndNil(ARespInfo);
            ARespInfo := TCloudResponseInfo.Create;
        except
          on e:exception do
          begin
            if (APartIndex>1)or(sizeLeft>0) then
              AbortMultipartUpload(
                ConnectionInfo.FStagingBucket, // BucketName
                AObjectName,        // ObjectName
                mpuID,
                ARespInfo
              );
            raise
          end
        end
      finally
        FreeAndNil(AMetaData);
        APartList.Free
      end;
    finally
      FRegularService := false;
//      freeandnil(AFileStream)
    end;

    AHeaders.Values['host'] := ConnectionInfo.FHost;
    AHeaders.Values['content-type'] := ConnectionInfo.FContentType;
    AHeaders.Values['x-amz-date'] := self.ISODateTime_noSeparators;
    AHeaders.Values['accept'] := ConnectionInfo.FAcceptedValues;
    FContentHash := TCloudSHA256Authentication.GetHashSHA256Hex('');

    AQueryParameters.Values['move'] := 'true';

    AURL :=format('https://%s%s%d?%s',[
      AHeaders.Values['host']
      , GetURI
      , AttachmentID
      , 'move=true'
      ]);

    ARestText := '';

    Log(lpInfo, 'AWSLIB:PutAttachment ..');
    Log(lpVerbose, 'AWSLIB:PutAttachment Headers'#13#10'%s',[AHeaders.Text]);
    Log(lpVerbose, 'AWSLIB:PutAttachment AQueryParameters'#13#10'%s',[AQueryParameters.Text]);
    Log(lpVerbose, 'AWSLIB:PutAttachment URL'#13#10'%s',[AURL]);
//    Log(lpVerbose, 'AWSLIB:PutAttachment Resp'#13#10'%s',[ReplaceStr(TJson.Format(TJSONObject.ParseJSONValue(ARestText) as TJSONObject),'\n',#13)]);

    AResponse := IssuePutRequest(
      AURL
      , AHeaders
      , AQueryParameters
      , ''
      , ARespInfo, nil, ARestText);

    case AResponse.ResponseCode of
      200:
      begin
        FreeAndNil(ARespJson);
        ARespJson := TJSONObject.ParseJSONValue(ARestText) as TJSONObject;
        APIResult := ReplaceStr(TJson.Format(ARespJson),'\n',#13);
        Log(lpDebug, 'AWSLIB:PutAttachment '#13#10'%s',[APIResult]);
        result := true;
        exit
      end;
      else
      begin
        FreeAndNil(ARespJson);
        ARespJson := TJSONObject.ParseJSONValue(ARestText) as TJSONObject;
        APIResult := ReplaceStr(TJson.Format(ARespJson),'\n',#13);
        Log(lpError, 'AWSLIB:PutAttachment '#13#10'%s',[APIResult]);
      end;
    end;
  finally
    freeandnil(AHeaders);
    freeandnil(ARespInfo);
    freeandnil(ARespJson);
    freeandnil(AQueryParameters);
    freeandnil(AResponse);
  end;
end;

class function TCustomAmazonService.PutAttachment(
  Ci:TCustomAmazonConnectionInfo
      ; var Svc:TCustomAmazonService
      ; var AttachmentID:Integer
      ; const AOrigFilepath:string
      ; var AFileStream:TStream
      ; var APIResult:string
      ; var AFilesize:int64
      ; out AFilename:string
      ; preUploadCallback:TPreUploadCallback2; forceUpload:boolean; LogVerbose:Boolean): Boolean;
var
  AOrigFileStream:TStream;// TJclFileMappingStream;
  APIRslt:string;
  AContentHash:string;
begin
  AOrigFileStream := AFileStream;
  TCustomAmazonService.GetFileHash(AOrigFilepath, AContentHash, AFilesize, AFileStream);
  if preUploadCallback(CI, Svc, AOrigFilepath, AContentHash, AFilesize, AttachmentID, AFilename) then
  begin
    if Svc = nil then
      Svc := TCustomAmazonService.Create(CI);
    result := svc.PutAttachment(AttachmentID, AFilename, AContentHash, AFilestream, APIResult, forceUpload, LogVerbose);
  end
end;

function TCustomAmazonService.PutAttachment(var AttachmentID:Integer; const AOrigFilepath:string; var AFileStream:TStream; out APIResult:string; out AFilesize:int64; out AFilehash:string; out AFilename:string; preUploadCallback:TPreUploadCallback = nil; forceUpload:boolean = true; LogVerbose:Boolean = false):Boolean;
begin
  TCustomAmazonService.GetFileHash(AOrigFilepath, AFilehash, AFileSize, AFileStream);
  if Assigned(preUploadCallback) then
    Assert(preUploadCallback(AOrigFilepath, FContentHash, AFilesize, AttachmentID, AFilename), Format('Error registering new attachment %s',[AOrigFilepath]));
  result := PutAttachment(AttachmentID, AOrigFilepath, AFilehash, AFileStream, APIResult, forceUpload, LogVerbose)
end;

function TCustomAmazonService.BuildStringToSign(const HTTPVerb: string; Headers, QueryParameters: TStringList;
  const QueryPrefix, URL: string): string;
var
  CanRequest, Scope, LdateISO, Ldate, Lregion : string;
  URLrec : TURI;
  LParams: TStringList;
  VPParam : TNameValuePair;
begin
  if FRegularService then
    result := inherited
  else
  begin
    LParams := nil;
    try
      //Build the first part of the string to sign, including HTTPMethod
      CanRequest := BuildStringToSignPrefix(HTTPVerb);

      //find and encode the requests resource
      URLrec :=  TURI.Create(URL);

      //CanonicalURI URL encoded
      CanRequest := CanRequest + URLrec.Path + #10;

      //CanonicalQueryString encoded
      if not URLrec.Query.IsEmpty then
      begin
        if Length(URLrec.Params) = 1 then
          CanRequest := CanRequest + URLrec.Query + #10
        else
        begin
          LParams := TStringList.Create;
          for VPParam in URLrec.Params do
            LParams.Append(VPParam.Name+'='+VPParam.Value);
          CanRequest := CanRequest + BuildStringToSignResources('', LParams).Substring(1) + #10
        end;
      end
      else
        CanRequest := CanRequest + #10;

      //add sorted headers and header names in series for signedheader part
      CanRequest := CanRequest + BuildStringToSignHeaders(Headers);
      CanRequest := CanRequest + #10 + FContentHash;

      LdateISO :=  Headers.Values['x-amz-date'];
      Ldate :=  Leftstr(LdateISO,8);
      Lregion := ConnectionInfo.FRegion;
      Scope :=  Ldate + '/'+Lregion+ '/' + ConnectionInfo.FService + '/' + ConnectionInfo.FProtocol;

      Result := 'AWS4-HMAC-SHA256' + #10 + LdateISO + #10 + Scope + #10 + TCloudSHA256Authentication.GetHashSHA256Hex(CanRequest);
    finally
       LParams.Free;
    end;
  end;
end;

function TCustomAmazonService.CreateAuthInstance(const ConnectionInfo: TAmazonConnectionInfo): TCloudAuthentication;
begin
  result := TCustomAmazonAuthenticator.Create(ConnectionInfo, true);
end;

function TCustomAmazonService.GetRequiredHeaderNames(out InstanceOwner: Boolean): TStrings;
begin
  if FRegularService then
    result := inherited
  else
  begin
    InstanceOwner := False;
    if (FRequiredHeaderNames = nil) or (FRequiredHeaderNames.Count = 0) then
    begin
      FRequiredHeaderNames.Free;
      FRequiredHeaderNames := TStringList.Create;

      FRequiredHeaderNames.Add('accept');
      FRequiredHeaderNames.Add('host');
      FRequiredHeaderNames.Add('x-amz-date');

//      FRequiredHeaderNames.Add('accept');
//      FRequiredHeaderNames.Add('content-type');
//      FRequiredHeaderNames.Add('host');
//      FRequiredHeaderNames.Add('x-amz-date');

    end;
    Result := FRequiredHeaderNames;
  end;
end;

function TCustomAmazonService.GetURI: string;
begin
  result := ConnectionInfo.FRestURL
end;

{ TMyAmazonAuthenticator }

function TCustomAmazonAuthenticator.BuildAuthorizationString(const
  StringToSign,
  DateISO,
  Region,
  AServiceName,
  AProtocol,
  SignedStrHeaders: string): string;

  function GetSignatureKey(const datestamp, region, serviceName: string): TBytes;
  begin
    Result := SignString(FSHAKey,datestamp);
    Result := SignString(Result, region);
    Result := SignString(Result, serviceName);
    Result := SignString(Result, AProtocol);
  end;

var
  Signature, Credentials, SignedHeaders: string;
  SigningKey : TBytes;
begin
  SigningKey := GetSignatureKey( DateISO, Region, AServiceName);
  Credentials   := 'Credential='+FConnectionInfo.AccountName + '/'+ DateISO + '/'+Region+ '/' + AServiceName + '/' + AProtocol +',';
  SignedHeaders := 'SignedHeaders='+SignedStrHeaders + ',';
  Signature     := 'Signature='+THash.DigestAsString(SignString(SigningKey, StringToSign));
  Result := GetAuthorizationType +' '+ Credentials + SignedHeaders + Signature;
end;

procedure TCustomAmazonService.PrepareRequestSignature(const HTTPVerb: string;
                                      const Headers, QueryParameters: TStringList;
                                      const StringToSign: string;
                                      var URL: string; Request: TCloudHTTP; var Content: TStream);

var
  AuthorizationString, SignStrHeaders, LdateISO, Lregion: string;
  RequiredHeadersInstanceOwner : Boolean;
  SignedHeaders: TStringList;
begin
  if FRegularService then
    inherited
  else
  begin
    if FAuthenticator <> nil then
    begin
      SignedHeaders :=  TStringList(GetRequiredHeaderNames(RequiredHeadersInstanceOwner));
      SignedHeaders.Delimiter := ';';
      SignStrHeaders := SignedHeaders.DelimitedText;
      LdateISO :=  Leftstr(Headers.Values['x-amz-date'],8);
      Lregion := ConnectionInfo.FRegion;
      AuthorizationString :=
        TCustomAmazonAuthenticator(FAuthenticator).BuildAuthorizationString(StringToSign, LdateISO, Lregion, ConnectionInfo.FService, ConnectionInfo.FProtocol, SignStrHeaders);
      Request.Client.CustomHeaders['Authorization'] := AuthorizationString;
      SignedHeaders.Clear;
      if RequiredHeadersInstanceOwner then
        FreeAndNil(SignedHeaders);
    end;
  end;
end;

{ TCustomAmazonConnectionInfo }

constructor TCustomAmazonConnectionInfo.create;
begin
  inherited create(nil);
  FRestURL := RestURL;
  FService := Service;
  FRegion := Region;
  FHost := Host;
  FStagingFolder := StagingFolder;
  FStagingBucket := StagingBucket;
  FAlgorithm := Algorithm;
  FSignedHeaders := SignedHeaders;
  FContentType := ContentType;
  FAcceptedValues := AcceptedValues;

  FProtocol := 'aws4_request';

  FLogVerbose := ALogVerbose;
end;

end.
