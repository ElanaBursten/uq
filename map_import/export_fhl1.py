# export-fhl1.py
# Based on Throwaway script to convert map data to a TSV file readable by bcp.
# Doesn't connect to the database, just writes the file.
# Created: 2003.02.25 HN
# Updated: 2003.06.21 HN

import string
import sys

FILENAME = "fhl1-assign-cells.tsv"

map_ids = {
    "TX.FHL-Keymaps": 933,
}

pages = []

f = open(FILENAME, "r")
f.readline()    # ignore first line
g = open("fhl1_map_page.tsv", "w")
h = open("fhl1_map_cell.tsv", "w")
for line in f.xreadlines():
    fields = string.split(line, "\t")
    map_id, map_page, x, y, area_id = fields
    area_id = string.rstrip(area_id)    # remove trailing newline

    s = string.join([map_id, map_page, "A", "Z", "1", "1"], "\t")
    if s not in pages:
        print >> g, s
        pages.append(s)
    h.write(line)

f.close()
print

print "OK"

