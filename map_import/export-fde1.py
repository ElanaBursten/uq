# export-fde1.py
# Throwaway script to convert map data to a TSV file readable by bcp.
# Created: 2003.02.25, HN

import string
import sys

FILENAME = "c:/internet/download/FDEpage-grids/map-cellsFDE.tsv"

map_ids = {
    "NEWC": 898,
    "SUSSX": 899,
    "KENTD": 900,
}

pages = {}

f = open(FILENAME, "r")
g = open("fde1_map_cell.tsv", "w")
f.readline()    # ignore first line
for line in f.xreadlines():
    fields = string.split(line, "\t")
    map_name, _, map_page, x, y, area_id = fields
    area_id = string.rstrip(area_id)    # remove trailing newline
    y = int(y)
    key = (map_name, map_page)    # map name and number, e.g. KENTD, 1

    try:
        z = pages[key]  # [minx, maxx, miny, maxy]
    except KeyError:
        pages[key] = [x, x, y, y]
    else:
        [minx, maxx, miny, maxy] = z
        pages[key] = [min(x, minx), max(x, maxy), min(y, miny), max(y, maxy)]

    # write record in TSV file
    if area_id and int(area_id):
        row = [map_ids[map_name], map_page, x, y, area_id]
        s = string.join(map(str, row), "\t")
        print >> g, s
        sys.stdout.write(".")
    else:
        sys.stdout.write(".")
f.close()
g.close()
print

# write the TSV file for map_page
print "Writing TSV file for map_page...",
items = pages.items()
items.sort()
g = open("fde1_map_page.tsv", "w")
for (map_name, map_page), (minx, maxx, miny, maxy) in items:
    row = [map_ids[map_name], map_page, minx, maxx, miny, maxy]
    s = string.join(map(str, row), "\t")
    print >> g, s
g.close()
print "OK"

