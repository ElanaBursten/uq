object frmTestMsg: TfrmTestMsg
  Left = 0
  Top = 0
  Caption = 'frmTestMsg'
  ClientHeight = 252
  ClientWidth = 427
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 112
    Top = 45
    Width = 34
    Height = 13
    Caption = 'Map ID'
  end
  object Label2: TLabel
    Left = 112
    Top = 88
    Width = 31
    Height = 13
    Caption = 'EmpID'
  end
  object Label3: TLabel
    Left = 112
    Top = 128
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object edtMapID: TEdit
    Left = 80
    Top = 64
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 0
    Text = '1211'
  end
  object btnSendMapID: TButton
    Left = 96
    Top = 187
    Width = 75
    Height = 25
    Caption = 'Send Map ID'
    TabOrder = 1
    OnClick = btnSendMapIDClick
  end
  object edtEmpID: TEdit
    Left = 80
    Top = 104
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '20491'
  end
  object edtPW: TEdit
    Left = 80
    Top = 147
    Width = 297
    Height = 21
    TabOrder = 3
    Text = '2B9A14A1D7ED9E3B42CA16BF64DD7552B6C51DC1'
  end
end
