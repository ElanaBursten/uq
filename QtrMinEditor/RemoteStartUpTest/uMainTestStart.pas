unit uMainTestStart;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, ShellApi;

type
  TmapRecord = packed record
    mapID: string[8];
    d: TDateTime;
  end;

type
  TfrmTestMsg = class(TForm)
    edtMapID: TEdit;
    Label1: TLabel;
    btnSendMapID: TButton;
    edtEmpID: TEdit;
    edtPW: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    procedure btnSendMapIDClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    receiverHandle: THandle;
    procedure SendData(copyDataStruct: TCopyDataStruct; sMapID:string);
    procedure writeToDesktop(sMapID: string);
    { Private declarations }
  public
    { Public declarations }
  end;

const
  QM_ED = 'QtrMinuteGrid.exe';

var
  frmTestMsg: TfrmTestMsg;

implementation

{$R *.dfm}

procedure TfrmTestMsg.btnSendMapIDClick(Sender: TObject);
begin
  writeToDesktop(edtMapID.Text);
end;

procedure TfrmTestMsg.writeToDesktop(sMapID: string);
var
  mapRecord: TmapRecord;
  copyDataStruct: TCopyDataStruct;
begin
  mapRecord.mapID := sMapID;
  mapRecord.d := Now;

  copyDataStruct.dwData := integer(1000);
  copyDataStruct.cbData := SizeOf(mapRecord);
  copyDataStruct.lpData := @mapRecord;

  SendData(copyDataStruct, sMapID);
end;

procedure TfrmTestMsg.FormClose(Sender: TObject; var Action: TCloseAction);
var
  receiverHandle, handle: THandle;
begin
  receiverHandle := FindWindow('TfrmMain', nil);
    PostMessage(receiverHandle, WM_QUIT, 0, 0);
end;

procedure TfrmTestMsg.SendData(copyDataStruct: TCopyDataStruct;
  sMapID: string);
var
  parameters: string;
begin
  receiverHandle := FindWindow('TfrmMain', nil);
  parameters := sMapID+' '+edtEmpID.Text+' '+edtPW.Text;
  if receiverHandle = 0 then
  begin
    ShellExecute(handle, 'open', PWideChar(QM_ED),
      PWideChar(parameters), nil, SW_SHOWNORMAL);
  end
  else
    SendMessage(receiverHandle, WM_COPYDATA, integer(handle),
      integer(@copyDataStruct));
end;


end.
