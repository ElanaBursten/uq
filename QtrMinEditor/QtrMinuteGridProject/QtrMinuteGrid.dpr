
{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
program QtrMinuteGrid;


uses
  Forms,
  SysUtils,
  uMainForm in 'uMainForm.pas' {frmMain},
  uAreaSel in 'uAreaSel.pas' {dlgAreaSelect},
  GlobalSU in 'GlobalSU.pas',
  ClientModuleUnit2 in 'ClientModuleUnit2.pas' {ClientModule2: TDataModule},
  ClientClassesUnit2 in 'ClientClassesUnit2.pas',
  uSavedLocations in 'uSavedLocations.pas' {frmSavedLocations},
  MsgBoxTimeOut in 'MsgBoxTimeOut.pas';

//{$R *.res}
{$R '..\..\QMIcon.res'}
{$R '..\..\QMVersion.res' '..\..\QMVersion.rc'}
var
   MyInstanceName: string;
begin
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
    begin
      ReportMemoryLeaksOnShutdown := DebugHook <> 0;
      Application.MainFormOnTaskbar := false;
      Application.Initialize;
      Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TClientModule2, ClientModule2);
  Application.Run;
    end
    else
    Application.Terminate;
end.
