object frmMain: TfrmMain
  AlignWithMargins = True
  Left = 192
  Top = 114
  ActiveControl = mapAreaList
  Caption = 
    '                                                                ' +
    '     The Quarter Minute Coloring Book'
  ClientHeight = 827
  ClientWidth = 1405
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 1053
    Top = 0
    Width = 7
    Height = 808
  end
  object pnlGridSide: TPanel
    Left = 1060
    Top = 0
    Width = 345
    Height = 808
    Align = alClient
    Color = clMenuBar
    ParentBackground = False
    PopupMenu = puWebMenu
    TabOrder = 0
    object Splitter2: TSplitter
      Left = 1
      Top = 481
      Width = 343
      Height = 3
      Cursor = crVSplit
      Align = alTop
    end
    object tsViewMode: TTabSet
      Left = 1
      Top = 1
      Width = 343
      Height = 21
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      SoftTop = True
      Tabs.Strings = (
        'Areas By Map'
        'Areas In View')
      TabIndex = 0
      OnChange = tsViewModeChange
    end
    object gridPanel: TPanel
      Left = 1
      Top = 22
      Width = 343
      Height = 459
      Align = alTop
      TabOrder = 1
      object viewList: TDBGrid
        Left = 1
        Top = 1
        Width = 341
        Height = 378
        Align = alClient
        DataSource = dsViewList
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ParentShowHint = False
        PopupMenu = puWebMenu
        ShowHint = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = viewListDrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'Color'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'area_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'call_center'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'emp_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'short_name'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WebColor'
            Visible = True
          end>
      end
      object mapAreaList: TDBGrid
        Left = 1
        Top = 1
        Width = 341
        Height = 378
        Align = alClient
        DataSource = dsMapList
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        PopupMenu = puWebMenu
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = mapAreaListDrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'Color '
            Title.Caption = 'color'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'area_name'
            Title.Caption = 'Area'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'short_name'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'area_id'
            Title.Caption = 'area Id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'map_name'
            Title.Caption = 'Map Name'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'map_id'
            Title.Caption = 'Map ID'
            Visible = True
          end>
      end
      object DBNavigator1: TDBNavigator
        Left = 1
        Top = 379
        Width = 341
        Height = 45
        DataSource = dsMapList
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        Align = alBottom
        TabOrder = 2
      end
      object cbExcludeMap: TCheckBox
        Left = 1
        Top = 424
        Width = 341
        Height = 17
        Align = alBottom
        Caption = 'Exclude Your Areas'
        TabOrder = 3
      end
      object ProgressBar1: TProgressBar
        Left = 1
        Top = 441
        Width = 341
        Height = 17
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alBottom
        Orientation = pbVertical
        TabOrder = 4
      end
    end
    object pnlBottom: TPanel
      Left = 1
      Top = 484
      Width = 343
      Height = 323
      Align = alClient
      TabOrder = 2
      DesignSize = (
        343
        323)
      object Label2: TLabel
        Left = 1
        Top = 219
        Width = 341
        Height = 26
        Align = alBottom
        Caption = 
          'Right-click on the Map and see a list of Areas that are assigned' +
          ' to the quarter minute selected.'
        Color = clMenu
        FocusControl = whoHereList
        ParentColor = False
        WordWrap = True
      end
      object btnFindAddress: TButton
        Left = 4
        Top = 16
        Width = 99
        Height = 25
        Caption = 'Find Address'
        TabOrder = 0
        OnClick = btnFindAddressClick
      end
      object edtFindAddress: TEdit
        Left = 112
        Top = 21
        Width = 184
        Height = 21
        TabOrder = 1
        Text = '54 Greenwich Park St, London, UK'
        OnDblClick = edtFindAddressDblClick
      end
      object btnClear: TBitBtn
        Left = 302
        Top = 17
        Width = 25
        Height = 25
        Caption = 'C'
        TabOrder = 2
        OnClick = btnClearClick
      end
      object edtLng: TEdit
        Left = 156
        Top = 91
        Width = 68
        Height = 21
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        TabOrder = 3
        Text = '0.00'
        TextHint = '-84.03487'
      end
      object edtLat: TEdit
        Left = 84
        Top = 90
        Width = 68
        Height = 21
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        TabOrder = 4
        Text = '51.482893'
        TextHint = '34.023132'
      end
      object btnGoto: TButton
        Left = 10
        Top = 90
        Width = 60
        Height = 20
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Go To'
        TabOrder = 5
        OnClick = btnGotoClick
      end
      object btnFindQtrMin: TButton
        Left = 13
        Top = 49
        Width = 80
        Height = 20
        Caption = 'Find QtrMin'
        TabOrder = 6
        OnClick = btnFindQtrMinClick
      end
      object edtQtrMin: TEdit
        Left = 99
        Top = 48
        Width = 93
        Height = 21
        ParentShowHint = False
        ShowHint = False
        TabOrder = 7
        TextHint = '3359D8358B'
      end
      object whoHereList: TDBGrid
        Left = 1
        Top = 245
        Width = 341
        Height = 77
        Hint = 'Displays list of Areas assigned to Qtr Min R-clicked'
        Align = alBottom
        DataSource = dsWhoIsHere
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgTitleClick, dgTitleHotTrack]
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 8
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Memo1: TMemo
        Left = 5
        Top = 124
        Width = 328
        Height = 98
        Anchors = [akLeft, akRight]
        TabOrder = 9
        WordWrap = False
        OnDblClick = Memo1DblClick
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 808
    Width = 1405
    Height = 19
    AutoHint = True
    Panels = <
      item
        Text = 'status'
        Width = 700
      end
      item
        Text = 'ver'
        Width = 30
      end
      item
        Width = 100
      end
      item
        Text = 'Rest URL'
        Width = 60
      end
      item
        Width = 100
      end
      item
        Width = 60
      end
      item
        Width = 50
      end>
  end
  object pnlWebPage: TPanel
    Left = 0
    Top = 0
    Width = 1053
    Height = 808
    Align = alLeft
    PopupMenu = puWebMenu
    ShowCaption = False
    TabOrder = 2
    object WVWindowParent1: TWVWindowParent
      Left = 1
      Top = 1
      Width = 1051
      Height = 806
      Align = alClient
      TabStop = True
      TabOrder = 0
      Browser = WVBrowser1
    end
  end
  object puWebMenu: TPopupMenu
    Left = 736
    Top = 144
    object RetrieveListinView1: TMenuItem
      Caption = 'Retrieve List in View'
      Hint = 'List all the Areas that are in map view'
      OnClick = RetrieveListinView1Click
    end
    object RetriveAreasbyMap1: TMenuItem
      Caption = 'Retrive Areas by Map'
      OnClick = RetriveAreasbyMap1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object DrawSelected1: TMenuItem
      Caption = 'Map Selected'
      Hint = 'Map the selected Area for Qtr Min in view'
      OnClick = DrawSelected1Click
    end
    object UnMapSelected1: TMenuItem
      Caption = 'UnMap Selected'
      OnClick = UnMapSelected1Click
    end
    object MapAllinView1: TMenuItem
      Caption = 'Map All in View'
      Hint = 'Draws all the areas listed'
      OnClick = MapAllinView1Click
    end
    object GetAllforSelected1: TMenuItem
      Caption = 'Map All for Selected'
      Hint = 'Retrieves all Qtr Mins for an Area ID'
      OnClick = GetAllforSelected1Click
    end
    object MapAllforList1: TMenuItem
      Caption = 'Map All for List'
      OnClick = MapAllforList1Click
    end
    object clearGrid: TMenuItem
      Caption = 'Clear Entire Grid'
      Hint = 'Clears the grid of ALL Qtr Mins'
      OnClick = clearGridClick
    end
    object GotoSelected1: TMenuItem
      Caption = 'Go to Selected'
      GroupIndex = 3
      OnClick = GotoSelected1Click
    end
    object N1: TMenuItem
      Caption = '-'
      GroupIndex = 3
    end
    object Accept1: TMenuItem
      Caption = 'Save to Selected'
      Enabled = False
      GroupIndex = 3
      Hint = 'Saves the Fenced Qtr Mins to the Selected Area'
      OnClick = btnSaveClick
    end
    object N3: TMenuItem
      Caption = '-'
      GroupIndex = 3
    end
    object DeleteFromDatabase1: TMenuItem
      Caption = 'Delete Fenced Qtr Mins'
      GroupIndex = 3
      OnClick = DeleteFromDatabase1Click
    end
    object DelAllQtrMinFromSelected1: TMenuItem
      Caption = 'Del Qtr Min by Area Selected'
      GroupIndex = 3
      Hint = 'Removes all the Qtr Minutes belonging to the selected Area ID.  '
      OnClick = DelAllQtrMinFromSelected1Click
    end
  end
  object FDmemQtrMinute: TFDMemTable
    FieldDefs = <
      item
        Name = 'area_id'
      end
      item
        Name = 'qtrmin_lat'
        DataType = ftWideString
        Size = 20
      end
      item
        Name = 'qtrmin_long'
        DataType = ftWideString
        Size = 20
      end
      item
        Name = 'call_center'
        DataType = ftWideString
        Size = 20
      end>
    IndexDefs = <
      item
        Name = 'FDmemQtrMinuteIndex1'
      end>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 552
    Top = 216
  end
  object FDStanStorageJSONLink1: TFDStanStorageJSONLink
    Left = 280
    Top = 312
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 280
    Top = 384
  end
  object FDmemQtrMinPost: TFDMemTable
    FieldDefs = <
      item
        Name = 'area_id'
        DataType = ftInteger
      end
      item
        Name = 'qtrmin_lat'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'qtrmin_long'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'call_center'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <
      item
        Name = 'FDmemQtrMinuteIndex1'
      end>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 552
    Top = 272
    object FDmemQtrMinPostarea_id: TIntegerField
      FieldName = 'area_id'
    end
    object FDmemQtrMinPostqtrmin_lat: TStringField
      FieldName = 'qtrmin_lat'
    end
    object FDmemQtrMinPostqtrmin_long: TStringField
      FieldName = 'qtrmin_long'
    end
    object FDmemQtrMinPostcall_center: TStringField
      FieldName = 'call_center'
    end
  end
  object dsViewList: TDataSource
    DataSet = FDmemViewList
    Left = 552
    Top = 344
  end
  object FDwhoIsHereList: TFDMemTable
    FieldDefs = <
      item
        Name = 'area_id'
        DataType = ftInteger
      end
      item
        Name = 'area_name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'emp_id'
        DataType = ftInteger
      end
      item
        Name = 'short_name'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 552
    Top = 144
    object FDwhoIsHereListarea_id: TIntegerField
      FieldName = 'area_id'
    end
    object FDwhoIsHereListarea_name: TStringField
      FieldName = 'area_name'
    end
    object FDwhoIsHereListemp_id: TIntegerField
      FieldName = 'emp_id'
    end
    object FDwhoIsHereListshort_name: TStringField
      FieldName = 'short_name'
    end
  end
  object dsWhoIsHere: TDataSource
    DataSet = FDwhoIsHereList
    Left = 648
    Top = 144
  end
  object dsMapList: TDataSource
    DataSet = FDmemMapList
    Left = 664
    Top = 344
  end
  object FDmemMapList: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'Color'
        DataType = ftInteger
      end
      item
        Name = 'map_id'
        DataType = ftInteger
      end
      item
        Name = 'map_name'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'area_id'
        DataType = ftInteger
      end
      item
        Name = 'area_name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'WebColor'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'centroid_lat'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'centroid_lng'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'state'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'short_name'
        DataType = ftString
        Size = 30
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 664
    Top = 408
    object FDmemMapListColor: TIntegerField
      FieldName = 'Color'
    end
    object FDmemMapListmap_id: TIntegerField
      FieldName = 'map_id'
    end
    object FDmemMapListmap_name: TStringField
      FieldName = 'map_name'
      Size = 40
    end
    object FDmemMapListarea_id: TIntegerField
      FieldName = 'area_id'
    end
    object FDmemMapListarea_name: TStringField
      FieldName = 'area_name'
    end
    object FDmemMapListWebColor: TStringField
      FieldName = 'WebColor'
    end
    object FDmemMapListcentroid_lat: TStringField
      FieldName = 'centroid_lat'
    end
    object FDmemMapListcentroid_lng: TStringField
      FieldName = 'centroid_lng'
    end
    object FDmemMapListstate: TStringField
      FieldName = 'state'
    end
    object FDmemMapListshort_name: TStringField
      DisplayWidth = 30
      FieldName = 'short_name'
      Size = 30
    end
  end
  object MainMenu1: TMainMenu
    Left = 280
    Top = 104
    object File1: TMenuItem
      Caption = 'File'
      object Open1: TMenuItem
        Caption = 'Open'
        OnClick = Open1Click
      end
      object Save1: TMenuItem
        Caption = 'Save'
        OnClick = Save1Click
      end
      object SaveAs1: TMenuItem
        Caption = 'SaveAs'
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
      end
    end
    object Edit1: TMenuItem
      Caption = 'Edit'
      object Cut1: TMenuItem
        Caption = 'Cut'
      end
      object Copy1: TMenuItem
        Caption = 'Copy'
      end
      object Paste1: TMenuItem
        Caption = 'Paste'
      end
    end
    object Control1: TMenuItem
      Caption = 'Display'
      object NoLabels1: TMenuItem
        Caption = 'No Labels'
        Checked = True
        RadioItem = True
        OnClick = NoLabels1Click
      end
      object DrawCounties1: TMenuItem
        Caption = 'Draw Counties'
        OnClick = DrawCounties1Click
      end
      object LargeAreaAlert1: TMenuItem
        AutoCheck = True
        Caption = 'Large Area Alert'
      end
    end
  end
  object FDmemViewList: TFDMemTable
    FieldDefs = <
      item
        Name = 'Color'
        DataType = ftInteger
      end
      item
        Name = 'area_id'
        DataType = ftInteger
      end
      item
        Name = 'call_center'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'emp_id'
        DataType = ftInteger
      end
      item
        Name = 'short_name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'WebColor'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <
      item
        Name = 'FDmemQtrMinuteIndex1'
      end>
    IndexesActive = False
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 552
    Top = 408
    object FDmemViewListColor: TIntegerField
      FieldName = 'Color'
    end
    object FDmemViewListarea_id: TIntegerField
      FieldName = 'area_id'
    end
    object FDmemViewListcall_center: TStringField
      FieldName = 'call_center'
    end
    object FDmemViewListemp_id: TIntegerField
      FieldName = 'emp_id'
    end
    object FDmemViewListshort_name: TStringField
      FieldName = 'short_name'
    end
    object FDmemViewListWebColor: TStringField
      FieldName = 'WebColor'
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 300
    OnTimer = Timer1Timer
    Left = 48
    Top = 128
  end
  object WVBrowser1: TWVBrowser
    TargetCompatibleBrowserVersion = '104.0.1293.44'
    AllowSingleSignOnUsingOSPrimaryAccount = False
    OnInitializationError = WVBrowser1InitializationError
    OnAfterCreated = WVBrowser1AfterCreated
    OnWebMessageReceived = WVBrowser1WebMessageReceived
    Left = 32
    Top = 40
  end
  object dxSkinController1: TdxSkinController
    Left = 840
    Top = 152
  end
  object dbIsamDB: TDBISAMDatabase
    EngineVersion = '4.50 Build 5'
    DatabaseName = 'DB3'
    SessionName = 'Default'
    Left = 112
    Top = 720
  end
  object qryGeoMapsAPIkey: TDBISAMQuery
    DatabaseName = 'DB3'
    EngineVersion = '4.50 Build 5'
    Params = <>
    Left = 256
    Top = 720
  end
end
