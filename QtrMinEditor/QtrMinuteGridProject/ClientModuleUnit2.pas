unit ClientModuleUnit2;

interface

uses
  System.SysUtils, System.Classes, ClientClassesUnit2, IPPeerClient, forms,
  Datasnap.DSClientRest, Data.Bind.Components, Data.Bind.ObjectScope,
  REST.Client, REST.Authenticator.Basic;

type
  TClientModule2 = class(TDataModule)
    DSRestConnection1: TDSRestConnection;
  private
    fPort : integer;
    fHost : string;
    fUrlPath : string;
    fContext : string;
    FInstanceOwner: Boolean;
    FServerMethods1Client: TQtrMinClient;
    function GetServerMethods1Client: TQtrMinClient;
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property InstanceOwner: Boolean read FInstanceOwner write FInstanceOwner;
    property ServerMethods1Client: TQtrMinClient read GetServerMethods1Client write FServerMethods1Client;

end;

var
  ClientModule2: TClientModule2;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}
uses inifiles, uMainForm;
{$R *.dfm}

constructor TClientModule2.Create(AOwner: TComponent);
var
  IniFile : TIniFile;
  Path: String;
begin
  inherited;
  Path:= ExtractFilePath(Application.ExeName);
  IniFile := TIniFile.Create(Path+'clientSettings.ini') ;
  fHost := IniFile.ReadString('ServerSettings','Host','qm-dev.utiliquest.com');
  fPort := IniFile.ReadInteger('ServerSettings','Port',443);
  fContext := IniFile.ReadString('ServerSettings','context','datasnap/');
  furlpath := IniFile.ReadString('ServerSettings','urlpath','');
  frmMain.fKmzUrlPath :=  IniFile.ReadString('DataSettings','kmzPath','');
  DSRestConnection1.LoginProperties.UserName := frmMain.ActiveEmpID;
  DSRestConnection1.LoginProperties.Password := frmMain.ActivePassword;
  DSRestConnection1.Password :=   frmMain.ActivePassword;
  DSRestConnection1.UserName :=   frmMain.ActiveEmpID;
  frmMain.StatusBar1.Panels[4].text := fHost+':'+IntToStr(fPort);
  DSRestConnection1.Host := fHost;
  DSRestConnection1.Port := fPort;

  DSRestConnection1.Context:= fContext;
  DSRestConnection1.UrlPath:= furlpath;


  FreeAndNil(IniFile);
  FInstanceOwner := True;
end;

destructor TClientModule2.Destroy;
begin
  FServerMethods1Client.Free;
  inherited;
end;

function TClientModule2.GetServerMethods1Client: TQtrMinClient;
begin
  if FServerMethods1Client = nil then
    FServerMethods1Client:= TQtrMinClient.Create(DSRestConnection1, FInstanceOwner);
  Result := FServerMethods1Client;
end;

end.
