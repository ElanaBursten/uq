//
// Created by the DataSnap proxy generator.
// 11/9/2015 7:50:12 PM
// 

unit ClientClassesUnit2;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TServerMethods1Client = class(TDSAdminRestClient)
  private
    FGetQtrMinuteByCallCenterCommand: TDSRestCommand;
    FGetQtrMinuteByCallCenterCommand_Cache: TDSRestCommand;
    FGetQtrMinuteByAreaIDCommand: TDSRestCommand;
    FGetQtrMinuteByAreaIDCommand_Cache: TDSRestCommand;
    FPostQtrMinutesCommand: TDSRestCommand;
    FEchoStringCommand: TDSRestCommand;
    FReverseStringCommand: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetQtrMinuteByCallCenter(callCenter: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetQtrMinuteByCallCenter_Cache(callCenter: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GetQtrMinuteByAreaID(areaID: Integer; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetQtrMinuteByAreaID_Cache(areaID: Integer; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    procedure PostQtrMinutes(qtrMinuteCode: WideString; callCenter: WideString; areaID: Integer);
    function EchoString(Value: string; const ARequestFilter: string = ''): string;
    function ReverseString(Value: string; const ARequestFilter: string = ''): string;
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>, IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TServerMethods1_GetQtrMinuteByCallCenter: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'callCenter'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetQtrMinuteByCallCenter_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'callCenter'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_GetQtrMinuteByAreaID: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetQtrMinuteByAreaID_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_PostQtrMinutes: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'qtrMinuteCode'; Direction: 1; DBXType: 26; TypeName: 'WideString'),
    (Name: 'callCenter'; Direction: 1; DBXType: 26; TypeName: 'WideString'),
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer')
  );

  TServerMethods1_EchoString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods1_ReverseString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

implementation

function TServerMethods1Client.GetQtrMinuteByCallCenter(callCenter: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetQtrMinuteByCallCenterCommand = nil then
  begin
    FGetQtrMinuteByCallCenterCommand := FConnection.CreateCommand;
    FGetQtrMinuteByCallCenterCommand.RequestType := 'GET';
    FGetQtrMinuteByCallCenterCommand.Text := 'TServerMethods1.GetQtrMinuteByCallCenter';
    FGetQtrMinuteByCallCenterCommand.Prepare(TServerMethods1_GetQtrMinuteByCallCenter);
  end;
  FGetQtrMinuteByCallCenterCommand.Parameters[0].Value.SetWideString(callCenter);
  FGetQtrMinuteByCallCenterCommand.Execute(ARequestFilter);
  if not FGetQtrMinuteByCallCenterCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetQtrMinuteByCallCenterCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetQtrMinuteByCallCenterCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetQtrMinuteByCallCenterCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetQtrMinuteByCallCenter_Cache(callCenter: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetQtrMinuteByCallCenterCommand_Cache = nil then
  begin
    FGetQtrMinuteByCallCenterCommand_Cache := FConnection.CreateCommand;
    FGetQtrMinuteByCallCenterCommand_Cache.RequestType := 'GET';
    FGetQtrMinuteByCallCenterCommand_Cache.Text := 'TServerMethods1.GetQtrMinuteByCallCenter';
    FGetQtrMinuteByCallCenterCommand_Cache.Prepare(TServerMethods1_GetQtrMinuteByCallCenter_Cache);
  end;
  FGetQtrMinuteByCallCenterCommand_Cache.Parameters[0].Value.SetWideString(callCenter);
  FGetQtrMinuteByCallCenterCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetQtrMinuteByCallCenterCommand_Cache.Parameters[1].Value.GetString);
end;

function TServerMethods1Client.GetQtrMinuteByAreaID(areaID: Integer; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetQtrMinuteByAreaIDCommand = nil then
  begin
    FGetQtrMinuteByAreaIDCommand := FConnection.CreateCommand;
    FGetQtrMinuteByAreaIDCommand.RequestType := 'GET';
    FGetQtrMinuteByAreaIDCommand.Text := 'TServerMethods1.GetQtrMinuteByAreaID';
    FGetQtrMinuteByAreaIDCommand.Prepare(TServerMethods1_GetQtrMinuteByAreaID);
  end;
  FGetQtrMinuteByAreaIDCommand.Parameters[0].Value.SetInt32(areaID);
  FGetQtrMinuteByAreaIDCommand.Execute(ARequestFilter);
  if not FGetQtrMinuteByAreaIDCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetQtrMinuteByAreaIDCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetQtrMinuteByAreaIDCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetQtrMinuteByAreaIDCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetQtrMinuteByAreaID_Cache(areaID: Integer; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetQtrMinuteByAreaIDCommand_Cache = nil then
  begin
    FGetQtrMinuteByAreaIDCommand_Cache := FConnection.CreateCommand;
    FGetQtrMinuteByAreaIDCommand_Cache.RequestType := 'GET';
    FGetQtrMinuteByAreaIDCommand_Cache.Text := 'TServerMethods1.GetQtrMinuteByAreaID';
    FGetQtrMinuteByAreaIDCommand_Cache.Prepare(TServerMethods1_GetQtrMinuteByAreaID_Cache);
  end;
  FGetQtrMinuteByAreaIDCommand_Cache.Parameters[0].Value.SetInt32(areaID);
  FGetQtrMinuteByAreaIDCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetQtrMinuteByAreaIDCommand_Cache.Parameters[1].Value.GetString);
end;

procedure TServerMethods1Client.PostQtrMinutes(qtrMinuteCode: WideString; callCenter: WideString; areaID: Integer);
begin
  if FPostQtrMinutesCommand = nil then
  begin
    FPostQtrMinutesCommand := FConnection.CreateCommand;
    FPostQtrMinutesCommand.RequestType := 'GET';
    FPostQtrMinutesCommand.Text := 'TServerMethods1.PostQtrMinutes';
    FPostQtrMinutesCommand.Prepare(TServerMethods1_PostQtrMinutes);
  end;
  FPostQtrMinutesCommand.Parameters[0].Value.SetWideString(qtrMinuteCode);
  FPostQtrMinutesCommand.Parameters[1].Value.SetWideString(callCenter);
  FPostQtrMinutesCommand.Parameters[2].Value.SetInt32(areaID);
  FPostQtrMinutesCommand.Execute;
end;

function TServerMethods1Client.EchoString(Value: string; const ARequestFilter: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FConnection.CreateCommand;
    FEchoStringCommand.RequestType := 'GET';
    FEchoStringCommand.Text := 'TServerMethods1.EchoString';
    FEchoStringCommand.Prepare(TServerMethods1_EchoString);
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.Execute(ARequestFilter);
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethods1Client.ReverseString(Value: string; const ARequestFilter: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FConnection.CreateCommand;
    FReverseStringCommand.RequestType := 'GET';
    FReverseStringCommand.Text := 'TServerMethods1.ReverseString';
    FReverseStringCommand.Prepare(TServerMethods1_ReverseString);
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.Execute(ARequestFilter);
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

constructor TServerMethods1Client.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TServerMethods1Client.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TServerMethods1Client.Destroy;
begin
  FGetQtrMinuteByCallCenterCommand.DisposeOf;
  FGetQtrMinuteByCallCenterCommand_Cache.DisposeOf;
  FGetQtrMinuteByAreaIDCommand.DisposeOf;
  FGetQtrMinuteByAreaIDCommand_Cache.DisposeOf;
  FPostQtrMinutesCommand.DisposeOf;
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  inherited;
end;

end.
