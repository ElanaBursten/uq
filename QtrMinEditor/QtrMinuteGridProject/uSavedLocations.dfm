object frmSavedLocations: TfrmSavedLocations
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Saved Mapped Center Locations'
  ClientHeight = 264
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 408
    Height = 264
    Align = alClient
    TabOrder = 0
    object dbGridSaveLocations: TDBGrid
      Left = 1
      Top = 1
      Width = 406
      Height = 176
      Align = alTop
      DataSource = dsSavedLocations
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = dbGridSaveLocationsDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'savedLocName'
          Title.Caption = 'Name'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'savedLocLat'
          Title.Caption = 'Latitude'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'savedLocLng'
          Title.Caption = 'Longitude'
          Visible = True
        end>
    end
    object BitBtn1: TBitBtn
      Left = 176
      Top = 224
      Width = 75
      Height = 25
      Kind = bkClose
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BitBtn1Click
    end
    object DBNavigator1: TDBNavigator
      Left = 88
      Top = 183
      Width = 234
      Height = 25
      DataSource = dsSavedLocations
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
      TabOrder = 2
    end
  end
  object savedLocations: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'savedLocName'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'savedLocLat'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'savedLocLng'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    BeforePost = savedLocationsBeforePost
    Left = 128
    Top = 80
  end
  object dsSavedLocations: TDataSource
    DataSet = savedLocations
    Left = 224
    Top = 80
  end
end
