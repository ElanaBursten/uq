unit uAreaSel;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, uMainForm;

type
  TdlgAreaSelect = class(TForm)
    rgAreaSelect: TRadioGroup;
    Panel1: TPanel;
    edtCntSel: TEdit;
    Label1: TLabel;
    procedure rgAreaSelectClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgAreaSelect: TdlgAreaSelect;

implementation

{$R *.dfm}

procedure TdlgAreaSelect.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  action  := caFree;
end;

procedure TdlgAreaSelect.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  iCntOut : integer;
begin
  if not(TryStrToInt(edtCntSel.Text, iCntOut)) then
  begin
    showmessage('Only a valid number for How Many');
    edtCntSel.Text := '';
    SetFocusedControl(edtCntSel);
    rgAreaSelect.ItemIndex := -1;
    CanClose := false;
  end
  else
  begin
    frmMain.cntSelected := iCntOut;
    CanClose := true;
  end;
end;

procedure TdlgAreaSelect.rgAreaSelectClick(Sender: TObject);
begin
  self.ModalResult :=  rgAreaSelect.ItemIndex +101;
end;

end.
