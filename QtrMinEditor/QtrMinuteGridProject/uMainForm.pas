{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
unit uMainForm;

interface

uses
  SysUtils, OleCtrls, SHDocVw, Classes, Controls, ComCtrls, Forms, MSHTML,
  System.Types, System.Variants, System.UITypes, System.StrUtils,
  Data.DB, Data.Win.ADODB, Vcl.StdCtrls, Data.FireDACJSONReflect,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.Buttons, Vcl.Menus,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, Winapi.Windows, Winapi.Messages,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.StorageBin, FireDAC.Stan.StorageJSON,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, ClientModuleUnit2,
  ClientClassesUnit2, Vcl.DBCtrls, Vcl.Tabs, uSavedLocations, Data.Bind.Components,
  Data.Bind.ObjectScope, REST.Client, REST.Authenticator.Basic,
  uWVBrowser, uWVWinControl,
  uWVWindowParent, uWVTypes, uWVConstants, uWVTypeLibrary,
  uWVLibFunctions, uWVLoader, uWVInterfaces, uWVCoreWebView2Args,
  uWVBrowserBase, dxSkinsCore, dxCore, cxClasses, cxLookAndFeels, dxSkinsForm, dxSkinBasic, dbisamtb;

type
  TlineType = (ltLat, ltLng);

type
  TinsertMode = (imAdd, imDel);

type
  TViewMode = (vmMap, vmView);
type
  TmapRecord = packed record
    mapID: string[8];
    d: TDateTime;
  end;
type
  TfrmMain = class(TForm)
    pnlGridSide: TPanel;
    Splitter1: TSplitter;
    puWebMenu: TPopupMenu;
    Accept1: TMenuItem;
    clearGrid: TMenuItem;
    StatusBar1: TStatusBar;
    FDmemQtrMinute: TFDMemTable;
    FDStanStorageJSONLink1: TFDStanStorageJSONLink;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    FDmemQtrMinPost: TFDMemTable;
    FDmemQtrMinPostarea_id: TIntegerField;
    FDmemQtrMinPostqtrmin_lat: TStringField;
    FDmemQtrMinPostqtrmin_long: TStringField;
    FDmemQtrMinPostcall_center: TStringField;
    dsViewList: TDataSource;
    DrawSelected1: TMenuItem;
    RetrieveListinView1: TMenuItem;
    pnlWebPage: TPanel;
    UnMapSelected1: TMenuItem;
    MapAllinView1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    GetAllforSelected1: TMenuItem;
    FDwhoIsHereList: TFDMemTable;
    dsWhoIsHere: TDataSource;
    MapAllforList1: TMenuItem;
    dsMapList: TDataSource;
    FDmemMapList: TFDMemTable;
    RetriveAreasbyMap1: TMenuItem;
    N3: TMenuItem;
    tsViewMode: TTabSet;
    gridPanel: TPanel;
    mapAreaList: TDBGrid;
    viewList: TDBGrid;
    DBNavigator1: TDBNavigator;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    Edit1: TMenuItem;
    Copy1: TMenuItem;
    Cut1: TMenuItem;
    Paste1: TMenuItem;
    N4: TMenuItem;
    Exit1: TMenuItem;
    DeleteFromDatabase1: TMenuItem;
    FDwhoIsHereListarea_id: TIntegerField;
    FDwhoIsHereListarea_name: TStringField;
    FDwhoIsHereListemp_id: TIntegerField;
    FDwhoIsHereListshort_name: TStringField;
    GotoSelected1: TMenuItem;
    Control1: TMenuItem;
    NoLabels1: TMenuItem;
    FDmemViewList: TFDMemTable;
    FDmemViewListColor: TIntegerField;
    FDmemViewListarea_id: TIntegerField;
    FDmemViewListcall_center: TStringField;
    FDmemViewListemp_id: TIntegerField;
    FDmemViewListshort_name: TStringField;
    FDmemViewListWebColor: TStringField;
    DelAllQtrMinFromSelected1: TMenuItem;
    cbExcludeMap: TCheckBox;
    ProgressBar1: TProgressBar;
    DrawCounties1: TMenuItem;
    pnlBottom: TPanel;
    btnFindAddress: TButton;
    edtFindAddress: TEdit;
    btnClear: TBitBtn;
    edtLng: TEdit;
    edtLat: TEdit;
    btnGoto: TButton;
    btnFindQtrMin: TButton;
    edtQtrMin: TEdit;
    Label2: TLabel;
    whoHereList: TDBGrid;
    Splitter2: TSplitter;
    FDmemMapListColor: TIntegerField;
    FDmemMapListmap_id: TIntegerField;
    FDmemMapListmap_name: TStringField;
    FDmemMapListarea_id: TIntegerField;
    FDmemMapListarea_name: TStringField;
    FDmemMapListWebColor: TStringField;
    FDmemMapListcentroid_lat: TStringField;
    FDmemMapListcentroid_lng: TStringField;
    FDmemMapListstate: TStringField;
    FDmemMapListshort_name: TStringField;
    LargeAreaAlert1: TMenuItem;
    WVWindowParent1: TWVWindowParent;
    Timer1: TTimer;
    WVBrowser1: TWVBrowser;
    Memo1: TMemo;
    dxSkinController1: TdxSkinController;
    dbIsamDB: TDBISAMDatabase;
    qryGeoMapsAPIkey: TDBISAMQuery;
    procedure FormCreate(Sender: TObject);
    procedure btnFindQtrMinClick(Sender: TObject);

    procedure btnSaveClick(Sender: TObject);
    procedure btnGotoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure viewListDrawColumnCell(Sender: TObject; const [Ref] Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure DrawSelected1Click(Sender: TObject);

    procedure RetrieveListinView1Click(Sender: TObject);
    procedure clearGridClick(Sender: TObject);
    procedure UnMapSelected1Click(Sender: TObject);
    procedure MapAllinView1Click(Sender: TObject);
    procedure GetAllforSelected1Click(Sender: TObject);
    procedure RetriveAreasbyMap1Click(Sender: TObject);
    procedure mapAreaListDrawColumnCell(Sender: TObject;
      const [Ref] Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure tsViewModeChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure MapAllforList1Click(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure DeleteFromDatabase1Click(Sender: TObject);
    procedure GotoSelected1Click(Sender: TObject);
    procedure NoLabels1Click(Sender: TObject);
    procedure btnFindAddressClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure DelAllQtrMinFromSelected1Click(Sender: TObject);
    procedure DrawCounties1Click(Sender: TObject);
    procedure edtFindAddressDblClick(Sender: TObject);
    procedure WVBrowser1AfterCreated(Sender: TObject);
    procedure WVBrowser1InitializationError(Sender: TObject;
      aErrorCode: HRESULT; const aErrorMessage: wvstring);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure WVBrowser1WebMessageReceived(Sender: TObject;
      const aWebView: ICoreWebView2;
      const aArgs: ICoreWebView2WebMessageReceivedEventArgs);
    procedure Memo1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);

  private
    iSavedCnt: Integer;
    fcntSelected: Integer;
    fActiveAreaID: Integer;
    colorList: TStringList;
    colorViewList: TStringList;
    flastUsedColor: Integer;
    FviewNorth: string;
    FviewWest: string;
    FviewEast: string;
    FviewSouth: string;
    fViewMode: TViewMode;
    flastLat: string;
    flastLng: string;
    fInsertMode: TinsertMode;
    fMapID: Integer;
    flastUsedViewColor: Integer;
    fActiveState: string;
    fActivePassword: string;
    fActiveEmpID: string;
    MarkerSet: boolean;
    procedure WMCopyData(var Msg : TWMCopyData); message WM_COPYDATA;
    procedure HandleCopyDataRecord(copyDataStruct: PCopyDataStruct);
    function extractDMS(qtrMinPt: String): real;
    procedure GetQtrMinutes(const iAreaID: Integer;
      sCallCenter, htmlColor: String);
    function GetQtrMinInView(const areaID: Integer;
      callCenter, htmlColor: string): Boolean;
    function SaveToServer: TFDJSONDataSets;
    function GetQtrMinCount(iAreaID: Integer; sCallCenter: String): Integer;
    procedure primeColorList;
    function GetActiveAreaId: Integer;
    function GetActiveCallCenter: string;
    procedure DrawAllNeighbors;
    function GetAreasByMap(MapID: Integer): Boolean;
    function LoadParams: Boolean;
    procedure ReDrawAfterSave;
    function GetActiveCentroidLat: string;
    function GetActiveCentroidLng: string;
    procedure timeOutMsgBox(sMsg, sCaption: WideString; timeOut: integer);
    procedure DelAllQtrMinByAreaID(Sender: TObject);
    procedure DrawCounties;
    procedure LoadFromFileAsString(const aFileName: string);
    procedure CreateMap(Sender: TObject);
    procedure RunMethod(Sender: TObject; methodName: string);
    procedure WriteToFireDac(Sender: TObject);
  protected

  public
    RowNo: Integer;
    btnButton: TButton;
    fKmzUrlPath :string;

    property ActiveEmpID    : string read fActiveEmpID;
    property ActivePassword : string read fActivePassword;
    property ActiveCentroidLat: string read GetActiveCentroidLat;
    property ActiveCentroidLng: string read GetActiveCentroidLng;
    property insertMode: TinsertMode read fInsertMode write fInsertMode;
    property lastLat: string read flastLat write flastLat;
    property lastLng: string read flastLng write flastLng;
    property ActiveCallCenter: string read GetActiveCallCenter;
    property ViewMode: TViewMode read fViewMode write fViewMode;
    property ActiveAreaId: Integer read GetActiveAreaId write fActiveAreaID;
    property viewNorth: string read FviewNorth write FviewNorth;
    property viewSouth: string read FviewSouth write FviewSouth;
    property viewEast: string read FviewEast write FviewEast;
    property viewWest: string read FviewWest write FviewWest;
    property cntSelected: Integer read fcntSelected write fcntSelected;
    property lastUsedColor: Integer read flastUsedColor write flastUsedColor;
    property lastUsedViewColor: Integer read flastUsedViewColor
      write flastUsedViewColor;

    procedure SaveToSelected(Sender: TObject);
    function GetWhoHereList(qtrMinLat, qtrMinLng: string): Boolean;
    function GetAllInViewList(sNorth, sSouth, sEast, sWest: WideString; mapID:integer = -1)
      : Boolean;
  end;
  function GetFileVersion(exeName : string): string;
var
  frmMain: TfrmMain;

const
  sQTRMINUTES = 'QtrMinutes';
  spRETURN = 'Records added';
  llOffset: real = ((1 / 60) / 8);

implementation

uses dialogs, uAreaSel, System.JSON, DateUtils, Vcl.Graphics,
  MsgBoxTimeOut, WinAPI.ShellAPI;


{$R *.dfm}

procedure TfrmMain.btnFindQtrMinClick(Sender: TObject);
var
  qtrMin, sLat, sLng: String;
begin
  qtrMin := edtQtrMin.Text;
  sLat := copy(qtrMin, 0, 5);
  sLng := copy(qtrMin, 6, 12);
  sLat := FloatToStr(extractDMS(sLat) + llOffset);
  sLng := FloatToStr((extractDMS(sLng) - llOffset) * -1);
   WVBrowser1.ExecuteScript(Format('GotoLatLng(%s,%s)', [sLat, sLng]));
end;

procedure TfrmMain.edtFindAddressDblClick(Sender: TObject);
begin
  edtFindAddress.text := '';
end;

function TfrmMain.extractDMS(qtrMinPt: String): real;
var
  sD, sM, sS: string;

  iDeg, iSec, qlen: Integer;
  dDecimal, dMin, dSec: real;
begin
  qlen := length(qtrMinPt);
  iSec := 0;
  if qlen = 5 then
    sD := copy(qtrMinPt, 0, 2)
  else
    sD := copy(qtrMinPt, 0, 3);

  if qlen = 5 then
    sM := copy(qtrMinPt, 3, 2)
  else
    sM := copy(qtrMinPt, 4, 2);

  if qlen = 5 then
    sS := copy(qtrMinPt, 5, 1)
  else
    sS := copy(qtrMinPt, 6, 1);

  if sS = 'D' then
    iSec := 0
  else
    if sS = 'C' then
      iSec := 15
    else
      if sS = 'B' then
        iSec := 30
      else
        if sS = 'A' then
          iSec := 45;

  dMin := 0.00;
  dSec := 0.00;

  if StrToInt(sM) <> 0 then
    dMin := (StrToInt(sM) / 60);
  if iSec <> 0 then
    dSec := iSec / 3600;

  iDeg := StrToInt(sD);

  result := iDeg + dMin + dSec;
end;

procedure TfrmMain.btnSaveClick(Sender: TObject);
begin
  SaveToSelected(Sender);
end;


procedure TfrmMain.SaveToSelected(Sender: TObject);
begin
  if ((Sender as TMenuItem).Name = 'DeleteFromDatabase1') then
  begin
    insertMode := imDel;
    memo1.Lines.Add('insertMode:Del ');
  end
  else
  if  ((Sender as TMenuItem).Name = 'Accept1') then
  begin
    insertMode := imAdd;
    memo1.Lines.Add('insertMode:Add ');
  end;
  FDmemQtrMinPost.Open;
  WVBrowser1.ExecuteScript('SaveQtrMin()');   //is this firing??
  memo1.Lines.Add('Ran script: '+'SaveQtrMin()');
end;

procedure TfrmMain.WriteToFireDac(Sender: TObject);
var
  iCnt: Integer;
  sAreaID, sCallCenter: string;
  tf, ti: TDatetime;
  sSecs: string;
const
  STATUS_TXT = 'Saving %d Quarter Minutes for Area %s of Call Center %s';
const
  STATUS_TXT2 =
    '%d Quarter Minutes Sent, %d Quarter Minutes Saved for Area %s of Call Center %s in %s seconds';

begin
  sAreaID := IntToStr(ActiveAreaId);

  sCallCenter := ActiveCallCenter;
  ti := now;
  iCnt := FDmemQtrMinPost.RecordCount;
  StatusBar1.panels[0].text := Format(STATUS_TXT,
    [iCnt, sAreaID, ActiveCallCenter]);
  SaveToServer;
  tf := now;
  sSecs := IntToStr(secondsBetween(ti, tf));
  StatusBar1.panels[0].text := Format(STATUS_TXT2, [iCnt, iSavedCnt, sAreaID,
    sCallCenter, sSecs]);
    ReDrawAfterSave;

  Accept1.Enabled := false;    //reversed for testing
end;

procedure TfrmMain.btnClearClick(Sender: TObject);
begin
  if MarkerSet then
    WVBrowser1.ExecuteScript('clearMarker()');
    edtFindAddress.clear;
    edtFindAddress.Refresh;
end;

procedure TfrmMain.btnFindAddressClick(Sender: TObject);
begin
  WVBrowser1.ExecuteScript(Format('codeAddress(''%s'')', [edtFindAddress.Text]));
  MarkerSet := true;
end;

procedure TfrmMain.MapAllforList1Click(Sender: TObject);
begin
  DrawAllNeighbors;
end;

procedure TfrmMain.MapAllinView1Click(Sender: TObject);
begin
  DrawAllNeighbors;
end;

procedure TfrmMain.mapAreaListDrawColumnCell(Sender: TObject;
  const [Ref] Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (DataCol = 0) and not(FDmemMapList.Fields[0].IsNull) then
  begin
    mapAreaList.Canvas.Brush.Color := FDmemMapList.Fields[0].AsVariant;
    mapAreaList.Canvas.FillRect(Rect);
  end;
end;

procedure TfrmMain.Memo1DblClick(Sender: TObject);
begin
  memo1.Clear;
end;

procedure TfrmMain.DelAllQtrMinFromSelected1Click(Sender: TObject);
begin
  if (MessageDlg('This will remove all the Quarter Minutes for the Area ID selected.  '
  +#13+#10+'Only the Area Id is used.  What is on the map has no effect. '
  +#13+#10+'It cannot be undone except to redraw the Quarter Minutes for the Area.'
  +#13+#10+'To continue, press OK.'+#13+#10+'To abort, press Cancel.',
  mtWarning, [mbOK, mbCancel], 0) = mrOk) then
  DelAllQtrMinByAreaID(Sender);
end;

procedure TfrmMain.DelAllQtrMinByAreaID(Sender: TObject);
begin
  ClientModule2.ServerMethods1Client.DELAreaById(ActiveEmpID, ActivePassword, ActiveAreaID);
  UnMapSelected1Click(Sender);
end;

procedure TfrmMain.DeleteFromDatabase1Click(Sender: TObject);
begin
  if (MessageDlg('This will delete ALL Quarter Minutes that are Fenced, regardless of Area Id or Call Center.  '
  +#13+#10+'There is no way to undo this process, except to redraw the Quarter Minutes.'
  +#13+#10+''
  +#13+#10+'Press Ok to continue or Cancel to abort this activity.',
  mtWarning, [mbOK, mbCancel], 0) = mrOk) then
  btnSaveClick(Sender);
end;

procedure TfrmMain.DrawAllNeighbors;
begin
  case ViewMode of
    vmMap:
      begin
        FDmemMapList.first;
        while not(FDmemMapList.eof) do
        begin
          DrawSelected1Click(nil);
          FDmemMapList.next;
        end;
      end;
    vmView:
      begin
        FDmemViewList.first;
        while not(FDmemViewList.eof) do
        begin
          DrawSelected1Click(nil);
          FDmemViewList.next;
        end;
      end;
  end;
end;

procedure TfrmMain.DrawCounties1Click(Sender: TObject);
begin
  DrawCounties;
end;

procedure TfrmMain.ReDrawAfterSave;
var
  htmlColor, webColor: string;
begin
  case ViewMode of
    vmMap:
      begin
        WVBrowser1.ExecuteScript('clearMarker()');
        WVBrowser1.ExecuteScript('ClearGrid()');
        WVBrowser1.ExecuteScript('ClearUserGrid()');
        lastUsedColor := -1;

        DrawAllNeighbors;
      end;

    vmView:
      begin
        webColor := FDmemViewList.Fields[5].AsString;
        WVBrowser1.ExecuteScript(Format('deListOneColor("%s")', [webColor]));

        htmlColor := FDmemViewList.FieldByName('WebColor').AsString;
        GetQtrMinutes(ActiveAreaId, ActiveCallCenter, htmlColor);
      end;
  end;

end;

procedure TfrmMain.DrawSelected1Click(Sender: TObject);
var
  htmlColor: string;
begin
  case ViewMode of
    vmMap:
      begin
        if (colorViewList.Count = flastUsedColor) then
        begin
          ShowMessage('There are too many areas to map.  Only 24 areas can be mapped!');
          exit;
        end;
        inc(flastUsedColor);
        if not(mapAreaList.DataSource.DataSet.State in [dsEdit, dsInsert]) then
          mapAreaList.DataSource.DataSet.edit;

        FDmemMapList.Fields[0].AsVariant :=
          TColor(colorList.Objects[lastUsedColor]);
        htmlColor := colorList[lastUsedColor];
        FDmemMapList.FieldByName('WebColor').AsString := htmlColor;
        GetQtrMinutes(ActiveAreaId, ActiveCallCenter, htmlColor);
      end;

    vmView:
      begin
        if colorViewList.Count = flastUsedViewColor then
        begin
          ShowMessage('There are too many areas to map.  Only 24 areas can be mapped!');
          exit;
        end;
        inc(flastUsedViewColor);
        if not(viewList.DataSource.DataSet.State in [dsEdit, dsInsert]) then
          viewList.DataSource.DataSet.edit;
        FDmemViewList.Fields[0].AsVariant :=
          TColor(colorViewList.Objects[lastUsedViewColor]);
        htmlColor := colorViewList[lastUsedViewColor];
        FDmemViewList.FieldByName('WebColor').AsString := htmlColor;
        GetQtrMinInView(ActiveAreaId, ActiveCallCenter, htmlColor);
      end;
  end;
end;

procedure TfrmMain.Save1Click(Sender: TObject);
begin
  frmSavedLocations := TfrmSavedLocations.create(self, lastLat, lastLng);
  frmSavedLocations.Show;
end;

function TfrmMain.SaveToServer: TFDJSONDataSets;
var
  resultCnt: TJsonObject;
begin
  result := TFDJSONDataSets.create;
  TFDJSONDataSetsWriter.ListAdd(result, FDmemQtrMinPost);
  resultCnt := ClientModule2.ServerMethods1Client.AddCallCenterQtrMinutes(ActiveEmpID, ActivePassword, result);
  if not(TryStrToInt(resultCnt.GetValue('cnt').Value, iSavedCnt)) then
    iSavedCnt := 0;
end;

procedure TfrmMain.UnMapSelected1Click(Sender: TObject);
var
  webColor: string;
begin
  case ViewMode of
    vmMap:
      begin
        if not(mapAreaList.DataSource.DataSet.State in [dsEdit, dsInsert]) then
          FDmemMapList.edit;

        webColor := FDmemMapList.Fields[5].AsString;
        WVBrowser1.ExecuteScript(Format('deListOneColor("%s")', [webColor]));
        FDmemMapList.Fields[0].AsVariant := NULL;
        mapAreaList.Repaint;

      end;
    vmView:
      begin
        if not(viewList.DataSource.DataSet.State in [dsEdit, dsInsert]) then
          FDmemViewList.edit;

        webColor := FDmemViewList.Fields[5].AsString;
        WVBrowser1.ExecuteScript(Format('deListOneColor("%s")', [webColor]));
        FDmemViewList.Fields[0].AsVariant := NULL;
        viewList.Repaint;
      end;
  end;
end;

procedure TfrmMain.btnGotoClick(Sender: TObject);
var
  sLat, sLng: string;
begin
  sLat := edtLat.Text;
  sLng := edtLng.Text;

  WVBrowser1.ExecuteScript(Format('GotoLatLng(%s,%s)', [sLat, sLng]));
end;

function TfrmMain.GetActiveAreaId: Integer;
begin
  case ViewMode of
    vmMap:
      result := FDmemMapList.FieldByName('Area_id').AsInteger;
    vmView:
      result := FDmemViewList.FieldByName('Area_id').AsInteger;
  end;
end;

function TfrmMain.GetActiveCallCenter: string;
begin
  case ViewMode of
    vmMap:
      result := '0';
    vmView:
      begin
        if FDmemViewList.FieldByName('call_center').AsString <> '' then
          result := FDmemViewList.FieldByName('call_center').AsString
        else
          result := '0';
      end;
  end;
end;

function TfrmMain.GetActiveCentroidLat: string;
begin
  case ViewMode of
    vmMap:
      result := FDmemMapList.FieldByName('centroid_lat').AsString;
    vmView:
      result := FDmemViewList.FieldByName('centroid_lat').AsString;
  end;
end;

function TfrmMain.GetActiveCentroidLng: string;
const
  minus = '-';
begin
  case ViewMode of
    vmMap:
      result := minus + FDmemMapList.FieldByName('centroid_lng').AsString;
    vmView:
      result := minus + FDmemViewList.FieldByName('centroid_lng').AsString;
  end;
end;

procedure TfrmMain.GetAllforSelected1Click(Sender: TObject);
{ Retrieves all quarter minutes, not limited by bounds }
begin
  case ViewMode of
    vmMap:
      GetQtrMinutes(ActiveAreaId, ActiveCallCenter,
        FDmemMapList.FieldByName('webColor').AsString);
    vmView:
      GetQtrMinutes(ActiveAreaId, ActiveCallCenter,
        FDmemViewList.FieldByName('webColor').AsString);
  end;
end;

function TfrmMain.GetAllInViewList(sNorth, sSouth, sEast,
  sWest: WideString; mapID:integer = -1): Boolean;
var
  LDataSetList: TFDJSONDataSets;
begin
  // Cnt, qm.area_id, qm.call_center, employee.emp_id, Rtrim(employee.short_name)short_name
  // It empties the memory table of any existing data before adding the new context.
  if cbExcludeMap.Checked then
   mapID := fMapID;
  FDmemViewList.Close;
  // @Color as Color, qm.area_id, qm.call_center, employee.emp_id, Rtrim(employee.short_name)short_name,@WebColor as WebColor
  // Get dataset list containing QtrMinutes
  LDataSetList := ClientModule2.ServerMethods1Client.GETViewList(ActiveEmpID, ActivePassword, sNorth, sSouth,
    sEast, sWest, mapID);
  FDmemViewList.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList, 0));
  FDmemViewList.Open;
  result := true;
end;

function TfrmMain.GetAreasByMap(MapID: Integer): Boolean;
var
  LDataSetList: TFDJSONDataSets;
begin
  try
    // It empties the memory table of any existing data before adding the new context.
    FDmemMapList.Close;

    // Get dataset list containing QtrMinutes
    LDataSetList := ClientModule2.ServerMethods1Client.GetAreasByMap(ActiveEmpID, ActivePassword, MapID);
    FDmemMapList.AppendData(TFDJSONDataSetsReader.GetListValue
      (LDataSetList, 0));

    FDmemMapList.Open;
    fActiveState := FDmemMapList.FieldByName('state').AsString;
  except
    on E: Exception do
    begin
      MessageDlg
        ('There was a problem connecting to the QtrMinApi server.  It has returned an error message: '
        + E.Message + #10#13 +#10#13 + ' Please contact UtiliQuest IT for assistance.',
        mtError, [mbOK], 0);

      result := false;
    end;
  end;
  result := true;
end;

function TfrmMain.GetWhoHereList(qtrMinLat, qtrMinLng: string): Boolean;
var
  LDataSetList: TFDJSONDataSets;
begin
  FDwhoIsHereList.Close;
  // Get dataset list containing QtrMinutes
  LDataSetList := ClientModule2.ServerMethods1Client.GETWhoIsHere(ActiveEmpID, ActivePassword, qtrMinLat,
    qtrMinLng);
  FDwhoIsHereList.AppendData(TFDJSONDataSetsReader.GetListValue
    (LDataSetList, 0));

  FDwhoIsHereList.Open;
  if FDwhoIsHereList.IsEmpty then
  begin
    whoHereList.BorderStyle := bsNone;
    whoHereList.Repaint;
    sleep(200);
  end;
  whoHereList.BorderStyle := bsSingle;
  whoHereList.Repaint;
  result := true;
end;

procedure TfrmMain.GotoSelected1Click(Sender: TObject);
begin
  if ActiveCentroidLat <> '' then
    WVBrowser1.ExecuteScript(Format('GotoLatLng(%s,%s)', [ActiveCentroidLat,
      ActiveCentroidLng]));
end;

procedure TfrmMain.HandleCopyDataRecord(copyDataStruct: PCopyDataStruct);
var
  mapRecord : TmapRecord;
begin
  mapRecord.mapID := TmapRecord(copyDataStruct.lpData^).mapID;
  mapRecord.d := TmapRecord(copyDataStruct.lpData^).d;
  fMapID := StrToInt(mapRecord.mapID);
  clearGridClick(nil);
  FormShow(nil);
end;

function TfrmMain.GetQtrMinCount(iAreaID: Integer; sCallCenter: String)
  : Integer;
var
  resultCnt: TJsonObject;
begin
  resultCnt := ClientModule2.ServerMethods1Client.GETQtrMinuteCount(ActiveEmpID, ActivePassword, iAreaID,
    sCallCenter);
  If not(TryStrToInt(resultCnt.GetValue('cnt').Value, result)) then
    result := 0;
end;

function TfrmMain.GetQtrMinInView(const areaID: Integer;
  callCenter, htmlColor: string): Boolean;
var
  LDataSetList: TFDJSONDataSets;
  fLat, fLng: double;
  sLat, sLng: String;
begin
  FDmemQtrMinute.Close;

  // Get dataset list containing QtrMinutes
  LDataSetList := ClientModule2.ServerMethods1Client.GETQtrMinutesInView(ActiveEmpID, ActivePassword, areaID,
    callCenter, viewNorth, viewSouth, viewEast, viewWest);
  FDmemQtrMinute.AppendData(TFDJSONDataSetsReader.GetListValue
    (LDataSetList, 0));
  // It uses a reader from The "TFDJSONDataSetsWriter" class to populate the memory table from the dataset list class.
  ProgressBar1.position := 0;
  FDmemQtrMinute.Open;
  while not(FDmemQtrMinute.eof) do
  begin

    sLat := FDmemQtrMinute.FieldByName('qtrmin_lat').AsString;
    sLng := FDmemQtrMinute.FieldByName('qtrmin_long').AsString;

    fLat := extractDMS(sLat) + llOffset;
    fLng := (extractDMS(sLng) - llOffset) * -1;

    sLat := FloatToStr(fLat);
    sLng := FloatToStr(fLng);

    WVBrowser1.ExecuteScript(Format('MarkArea(%s,%s,"%s")',
      [sLat, sLng, htmlColor]));

    FDmemQtrMinute.next;
    ProgressBar1.StepIt;
  end;
  result := true;
end;

procedure TfrmMain.GetQtrMinutes(const iAreaID: Integer;
  sCallCenter, htmlColor: String);
const
  STATUS_TXT =
    'Drawing %d Quarter Minutes- %s selected for Area %d of Call Center %s in %s seconds';
  NO_QTR_MIN = 'There are no Quarter Minutes for %d';
var
  LDataSetList: TFDJSONDataSets;

  optSelected, cnt: Integer;
  tf, ti: TDatetime;
  sLat, sLng, sSecs: String;
  fLat, fLng, midLat, midLng: double;
  sMsg : WideString;
begin
  optSelected:=0;
  ti := now();

  cnt := GetQtrMinCount(iAreaID, sCallCenter);
  if cnt = 0 then
  begin
    sMsg := 'There are no Quarter Minutes for ' + IntToStr(iAreaID);
    timeOutMsgBox(sMsg, 'Drawing Quarter Minutes', 2000);

    UnMapSelected1Click(nil);
    exit;
  end;

  if (cnt > 2000) and (LargeAreaAlert1.checked) then
  begin
    case MessageDlg(IntToStr(cnt) +
      ' is a lot of Quarter Minutes to draw.  Would you like to draw a smaller area and see if that works?',
      mtWarning, [mbYes, mbNo, mbCancel], 0) of
      mrYes:
        begin
          dlgAreaSelect := TdlgAreaSelect.create(self);
          dlgAreaSelect.ShowModal;
          optSelected := dlgAreaSelect.ModalResult;
        end;

      mrCancel:
        exit;
    end;
  end;

  sLat := '';
  sLng := '';
  midLat := 0.0;
  midLng := 0.0;
  cnt := 0;
  // It empties the memory table of any existing data before adding the new context.
  FDmemQtrMinute.Close;

  // Get dataset list containing QtrMinutes
  LDataSetList := ClientModule2.ServerMethods1Client.GetQtrMinutes(ActiveEmpID, ActivePassword, iAreaID,
    sCallCenter, optSelected, cntSelected);
  FDmemQtrMinute.AppendData(TFDJSONDataSetsReader.GetListValue
    (LDataSetList, 0));
  // It uses a reader from The "TFDJSONDataSetsWriter" class to populate the memory table from the dataset list class.
  ProgressBar1.position := 0;
  FDmemQtrMinute.Open;
  while not(FDmemQtrMinute.eof) do
  begin
    inc(cnt);
    sLat := FDmemQtrMinute.FieldByName('qtrmin_lat').AsString;
    sLng := FDmemQtrMinute.FieldByName('qtrmin_long').AsString;
    if (sLat='') or (sLng='') then
    begin
      FDmemQtrMinute.next;
      continue;
    end;

    try
       fLat := extractDMS(sLat) + llOffset;
       fLng := (extractDMS(sLng) - llOffset) * -1;
    except on E: Exception do
    begin
      showmessage('Line 898:'+e.Message);
    end;
    end;
    midLat := (fLat + midLat);
    midLng := (fLng + midLng);
    sLat := FloatToStr(fLat);
    sLng := FloatToStr(fLng);


    WVBrowser1.ExecuteScript(Format('MarkArea(%s,%s,"%s")',
      [sLat, sLng, htmlColor]));
    FDmemQtrMinute.next;
    ProgressBar1.StepIt;
  end;

  tf := now;
  sSecs := IntToStr(secondsBetween(ti, tf));
  case optSelected of
    0:
      StatusBar1.panels[0].text := Format(STATUS_TXT, [cnt, 'all regions', iAreaID,
        sCallCenter, sSecs]);
    101:
      StatusBar1.panels[0].text := Format(STATUS_TXT,
        [cnt, 'north', iAreaID, sCallCenter, sSecs]);
    102:
      StatusBar1.panels[0].text := Format(STATUS_TXT,
        [cnt, 'south', iAreaID, sCallCenter, sSecs]);
    103:
      StatusBar1.panels[0].text := Format(STATUS_TXT, [cnt, 'northEast', iAreaID,
        sCallCenter, sSecs]);
    104:
      StatusBar1.panels[0].text := Format(STATUS_TXT, [cnt, 'southEast', iAreaID,
        sCallCenter, sSecs]);
    105:
      StatusBar1.panels[0].text := Format(STATUS_TXT,
        [cnt, 'east', iAreaID, sCallCenter, sSecs]);
    106:
      StatusBar1.panels[0].text := Format(STATUS_TXT,
        [cnt, 'west', iAreaID, sCallCenter, sSecs]);
    107:
      StatusBar1.panels[0].text := Format(STATUS_TXT, [cnt, 'northWest', iAreaID,
        sCallCenter, sSecs]);
    108:
      StatusBar1.panels[0].text := Format(STATUS_TXT, [cnt, 'southWest', iAreaID,
        sCallCenter, sSecs]);
  end;
  WVBrowser1.ExecuteScript(Format('GotoLatLng(%s,%s)', [FloatToStr(midLat / cnt),
    FloatToStr(midLng / cnt)]));
  viewList.Repaint;
  ProgressBar1.position := 0;
end;

procedure TfrmMain.viewListDrawColumnCell(Sender: TObject;
  const [Ref] Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (DataCol = 0) and not(FDmemViewList.Fields[0].IsNull) then
  begin
    viewList.Canvas.Brush.Color := FDmemViewList.Fields[0].AsVariant;
    viewList.Canvas.FillRect(Rect);
  end;
end;

procedure TfrmMain.WMCopyData(var Msg: TWMCopyData);
begin
  HandleCopyDataRecord(Msg.CopyDataStruct);
end;

procedure TfrmMain.WVBrowser1AfterCreated(Sender: TObject);
begin
  WVWindowParent1.UpdateSize;
  Caption := 'Quarter Minute Coloring Book on the Edge  --  In work';
  CreateMap(Sender);

end;

procedure TfrmMain.CreateMap(Sender: TObject);
var
  htmFilePath:Tfilename;
begin
   htmFilePath:=ExtractFilePath(paramStr(0))+ 'HTML\QtrMinGrid.htm';
   LoadFromFileAsString(htmFilePath);
end;

procedure TfrmMain.LoadFromFileAsString(const aFileName : string);
var
  TempLines : TStringList;
  idx:integer;
const
  GOOGLE_MAPS_API = 'src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJawB5LUbGAQkCJjtMw7F7Le9ZpOQP8oQ&amp;libraries=geometry"> ';
  PLACE_HOLDER= '[PUT_API_HERE]';
begin
  TempLines := nil;
  idx:=25;
  try
    try
      if (length(aFileName) > 0) and FileExists(aFileName) then
        begin
          TempLines := TStringList.Create;
          TempLines.LoadFromFile(aFileName);
          TempLines.Strings[idx]:=GOOGLE_MAPS_API;
          WVBrowser1.NavigateToString(TempLines.Text);
        end;
    except
      {$IFDEF DEBUG}
      on e : exception do
        OutputDebugString(PWideChar('LoadFromFileAsString error: ' + e.message + chr(0)));
      {$ENDIF}
    end;
  finally
    if assigned(TempLines) then
      FreeAndNil(TempLines);
  end;
end;

procedure TfrmMain.WVBrowser1InitializationError(Sender: TObject;
  aErrorCode: HRESULT; const aErrorMessage: wvstring);
begin
  showmessage(aErrorMessage);
end;

procedure TfrmMain.WVBrowser1WebMessageReceived(Sender: TObject;
  const aWebView: ICoreWebView2;
  const aArgs: ICoreWebView2WebMessageReceivedEventArgs);
var
  TempArgs : TCoreWebView2WebMessageReceivedEventArgs;
begin
  TempArgs := TCoreWebView2WebMessageReceivedEventArgs.Create(aArgs);
  Memo1.Lines.Add(TempArgs.WebMessageAsString);
  RunMethod(Sender, TempArgs.WebMessageAsString);
  TempArgs.Free;
end;

procedure TfrmMain.NoLabels1Click(Sender: TObject);
begin
  NoLabels1.Checked := not(NoLabels1.Checked);
  if NoLabels1.Checked then
  begin
    WVBrowser1.ExecuteScript('NoLabelsOn()');
    Memo1.Lines.Add('NoLabelsOn');
  end
  else
  begin
    WVBrowser1.ExecuteScript('NoLabelsOff()');
    Memo1.Lines.Add('NoLabelsOff');
  end;
end;

procedure TfrmMain.RunMethod(Sender: TObject; methodName: string);
const
  VISIBLE = 'visible';
  FUNCTION_NAME = 'CreateMask';
var

  sLat: String;
  sLng: String;
  sCallCenter: String;
  areaID: Integer;

  cnt: Integer;
  QuarterMinList: TStrings;
  i: Integer;
  qtrMinuteCode: WideString;
  QtrNorth, QtrSouth, QtrEast, QtrWest: WideString;
  functionName, retState: WideString;

  procedure DeleteLastChar(var aText: string; const aLastChar: string);
  begin    //QM-569
    If aText <> '' then
    if aText[Length(aText)] = aLastChar then
      SetLength(aText, Length(aText) - 1);
  end;

begin
  try
    QuarterMinList := TStringList.create;
    QuarterMinList.Clear;
    QuarterMinList.Delimiter := ',';
    QuarterMinList.StrictDelimiter := true;

    StatusBar1.panels[6].Text := 'Stopped';
    DeleteLastChar(methodName, ',');   //QM-569
    QuarterMinList.DelimitedText := methodName;

    if QuarterMinList[0] = 'SaveQtrMinutes' then
    begin

      cnt := QuarterMinList.Count;
      for i := 1 to cnt-1 do
      begin
        Memo1.Lines.Add('RunMethod/SaveQtrMinutes');
        qtrMinuteCode := QuarterMinList.Strings[i];
        sLat := copy(qtrMinuteCode, 0, 5);
        sLng := copy(qtrMinuteCode, 6, 12);
        if insertMode = imAdd then
          areaID := frmMain.ActiveAreaId
        else
          areaID := 0;

        sCallCenter := ActiveCallCenter;
        with FDmemQtrMinPost do
        begin
          Open;
          Append;
          FieldByName('area_id').AsInteger := areaID;
          FieldByName('qtrmin_lat').AsString := sLat;
          FieldByName('qtrmin_long').AsString := sLng;
          FieldByName('call_center').AsString := sCallCenter;
          if (FieldByName('qtrmin_lat').AsString<>'') and (FieldByName('qtrmin_long').AsString <>'') then
            Post;
          next;
          Memo1.Lines.Add('RunMethod/FDmemQtrMinPost');
        end;
      end;
      WriteToFireDac(Sender);
    end
    else if QuarterMinList[0] = 'ManageQtrGrid' then
    begin
      functionName := trim(QuarterMinList[1]);
      retState := trim(QuarterMinList[2]);
      if (functionName = FUNCTION_NAME) and (retState = VISIBLE) then
        Accept1.Enabled := true;
    end
    else if QuarterMinList[0] = 'WhoIsHere' then
    begin
      qtrMinuteCode := QuarterMinList[1];
      sLat := copy(qtrMinuteCode, 0, 5);
      sLng := copy(qtrMinuteCode, 6, 12);
      GetWhoHereList(sLat, sLng);
      Memo1.Lines.Add('QuarterMinList/GetWhoHereList');
    end
    else if QuarterMinList[0] = 'ShowMarker' then
    begin
      qtrMinuteCode := QuarterMinList[1];
      sLat := copy(qtrMinuteCode, 0, 5);
      sLng := copy(qtrMinuteCode, 6, 12);
      edtLat.Text := sLat;
      edtLng.Text := sLng;
    end
    else if QuarterMinList[0] = 'ShowQtrMinBounds' then
    begin
      QtrNorth := '';
      QtrSouth := '';
      QtrEast := '';
      QtrWest := '';
      QtrNorth := QuarterMinList[1];
      QtrSouth := QuarterMinList[2];
      QtrEast := QuarterMinList[3];
      QtrWest := QuarterMinList[4];
      viewNorth := QtrNorth;
      viewSouth := QtrSouth;
      viewEast := QtrEast;
      viewWest := QtrWest;

      GetAllInViewList(QtrNorth, QtrSouth, QtrEast, QtrWest);
    end;

  finally
    QuarterMinList.Free;

    StatusBar1.panels[0].Text := '';
    StatusBar1.panels[0].Text := IntToStr(cnt);
    StatusBar1.panels[6].Text := 'Started';
  end;
end;

procedure TfrmMain.Open1Click(Sender: TObject);
begin
  frmSavedLocations := TfrmSavedLocations.create(self);
  frmSavedLocations.ShowModal;
end;

procedure TfrmMain.tsViewModeChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
  WVBrowser1.ExecuteScript('toggleButtons()');
  case NewTab of
    0:
      begin
        ViewMode := vmMap;
        mapAreaList.BringToFront;
        MapAllinView1.Visible := false;
        MapAllforList1.Visible := true;
        GetAllforSelected1.Visible := false;
        RetriveAreasbyMap1.Visible := true;
        Accept1.Visible := true;
        RetrieveListinView1.Visible := false;
        DBNavigator1.DataSource := dsMapList;
        cbExcludeMap.Visible := false;
        DelAllQtrMinFromSelected1.Visible := true;
        DeleteFromDatabase1.Visible := true;
        GotoSelected1.Visible := true;
        clearGridClick(nil);
        GetAreasByMap(fMapID);
      end;
    1:
      begin
        ViewMode := vmView;
        viewList.BringToFront;
        MapAllinView1.Visible := true;
        MapAllforList1.Visible := false;
        GetAllforSelected1.Visible := true;
        RetrieveListinView1.Visible := true;
        Accept1.Visible := false;    //reversed for testing
        RetriveAreasbyMap1.Visible := false;
        DBNavigator1.DataSource := dsViewList;
        cbExcludeMap.Visible := true;
        DelAllQtrMinFromSelected1.Visible := false;
        DeleteFromDatabase1.Visible := false;
        GotoSelected1.Visible := false;
        clearGridClick(nil);
      end;
  end;

end;

procedure TfrmMain.clearGridClick(Sender: TObject);
begin
  WVBrowser1.ExecuteScript('ClearUserGrid()');
  FDmemViewList.Close;
  FDmemMapList.Close;
  lastUsedColor := -1;
  lastUsedViewColor := -1;
end;

procedure TfrmMain.FormActivate(Sender: TObject);
begin
  GetAreasByMap(fMapID);
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  LoadParams;
end;

procedure TfrmMain.primeColorList;
var
  myColorFile: TextFile;
  Text: string;
  myColor: TColor;

  function HtmlToColor(s: string; aDefault: TColor): TColor;
  begin
    if copy(s, 1, 1) = '#' then
    begin
      s := '$' + copy(s, 6, 2) + copy(s, 4, 2) + copy(s, 2, 2);
    end
    else
      s := 'clNone';
    try
      result := StringToColor(s);
    except
      result := aDefault;
    end;
  end;

begin
  colorList := TStringList.create;
  colorViewList := TStringList.create;
  AssignFile(myColorFile, 'colorFile.txt');
  Reset(myColorFile);
  while not eof(myColorFile) do
  begin
    ReadLn(myColorFile, Text);
    myColor := HtmlToColor(Text, clWebSienna);
    colorViewList.AddObject(Text, TObject(myColor));
    colorList.AddObject(Text, TObject(myColor));
  end;
  CloseFile(myColorFile);
end;

procedure TfrmMain.RetrieveListinView1Click(Sender: TObject);
begin
  WVBrowser1.ExecuteScript('getAllInViewOn()');
end;

procedure TfrmMain.RetriveAreasbyMap1Click(Sender: TObject);
begin
  GetAreasByMap(fMapID);
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  freeandnil(colorList);
  freeandnil(colorViewList);
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  lastUsedColor := -1;
  lastUsedViewColor := -1;
  primeColorList;
  viewList.Columns[0].Width := 34;
  mapAreaList.Columns[0].Width := 34;

  ViewMode := vmMap;
  mapAreaList.BringToFront;
  RetriveAreasbyMap1.Visible := true;
  RetrieveListinView1.Visible := false;
  GetAllforSelected1.Visible := false;
  MapAllinView1.Visible := false;
  cbExcludeMap.Visible := false;
  StatusBar1.Panels[2].text := GetFileVersion(ParamStr(0));

  if GlobalWebView2Loader.InitializationError then
    showmessage(GlobalWebView2Loader.ErrorMessage)
   else
    if GlobalWebView2Loader.Initialized then
      WVBrowser1.CreateBrowser(WVWindowParent1.Handle)
     else
      Timer1.Enabled := True;
end;

procedure TfrmMain.DrawCounties;
const
  KMZ_PATH ='County_%s.kmz';
var
  kmzPath : string;
begin    //  'https://sites.google.com/site/countykmltester/QtrGridLinesBig.kmz';
  kmzPath := format(fKmzUrlPath+KMZ_PATH,[upperCase(fActiveState)]);
  WVBrowser1.ExecuteScript(Format('CountyOn("%s")',[kmzPath]));
end;

function TfrmMain.LoadParams: Boolean;
begin
  result := false;
  if ParamCount < 3  then
  begin
    showmessage('This program can only be started from QManager');
    application.Terminate;
    exit;
  end;
  TryStrToInt(ParamStr(1), fMapID);
  fActiveEmpID    :=ParamStr(2);
  fActivePassword := ParamStr(3);
end;

procedure TfrmMain.timeOutMsgBox(sMsg, sCaption : WideString; timeOut : integer);
var
 iRet: Integer;
 iFlags: Integer;
begin
 iRet:=0;
 iFlags := MB_ICONEXCLAMATION;
 iRet   := MessageBoxTimeoutW(Application.Handle, PWideChar(sMsg),
                            PWideChar(sCaption), iFlags, 0, timeOut);
end;

procedure TfrmMain.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;

  if GlobalWebView2Loader.Initialized then
    WVBrowser1.CreateBrowser(WVWindowParent1.Handle)
   else
    Timer1.Enabled := True;
end;

function GetFileVersion(exeName : string): string;
const
  c_StringInfo = 'StringFileInfo\040904E4\FileVersion';
var
  n, Len : cardinal;
  Buf, Value : PChar;
begin
  Result := '';
  n := GetFileVersionInfoSize(PChar(exeName),n);
  if n > 0 then begin
    Buf := AllocMem(n);
    try
      GetFileVersionInfo(PChar(exeName),0,n,Buf);
      if VerQueryValue(Buf,PChar(c_StringInfo),Pointer(Value),Len) then begin
        Result := Trim(Value);
      end;
    finally
      FreeMem(Buf,n);
    end;
  end;
end;

initialization
  GlobalWebView2Loader                := TWVLoader.Create(nil);
  GlobalWebView2Loader.UserDataFolder := ExtractFileDir(Application.ExeName) + '\CustomCache';
  GlobalWebView2Loader.StartWebView2;
end.
