unit Timedmsg;

interface

uses
  SysUtils, WinTypes, WinProcs, Graphics, Forms, ExtCtrls, Controls,
  StdCtrls, Buttons, Classes;

type
  TTimedMsgDlgType = (tmtWarning, tmtError, tmtInformation);

type
  TfMsgDlg = class(TForm)
    Button: TBitBtn;
    Glyph: TImage;
    Timer: TTimer;
    MsgLabel: TLabel;
    SexLabel: TLabel;
    procedure FormShow(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
    Seconds : Word;
    Msg : String;
    AType : TTimedMsgDlgType;
  end;

procedure TimedMessageDlg(const M : string; A : TTimedMsgDlgType; S : Word);

implementation

{$R *.DFM}

uses Consts;

procedure TimedMessageDlg(const M : string; A : TTimedMsgDlgType; S : Word);
var
  MsgDlg : TfMsgDlg;
begin
  try
    MsgDlg:=TfMsgDlg.Create (Application);
    MsgDlg.Msg:=M;
    MsgDlg.AType:=A;
    MsgDlg.Seconds:=S;
    MsgDlg.ShowModal;
  finally
    MsgDlg.Free;
  end;
end;

procedure TfMsgDlg.FormShow(Sender: TObject);
var
  Captions: array of string = ( 'Warning',  'Error',  'Information');
  ResIDs: array[TTimedMsgDlgType] of PWideChar = (IDI_EXCLAMATION, IDI_HAND, IDI_ASTERISK);

  FIcon: TIcon;
  TextRect: TRect;
  C: array[0..255] of Char;

  function Max(v1, v2: Integer): Integer;
  begin
    if v2 > v1 then Result := v2 else Result := v1;
  end;

begin
  { load glyph }
  try
    FIcon:=TIcon.Create;
    FIcon.Handle:=LoadIcon (0, ResIDs[AType]);
    Glyph.Picture.Graphic:=FIcon;
    Glyph.BoundsRect:=Bounds (10, 0, FIcon.Width, FIcon.Height);
  finally
    FIcon.Free;
  end;
  { setup message }
  TextRect:=Rect (0, 0, Screen.Width div 2, 0);
  DrawText (Canvas.Handle, StrPCopy(C, Msg), -1, TextRect, DT_CALCRECT or DT_WORDBREAK);
  MsgLabel.Caption:=Msg;
  MsgLabel.BoundsRect:=TextRect;
  { adjust tops of everything }
  if (Glyph.Height>(TextRect.Bottom-TextRect.Top)) then
  begin
    Glyph.Top:=10;
    MsgLabel.Top:= Glyph.Top+(Glyph.Picture.Graphic.Height div 2)-((TextRect.Bottom-TextRect.Top) div 2);
    Button.Top:=Glyph.Top+Glyph.Height+10;
  end
  else
  begin
    MsgLabel.Top:=10;
    Glyph.Top:=MsgLabel.Top+(((TextRect.Bottom-TextRect.Top) div 2)-(Glyph.Height div 2));
    Button.Top:=MsgLabel.Top+MsgLabel.Height+10;
  end;
  { configure form }
  ClientWidth:=Max (ClientWidth, (TextRect.Right-TextRect.Left)+Glyph.Picture.Graphic.Width+30);
  ClientHeight:=Max (ClientHeight, Button.Top+Button.Height+8);
  Caption:=LoadStr (Captions[AType]);
  { adjust lefts of everything }
  MsgLabel.Left:= Glyph.Left + Glyph.Width+10;
  Button.Left:=(ClientWidth-Button.Width) div 2;
  { position form }
  Left:=(Screen.Width div 2)-(Width div 2);
  Top:=(Screen.Height div 2)-(Height div 2);
  { if required setup timer }
  if Seconds>0 then
  begin
    SexLabel.Top:=ClientHeight-SexLabel.Height-10;
    SexLabel.Left:=ClientWidth-SexLabel.Width-10;
    SexLabel.Caption:=IntToStr (Seconds);
    SexLabel.Visible:=True;
    Timer.Enabled:=True;
  end;
end;

procedure TfMsgDlg.TimerTimer(Sender: TObject);
begin
  Dec (Seconds);
  SexLabel.Caption:=IntToStr (Seconds);
  if Seconds=0 then ModalResult:=mrOK;
end;

end.
