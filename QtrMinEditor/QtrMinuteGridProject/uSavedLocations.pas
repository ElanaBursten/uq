unit uSavedLocations;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,   Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Data.DB, Vcl.StdCtrls, Vcl.Buttons, Vcl.Grids, Vcl.DBGrids, Datasnap.DBClient,
  Vcl.ExtCtrls, Vcl.DBCtrls;

type
  TfrmSavedLocations = class(TForm)
    Panel1: TPanel;
    savedLocations: TClientDataSet;
    dsSavedLocations: TDataSource;
    dbGridSaveLocations: TDBGrid;
    BitBtn1: TBitBtn;
    DBNavigator1: TDBNavigator;
    procedure BitBtn1Click(Sender: TObject);
    procedure dbGridSaveLocationsDblClick(Sender: TObject);
    procedure savedLocationsBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    saved : boolean;
    sLat, sLng : string;
    function InsertLocation: Boolean;
  public
    { Public declarations }
    xmlPath : string;
    constructor create(aOwner : TComponent); overload;
    constructor create(aOwner : TComponent; Lat,Lng :string);overload;
  end;

var
  frmSavedLocations: TfrmSavedLocations;
const
  XML_REL_PATH = 'HTML\savedLocations.xml';
implementation

{$R *.dfm}

uses uMainForm;

{ TfrmSavedLocations }

constructor TfrmSavedLocations.create(aOwner : TComponent; Lat, Lng: string);
begin
  inherited create(aOwner);
  xmlPath := ExtractFilePath( Application.ExeName )  + XML_REL_PATH;
  savedLocations.FileName := xmlPath;
  sLat := Lat;
  sLng := Lng;
  saved := true;
  InsertLocation;
end;

procedure TfrmSavedLocations.BitBtn1Click(Sender: TObject);
begin
  savedLocations.Post;
end;

constructor TfrmSavedLocations.create(aOwner : TComponent);
begin
  inherited create(aOwner);
  xmlPath := ExtractFilePath( Application.ExeName )  + XML_REL_PATH;
  savedLocations.FileName := xmlPath;
  saved := true;
  savedLocations.Open;
end;

procedure TfrmSavedLocations.dbGridSaveLocationsDblClick(Sender: TObject);
begin
  frmMain.edtLat.Text := savedLocations.FieldByName('savedLocLat').AsString;
  frmMain.edtLng.Text := savedLocations.FieldByName('savedLocLng').AsString;
  frmMain.btnGotoClick(nil);
end;

procedure TfrmSavedLocations.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  savedLocations.Close;
  Action := caFree;
end;

procedure TfrmSavedLocations.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if dsSavedLocations.State in [dsEdit, dsInsert] then
  savedLocations.Post;
  canclose := saved;
end;

function TfrmSavedLocations.InsertLocation:Boolean;
begin
  savedLocations.Open;
  savedLocations.Append;
  savedLocations.FieldByName('savedLocLat').AsString := sLat;
  savedLocations.FieldByName('savedLocLng').AsString := sLng;
end;


procedure TfrmSavedLocations.savedLocationsBeforePost(DataSet: TDataSet);
begin
   saved := true;
   if savedLocations.FieldByName('savedLocName').AsString = '' then
   begin
     showMessage('You must provide a name for this location');
     savedLocations.Cancel;
     saved := false;
     exit;
   end;
end;

end.
