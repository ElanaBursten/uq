object dlgAreaSelect: TdlgAreaSelect
  Left = 0
  Top = 0
  ActiveControl = rgAreaSelect
  BorderStyle = bsDialog
  Caption = 'Area to Select'
  ClientHeight = 158
  ClientWidth = 141
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -8
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 120
  TextHeight = 10
  object rgAreaSelect: TRadioGroup
    Left = 0
    Top = 41
    Width = 141
    Height = 117
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alClient
    Caption = '          sub area to draw    '
    Columns = 2
    Items.Strings = (
      'North'
      'South'
      'North-East'
      'South-East'
      'East'
      'West'
      'North-West'
      'South-West')
    TabOrder = 0
    OnClick = rgAreaSelectClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 141
    Height = 41
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 15
      Top = 14
      Width = 37
      Height = 10
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'How many'
    end
    object edtCntSel: TEdit
      Left = 58
      Top = 13
      Width = 36
      Height = 19
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      TabOrder = 0
      Text = '400'
    end
  end
end
