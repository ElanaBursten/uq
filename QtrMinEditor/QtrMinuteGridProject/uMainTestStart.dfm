object frmTestMsg: TfrmTestMsg
  Left = 0
  Top = 0
  Caption = 'frmTestMsg'
  ClientHeight = 141
  ClientWidth = 247
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 112
    Top = 45
    Width = 34
    Height = 13
    Caption = 'Map ID'
  end
  object edtMapID: TEdit
    Left = 80
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object btnSendMapID: TButton
    Left = 96
    Top = 91
    Width = 75
    Height = 25
    Caption = 'Send Map ID'
    TabOrder = 1
    OnClick = btnSendMapIDClick
  end
end
