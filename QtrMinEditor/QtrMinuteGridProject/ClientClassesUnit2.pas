//
// Created by the DataSnap proxy generator.
// 6/10/2016 1:24:18 PM
//

unit ClientClassesUnit2;

interface

uses System.JSON, dialogs, Datasnap.DSProxyRest, Datasnap.DSClientRest,
Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON,
Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr,
Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TQtrMinClient = class(TDSAdminRestClient)
  private
    FDELAreaByIdCommand: TDSRestCommand;
    FDELAreaByIdCommand_Cache: TDSRestCommand;
    FGETAreasByMapCommand: TDSRestCommand;
    FGETAreasByMapCommand_Cache: TDSRestCommand;
    FGETQtrMinutesInViewCommand: TDSRestCommand;
    FGETQtrMinutesInViewCommand_Cache: TDSRestCommand;
    FGETQtrMinutesCommand: TDSRestCommand;
    FGETQtrMinutesCommand_Cache: TDSRestCommand;
    FGETViewListCommand: TDSRestCommand;
    FGETViewListCommand_Cache: TDSRestCommand;
    FGETQtrMinuteCountCommand: TDSRestCommand;
    FGETQtrMinuteCountCommand_Cache: TDSRestCommand;
    FGETWhoIsHereCommand: TDSRestCommand;
    FGETWhoIsHereCommand_Cache: TDSRestCommand;
    FAddCallCenterQtrMinutesCommand: TDSRestCommand;
    FAddCallCenterQtrMinutesCommand_Cache: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function DELAreaById(login: string; password: string; areaID: Integer; const ARequestFilter: string = ''): TJSONObject;
    function DELAreaById_Cache(login: string; password: string; areaID: Integer; const ARequestFilter: string = ''): IDSRestCachedJSONObject;
    function GETAreasByMap(login: string; password: string; mapID: Integer; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GETAreasByMap_Cache(login: string; password: string; mapID: Integer; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GETQtrMinutesInView(login: string; password: string; areaID: Integer; callCenter: string; North: string; South: string; East: string; West: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GETQtrMinutesInView_Cache(login: string; password: string; areaID: Integer; callCenter: string; North: string; South: string; East: string; West: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GETQtrMinutes(login: string; password: string; areaID: Integer; callCenter: string; optSelected: Integer; cntSelected: Integer; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GETQtrMinutes_Cache(login: string; password: string; areaID: Integer; callCenter: string; optSelected: Integer; cntSelected: Integer; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GETViewList(login: string; password: string; North: string; South: string; East: string; West: string; mapID: Integer; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GETViewList_Cache(login: string; password: string; North: string; South: string; East: string; West: string; mapID: Integer; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GETQtrMinuteCount(login: string; password: string; areaID: Integer; callCenter: string; const ARequestFilter: string = ''): TJSONObject;
    function GETQtrMinuteCount_Cache(login: string; password: string; areaID: Integer; callCenter: string; const ARequestFilter: string = ''): IDSRestCachedJSONObject;
    function GETWhoIsHere(login: string; password: string; QtrMinLat: string; QtrMinLng: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GETWhoIsHere_Cache(login: string; password: string; QtrMinLat: string; QtrMinLng: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function AddCallCenterQtrMinutes(login: string; password: string; newQtrMinutes: TFDJSONDataSets; const ARequestFilter: string = ''): TJSONObject;
    function AddCallCenterQtrMinutes_Cache(login: string; password: string; newQtrMinutes: TFDJSONDataSets; const ARequestFilter: string = ''): IDSRestCachedJSONObject;
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>, IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TQtrMin_DELAreaById: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TJSONObject')
  );

  TQtrMin_DELAreaById_Cache: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TQtrMin_GETAreasByMap: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'mapID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TQtrMin_GETAreasByMap_Cache: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'mapID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TQtrMin_GETQtrMinutesInView: array [0..8] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'callCenter'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'North'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'South'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'East'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'West'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TQtrMin_GETQtrMinutesInView_Cache: array [0..8] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'callCenter'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'North'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'South'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'East'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'West'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TQtrMin_GETQtrMinutes: array [0..6] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'callCenter'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'optSelected'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'cntSelected'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TQtrMin_GETQtrMinutes_Cache: array [0..6] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'callCenter'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'optSelected'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'cntSelected'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TQtrMin_GETViewList: array [0..7] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'North'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'South'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'East'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'West'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'mapID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TQtrMin_GETViewList_Cache: array [0..7] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'North'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'South'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'East'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'West'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'mapID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TQtrMin_GETQtrMinuteCount: array [0..4] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'callCenter'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TJSONObject')
  );

  TQtrMin_GETQtrMinuteCount_Cache: array [0..4] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'areaID'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'callCenter'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TQtrMin_GETWhoIsHere: array [0..4] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'QtrMinLat'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'QtrMinLng'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TQtrMin_GETWhoIsHere_Cache: array [0..4] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'QtrMinLat'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'QtrMinLng'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TQtrMin_AddCallCenterQtrMinutes: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'newQtrMinutes'; Direction: 1; DBXType: 37; TypeName: 'TFDJSONDataSets'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TJSONObject')
  );

  TQtrMin_AddCallCenterQtrMinutes_Cache: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'login'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'password'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'newQtrMinutes'; Direction: 1; DBXType: 37; TypeName: 'TFDJSONDataSets'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

implementation

function TQtrMinClient.DELAreaById(login: string; password: string; areaID: Integer; const ARequestFilter: string): TJSONObject;
begin
  if FDELAreaByIdCommand = nil then
  begin
    FDELAreaByIdCommand := FConnection.CreateCommand;
    FDELAreaByIdCommand.RequestType := 'GET';
    FDELAreaByIdCommand.Text := 'TQtrMin.DELAreaById';
    FDELAreaByIdCommand.Prepare(TQtrMin_DELAreaById);
  end;
  FDELAreaByIdCommand.Parameters[0].Value.SetWideString(login);
  FDELAreaByIdCommand.Parameters[1].Value.SetWideString(password);
  FDELAreaByIdCommand.Parameters[2].Value.SetInt32(areaID);
  FDELAreaByIdCommand.Execute(ARequestFilter);
  Result := TJSONObject(FDELAreaByIdCommand.Parameters[3].Value.GetJSONValue(FInstanceOwner));
end;

function TQtrMinClient.DELAreaById_Cache(login: string; password: string; areaID: Integer; const ARequestFilter: string): IDSRestCachedJSONObject;
begin
  if FDELAreaByIdCommand_Cache = nil then
  begin
    FDELAreaByIdCommand_Cache := FConnection.CreateCommand;
    FDELAreaByIdCommand_Cache.RequestType := 'GET';
    FDELAreaByIdCommand_Cache.Text := 'TQtrMin.DELAreaById';
    FDELAreaByIdCommand_Cache.Prepare(TQtrMin_DELAreaById_Cache);
  end;
  FDELAreaByIdCommand_Cache.Parameters[0].Value.SetWideString(login);
  FDELAreaByIdCommand_Cache.Parameters[1].Value.SetWideString(password);
  FDELAreaByIdCommand_Cache.Parameters[2].Value.SetInt32(areaID);
  FDELAreaByIdCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedJSONObject.Create(FDELAreaByIdCommand_Cache.Parameters[3].Value.GetString);
end;

function TQtrMinClient.GETAreasByMap(login: string; password: string; mapID: Integer; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGETAreasByMapCommand = nil then
  begin
    FGETAreasByMapCommand := FConnection.CreateCommand;
    FGETAreasByMapCommand.RequestType := 'GET';
    FGETAreasByMapCommand.Text := 'TQtrMin.GETAreasByMap';
    FGETAreasByMapCommand.Prepare(TQtrMin_GETAreasByMap);
  end;
  FGETAreasByMapCommand.Parameters[0].Value.SetWideString(login);
  FGETAreasByMapCommand.Parameters[1].Value.SetWideString(password);
  FGETAreasByMapCommand.Parameters[2].Value.SetInt32(mapID);
  FGETAreasByMapCommand.Execute(ARequestFilter);
  if not FGETAreasByMapCommand.Parameters[3].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGETAreasByMapCommand.Parameters[3].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGETAreasByMapCommand.Parameters[3].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGETAreasByMapCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TQtrMinClient.GETAreasByMap_Cache(login: string; password: string; mapID: Integer; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGETAreasByMapCommand_Cache = nil then
  begin
    FGETAreasByMapCommand_Cache := FConnection.CreateCommand;
    FGETAreasByMapCommand_Cache.RequestType := 'GET';
    FGETAreasByMapCommand_Cache.Text := 'TQtrMin.GETAreasByMap';
    FGETAreasByMapCommand_Cache.Prepare(TQtrMin_GETAreasByMap_Cache);
  end;
  FGETAreasByMapCommand_Cache.Parameters[0].Value.SetWideString(login);
  FGETAreasByMapCommand_Cache.Parameters[1].Value.SetWideString(password);
  FGETAreasByMapCommand_Cache.Parameters[2].Value.SetInt32(mapID);
  FGETAreasByMapCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGETAreasByMapCommand_Cache.Parameters[3].Value.GetString);
end;

function TQtrMinClient.GETQtrMinutesInView(login: string; password: string; areaID: Integer; callCenter: string; North: string; South: string; East: string; West: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGETQtrMinutesInViewCommand = nil then
  begin
    FGETQtrMinutesInViewCommand := FConnection.CreateCommand;
    FGETQtrMinutesInViewCommand.RequestType := 'GET';
    FGETQtrMinutesInViewCommand.Text := 'TQtrMin.GETQtrMinutesInView';
    FGETQtrMinutesInViewCommand.Prepare(TQtrMin_GETQtrMinutesInView);
  end;
  FGETQtrMinutesInViewCommand.Parameters[0].Value.SetWideString(login);
  FGETQtrMinutesInViewCommand.Parameters[1].Value.SetWideString(password);
  FGETQtrMinutesInViewCommand.Parameters[2].Value.SetInt32(areaID);
  FGETQtrMinutesInViewCommand.Parameters[3].Value.SetWideString(callCenter);
  FGETQtrMinutesInViewCommand.Parameters[4].Value.SetWideString(North);
  FGETQtrMinutesInViewCommand.Parameters[5].Value.SetWideString(South);
  FGETQtrMinutesInViewCommand.Parameters[6].Value.SetWideString(East);
  FGETQtrMinutesInViewCommand.Parameters[7].Value.SetWideString(West);
  FGETQtrMinutesInViewCommand.Execute(ARequestFilter);
  if not FGETQtrMinutesInViewCommand.Parameters[8].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGETQtrMinutesInViewCommand.Parameters[8].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGETQtrMinutesInViewCommand.Parameters[8].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGETQtrMinutesInViewCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TQtrMinClient.GETQtrMinutesInView_Cache(login: string; password: string; areaID: Integer; callCenter: string; North: string; South: string; East: string; West: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGETQtrMinutesInViewCommand_Cache = nil then
  begin
    FGETQtrMinutesInViewCommand_Cache := FConnection.CreateCommand;
    FGETQtrMinutesInViewCommand_Cache.RequestType := 'GET';
    FGETQtrMinutesInViewCommand_Cache.Text := 'TQtrMin.GETQtrMinutesInView';
    FGETQtrMinutesInViewCommand_Cache.Prepare(TQtrMin_GETQtrMinutesInView_Cache);
  end;
  FGETQtrMinutesInViewCommand_Cache.Parameters[0].Value.SetWideString(login);
  FGETQtrMinutesInViewCommand_Cache.Parameters[1].Value.SetWideString(password);
  FGETQtrMinutesInViewCommand_Cache.Parameters[2].Value.SetInt32(areaID);
  FGETQtrMinutesInViewCommand_Cache.Parameters[3].Value.SetWideString(callCenter);
  FGETQtrMinutesInViewCommand_Cache.Parameters[4].Value.SetWideString(North);
  FGETQtrMinutesInViewCommand_Cache.Parameters[5].Value.SetWideString(South);
  FGETQtrMinutesInViewCommand_Cache.Parameters[6].Value.SetWideString(East);
  FGETQtrMinutesInViewCommand_Cache.Parameters[7].Value.SetWideString(West);
  FGETQtrMinutesInViewCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGETQtrMinutesInViewCommand_Cache.Parameters[8].Value.GetString);
end;

function TQtrMinClient.GETQtrMinutes(login: string; password: string; areaID: Integer; callCenter: string; optSelected: Integer; cntSelected: Integer; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGETQtrMinutesCommand = nil then
  begin
    FGETQtrMinutesCommand := FConnection.CreateCommand;
    FGETQtrMinutesCommand.RequestType := 'GET';
    FGETQtrMinutesCommand.Text := 'TQtrMin.GETQtrMinutes';
    FGETQtrMinutesCommand.Prepare(TQtrMin_GETQtrMinutes);
  end;
  FGETQtrMinutesCommand.Parameters[0].Value.SetWideString(login);
  FGETQtrMinutesCommand.Parameters[1].Value.SetWideString(password);
  FGETQtrMinutesCommand.Parameters[2].Value.SetInt32(areaID);
  FGETQtrMinutesCommand.Parameters[3].Value.SetWideString(callCenter);
  FGETQtrMinutesCommand.Parameters[4].Value.SetInt32(optSelected);
  FGETQtrMinutesCommand.Parameters[5].Value.SetInt32(cntSelected);
  FGETQtrMinutesCommand.Execute(ARequestFilter);
  if not FGETQtrMinutesCommand.Parameters[6].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGETQtrMinutesCommand.Parameters[6].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGETQtrMinutesCommand.Parameters[6].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGETQtrMinutesCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TQtrMinClient.GETQtrMinutes_Cache(login: string; password: string; areaID: Integer; callCenter: string; optSelected: Integer; cntSelected: Integer; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGETQtrMinutesCommand_Cache = nil then
  begin
    FGETQtrMinutesCommand_Cache := FConnection.CreateCommand;
    FGETQtrMinutesCommand_Cache.RequestType := 'GET';
    FGETQtrMinutesCommand_Cache.Text := 'TQtrMin.GETQtrMinutes';
    FGETQtrMinutesCommand_Cache.Prepare(TQtrMin_GETQtrMinutes_Cache);
  end;
  FGETQtrMinutesCommand_Cache.Parameters[0].Value.SetWideString(login);
  FGETQtrMinutesCommand_Cache.Parameters[1].Value.SetWideString(password);
  FGETQtrMinutesCommand_Cache.Parameters[2].Value.SetInt32(areaID);
  FGETQtrMinutesCommand_Cache.Parameters[3].Value.SetWideString(callCenter);
  FGETQtrMinutesCommand_Cache.Parameters[4].Value.SetInt32(optSelected);
  FGETQtrMinutesCommand_Cache.Parameters[5].Value.SetInt32(cntSelected);
  FGETQtrMinutesCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGETQtrMinutesCommand_Cache.Parameters[6].Value.GetString);
end;

function TQtrMinClient.GETViewList(login: string; password: string; North: string; South: string; East: string; West: string; mapID: Integer; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGETViewListCommand = nil then
  begin
    FGETViewListCommand := FConnection.CreateCommand;
    FGETViewListCommand.RequestType := 'GET';
    FGETViewListCommand.Text := 'TQtrMin.GETViewList';
    FGETViewListCommand.Prepare(TQtrMin_GETViewList);
  end;
  FGETViewListCommand.Parameters[0].Value.SetWideString(login);
  FGETViewListCommand.Parameters[1].Value.SetWideString(password);
  FGETViewListCommand.Parameters[2].Value.SetWideString(North);
  FGETViewListCommand.Parameters[3].Value.SetWideString(South);
  FGETViewListCommand.Parameters[4].Value.SetWideString(East);
  FGETViewListCommand.Parameters[5].Value.SetWideString(West);
  FGETViewListCommand.Parameters[6].Value.SetInt32(mapID);
  FGETViewListCommand.Execute(ARequestFilter);
  if not FGETViewListCommand.Parameters[7].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGETViewListCommand.Parameters[7].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGETViewListCommand.Parameters[7].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGETViewListCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TQtrMinClient.GETViewList_Cache(login: string; password: string; North: string; South: string; East: string; West: string; mapID: Integer; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGETViewListCommand_Cache = nil then
  begin
    FGETViewListCommand_Cache := FConnection.CreateCommand;
    FGETViewListCommand_Cache.RequestType := 'GET';
    FGETViewListCommand_Cache.Text := 'TQtrMin.GETViewList';
    FGETViewListCommand_Cache.Prepare(TQtrMin_GETViewList_Cache);
  end;
  FGETViewListCommand_Cache.Parameters[0].Value.SetWideString(login);
  FGETViewListCommand_Cache.Parameters[1].Value.SetWideString(password);
  FGETViewListCommand_Cache.Parameters[2].Value.SetWideString(North);
  FGETViewListCommand_Cache.Parameters[3].Value.SetWideString(South);
  FGETViewListCommand_Cache.Parameters[4].Value.SetWideString(East);
  FGETViewListCommand_Cache.Parameters[5].Value.SetWideString(West);
  FGETViewListCommand_Cache.Parameters[6].Value.SetInt32(mapID);
  FGETViewListCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGETViewListCommand_Cache.Parameters[7].Value.GetString);
end;

function TQtrMinClient.GETQtrMinuteCount(login: string; password: string; areaID: Integer; callCenter: string; const ARequestFilter: string): TJSONObject;
begin
  if FGETQtrMinuteCountCommand = nil then
  begin
    FGETQtrMinuteCountCommand := FConnection.CreateCommand;
    FGETQtrMinuteCountCommand.RequestType := 'GET';
    FGETQtrMinuteCountCommand.Text := 'TQtrMin.GETQtrMinuteCount';
    FGETQtrMinuteCountCommand.Prepare(TQtrMin_GETQtrMinuteCount);
  end;
  FGETQtrMinuteCountCommand.Parameters[0].Value.SetWideString(login);
  FGETQtrMinuteCountCommand.Parameters[1].Value.SetWideString(password);
  FGETQtrMinuteCountCommand.Parameters[2].Value.SetInt32(areaID);
  FGETQtrMinuteCountCommand.Parameters[3].Value.SetWideString(callCenter);
  FGETQtrMinuteCountCommand.Execute(ARequestFilter);
  Result := TJSONObject(FGETQtrMinuteCountCommand.Parameters[4].Value.GetJSONValue(FInstanceOwner));
end;

function TQtrMinClient.GETQtrMinuteCount_Cache(login: string; password: string; areaID: Integer; callCenter: string; const ARequestFilter: string): IDSRestCachedJSONObject;
begin
  if FGETQtrMinuteCountCommand_Cache = nil then
  begin
    FGETQtrMinuteCountCommand_Cache := FConnection.CreateCommand;
    FGETQtrMinuteCountCommand_Cache.RequestType := 'GET';
    FGETQtrMinuteCountCommand_Cache.Text := 'TQtrMin.GETQtrMinuteCount';
    FGETQtrMinuteCountCommand_Cache.Prepare(TQtrMin_GETQtrMinuteCount_Cache);
  end;
  FGETQtrMinuteCountCommand_Cache.Parameters[0].Value.SetWideString(login);
  FGETQtrMinuteCountCommand_Cache.Parameters[1].Value.SetWideString(password);
  FGETQtrMinuteCountCommand_Cache.Parameters[2].Value.SetInt32(areaID);
  FGETQtrMinuteCountCommand_Cache.Parameters[3].Value.SetWideString(callCenter);
  FGETQtrMinuteCountCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedJSONObject.Create(FGETQtrMinuteCountCommand_Cache.Parameters[4].Value.GetString);
end;

function TQtrMinClient.GETWhoIsHere(login: string; password: string; QtrMinLat: string; QtrMinLng: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGETWhoIsHereCommand = nil then
  begin
    FGETWhoIsHereCommand := FConnection.CreateCommand;
    FGETWhoIsHereCommand.RequestType := 'GET';
    FGETWhoIsHereCommand.Text := 'TQtrMin.GETWhoIsHere';
    FGETWhoIsHereCommand.Prepare(TQtrMin_GETWhoIsHere);
  end;
  FGETWhoIsHereCommand.Parameters[0].Value.SetWideString(login);
  FGETWhoIsHereCommand.Parameters[1].Value.SetWideString(password);
  FGETWhoIsHereCommand.Parameters[2].Value.SetWideString(QtrMinLat);
  FGETWhoIsHereCommand.Parameters[3].Value.SetWideString(QtrMinLng);
  FGETWhoIsHereCommand.Execute(ARequestFilter);
  if not FGETWhoIsHereCommand.Parameters[4].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGETWhoIsHereCommand.Parameters[4].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGETWhoIsHereCommand.Parameters[4].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGETWhoIsHereCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TQtrMinClient.GETWhoIsHere_Cache(login: string; password: string; QtrMinLat: string; QtrMinLng: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGETWhoIsHereCommand_Cache = nil then
  begin
    FGETWhoIsHereCommand_Cache := FConnection.CreateCommand;
    FGETWhoIsHereCommand_Cache.RequestType := 'GET';
    FGETWhoIsHereCommand_Cache.Text := 'TQtrMin.GETWhoIsHere';
    FGETWhoIsHereCommand_Cache.Prepare(TQtrMin_GETWhoIsHere_Cache);
  end;
  FGETWhoIsHereCommand_Cache.Parameters[0].Value.SetWideString(login);
  FGETWhoIsHereCommand_Cache.Parameters[1].Value.SetWideString(password);
  FGETWhoIsHereCommand_Cache.Parameters[2].Value.SetWideString(QtrMinLat);
  FGETWhoIsHereCommand_Cache.Parameters[3].Value.SetWideString(QtrMinLng);
  FGETWhoIsHereCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGETWhoIsHereCommand_Cache.Parameters[4].Value.GetString);
end;

function TQtrMinClient.AddCallCenterQtrMinutes(login: string; password: string; newQtrMinutes: TFDJSONDataSets; const ARequestFilter: string): TJSONObject;
begin
try
    if FAddCallCenterQtrMinutesCommand = nil then
    begin
      FAddCallCenterQtrMinutesCommand := FConnection.CreateCommand;
      FAddCallCenterQtrMinutesCommand.RequestType := 'POST';
      FAddCallCenterQtrMinutesCommand.Text := 'TQtrMin."AddCallCenterQtrMinutes"';
      FAddCallCenterQtrMinutesCommand.Prepare(TQtrMin_AddCallCenterQtrMinutes);
    end;
    FAddCallCenterQtrMinutesCommand.Parameters[0].Value.SetWideString(login);
    FAddCallCenterQtrMinutesCommand.Parameters[1].Value.SetWideString(password);
    if not Assigned(newQtrMinutes) then
      FAddCallCenterQtrMinutesCommand.Parameters[2].Value.SetNull
    else
    begin
      FMarshal := TDSRestCommand(FAddCallCenterQtrMinutesCommand.Parameters[2].ConnectionHandler).GetJSONMarshaler;
      try
        FAddCallCenterQtrMinutesCommand.Parameters[2].Value.SetJSONValue(FMarshal.Marshal(newQtrMinutes), True);
        if FInstanceOwner then
          newQtrMinutes.Free
      finally
        FreeAndNil(FMarshal)
      end
      end;
//    FAddCallCenterQtrMinutesCommand.Connection.ConnectTimeout := 30000;
    FAddCallCenterQtrMinutesCommand.Execute(ARequestFilter);
    Result := TJSONObject(FAddCallCenterQtrMinutesCommand.Parameters[3].Value.GetJSONValue(FInstanceOwner));
except on E: Exception do
showmessage(E.message);
end;
end;

function TQtrMinClient.AddCallCenterQtrMinutes_Cache(login: string; password: string; newQtrMinutes: TFDJSONDataSets; const ARequestFilter: string): IDSRestCachedJSONObject;
begin
  if FAddCallCenterQtrMinutesCommand_Cache = nil then
  begin
    FAddCallCenterQtrMinutesCommand_Cache := FConnection.CreateCommand;
    FAddCallCenterQtrMinutesCommand_Cache.RequestType := 'POST';
    FAddCallCenterQtrMinutesCommand_Cache.Text := 'TQtrMin."AddCallCenterQtrMinutes"';
    FAddCallCenterQtrMinutesCommand_Cache.Prepare(TQtrMin_AddCallCenterQtrMinutes_Cache);
  end;
  FAddCallCenterQtrMinutesCommand_Cache.Parameters[0].Value.SetWideString(login);
  FAddCallCenterQtrMinutesCommand_Cache.Parameters[1].Value.SetWideString(password);
  if not Assigned(newQtrMinutes) then
    FAddCallCenterQtrMinutesCommand_Cache.Parameters[2].Value.SetNull
  else
  begin
    FMarshal := TDSRestCommand(FAddCallCenterQtrMinutesCommand_Cache.Parameters[2].ConnectionHandler).GetJSONMarshaler;
    try
      FAddCallCenterQtrMinutesCommand_Cache.Parameters[2].Value.SetJSONValue(FMarshal.Marshal(newQtrMinutes), True);
      if FInstanceOwner then
        newQtrMinutes.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FAddCallCenterQtrMinutesCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedJSONObject.Create(FAddCallCenterQtrMinutesCommand_Cache.Parameters[3].Value.GetString);
end;

constructor TQtrMinClient.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TQtrMinClient.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TQtrMinClient.Destroy;
begin
  FDELAreaByIdCommand.DisposeOf;
  FDELAreaByIdCommand_Cache.DisposeOf;
  FGETAreasByMapCommand.DisposeOf;
  FGETAreasByMapCommand_Cache.DisposeOf;
  FGETQtrMinutesInViewCommand.DisposeOf;
  FGETQtrMinutesInViewCommand_Cache.DisposeOf;
  FGETQtrMinutesCommand.DisposeOf;
  FGETQtrMinutesCommand_Cache.DisposeOf;
  FGETViewListCommand.DisposeOf;
  FGETViewListCommand_Cache.DisposeOf;
  FGETQtrMinuteCountCommand.DisposeOf;
  FGETQtrMinuteCountCommand_Cache.DisposeOf;
  FGETWhoIsHereCommand.DisposeOf;
  FGETWhoIsHereCommand_Cache.DisposeOf;
  FAddCallCenterQtrMinutesCommand.DisposeOf;
  FAddCallCenterQtrMinutesCommand_Cache.DisposeOf;
  inherited;
end;

end.

