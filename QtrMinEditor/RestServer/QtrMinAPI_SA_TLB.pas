unit QtrMinAPI_SA_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 12/2/2015 11:40:56 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\SoftwareProjects\QtrMinClientServer\RestServer\QtrMinAPI_SA (1)
// LIBID: {399C4881-C640-4CC5-98A6-AFC62A3234A1}
// LCID: 0
// Helpfile:
// HelpString:
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Winapi.ActiveX;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  QtrMinAPI_SAMajorVersion = 1;
  QtrMinAPI_SAMinorVersion = 0;

  LIBID_QtrMinAPI_SA: TGUID = '{399C4881-C640-4CC5-98A6-AFC62A3234A1}';


implementation

uses System.Win.ComObj;

end.

