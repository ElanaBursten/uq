unit WebModuleUnit1;

interface

uses
  System.SysUtils, System.Classes, Web.HTTPApp, Datasnap.DSHTTPCommon,
  Datasnap.DSHTTPWebBroker, Datasnap.DSServer,
  Web.WebFileDispatcher, Web.HTTPProd,
  Datasnap.DSAuth,
  Datasnap.DSProxyJavaScript, IPPeerServer, Datasnap.DSMetadata,
  Datasnap.DSServerMetadata, Datasnap.DSClientMetadata, Datasnap.DSCommonServer,
  Datasnap.DSHTTP;

type
  TWebModule1 = class(TWebModule)
    DSHTTPWebDispatcher1: TDSHTTPWebDispatcher;
    DSServer1: TDSServer;
    DSServerClass1: TDSServerClass;
    ServerFunctionInvoker: TPageProducer;
    WebFileDispatcher1: TWebFileDispatcher;
    DSProxyGenerator1: TDSProxyGenerator;
    DSServerMetaDataProvider1: TDSServerMetaDataProvider;
    procedure DSServerClass1GetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);

    procedure WebModuleDefaultAction(Sender: TObject; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);

  private

    { Private declarations }

  public
    { Public declarations }


  end;

var
  WebModuleClass: TComponentClass = TWebModule1;

implementation

{$R *.dfm}

uses QtrMinU, Web.WebReq;
{
Server Class LifeCycleFrom RAD Studio

Server Class LifeCycleThe TDSServerClass metaclass has a LifeCycle property
  that determines the situations where new instances of server methods class
  are created, and when existing instances are reused or destroyed.
LifeCycle Property TypesThe options for this property are Server, Session,
  and Invocation. In general, they can be interpreted as:

Server: one instance is created per running server (singleton).
Session: one instance is created per active client connection.
Invocation: a new instance is created for each invocation from a client (stateless).
REST Clients
  For a REST client connection, if Session LifeCycle is used
  on the server class, it behaves like Invocation LifeCycle.

Connection LossIf a client abruptly loses connection (non-REST connections),
  then, by default, the OnDisconnect event is not fired. This is because the
  socket remains open until an I/O operation is attempted and fails.
  If the LifeCycle property is Session, then the server class instance will
  be dangling, and only be freed when the server is stopped. If the LifeCycle
  property is set to Invocation and the connection was lost during an invocation
  (before the response was sent to the client), then the server class will,
  again, not be destroyed until the server is stopped.


However, if your OS is configured to use keep-alive packets for all TCP/IP
  connections, based on its configuration, you can control this behavior
  using the EnableKeepAlive and DisableKeepAlive methods. For more details,
  go to Monitoring and Controlling DataSnap TCP/IP Connections - Detecting
  Graceless Disconnects section.
  }

procedure TWebModule1.DSServerClass1GetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := QtrMinU.TQtrMin;
end;

procedure TWebModule1.WebModuleDefaultAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  Response.Content :=
     '<html>' +
     '<head><title>DataSnap Server</title></head>' +
     '<body>DataSnap Server</body>' +
     '</html>';
end;

initialization

finalization

Web.WebReq.FreeWebModules;

end.
