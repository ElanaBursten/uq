object Form1: TForm1
  Left = 271
  Top = 114
  Caption = 'Form1'
  ClientHeight = 561
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 73
    Width = 456
    Height = 488
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 0
    OnDblClick = Memo1DblClick
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 456
    Height = 73
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 8
      Width = 20
      Height = 13
      Caption = 'Port'
    end
    object ButtonStop: TButton
      Left = 289
      Top = 19
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 0
      OnClick = ButtonStopClick
    end
    object ButtonStart: TButton
      Left = 152
      Top = 19
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 1
      OnClick = ButtonStartClick
    end
    object EditPort: TEdit
      Left = 8
      Top = 21
      Width = 81
      Height = 21
      TabOrder = 2
      Text = '8080'
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 400
    Top = 16
  end
end
