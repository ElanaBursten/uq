unit QtrMinU;

interface

uses System.SysUtils, System.Classes, System.Json, Data.FireDACJSONReflect,
  Datasnap.DSServer, Datasnap.DSAuth, Datasnap.DSProviderDataModuleAdapter,
  Data.DB, Windows,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MSSQL,FireDAC.Phys.MSSQLDef, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.VCLUI.Wait, FireDAC.Phys.ODBCBase, FireDAC.Stan.StorageBin,
  FireDAC.Stan.StorageJSON, FireDAC.Comp.UI, FireDAC.Comp.DataSet, FireDAC.Comp.Client;



type
{$METHODINFO ON}
  TQtrMin = class(TDataModule)
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDStanStorageJSONLink1: TFDStanStorageJSONLink;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink;
    spQtrMinIUD: TFDStoredProc;
    FDmemQtrMinute: TFDMemTable;
    qryQtrMinutes: TFDQuery;
    qryAreaIdCallCenter: TFDQuery;
    spFindQtrMin: TFDStoredProc;
    qryMap: TFDQuery;
    qryMapmap_id: TFDAutoIncField;
    qryMapmap_name: TStringField;
    qryMaparea_id: TFDAutoIncField;
    qryMaparea_name: TStringField;
    qryMapColor: TIntegerField;
    qryMapWebColor: TStringField;
    updAreaCentroid: TFDQuery;
    qryMapcentroid_lat: TStringField;
    qryMapcentroid_lng: TStringField;
    qryAreaList: TFDQuery;
    qryQtrMinInView: TFDQuery;
    qryAreaListColor: TIntegerField;
    qryAreaListarea_id: TIntegerField;
    qryAreaListcall_center: TStringField;
    qryAreaListemp_id: TFDAutoIncField;
    qryAreaListshort_name: TStringField;
    qryAreaListWebColor: TStringField;
    qryQtrMinInViewColor: TIntegerField;
    qryQtrMinInViewqtrmin_lat: TStringField;
    qryQtrMinInViewqtrmin_long: TStringField;
    qryQtrMinInViewarea_id: TIntegerField;
    qryQtrMinInViewcall_center: TStringField;
    qryQtrMinInViewWebColor: TStringField;
    delArea: TFDQuery;
    qryMapstate: TStringField;
    qryMapshort_name: TStringField;
    qryLogin: TFDQuery;
    FDConn: TFDConnection;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure FDConnAfterDisconnect(Sender: TObject);
  private
    function GetLogin(login, password: string): boolean;

    { Private declarations }
  public
    { Public declarations }
    function DELAreaById(login, password:string; areaID: integer): TJSONObject;
    function GETAreasByMap(login, password:string; const mapID: integer): TFDJSONDataSets;
    function GETQtrMinutesInView(login, password:string; const areaID: integer;
      callCenter, North, South, East, West: string): TFDJSONDataSets;
    function GETQtrMinutes(login, password:string; const areaID: integer; callCenter: string;
      optSelected, cntSelected: integer ): TFDJSONDataSets;
    function GETViewList(login, password:string; const North, South, East, West: string;
      mapID: integer = -1): TFDJSONDataSets;
    function GETQtrMinuteCount(login, password:string; const areaID: integer; callCenter: string)
      : TJSONObject;
    function GETWhoIsHere(login, password:string; const QtrMinLat, QtrMinLng: string): TFDJSONDataSets;
    function AddCallCenterQtrMinutes(login, password:string; const newQtrMinutes: TFDJSONDataSets)
      : TJSONObject;

  end;
{$METHODINFO OFF}
const
  offSet = '         ';
var
    sConnStr :string;

implementation

{$R *.dfm}

uses System.StrUtils, inifiles{$IFDEF  STANDALONE}
,SvrFormU
{$ENDIF}, WebModuleUnit1;

const
  sQTRMINUTES = 'QtrMinutes';
  sQTRMINUTESINVIEW = 'QtrMinutesInView';
  sAREASBYMAP = 'Areas by Map Id';
  sAreaList = 'Area List';
  sWHO_IS_HERE = 'WhoIsHere';
  sRETURN = 'Records added';
//  sConnStr =
//    'DriverID=MSSQL;Server=dyatl-dqmgdb01;Database=QM;User_Name=uqweb;Password=ihjidojo3';

function TQtrMin.GETViewList(login, password:string; const North, South, East, West: string;
  mapID: integer = -1): TFDJSONDataSets;
begin
  if not(GetLogin(login, password)) then exit;


  qryAreaList.ParamByName('North').AsString := North;
  qryAreaList.ParamByName('South').AsString := South;
  qryAreaList.ParamByName('East').AsString := East;
  qryAreaList.ParamByName('West').AsString := West;
  qryAreaList.ParamByName('mapID').AsInteger := mapID;

  try
    qryAreaList.Open;
  finally
    Result := TFDJSONDataSets.Create;
    TFDJSONDataSetsWriter.ListAdd(Result, sAreaList, qryAreaList);
    qryAreaList.close;
  end;
end;

function TQtrMin.GETQtrMinuteCount(login, password:string; const areaID: integer; callCenter: string)
  : TJSONObject;
const
  QRY_CNT =
  'Select count(*) as HowMany ' +
  'FROM [dbo].[map_qtrmin] ' +
  'where call_center = ''%s'' ' +
  'and area_id = %d ';
var
  cntReturn: TJSONObject;
  sCnt: string;
begin
  if not(GetLogin(login, password)) then exit;
  cntReturn := TJSONObject.Create;
  if callCenter = '0' then
    callCenter := '';
  qryQtrMinutes.SQL.Clear;
  qryQtrMinutes.SQL.Add(Format(QRY_CNT, [callCenter, areaID]));
  qryQtrMinutes.Open;

  sCnt := qryQtrMinutes.FieldByName('HowMany').AsString;

{$IFDEF  STANDALONE}
  Form1.Memo1.Lines.Add(TimeToStr(Time())+' | '+'GETQtrMinuteCount');
  Form1.Memo1.Lines.Add(offSet+'areaID '+IntToStr(areaID)+' | '+'callCenter '+callCenter);
  Form1.Memo1.Lines.Add(offSet+'sCnt '+sCnt);
{$ENDIF}

  Result := cntReturn.AddPair('cnt', sCnt);
end;

function TQtrMin.AddCallCenterQtrMinutes(login, password:string; const newQtrMinutes: TFDJSONDataSets)
  : TJSONObject;
var
  cntReturn: TJSONObject;
  sCnt: string;
  areaID: integer;
begin
  if not(GetLogin(login, password)) then exit;
  areaID := 0;
  cntReturn := TJSONObject.Create;
  FDmemQtrMinute.Active := false;
  FDmemQtrMinute.AppendData(TFDJSONDataSetsReader.GetListValue
    (newQtrMinutes, 0));
  FDmemQtrMinute.Open;

  while not(FDmemQtrMinute.Eof) do
  begin
    spQtrMinIUD.Params.ParamByName('@qtrminlat').Value :=
      FDmemQtrMinute.FieldByName('qtrmin_lat').AsString;
    spQtrMinIUD.Params.ParamByName('@qtrminlng').Value :=
      FDmemQtrMinute.FieldByName('qtrmin_long').AsString;
    if FDmemQtrMinute.FieldByName('call_center').AsString = '0' then
      spQtrMinIUD.Params.ParamByName('@callCenter').Value := ''
    else
      spQtrMinIUD.Params.ParamByName('@callCenter').Value :=
        FDmemQtrMinute.FieldByName('call_center').AsString;
    areaID := FDmemQtrMinute.FieldByName('area_id').AsInteger;
    spQtrMinIUD.Params.ParamByName('@areaId').Value := areaID;

    spQtrMinIUD.ExecProc;

    FDmemQtrMinute.Next;
  end;
  sCnt := IntToStr(FDmemQtrMinute.RecordCount);
  FDmemQtrMinute.close;
  if areaID <> 0 then
  begin
    updAreaCentroid.ParamByName('areaID').Value := areaID;
    updAreaCentroid.ExecSQL;
  end;
  Result := cntReturn.AddPair('cnt', sCnt);

end;

function TQtrMin.GETQtrMinutes(login, password:string; const areaID: integer; callCenter: string;
  optSelected, cntSelected: integer): TFDJSONDataSets;
const
  QRY_TOP = 'Select top %d * ';
  QRY_ALL = 'Select * ';

  QRY_FROM = 'FROM [dbo].[map_qtrmin] ' +
            'where call_center = ''%s'' ' +
            'and area_id = %d ';

  QRY_CORNERS = 'declare @callCenter varChar(20); ' + 'declare @areaId int; ' +
    ' set @callCenter = ''%s''; ' + ' set @areaId=%d; ' + ' ' + ' ' +
    'Select qm.* ' + 'FROM map_qtrmin qm ' + 'where ' +
    'qm.qtrmin_lat in (Select top %d qm2.qtrmin_lat ' + 'From map_qtrmin qm2 ' +
    'where qm2.call_center = @callCenter ' + 'and qm2.area_id = @areaId ' +
    'order by qm2.qtrmin_lat %s) ' + 'and ' +
    'qm.qtrmin_long in (Select top %d qm2.qtrmin_long ' + 'From map_qtrmin qm2 '
    + 'where qm2.call_center = @callCenter ' + 'and qm2.area_id = @areaId ' +
    'order by qm2.qtrmin_long %s) ' +
    'and (qm.call_center = @callCenter and qm.area_id = @areaId)';

  North = ' order by qtrmin_lat desc';
  South = ' order by qtrmin_lat asc';
   East = ' order by qtrmin_long asc';
   West = ' order by qtrmin_long desc';
begin
  if not(GetLogin(login, password)) then exit;
  qryQtrMinutes.Active := false;
  if callCenter = '0' then
    callCenter := '';

  case optSelected of
    0:
      begin
        qryQtrMinutes.SQL.Clear;
        qryQtrMinutes.SQL.Add(QRY_ALL);
        qryQtrMinutes.SQL.Add(Format(QRY_FROM, [callCenter, areaID]));
      end;

    101:
      begin
        qryQtrMinutes.SQL.Clear;
        qryQtrMinutes.SQL.Add(Format(QRY_TOP, [cntSelected]));
        qryQtrMinutes.SQL.Add(Format(QRY_FROM, [callCenter, areaID]));
        qryQtrMinutes.SQL.Add(North);
      end;
    102:
      begin
        qryQtrMinutes.SQL.Clear;
        qryQtrMinutes.SQL.Add(Format(QRY_TOP, [cntSelected]));
        qryQtrMinutes.SQL.Add(Format(QRY_FROM, [callCenter, areaID]));
        qryQtrMinutes.SQL.Add(South);
      end;
    103:
      begin
        qryQtrMinutes.SQL.Clear;
        qryQtrMinutes.SQL.Add(Format(QRY_CORNERS, [callCenter, areaID,
          cntSelected * 2, 'desc', cntSelected * 2, 'asc']));
      end;
    104:
      begin
        qryQtrMinutes.SQL.Clear;
        qryQtrMinutes.SQL.Add(Format(QRY_CORNERS, [callCenter, areaID,
          cntSelected * 2, 'asc', cntSelected * 2, 'asc']));
      end;
    105:
      begin
        qryQtrMinutes.SQL.Clear;
        qryQtrMinutes.SQL.Add(Format(QRY_TOP, [cntSelected]));
        qryQtrMinutes.SQL.Add(Format(QRY_FROM, [callCenter, areaID]));
        qryQtrMinutes.SQL.Add(East);
      end;
    106:
      begin
        qryQtrMinutes.SQL.Clear;
        qryQtrMinutes.SQL.Add(Format(QRY_TOP, [cntSelected]));
        qryQtrMinutes.SQL.Add(Format(QRY_FROM, [callCenter, areaID]));
        qryQtrMinutes.SQL.Add(West);
      end;
    107:
      begin
        qryQtrMinutes.SQL.Clear;
        qryQtrMinutes.SQL.Add(Format(QRY_CORNERS, [callCenter, areaID,
          cntSelected * 2, 'desc', cntSelected * 2, 'desc']));
      end;
    108:
      begin
        qryQtrMinutes.SQL.Clear;
        qryQtrMinutes.SQL.Add(Format(QRY_CORNERS, [callCenter, areaID,
          cntSelected * 2, 'asc', cntSelected * 2, 'desc']));
      end;
  end;

{$IFDEF  STANDALONE}
  Form1.Memo1.Lines.Add(TimeToStr(Time())+' | '+'GETQtrMinutes');
  Form1.Memo1.Lines.Add(offSet+'areaID '+IntToStr(areaID)+' | '+'callCenter '+callCenter);
  Form1.Memo1.Lines.Add(offSet+'optSelected '+IntToStr(optSelected)+' | '+'cntSelected '+IntToStr(cntSelected));
  Form1.Memo1.Lines.Add(offSet+'qryQtrMinutes '+qryQtrMinutes.SQL.Text);
{$ENDIF}

  Result := TFDJSONDataSets.Create;
  TFDJSONDataSetsWriter.ListAdd(Result, sQTRMINUTES, qryQtrMinutes);
end;

function TQtrMin.GetLogin(login, password :string):boolean;
var
  valid : boolean;
begin
  result := false;
  if ((login <> '') and (Password <> '')) then
  begin
    with qryLogin do
    begin
      close;
      ParamByName('empID').Value := StrToInt(login);
      ParamByName('password').Value := Password;
      open;
      valid := not(IsEmpty);
      close;
    end;
  end
  else
    valid := false;

 result := valid;
end;


function TQtrMin.GETQtrMinutesInView(login, password:string; const areaID: integer;
  callCenter, North, South, East, West: string): TFDJSONDataSets;
begin
  if not(GetLogin(login, password)) then exit;

  qryQtrMinInView.Active := false;
  if callCenter = '0' then
    callCenter := '';
  qryQtrMinInView.ParamByName('North').AsString := North;
  qryQtrMinInView.ParamByName('South').AsString := South;
  qryQtrMinInView.ParamByName('East').AsString := East;
  qryQtrMinInView.ParamByName('West').AsString := West;
  qryQtrMinInView.ParamByName('callCenter').AsString := callCenter;
  qryQtrMinInView.ParamByName('areaID').AsInteger := areaID;

  Result := TFDJSONDataSets.Create;
  TFDJSONDataSetsWriter.ListAdd(Result, sQTRMINUTESINVIEW, qryQtrMinInView);
end;

function TQtrMin.GETWhoIsHere(login, password:string; const QtrMinLat, QtrMinLng: string)
  : TFDJSONDataSets;
begin
  if not(GetLogin(login, password)) then exit;
  spFindQtrMin.ParamByName('@LatCode').Value := QtrMinLat;
  spFindQtrMin.ParamByName('@LongCode').Value := QtrMinLng;

  Result := TFDJSONDataSets.Create;
  TFDJSONDataSetsWriter.ListAdd(Result, sWHO_IS_HERE, spFindQtrMin);
end;

procedure TQtrMin.DataModuleCreate(Sender: TObject);
var
  IniFile : TIniFile;
  Path: String;
  sDriverID,sServer,sDatabase, sUser_Name, sPassword : string;

{
Fires once for Server LifeCycle
Fires each session for Session LifeCycle
Fires each call for Invocation LifeCycle
}

begin
  Path:= ExtractFilePath(GetModuleName(HInstance));
  IniFile := TIniFile.Create(Path+'QtrMinSetup.ini');
  sDriverID := IniFile.ReadString('connStr','DriverID','MSSQL');
  sServer := IniFile.ReadString('connStr','Server','dyatl-dqmgdb01');
  sDatabase := IniFile.ReadString('connStr','Database','QM');
  sUser_Name := IniFile.ReadString('connStr','User_Name','uqweb');
  sPassword := IniFile.ReadString('connStr','Password','ihjidojo3');

//{$IFDEF  STANDALONE}
//  sConnStr := 'DriverID=MSSQL;Server=SPRINGERRIDER\SPRINGERRIDERDB;Database=UtiliQuest;OSAuthent=Yes;';
//{$ELSE}
  sConnStr := 'DriverID='+sDriverID+';'+'Server='+sServer+';'+'Database='+sDatabase+';'+'User_Name='+sUser_Name+';'+'Password='+sPassword+';';
//{$ENDIF}

  FDConn.ConnectionString := sConnStr;
  FDConn.Open;

{$IFDEF  STANDALONE}
  Form1.Memo1.Lines.Add(TimeToStr(Time())+' | '+'DataModuleCreate');
  Form1.Memo1.Lines.Add(offSet+'sConnStr '+sConnStr);
{$ENDIF}

end;

procedure TQtrMin.DataModuleDestroy(Sender: TObject);
begin
   FDConn.Close;
{$IFDEF  STANDALONE}
  Form1.Memo1.Lines.Add(TimeToStr(Time())+' | '+'DataModuleDestroy');

{$ENDIF}
end;

function TQtrMin.DELAreaById(login, password:string; areaID: integer): TJSONObject;
var
  cntReturn: TJSONObject;
  sCnt: string;
begin
  if not(GetLogin(login, password)) then exit;
  cntReturn := TJSONObject.Create;

  delArea.ParamByName('areaID').Value := areaID;
  delArea.ExecSQL;

  sCnt := IntToStr(delArea.RowsAffected);

  Result := cntReturn.AddPair('cnt', sCnt);
end;

procedure TQtrMin.FDConnAfterDisconnect(Sender: TObject);
begin
{$IFDEF  STANDALONE}
  Form1.Memo1.Lines.Add(TimeToStr(Time())+' | '+'FDConnAfterDisconnect');
{$ENDIF}
end;

function TQtrMin.GETAreasByMap(login, password:string; const mapID: integer): TFDJSONDataSets;
var
  Emsg: string;
begin
  if not(GetLogin(login, password)) then exit;
  try
    qryMap.ParamByName('MAPID').AsInteger := mapID;
    Result := TFDJSONDataSets.Create;
    TFDJSONDataSetsWriter.ListAdd(Result, sAREASBYMAP, qryMap);
  except
    on e: exception do
    begin
      Emsg := e.message;
      OutputDebugString(PChar(Emsg));

    end;
  end;

{$IFDEF  STANDALONE}
  Form1.Memo1.Lines.Add(TimeToStr(Time())+' | '+'GETAreasByMap');
  Form1.Memo1.Lines.Add(offSet+'mapID '+IntToStr(mapID));
  Form1.Memo1.Lines.Add(offSet+'qryMap '+qryMap.SQL.Text);
{$ENDIF}

end;


end.
