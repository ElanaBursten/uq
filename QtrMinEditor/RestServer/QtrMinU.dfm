object QtrMin: TQtrMin
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 387
  Width = 508
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 216
    Top = 88
  end
  object FDStanStorageJSONLink1: TFDStanStorageJSONLink
    Left = 216
    Top = 248
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 216
    Top = 168
  end
  object FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink
    Left = 216
    Top = 24
  end
  object spQtrMinIUD: TFDStoredProc
    Connection = FDConn
    StoredProcName = 'IUD_QtrMinute'
    Left = 64
    Top = 248
    ParamData = <
      item
        Position = 1
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        ParamType = ptResult
      end
      item
        Position = 2
        Name = '@qtrminlat'
        DataType = ftString
        ParamType = ptInput
        Size = 5
      end
      item
        Position = 3
        Name = '@qtrminlng'
        DataType = ftString
        ParamType = ptInput
        Size = 6
      end
      item
        Position = 4
        Name = '@callCenter'
        DataType = ftString
        ParamType = ptInput
        Size = 20
      end
      item
        Position = 5
        Name = '@areaId'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object FDmemQtrMinute: TFDMemTable
    FieldDefs = <
      item
        Name = 'qtrmin_lat'
        DataType = ftWideString
      end
      item
        Name = 'qtrmin_long'
        DataType = ftWideString
      end
      item
        Name = 'Area_ID'
        DataType = ftInteger
      end
      item
        Name = 'call_center'
        DataType = ftWideString
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 328
    Top = 24
  end
  object qryQtrMinutes: TFDQuery
    Connection = FDConn
    UpdateOptions.AssignedValues = [uvCountUpdatedRecords, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.CountUpdatedRecords = False
    UpdateOptions.CheckRequired = False
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.CheckUpdatable = False
    Left = 328
    Top = 168
  end
  object qryAreaIdCallCenter: TFDQuery
    Connection = FDConn
    UpdateOptions.AssignedValues = [uvCountUpdatedRecords, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.CountUpdatedRecords = False
    UpdateOptions.CheckRequired = False
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.CheckUpdatable = False
    SQL.Strings = (
      'select area_id, call_center'
      'from [dbo].[map_qtrmin]'
      'where  [qtrmin_lat]= :qtrMinLat'
      ' and [qtrmin_long] = :qtrMinLng')
    Left = 328
    Top = 88
    ParamData = <
      item
        Name = 'QTRMINLAT'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'QTRMINLNG'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object spFindQtrMin: TFDStoredProc
    Connection = FDConn
    StoredProcName = 'find_qtrmin'
    Left = 64
    Top = 312
    ParamData = <
      item
        Position = 1
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        ParamType = ptResult
      end
      item
        Position = 2
        Name = '@LatCode'
        DataType = ftString
        ParamType = ptInput
        Size = 20
      end
      item
        Position = 3
        Name = '@LongCode'
        DataType = ftString
        ParamType = ptInput
        Size = 20
      end>
  end
  object qryMap: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'Declare @Color int;'
      'Declare @WebColor varChar(10);'
      'Set @Color = null;'
      'Set @WebColor = null;'
      'select '
      '@Color as Color'
      ',m.map_id'
      ',m.map_name'
      ',a.area_id'
      ',a.area_name'
      ',@WebColor as WebColor'
      ',a.[centroid_lat]'
      ',a.[centroid_lng]'
      ',m.state'
      ',e.short_name'
      'from map m '
      'inner join area a on m.map_id = a.map_id'
      'inner join employee e on a.locator_id = e.emp_id'
      'where m.map_id = :MapId'
      'and map_type = '#39'QUARTER'#39)
    Left = 216
    Top = 312
    ParamData = <
      item
        Name = 'MAPID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryMapColor: TIntegerField
      FieldName = 'Color'
      Origin = 'Color'
      ReadOnly = True
    end
    object qryMapmap_id: TFDAutoIncField
      FieldName = 'map_id'
      Origin = 'map_id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object qryMapmap_name: TStringField
      FieldName = 'map_name'
      Origin = 'map_name'
      Required = True
      Size = 30
    end
    object qryMaparea_id: TFDAutoIncField
      FieldName = 'area_id'
      Origin = 'area_id'
      ReadOnly = True
    end
    object qryMaparea_name: TStringField
      FieldName = 'area_name'
      Origin = 'area_name'
      Required = True
      Size = 40
    end
    object qryMapWebColor: TStringField
      FieldName = 'WebColor'
      Origin = 'WebColor'
      ReadOnly = True
      Size = 10
    end
    object qryMapcentroid_lat: TStringField
      FieldName = 'centroid_lat'
      Origin = 'centroid_lat'
      Size = 18
    end
    object qryMapcentroid_lng: TStringField
      FieldName = 'centroid_lng'
      Origin = 'centroid_lng'
      Size = 18
    end
    object qryMapstate: TStringField
      FieldName = 'state'
      Origin = 'state'
      Size = 5
    end
    object qryMapshort_name: TStringField
      FieldName = 'short_name'
      Origin = 'short_name'
      Size = 30
    end
  end
  object updAreaCentroid: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'declare'
      '  @cenLat varChar(18),'
      '  @cenLng varChar(18),'
      '  @areaID int;'
      ''
      'set @areaID = :areaID'
      ''
      
        'SELECT @cenLat = NullIf(Avg(dbo.QtrMinToDMS(QTR.qtrmin_lat)),0),' +
        ' @cenLng = NullIf(Avg(dbo.QtrMinToDMS(QTR.qtrmin_long)),0)'
      '  FROM [dbo].[map_qtrmin] QTR'
      '  where area_id = @areaID'
      ''
      '  update [dbo].[area]'
      '  set [centroid_lat] = @cenLat,'
      '      [centroid_lng] = @cenLng'
      'where area_id  = @areaID')
    Left = 328
    Top = 248
    ParamData = <
      item
        Name = 'AreaID'
        DataType = ftInteger
        FDDataType = dtInt32
        ParamType = ptInput
      end>
  end
  object qryAreaList: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'Declare @Color int;'
      'Declare @WebColor varChar(12);'
      'Set @Color = null;'
      'Set @WebColor = null;'
      
        'select distinct @Color as Color, qm.area_id, qm.call_center, emp' +
        'loyee.emp_id, Rtrim(employee.short_name)short_name,@WebColor as ' +
        'WebColor '
      'from [dbo].[map_qtrmin] qm'
      'left outer join area on qm.area_id = area.area_id'
      'left outer join employee on area.locator_id = employee.emp_id'
      'where ([qtrmin_lat]>= :South and [qtrmin_lat]<= :North)'
      'and ([qtrmin_long]>= :East and  [qtrmin_long] <= :West)'
      '  and employee.active = 1 '
      '  and employee.can_receive_tickets = 1'
      '  and ((-1 = area.map_id) or (area.map_id <> :mapID))'
      '')
    Left = 64
    Top = 96
    ParamData = <
      item
        Name = 'SOUTH'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'NORTH'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'EAST'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'WEST'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'MAPID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryAreaListColor: TIntegerField
      FieldName = 'Color'
      Origin = 'Color'
      ReadOnly = True
    end
    object qryAreaListarea_id: TIntegerField
      FieldName = 'area_id'
      Origin = 'area_id'
    end
    object qryAreaListcall_center: TStringField
      FieldName = 'call_center'
      Origin = 'call_center'
      Required = True
    end
    object qryAreaListemp_id: TFDAutoIncField
      FieldName = 'emp_id'
      Origin = 'emp_id'
      ReadOnly = True
    end
    object qryAreaListshort_name: TStringField
      FieldName = 'short_name'
      Origin = 'short_name'
      ReadOnly = True
      Size = 30
    end
    object qryAreaListWebColor: TStringField
      FieldName = 'WebColor'
      Origin = 'WebColor'
      ReadOnly = True
      Size = 12
    end
  end
  object qryQtrMinInView: TFDQuery
    Connection = FDConn
    UpdateOptions.AssignedValues = [uvCountUpdatedRecords, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.CountUpdatedRecords = False
    UpdateOptions.CheckRequired = False
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.CheckUpdatable = False
    SQL.Strings = (
      'Declare @Color int;'
      'Declare @WebColor varChar(12);'
      'Set @Color = null;'
      'Set @WebColor = null;'
      'select @Color as Color'
      ',[qtrmin_lat]'
      ',[qtrmin_long]'
      ',[area_id]'
      ',[call_center]'
      ',@WebColor as WebColor'
      'from [dbo].[map_qtrmin] '
      'where ([qtrmin_lat]>= :South and [qtrmin_lat]<= :North)'
      'and ([qtrmin_long]>= :East and  [qtrmin_long] <= :West)'
      'and call_center = :callCenter'
      'and area_id = :areaId'
      ''
      '')
    Left = 64
    Top = 168
    ParamData = <
      item
        Name = 'SOUTH'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'NORTH'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'EAST'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'WEST'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CALLCENTER'
        ParamType = ptInput
      end
      item
        Name = 'AREAID'
        ParamType = ptInput
      end>
    object qryQtrMinInViewColor: TIntegerField
      FieldName = 'Color'
      Origin = 'Color'
    end
    object qryQtrMinInViewqtrmin_lat: TStringField
      FieldName = 'qtrmin_lat'
      Origin = 'qtrmin_lat'
      Required = True
      Size = 5
    end
    object qryQtrMinInViewqtrmin_long: TStringField
      FieldName = 'qtrmin_long'
      Origin = 'qtrmin_long'
      Required = True
      Size = 6
    end
    object qryQtrMinInViewarea_id: TIntegerField
      FieldName = 'area_id'
      Origin = 'area_id'
    end
    object qryQtrMinInViewcall_center: TStringField
      FieldName = 'call_center'
      Origin = 'call_center'
      Required = True
    end
    object qryQtrMinInViewWebColor: TStringField
      FieldName = 'WebColor'
      Origin = 'WebColor'
      Size = 12
    end
  end
  object delArea: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'DELETE FROM [dbo].[map_qtrmin]'
      '      WHERE area_id = :areaID'
      ''
      '')
    Left = 328
    Top = 312
    ParamData = <
      item
        Name = 'AREAID'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryLogin: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'select 1  '
      'FROM [dbo].[users]'
      '  where emp_id = :empID'
      '  and password = :passWord'
      ''
      '')
    Left = 414
    Top = 165
    ParamData = <
      item
        Name = 'EMPID'
        ParamType = ptInput
      end
      item
        Name = 'PASSWORD'
        ParamType = ptInput
      end>
  end
  object FDConn: TFDConnection
    LoginPrompt = False
    AfterDisconnect = FDConnAfterDisconnect
    Left = 56
    Top = 24
  end
end
