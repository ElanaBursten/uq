unit aBaseAttachmentDMu;

interface

uses
  System.SysUtils, System.Classes, BaseAttachmentDMu, Data.DB, Data.Win.ADODB;

type
  TimageDM = class(TBaseAttachment)
    ADOConn: TADOConnection;
    qryTicketList: TADOQuery;
    qryTicketListticket_number: TStringField;
    qryTicketListticket_id: TAutoIncField;
    qryTicketListclosed_date_only: TDateTimeField;
    qryTicketListFileDate: TDateTimeField;
    qryTicketListclosed_date: TDateTimeField;
    qryTicketListclient_code: TStringField;
    qryTicketListstatus: TStringField;
    qryTicketListmodified_date: TDateTimeField;
    qryTicketListclosed: TBooleanField;
    qryTicketListwork_address_number: TStringField;
    qryTicketListwork_address_number_2: TStringField;
    qryTicketListwork_address_street: TStringField;
    qryTicketListwork_state: TStringField;
    qryTicketListwork_county: TStringField;
    qryTicketListwork_city: TStringField;
    qryTicketListmap_page: TStringField;
    qryTicketListtransmit_date: TDateTimeField;
    qryTicketListdue_date: TDateTimeField;
    qryTicketListFeetMarked: TIntegerField;
    qryTicketListshort_name: TStringField;
    qryClient: TADOQuery;
    qryCallCenter: TADOQuery;
    qryAPI_storage_credentials: TADOQuery;
    qryAPI_storage_constants: TADOQuery;
    qryAttachmentIDs: TADOQuery;
  private

    function GetAWSConstants: Boolean;
    function GetAWSCredentials: Boolean;
    { Private declarations }
  public
    { Public declarations }
    function connectToDB: Boolean;
  end;

var
  imageDM: TimageDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uMainFTP, Vcl.Forms, OdUqInternet;

{$R *.dfm}
function TimageDM.connectToDB: Boolean;
var
  Path, Path2: String;
begin
  try
    Path := ExtractFilePath(application.ExeName);
    try
      if (frmMainFTP.DBType = 'dbQM') then
        Path2 := 'FILE NAME=' + Path + 'adoConnQM.udl';
//        ADOConn.ConnectionString := Path2;
//      end
//      else if (frmMainFTP.DBType = 'dbLocQM') then
//        ADOConn.ConnectionString := 'FILE NAME=' + Path + 'adoConnLocQM.udl';

      ADOConn.Open();
    except
      on E: Exception do
      begin
        frmMainFTP.LogResult.LogType := ltError;
        frmMainFTP.LogResult.MethodName := 'connectToDB';
        frmMainFTP.LogResult.ExcepMsg := 'Connection failed ' + E.Message;
        ADOConn.ConnectionString;
        frmMainFTP.WriteLog(frmMainFTP.LogResult);
      end;
    end;
  finally
    If ADOConn.Connected then
    begin
      GetAWSConstants;
      GetAWSCredentials;
      frmMainFTP.LogResult.LogType := ltInfo;
      frmMainFTP.LogResult.MethodName := 'connectToDB';
      frmMainFTP.LogResult.DataStream := 'ADOConn.ConnectionString: ' +
        ADOConn.ConnectionString;
      frmMainFTP.WriteLog(frmMainFTP.LogResult);
      result := true;
    end
    else
      result := false;
  end;
end;

function TimageDM.GetAWSConstants: Boolean;  //QMANTWO-555 sr
begin
   if qryAPI_storage_constants.Active then
      qryAPI_storage_constants.Close;
   qryAPI_storage_constants.Open;
   if qryAPI_storage_constants.RecordCount > 0 then begin
     myAttachmentData.Constants.Host := qryAPI_storage_constants.FieldByName('host').AsString;
     myAttachmentData.Constants.RestURL :=  qryAPI_storage_constants.FieldByName('rest_url').AsString;
     myAttachmentData.Constants.Service := qryAPI_storage_constants.FieldByName('service').AsString;
     myAttachmentData.Constants.StagingBucket := qryAPI_storage_constants.FieldByName('bucket').AsString;
     myAttachmentData.Constants.StagingFolder := qryAPI_storage_constants.FieldByName('bucket_folder').AsString;
     myAttachmentData.Constants.Region := qryAPI_storage_constants.FieldByName('region').AsString;
     myAttachmentData.Constants.Algorithm := qryAPI_storage_constants.FieldByName('algorithm').AsString;
     myAttachmentData.Constants.SignedHeaders := qryAPI_storage_constants.FieldByName('signed_headers').AsString;
     myAttachmentData.Constants.ContentType := qryAPI_storage_constants.FieldByName('content_type').AsString;
     myAttachmentData.Constants.AcceptedValues := qryAPI_storage_constants.FieldByName('accepted_values').AsString;
     if myAttachmentData.Constants.Host <> '' then
       Result := True
     else
       Result := False
   end
   else
     Result := False;
end;

function TimageDM.GetAWSCredentials: Boolean;   //QMANTWO-555 sr
begin
    myAttachmentData.AWSKey := '';
    myAttachmentData.AWSSecret := '';
  if qryAPI_storage_credentials.Active then
    qryAPI_storage_credentials.Close;

  qryAPI_storage_credentials.Open;
  if qryAPI_storage_credentials.RecordCount > 0 then begin
    myAttachmentData.AWSKey := qryAPI_storage_credentials.FieldByName('enc_access_key').AsWideString;
    myAttachmentData.AWSSecret := qryAPI_storage_credentials.FieldByName('enc_secret_key').AsWideString;
    if (myAttachmentData.AWSKey <> '') and (myAttachmentData.AWSSecret <> '') then
      Result := True
    else
      Result := False;
  end
  else begin
    myAttachmentData.AWSKey := '';
    myAttachmentData.AWSSecret := '';
    Result := False;
  end;
end;
end.
