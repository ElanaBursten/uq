object frmMainFTP: TfrmMainFTP
  Left = 0
  Top = 0
  Caption = 'Image Mover'
  ClientHeight = 761
  ClientWidth = 953
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -18
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  ShowHint = True
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 22
  object Label4: TLabel
    Left = 343
    Top = 18
    Width = 44
    Height = 22
    Margins.Left = 13
    Margins.Top = 13
    Margins.Right = 13
    Margins.Bottom = 13
    Caption = 'Client'
  end
  object Label3: TLabel
    Left = 51
    Top = 18
    Width = 82
    Height = 22
    Margins.Left = 13
    Margins.Top = 13
    Margins.Right = 13
    Margins.Bottom = 13
    Caption = 'Call center'
  end
  object Label5: TLabel
    Left = 305
    Top = 88
    Width = 106
    Height = 22
    Margins.Left = 13
    Margins.Top = 13
    Margins.Right = 13
    Margins.Bottom = 13
    Caption = 'Sync Date To'
  end
  object Label6: TLabel
    Left = 43
    Top = 88
    Width = 125
    Height = 22
    Margins.Left = 13
    Margins.Top = 13
    Margins.Right = 13
    Margins.Bottom = 13
    Caption = 'Sync Date From'
  end
  object Label1: TLabel
    Left = 648
    Top = 18
    Width = 120
    Height = 22
    Caption = 'Export Settings'
  end
  object lblConnected: TLabel
    Left = 576
    Top = 648
    Width = 175
    Height = 22
    Caption = 'Not Connected to FTP'
  end
  object cbClient: TComboBox
    Left = 290
    Top = 48
    Width = 165
    Height = 30
    Margins.Left = 13
    Margins.Top = 13
    Margins.Right = 13
    Margins.Bottom = 13
    TabOrder = 1
    OnCloseUp = cbClientCloseUp
  end
  object cbCallCenter: TComboBox
    Left = 26
    Top = 48
    Width = 165
    Height = 30
    Margins.Left = 13
    Margins.Top = 13
    Margins.Right = 13
    Margins.Bottom = 13
    TabOrder = 0
    OnCloseUp = cbCallCenterCloseUp
    OnSelect = cbCallCenterSelect
  end
  object btnRun: TButton
    Left = 94
    Top = 637
    Width = 107
    Height = 50
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Run'
    TabOrder = 5
    OnClick = btnRunClick
  end
  object SyncDateFr: TDateTimePicker
    Left = 26
    Top = 123
    Width = 165
    Height = 30
    Margins.Left = 13
    Margins.Top = 13
    Margins.Right = 13
    Margins.Bottom = 13
    Date = 42779.619772500000000000
    Time = 42779.619772500000000000
    TabOrder = 2
    OnChange = SyncDateFrChange
  end
  object SyncDateTo: TDateTimePicker
    Left = 290
    Top = 123
    Width = 165
    Height = 30
    Margins.Left = 13
    Margins.Top = 13
    Margins.Right = 13
    Margins.Bottom = 13
    Date = 42779.619847997690000000
    Time = 42779.619847997690000000
    TabOrder = 3
  end
  object Memo1: TMemo
    Left = 21
    Top = 264
    Width = 478
    Height = 341
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    ScrollBars = ssBoth
    TabOrder = 6
  end
  object StatusBar2: TStatusBar
    Left = 0
    Top = 742
    Width = 953
    Height = 19
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Panels = <
      item
        Width = 350
      end
      item
        Width = 50
      end
      item
        Text = '  of'
        Width = 30
      end
      item
        Width = 50
      end>
  end
  object cbSendFTP: TCheckBox
    Left = 26
    Top = 170
    Width = 175
    Height = 37
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Send To FTP'
    TabOrder = 4
  end
  object cbCloseDone: TCheckBox
    Left = 291
    Top = 169
    Width = 164
    Height = 38
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Close when done'
    Checked = True
    State = cbChecked
    TabOrder = 8
  end
  object btnAbort: TButton
    Left = 266
    Top = 637
    Width = 97
    Height = 46
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Abort'
    TabOrder = 9
    OnClick = btnAbortClick
  end
  object cbSendTSV: TCheckBox
    Left = 26
    Top = 216
    Width = 127
    Height = 38
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Send TSV'
    TabOrder = 10
    OnClick = cbSendTSVClick
  end
  object cbSendXML: TCheckBox
    Left = 185
    Top = 217
    Width = 120
    Height = 37
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Send XML'
    TabOrder = 11
    OnClick = cbSendXMLClick
  end
  object cbSendImages: TCheckBox
    Left = 337
    Top = 217
    Width = 152
    Height = 37
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 16
    Caption = 'Send Images'
    TabOrder = 12
    OnClick = cbSendImagesClick
  end
  object settingsList: TCheckListBox
    Left = 518
    Top = 641
    Width = 427
    Height = 46
    ItemHeight = 22
    TabOrder = 13
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 726
    Width = 953
    Height = 16
    Align = alBottom
    TabOrder = 14
  end
  object IdFTP1: TIdFTP
    OnStatus = IdFTP1Status
    IPVersion = Id_IPv4
    Host = '192.168.1.83'
    ConnectTimeout = 0
    Password = 'nwn2009!'
    TransferType = ftBinary
    Username = 'locatinginc'
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 264
    Top = 288
  end
  object MainMenu1: TMainMenu
    Left = 224
    Top = 48
    object Settings1: TMenuItem
      Caption = 'Settings'
      Hint = 'Used for Settings options'
      object InitExportSettings1: TMenuItem
        Caption = 'Init Export Settings'
        Hint = 
          'Puts the dataset fields in INI.  Should only have to be run once' +
          ' unless dataset changes.'
        OnClick = InitExportSettings1Click
      end
      object ReadSettings1: TMenuItem
        Caption = 'Read Settings'
        Hint = 'Reads Dataset fields into Settings screen'
        OnClick = ReadSettings1Click
      end
      object SavetoINI1: TMenuItem
        Caption = 'Save to INI'
        Hint = 'Saves Settings screen to INI'
        OnClick = SavetoINI1Click
      end
    end
  end
end
