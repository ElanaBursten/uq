program ImageMover;

uses
  Vcl.Forms,
  Windows,
  SysUtils,
  uMainFTP in 'uMainFTP.pas' {frmMainFTP},
  GlobalSU in 'GlobalSU.pas',
  miniCVUtils in 'miniCVUtils.pas',
  BaseAttachmentDMu in '..\common\BaseAttachmentDMu.pas' {BaseAttachment: TDataModule},
  ServerAttachmentDMu in '..\server\ServerAttachmentDMu.pas' {ServerAttachment: TDataModule},
  XSuperObject in '..\thirdparty\XSuperObject\XSuperObject.pas',
  XSuperJSON in '..\thirdparty\XSuperObject\XSuperJSON.pas',
  QMConst in '..\client\QMConst.pas',
  OdUqInternet in '..\common\OdUqInternet.pas',
  xqCbListBoxHelper in  '..\common\xqCbListBoxHelper.pas',
  Hashes in '..\thirdparty\Hashes.pas',
  aBaseAttachmentDMu in 'aBaseAttachmentDMu.pas' {imageDM: TDataModule};

{$R *.res}

var
  MyInstanceName: string;

begin
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    if ParamCount > 1 then
    begin
      Application.ShowMainForm := false;
    end
    else
    begin
      Application.ShowMainForm := true;

    end;
    ReportMemoryLeaksOnShutdown := DebugHook <> 0;
    Application.Title := 'Image Mover';
  Application.CreateForm(TfrmMainFTP, frmMainFTP);
  Application.CreateForm(TimageDM, imageDM);
  Application.Run;
  end;

end.
