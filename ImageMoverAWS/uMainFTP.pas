unit uMainFTP;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,  OdUqInternet,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB,
  Winapi.SHFolder, Winapi.SHLObj, Winapi.KnownFolders, Winapi.ActiveX,
  System.IOUtils, System.Types, IdComponent, IdBaseComponent,
  IdTCPClient, IdExplicitTLSClientServerBase, IdFTP, Vcl.Grids, Vcl.ValEdit,
  Vcl.ComCtrls, aBaseAttachmentDMu, IdTCPConnection, idFTPCommon, Vcl.CheckLst,
  xqCbListBoxHelper, Vcl.Menus;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[90];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;

type
  TfrmMainFTP = class(TForm)
    Label4: TLabel;
    cbClient: TComboBox;
    cbCallCenter: TComboBox;
    Label3: TLabel;
    btnRun: TButton;
    SyncDateFr: TDateTimePicker;
    SyncDateTo: TDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    Memo1: TMemo;
    IdFTP1: TIdFTP;
    StatusBar2: TStatusBar;
    cbSendFTP: TCheckBox;
    cbCloseDone: TCheckBox;
    btnAbort: TButton;
    cbSendTSV: TCheckBox;
    cbSendXML: TCheckBox;cbSendImages: TCheckBox;
    settingsList: TCheckListBox;
    MainMenu1: TMainMenu;
    Settings1: TMenuItem;
    InitExportSettings1: TMenuItem;
    ReadSettings1: TMenuItem;
    SavetoINI1: TMenuItem;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    lblConnected: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cbCallCenterSelect(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure IdFTP1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure SyncDateFrChange(Sender: TObject);
    procedure ADOConnAfterConnect(Sender: TObject);
    procedure btnAbortClick(Sender: TObject);
    procedure cbCallCenterCloseUp(Sender: TObject);
    procedure cbClientCloseUp(Sender: TObject);
    procedure cbSendTSVClick(Sender: TObject);
    procedure cbSendXMLClick(Sender: TObject);
    procedure cbSendImagesClick(Sender: TObject);
    procedure InitExportSettings1Click(Sender: TObject);
    procedure ReadSettings1Click(Sender: TObject);
    procedure SavetoINI1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
      cnt: integer;
    function ReadINI: Boolean;
    function connectToDB: Boolean;
    procedure ExportTSV(state: string; closeDate: TDate);
    procedure ExportToXML(state: string; closeDate: TDate);
    procedure clearLogRecord;

    function getAppVer: String;
    procedure RunProcess(Sender: TObject);
    function downloadAttachmentsForTicket(ticketID: integer; tempTktPath:string): integer;

    procedure InitData;
    procedure PopCallCenters;
    procedure PopClientList;
    procedure GetTicketList;
    procedure copyAttachments;

    procedure ProcessCV(tempTktPath, sTicket, sLeadIn: string);
    procedure RenameFiles(tempTktPath, sLeadIn: string);

    function FtpInit: Boolean;
    procedure primeCbListBox;
    { Private declarations }
  public
    { Public declarations }
    LogResult: TLogResults;
    LocalAppDataFolder: string;
    fActiveCallCenter: string;
    fActiveCallClient: string;
    fProcessDate: TDate;
    fFTPmode: Boolean;
    fPort: integer;
    fpassword: string;
    fHost: string;
    fusername: string;
    fDBType: string;
    fFtpAttLog: string;
    canRun: Boolean;
    fremFtpDir: string;
    fsendTSV: Boolean;
    fsendXML: Boolean;
    fsendImages: Boolean;
    IniName: string;
    imageDM: TimageDM;
    property sendImages: Boolean read fsendImages write fsendImages;
    property sendTSV: Boolean read fsendTSV write fsendTSV;
    property sendXML: Boolean read fsendXML write fsendXML;
    property appVer: String read getAppVer;
    property DBType: string read fDBType write fDBType;
    property Host: string read fHost write fHost;
    property Port: integer read fPort write fPort;
    property username: string read fusername write fusername;
    property password: string read fpassword write fpassword;
    property FTPmode: Boolean read fFTPmode write fFTPmode;
    property FtpAttLog: string read fFtpAttLog write fFtpAttLog;
    property remFtpDir: string read fremFtpDir write fremFtpDir;
    property ActiveCallCenter: string read fActiveCallCenter
      write fActiveCallCenter;
    property ActiveCallClient: string read fActiveCallClient
      write fActiveCallClient;
    property ProcessDate: TDate read fProcessDate write fProcessDate;
    procedure WriteLog(LogResult: TLogResults);
  end;

function GetKnownFolderPath(const folder: KNOWNFOLDERID): string;
function GetAppVersionStr: string;

const
  BASE_PATH_UTL_ATT = '\\dynutil.com\utq\QMattach\UTL_ATT\Tickets\';


var
  frmMainFTP: TfrmMainFTP;
  ABORT_PROCESS: Boolean;

implementation

uses DateUtils, inifiles, miniCVUtils;
{$R *.dfm}

procedure TfrmMainFTP.ADOConnAfterConnect(Sender: TObject);
begin
  if DBType = 'dbQM' then
    StatusBar2.Panels[2].Text := 'Connected to: QM on QACluster'
  else
    StatusBar2.Panels[2].Text := 'Not Connected';

  ABORT_PROCESS := false;
end;

procedure TfrmMainFTP.btnAbortClick(Sender: TObject);
begin
  ABORT_PROCESS := true;
  cbCloseDone.Checked:=true;
end;

procedure TfrmMainFTP.btnRunClick(Sender: TObject);
begin
  RunProcess(Sender);
end;

procedure TfrmMainFTP.RunProcess(Sender: TObject);
var
  bldThread: Tthread;
begin
  if not assigned(Sender) then
    self.WindowState := wsMinimized;

  btnRun.Enabled := false;
  canRun := true;
  self.Cursor := crSQLWait;
  GetTicketList;
  self.Cursor := crDefault;
  bldThread := Tthread.CreateAnonymousThread(
    procedure
    begin
      while ((not application.Terminated) and (canRun = true)) do
      begin
        copyAttachments;
      end;
    end);
  bldThread.Start;
end;

procedure TfrmMainFTP.cbCallCenterCloseUp(Sender: TObject);
begin
  ActiveCallCenter := cbCallCenter.Text;
end;

procedure TfrmMainFTP.cbCallCenterSelect(Sender: TObject);
begin
  PopClientList;
end;

procedure TfrmMainFTP.cbClientCloseUp(Sender: TObject);
begin
  ActiveCallClient := cbClient.Text;
end;

procedure TfrmMainFTP.cbSendImagesClick(Sender: TObject);
begin
  sendImages := cbSendImages.Checked;
end;

procedure TfrmMainFTP.cbSendTSVClick(Sender: TObject);
begin
  sendTSV := cbSendTSV.Checked;
end;

procedure TfrmMainFTP.cbSendXMLClick(Sender: TObject);
begin
  sendXML := cbSendXML.Checked;
end;

procedure TfrmMainFTP.copyAttachments;
var
  closedDate: TDate;
  ticketID: integer;
  processed : integer;
  sTicketID: string;
  ticketNo: string;
  workState: string;
  tempTktPath: string;
  LeadIn: string;
begin
  try
    processed:= 0;
    closedDate := SyncDateTo.Date;
    ticketID := -1;
    sTicketID := '';
    if cbSendFTP.Checked then
      FtpInit;
    if sendImages then
    begin
      with imageDM do
      begin
        qryTicketList.First;
        while not qryTicketList.eof do
        begin
          if ABORT_PROCESS then
          begin
            canRun := false;
            break;
          end;

          StatusBar2.Panels[0].Text := '';

          ticketID := qryTicketList.FieldByName('ticket_id').AsInteger;
          ticketNo := qryTicketList.FieldByName('ticket_number').AsString;
          sTicketID := 'T' + intToStr(ticketID);
          closedDate := DateOf(qryTicketList.FieldByName('filedate')
            .AsDateTime);
          workState := qryTicketList.FieldByName('work_state').AsString;
          LeadIn := UpperCase(workState) + '-' + ticketNo + '-';
          tempTktPath := LocalAppDataFolder + '\Temp\tempFtpExport' + '\' + intToStr(ticketID);
          if downloadAttachmentsForTicket(ticketID, tempTktPath) = 0 then
          begin
            qryTicketList.next;
            continue;  //has no attachments
          end;
          inc(processed);
          StatusBar2.Panels[0].Text := tempTktPath;
          StatusBar2.Panels[1].Text := IntToStr(processed);
          StatusBar2.Panels[3].Text := IntToStr(cnt);
          StatusBar2.refresh;

{$IFNDEF DEBUG}
          ProcessCV(tempTktPath, sTicketID, LeadIn);
{$ENDIF}
          RenameFiles(tempTktPath, LeadIn);
          qryTicketList.Next;
          ProgressBar1.StepIt;
          LogResult.LogType := ltInfo;
          LogResult.Status := 'Processed attachments for ticket ' + ticketNo;
          LogResult.MethodName := 'ExportTSV';
          WriteLog(LogResult);
        end; // while not qryTicketList.eof do
      end; // with imageDM do
    end; // if sendImages then

  finally
{$IFNDEF DEBUG}
    if cbSendFTP.Checked then
    begin
{$ENDIF}
      if sendTSV then
        ExportTSV(workState, closedDate);
      if sendXML then
        ExportToXML(workState, closedDate);
{$IFNDEF DEBUG}
    end;
{$ENDIF}
    imageDM.qryTicketList.close;
    Memo1.Lines.Add('Completed transferring images for ' + cbCallCenter.Text +
      ' client ' + cbClient.Text);
    Memo1.Lines.Add('In ' + DateTimeToStr(closedDate) + ' folder');
    btnRun.Enabled := true;
    if cbSendFTP.Checked then
      IdFTP1.Disconnect;
    canRun := false;
    if cbCloseDone.Checked then
      application.Terminate;
    application.ProcessMessages;
  end;
end;

function TfrmMainFTP.FtpInit: Boolean;
var
  currDir: string;
begin
  try
    IdFTP1.Host := Host;
    IdFTP1.Port := Port;
    IdFTP1.username := username;
    IdFTP1.password := password;
    IdFTP1.Passive := FTPmode;

    if not IdFTP1.Connected then
      IdFTP1.Connect;
    IdFTP1.ChangeDir(remFtpDir);
    currDir := IdFTP1.RetrieveCurrentDir;

    if currDir = remFtpDir then
      result := true;
  finally
    LogResult.LogType := ltInfo;
    LogResult.Status := 'FtpInit';
    LogResult.MethodName := 'Ftp Initialization';
    WriteLog(LogResult);
  end;

end;

procedure TfrmMainFTP.ProcessCV(tempTktPath, sTicket, sLeadIn: string);
var
  searchResult: TSearchRec;
  oldJpegName, newJpegName: string;
begin
  Memo1.Lines.Add('Looking for @CV to crack in ' + tempTktPath);
  setCurrentDir(tempTktPath);

  if findFirst('*.@CV', faAnyFile, searchResult) = 0 then
  begin
    repeat
      Memo1.Lines.Add(searchResult.Name + ' to crack');

      If ReplaceCVs(tempTktPath + '\' + searchResult.Name, sTicket, IniName)
      then
      begin
        LogResult.LogType := ltInfo;
        LogResult.Status := 'Cracked ' + searchResult.Name;
        LogResult.MethodName := 'ProcessCV';
      end
      else
      begin
        LogResult.LogType := ltError;
        LogResult.Status := 'Failed to Crack ' + searchResult.Name;
        LogResult.MethodName := 'ProcessCV';
      end;
      WriteLog(LogResult);
    until FindNext(searchResult) <> 0;
    FindClose(searchResult);
  end;
end;

procedure TfrmMainFTP.RenameFiles(tempTktPath, sLeadIn: string);
var
  searchResult: TStringDynArray; // TSearchRec;
  oldJpegName, newJpegName: string;
  i: integer;
  ftpFile: string;
begin
  setCurrentDir(tempTktPath);

  searchResult := TDirectory.GetFiles(tempTktPath, '*.JPG');
  for i := Low(searchResult) to High(searchResult) do
  begin
    oldJpegName := ExtractFileName(searchResult[i]);
    newJpegName := sLeadIn + oldJpegName;
    RenameFile(oldJpegName, newJpegName);
    ftpFile := ExtractFileName(newJpegName);
    try
      if cbSendFTP.Checked then
      begin

        if not IdFTP1.Connected then
          IdFTP1.Connect;
        IdFTP1.TransferType := ftBinary;
        IdFTP1.Put(newJpegName, ftpFile);
      end;
    except
      on E: Exception do
        Memo1.Lines.Add(E.Message);
    end;
  end;
  searchResult := nil;
end;

procedure TfrmMainFTP.SavetoINI1Click(Sender: TObject);
begin
  settingsList.SaveToINI;
end;

procedure TfrmMainFTP.SyncDateFrChange(Sender: TObject);
begin
  SyncDateTo.Date := SyncDateFr.Date + 1;
end;

procedure TfrmMainFTP.FormActivate(Sender: TObject);
begin
  settingsList.ReadFromINI;
end;

procedure TfrmMainFTP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ABORT_PROCESS := true;
  Action := caFree;
end;

procedure TfrmMainFTP.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if imageDM.ADOConn.Connected then
    imageDM.ADOConn.close;
  CanClose := cbCloseDone.Checked;
end;

procedure TfrmMainFTP.FormCreate(Sender: TObject);
begin
  self.Caption := self.Caption + '       ' + appVer;
  ReadINI;
  InitData;
//  primeCbListBox;
  PopCallCenters;
{$IFNDEF DEBUG}
  if ParamCount > 1 then
  begin
    RunProcess(nil);
  end;
{$ENDIF}
end;

procedure TfrmMainFTP.primeCbListBox;
begin
    with imageDM.qryTicketList do
    begin
      close;
      Parameters.ParamByName('SyncDateFrom').value := '1/1/1900';
      Parameters.ParamByName('SyncDateTo').value := '1/2/1900';
      Parameters.ParamByName('ClientCode').value := 'LOR1';
      Parameters.ParamByName('callCenter').value := 'NWN01';
    end;
   SettingsList.InitializeFromDataset(imageDM.qryTicketList);
end;

procedure TfrmMainFTP.GetTicketList;
begin
  cnt := 0;
  try
    imageDM.qryTicketList.close;
    with imageDM.qryTicketList do
    begin
      Parameters.ParamByName('SyncDateFrom').value := SyncDateFr.Date;
      Parameters.ParamByName('SyncDateTo').value := SyncDateTo.Date;
      Parameters.ParamByName('ClientCode').value := cbClient.Text;
      Parameters.ParamByName('callCenter').value := cbCallCenter.Text;
    end;
    Memo1.Lines.Add('qryTicketList initiated: ' + TimeToStr(now));
    imageDM.qryTicketList.Open;
    cnt := imageDM.qryTicketList.RecordCount;
    ProgressBar1.max:=cnt;
    Memo1.Lines.Add('qryTicketList opened ' + intToStr(cnt) + ' records at ' +
      TimeToStr(now));
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'GetTicketList';
    LogResult.Status := 'qryTicketList opened ' + intToStr(cnt) + ' records at '
      + TimeToStr(now);
    WriteLog(LogResult);
  except
    on E: Exception do
    begin
      Memo1.Lines.Add('Ticket list query failed with: ' + E.Message);
      LogResult.LogType := ltError;
      LogResult.MethodName := 'GetTicketList';
      LogResult.ExcepMsg := 'Ticket list query failed with: ' + E.Message;
      LogResult.DataStream := imageDM.qryTicketList.SQL.Text;
      WriteLog(LogResult);
    end;

  end;

  Memo1.Lines.Add('Running query qryTicketList.sql with parameters:');
  Memo1.Lines.Add('SyncDateFrom: ' + DateToStr(SyncDateFr.Date));
  Memo1.Lines.Add('SyncDateTo: ' + DateToStr(SyncDateTo.Date));
  Memo1.Lines.Add('ClientCode: ' + cbClient.Text);
  Memo1.Lines.Add('callCenter: ' + cbCallCenter.Text);
  Memo1.Lines.AddStrings(imageDM.qryTicketList.SQL);
end;

function TfrmMainFTP.getAppVer: String;
begin
  result := GetAppVersionStr;
end;

function TfrmMainFTP.downloadAttachmentsForTicket(ticketID: integer;tempTktPath:string): integer;
begin
  imageDM.qryAttachmentIDs.Parameters.ParamByName('ticketID').value := ticketID;
  imageDM.qryAttachmentIDs.open;
  result := 0;
  while not(imageDM.qryAttachmentIDs.eof) do
  begin
    myAttachmentData.AttachmentID  := imageDM.qryAttachmentIDs.FieldByName('Attachment_ID').AsInteger;
    myAttachmentData.LocalFilename := imageDM.qryAttachmentIDs.FieldByName('Filename').AsString;
    myAttachmentData.OrigFilename  := imageDM.qryAttachmentIDs.FieldByName('orig_filename').AsString;

    imageDM.DownloadAttachmentToFileAWS(myAttachmentData, tempTktPath+'\'+myAttachmentData.LocalFilename);
    imageDM.qryAttachmentIDs.next;
    result := result+1;
  end;
  imageDM.qryAttachmentIDs.close;
end;

procedure TfrmMainFTP.PopCallCenters;
begin
  with imageDM.qryCallCenter do
  begin
    Open;
    while not eof do
    begin
      cbCallCenter.Items.Add(FieldByName('call_center').AsString);
      Next;
    end;
  end;
//
//
  cbCallCenter.ItemIndex := cbCallCenter.Items.IndexOf(ActiveCallCenter);
  cbCallCenterSelect(nil);

  cbClient.ItemIndex := cbClient.Items.IndexOf(ActiveCallClient);

  imageDM.qryCallCenter.close;
end;

procedure TfrmMainFTP.PopClientList;
VAR
  Sender: TObject;
begin
  cbClient.Clear;
  with imageDM.qryClient do
  begin
    Parameters.ParamByName('callCenter').value := cbCallCenter.Text;
    Open;
    while not eof do
    begin
      cbClient.Items.Add(FieldByName('ClientCode').AsString);
      Next;
    end;
  end;
  imageDM.qryClient.close;
end;

procedure TfrmMainFTP.IdFTP1Status(ASender: TObject; const AStatus: TIdStatus;
const AStatusText: string);
begin
  lblConnected.Caption := AStatusText;
end;

procedure TfrmMainFTP.InitData;
var
  i: integer; // C:\Users\svc.utl.qmanager\AppData\Local\Temp
begin
  imageDM:= TimageDM.Create(self);
  If not connectToDB then
  begin
    Memo1.Lines.Add('Failed to connect to database.  exiting');
    application.Terminate;
  end;

  LocalAppDataFolder := GetKnownFolderPath(FOLDERID_LocalAppData);
  If TDirectory.Exists(LocalAppDataFolder + '\Temp\tempFtpExport') then
    TDirectory.Delete(LocalAppDataFolder + '\Temp\tempFtpExport', true);
  ForceDirectories(LocalAppDataFolder + '\Temp\tempFtpExport');
  LogResult.LogType := ltInfo;
  LogResult.MethodName := 'Cleaned up temp folder';
  WriteLog(LogResult);
  IniName := IncludeTrailingBackSlash(ExtractFileDir(application.ExeName)) +
    'eprCertusViewConn.ini';
  ProcessDate := Today -1;
  if ParamCount > 1 then
  begin
    cbCloseDone.Checked := true;
    for i := 1 to ParamCount do
      case i of
        1:
          ActiveCallCenter := paramStr(i);
        2:
          ActiveCallClient := paramStr(i);
        3:
          ProcessDate := Today + (StrToInt(paramStr(i)));
      end;

    SyncDateFr.Date := ProcessDate;
    SyncDateTo.Date := ProcessDate + 1;

    LogResult.LogType := ltInfo;
    LogResult.Status := 'Processed param arguments';
    LogResult.MethodName := 'ProcessDate: ' + DateToStr(ProcessDate);
    WriteLog(LogResult);
  end
  else
  begin
    SyncDateFr.Date := Today() - 1;
    SyncDateTo.Date := Today();
  end;
end;

procedure TfrmMainFTP.InitExportSettings1Click(Sender: TObject);
begin
  primeCbListBox;
end;

function TfrmMainFTP.connectToDB: Boolean;
begin
  imageDM.connectToDB;
end;

function TfrmMainFTP.ReadINI: Boolean;
var
  IniFile: TIniFile;
  Path: String;
begin
  try
    Path := ExtractFilePath(application.ExeName);
    IniFile := TIniFile.Create(Path + 'imageMover.ini');
    fHost := IniFile.ReadString('remoteFTP', 'Host', 'localhost');
    fPort := IniFile.ReadInteger('remoteFTP', 'Port', 21);
    fusername := IniFile.ReadString('remoteFTP', 'username', 'locatinginc');
    fpassword := IniFile.ReadString('remoteFTP', 'password', 'nwn2009!');
    fFTPmode := IniFile.ReadBool('remoteFTP', 'passive', false);
    DBType := IniFile.ReadString('remoteFTP', 'database', 'dbLocQM');
    FtpAttLog := IniFile.ReadString('remoteFTP', 'ftpAttLog', 'E:\QM\Logs');
    remFtpDir := IniFile.ReadString('remoteFTP', 'remDir', 'LocateCompletions');
    sendXML := IniFile.ReadBool('remoteFTP', 'sendXML', true);
    sendTSV := IniFile.ReadBool('remoteFTP', 'sendTSV', true);
    sendImages := IniFile.ReadBool('remoteFTP', 'sendImages', true);

    cbSendTSV.Checked := sendTSV;
    cbSendXML.Checked := sendXML;
    cbSendImages.Checked := sendImages;
  finally
    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ReadINI';
    LogResult.Status := 'IniFile processed';
    LogResult.DataStream := 'Host: ' + fHost + ' Port: ' + intToStr(fPort) +
      ' User: ' + fusername + ' Password: ' + fpassword + ' DB: ' + DBType;
    WriteLog(LogResult);
    FreeAndNil(IniFile);
  end;
end;

procedure TfrmMainFTP.ReadSettings1Click(Sender: TObject);
begin
  settingsList.ReadFromINI;
end;

procedure TfrmMainFTP.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;

const
  PRE_PEND = '[yyyy-mm-dd hh:nn:ss] ';

  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  if ActiveCallCenter <> '' then
    LogName := FtpAttLog + '\' + ActiveCallCenter + '_' + ActiveCallClient + '_'
      + FormatDateTime('yyyy-mm-dd', Date) + FILE_EXT
  else
    LogName := FtpAttLog + '\' + 'StartUpLog' + '_' +
      FormatDateTime('yyyy-mm-dd', Date) + FILE_EXT;
  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);
  if not(FileExists(LogName)) then
    WriteLn(myFile, 'FTP Att Version: ' + appVer);
  if LogResult.MethodName <> '' then
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  if LogResult.ExcepMsg <> '' then
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  if LogResult.Status <> '' then
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  if LogResult.DataStream <> '' then
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmMainFTP.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := paramStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function GetKnownFolderPath(const folder: KNOWNFOLDERID): string;
var
  Path: LPWSTR;
begin
  if SUCCEEDED(SHGetKnownFolderPath(folder, 0, 0, Path)) then
  begin
    try
      result := Path;
    finally
      CoTaskMemFree(Path);
    end;
  end
  else
    result := '';
end;

procedure TfrmMainFTP.ExportToXML(state: string; closeDate: TDate);
var  // calls Helper class
  xmlFile: TextFile;
  xmlStr, xmlName, sDate: string;

  ftpPath: string;
  sClient, sCallCenter: string;
const
  EXT = '.xml';
  TSV_REPORT = '%s-Closed Tickets (%s) %s %s';
  ROOT = 'Tickets';
  ITEM_TAG = 'Ticket';
begin
  closeDate := DateOf(imageDM.qryTicketList.FieldByName('filedate').AsDateTime);
  sDate := FormatDateTime('yyyy-mm-dd', closeDate);
  state := imageDM.qryTicketList.FieldByName('work_state').AsString;
  try
    sClient := cbClient.Text;
    sCallCenter := cbCallCenter.Text;

    xmlStr := settingsList.SaveToXML(ROOT,ITEM_TAG,imageDM.qryTicketList);

    xmlName := format(TSV_REPORT, [state, sClient, sDate, sCallCenter]);
    forcedirectories(LocalAppDataFolder + '\Temp\tempFtpExport\');
    ftpPath := LocalAppDataFolder + '\Temp\tempFtpExport\' + xmlName + EXT;
    AssignFile(xmlFile, ftpPath);
    Rewrite(xmlFile);
    WriteLn(xmlFile, xmlStr);
    CloseFile(xmlFile);
    if ((not IdFTP1.Connected) and (cbSendFTP.Checked)) then
    begin
      IdFTP1.Connect;
      IdFTP1.TransferType := ftASCII;
      IdFTP1.Put(ftpPath, xmlName + EXT);
    end;
  finally
    LogResult.LogType := ltInfo;
    LogResult.Status := 'Sent ' + xmlName + ' to ' + IdFTP1.Host;
    LogResult.MethodName := 'ExportXML';
    WriteLog(LogResult);
  end;
end;

procedure TfrmMainFTP.ExportTSV(state: string; closeDate: TDate);
var // consider passing log into Helper Class
  tsvName, sDate: string;
  ftpPath: string;
  sClient, sCallCenter: string;
const
  EXT = '.tsv';
  TSV_REPORT = '%s-Closed Tickets (%s) %s %s';
begin
  sClient := cbClient.Text;
  sCallCenter := cbCallCenter.Text;
  closeDate := DateOf(imageDM.qryTicketList.FieldByName('filedate').AsDateTime);
  sDate := FormatDateTime('yyyy-mm-dd', closeDate);
  state := imageDM.qryTicketList.FieldByName('work_state').AsString;
  try
    try
      tsvName := format(TSV_REPORT, [state, sClient, sDate, sCallCenter]);
      ForceDirectories(LocalAppDataFolder + '\Temp\tempFtpExport\');
      ftpPath := LocalAppDataFolder + '\Temp\tempFtpExport\' + tsvName + EXT;

      settingsList.SaveToTSV(imageDM.qryTicketList, ftpPath);

      if ((not IdFTP1.Connected) and (cbSendFTP.Checked)) then
      begin
        IdFTP1.Connect;
        IdFTP1.TransferType := ftASCII;
        IdFTP1.Put(ftpPath, tsvName + EXT);
      end;
    finally
      LogResult.LogType := ltInfo;
      LogResult.Status := 'Sent ' + tsvName + ' to ' + IdFTP1.Host;
      LogResult.MethodName := 'ExportTSV';
      WriteLog(LogResult);
    end;
  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.Status := 'Failed to Send ' + tsvName + ' to ' + IdFTP1.Host;
      LogResult.MethodName := 'ExportTSV';
      LogResult.ExcepMsg := E.Message;
      WriteLog(LogResult);
    end;
  end;
end;


//function TfrmMainFTP.DataSetToXML(DataSet: TDataSet): AnsiString;
//const
//  ROOT = 'tickets';
//var
//  i: integer;
//  function MakeTag(TagName, value: String): string;
//  begin
//    result := '<' + TagName + '>' + value + '</' + TagName + '>';
//  end;

//begin
//  result := '';
//  if (not DataSet.Active) or (DataSet.IsEmpty) then
//    Exit;
//  result := result + '<' + ROOT + '>';
//  DataSet.First;
//  while not DataSet.eof do
//  begin
//    result := result + '<ticket>';
//    for i := 0 to DataSet.Fields.Count - 1 do
//    begin
//      if settingsList.items.IndexOf(imageDM.qryTicketList.Fields[i].FieldName) = i
//      then
//        if settingsList.Checked[i] then
//        result := result + MakeTag(DataSet.Fields[i].DisplayName,DataSet.Fields[i].Text);
//    end;
//    result := result + '</ticket>';
//    DataSet.Next;
//  end;
//  result := result + '</' + ROOT + '>';
//end;

end.
