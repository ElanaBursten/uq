inherited imageDM: TimageDM
  OldCreateOrder = True
  Height = 384
  Width = 540
  object ADOConn: TADOConnection
    CommandTimeout = 300
    ConnectionString = 'FILE NAME=C:\Trunk1\ImageMoverAWS\adoConnQM.udl'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 256
    Top = 16
  end
  object qryTicketList: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    CommandTimeout = 300
    Parameters = <
      item
        Name = 'SyncDateFrom'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'SyncDateTo'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'ClientCode'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'callCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'declare'
      '  @SyncDateFrom      DATETIME,'
      '  @SyncDateTo        DATETIME,'
      '  @XmitDateFrom      DATETIME,'
      '  @XmitDateTo        DATETIME,'
      '  @ClientCode        VARCHAR(40),'
      '  @CallCenter        VARCHAR(30)'
      ''
      #9'set @SyncDateFrom      = :SyncDateFrom;'
      #9'set @SyncDateTo        = :SyncDateTo;'
      ''
      #9'set @ClientCode        = :ClientCode;'
      #9'set @CallCenter        = :CallCenter;'
      ''
      ''
      '   SELECT T.ticket_number,'
      #9#9'  T.ticket_id,'
      '          L.closed_date_only,'
      '          (Select min(arrival_date)'
      '             from ticket_version'
      '             where ticket_id = T.ticket_id) as FileDate,'
      '           L.closed_date,'
      '           L.client_code,'
      '           L.status,'
      '           L.modified_date,'
      '           L.closed,'
      '           L.short_name,'
      '           T.work_address_number,'
      '           T.work_address_number_2,'
      '           T.work_address_street,'
      '           T.work_state,'
      '           T.work_county,'
      '           T.work_city,'
      '           T.map_page,'
      '           T.transmit_date,'
      '           T.due_date,'
      #9#9'   L.FeetMarked'
      
        '    FROM   (SELECT CONVERT(DATETIME, CONVERT(VARCHAR(12), ls.sta' +
        'tus_date, 102),'
      '                   102) AS'
      '                   closed_date_only,'
      '                   Min(ls.status_date) AS closed_date,'
      '                   locate.client_code,'
      '                   ls.status,'
      
        '                   Min(ls.insert_date) AS  modified_date,-- sync' +
        ' date'
      '                   locate.closed,'
      '                   e.short_name,'
      '                   locate.ticket_id,'
      '                   locate.locate_id,'
      #9#9#9#9'   (first_unit_factor * locate.qty_marked) as FeetMarked'
      '            FROM   locate_status ls'
      
        '                   INNER JOIN locate ON ls.locate_id = locate.lo' +
        'cate_id'
      
        '                   INNER JOIN employee e ON locate.closed_by_id ' +
        '= e.emp_id'
      #9#9#9#9'   INNER JOIN client c on locate.client_id = c.client_id'
      
        #9#9#9#9'   INNER JOIN billing_unit_conversion b on b.unit_conversion' +
        '_id = c.unit_conversion_id'
      
        '            WHERE  ls.insert_date BETWEEN @SyncDateFrom AND @Syn' +
        'cDateTo'
      '                   AND ls.status <> '#39'-N'#39
      '                   AND locate.modified_date >= @SyncDateFrom'
      '                   AND locate.client_code = @ClientCode'
      
        '                   AND ( ls.status_date IN (SELECT Max(locate_st' +
        'atus.status_date)'
      '                                            FROM   locate_status'
      
        '                                            WHERE  locate_status' +
        '.locate_id = locate.locate_id)'
      #9#9#9#9'   )'
      
        '            GROUP  BY CONVERT(DATETIME, CONVERT(VARCHAR(12), ls.' +
        'status_date, 102), 102),'
      '                      locate.client_code,'
      '                      ls.status,'
      '                      locate.closed,'
      '                      e.short_name,'
      '                      locate.ticket_id,'
      '                      locate.locate_id,'
      #9#9#9#9#9'  (first_unit_factor * locate.qty_marked)'
      #9#9#9#9#9'  ) L'
      '           INNER JOIN ticket T ON T.ticket_id = L.ticket_id'
      '    WHERE T.ticket_format = @CallCenter'
      '    ORDER BY L.closed_date')
    Left = 24
    Top = 100
    object qryTicketListticket_number: TStringField
      FieldName = 'ticket_number'
    end
    object qryTicketListticket_id: TAutoIncField
      FieldName = 'ticket_id'
      ReadOnly = True
    end
    object qryTicketListclosed_date_only: TDateTimeField
      FieldName = 'closed_date_only'
      ReadOnly = True
    end
    object qryTicketListFileDate: TDateTimeField
      FieldName = 'FileDate'
      ReadOnly = True
    end
    object qryTicketListclosed_date: TDateTimeField
      FieldName = 'closed_date'
      ReadOnly = True
    end
    object qryTicketListclient_code: TStringField
      FieldName = 'client_code'
      Size = 10
    end
    object qryTicketListstatus: TStringField
      FieldName = 'status'
      Size = 5
    end
    object qryTicketListmodified_date: TDateTimeField
      FieldName = 'modified_date'
      ReadOnly = True
    end
    object qryTicketListclosed: TBooleanField
      FieldName = 'closed'
    end
    object qryTicketListwork_address_number: TStringField
      FieldName = 'work_address_number'
      Size = 10
    end
    object qryTicketListwork_address_number_2: TStringField
      FieldName = 'work_address_number_2'
      Size = 10
    end
    object qryTicketListwork_address_street: TStringField
      FieldName = 'work_address_street'
      Size = 60
    end
    object qryTicketListwork_state: TStringField
      FieldName = 'work_state'
      Size = 2
    end
    object qryTicketListwork_county: TStringField
      FieldName = 'work_county'
      Size = 40
    end
    object qryTicketListwork_city: TStringField
      FieldName = 'work_city'
      Size = 40
    end
    object qryTicketListmap_page: TStringField
      FieldName = 'map_page'
    end
    object qryTicketListtransmit_date: TDateTimeField
      FieldName = 'transmit_date'
    end
    object qryTicketListdue_date: TDateTimeField
      FieldName = 'due_date'
    end
    object qryTicketListFeetMarked: TIntegerField
      FieldName = 'FeetMarked'
      ReadOnly = True
    end
    object qryTicketListshort_name: TStringField
      FieldName = 'short_name'
      Size = 30
    end
  end
  object qryClient: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'CallCenter'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'SELECT distinct [oc_code] as ClientCode'
      '  FROM [dbo].[client]'
      '  where [call_center] = :CallCenter'
      '  and active = '#39'true'#39
      'order by  ClientCode ')
    Left = 128
    Top = 104
  end
  object qryCallCenter: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'SELECT distinct [call_center]'
      '  FROM [dbo].[client]'
      '  where active = '#39'true'#39
      'order by [call_center]')
    Left = 264
    Top = 120
  end
  object qryAPI_storage_credentials: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [id]'
      '      ,[storage_type]'
      '      ,[enc_access_key]'
      '      ,[enc_secret_key]'
      '      ,[unc_date_created]'
      '      ,[modified_date]'
      '      ,[unc_date_expired]'
      '      ,[api_storage_constants_id]'
      '      ,[description]'
      '  FROM [dbo].[api_storage_credentials]'
      '  where id = 1')
    Left = 304
    Top = 284
  end
  object qryAPI_storage_constants: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [id]'
      '      ,[host]'
      '      ,[rest_url]'
      '      ,[service]'
      '      ,[bucket]'
      '      ,[bucket_folder]'
      '      ,[region]'
      '      ,[algorithm]'
      '      ,[signed_headers]'
      '      ,[content_type]'
      '      ,[accepted_values]'
      '      ,[environment]'
      '      ,[description]'
      '      ,[unc_date_modified]'
      '      ,[modified_date]'
      '  FROM [dbo].[api_storage_constants]'
      '  where id = 1')
    Left = 96
    Top = 284
  end
  object qryAttachmentIDs: TADOQuery
    Connection = ADOConn
    CommandTimeout = 300
    Parameters = <
      item
        Name = 'ticketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select attachment_id, filename, orig_filename'
      'from attachment'
      'where foreign_id = :ticketID'
      'and foreign_type = 1')
    Left = 40
    Top = 172
  end
end
