osql -S %SERVER% -U %USERNAME% -P %PASSWORD% -n -Q "drop database %EASDB%"
if errorlevel 1 goto failed

osql -S %SERVER% -U %USERNAME% -P %PASSWORD% -n -Q "CREATE database %EASDB%"
if errorlevel 1 goto failed

REM create views in destination to reference source data
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view employee_list as select * from %SOURCEDB%..%SOURCEEMPTABLE%"
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view employee as select * from %SOURCEDB%..employee"
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view sync_log as select * from %SOURCEDB%..sync_log"
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view timesheet_entry as select * from %SOURCEDB%..timesheet_entry"
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view users as select * from %SOURCEDB%..users"
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view notes as select * from %SOURCEDB%..notes"
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view attachment as select * from %SOURCEDB%..attachment"
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view locate_status as select * from %SOURCEDB%..locate_status"
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view employment_spans_link as select * from %SOURCEDB%..%SOURCEEMPTABLE%_employment"
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view timesheet as select * from %SOURCEDB%..timesheet"
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "create view timesheet_detail as select * from %SOURCEDB%..timesheet_detail"

REM create & populate work tables in destination 
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -i extract.sql
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "exec eas_raw_data_copy '%STARTDATE%','%ENDDATE%'" 
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -Q "exec eas_data_extract '%STARTDATE%','%ENDDATE%'" 
osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -i add_indexes.sql

if errorlevel 1 goto failed
goto done

:failed
echo ***** FAILED ****

:done
