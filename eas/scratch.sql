-- This is a long set of useful queries from the process of getting this to work.

select * from base12_employeelist
where short_name like '%Cam%'


delete from base12_employeelist
where emp_id = 10799


select * from employee
where last_name in ( 'John', 'Johns')

insert into base12_employeelist
select * from base13_employeelist


insert into base12_employeelist
values (0, 'Jeremy Colson', 'Jeremy_Colson', NULL, 'JEREMY')



7312	11	1	NULL	68055	901.John Rodriguez	John


8816	11	NULL	NULL	664849	910.Samuel Johns	Samuel	John


insert into base12_employeelist
values (8816, 'Samuel Johns', 'Samuel_Johns', NULL, '664849')



select * from employee_list where short_name like '%wright%'




select work_date from base_workspan where eas_emp_id in (3682, 7505)
and work_start1 is not null
order by 1




select time_id, description, count(*) as N
 from timesheet_detail inner join reference on time_id=ref_id
 group by time_id, description
 order by 2

-- So I think we need to use these IDs:
-- 589, 19, 18 


select top 10 * from timesheet_detail


create view timesheet as select * from QM..timesheet
go
create view timesheet_detail as select * from QM..timesheet_detail
go


select top 10 * from timesheet
select top 10 * from timesheet_detail


select timesheet.*
into base_timesheet
from base_employeelist
inner join timesheet on timesheet.emp_id = base_employeelist.emp_id
where timesheet.end_date >= '2002-01-01'

select timesheet_detail.*
into base_timesheet_detail
from base_timesheet
inner join timesheet_detail on base_timesheet.timesheet_id = timesheet_detail.timesheet_id
where time_id in (589, 19, 18)


select top 10 * from base_timesheet

select min(end_date) from base_timesheet

-- Chris Braxton  row 4645, 8/23/2006, 219 locates?
select * from timesheet_entry where work_emp_id=7135 and work_date='8/23/2006'
select * from easdata where eas_emp_id=7135 and work_date='8/23/2006'

-- Reproduce the number:
  select count(*) 
    from base_locate_status l
     inner join easdata on eas_emp_id=7135 and easdata.work_date='8/23/2006'
    where l.statused_by = eas_emp_id
    and l.status_date between work_date and work_date_ending
    and not
      (l.status_date between first_work_start_grace and last_work_stop_grace or
       l.status_date between first_callout_start and last_callout_stop)


    and ((l.status_date < first_work_start_grace) and (first_work_start_grace > IsNull(first_callout_start, work_date))
     or l.status_date < first_callout_start and first_work_start_grace < first_callout_start
     or l.status_date between last_work_stop_grace and first_callout_start
     or first_work_start_grace > IsNull(first_callout_start, work_date) and l.status_date > last_callout_stop
     or first_work_start IS NULL and first_callout_start IS NULL
   )

-- Get the details:
  select l.status_date, easdata.first_work_start_grace, last_work_stop_grace, first_callout_start, last_callout_stop
    from base_locate_status l
     inner join easdata on eas_emp_id=7135 and easdata.work_date='8/23/2006'
    where l.statused_by = eas_emp_id
    and l.status_date between work_date and work_date_ending
    and ((l.status_date < first_work_start_grace) and (first_work_start_grace > IsNull(first_callout_start, work_date))
     or l.status_date < first_callout_start and first_work_start_grace < first_callout_start
     or l.status_date between last_work_stop_grace and first_callout_start
     or first_work_start_grace > IsNull(first_callout_start, work_date) and l.status_date > last_callout_stop
     or first_work_start IS NULL and first_callout_start IS NULL
   )


  select l.status_date, easdata.first_work_start_grace, last_work_stop_grace, first_callout_start, last_callout_stop
    from base_locate_status l
     inner join easdata on eas_emp_id=7135 and easdata.work_date='8/23/2006'
    where l.statused_by = eas_emp_id
    and l.status_date between work_date and work_date_ending
    and not
      (l.status_date between first_work_start_grace and last_work_stop_grace or
       l.status_date between first_callout_start and last_callout_stop)

((l.status_date < first_work_start_grace) and (first_work_start_grace > IsNull(first_callout_start, work_date))
     or l.status_date < first_callout_start and first_work_start_grace < first_callout_start
     or l.status_date between last_work_stop_grace and first_callout_start
     or first_work_start_grace > IsNull(first_callout_start, work_date) and l.status_date > last_callout_stop
     or first_work_start IS NULL and first_callout_start IS NULL
   )


-- Get the missing Colson on to the list:
  insert into base_workspan (eas_emp_id, emp_number, short_name, last_name, first_name, work_date, work_date_ending)
  select
    emp_id,
    emp_number,
    short_name,
    'lastname',
    'firstname',
    '2005-01-01',
    '2005-01-01'
  from base_employeelist
  where emp_id=0

select * from base_workspan where first_name like '%cols%'
select * from base_workspan where eas_emp_id=0

select * from base_employeelist where name2 like '%cols%'

select * from base_employeelist, base_workspan where eas_emp_id=emp_id and eas_emp_id=0

select * from easdata where first_name like '%cols%'

-- other experiment

select top 10 work_date, first_sync, last_sync, sync_span, old_timeentry_hours, sync_minus_hours, smh_plus, smh_minus
 from easdata
 where eas_emp_id=1854 and old_timeentry_hours>0
select count(*) from easdata where estatus='N' and after_hours_activity>0

select count(*) from easdata  -- 74K rows
select count(*) from easdata where work_day>0 or old_work_day>0 or work_but_not_emp_day>0   -- 18K rows

select count(*) from base_workspan



-- osql -E -s "," -Q "SELECT * FROM Northwind.dbo.Shippers" -o MyOutput.rpt
-- osql -E -S servername -d dbname -s "\t" -w 250 -Q "SELECT Column1, Column2,Column3 FROM viewname" -o TabDelimitedFileForExcel.txt
-- 


-- Gather the old-timesheet system working hours by day, for Mig Cruz (emp ID 4718)
-- Email to Gene F at UQ
-- Miguel_Cruz



if object_id('old_timesheet_hours_detail') is not null
  drop PROC old_timesheet_hours_detail
go

CREATE PROC old_timesheet_hours_detail ( @EmployeeID int )
as

declare @old_time table (
  emp_id int not null,
  date_worked datetime not null,
  work_hours decimal(5,2) not null,
  time_type int not null
)

insert into @old_time 
  select ts.emp_id, DateAdd(day, -6, ts.end_date) as date_worked, tsd.sun_hours, time_id
    from timesheet ts 
    join timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id 
    where ts.emp_id=@EmployeeID
  union select ts.emp_id, DateAdd(day, -5, ts.end_date) as date_worked, tsd.mon_hours, time_id
    from timesheet ts 
    join timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    where ts.emp_id=@EmployeeID
  union select ts.emp_id, DateAdd(day, -4, ts.end_date) as date_worked, tsd.tue_hours, time_id
    from timesheet ts 
    join timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    where ts.emp_id=@EmployeeID
  union select ts.emp_id, DateAdd(day, -3, ts.end_date) as date_worked, tsd.wed_hours, time_id
    from timesheet ts 
    join timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    where ts.emp_id=@EmployeeID
  union select ts.emp_id, DateAdd(day, -2, ts.end_date) as date_worked, tsd.thu_hours, time_id
    from timesheet ts 
    join timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    where ts.emp_id=@EmployeeID
  union select ts.emp_id, DateAdd(day, -1, ts.end_date) as date_worked,tsd.fri_hours, time_id
    from timesheet ts 
    join timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    where ts.emp_id=@EmployeeID
  union select ts.emp_id, ts.end_date as date_worked, tsd.sat_hours, time_id
    from timesheet ts 
    join timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    where ts.emp_id=@EmployeeID

delete from @old_time where work_hours is null or work_hours=0

select short_name, tm.emp_id, date_worked, description, work_hours
from @old_time tm
inner join reference on time_type=ref_id
inner join employee e on e.emp_id=tm.emp_id
where work_hours is not null
go

old_timesheet_hours_detail 4718

-- Investigate John_Newton sync span on , emp id 5037

select top 1 * from base_timesheet where emp_id=5037 and end_date >= '9/18/2003' order by end_date
timesheet_id = 83201

select * from base_timesheet_detail where timesheet_id = 83201
-- The timesheets look normal.



select
    first_sync = (select min(s.local_date) 
      from base_sync_log s 
      where s.emp_id = 5037 
      and s.sync_date between '9/18/2003' and '9/19/2003'
      and s.local_date between '9/18/2003' and '9/19/2003'),
    last_sync = (select max(s.local_date) 
      from base_sync_log s 
      where s.emp_id = 5037 
      and s.sync_date between '9/18/2003' and '9/19/2003'
      and s.local_date between '9/18/2003' and '9/19/2003')


select * from base_sync_log where local_date is null



select *
from base_Locate_status
where Statused_by=7135 and status_date between '11/27/2006' and '11/28/2006'
order by statused_by, status_date


--first_work_start_grace and friends are NULL for this date for this guy.

select 
  first_work_start, last_work_stop, first_callout_start, last_callout_stop,
  first_work_start_grace, last_work_stop_grace, first_callout_start_grace, last_callout_stop_grace
 from easdata where eas_emp_id=7135 and work_date='11/27/2006'



-


  select *,
    from base_locate_status l
  inner join easdata on eas_emp_id=7135 and work_date='11/27/2006'
    where l.statused_by = 7135
    and l.status_date between '11/27/2006' and '11/28/2006'
    and not
      (l.status_date between coalesce(first_work_start_grace, work_date) and coalesce(last_work_stop_grace, work_date) or
       l.status_date between coalesce(first_callout_start_grace, work_date) and coalesce(last_callout_stop_grace, work_date)) 



select case when 4 between 3 and 5 then 'true' else 'false' end as "resultcol"
select case when 4 between 5 and 3 then 'true' else 'false' end as "resultcol"



select * from easdata where eas_emp_id=9052 and work_date = '12/3/2005'

1900-01-01 .000	1900-01-01 .000


-- NOTES
  select n.*
    from base_notes n 
    inner join base_users u on u.uid = n.uid
    where u.emp_id = 9052
    and n.entry_date between '12/3/2005 13:00:00' and '12/4/2005 13:30:00'

--   ATTACHMENT
  select a.attach_date, *
    from base_attachment a 
    where a.attached_by = 9052
    and a.attach_date between '12/3/2005' and '12/4/2005'

--   LOCATE
  select l.statused_by, *
    from base_locate_status l
    where l.statused_by = 9052
    and l.status_date between '12/3/2005' and '12/4/2005'





local_date



select
    first_sync = (select min(s.local_date) 
      from base_sync_log s 
      where s.emp_id = 5037 
      and s.sync_date between '9/18/2003' and '9/19/2003'
      and s.local_date between '9/18/2003' and '9/19/2003'),
    last_sync = (select max(s.local_date) 
      from base_sync_log s 
      where s.emp_id = 5037 
      and s.sync_date between '9/18/2003' and '9/19/2003'
      and s.local_date between '9/18/2003' and '9/19/2003')


select * from base_sync_log where emp_id = 3812 and local_date between '11/14/2004' and '11/15/2004'


select min(4,5) as m



create table events (
  emp_id int not null,
  event_date datetime not null,
  event_date_time datetime not null,
  event_type varchar(10) not null,
  after_hours bit not null default 1
)

select * from events where emp_id=591 and event_date='11/4/2004'

-- Windi J, strange hours entry here:
select * from easdata where eas_emp_id=9516 and work_date='10/14/2006'

select count(*) from easdata where work_start1='1900-01-01 00:00:00'


update easdata set work_start1=NULL, work_stop1=NULL where work_start1='1900-01-01 00:00:00' and work_stop1='1900-01-01 00:00:00'
update easdata set work_start1=NULL, work_stop1=NULL where work_start1='1900-01-01 00:00:00' and work_stop1='1900-01-01 00:00:00'
update easdata set work_start2=NULL, work_stop2=NULL where work_start2='1900-01-01 00:00:00' and work_stop2='1900-01-01 00:00:00'
update easdata set work_start3=NULL, work_stop3=NULL where work_start3='1900-01-01 00:00:00' and work_stop3='1900-01-01 00:00:00'
update easdata set work_start4=NULL, work_stop4=NULL where work_start4='1900-01-01 00:00:00' and work_stop4='1900-01-01 00:00:00'
update easdata set work_start5=NULL, work_stop5=NULL where work_start5='1900-01-01 00:00:00' and work_stop5='1900-01-01 00:00:00'
update easdata set callout_start1=NULL, callout_stop1=NULL where callout_start1='1900-01-01 00:00:00' and callout_stop1='1900-01-01 00:00:00'
update easdata set callout_start2=NULL, callout_stop2=NULL where callout_start2='1900-01-01 00:00:00' and callout_stop2='1900-01-01 00:00:00'
update easdata set callout_start3=NULL, callout_stop3=NULL where callout_start3='1900-01-01 00:00:00' and callout_stop3='1900-01-01 00:00:00'
update easdata set callout_start4=NULL, callout_stop4=NULL where callout_start4='1900-01-01 00:00:00' and callout_stop4='1900-01-01 00:00:00'
update easdata set callout_start5=NULL, callout_stop5=NULL where callout_start5='1900-01-01 00:00:00' and callout_stop5='1900-01-01 00:00:00'
update easdata set callout_start6=NULL, callout_stop6=NULL where callout_start6='1900-01-01 00:00:00' and callout_stop6='1900-01-01 00:00:00'


dump tran eas15 with truncate_only



create table span (
	emp_id int not null,
	work_date datetime not null,
	start_date datetime not null,
	end_date datetime not null,
	minutes int
)


update span set minutes = datediff(mi, start_date, dateadd(s, 59, end_date))
 where minutes <> datediff(mi, start_date, dateadd(s, 59, end_date))

select * from span where emp_id=4371 and work_date='2006-03-13' order by 1,2,3


select top 200 * from easdata where (ao_span_minutes>0 or ao_singletons>0) and after_hours_activity=0 and no_hours_activity=0

select count(*), sum(ao_span_minutes) from easdata where (ao_span_minutes>0 or ao_singletons>0) and after_hours_activity=0 and no_hours_activity=0
select count(*), sum(ao_span_minutes) from easdata where (ao_span_minutes>0 or ao_singletons>0) and not (after_hours_activity=0 and no_hours_activity=0)

update easdata set ao_span_minutes=null, ao_singletons=null where (ao_span_minutes>0 or ao_singletons>0) and after_hours_activity=0 and no_hours_activity=0






select convert(varchar, insert_date, 102), count(distinct statused_by), count(*)
 from locate_status
-- where insert_date<'1/1/2006'
 group by convert(varchar, insert_date, 102)
 order by 1



select top 10 *
 from locate_status




select convert(varchar, work_date, 102), count(*)
 from timesheet_entry
 group by convert(varchar, work_date, 102)
 order by 1






select * from employee
where last_name in ( 'Norris', 'Seals')


insert into base12_employeelist
values (2476, 'Jeffery Seals', 'Jeffery_Seals', NULL, '995013')
insert into base12_employeelist
values (7299, 'Pamela Norris', 'Pamela_Norris', NULL, '65350')





select * from events where emp_id=591 and event_date='2004-11-18' order by event_date_time

select * from events where emp_id=591 and event_date='2004-11-09' order by event_date_time
select * from events where emp_id=591 and event_date='2004-11-09' order by event_date_time



select * from eas_analysis_employee_employment where first_last_name='Andre_Leigh'


select emp_id, event_date, event_date_time, event_date_time, 0
 from events
 where after_hours = 1
and emp_id=4493
and event_date between '2004-10-20' and '2004-10-21'
 group by emp_id, event_date, event_date_time
 order by emp_id, event_date, event_date_time

select * from span
where emp_id=4493
and work_date='2004-10-20'

select * from new_time
where emp_id=4493
and work_date='2004-10-20'



select * from base_employeelist 




if object_id('dbo.overlap_days') is not null
drop table overlap_days
go

create table overlap_days (
	name2 varchar(51) NOT NULL ,
	work_date datetime NOT NULL ,
	emp_id1 int NOT NULL ,
	emp_id2 int NOT NULL 
)
go

insert into overlap_days
select distinct e1.name2, nt1.work_date, nt1.emp_id as emp_id1, nt2.emp_id as emp_id2
from new_time nt1
 inner join base_employeelist e1 on nt1.emp_id=e1.emp_id
 inner join base_employeelist e2 on e2.emp_id > e1.emp_id
    and e2.name2=e1.name2
 inner join new_time nt2 on nt2.emp_id=e2.emp_id
    and nt2.work_date=nt1.work_date


select * from overlap_days



select ao_span_minutes from easdata where work_date='2004-10-20' and eas_emp_id=4493


-- select top 10 * from base_workspan
-- select top 10 * from easdata
-- select * from employment_spans
-- select count(*) from easdata


select * from easdata where eas_emp_id=6572 and work_date='2005-02-26'
select * from new_time where emp_id=6572 and work_date='2005-02-26'
select * from base_workspan where eas_emp_id=6572 and work_date='2005-02-26'

-- Harry Jackson  2004-06-08
select * from easdata where eas_emp_id=583 and work_date='2004-06-08'
select * from new_time where emp_id=583 and work_date='2004-06-08'
select * from base_workspan where eas_emp_id=583 and work_date='2004-06-08'


Edward_Norkett	All Emp IDs	2005-02-26 



select el2.emp_id, el1.emp_id
 from base_employeelist el1
  inner join base_employeelist el2
   on el1.name2=el2.name2
   and el2.emp_id>el1.emp_id



if object_id('dbo.emp_merge') is not null
drop table emp_merge
go

create table emp_merge (
  from_emp_id int not null,
  to_emp_id int not null
)
go

insert into emp_merge
select el2.emp_id, el1.emp_id
 from base_employeelist el1
  inner join base_employeelist el2
   on el1.name2=el2.name2
   and el2.emp_id>el1.emp_id
go

update old_time set emp_id = to_emp_id
 from emp_merge where emp_id = from_emp_id

delete from easdata

insert into easdata (first_name, eas_emp_id, work_date, work_date_ending)
  select el.name2, min(eas_emp_id), work_date, work_date_ending
    from base_workspan
     inner join base_employeelist el on el.emp_id = base_workspan.eas_emp_id
   group by el.name2, work_date, work_date_ending


print 'Merge events'
update events set emp_id = to_emp_id
 from emp_merge where emp_id = from_emp_id




base_notes

select * from new_time where emp_id=988 and work_date='2005-02-04'
select * from base_workspan where eas_emp_id=988 and work_date='2005-02-04'


select * from base_employeelist order by name2

select * from time_overlap






select top 10 * from events


 baseyear = datepart(yy,work_date)


select name2, datepart(yy,event_date) as year, count(*) as N_locates
 from events
 inner join base_employeelist be on events.emp_id=be.emp_id
 where event_type='locate'
 group by name2, datepart(yy,event_date)
 order by 1,2


name
date/time


select top 10 name2, status_date, status
 from base_locate_status ls
 left join base_employeelist el on ls.statused_by=el.emp_id
 order by 1,2



select count(*)
 from base_locate_status ls
 left join base_employeelist el on ls.statused_by=el.emp_id

select top 10 status_date, status from locate_status ls where ls.statused_by= order by 1,2



select * from eas_analysis_employee where short_name like '%Williams%'
insert into eas_analysis_employee
values (6451, 'James Williams', 'James_Williams', NULL, '23520')



Williams


select * from employee where last_name='Dawson'
select * from employee where last_name='Grissom'
select * from employee where last_name='Ireland'
select * from employee where last_name='Switala'



Dawson	 Donald Lee	61260
Grissom	 James	96996
Ireland	 Louie G.	98830(UQ)
Ireland	 Louie G.	09163
Switala	 Gregory	19835
Williams	 James	23520



