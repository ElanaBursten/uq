
-- Current behaviour:

select datediff(mi, '2005-05-05 07:10:00', '2005-05-05 07:11:00')
-- 1

select datediff(mi, '2005-05-05 07:10:00', '2005-05-05 07:11:59')
-- 1   it is truncating, not rounding

select datediff(mi, '2005-05-05 07:10:10', '2005-05-05 07:11:10')
-- 1

select datediff(mi, '2005-05-05 07:10:10', '2005-05-05 07:11:11')
-- 1   it's truncating to minutes then subtracting; not calculating in seconds then truncating




-- New Behaviour:

-- I don't like how that works.  Here is a different way.  It will
-- return the number of minutes, or fraction thereof, and never less than 1.

drop function minutes_between
go

create function minutes_between (@a as datetime, @b as datetime)
returns int
as
begin
declare @n int
select @n = datediff(s, @a, @b)
if @n <= 0
select @n=1
return (@n+59) / 60
end

select dbo.minutes_between( '2005-05-05 07:10:00', '2005-05-05 07:11:00')
-- 1

select dbo.minutes_between( '2005-05-05 07:10:00', '2005-05-05 07:11:59')
-- 2   it was in parts of 2 minutes

select dbo.minutes_between( '2005-05-05 07:10:10', '2005-05-05 07:11:10')
-- 1

select dbo.minutes_between( '2005-05-05 07:10:10', '2005-05-05 07:11:11')
-- 2   that partial second minute, makes it 2 minutes

select dbo.minutes_between( '2005-05-05 07:10:10', '2005-05-05 07:10:10')
-- 1   always return at least 1



-- Leigh on 12/14/2004:

-- Old way:
select datediff(mi, '2004-12-14 18:24:42', dateadd(s, 59, '2004-12-14 18:26:52'))
-- 3

-- New way:
select dbo.minutes_between( '2004-12-14 18:24:42', '2004-12-14 18:26:52')
-- 3






