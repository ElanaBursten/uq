mkdir result-%EASDB%
del result-%EASDB%\*.tsv

osql -S %SERVER% -b -U %USERNAME% -P %PASSWORD% -n -d %EASDB% -i summarize.sql
if errorlevel 1 goto failed

REM export the emp list and spans for easy reference:

bcp "select * from %EASDB%..base_employeelist order by name2" queryout result-%EASDB%\employee_list.tsv -S %SERVER% -U %USERNAME% -P %PASSWORD% -c -t\t
if errorlevel 1 goto failed

bcp "select * from %EASDB%..employment_spans order by first_last_name, span_start" queryout result-%EASDB%\employment_spans.tsv -S %SERVER% -U %USERNAME% -P %PASSWORD% -c -t\t
if errorlevel 1 goto failed

REM export the results of the calculations:

bcp "select * from %EASDB%..result_summary order by first_name" queryout result-%EASDB%\summary.tsv -S %SERVER% -U %USERNAME% -P %PASSWORD% -c -t\t
if errorlevel 1 goto failed

bcp "select * from %EASDB%..result_emp_year order by first_name, baseyear" queryout result-%EASDB%\emp_year.tsv -S %SERVER% -U %USERNAME% -P %PASSWORD% -c -t\t
if errorlevel 1 goto failed

bcp "select * from %EASDB%..result_emp_week order by first_name, weekending" queryout result-%EASDB%\emp_week.tsv -S %SERVER% -U %USERNAME% -P %PASSWORD% -c -t\t
if errorlevel 1 goto failed

bcp "select * from %EASDB%..result_detail order by first_name, work_date" queryout result-%EASDB%\detail.tsv -S %SERVER% -U %USERNAME% -P %PASSWORD% -c -t\t
if errorlevel 1 goto failed

bcp "select * from %EASDB%..result_employment order by name2" queryout result-%EASDB%\employment.tsv -S %SERVER% -U %USERNAME% -P %PASSWORD% -c -t\t
if errorlevel 1 goto failed

bcp "select * from %EASDB%..time_overlap order by emp_id, work_date" queryout result-%EASDB%\time_overlap.tsv -S %SERVER% -U %USERNAME% -P %PASSWORD% -c -t\t
if errorlevel 1 goto failed

goto done
:failed
echo ***** FAILED ****
:done
