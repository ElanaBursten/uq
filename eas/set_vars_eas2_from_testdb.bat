REM this is an example:
REM Make a copy that does what you need for the DB you want to run against.
REM Make up a new DB name as needed.

REM this is the server it's all located on
set SERVER=10.1.1.26
set USERNAME=10.1.1.26
set PASSWORD=doggy183

REM SOURCE is the QM DB we are hitting (UQ, LocInc);
REM the emp list table is stored there (and copied here by this script)
set SOURCEDB=TestDB
set SOURCEEMPTABLE=

REM this is where we will do the EAS work and leave the intermediate data.
set EASDB=EAS12

set STARTDATE=2003-05-05
set ENDDATE=2003-05-07
