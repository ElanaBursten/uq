
-- This is the SQL I used to set up the spans.

-- Do NOT run this as part of reprocessing.

-- IN THE SOURCE DB:
-- Create storage location
CREATE TABLE base12_employeelist_employment (
  first_last_name varchar(50) NOT NULL,
  span_type varchar(1) NOT NULL,
  span_start datetime not null,
  span_end datetime not null
)
GO

-- POPULATE with this first stab data from Kyle
insert into base12_employeelist_employment values ('Chad_Adamson','N','2002-12-30','2004-05-28')
insert into base12_employeelist_employment values ('Chad_Adamson','E','2004-05-29','2005-07-30')
insert into base12_employeelist_employment values ('Richard_Bell','N','2001-11-05','2005-03-04')
insert into base12_employeelist_employment values ('Richard_Bell','E','2005-03-05','2006-07-29')
insert into base12_employeelist_employment values ('Richard_Bell','N','2006-07-30','2007-01-02')
insert into base12_employeelist_employment values ('Chris_Braxton','N','2004-11-30','2006-12-31')
insert into base12_employeelist_employment values ('Tacey_Brigman','N','2003-05-22','2005-07-01')
insert into base12_employeelist_employment values ('Tacey_Brigman','E','2005-07-02','2006-01-06')
insert into base12_employeelist_employment values ('Michael_Byrum','N','2003-03-15','2004-04-10')
insert into base12_employeelist_employment values ('Michael_Byrum','N','2005-04-03','2005-12-30')
insert into base12_employeelist_employment values ('Russell_Cameron','N','2002-10-01','2006-08-25')
insert into base12_employeelist_employment values ('Richard_Campbell','N','2002-07-22','2003-08-02')
insert into base12_employeelist_employment values ('Linus_Charles','E','2003-04-14','2004-09-12')
insert into base12_employeelist_employment values ('Linus_Charles','N','2004-09-13','2006-04-10')
insert into base12_employeelist_employment values ('Jeremy_Colson','N','2003-06-09','2004-05-10')
insert into base12_employeelist_employment values ('Steve_Corcoran','N','2004-01-05','2004-01-31')
insert into base12_employeelist_employment values ('Joshua_Coxe','N','2003-04-14','2004-11-05')
insert into base12_employeelist_employment values ('Miguel_Cruz','N','2003-07-02','2003-12-30')
insert into base12_employeelist_employment values ('Travis_Elks','N','2002-04-03','2005-04-08')
insert into base12_employeelist_employment values ('George_Frison','N','2003-09-29','2004-12-15')
insert into base12_employeelist_employment values ('Merrick_Gardner','N','2003-04-21','2006-06-26')
insert into base12_employeelist_employment values ('Bobby_Granger','N','2003-08-25','2006-12-31')
insert into base12_employeelist_employment values ('Michael_Hillberry','N','2002-01-28','2006-12-01')
insert into base12_employeelist_employment values ('Chris_Humphreys','N','2004-01-15','2004-07-26')
insert into base12_employeelist_employment values ('Chris_Humphreys','E','2004-07-27','2005-12-30')
insert into base12_employeelist_employment values ('Chris_Humphreys','E','2006-01-02','2006-08-26')
insert into base12_employeelist_employment values ('Harry_Jackson','N','1999-10-01','2005-06-08')
insert into base12_employeelist_employment values ('Samuel_Johns','N','2005-03-07','2005-09-02')
insert into base12_employeelist_employment values ('Samuel_Johns','N','2005-09-06','2005-12-30')
insert into base12_employeelist_employment values ('Windi_Johnson','N','2005-12-27','2006-12-31')
insert into base12_employeelist_employment values ('Windi_Johnson','N','2005-10-17','2005-12-27')
insert into base12_employeelist_employment values ('Michael_Ledet','N','2005-02-28','2005-09-16')
insert into base12_employeelist_employment values ('Michael_Ledet','E','2005-09-17','2006-01-20')
insert into base12_employeelist_employment values ('Andre_Leigh','N','2005-11-27','2003-09-05')
insert into base12_employeelist_employment values ('Andre_Leigh','E','2003-09-06','2004-10-29')
insert into base12_employeelist_employment values ('Andre_Leigh','N','2004-10-30','2006-01-19')
insert into base12_employeelist_employment values ('Talya_Makus','N','2004-11-04','2005-03-19')
insert into base12_employeelist_employment values ('Talya_Makus','N','2006-04-10','2006-08-31')
insert into base12_employeelist_employment values ('Greg_McClain','N','2003-05-12','2005-04-01')
insert into base12_employeelist_employment values ('Steven_McDaniel','N','2004-01-15','2004-04-22')
insert into base12_employeelist_employment values ('William_Moskal','N','2000-08-14','2002-01-26')
insert into base12_employeelist_employment values ('William_Moskal','E','2002-01-26','2005-04-07')
insert into base12_employeelist_employment values ('John_Newton','N','2003-09-09','2006-04-14')
insert into base12_employeelist_employment values ('David_Nordick','N','2004-10-19','2006-04-14')
insert into base12_employeelist_employment values ('Edward_Norkett','N','2003-04-28','2006-01-13')
insert into base12_employeelist_employment values ('Ronald_Petroviak','N','2004-01-02','2004-03-05')
insert into base12_employeelist_employment values ('Steven_Plaga','N','2003-04-21','2004-11-18')
insert into base12_employeelist_employment values ('Steven_Plaga','N','2005-10-01','2006-12-31')
insert into base12_employeelist_employment values ('James_Roberts','N','2001-07-02','2005-05-12')
insert into base12_employeelist_employment values ('Martin_Rosario','N','2005-09-14','2006-05-26')
insert into base12_employeelist_employment values ('Josie_Scholten','N','2003-05-16','2005-05-07')
insert into base12_employeelist_employment values ('Chavis_Sibert','N','2004-03-08','2004-10-02')
insert into base12_employeelist_employment values ('Chavis_Sibert','E','2004-10-03','2005-06-29')
insert into base12_employeelist_employment values ('Carmelo_Signorelli','N','2005-09-14','2006-05-26')
insert into base12_employeelist_employment values ('Carl_Sims','N','2003-09-18','2004-11-12')
insert into base12_employeelist_employment values ('Carl_Sims','E','2004-11-13','2006-05-26')
insert into base12_employeelist_employment values ('Jackie_Smith','N','2004-01-30','2004-11-18')
insert into base12_employeelist_employment values ('Joe_Storey','N','2002-04-18','2005-04-08')
insert into base12_employeelist_employment values ('Ernest_Thompson','N','2002-08-26','2006-02-21')
insert into base12_employeelist_employment values ('Ruben_Vargas','N','2001-01-23','2005-01-23')
insert into base12_employeelist_employment values ('Ruben_Vargas','N','2005-08-08','2006-01-24')
insert into base12_employeelist_employment values ('Chris_Werner','N','2004-09-24','2005-12-30')
insert into base12_employeelist_employment values ('Jeremy_Whaley','N','2004-12-01','2006-05-11')
insert into base12_employeelist_employment values ('Mahogany_Williams','N','2006-04-25','2006-11-17')
insert into base12_employeelist_employment values ('Steven_Wright','N','2003-05-05','2005-12-30')
insert into base12_employeelist_employment values ('Jeff_Carlile','E','1999-12-01','2005-04-30')
insert into base12_employeelist_employment values ('Jeff_Carlile','E','2005-05-01','2005-09-10')
insert into base12_employeelist_employment values ('Jeff_Carlile','N','2005-09-11','2005-10-08')
insert into base12_employeelist_employment values ('Jeff_Carlile','E','2005-10-09','2005-11-28')
insert into base12_employeelist_employment values ('Daniel_Cook','N','2006-01-13','2007-02-12')
insert into base12_employeelist_employment values ('Dean_Pierce','N','2005-09-26','2006-12-31')
insert into base12_employeelist_employment values ('Gregory_Prior','N','1999-08-25','2006-12-31')
insert into base12_employeelist_employment values ('John_Rodriguez','N','2005-09-30','2006-12-31')
insert into base12_employeelist_employment values ('John_Rodriguez','N','2003-07-11','2006-06-05')

-- Then switch to the EAS DB, create the place for the data to live during processing:
CREATE TABLE employment_spans (
  first_last_name varchar(50) NOT NULL,
  span_type varchar(1) NOT NULL,
  span_start datetime not null,
  span_end datetime not null
)
GO

-- Link it with a view
create view employment_spans_link as select * from QM..base12_employeelist_employment

-- populate it locally
insert into employment_spans
select * from employment_spans_link


-- Verify every span matches an emp:     (should return no results)
select * from employment_spans where first_last_name not in
(select name2 from base_employeelist)
order by 1

-- and every emp has a span:    (should return no results)
select * from base_employeelist where name2 not in
(select first_last_name from employment_spans)
order by 1



-- Fix Greg Prior
update base12_employeelist_employment set first_last_name='Greg_Prior' where first_last_name='Gregory_Prior'
update employment_spans set first_last_name='Greg_Prior' where first_last_name='Gregory_Prior'
update base12_employeelist_employment set first_last_name='Greg_Prior' where first_last_name='Gregory_Prior'

-- Set the people who are still on, to last through mar 07
update base12_employeelist_employment set span_end='2007-03-31' where span_end='2006-12-31'

-- To set things up in the main DB, for current data analysis:

CREATE TABLE eas_analysis_employee (
	[emp_id] [int] NOT NULL ,
	[short_name] [varchar] (30) NOT NULL ,
	[name2] [varchar] (51) NOT NULL ,
	[state] [varchar] (2) NULL ,
	[emp_number] [varchar] (15) NULL 
)

CREATE TABLE eas_analysis_employee_employment (
  first_last_name varchar(50) NOT NULL,
  span_type varchar(1) NOT NULL,
  span_start datetime not null,
  span_end datetime not null
)

insert into eas_analysis_employee_employment values ('Andre_Leigh','E','2003-09-06','2004-10-29')
insert into eas_analysis_employee_employment values ('Andre_Leigh','N','2004-10-30','2006-01-19')
insert into eas_analysis_employee_employment values ('Andre_Leigh','N','2005-11-27','2003-09-05')
insert into eas_analysis_employee_employment values ('Bobby_Granger','N','2003-08-25','2007-03-31')
insert into eas_analysis_employee_employment values ('Carl_Sims','N','2003-09-18','2004-11-12')
insert into eas_analysis_employee_employment values ('Carl_Sims','E','2004-11-13','2006-05-26')
insert into eas_analysis_employee_employment values ('Carmelo_Signorelli','N','2005-09-14','2006-05-26')
insert into eas_analysis_employee_employment values ('Chad_Adamson','N','2002-12-30','2004-05-28')
insert into eas_analysis_employee_employment values ('Chad_Adamson','E','2004-05-29','2005-07-30')
insert into eas_analysis_employee_employment values ('Chavis_Sibert','N','2004-03-08','2004-10-02')
insert into eas_analysis_employee_employment values ('Chavis_Sibert','E','2004-10-03','2005-06-29')
insert into eas_analysis_employee_employment values ('Chris_Braxton','N','2004-11-30','2007-03-31')
insert into eas_analysis_employee_employment values ('Chris_Humphreys','N','2004-01-15','2004-07-26')
insert into eas_analysis_employee_employment values ('Chris_Humphreys','E','2004-07-27','2005-12-30')
insert into eas_analysis_employee_employment values ('Chris_Humphreys','E','2006-01-02','2006-08-26')
insert into eas_analysis_employee_employment values ('Chris_Werner','N','2004-09-24','2005-12-30')
insert into eas_analysis_employee_employment values ('Daniel_Cook','N','2006-01-13','2007-02-12')
insert into eas_analysis_employee_employment values ('David_Nordick','N','2004-10-19','2006-04-14')
insert into eas_analysis_employee_employment values ('Dean_Pierce','N','2005-09-26','2007-03-31')
insert into eas_analysis_employee_employment values ('Edward_Norkett','N','2003-04-28','2006-01-13')
insert into eas_analysis_employee_employment values ('Ernest_Thompson','N','2002-08-26','2006-02-21')
insert into eas_analysis_employee_employment values ('George_Frison','N','2003-09-29','2004-12-15')
insert into eas_analysis_employee_employment values ('Greg_McClain','N','2003-05-12','2005-04-01')
insert into eas_analysis_employee_employment values ('Greg_Prior','N','1999-08-25','2007-03-31')
insert into eas_analysis_employee_employment values ('Harry_Jackson','N','1999-10-01','2005-06-08')
insert into eas_analysis_employee_employment values ('Jackie_Smith','N','2004-01-30','2004-11-18')
insert into eas_analysis_employee_employment values ('James_Roberts','N','2001-07-02','2005-05-12')
insert into eas_analysis_employee_employment values ('Jeff_Carlile','E','1999-12-01','2005-04-30')
insert into eas_analysis_employee_employment values ('Jeff_Carlile','E','2005-05-01','2005-09-10')
insert into eas_analysis_employee_employment values ('Jeff_Carlile','N','2005-09-11','2005-10-08')
insert into eas_analysis_employee_employment values ('Jeff_Carlile','E','2005-10-09','2005-11-28')
insert into eas_analysis_employee_employment values ('Jeremy_Colson','N','2003-06-09','2004-05-10')
insert into eas_analysis_employee_employment values ('Jeremy_Whaley','N','2004-12-01','2006-05-11')
insert into eas_analysis_employee_employment values ('Joe_Storey','N','2002-04-18','2005-04-08')
insert into eas_analysis_employee_employment values ('John_Newton','N','2003-09-09','2006-04-14')
insert into eas_analysis_employee_employment values ('John_Rodriguez','N','2003-07-11','2006-06-05')
insert into eas_analysis_employee_employment values ('John_Rodriguez','N','2005-09-30','2007-03-31')
insert into eas_analysis_employee_employment values ('Joshua_Coxe','N','2003-04-14','2004-11-05')
insert into eas_analysis_employee_employment values ('Josie_Scholten','N','2003-05-16','2005-05-07')
insert into eas_analysis_employee_employment values ('Linus_Charles','E','2003-04-14','2004-09-12')
insert into eas_analysis_employee_employment values ('Linus_Charles','N','2004-09-13','2006-04-10')
insert into eas_analysis_employee_employment values ('Mahogany_Williams','N','2006-04-25','2006-11-17')
insert into eas_analysis_employee_employment values ('Martin_Rosario','N','2005-09-14','2006-05-26')
insert into eas_analysis_employee_employment values ('Merrick_Gardner','N','2003-04-21','2006-06-26')
insert into eas_analysis_employee_employment values ('Michael_Byrum','N','2003-03-15','2004-04-10')
insert into eas_analysis_employee_employment values ('Michael_Byrum','N','2005-04-03','2005-12-30')
insert into eas_analysis_employee_employment values ('Michael_Hillberry','N','2002-01-28','2006-12-01')
insert into eas_analysis_employee_employment values ('Michael_Ledet','N','2005-02-28','2005-09-16')
insert into eas_analysis_employee_employment values ('Michael_Ledet','E','2005-09-17','2006-01-20')
insert into eas_analysis_employee_employment values ('Miguel_Cruz','N','2003-07-02','2003-12-30')
insert into eas_analysis_employee_employment values ('Richard_Bell','N','2001-11-05','2005-03-04')
insert into eas_analysis_employee_employment values ('Richard_Bell','E','2005-03-05','2006-07-29')
insert into eas_analysis_employee_employment values ('Richard_Bell','N','2006-07-30','2007-01-02')
insert into eas_analysis_employee_employment values ('Richard_Campbell','N','2002-07-22','2003-08-02')
insert into eas_analysis_employee_employment values ('Ronald_Petroviak','N','2004-01-02','2004-03-05')
insert into eas_analysis_employee_employment values ('Ruben_Vargas','N','2001-01-23','2005-01-23')
insert into eas_analysis_employee_employment values ('Ruben_Vargas','N','2005-08-08','2006-01-24')
insert into eas_analysis_employee_employment values ('Russell_Cameron','N','2002-10-01','2006-08-25')
insert into eas_analysis_employee_employment values ('Samuel_Johns','N','2005-03-07','2005-09-02')
insert into eas_analysis_employee_employment values ('Samuel_Johns','N','2005-09-06','2005-12-30')
insert into eas_analysis_employee_employment values ('Steve_Corcoran','N','2004-01-05','2004-01-31')
insert into eas_analysis_employee_employment values ('Steven_McDaniel','N','2004-01-15','2004-04-22')
insert into eas_analysis_employee_employment values ('Steven_Plaga','N','2003-04-21','2004-11-18')
insert into eas_analysis_employee_employment values ('Steven_Plaga','N','2005-10-01','2007-03-31')
insert into eas_analysis_employee_employment values ('Steven_Wright','N','2003-05-05','2005-12-30')
insert into eas_analysis_employee_employment values ('Tacey_Brigman','N','2003-05-22','2005-07-01')
insert into eas_analysis_employee_employment values ('Tacey_Brigman','E','2005-07-02','2006-01-06')
insert into eas_analysis_employee_employment values ('Talya_Makus','N','2004-11-04','2005-03-19')
insert into eas_analysis_employee_employment values ('Talya_Makus','N','2006-04-10','2006-08-31')
insert into eas_analysis_employee_employment values ('Travis_Elks','N','2002-04-03','2005-04-08')
insert into eas_analysis_employee_employment values ('William_Moskal','N','2000-08-14','2002-01-26')
insert into eas_analysis_employee_employment values ('William_Moskal','E','2002-01-26','2005-04-07')
insert into eas_analysis_employee_employment values ('Windi_Johnson','N','2005-10-17','2005-12-27')
insert into eas_analysis_employee_employment values ('Windi_Johnson','N','2005-12-27','2007-03-31')





select * from base14_employeelist order by name2

select * from base14_employeelist_employment order by 1, 3


update base14_employeelist_employment
set span_start='2006-03-04'
where span_start='2006-03-03'
and first_last_name='Brian_Houk'

delete from base14_employeelist_employment

insert into base14_employeelist_employment values ('Brian_Houk','N','2002-02-14','2004-10-06')
insert into base14_employeelist_employment values ('Brian_Houk','N','2005-01-12','2050-12-31')
insert into base14_employeelist_employment values ('Charlie_Croft','N','2002-07-02','2050-12-31')
insert into base14_employeelist_employment values ('Christina_Myers','N','2002-01-16','2050-12-31')
insert into base14_employeelist_employment values ('Dale_Beckham','N','2006-05-23','2006-12-08')
insert into base14_employeelist_employment values ('Dale_Beckham','N','2007-02-05','2050-12-31')
insert into base14_employeelist_employment values ('Damon_Richey','N','2004-10-18','2006-10-13')
insert into base14_employeelist_employment values ('Damon_Richey','N','2006-10-14','2050-12-31')
insert into base14_employeelist_employment values ('James_Austin','N','2004-08-30','2050-12-31')
insert into base14_employeelist_employment values ('James_Moran','N','2003-01-13','2050-12-31')
insert into base14_employeelist_employment values ('Jeffrey_Hamrick','N','2004-02-03','2005-06-04')
insert into base14_employeelist_employment values ('Jeffrey_Hamrick','E','2005-06-05','2050-12-31')
insert into base14_employeelist_employment values ('Jeremy_Ross','N','2004-11-15','2050-12-31')
insert into base14_employeelist_employment values ('Joshua_Palczynsky','N','2006-07-17','2050-12-31')
insert into base14_employeelist_employment values ('Lorrelie_Tipton','N','2004-08-30','2050-12-31')
insert into base14_employeelist_employment values ('Luis_Olivero','N','2003-03-10','2050-12-31')
insert into base14_employeelist_employment values ('Matthew_Biddix','N','2006-03-21','2050-12-31')
insert into base14_employeelist_employment values ('Ramon_Maldonado','N','2003-09-08','2050-12-31')
insert into base14_employeelist_employment values ('Thomas_Hooker','N','2003-08-25','2050-12-31')


select * into base14_employeelist_employment_backup from base14_employeelist_employment




-- Verify every span matches an emp:     (should return no results)
select * from base14_employeelist_employment where first_last_name not in
(select name2 from base14_employeelist)
order by 1

-- and every emp has a span:    (should return no results)
select * from base14_employeelist where name2 not in
(select first_last_name from base14_employeelist_employment)
order by 1




insert into base12_employeelist_employment values ('Pamela_Norris','E','2003-04-27','2005-11-12')
insert into base12_employeelist_employment values ('Pamela_Norris','N','2005-11-13','2006-01-03')
insert into base12_employeelist_employment values ('Jeffery_Seals','N','2002-11-29','2003-09-25')
insert into base12_employeelist_employment values ('Jeffery_Seals','E','2003-09-26','2004-07-04')
insert into base12_employeelist_employment values ('Jeffery_Seals','N','2004-07-05','2005-05-25')



-- Verify every span matches an emp:     (should return no results)
select * from base12_employeelist_employment where first_last_name not in
(select name2 from base12_employeelist)
order by 1

-- and every emp has a span:    (should return no results)
select * from base12_employeelist where name2 not in
(select first_last_name from base12_employeelist_employment)
order by 1


-- Look for overlap

select top 10 * from employment_spans

select top 1000 t1.*, t2.*
 from employment_spans t1
 inner join employment_spans t2 on t1.first_last_name=t2.first_last_name
  and t2.span_start > t1.span_start
  and (t2.span_start > t1.span_start and t2.span_start < t1.span_end or
       t2.span_end > t1.span_start and t2.span_end < t1.span_end)


-- -----------------------------------------------------------------------------
-- Set up the def list

CREATE TABLE defendant_employeelist (
	[emp_id] [int] NOT NULL ,
	[short_name] [varchar] (30) NOT NULL ,
	[name2] [varchar] (51) NOT NULL ,
	[state] [varchar] (2) NULL ,
	[emp_number] [varchar] (15) NULL 
)
GO

insert into defendant_employeelist values (7416,'Brian Houk','Brian_Houk',NULL,'93866')
insert into defendant_employeelist values (2184,'Charlie Croft','Charlie_Croft',NULL,'94577')
insert into defendant_employeelist values (1801,'Christina Myers','Christina_Myers',NULL,'93686')
insert into defendant_employeelist values (10063,'Dale Beckham','Dale_Beckham',NULL,'98842')
insert into defendant_employeelist values (6928,'Damon Richey','Damon_Richey',NULL,'97097')
insert into defendant_employeelist values (6677,'James Austin','James_Austin',NULL,'97021')
insert into defendant_employeelist values (3656,'James Moran','James_Moran',NULL,'95130')
insert into defendant_employeelist values (5457,'Jeffrey Hamrick','Jeffrey_Hamrick',NULL,'96377')
insert into defendant_employeelist values (7100,'Jeremy Ross','Jeremy_Ross',NULL,'7531')
insert into defendant_employeelist values (10317,'Joshua Palczynsky','Joshua_Palczynsky',NULL,'98987')
insert into defendant_employeelist values (6684,'Lorrelie Tipton','Lorrelie_Tipton',NULL,'96989')
insert into defendant_employeelist values (4126,'Luis Olivero','Luis_Olivero',NULL,'95364')
insert into defendant_employeelist values (9805,'Matthew Biddix','Matthew_Biddix',NULL,'98647')
insert into defendant_employeelist values (6478,'Ramon Maldonado','Ramon_Maldonado',NULL,'13016')
insert into defendant_employeelist values (4927,'Thomas Hooker','Thomas_Hooker',NULL,'95979')

CREATE TABLE defendant_employeelist_employment (
  first_last_name varchar(50) NOT NULL,
  span_type varchar(1) NOT NULL,
  span_start datetime not null,
  span_end datetime not null
)
GO

delete from defendant_employeelist_employment

insert into defendant_employeelist_employment values ('Brian_Houk','N','2002-02-14','2004-10-06')
insert into defendant_employeelist_employment values ('Brian_Houk','N','2005-01-12','2050-12-31')
insert into defendant_employeelist_employment values ('Charlie_Croft','N','2002-07-02','2050-12-31')
insert into defendant_employeelist_employment values ('Christina_Myers','N','2002-01-16','2050-12-31')
insert into defendant_employeelist_employment values ('Dale_Beckham','N','2006-05-23','2006-12-08')
insert into defendant_employeelist_employment values ('Dale_Beckham','N','2007-02-05','2050-12-31')
insert into defendant_employeelist_employment values ('Damon_Richey','N','2004-10-18','2006-10-13')
insert into defendant_employeelist_employment values ('Damon_Richey','N','2006-10-14','2050-12-31')
insert into defendant_employeelist_employment values ('James_Austin','N','2004-08-30','2050-12-31')
insert into defendant_employeelist_employment values ('James_Moran','N','2003-01-13','2050-12-31')
insert into defendant_employeelist_employment values ('Jeffrey_Hamrick','N','2004-02-03','2005-06-04')
insert into defendant_employeelist_employment values ('Jeffrey_Hamrick','E','2005-06-05','2050-12-31')
insert into defendant_employeelist_employment values ('Jeremy_Ross','N','2004-11-15','2050-12-31')
insert into defendant_employeelist_employment values ('Joshua_Palczynsky','N','2006-07-17','2050-12-31')
insert into defendant_employeelist_employment values ('Lorrelie_Tipton','N','2004-08-30','2050-12-31')
insert into defendant_employeelist_employment values ('Luis_Olivero','N','2003-03-10','2050-12-31')
insert into defendant_employeelist_employment values ('Matthew_Biddix','N','2006-03-21','2050-12-31')
insert into defendant_employeelist_employment values ('Ramon_Maldonado','N','2003-09-08','2050-12-31')
insert into defendant_employeelist_employment values ('Thomas_Hooker','N','2003-08-25','2050-12-31')

-- Verify every span matches an emp:     (should return no results)
select * from defendant_employeelist_employment where first_last_name not in
(select name2 from defendant_employeelist)
order by 1

-- and every emp has a span:    (should return no results)
select * from defendant_employeelist where name2 not in
(select first_last_name from defendant_employeelist_employment)
order by 1



-- -----------------------------------------------------------------------------
-- Set up the plantiff list



insert into eas_analysis_employee
values (2476, 'Jeffery Seals', 'Jeffery_Seals', NULL, '995013')
insert into eas_analysis_employee
values (7299, 'Pamela Norris', 'Pamela_Norris', NULL, '65350')




delete from eas_analysis_employee_employment

insert into eas_analysis_employee_employment values ('Andre_Leigh','N','1995-11-27','2003-09-05')
insert into eas_analysis_employee_employment values ('Andre_Leigh','E','2003-09-06','2004-10-29')
insert into eas_analysis_employee_employment values ('Andre_Leigh','N','2004-10-30','2006-01-19')
insert into eas_analysis_employee_employment values ('Bobby_Granger','N','2003-08-25','2007-06-30')
insert into eas_analysis_employee_employment values ('Carl_Sims','N','2003-09-18','2004-11-12')
insert into eas_analysis_employee_employment values ('Carl_Sims','E','2004-11-13','2006-05-26')
insert into eas_analysis_employee_employment values ('Carmelo_Signorelli','N','2005-09-14','2006-05-26')
insert into eas_analysis_employee_employment values ('Chad_Adamson','N','2002-12-30','2004-05-28')
insert into eas_analysis_employee_employment values ('Chad_Adamson','E','2004-05-29','2005-07-30')
insert into eas_analysis_employee_employment values ('Chavis_Sibert','N','2004-03-08','2004-10-02')
insert into eas_analysis_employee_employment values ('Chavis_Sibert','E','2004-10-03','2005-06-29')
insert into eas_analysis_employee_employment values ('Chris_Braxton','N','2004-11-30','2007-06-30')
insert into eas_analysis_employee_employment values ('Chris_Humphreys','N','2004-01-15','2004-07-26')
insert into eas_analysis_employee_employment values ('Chris_Humphreys','E','2004-07-27','2005-12-30')
insert into eas_analysis_employee_employment values ('Chris_Humphreys','E','2006-01-02','2006-08-26')
insert into eas_analysis_employee_employment values ('Chris_Werner','N','2004-09-24','2005-12-30')
insert into eas_analysis_employee_employment values ('Daniel_Cook','N','2006-01-13','2007-02-12')
insert into eas_analysis_employee_employment values ('David_Nordick','N','2004-10-19','2006-04-14')
insert into eas_analysis_employee_employment values ('Dean_Pierce','N','2005-09-26','2007-06-30')
insert into eas_analysis_employee_employment values ('Edward_Norkett','N','2003-04-28','2006-01-13')
insert into eas_analysis_employee_employment values ('Ernest_Thompson','N','2002-08-26','2006-02-21')
insert into eas_analysis_employee_employment values ('George_Frison','N','2003-09-29','2004-12-15')
insert into eas_analysis_employee_employment values ('Greg_McClain','N','2003-05-12','2005-04-01')
insert into eas_analysis_employee_employment values ('Greg_Prior','N','1999-08-25','2007-06-30')
insert into eas_analysis_employee_employment values ('Harry_Jackson','N','1999-10-01','2005-06-08')
insert into eas_analysis_employee_employment values ('Jackie_Smith','N','2004-01-30','2004-11-18')
insert into eas_analysis_employee_employment values ('James_Roberts','N','2001-07-02','2005-05-12')
insert into eas_analysis_employee_employment values ('Jeff_Carlile','E','1999-12-01','2005-04-30')
insert into eas_analysis_employee_employment values ('Jeff_Carlile','E','2005-05-01','2005-09-10')
insert into eas_analysis_employee_employment values ('Jeff_Carlile','N','2005-09-11','2005-10-08')
insert into eas_analysis_employee_employment values ('Jeff_Carlile','E','2005-10-09','2005-11-28')
insert into eas_analysis_employee_employment values ('Jeremy_Colson','N','2003-06-09','2004-05-10')
insert into eas_analysis_employee_employment values ('Jeremy_Whaley','N','2004-12-01','2006-05-11')
insert into eas_analysis_employee_employment values ('Joe_Storey','N','2002-04-18','2005-04-08')
insert into eas_analysis_employee_employment values ('John_Newton','N','2003-09-09','2006-04-14')
insert into eas_analysis_employee_employment values ('John_Rodriguez','N','2003-07-11','2005-06-05')
insert into eas_analysis_employee_employment values ('John_Rodriguez','N','2005-09-30','2007-06-30')
insert into eas_analysis_employee_employment values ('Joshua_Coxe','N','2003-04-14','2004-11-05')
insert into eas_analysis_employee_employment values ('Josie_Scholten','N','2003-05-16','2005-05-07')
insert into eas_analysis_employee_employment values ('Linus_Charles','E','2003-04-14','2004-09-12')
insert into eas_analysis_employee_employment values ('Linus_Charles','N','2004-09-13','2006-04-10')
insert into eas_analysis_employee_employment values ('Mahogany_Williams','N','2006-04-25','2006-11-17')
insert into eas_analysis_employee_employment values ('Martin_Rosario','N','2005-09-14','2006-05-26')
insert into eas_analysis_employee_employment values ('Merrick_Gardner','N','2003-04-21','2006-06-26')
insert into eas_analysis_employee_employment values ('Michael_Byrum','N','2003-03-15','2004-04-10')
insert into eas_analysis_employee_employment values ('Michael_Byrum','N','2005-04-03','2005-12-30')
insert into eas_analysis_employee_employment values ('Michael_Hillberry','N','2002-01-28','2006-12-01')
insert into eas_analysis_employee_employment values ('Michael_Ledet','N','2005-02-28','2005-09-16')
insert into eas_analysis_employee_employment values ('Michael_Ledet','E','2005-09-17','2006-01-20')
insert into eas_analysis_employee_employment values ('Miguel_Cruz','N','2003-07-02','2003-12-30')
insert into eas_analysis_employee_employment values ('Richard_Bell','N','2001-11-05','2005-03-04')
insert into eas_analysis_employee_employment values ('Richard_Bell','E','2005-03-05','2006-07-29')
insert into eas_analysis_employee_employment values ('Richard_Bell','N','2006-07-30','2007-01-02')
insert into eas_analysis_employee_employment values ('Richard_Campbell','N','2002-07-22','2003-08-02')
insert into eas_analysis_employee_employment values ('Ronald_Petroviak','N','2004-01-02','2004-03-05')
insert into eas_analysis_employee_employment values ('Ruben_Vargas','N','2001-01-23','2005-01-23')
insert into eas_analysis_employee_employment values ('Ruben_Vargas','N','2005-08-08','2006-01-24')
insert into eas_analysis_employee_employment values ('Russell_Cameron','N','2002-10-01','2006-08-25')
insert into eas_analysis_employee_employment values ('Samuel_Johns','N','2005-03-07','2005-09-02')
insert into eas_analysis_employee_employment values ('Samuel_Johns','N','2005-09-06','2005-12-30')
insert into eas_analysis_employee_employment values ('Steve_Corcoran','N','2004-01-05','2004-01-31')
insert into eas_analysis_employee_employment values ('Steven_McDaniel','N','2004-01-15','2004-04-22')
insert into eas_analysis_employee_employment values ('Steven_Plaga','N','2003-04-21','2004-11-18')
insert into eas_analysis_employee_employment values ('Steven_Plaga','N','2005-10-01','2007-06-30')
insert into eas_analysis_employee_employment values ('Steven_Wright','N','2003-05-05','2005-12-30')
insert into eas_analysis_employee_employment values ('Tacey_Brigman','N','2003-05-22','2005-07-01')
insert into eas_analysis_employee_employment values ('Tacey_Brigman','E','2005-07-02','2006-01-06')
insert into eas_analysis_employee_employment values ('Talya_Makus','N','2004-11-04','2005-03-19')
insert into eas_analysis_employee_employment values ('Talya_Makus','N','2006-04-10','2006-08-31')
insert into eas_analysis_employee_employment values ('Travis_Elks','N','2002-04-03','2005-04-08')
insert into eas_analysis_employee_employment values ('William_Moskal','N','2000-08-14','2002-01-26')
insert into eas_analysis_employee_employment values ('William_Moskal','E','2002-01-26','2005-04-07')
insert into eas_analysis_employee_employment values ('Windi_Johnson','N','2005-10-17','2005-12-27')
insert into eas_analysis_employee_employment values ('Windi_Johnson','N','2005-12-27','2007-06-30')
insert into eas_analysis_employee_employment values ('Pamela_Norris','E','2003-04-27','2005-11-12')
insert into eas_analysis_employee_employment values ('Pamela_Norris','N','2005-11-13','2006-01-03')
insert into eas_analysis_employee_employment values ('Jeffery_Seals','N','2002-11-29','2003-09-25')
insert into eas_analysis_employee_employment values ('Jeffery_Seals','E','2003-09-26','2004-07-04')
insert into eas_analysis_employee_employment values ('Jeffery_Seals','N','2004-07-05','2005-05-25')



-- Verify every span matches an emp:     (should return no results)
select * from eas_analysis_employee_employment where first_last_name not in
(select name2 from eas_analysis_employee)
order by 1

-- and every emp has a span:    (should return no results)
select * from eas_analysis_employee where name2 not in
(select first_last_name from eas_analysis_employee_employment)
order by 1


select * from eas_analysis_employee

select * from eas_analysis_employee_employment



select * from eas_analysis_employee where name2 like '%will%'
delete from eas_analysis_employee where emp_id=324


insert into eas_analysis_employee_employment values ('James_Williams','N','09/05/2003','07/16/2004')
insert into eas_analysis_employee_employment values ('James_Williams','N','07/23/2004','02/25/2005')
insert into eas_analysis_employee_employment values ('James_Williams','N','11/17/2006','12/31/2006')




Hire date - 09/05/2003

Term date - 07/16/2004

 

Utiliquest / TX (ID# 6451)

Hire date - 07/23/2004

Term date - 02/25/2005

Hire date - 11/17/2006

Term date - 12/31/2006




