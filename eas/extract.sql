-- EAS Data Analyis and Summary
-- Kyle Cordes and Joe Apperson
-- Oasis Digital Solutions Inc.

-- Started with queries from Adam Portal at UtiliQuest,
-- Rewrote more or less entirely since then.

-- ---------------------------------------------


-- TODO: better table names

/* clean things out for manuals runs:

drop table base_employeelist
drop table base_employee_nonexempt_range
drop table base_workspan

drop table base_users
drop table base_Notes
drop table base_attachment
drop table base_timesheet_entry
drop table base_Sync_log
drop table base_Locate_status
*/


/* employees to analyze */
CREATE TABLE base_employeelist (
  emp_id int NOT NULL PRIMARY KEY,
  short_name varchar (30) NULL,
  name2 varchar (51) NULL,
  state varchar (2) NULL,
  emp_number varchar (15) NULL, 
  first_new_tse_date datetime NULL
) 
GO

/* work table for activity during breaks. 1 row for each emp & day */
CREATE TABLE base_workspan (
	-- Gathered from time entry data:
	eas_emp_id int NOT NULL ,
	emp_number varchar (15)  NOT NULL ,
	short_name varchar (60)  NOT NULL ,
	last_name varchar (60)  NOT NULL ,
	first_name varchar (60)  NOT NULL ,
	work_date datetime NOT NULL ,
	work_date_ending datetime NOT NULL ,  -- 23:59:59 later

	work_start1 datetime NULL ,	-- Stripped of times, all up front
	work_stop1 datetime NULL ,
	work_start2 datetime NULL ,
	work_stop2 datetime NULL ,
	work_start3 datetime NULL ,
	work_stop3 datetime NULL ,
	work_start4 datetime NULL ,
	work_stop4 datetime NULL ,
	work_start5 datetime NULL ,
	work_stop5 datetime NULL ,
	callout_start1 datetime NULL ,
	callout_stop1 datetime NULL ,
	callout_start2 datetime NULL ,
	callout_stop2 datetime NULL ,
	callout_start3 datetime NULL ,
	callout_stop3 datetime NULL ,
	callout_start4 datetime NULL ,
	callout_stop4 datetime NULL ,
	callout_start5 datetime NULL ,
	callout_stop5 datetime NULL ,
	callout_start6 datetime NULL ,
	callout_stop6 datetime NULL ,
)
GO
create clustered index base_idx_001 on base_workspan (eas_emp_id, work_date)
GO

CREATE TABLE employment_spans (
  first_last_name varchar(50) NOT NULL,
  span_type varchar(1) NOT NULL,
  span_start datetime not null,
  span_end datetime not null
)
GO

/*
-- Look over in the main DB to see our target size:
select * from employee_list
*/

-- Functions used later, called from batch file which has data we won't have here (like date span)

if object_id('dbo.DateTimeToTime') is not null
  drop function dbo.DateTimeToTime
go

create function DateTimeToTime(@InDate datetime) returns datetime
as begin
  declare @Result datetime

  if @InDate is null
    set @Result = null
  else
    set @Result = convert(datetime, convert(varchar(8), @InDate, 108))

  return @Result
end
go


-- Populate base_workspan table with data from source
if object_id('dbo.eas_raw_data_copy') is not null
  drop PROC dbo.eas_raw_data_copy
go

CREATE PROC eas_raw_data_copy (
  @StartDate datetime,		-- Dates get passed in from the batch file
  @EndDate datetime
)
as

delete from base_employeelist

-- copy list of employees to process into working db
insert into base_employeelist (emp_id, short_name, name2, state, emp_number) 
  select emp_id, short_name, name2, state, emp_number from employee_list
  
update base_employeelist
  set first_new_tse_date = (select min(tse.work_date) from timesheet_entry tse where tse.work_emp_id = emp_id)

delete from employment_spans

insert into employment_spans
select * from employment_spans_link

-- TODO: only get the fields we care about, which would be smaller and faster.

-- (To add users) - 
select users.*
into base_users 
from base_employeelist
join users on users.emp_id = base_employeelist.emp_id 

select count(*) from base_users

-- (To add notes) - 
select notes.*
into base_Notes 
from base_employeelist
join Users on users.emp_id = base_employeelist.emp_id 
join notes on notes.uid = users.uid
where notes.entry_date between @StartDate and DateAdd(day, 1, @EndDate)
order by notes.uid, notes.entry_date

select count(*) from base_notes

-- (To add attachment) - 
select attachment.*
into base_attachment
from base_employeelist
join attachment on attachment.attached_By = base_employeelist.emp_id
where attachment.attach_date between @StartDate and DateAdd(day, 1, @EndDate)
order by attached_by, attach_date

select count(*) from base_attachment

-- (To add timesheet_entry) - 
select timesheet_entry.*
into base_timesheet_entry
from base_employeelist
join timesheet_entry on timesheet_entry.Work_Emp_id = base_employeelist.emp_id
where timesheet_entry.work_date between @StartDate and DateAdd(day, 1, @EndDate)

select count(*) from base_timesheet_entry

-- (To add Sync_log) - 
select Sync_log.*
into base_Sync_log
from base_employeelist
join Sync_log on Sync_log.Emp_id = base_employeelist.emp_id
where Sync_log.sync_date between @StartDate and DateAdd(day, 1, @EndDate)

select count(*) from base_sync_log

-- (To add Locate_status) - 
select Locate_status.*
into base_Locate_status
from base_employeelist
join locate_status on Locate_status.Statused_by = base_employeelist.emp_id
where locate_status.status_date between @StartDate and DateAdd(day, 1, @EndDate)
order by statused_by, status_date

select count(*) from base_locate_Status

-- Old timesheet system
select timesheet.*
into base_timesheet
from base_employeelist
inner join timesheet on timesheet.emp_id = base_employeelist.emp_id
where timesheet.end_date >= @StartDate

select timesheet_detail.*
into base_timesheet_detail
from base_timesheet
inner join timesheet_detail on base_timesheet.timesheet_id = timesheet_detail.timesheet_id
where time_id in (589, 19, 18)

GO


-- Populate base_workspan table with data from source
if object_id('dbo.eas_data_extract') is not null
  drop PROC dbo.eas_data_extract
go

CREATE PROC eas_data_extract (
  @StartDate datetime,		-- Dates get passed in from the batch file
  @EndDate datetime
)
as
  declare @dates table (
    work_date datetime NULL
  )

  -- TODO: Is this really needed?
  -- STUB IN A FULL PERIOD OF RECORDS
  declare @WorkDate datetime
  set @WorkDate = @StartDate
  while @WorkDate <= @EndDate
  begin
    insert into @dates values (@WorkDate)
    set @WorkDate = DateAdd(day, 1, @WorkDate)
  end

  delete from base_workspan

  -- ready to populate work table
  insert into base_workspan
  select
    base_employeelist.emp_id,
    COALESCE(e.emp_number, 'NONE'),
    COALESCE(e.short_name, name2),
    COALESCE(e.last_name, 'last'),
    COALESCE(e.first_name, 'first'),
    d.work_date,
    DateAdd(second, -1, (DateAdd(day, 1, d.work_date))),  -- 23:59:59 of that date.

    dbo.DateTimeToTime(work_start1),
    dbo.DateTimeToTime(work_stop1),
    dbo.DateTimeToTime(work_start2),
    dbo.DateTimeToTime(work_stop2),
    dbo.DateTimeToTime(work_start3),
    dbo.DateTimeToTime(work_stop3),
    dbo.DateTimeToTime(work_start4),
    dbo.DateTimeToTime(work_stop4),
    dbo.DateTimeToTime(work_start5),
    dbo.DateTimeToTime(work_stop5),
    dbo.DateTimeToTime(callout_start1),
    dbo.DateTimeToTime(callout_stop1),
    dbo.DateTimeToTime(callout_start2),
    dbo.DateTimeToTime(callout_stop2),
    dbo.DateTimeToTime(callout_start3),
    dbo.DateTimeToTime(callout_stop3),
    dbo.DateTimeToTime(callout_start4),
    dbo.DateTimeToTime(callout_stop4),
    dbo.DateTimeToTime(callout_start5),
    dbo.DateTimeToTime(callout_stop5),
    dbo.DateTimeToTime(callout_start6),
    dbo.DateTimeToTime(callout_stop6)
  from base_employeelist
  inner join @dates d on 1=1   -- Cartesian product
  left join employee e on base_employeelist.emp_id = e.emp_id
  left join timesheet_entry tse on tse.work_emp_id=e.emp_id
    and tse.status in ('ACTIVE', 'SUBMIT')
    and tse.work_date = d.work_date
  -- we want all the rows, to populate with other data, regardless of time entry
GO


/*
It's important to test these for 2004-04 and later, that is where the timesheet data is available.

exec eas_raw_data_copy '3/1/05','3/15/05'    -- 1:30 to run, figure 3 minutes per month

-- Enough to get more than one year span:
exec eas_raw_data_copy '7/1/04','1/31/05'

-- The whole data requested by the users: 
exec eas_raw_data_copy '9/11/03','12/31/06' 

exec eas_data_extract '3/1/05','3/15/05'  -- 3 seconds
exec eas_data_extract '7/1/04','1/31/05'   -- 26 seconds, enough data for plenty of dev/test.
exec eas_data_extract '9/11/03','12/31/06'
*/
