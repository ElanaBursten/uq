-- EAS Data Analyis and Summary
-- Kyle Cordes and Joe Apperson
-- Oasis Digital Solutions Inc.

-- Started with queries from Adam Portal at UtiliQuest,
-- Rewrote more or less entirely since then.

-- ---------------------------------------------

print 'Starting summarize process'

UPDATE STATISTICS base_attachment
UPDATE STATISTICS base_locate_status
UPDATE STATISTICS base_notes
go

print '****************************************** PREP MERGE TABLES'

if object_id('dbo.emp_merge') is not null
drop table emp_merge
go

create table emp_merge (
  from_emp_id int not null primary key,
  to_emp_id int not null
)
go

insert into emp_merge
select el2.emp_id as from_emp_id, el1.emp_id as to_emp_id
 from base_employeelist el1
  inner join base_employeelist el2
   on el1.name2=el2.name2
   and el2.emp_id>el1.emp_id
   and not exists (select * from base_employeelist el3
                         where el3.name2=el1.name2
                          and el3.emp_id<el1.emp_id)
 order by 1
go

if object_id('dbo.emp_join') is not null
drop table emp_join
go

create table emp_join (
  orig_emp_id int not null primary key,
  to_emp_id int not null
)
go

insert into emp_join
select el1.emp_id as orig_emp_id, 
   (select min(emp_id) from base_employeelist el3 where el3.name2=el1.name2) as to_emp_id
 from base_employeelist el1
 order by 1
go

create index emp_join_to on emp_join(to_emp_id)
go

print '****************************************** OLD TIME ENTRY HOURS'

if object_id('dbo.old_time') is not null
drop table old_time
go

create table old_time (
  emp_id int not null,
  date_worked datetime not null,
  work_hours decimal(5,2) not null 
)
go

-- Untangle the daily data from the weekly storage format of the old system
insert into old_time 
  select ts.emp_id, DateAdd(day, -6, ts.end_date) as date_worked, sum(Coalesce(tsd.sun_hours, 0))
    from base_timesheet ts 
    join base_timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id 
    group by emp_id, DateAdd(day, -6, ts.end_date)
  union select ts.emp_id, DateAdd(day, -5, ts.end_date) as date_worked, sum(Coalesce(tsd.mon_hours, 0))
    from base_timesheet ts 
    join base_timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    group by emp_id, DateAdd(day, -5, ts.end_date) 
  union select ts.emp_id, DateAdd(day, -4, ts.end_date) as date_worked, sum(Coalesce(tsd.tue_hours, 0))
    from base_timesheet ts 
    join base_timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    group by emp_id, DateAdd(day, -4, ts.end_date) 
  union select ts.emp_id, DateAdd(day, -3, ts.end_date) as date_worked, sum(Coalesce(tsd.wed_hours, 0))
    from base_timesheet ts 
    join base_timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    group by emp_id, DateAdd(day, -3, ts.end_date) 
  union select ts.emp_id, DateAdd(day, -2, ts.end_date) as date_worked, sum(Coalesce(tsd.thu_hours, 0))
    from base_timesheet ts 
    join base_timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    group by emp_id, DateAdd(day, -2, ts.end_date) 
  union select ts.emp_id, DateAdd(day, -1, ts.end_date) as date_worked, sum(Coalesce(tsd.fri_hours, 0))
    from base_timesheet ts 
    join base_timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    group by emp_id, DateAdd(day, -1, ts.end_date) 
  union select ts.emp_id, ts.end_date as date_worked, sum(Coalesce(tsd.sat_hours, 0))
    from base_timesheet ts 
    join base_timesheet_detail tsd on ts.timesheet_id = tsd.timesheet_id
    group by emp_id, end_date

delete from old_time where work_hours=0

print 'Merge old_time'
update old_time set emp_id = to_emp_id
 from emp_merge where emp_id = from_emp_id
go

print '****************************************** NEW TIME ENTRY HOURS'

if object_id('dbo.new_time') is not null
drop table new_time
go

create table new_time (
  emp_id int not null,
  work_date datetime not null,
  span_id varchar(15) not null,
  start datetime null,
  stop datetime null,
  start_grace datetime null,
  stop_grace datetime null
)
go

insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'work_start1', work_start1, work_stop1 from base_workspan where work_start1 is not null or work_stop1 is not null
insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'work_start2', work_start2, work_stop2 from base_workspan where work_start2 is not null or work_stop2 is not null
insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'work_start3', work_start3, work_stop3 from base_workspan where work_start3 is not null or work_stop3 is not null
insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'work_start4', work_start4, work_stop4 from base_workspan where work_start4 is not null or work_stop4 is not null
insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'work_start5', work_start5, work_stop5 from base_workspan where work_start5 is not null or work_stop5 is not null
insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'callout_start1', callout_start1, callout_stop1 from base_workspan where callout_start1 is not null or callout_stop1 is not null
insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'callout_start2', callout_start2, callout_stop2 from base_workspan where callout_start2 is not null or callout_stop2 is not null
insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'callout_start3', callout_start3, callout_stop3 from base_workspan where callout_start3 is not null or callout_stop3 is not null
insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'callout_start4', callout_start4, callout_stop4 from base_workspan where callout_start4 is not null or callout_stop4 is not null
insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'callout_start5', callout_start5, callout_stop5 from base_workspan where callout_start5 is not null or callout_stop5 is not null
insert into new_time (emp_id, work_date, span_id, start, stop) select eas_emp_id, work_date, 'callout_start6', callout_start6, callout_stop6 from base_workspan where callout_start6 is not null or callout_stop6 is not null

create clustered index new_time001 on new_time(emp_id, work_date)
GO

-- There are 191 partial entries to ignore:
delete from new_time where start is null or stop is null

-- Clear out 674 bogus empty time spans:
delete from new_time where start='1900-01-01 00:00:00' or stop='1900-01-01 00:00:00'

if object_id('dbo.time_overlap') is not null
drop table time_overlap
go

-- This finds 8 cases of overlap (within the same emp_id):
select el.name2,
       t1.emp_id, t1.work_date, t2.span_id as deleted_span_id, t2.start as deleted_start, t2.stop as deleted_stop,
                                t1.span_id as kept_span_id, t1.start as kept_start, t1.stop as kept_stop
 into time_overlap
 from new_time t1
 inner join new_time t2 on t1.work_date=t2.work_date and t1.emp_id=t2.emp_id
  and t1.span_id <> t2.span_id
  and (t2.start > t1.start and t2.start < t1.stop or
       t2.stop > t1.start and t2.stop < t1.stop)
  and (t2.start > t1.start or t2.start = t1.start and t2.stop < t1.stop)
 inner join base_employeelist el on el.emp_id = t1.emp_id

-- This deletes them:
delete from t2
 from new_time t1
 inner join new_time t2 on t1.work_date=t2.work_date and t1.emp_id=t2.emp_id
  and t1.span_id <> t2.span_id
  and (t2.start > t1.start and t2.start < t1.stop or
       t2.stop > t1.start and t2.stop < t1.stop)
  and (t2.start > t1.start or t2.start = t1.start and t2.stop < t1.stop)

-- ****************************************** Grace period data for After Hours and Lunch

update new_time set
  start_grace = DateAdd(mi, -5, work_date + start),
  stop_grace = DateAdd(mi, 5, work_date + stop)

-- The lunch grace period is only 1 minute
update new_time set
  start_grace = DateAdd(mi, -1, work_date + start)
   where span_id='work_start2'
   and exists (select * from new_time t2
      where t2.emp_id=new_time.emp_id
        and t2.work_date = new_time.work_date
        and t2.span_id='work_start1')

update new_time set
  stop_grace = DateAdd(mi, 1, work_date + stop)
   where span_id='work_start1'
   and exists (select * from new_time t2
      where t2.emp_id=new_time.emp_id
        and t2.work_date = new_time.work_date
        and t2.span_id='work_start2')
go

print '***************************************** emp_id Time OVERLAPS'
-- Figure out what days have time entry from both emp_ids for a person:

if object_id('dbo.overlap_days') is not null
drop table overlap_days
go

create table overlap_days (
	name2 varchar(51) NOT NULL ,
	work_date datetime NOT NULL ,
	emp_id1 int NOT NULL ,
	emp_id2 int NOT NULL 
)
go

insert into overlap_days
select distinct e1.name2, nt1.work_date, nt1.emp_id as emp_id1, nt2.emp_id as emp_id2
from new_time nt1
 inner join base_employeelist e1 on nt1.emp_id=e1.emp_id
 inner join base_employeelist e2 on e2.emp_id > e1.emp_id
    and e2.name2=e1.name2
 inner join new_time nt2 on nt2.emp_id=e2.emp_id
    and nt2.work_date=nt1.work_date

-- Hold on to those for later, we will discard these days from the analysis.

print 'Merge new_time'
update new_time set emp_id = to_emp_id
 from emp_merge where emp_id = from_emp_id

print '***************************************** Gather all activity detail'

if object_id('dbo.events') is not null
drop table events
go

create table events (
  emp_id int not null,
  event_date datetime not null,
  event_date_time datetime not null,
  event_type varchar(10) not null,
  event_id int not null,
  after_hours bit not null default 1
)
go

insert into events (emp_id, event_date, event_date_time, event_type, event_id)
  select a.attached_by, CAST(CONVERT(varchar, a.attach_date, 101) AS DateTime), a.attach_date, 'attach' as entry_type, attachment_id
    from base_attachment a
  union
  select l.statused_by, CAST(CONVERT(varchar, l.status_date, 101) AS DateTime), l.status_date, 'locate' as entry_type, l.ls_id
    from base_locate_status l
  union
  select u.emp_id, CAST(CONVERT(varchar, n.entry_date, 101) AS DateTime), n.entry_date, 'note' as entry_type, n.notes_id
    from base_users u
    inner join base_notes n on u.uid = n.uid
    where n.note not like 'Area changed to%'
  union
  select s.emp_id, CAST(CONVERT(varchar, s.local_date, 101) AS DateTime), s.local_date, 'sync' as entry_type, s.sync_id
    from base_sync_log s
    where s.local_date is not null
  order by 1, 2, 3

print 'Merge events'
update events set emp_id = to_emp_id
 from emp_merge where emp_id = from_emp_id

create clustered index eventsind on events(emp_id, event_date, event_date_time)
go

-- All events during any working / callout span, are OK:

update events set after_hours = 0
  from new_time
  where new_time.emp_id = events.emp_id
   and event_date = work_date
   and event_date_time between start_grace and stop_grace
go

print '***************************************** Look for spans of after hours activity'

if object_id('span') is not null
drop table span
go

-- drop table span
create table span (
	emp_id int not null,
	work_date datetime not null,
	start_date datetime not null,
	end_date datetime not null,
	minutes int
)

delete from span

insert into span
select emp_id, event_date, event_date_time, event_date_time, 0
 from events
 where after_hours = 1
 group by emp_id, event_date, event_date_time
 order by emp_id, event_date, event_date_time

create index span001 on span(emp_id, work_date, start_date)
go

select 1 -- prime the WHILE

-- loop until we haven't done anything
while @@rowcount <> 0
begin

-- extend a span, if it is close enough to another one, first using a 2 minute limit
update s1 set end_date = s2.end_date
from span s1, span s2
 where s1.emp_id = s2.emp_id
  and s1.work_date = s2.work_date
  and s2.start_date > s1.start_date   -- starts after me
  and s2.start_date < dateadd(mi, 2, s1.end_date)  -- starts within the gap of me
  and s1.end_date < s2.end_date

-- delete any unnecessary spans thus created
delete from span where exists
(select * from span containing
  where containing.emp_id = span.emp_id
    and containing.work_date = span.work_date
    and ((containing.start_date < span.start_date and span.end_date <= containing.end_date) or
         (containing.start_date <= span.start_date and span.end_date < containing.end_date)     ) )

end
go

select 1 -- prime the WHILE

-- loop until we haven't done anything
while @@rowcount <> 0
begin

-- extend a span, if it is close enough to another one, now taking it up to 60
update s1 set end_date = s2.end_date
from span s1, span s2
 where s1.emp_id = s2.emp_id
  and s1.work_date = s2.work_date
  and s2.start_date >= s1.start_date   -- starts after me
  and s2.start_date < dateadd(mi, 60, s1.end_date)  -- starts within the gap of me
  and s1.end_date < s2.end_date

  -- Don't leap over a work time span:
  and not exists (select * from new_time nt
    where nt.emp_id=s1.emp_id
     and nt.work_date=s1.work_date
     and nt.start_grace between s1.start_date and s2.end_date)

-- delete any unnecessary spans thus created
delete from span where exists
(select * from span containing
  where containing.emp_id = span.emp_id
    and containing.work_date = span.work_date
    and ((containing.start_date < span.start_date and span.end_date <= containing.end_date) or
         (containing.start_date <= span.start_date and span.end_date < containing.end_date)     ) )

end
go

-- spans are already merged, because they were built from merged events.

if object_id('minutes_between') is not null
  drop function minutes_between
go

create function minutes_between (@a as datetime, @b as datetime)
returns int
as
begin
declare @n int
select @n = datediff(s, @a, @b)
return (@n+59) / 60
end
go

update span set minutes = dbo.minutes_between(start_date, end_date)
 where minutes is null or minutes <> dbo.minutes_between(start_date, end_date)

print '***************************************** Get a fresh result table'

if object_id('dbo.easdata') is not null
drop table easdata
go

CREATE table easdata (
	first_name varchar(50) NULL,
	[eas_emp_id] [int] NOT NULL ,
	[work_date] [datetime] NOT NULL ,
	[work_date_ending] [datetime] NOT NULL ,
	WeekEnding datetime null ,
	BaseYear int null,
	estatus varchar(1) NOT NULL DEFAULT 'X',  -- Employment status as of this date: X, N, E

	lunch_sync int NOT NULL default 0 ,	-- Counts of activities during lunch
	lunch_notes int NOT NULL default 0 ,
	lunch_time int NOT NULL default 0 ,
	lunch_attach int NOT NULL default 0 ,
	lunch_locate int NOT NULL default 0 ,

	after_sync int NOT NULL default 0 ,	-- Counts of activities after hours
	after_notes int NOT NULL default 0 ,
	after_time int NOT NULL default 0 ,
	after_attach int NOT NULL default 0 ,
	after_locate int NOT NULL default 0 ,

	first_sync datetime NULL ,
	last_sync datetime NULL ,
	sync_span integer NULL ,  -- sync span in minutes

	first_act datetime NULL ,  -- first activity of the day
	last_act datetime NULL ,

	-- For sync vs work comparison:
	first_work_start datetime NULL ,
	last_work_stop datetime NULL ,

	-- To support fully detailed work time span detection:
	work_stop1_grace datetime NULL ,
	work_start2_grace datetime NULL ,

	-- begin and end of day calcs - NEW time system
	first_span integer ,	-- Minutes
	last_span integer ,	-- Minutes
	Tot1stMin_plus int null ,
	Tot1stMin_minus int null ,
	TotLastMin_plus int null ,
	TotLastMin_minus int null ,

	-- Sync vs Working Hours - OLD time system:
	sync_minus_hours int null ,
	smh_plus int null ,
	smh_minus int null ,

	-- Work day and employment status
	work_day int NOT NULL default 0 ,   -- Any time in the NEW time system?
	employed_ne_day datetime NULL ,   -- Set to Date if Employed Non-Exempt this day
	employed_ne_day_flag int NOT NULL default 0,  -- 0 or 1
	employed_e_day  datetime NULL ,   -- Set to Date if Employed Exempt this day

	old_timeentry_hours decimal(7,2) NULL , -- working hours from old time system
	old_work_day int NOT NULL default 0 ,   -- Any time in the OLD time system?

	work_but_not_emp_day int NOT NULL default 0 ,   -- Work with no Emp - uhoh

	TotMeals int null ,
	lunch_activity int null ,
	after_hours_activity int null ,
	no_hours_activity int null ,

	last_ok_activity datetime,
	last_ok_activity_clockout_gap_ao int null,
	last_ok_activity_clockout_gap_nonao int null,

	ao_span_minutes int null,
	ao_singletons int null
)
go

print 'inserted merged rows in to easdata'
insert into easdata (first_name, eas_emp_id, work_date, work_date_ending)
  select el.name2, min(eas_emp_id), work_date, work_date_ending
    from base_workspan
     inner join base_employeelist el on el.emp_id = base_workspan.eas_emp_id
   group by el.name2, work_date, work_date_ending
go

print 'index easdata for faster processing'
create clustered index easdata001 on easdata(eas_emp_id, work_date)
GO

UPDATE STATISTICS easdata
go

print 'bring emp span data in to easdata'
update easdata set estatus = span_type
 from employment_spans
  where employment_spans.first_last_name = first_name
   and work_date between span_start and span_end

print '****************************************** OLD TIME ENTRY HOURS'

update easdata
  set old_timeentry_hours = work_hours
  from old_time 
  where emp_id = eas_emp_id
  and date_worked = work_date
  and work_hours>0  -- So that we leave any NULLs alone

print '****************************************** NEW TIME ENTRY LUNCH HOURS'
-- Pick up the special Lunch time processing on Easdata
-- We assume that the first break (between work stop 1 and work start 2
-- is the lunch break.  This is probably mostly true.
-- These have been NULLed if they did not comprise a while (start/stop) span.

update easdata
  set work_stop1_grace=stop_grace
  from new_time
  where easdata.work_date=new_time.work_date
   and easdata.eas_emp_id=new_time.emp_id
   and span_id='work_start1'

update easdata
  set work_start2_grace=start_grace
  from new_time
  where easdata.work_date=new_time.work_date
   and easdata.eas_emp_id=new_time.emp_id
   and span_id='work_start2'

update easdata set
 lunch_notes = (
  select count(*) 
    from base_notes n 
    inner join base_users u on u.uid = n.uid
    inner join emp_join on u.emp_id = orig_emp_id 
    where to_emp_id = eas_emp_id
    and n.entry_date between work_stop1_grace and work_start2_grace),
 lunch_attach = (
  select count(*) 
    from base_attachment a 
    inner join emp_join on a.attached_by=orig_emp_id
    where to_emp_id = eas_emp_id
    and a.attach_date between work_stop1_grace and work_start2_grace),
 lunch_locate = (
  select count(*) 
    from base_locate_status l
    inner join emp_join on l.statused_by = orig_emp_id
    where to_emp_id = eas_emp_id
    and l.status_date between work_stop1_grace and work_start2_grace)

print '****************************************** After Hours'

update easdata set
 after_notes = (
  select count(*) 
    from events
    where events.emp_id = eas_emp_id
    and events.event_date = work_date
    and event_type='note'
    and after_hours=1
  ),
 after_attach = (
  select count(*) 
    from events
    where events.emp_id = eas_emp_id
    and events.event_date = work_date
    and event_type='attach'
    and after_hours=1
  ),
 after_locate = (
  select count(*) 
    from events
    where events.emp_id = eas_emp_id
    and events.event_date = work_date
    and event_type='locate'
    and after_hours=1
  )

-- the above queries caught activity in all gaps, including lunch; so lunch
-- is now subtracted back off:

print 'subtract lunch back from after hours'

update easdata set
  after_notes = after_notes - lunch_notes,
  after_attach = after_attach - lunch_attach,
  after_locate = after_locate - lunch_locate

print '***************************************** first / last sync / work mismatch calculations'

-- the data entry requires that the spans 1,2,3... be in chronological
-- order, so COALESCEing them this way finds the last one.

update easdata set
  work_day = 0,
  first_span = 0,
  last_span = 0,
  Tot1stMin_plus = NULL,   -- So that they COUNT as 0, not as 1.
  Tot1stMin_minus = NULL,
  TotLastMin_plus = NULL,
  TotLastMin_minus = NULL, 
  first_work_start = work_date + (select min(start) from new_time where new_time.work_date=easdata.work_date and emp_id=eas_emp_id),
  last_work_stop = work_date + ( select max(stop) from new_time where new_time.work_date=easdata.work_date and emp_id=eas_emp_id),
  first_sync = (select min(s.local_date)
      from base_sync_log s 
      inner join emp_join on orig_emp_id = s.emp_id
      where to_emp_id = eas_emp_id 
      and s.local_date between work_date and work_date_ending),
  last_sync = (select max(s.local_date)
      from base_sync_log s 
      inner join emp_join on orig_emp_id = s.emp_id
      where to_emp_id = eas_emp_id 
      and s.local_date between work_date and work_date_ending)

update easdata set
  sync_span = datediff(mi, first_sync, last_sync)
  where first_sync IS NOT NULL
   and last_sync > first_sync

-- For the purpose of these spans, the last sync must be after the first clockin for it to count.
update easdata set last_sync = NULL where last_sync < first_work_start
-- and vice versa for the first sync.
update easdata set first_sync = NULL where first_sync > last_work_stop

update easdata set
  first_span = datediff(mi, first_sync, first_work_start)
  where first_sync is not null
   and first_work_start is not null

update easdata set
  last_span = datediff(mi, last_work_stop, last_sync)
  where last_sync is not null
   and last_work_stop is not null

-- If less than 3 hours then do not sum.
update easdata set Tot1stMin_plus   = first_span where first_span>0 and first_span<180
update easdata set Tot1stMin_minus  = first_span where first_span<0 and first_span>-180
update easdata set TotLastMin_plus  = last_span  where last_span>0 and last_span<180
update easdata set TotLastMin_minus = last_span  where last_span<0 and last_span>-180
go

print '****************************************** COUNT WORK DAYS'

update easdata set work_day = 1
 where exists (select * from new_time where emp_id=eas_emp_id and new_time.work_date=easdata.work_date)

update easdata set old_work_day = 0 where old_work_day=1
-- If they have NEW hours, do not count their OLD hours:
update easdata set old_work_day = 1 where old_timeentry_hours>0 and work_day=0

-- Calculate the over/under for old hours days:
update easdata set sync_minus_hours = sync_span - old_timeentry_hours*60
 where sync_span>0 and old_timeentry_hours>0 and work_day=0

update easdata set smh_plus   = sync_minus_hours where sync_minus_hours>0
update easdata set smh_minus  = sync_minus_hours where sync_minus_hours<0

update easdata set employed_ne_day = work_date, employed_ne_day_flag=1 where estatus = 'N'
update easdata set employed_e_day = work_date                          where estatus = 'E'

update easdata set work_but_not_emp_day = 1
 where estatus = 'X' and (work_day=1 or old_timeentry_hours>0)

-- lunch occured if there was a stop1 and start2.
update easdata set
 TotMeals = case when ((work_stop1_grace is not null) and (work_start2_grace is not null))
     then 1
     else 0
   end,
 lunch_activity = case when lunch_notes + lunch_attach + lunch_locate > 0
     then 1
     else 0
   end,
 after_hours_activity = case when work_day=1 and after_notes + after_attach + after_locate > 0
     then 1
     else 0
    end,
 no_hours_activity= case when work_day=0 and old_work_day=0 and
               after_notes + after_attach + after_locate > 0
     then 1
     else 0
    end,
 weekending =  case datepart(dw,work_date)
  when 1 then dateadd(day, 6, work_date)
  when 2 then dateadd(day, 5, work_date)
  when 3 then dateadd(day, 4, work_date)
  when 4 then dateadd(day, 3, work_date)
  when 5 then dateadd(day, 2, work_date)
  when 6 then dateadd(day, 1, work_date)
  when 7 then work_date
  end,
 baseyear = datepart(yy,work_date)

print '***************************************** Analyze gap between last OK activity and last clockout'

update easdata set last_ok_activity = (
  select max(event_date_time) from events
   where event_date=work_date
    and emp_id=eas_emp_id
    and after_hours=0
  )

update easdata set
  last_ok_activity_clockout_gap_ao = datediff(mi, last_ok_activity, last_work_stop)
  where last_ok_activity is not null
   and last_work_stop is not null
   and after_hours_activity=1

update easdata set
  last_ok_activity_clockout_gap_nonao = datediff(mi, last_ok_activity, last_work_stop)
  where last_ok_activity is not null
   and last_work_stop is not null
   and after_hours_activity=0
go

-- Gather after hours spans in to easdata:

update easdata set
	ao_span_minutes = (
		select sum(minutes) from span
		where span.work_date=easdata.work_date
		and span.emp_id = eas_emp_id
	),
	ao_singletons = (
		select count(minutes) from span
		where span.work_date=easdata.work_date
		and span.emp_id = eas_emp_id
		and span.minutes = 0
	)

update easdata set ao_span_minutes=NULL, ao_singletons=NULL
 where work_day=0

-- Get rid of the values in the non-after-hours days, because merely some syncs
-- do not constitute after hours work.
update easdata set ao_span_minutes=null, ao_singletons=null
  where (ao_span_minutes>0 or ao_singletons>0)
   and after_hours_activity=0 and no_hours_activity=0
   and lunch_activity=0
go

-- Now, go back and don't count ANYTHING where the person is not employed Non-EX
update easdata set
	lunch_sync=0,
	lunch_notes=0,
	lunch_time=0,
	lunch_attach=0,
	lunch_locate=0,
	after_sync=0,
	after_notes=0,
	after_time=0,
	after_attach=0,
	after_locate=0,
	first_span=0,
	last_span=0,
	Tot1stMin_plus=NULL,
	Tot1stMin_minus=NULL,
	TotLastMin_plus=NULL,
	TotLastMin_minus=NULL,
	sync_minus_hours=NULL,
	smh_plus=NULL,
	smh_minus=NULL,
	TotMeals=0,
	lunch_activity=0,
	after_hours_activity=0,
	no_hours_activity=0,
	sync_span=NULL,
	last_ok_activity=NULL,
	last_ok_activity_clockout_gap_ao=NULL,
	last_ok_activity_clockout_gap_nonao=NULL,
	ao_span_minutes=NULL,
	ao_singletons=NULL
 where estatus <> 'N'  -- this is a non-null field, by the way.
  or exists (select * from overlap_days od
             where od.work_date=easdata.work_date and (od.emp_id1=eas_emp_id or od.emp_id2=eas_emp_id))

print '***************************************** Create result views'

-- Totals
if object_id('result_summary') is not null
begin
drop view result_summary
drop view result_emp_year
drop view result_emp_week
drop view result_detail
drop view result_employment
end
go

create view result_summary as
select 
first_name,
'All Emp IDs' as 'emp_id',
'All Years' as 'time_period',
count(distinct employed_ne_day)+count(distinct employed_e_day) as Employed_Days,
count(distinct employed_ne_day) as Employed_NE_Days,
count(distinct employed_e_day) as Employed_E_Days,
sum(employed_ne_day_flag * case when work_day=1 or old_work_day=1 then 1 else 0 end) as Employed_NE_Either_Work_Days,
sum(employed_ne_day_flag * work_day) as Employed_NE_New_Work_Days,
sum(employed_ne_day_flag * old_work_day) as Employed_NE_Old_Work_Days,
sum(work_Day * (1-employed_ne_day_flag)) as Work_Days_New_NonNE,
sum(old_work_day * (1-employed_ne_day_flag)) as Work_Days_Old_NonNE,
sum(Tot1stMin_plus) as Tot1stMin_plus,
count(Tot1stMin_plus) as Tot1stMin_plus_days,
cast(sum(Tot1stMin_plus) as float) / nullif(count(Tot1stMin_plus),0) as 'formula6',
sum(Tot1stMin_minus) as Tot1stMin_minus,
count(Tot1stMin_minus) as Tot1stMin_minus_days,
cast(sum(Tot1stMin_minus) as float) / nullif(count(Tot1stMin_minus),0)  as 'formula7',
cast(sum(Tot1stMin_plus)+sum(Tot1stMin_minus) as float) /
   nullif(coalesce(count(Tot1stMin_plus),0) + coalesce(count(Tot1stMin_minus),0),0) as 'formula1',
sum(TotLastMin_plus) as TotLastMin_plus,
count(TotLastMin_plus) as TotLastMin_plus_days,
cast(sum(TotLastMin_plus) as float) / nullif(count(TotLastMin_plus),0) as 'formula8',
sum(TotLastMin_minus) as TotLastMin_minus,
count(TotLastMin_minus) as TotLastMin_minus_days,
cast(sum(TotLastMin_minus) as float) / nullif(count(TotLastMin_minus),0) as 'formula9',
cast(sum(TotLastMin_plus) + sum(TotLastMin_minus) as float) /
   nullif(coalesce(count(TotLastMin_plus),0) + coalesce(count(TotLastMin_minus),0),0) as 'formula2',
sum(TotMeals) as MealBreak,
sum(lunch_activity) as MealBreakActivity, 
cast(sum(lunch_activity) as float) / nullif(sum(TotMeals),0) as 'formula3',
cast(sum(lunch_activity) as float) / nullif(sum(employed_ne_day_flag * work_day),0) as 'formula4',
sum(after_hours_activity) as AfterHoursActivityOnWorkDay,
sum(smh_plus) as smh_plus,
count(smh_plus) as smh_plus_days,
sum(smh_minus) as smh_minus,
count(smh_minus) as smh_minus_days,
cast(sum(smh_plus) + sum(smh_minus) as float) / nullif(sum(employed_ne_day_flag * old_work_day),0) as 'formula5',
sum(no_hours_activity) as NoHoursActivity,
cast(sum(after_hours_activity) + sum(no_hours_activity) as float) / nullif(count(distinct employed_ne_day),0) as 'formula10',
sum(last_ok_activity_clockout_gap_ao) as last_ok_activity_clockout_gap_ao,
avg(last_ok_activity_clockout_gap_ao) as last_ok_activity_clockout_gap_ao_avg,
sum(last_ok_activity_clockout_gap_nonao) as last_ok_activity_clockout_gap_nonao,
avg(last_ok_activity_clockout_gap_nonao) as last_ok_activity_clockout_gap_nonao_avg,
sum(ao_span_minutes) as ao_span_minutes,
cast(sum(ao_span_minutes) as float) / nullif(sum(after_hours_activity) + sum(no_hours_activity), 0) as ao_span_minutes_per_aono_day,
sum(ao_singletons) as ao_singletons
from easdata
group by first_name
go

create view result_emp_year as
select 
first_name,
'All Emp IDs' as 'emp_id',
baseyear,
count(distinct employed_ne_day)+count(distinct employed_e_day) as Employed_Days,
count(distinct employed_ne_day) as Employed_NE_Days,
count(distinct employed_e_day) as Employed_E_Days,
sum(employed_ne_day_flag * case when work_day=1 or old_work_day=1 then 1 else 0 end) as Employed_NE_Either_Work_Days,
sum(employed_ne_day_flag * work_day) as Employed_NE_New_Work_Days,
sum(employed_ne_day_flag * old_work_day) as Employed_NE_Old_Work_Days,
sum(work_Day * (1-employed_ne_day_flag)) as Work_Days_New_NonNE,
sum(old_work_day * (1-employed_ne_day_flag)) as Work_Days_Old_NonNE,
sum(Tot1stMin_plus) as Tot1stMin_plus,
count(Tot1stMin_plus) as Tot1stMin_plus_days,
cast(sum(Tot1stMin_plus) as float) / nullif(count(Tot1stMin_plus),0) as 'formula6',
sum(Tot1stMin_minus) as Tot1stMin_minus,
count(Tot1stMin_minus) as Tot1stMin_minus_days,
cast(sum(Tot1stMin_minus) as float) / nullif(count(Tot1stMin_minus),0)  as 'formula7',
cast(sum(Tot1stMin_plus)+sum(Tot1stMin_minus) as float) /
   nullif(coalesce(count(Tot1stMin_plus),0) + coalesce(count(Tot1stMin_minus),0),0) as 'formula1',
sum(TotLastMin_plus) as TotLastMin_plus,
count(TotLastMin_plus) as TotLastMin_plus_days,
cast(sum(TotLastMin_plus) as float) / nullif(count(TotLastMin_plus),0) as 'formula8',
sum(TotLastMin_minus) as TotLastMin_minus,
count(TotLastMin_minus) as TotLastMin_minus_days,
cast(sum(TotLastMin_minus) as float) / nullif(count(TotLastMin_minus),0) as 'formula9',
cast(sum(TotLastMin_plus) + sum(TotLastMin_minus) as float) /
   nullif(coalesce(count(TotLastMin_plus),0) + coalesce(count(TotLastMin_minus),0),0) as 'formula2',
sum(TotMeals) as MealBreak,
sum(lunch_activity) as MealBreakActivity, 
cast(sum(lunch_activity) as float) / nullif(sum(TotMeals),0) as 'formula3',
cast(sum(lunch_activity) as float) / nullif(sum(employed_ne_day_flag * work_day),0) as 'formula4',
sum(after_hours_activity) as AfterHoursActivityOnWorkDay,
sum(smh_plus) as smh_plus,
count(smh_plus) as smh_plus_days,
sum(smh_minus) as smh_minus,
count(smh_minus) as smh_minus_days,
cast(sum(smh_plus) + sum(smh_minus) as float) / nullif(sum(employed_ne_day_flag * old_work_day),0) as 'formula5',
sum(no_hours_activity) as NoHoursActivity,
cast(sum(after_hours_activity) + sum(no_hours_activity) as float) / nullif(count(distinct employed_ne_day),0) as 'formula10',
sum(last_ok_activity_clockout_gap_ao) as last_ok_activity_clockout_gap_ao,
avg(last_ok_activity_clockout_gap_ao) as last_ok_activity_clockout_gap_ao_avg,
sum(last_ok_activity_clockout_gap_nonao) as last_ok_activity_clockout_gap_nonao,
avg(last_ok_activity_clockout_gap_nonao) as last_ok_activity_clockout_gap_nonao_avg,
sum(ao_span_minutes) as ao_span_minutes,
cast(sum(ao_span_minutes) as float) / nullif(sum(after_hours_activity) + sum(no_hours_activity), 0) as ao_span_minutes_per_aono_day,
sum(ao_singletons) as ao_singletons
from easdata
group by first_name, baseyear
go

create view result_emp_week as
select
first_name,
'All Emp IDs' as 'emp_id',
weekending,
count(distinct employed_ne_day)+count(distinct employed_e_day) as Employed_Days,
count(distinct employed_ne_day) as Employed_NE_Days,
count(distinct employed_e_day) as Employed_E_Days,
sum(employed_ne_day_flag * case when work_day=1 or old_work_day=1 then 1 else 0 end) as Employed_NE_Either_Work_Days,
sum(employed_ne_day_flag * work_day) as Employed_NE_New_Work_Days,
sum(employed_ne_day_flag * old_work_day) as Employed_NE_Old_Work_Days,
sum(work_Day * (1-employed_ne_day_flag)) as Work_Days_New_NonNE,
sum(old_work_day * (1-employed_ne_day_flag)) as Work_Days_Old_NonNE,
sum(Tot1stMin_plus) as Tot1stMin_plus,
count(Tot1stMin_plus) as Tot1stMin_plus_days,
cast(sum(Tot1stMin_plus) as float) / nullif(count(Tot1stMin_plus),0) as 'formula6',
sum(Tot1stMin_minus) as Tot1stMin_minus,
count(Tot1stMin_minus) as Tot1stMin_minus_days,
cast(sum(Tot1stMin_minus) as float) / nullif(count(Tot1stMin_minus),0)  as 'formula7',
cast(sum(Tot1stMin_plus)+sum(Tot1stMin_minus) as float) /
   nullif(coalesce(count(Tot1stMin_plus),0) + coalesce(count(Tot1stMin_minus),0),0) as 'formula1',
sum(TotLastMin_plus) as TotLastMin_plus,
count(TotLastMin_plus) as TotLastMin_plus_days,
cast(sum(TotLastMin_plus) as float) / nullif(count(TotLastMin_plus),0) as 'formula8',
sum(TotLastMin_minus) as TotLastMin_minus,
count(TotLastMin_minus) as TotLastMin_minus_days,
cast(sum(TotLastMin_minus) as float) / nullif(count(TotLastMin_minus),0) as 'formula9',
cast(sum(TotLastMin_plus) + sum(TotLastMin_minus) as float) /
   nullif(coalesce(count(TotLastMin_plus),0) + coalesce(count(TotLastMin_minus),0),0) as 'formula2',
sum(TotMeals) as MealBreak,
sum(lunch_activity) as MealBreakActivity, 
cast(sum(lunch_activity) as float) / nullif(sum(TotMeals),0) as 'formula3',
cast(sum(lunch_activity) as float) / nullif(sum(employed_ne_day_flag * work_day),0) as 'formula4',
sum(after_hours_activity) as AfterHoursActivityOnWorkDay,
sum(smh_plus) as smh_plus,
count(smh_plus) as smh_plus_days,
sum(smh_minus) as smh_minus,
count(smh_minus) as smh_minus_days,
cast(sum(smh_plus) + sum(smh_minus) as float) / nullif(sum(employed_ne_day_flag * old_work_day),0) as 'formula5',
sum(no_hours_activity) as NoHoursActivity,
cast(sum(after_hours_activity) + sum(no_hours_activity) as float) / nullif(count(distinct employed_ne_day),0) as 'formula10',
sum(last_ok_activity_clockout_gap_ao) as last_ok_activity_clockout_gap_ao,
avg(last_ok_activity_clockout_gap_ao) as last_ok_activity_clockout_gap_ao_avg,
sum(last_ok_activity_clockout_gap_nonao) as last_ok_activity_clockout_gap_nonao,
avg(last_ok_activity_clockout_gap_nonao) as last_ok_activity_clockout_gap_nonao_avg,
sum(ao_span_minutes) as ao_span_minutes,
cast(sum(ao_span_minutes) as float) / nullif(sum(after_hours_activity) + sum(no_hours_activity), 0) as ao_span_minutes_per_aono_day,
sum(ao_singletons) as ao_singletons
from easdata
where estatus='N'
group by first_name, weekending
go

create view result_detail as
select 
first_name,
'All Emp IDs' as 'emp_id',
work_date,
count(distinct employed_ne_day)+count(distinct employed_e_day) as Employed_Days,
count(distinct employed_ne_day) as Employed_NE_Days,
count(distinct employed_e_day) as Employed_E_Days,
sum(employed_ne_day_flag * case when work_day=1 or old_work_day=1 then 1 else 0 end) as Employed_NE_Either_Work_Days,
sum(employed_ne_day_flag * work_day) as Employed_NE_New_Work_Days,
sum(employed_ne_day_flag * old_work_day) as Employed_NE_Old_Work_Days,
sum(work_Day * (1-employed_ne_day_flag)) as Work_Days_New_NonNE,
sum(old_work_day * (1-employed_ne_day_flag)) as Work_Days_Old_NonNE,
sum(Tot1stMin_plus) as Tot1stMin_plus,
count(Tot1stMin_plus) as Tot1stMin_plus_days,
cast(sum(Tot1stMin_plus) as float) / nullif(count(Tot1stMin_plus),0) as 'formula6',
sum(Tot1stMin_minus) as Tot1stMin_minus,
count(Tot1stMin_minus) as Tot1stMin_minus_days,
cast(sum(Tot1stMin_minus) as float) / nullif(count(Tot1stMin_minus),0)  as 'formula7',
cast(sum(Tot1stMin_plus)+sum(Tot1stMin_minus) as float) /
   nullif(coalesce(count(Tot1stMin_plus),0) + coalesce(count(Tot1stMin_minus),0),0) as 'formula1',
sum(TotLastMin_plus) as TotLastMin_plus,
count(TotLastMin_plus) as TotLastMin_plus_days,
cast(sum(TotLastMin_plus) as float) / nullif(count(TotLastMin_plus),0) as 'formula8',
sum(TotLastMin_minus) as TotLastMin_minus,
count(TotLastMin_minus) as TotLastMin_minus_days,
cast(sum(TotLastMin_minus) as float) / nullif(count(TotLastMin_minus),0) as 'formula9',
cast(sum(TotLastMin_plus) + sum(TotLastMin_minus) as float) /
   nullif(coalesce(count(TotLastMin_plus),0) + coalesce(count(TotLastMin_minus),0),0) as 'formula2',
sum(TotMeals) as MealBreak,
sum(lunch_activity) as MealBreakActivity, 
cast(sum(lunch_activity) as float) / nullif(sum(TotMeals),0) as 'formula3',
cast(sum(lunch_activity) as float) / nullif(sum(employed_ne_day_flag * work_day),0) as 'formula4',
sum(after_hours_activity) as AfterHoursActivityOnWorkDay,
sum(smh_plus) as smh_plus,
count(smh_plus) as smh_plus_days,
sum(smh_minus) as smh_minus,
count(smh_minus) as smh_minus_days,
cast(sum(smh_plus) + sum(smh_minus) as float) / nullif(sum(employed_ne_day_flag * old_work_day),0) as 'formula5',
sum(no_hours_activity) as NoHoursActivity,
cast(sum(after_hours_activity) + sum(no_hours_activity) as float) / nullif(count(distinct employed_ne_day),0) as 'formula10',
sum(last_ok_activity_clockout_gap_ao) as last_ok_activity_clockout_gap_ao,
avg(last_ok_activity_clockout_gap_ao) as last_ok_activity_clockout_gap_ao_avg,
sum(last_ok_activity_clockout_gap_nonao) as last_ok_activity_clockout_gap_nonao,
avg(last_ok_activity_clockout_gap_nonao) as last_ok_activity_clockout_gap_nonao_avg,
sum(ao_span_minutes) as ao_span_minutes,
cast(sum(ao_span_minutes) as float) / nullif(sum(after_hours_activity) + sum(no_hours_activity), 0) as ao_span_minutes_per_aono_day,
sum(ao_singletons) as ao_singletons
,  -- Add more more to the detail only:
sum(lunch_sync) as lunch_sync,
sum(lunch_notes) as lunch_notes,
sum(lunch_time) as lunch_time,
sum(lunch_attach) as lunch_attach,
sum(lunch_locate) as lunch_locate,
sum(after_sync) as after_sync,
sum(after_notes) as after_notes,
sum(after_time) as after_time,
sum(after_attach) as after_attach,
sum(after_locate) as after_locate
from easdata
where (work_day>0 or old_work_day>0 or work_but_not_emp_day>0)
group by first_name, work_date
go

-- result_employment already returns data correclty aggregated per emp (not emp_id)
create view result_employment as
select name2,
 (select min(work_date) from base_employeelist inner join easdata on emp_id=eas_emp_id
   where base_employeelist.name2=emp.name2 and employed_ne_day_flag>0) AS Oldest_NE_Employment,
 (select min(work_date) from base_employeelist inner join easdata on emp_id=eas_emp_id
   where base_employeelist.name2=emp.name2 and employed_ne_day_flag>0 and (work_day=1 or old_work_day=1)) AS Oldest_Hours_Data,
 (select min(event_date) from base_employeelist inner join events on events.emp_id = base_employeelist.emp_id
   where base_employeelist.name2=emp.name2) AS Oldest_Event_Date
from
 (select distinct name2 from base_employeelist) emp
go

