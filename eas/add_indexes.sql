-- EAS Data Analyis and Summary
-- Kyle Cordes and Joe Apperson
-- Oasis Digital Solutions Inc.

-- Started with queries from Adam Portal at UtiliQuest,
-- Rewrote more or less entirely since then.

-- ---------------------------------------------


create clustered index base_notesuid on base_notes(uid, entry_date)
create clustered index base_attachmentattached_by on base_attachment(attached_by, attach_date)
create clustered index base_locate_statusstatused_by on base_locate_status(statused_by, status_date)

create clustered index base_timesheet on base_timesheet(emp_id, end_date)
create clustered index base_timesheet_detail on base_timesheet_detail(timesheet_id)

create clustered index base_sync_log1 on base_sync_log(emp_id, local_date)
