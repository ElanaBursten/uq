@ECHO OFF
REM this is the server it's all located on
set SERVER=10.1.1.173
set USERNAME=sa
set PASSWORD=doggy183

REM SOURCE is the QM DB we are hitting (UQ, LocInc);
REM the emp list table is stored there (and copied here by this script)
set SOURCEDB=QMReporting
set SOURCEEMPTABLE=defendant_employeelist

REM this is where we will do the EAS work and leave the intermediate data.
set EASDB=EAS14DEF

set STARTDATE=2003-09-11
set ENDDATE=2006-12-31
