use EASLA
drop table tsedata_supervisor
drop table #subjects
go

create table #subjects (
  emp_id int not null primary key,
  witness_type varchar(1) not null,
  name varchar(50) not null
)
insert into #subjects values (2066, 'S', 'Carlile, Jeff')
insert into #subjects values (4517, 'S', 'Carlile, Jeff')
insert into #subjects values (2712, 'S', 'Adamson, Chad')
insert into #subjects values (1587, 'S', 'Bell, Richard')
insert into #subjects values (7498, 'S', 'Brigman, Tracey')
insert into #subjects values (7350, 'S', 'Charles, Linus')
insert into #subjects values (7507, 'S', 'Humphreys, Christopher')
insert into #subjects values (7696, 'S', 'Ledet, Michael')
insert into #subjects values (591,  'S', 'Leigh, Andre')
insert into #subjects values (217,  'S', 'Moskal, William')
insert into #subjects values (7299, 'S', 'Norris, Pamela')
insert into #subjects values (7503, 'S', 'Sibert, Chavis')

  declare @activity table (
    activity varchar(20) not null,
    activity_date datetime not null,
    activity_by int not null,
    activity_emp_number varchar(40) null,
    activity_short_name varchar(40) null,
    activity_entry_id int not null,
    activity_work_date datetime not null,
    activity_work_emp_id int not null
  )

  declare @oldvers table (
    new_entry_id int not null primary key,
    old_entry_id int not null
  )

  -- Get timeentry done for one of the affected supervisors, but entered by someone else
  insert into @activity
  select 'ENTRY',
    entry_date as activity_date, 
    entry_by as activity_by,
    ent_emp.emp_number as activity_emp_number,
    ent_emp.last_name as activity_short_name,
    tse.entry_id,
    tse.work_date,
    tse.work_emp_id
   from QMReporting..timesheet_entry tse
     inner join #subjects s on tse.entry_by = s.emp_id
     left join QMReporting..employee ent_emp on tse.entry_by = ent_emp.emp_id
   where tse.work_date between '2003-09-01' and '2006-12-31'
     and work_emp_id<>entry_by

  -- Now go get the prior version for all the entry versions
  insert into @oldvers (new_entry_id, old_entry_id)
  select activity_entry_id, max(tse.entry_id)
    from @activity a
     inner join QMReporting..timesheet_entry tse
        on a.activity_work_date = tse.work_date
         and a.activity_by = tse.work_emp_id
         and a.activity = 'ENTRY'
         and a.activity_entry_id > tse.entry_id
   group by activity_entry_id

  -- First result set is the well-ordered activity log with detail data
  select a.*, tse.*,
      tse_old.work_start1 as old_work_start1,
      tse_old.work_stop1 as old_work_stop1,
      tse_old.work_start2 as old_work_start2,
      tse_old.work_stop2 as old_work_stop2,
      tse_old.work_start3 as old_work_start3,
      tse_old.work_stop3 as old_work_stop3,
      tse_old.work_start4 as old_work_start4,
      tse_old.work_stop4 as old_work_stop4,
      tse_old.work_start5 as old_work_start5,
      tse_old.work_stop5 as old_work_stop5,
      tse_old.callout_start1 as old_callout_start1,
      tse_old.callout_stop1 as old_callout_stop1,
      tse_old.callout_start2 as old_callout_start2,
      tse_old.callout_stop2 as old_callout_stop2,
      tse_old.callout_start3 as old_callout_start3,
      tse_old.callout_stop3 as old_callout_stop3,
      tse_old.callout_start4 as old_callout_start4,
      tse_old.callout_stop4 as old_callout_stop4,
      tse_old.callout_start5 as old_callout_start5,
      tse_old.callout_stop5 as old_callout_stop5,
      tse_old.callout_start6 as old_callout_start6,
      tse_old.callout_stop6 as old_callout_stop6,
      tse_old.reg_hours as old_reg_hours,
      tse_old.ot_hours as old_ot_hours,
      tse_old.dt_hours as old_dt_hours,
      tse_old.callout_hours as old_callout_hours,
      tse_old.vac_hours as old_vac_hours,
      tse_old.leave_hours as old_leave_hours,
      tse_old.br_hours as old_br_hours,
      tse_old.hol_hours as old_hol_hours,
      tse_old.jury_hours as old_jury_hours
   into tsedata_supervisor
   from @activity a
    inner join QMReporting..timesheet_entry tse on a.activity_entry_id = tse.entry_id
    left join @oldvers o on o.new_entry_id = tse.entry_id
    left join QMReporting..timesheet_entry tse_old on o.old_entry_id = tse_old.entry_id
  order by a.activity_date, a.activity
  go

  -- add some extra fields & populate them
  alter table tsedata_supervisor add hrs decimal(5,2) null
  alter table tsedata_supervisor add old_hrs decimal(5,2) null
  alter table tsedata_supervisor add category varchar(20) null
  alter table tsedata_supervisor add worker_name varchar(50) null
  alter table tsedata_supervisor add worker_emp_number varchar(10) null
  alter table tsedata_supervisor add exempt bit not null default 0
  alter table tsedata_supervisor add witness varchar(1) null -- P=Plaintiff, C=Claimant, S=Supervisor
  go

  update tsedata_supervisor set hrs = 
    coalesce(reg_hours, 0) +
    coalesce(ot_hours, 0) +
    coalesce(dt_hours, 0) +
    coalesce(callout_hours, 0) +
    coalesce(vac_hours, 0) +
    coalesce(leave_hours, 0) +
    coalesce(br_hours, 0) +
    coalesce(hol_hours, 0) +
    coalesce(jury_hours, 0),
  old_hrs =
    coalesce(old_reg_hours, 0) +
    coalesce(old_ot_hours, 0) +
    coalesce(old_dt_hours, 0) +
    coalesce(old_callout_hours, 0) +
    coalesce(old_vac_hours, 0) +
    coalesce(old_leave_hours, 0) +
    coalesce(old_br_hours, 0) +
    coalesce(old_hol_hours, 0) +
    coalesce(old_jury_hours, 0)

  update tsedata_supervisor set worker_emp_number = emp_number, worker_name = short_name
  from QMReporting..employee where work_emp_id=emp_id

	update tsedata_supervisor set witness = witness_type, activity_short_name = name
  from #subjects where entry_by = emp_id
