-- get a local copy of the tsedata_LocInc:
use EAS_200801
drop table tsedata_LocInc
select * into tsedata_LocInc from "10.1.1.24".EAS18LI07.dbo.tsedata_LocInc

select activity_short_name, count(*) from tsedata_LocInc group by activity_short_name order by activity_short_name

select top 10 * from tsedata_LocInc

-- Categorize the types of changes made
update tsedata_LocInc set category = null
update tsedata_LocInc set category = 'Zero / Zero' where old_hrs=0 and hrs=0
update tsedata_LocInc set category = 'New Entry' where old_hrs=0 and hrs>0
update tsedata_LocInc set category = 'Remove All' where old_hrs>0 and hrs=0
update tsedata_LocInc set category = 'Added Hours' where old_hrs>0 and hrs>old_hrs
update tsedata_LocInc set category = 'Removed Hours' where old_hrs>0 and hrs<old_hrs

update tsedata_LocInc set category = 'Added Lunch' 
from (select activity_entry_id, coalesce(datediff(mi, old_work_stop1, old_work_start2), 0) as old_lunch_minutes,
  coalesce(datediff(mi, work_stop1, work_start2), 0) as new_lunch_minutes  
  from tsedata_LocInc) data
where tsedata_LocInc.activity_entry_id=data.activity_entry_id 
  and old_hrs>0 and hrs>0
  and old_lunch_minutes=0
  and new_lunch_minutes>0

update tsedata_LocInc set category = 'Removed Lunch' 
from (select activity_entry_id, coalesce(datediff(mi, old_work_stop1, old_work_start2), 0) as old_lunch_minutes,
  coalesce(datediff(mi, work_stop1, work_start2), 0) as new_lunch_minutes  
  from tsedata_LocInc) data
where tsedata_LocInc.activity_entry_id=data.activity_entry_id 
  and old_hrs>0 and hrs>0
  and old_lunch_minutes>0
  and new_lunch_minutes=0

update tsedata_LocInc set category = 'New Entry' where old_hrs=hrs and category is null

-- check what other uncategorized changes there are:
select top 100 * from tsedata_LocInc where category is null

select activity_short_name, worker_name,
  count(*),
  min(work_date) as oldest_work_date,
  max(work_date) as newest_work_date,
  sum(old_hrs) as old_hrs, sum(hrs) as new_hrs,
  sum(hrs) - sum(old_hrs) as net_change,
  sum(case when hrs>old_hrs then 1 else 0 end) as increases,
  sum(case when old_hrs>hrs then 1 else 0 end) as decreases
 from tsedata_LocInc
 where category is null
 group by activity_short_name, worker_name
 order by 1,2

select worker_name, count(*) as N from tsedata_LocInc  where category is null
 group by worker_name order by 1

select activity_short_name, work_date, worker_name, old_hrs, hrs
from tsedata_LocInc
where hrs>old_hrs
and category is null
order by 1, 2, 3

-- Claimant summary results:
select worker_emp_number, worker_name,
  min(work_date) as oldest_work_date,
  max(work_date) as newest_work_date,
  sum(old_hrs) as old_hrs, 
  sum(hrs) as new_hrs,
  sum(hrs) - sum(old_hrs) as net_change,
  sum(case when hrs > old_hrs then 1 else 0 end) as increases,
  sum(case when old_hrs > hrs then 1 else 0 end) as decreases,
  sum(case when category = 'Added Lunch' then 1 else 0 end) as added_lunch,
  sum(case when category = 'Removed Lunch' then 1 else 0 end) as removed_lunch,
  sum(case when category = 'Added Hours' then 1 else 0 end) as added_hours,
  sum(case when category = 'Removed Hours' then 1 else 0 end) as removed_hours,
  sum(case when category = 'New Entry' then 1 else 0 end) as new_entry,
  sum(case when category = 'Zero / Zero' then 1 else 0 end) as new_blank_entry,
  count(*) as total_changes
from tsedata_LocInc
where exempt = 0 and witness = 'C'
group by worker_emp_number, worker_name
order by worker_name

-- Claimant detail results:
select worker_name
  ,worker_emp_number
  ,category
  ,activity_date
  ,activity_by
  ,activity_emp_number
  ,activity_short_name
  ,activity_entry_id
  ,activity_work_date
  ,activity_work_emp_id
  ,entry_id
  ,work_emp_id
  ,work_date
  ,modified_date
  ,status
  ,entry_date
  ,entry_date_local
  ,entry_by
  ,approve_date
  ,approve_date_local
  ,approve_by
  ,final_approve_date
  ,final_approve_date_local
  ,final_approve_by
	-- These DateAdd's convert 1899-12-30 to 1900-01-01 which Excel recognizes as a Time only value
  ,DateAdd(Day, 2, work_start1) as work_start1
  ,DateAdd(Day, 2, work_stop1) as work_stop1
  ,DateAdd(Day, 2, work_start2) as work_start2
  ,DateAdd(Day, 2, work_stop2) as work_stop2
  ,DateAdd(Day, 2, work_start3) as work_start3
  ,DateAdd(Day, 2, work_stop3) as work_stop3
  ,DateAdd(Day, 2, work_start4) as work_start4
  ,DateAdd(Day, 2, work_stop4) as work_stop4
  ,DateAdd(Day, 2, work_start5) as work_start5
  ,DateAdd(Day, 2, work_stop5) as work_stop5
  ,DateAdd(Day, 2, callout_start1) as callout_start1
  ,DateAdd(Day, 2, callout_stop1) as callout_stop1
  ,DateAdd(Day, 2, callout_start2) as callout_start2
  ,DateAdd(Day, 2, callout_stop2) as callout_stop2
  ,DateAdd(Day, 2, callout_start3) as callout_start3
  ,DateAdd(Day, 2, callout_stop3) as callout_stop3
  ,DateAdd(Day, 2, callout_start4) as callout_start4
  ,DateAdd(Day, 2, callout_stop4) as callout_stop4
  ,DateAdd(Day, 2, callout_start5) as callout_start5
  ,DateAdd(Day, 2, callout_stop5) as callout_stop5
  ,DateAdd(Day, 2, callout_start6) as callout_start6
  ,DateAdd(Day, 2, callout_stop6) as callout_stop6
  ,miles_start1
  ,miles_stop1
  ,vehicle_use
  ,reg_hours
  ,ot_hours
  ,dt_hours
  ,callout_hours
  ,vac_hours
  ,leave_hours
  ,br_hours
  ,hol_hours
  ,jury_hours
  ,DateAdd(Day, 2, old_work_start1) as old_work_start1
  ,DateAdd(Day, 2, old_work_stop1) as old_work_stop1
  ,DateAdd(Day, 2, old_work_start2) as old_work_start2
  ,DateAdd(Day, 2, old_work_stop2) as old_work_stop2
  ,DateAdd(Day, 2, old_work_start3) as old_work_start3
  ,DateAdd(Day, 2, old_work_stop3) as old_work_stop3
  ,DateAdd(Day, 2, old_work_start4) as old_work_start4
  ,DateAdd(Day, 2, old_work_stop4) as old_work_stop4
  ,DateAdd(Day, 2, old_work_start5) as old_work_start5
  ,DateAdd(Day, 2, old_work_stop5) as old_work_stop5
  ,DateAdd(Day, 2, old_callout_start1) as old_callout_start1
  ,DateAdd(Day, 2, old_callout_stop1) as old_callout_stop1
  ,DateAdd(Day, 2, old_callout_start2) as old_callout_start2
  ,DateAdd(Day, 2, old_callout_stop2) as old_callout_stop2
  ,DateAdd(Day, 2, old_callout_start3) as old_callout_start3
  ,DateAdd(Day, 2, old_callout_stop3) as old_callout_stop3
  ,DateAdd(Day, 2, old_callout_start4) as old_callout_start4
  ,DateAdd(Day, 2, old_callout_stop4) as old_callout_stop4
  ,DateAdd(Day, 2, old_callout_start5) as old_callout_start5
  ,DateAdd(Day, 2, old_callout_stop5) as old_callout_stop5
  ,DateAdd(Day, 2, old_callout_start6) as old_callout_start6
  ,DateAdd(Day, 2, old_callout_stop6) as old_callout_stop6
  ,old_reg_hours
  ,old_ot_hours
  ,old_dt_hours
  ,old_callout_hours
  ,old_vac_hours
  ,old_leave_hours
  ,old_br_hours
  ,old_hol_hours
  ,old_jury_hours
  ,hrs
  ,old_hrs
  ,hrs - old_hrs as net_change
  ,exempt
  ,witness
from tsedata_LocInc
where witness = 'C'
order by worker_name, category, activity_date

-- Plaintiff summary results:
select worker_emp_number, worker_name,
  min(work_date) as oldest_work_date,
  max(work_date) as newest_work_date,
  sum(old_hrs) as old_hrs, 
  sum(hrs) as new_hrs,
  sum(hrs) - sum(old_hrs) as net_change,
  sum(case when hrs > old_hrs then 1 else 0 end) as increases,
  sum(case when old_hrs > hrs then 1 else 0 end) as decreases,
  sum(case when category = 'Added Lunch' then 1 else 0 end) as added_lunch,
  sum(case when category = 'Removed Lunch' then 1 else 0 end) as removed_lunch,
  sum(case when category = 'Added Hours' then 1 else 0 end) as added_hours,
  sum(case when category = 'Removed Hours' then 1 else 0 end) as removed_hours,
  sum(case when category = 'New Entry' then 1 else 0 end) as new_entry,
  sum(case when category = 'Zero / Zero' then 1 else 0 end) as new_blank_entry,
  count(*) as total_changes
from tsedata_LocInc
where exempt = 0 and witness = 'P'
group by worker_emp_number, worker_name
order by worker_name

-- Plaintiff detail results:
select worker_name
  ,worker_emp_number
  ,category
  ,activity_date
  ,activity_by
  ,activity_emp_number
  ,activity_short_name
  ,activity_entry_id
  ,activity_work_date
  ,activity_work_emp_id
  ,entry_id
  ,work_emp_id
  ,work_date
  ,modified_date
  ,status
  ,entry_date
  ,entry_date_local
  ,entry_by
  ,approve_date
  ,approve_date_local
  ,approve_by
  ,final_approve_date
  ,final_approve_date_local
  ,final_approve_by
  ,DateAdd(Day, 2, work_start1) as work_start1
  ,DateAdd(Day, 2, work_stop1) as work_stop1
  ,DateAdd(Day, 2, work_start2) as work_start2
  ,DateAdd(Day, 2, work_stop2) as work_stop2
  ,DateAdd(Day, 2, work_start3) as work_start3
  ,DateAdd(Day, 2, work_stop3) as work_stop3
  ,DateAdd(Day, 2, work_start4) as work_start4
  ,DateAdd(Day, 2, work_stop4) as work_stop4
  ,DateAdd(Day, 2, work_start5) as work_start5
  ,DateAdd(Day, 2, work_stop5) as work_stop5
  ,DateAdd(Day, 2, callout_start1) as callout_start1
  ,DateAdd(Day, 2, callout_stop1) as callout_stop1
  ,DateAdd(Day, 2, callout_start2) as callout_start2
  ,DateAdd(Day, 2, callout_stop2) as callout_stop2
  ,DateAdd(Day, 2, callout_start3) as callout_start3
  ,DateAdd(Day, 2, callout_stop3) as callout_stop3
  ,DateAdd(Day, 2, callout_start4) as callout_start4
  ,DateAdd(Day, 2, callout_stop4) as callout_stop4
  ,DateAdd(Day, 2, callout_start5) as callout_start5
  ,DateAdd(Day, 2, callout_stop5) as callout_stop5
  ,DateAdd(Day, 2, callout_start6) as callout_start6
  ,DateAdd(Day, 2, callout_stop6) as callout_stop6
  ,miles_start1
  ,miles_stop1
  ,vehicle_use
  ,reg_hours
  ,ot_hours
  ,dt_hours
  ,callout_hours
  ,vac_hours
  ,leave_hours
  ,br_hours
  ,hol_hours
  ,jury_hours
  ,DateAdd(Day, 2, old_work_start1) as old_work_start1
  ,DateAdd(Day, 2, old_work_stop1) as old_work_stop1
  ,DateAdd(Day, 2, old_work_start2) as old_work_start2
  ,DateAdd(Day, 2, old_work_stop2) as old_work_stop2
  ,DateAdd(Day, 2, old_work_start3) as old_work_start3
  ,DateAdd(Day, 2, old_work_stop3) as old_work_stop3
  ,DateAdd(Day, 2, old_work_start4) as old_work_start4
  ,DateAdd(Day, 2, old_work_stop4) as old_work_stop4
  ,DateAdd(Day, 2, old_work_start5) as old_work_start5
  ,DateAdd(Day, 2, old_work_stop5) as old_work_stop5
  ,DateAdd(Day, 2, old_callout_start1) as old_callout_start1
  ,DateAdd(Day, 2, old_callout_stop1) as old_callout_stop1
  ,DateAdd(Day, 2, old_callout_start2) as old_callout_start2
  ,DateAdd(Day, 2, old_callout_stop2) as old_callout_stop2
  ,DateAdd(Day, 2, old_callout_start3) as old_callout_start3
  ,DateAdd(Day, 2, old_callout_stop3) as old_callout_stop3
  ,DateAdd(Day, 2, old_callout_start4) as old_callout_start4
  ,DateAdd(Day, 2, old_callout_stop4) as old_callout_stop4
  ,DateAdd(Day, 2, old_callout_start5) as old_callout_start5
  ,DateAdd(Day, 2, old_callout_stop5) as old_callout_stop5
  ,DateAdd(Day, 2, old_callout_start6) as old_callout_start6
  ,DateAdd(Day, 2, old_callout_stop6) as old_callout_stop6
  ,old_reg_hours
  ,old_ot_hours
  ,old_dt_hours
  ,old_callout_hours
  ,old_vac_hours
  ,old_leave_hours
  ,old_br_hours
  ,old_hol_hours
  ,old_jury_hours
  ,hrs
  ,old_hrs
  ,hrs - old_hrs as net_change
  ,exempt
  ,witness
from tsedata_LocInc
where witness = 'P'
order by worker_name, category, activity_date

-- Other specific case scenarios for additional analysis (Not included in the primary output results)

-- a summary by activity & worker:
select IsNull(worker_name, '- TOTAL -') as worker_name, 
 IsNull(category, '- TOTAL -') as category, count(*) N
from tsedata_LocInc 
where worker_name is not null 
  and category is not null
  and exempt = 0
group by worker_name, category with rollup
order by worker_name, category

-- look for lengthened lunches:
select activity_short_name, worker_name, work_date, old_lunch_minutes, new_lunch_minutes,
  old_hrs, hrs
from 
(select *,
  datediff(mi, old_work_stop1, old_work_start2) as old_lunch_minutes,
  datediff(mi, work_stop1, work_start2) as new_lunch_minutes  
from tsedata_LocInc
 where old_work_stop1 is not null
  and old_work_start2 is not null
  and work_stop1 is not null
  and work_start2 is not null) data
where old_lunch_minutes>0
 and new_lunch_minutes>0
 and new_lunch_minutes > old_lunch_minutes
 order by 1,2,3

-- look for shortened lunches:
select activity_short_name, worker_name, work_date, old_lunch_minutes, new_lunch_minutes,
  old_hrs, hrs
from 
(select *,
  datediff(mi, old_work_stop1, old_work_start2) as old_lunch_minutes,
  datediff(mi, work_stop1, work_start2) as new_lunch_minutes  
from tsedata_LocInc
 where old_work_stop1 is not null
  and old_work_start2 is not null
  and work_stop1 is not null
  and work_start2 is not null) data
where old_lunch_minutes>0
 and new_lunch_minutes>0
 and new_lunch_minutes < old_lunch_minutes
 order by 1,2,3

-- look for added lunches:
select activity_short_name, worker_name, work_date, old_lunch_minutes, new_lunch_minutes,
  old_hrs, hrs
from 
(select *,
  coalesce(datediff(mi, old_work_stop1, old_work_start2), 0) as old_lunch_minutes,
  coalesce(datediff(mi, work_stop1, work_start2), 0) as new_lunch_minutes  
from tsedata_LocInc) data
where old_hrs>0 and hrs>0
 and old_lunch_minutes=0
 and new_lunch_minutes>0
 order by 1,2,3

-- look for removed lunches:
select activity_short_name, worker_name, work_date, old_lunch_minutes, new_lunch_minutes,
  old_hrs, hrs
from 
(select *,
  coalesce(datediff(mi, old_work_stop1, old_work_start2), 0) as old_lunch_minutes,
  coalesce(datediff(mi, work_stop1, work_start2), 0) as new_lunch_minutes  
from tsedata_LocInc) data
where old_hrs>0 and hrs>0
 and old_lunch_minutes>0
 and new_lunch_minutes=0
 order by 1,2,3

-- look for lengthened hours:
select activity_short_name, worker_name, work_date, old_lunch_minutes, new_lunch_minutes,
  old_hrs, hrs
from 
(select *,
  datediff(mi, old_work_stop1, old_work_start2) as old_lunch_minutes,
  datediff(mi, work_stop1, work_start2) as new_lunch_minutes  
from tsedata_LocInc
 where old_work_stop1 is not null
  and old_work_start2 is not null
  and work_stop1 is not null
  and work_start2 is not null) data
where old_lunch_minutes>0
 and new_lunch_minutes>0
 and new_lunch_minutes > old_lunch_minutes
 order by 1,2,3
