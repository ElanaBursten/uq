@ECHO OFF
REM this is the server it's all located on
set SERVER=10.1.1.173
set USERNAME=sa
set PASSWORD=doggy183

REM SOURCE is the QM DB we are hitting (UQ, LocInc);
REM the emp list table is stored there (and copied here by this script)
set SOURCEDB=QMReporting
set SOURCEEMPTABLE=eas_analysis_employee

REM this is where we will do the EAS work and leave the intermediate data.
set EASDB=EAS18UQ07

set STARTDATE=2007-01-01
set ENDDATE=2007-04-30

