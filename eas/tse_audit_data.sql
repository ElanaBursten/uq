use EASLA
drop table tsedata
drop table #subjects
go

create table #subjects (
  emp_id int not null primary key,
  witness_type varchar(1) not null,
  name varchar(50) not null
)

insert into #subjects values (2712,  'C', 'Adamson, Chad')
insert into #subjects values (6022,  'C', 'Aguirre, Baldemar')
insert into #subjects values (1587,  'C', 'Bell, Richard')
insert into #subjects values (7135,  'C', 'Braxton, Christopher')
insert into #subjects values (7498,  'C', 'Brigman, Tracey')
insert into #subjects values (4583,  'C', 'Cameron, Russell')
insert into #subjects values (10193, 'C', 'Cameron, Russell')
insert into #subjects values (7350,  'C', 'Charles, Linus')
-- insert into #subjects values (7553,  'C', 'Dawson, Donald') -- Excluded by request 1/4/2008
insert into #subjects values (254,   'C', 'Fitzgerald, Michael')
insert into #subjects values (4371,  'C', 'Gardner, Merrick')
insert into #subjects values (4926,  'C', 'Granger, Bobby')
insert into #subjects values (6601,  'C', 'Grissom, James')
insert into #subjects values (322,   'C', 'Hillberry, Michael ')
insert into #subjects values (7507,  'C', 'Humphreys, Christopher')
insert into #subjects values (7724,  'C', 'Johns, Samuel')
insert into #subjects values (8816,  'C', 'Johns, Samuel')
insert into #subjects values (9516,  'C', 'Johnson, Windi')
insert into #subjects values (7696,  'C', 'Ledet, Michael')
insert into #subjects values (591,   'C', 'Leigh, Andre')
insert into #subjects values (6981,  'C', 'Makus, Talya')
insert into #subjects values (4437,  'C', 'McClain, Greg')
insert into #subjects values (6959,  'C', 'McClain, Greg')
insert into #subjects values (5208,  'C', 'McClain, Greg')
insert into #subjects values (5037,  'C', 'Newton, John')
insert into #subjects values (7299,  'C', 'Norris, Pamela')
insert into #subjects values (5353,  'C', 'Petroviak, Ronald')
insert into #subjects values (4354,  'C', 'Plaga, Steven')
insert into #subjects values (2506,  'C', 'Roberts, James')
insert into #subjects values (5314,  'C', 'Roberts, James')
insert into #subjects values (6960,  'C', 'Roberts, James')
insert into #subjects values (7312,  'C', 'Rodriguez, John')
insert into #subjects values (7503,  'C', 'Sibert, Chavis')
insert into #subjects values (8895,  'C', 'Signorelli, Carmelo')
insert into #subjects values (988,   'C', 'Storey, Joe')
insert into #subjects values (7385,  'C', 'Switala, Greg')
insert into #subjects values (1854,  'C', 'Thompson, Ernest ')
insert into #subjects values (7353,  'C', 'Whaley, Jeremy')
insert into #subjects values (6451,  'C', 'Williams, James')
insert into #subjects values (10038, 'C', 'Williams, James')
insert into #subjects values (7961,  'C', 'Williams, Jr, James')
insert into #subjects values (6677,  'P', 'Austin, James')
insert into #subjects values (10063, 'P', 'Beckham, Dale')
insert into #subjects values (9805,  'P', 'Biddix, Matthew')
insert into #subjects values (2184,  'P', 'Croft, Charlie')
insert into #subjects values (5457,  'P', 'Hamrick, Scott')
insert into #subjects values (4927,  'P', 'Hooker, Thomas')
insert into #subjects values (7416,  'P', 'Houk, Brian')
insert into #subjects values (6478,  'P', 'Maldonado, Ramon')
insert into #subjects values (3656,  'P', 'Moran, James')
insert into #subjects values (1801,  'P', 'Myers, Christina')
insert into #subjects values (4126,  'P', 'Olivero, Louis')
insert into #subjects values (10317, 'P', 'Palczynsky, Joshua')
insert into #subjects values (6928,  'P', 'Richey, Damon')
insert into #subjects values (7100,  'P', 'Ross, Jeremy')
insert into #subjects values (6684,  'P', 'Tipton, Lorrelie')
-- insert into #subjects values ()  -- Kahafer, Uriah  (not in QM db)

  declare @activity table (
    activity varchar(20) not null,
    activity_date datetime not null,
    activity_by int not null,
    activity_emp_number varchar(40) null,
    activity_short_name varchar(40) null,
    activity_entry_id int not null,
    activity_work_date datetime not null,
    activity_work_emp_id int not null
  )

  declare @oldvers table (
    new_entry_id int not null primary key,
    old_entry_id int not null
  )

  -- Get timeentry done for one of the affected employees, but entered by someone else
  insert into @activity
  select 'ENTRY',
    entry_date as activity_date,
    entry_by as activity_by,
    ent_emp.emp_number as activity_emp_number,
    ent_emp.last_name as activity_short_name,
    tse.entry_id,
    tse.work_date,
    tse.work_emp_id
   from QMReporting..timesheet_entry tse
     inner join #subjects s on tse.work_emp_id=s.emp_id
     left join QMReporting..employee ent_emp on tse.entry_by = ent_emp.emp_id
   where tse.work_date between '2003-09-01' and '2006-12-31'
     and work_emp_id<>entry_by

  -- Now go get the prior version for all the entry versions
  insert into @oldvers (new_entry_id, old_entry_id)
  select activity_entry_id, max(tse.entry_id)
    from @activity a
     inner join QMReporting..timesheet_entry tse
        on a.activity_work_date = tse.work_date
         and a.activity_by = tse.work_emp_id
         and a.activity = 'ENTRY'
         and a.activity_entry_id > tse.entry_id
   group by activity_entry_id

  -- First result set is the well-ordered activity log with detail data
  select a.*, tse.*,
      tse_old.work_start1 as old_work_start1,
      tse_old.work_stop1 as old_work_stop1,
      tse_old.work_start2 as old_work_start2,
      tse_old.work_stop2 as old_work_stop2,
      tse_old.work_start3 as old_work_start3,
      tse_old.work_stop3 as old_work_stop3,
      tse_old.work_start4 as old_work_start4,
      tse_old.work_stop4 as old_work_stop4,
      tse_old.work_start5 as old_work_start5,
      tse_old.work_stop5 as old_work_stop5,
      tse_old.callout_start1 as old_callout_start1,
      tse_old.callout_stop1 as old_callout_stop1,
      tse_old.callout_start2 as old_callout_start2,
      tse_old.callout_stop2 as old_callout_stop2,
      tse_old.callout_start3 as old_callout_start3,
      tse_old.callout_stop3 as old_callout_stop3,
      tse_old.callout_start4 as old_callout_start4,
      tse_old.callout_stop4 as old_callout_stop4,
      tse_old.callout_start5 as old_callout_start5,
      tse_old.callout_stop5 as old_callout_stop5,
      tse_old.callout_start6 as old_callout_start6,
      tse_old.callout_stop6 as old_callout_stop6,
      tse_old.reg_hours as old_reg_hours,
      tse_old.ot_hours as old_ot_hours,
      tse_old.dt_hours as old_dt_hours,
      tse_old.callout_hours as old_callout_hours,
      tse_old.vac_hours as old_vac_hours,
      tse_old.leave_hours as old_leave_hours,
      tse_old.br_hours as old_br_hours,
      tse_old.hol_hours as old_hol_hours,
      tse_old.jury_hours as old_jury_hours
   into tsedata
   from @activity a
    inner join QMReporting..timesheet_entry tse on a.activity_entry_id = tse.entry_id
    left join @oldvers o on o.new_entry_id = tse.entry_id
    left join QMReporting..timesheet_entry tse_old on o.old_entry_id = tse_old.entry_id
  order by a.activity_date, a.activity
  go

  -- add some extra fields & populate them
  alter table tsedata add hrs decimal(5,2) null
  alter table tsedata add old_hrs decimal(5,2) null
  alter table tsedata add category varchar(20) null
  alter table tsedata add worker_name varchar(50) null
  alter table tsedata add worker_emp_number varchar(10) null
  alter table tsedata add exempt bit not null default 0
  alter table tsedata add witness varchar(1) null -- P=Plaintiff, C=Claimant
  go

  update tsedata set hrs = 
    coalesce(reg_hours, 0) +
    coalesce(ot_hours, 0) +
    coalesce(dt_hours, 0) +
    coalesce(callout_hours, 0) +
    coalesce(vac_hours, 0) +
    coalesce(leave_hours, 0) +
    coalesce(br_hours, 0) +
    coalesce(hol_hours, 0) +
    coalesce(jury_hours, 0),
  old_hrs =
    coalesce(old_reg_hours, 0) +
    coalesce(old_ot_hours, 0) +
    coalesce(old_dt_hours, 0) +
    coalesce(old_callout_hours, 0) +
    coalesce(old_vac_hours, 0) +
    coalesce(old_leave_hours, 0) +
    coalesce(old_br_hours, 0) +
    coalesce(old_hol_hours, 0) +
    coalesce(old_jury_hours, 0)

  update tsedata set worker_emp_number = emp_number
  from QMReporting..employee where work_emp_id=emp_id

  update tsedata set worker_name = name, witness = witness_type
  from #subjects where work_emp_id = emp_id
