library Geocode;

uses
  REST.Client,
  REST.Types,
  JSON,
  JSON.Types,
  JSON.Readers,
  IndyPeerImpl,
  System.SysUtils,
  System.StrUtils,
  Math,
  Classes;

  const
   SUCCESS = '"OK"';
   ErrMsg = '[ERROR]';
   GoogleAPIKey = 'AIzaSyDJawB5LUbGAQkCJjtMw7F7Le9ZpOQP8oQ';

type
  Request = (Address, LatLong, DistTime, AdminAreaLevel2);     //qm-391 added to return county    sr

function ProcessAddressRead(jtr: TJsonTextReader): String; forward;
function ProcessLatLongRead(jtr: TJsonTextReader): String; forward;
function ProcessDistTimeRead(jtr: TJsonTextReader): String; forward;
function ProcessAdminAreaLevel2(jtr: TJsonTextReader): String; forward;  //qm-391 added to return county    sr
function ProcessJSON(RequestStr: String; RequestType: Request): String; forward;
function rad2deg(rad:double): double; forward;
function deg2rad(deg:double):double; forward;

{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

function rad2deg(rad:double): double;
{radians to degrees}
begin result:=rad / PI * 180.0; end;

function deg2rad(deg:double):double;
{Degrees to Radians}
begin result:=deg * PI / 180.0; end;

function GetHeading(dlat1,dlon1,dlat2,dlon2: double;
         Var AzimuthInit, AzimuthFinal: double; units:integer): double; stdcall;
{"Units" values: 0 = miles, 1 = kilometers, 2 = nautical miles}
var
  lat1,lat2,Lon1,lon2: double;
  f,OldLambda,Lambda: double;
  a,b,TanU1,U1,tanU2,U2, Usq: double;
  count:integer;
  sigma,sinSigma,cosSigma,TanSigma: double;
  cos2sigmam: double;
  sinAlpha, cosalpha, cosSqAlpha,C: double;
  AA,BB,L,L1,L2: double;
  dsigma,dist,azimuthreverse: double;
begin
  try
  a:=6378137.0;
  b:=6356752.314;
  f:=(a-b)/a;
  lat1:=deg2rad(dlat1);
  lat2:=deg2rad(dlat2);
  lon1:=deg2rad(dlon1);
  lon2:=deg2rad(dlon2);
  //AzimuthInit:=deg2Rad(Azimuthinit);
  TanU1:=(1-f)*tan(Lat1);
  U1:=arctan(Tanu1);
  tanu2 :=(1-f)*Tan(lat2);
  U2:=arctan(TanU2);
  L:=lon2-lon1;
  Lambda:=L; {first approximation of distance}
  count:=0;
  repeat  {iterate}
    inc(count);
    sinSigma:=sqrt(sqr(cos(U2)*sin(Lambda))+sqr(cos(u1)*sin(u2)-cos(U2)*sin(U1)*cos(Lambda)));
    CosSigma:=sin(U1)*sin(u2)+cos(u1)*cos(u2)*cos(Lambda);
    sigma := ArcTan(sinSigma/cosSigma);
    SinAlpha:=cos(u1)*cos(u2)*sin(lambda)/sinSigma;

    cosalpha:=cos(arcsin(sinAlpha));
    cossqalpha:=sqr(cosAlpha);
    cosSqalpha:=1-sqr(sinalpha);
    if (Lat1=0) and (Lat2=0) then cos2Sigmam:=0
    else  cos2sigmam:=cosSigma-(2*sin(U1)*sin(u2)/cosSqalpha);
    C:=(f/16)*cosSqalpha*(4+f*(4-3*cosSqalpha));
    oldlambda:=lambda;
    Lambda:=L+(1-c)*f*sinalpha*(sigma+C*sinsigma*(cos2Sigmam+c*cosSigma*(2*sqr(Cos2Sigmam)-1)));
  until (abs(lambda-oldlambda)<10e-12) or (count>1000);
  if count>1000 then raise Exception.Create('No convergence')
  else
  begin
    uSq := cosSqAlpha *  (Sqr(a) - Sqr(b)) / Sqr(b);

    AA :=  1.0+(uSq/16384.0)*(4096.0+uSq*(-768.0+uSq*(320.0-175.0*uSq)));

    BB := (uSq/1024.0)*(256.0+uSq*(-128.0+uSq*(74.0-47.0*uSq)));

    dsigma := BB*sinSigma*(cos2sigmam+(BB/4.0)*(-1+cosSigma*(2*Sqr(cos2Sigmam)) -
             (BB/6)*cos2Sigmam*(-3+4*Sqr(sinSigma))*(-3+4*Sqr(cos2sigmam))));

    Dist:=b*AA*(sigma-dsigma);
    AzimuthInit:=ArcTan2(cos(u2)*Sin(lambda),cos(u1)*sin(u2)-sin(u1)*cos(u2)*Cos(lambda)); {initial azimuth}
    while AzimuthInit<0 do AzimuthInit:=AzimuthInit+2*Pi;
    AzimuthReverse := ArcTan2(cos(u1)*Sin(lambda),-sin(u1)*cos(u2)+cos(u1)*sin(u2)*Cos(lambda))-Pi; {reverse azimuth}
    Azimuthfinal:=AzimuthReverse-Pi;
    while azimuthfinal<0 do azimuthfinal:=azimuthfinal+2*pi;
    while AzimuthReverse<0 do AzimuthReverse:=AzimuthReverse+2*pi;
    {Convert meters back to user preferred distance units}
    case units of
      0:{miles} result:=dist*0.0006212;
      1:{kilometers} result:=dist*0.001;
      2:{nautical miles} result:=dist*0.0005398;
    end;
    AzimuthInit:=rad2Deg(AzimuthInit);
    AzimuthFinal:=rad2Deg(AzimuthFinal);
  end;
  except
    raise;
  end;
end;

function GetDistanceTime(OriginLatLng: Shortstring; DestLatLng: Shortstring; out Distance: Shortstring; out Time: Shortstring): Shortstring; stdcall;
begin
  try
    Result := ProcessJson(OriginLatLng+'|'+DestLatLng, DistTime);
    if (Result = '') then
      Result := ErrMsg;
    if Pos('ERROR', Result) = 0 then
    begin
      Distance := Copy(Result,1, Pos('|', Result)-1);
      Time := Copy(Result,Pos('|', Result)+1, Length(Result));
    end;
  except
   On E:Exception do
     Result := ErrMsg +': '+ E.Message;
  end;
end;

function GetRevGeoCode(StrLatLng: Shortstring): Shortstring; stdcall;
begin
  try
    Result := ProcessJson(Strlatlng, Address);
    if Result = '' then
      Result := ErrMsg
  except
   On E:Exception do
     Result := ErrMsg +': '+ E.Message;
  end;
end;

function GetGeoCode(StrAddress: Shortstring): Shortstring; stdcall;
begin
  try
    Result := ProcessJson(StrAddress, LatLong);
    if Result = '' then
      Result := ErrMsg;
  except
  On E:Exception do
    Result := ErrMsg +': '+ E.Message;
  end;
end;

function GetAdminAreaLevel2(StrAddress: Shortstring): Shortstring; stdcall;
begin
  try
    Result := ProcessJson(StrAddress, AdminAreaLevel2);
    if Result = '' then
      Result := ErrMsg;
  except
  On E:Exception do
    Result := ErrMsg +': '+ E.Message;
  end;
end;     //AdminAreaLevel2

function ProcessJSON(RequestStr: String; RequestType: Request): String;
var
  jtr: TJsonTextReader;
  sr: TStringReader;
  retJSON: string;
  RestClient: TRestClient;
  RestRequest: TRestRequest;
  RestResponse: TRestResponse;
  OriginLatLng, DestLatLng: String;
const         //qm-539 Google maps version changed to 3.47 SR
  GoogleAPIKey = 'AIzaSyDJawB5LUbGAQkCJjtMw7F7Le9ZpOQP8oQ&amp;v=3.47&amp';   //qm-931
  GEOCODE_BASE_URL = 'https://maps.googleapis.com/maps/api/geocode/json';
  DISTTIME_BASE_URL = 'https://maps.googleapis.com/maps/api/distancematrix/json';
begin
  try
   try
    RestClient := nil;
    RestRequest := nil;
    RestResponse := nil;
    jtr := nil; sr := nil;
    RestClient:= TRestClient.Create(nil);
    RestClient.Accept := 'application/json, text/plain; q=0.9, text/html;q=0.8,';
    RestClient.AcceptCharset := 'UTF-8,*;q=0.8';
    if (RequestType = Address) or (RequestType = LatLong) or (RequestType = AdminAreaLevel2) then
      RestClient.BaseURL := GEOCODE_BASE_URL + '?key=' + GoogleApiKey else
    if (RequestType = DistTime) then
      RestClient.BaseURL := DISTTIME_BASE_URL + '?key=' + GoogleApiKey;

    RestRequest := TRestRequest.Create(nil);
    RestRequest.Client := RestClient;
    Case RequestType of
      Address:
        RestRequest.AddParameter('latlng',RequestStr,TRESTRequestParameterKind.pkGETorPOST);
      LatLong:
        RestRequest.AddParameter('address',RequestStr,TRESTRequestParameterKind.pkGETorPOST);
      DistTime:
      begin
        OriginLatLng := Copy(RequestStr,1, Pos('|', RequestStr)-1);
        DestLatLng := Copy(RequestStr,Pos('|', RequestStr)+1, Length(RequestStr));
        RestRequest.AddParameter('units','imperial',TRESTRequestParameterKind.pkGETorPOST);
        RestRequest.AddParameter('origins',OriginLatLng,TRESTRequestParameterKind.pkGETorPOST);
        RestRequest.AddParameter('destinations',DestLatLng,TRESTRequestParameterKind.pkGETorPOST);
      end;
      AdminAreaLevel2:
        RestRequest.AddParameter('address',RequestStr,TRESTRequestParameterKind.pkGETorPOST);
    end;
    RestResponse := TRestResponse.Create(nil);
    RestResponse.ContentType := 'application/json';
    RestRequest.Response := RestResponse;
    RestRequest.Execute;
    retJSON := RestResponse.Content;
    if pos(SUCCESS, retJSON) = 0 then
    begin
      Result := ErrMsg + ': ' + RetJSON;
    end else
    begin
      sr := TStringReader.Create(retJSON);
      jtr := TJsonTextReader.Create(sr);
      Result := '';
      while jtr.Read do
      begin
        if jtr.TokenType = TJsonToken.StartObject then
        begin
          if RequestType = Address then
          begin
            Result := ProcessAddressRead(jtr);
            break;
          end
          else
          if RequestType = LatLong then
          begin
            Result := ProcessLatLongRead(jtr);
            break;
          end
          else
          if RequestType = DistTime then
          begin
            Result := ProcessDistTimeRead(jtr);
            break;
          end
          else
          if RequestType = AdminAreaLevel2 then     //qm-391 added to return county    sr
          begin
            Result := ProcessAdminAreaLevel2(jtr);
            break;
          end
        end;
      end;
    end;
   except
    raise;
   end;
  finally
    jtr.Free;
    sr.Free;
    RestClient.Free;
    RestResponse.Free;
    RestRequest.Free;
  end;
end;

function ProcessDistTimeRead(jtr: TJsonTextReader): String;
var
  Distance, Time: String;
begin
  Result := '';
  while jtr.Read do
  begin
    if jtr.TokenType = TJsonToken.PropertyName then
    begin
      if jtr.Value.ToString = 'distance' then
      begin
        jtr.Read;
        jtr.Read;
        jtr.Read;
        Distance := jtr.Value.ToString;
      end;
      if jtr.Value.ToString = 'duration' then
      begin
        jtr.Read;
        jtr.Read;
        jtr.Read;
        Time := jtr.Value.ToString;
        break;
      end;
    end;
  end;
  if (Distance <> '') and (Time <> '') then
    Result := Distance + '|' + Time;
end;

function ProcessLatLongRead(jtr: TJsonTextReader): String;
begin
  Result := '';
  while jtr.Read do
  begin
    if jtr.TokenType = TJsonToken.PropertyName then
    begin
      if jtr.Value.ToString = 'location' then
      begin
        jtr.Read;
        jtr.Read;
        jtr.Read;
        Result := jtr.Value.ToString;
        jtr.Read;
        jtr.Read;
        Result := Result +',' + jtr.Value.ToString;
        break;
      end;
    end;
  end;
end;

function ProcessAddressRead(jtr: TJsonTextReader): String;
begin
  Result := '';
  while jtr.Read do
  begin
    if jtr.TokenType = TJsonToken.PropertyName then
    begin
      if jtr.Value.ToString = 'formatted_address' then
      begin
        jtr.Read;
        Result := jtr.Value.AsString;
        break;
      end;
    end;
  end;
end;

function ProcessAdminAreaLevel2(jtr: TJsonTextReader): String;  //qm-391    //added to return county    sr
var
  KeyValue: String;
begin
  Result := '';
  while jtr.Read do
  begin
    if jtr.TokenType = TJsonToken.PropertyName then
    begin
      if jtr.Value.ToString = 'address_components' then
      begin
        jtr.Read;
        If jtr.TokenType = TJsonToken.StartArray then
        begin
          While jtr.Read do
          begin
            If jtr.TokenType = TJsonToken.StartObject then
            begin
              jtr.Read;
              if jtr.Value.ToString = 'long_name' then
               jtr.Read;
              KeyValue := jtr.Value.AsString;

              repeat
               Jtr.Read;
              until Jtr.TokenType = TJsonToken.StartArray;
              Jtr.Read;
              if jtr.Value.ToString = 'administrative_area_level_2' then
              begin
                Result := KeyValue;
                break;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

exports
  GetRevGeoCode, GetGeoCode, GetDistanceTime, GetHeading, GetAdminAreaLevel2;  //added to return county  sr
begin

end.
