unit TicketManTest;

interface

uses
  Classes, TestFrameWork, SysUtils, DB, SyncEngineU, SingleEngineTest;

type
  TTicketMangementTest = class(TBaseTest)
  public
  protected
  published
    procedure TestEmptyTicketList;
  end;


implementation

{ TTicketMangementTest }

procedure TTicketMangementTest.TestEmptyTicketList;
begin
  // Run these, so we fail if an exception is thrown.  An error had been reported.
  //Engine.Server.GetTicketsForLocator(50, 0, True, False);
  //Engine.Server.GetTicketsForLocator(50, 999, False, False);
  Check(True);
end;

initialization
  TestFramework.RegisterTest(TTicketMangementTest.Suite);

end.
