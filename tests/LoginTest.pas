unit LoginTest;

interface

uses
  Classes, TestFrameWork, SysUtils, DB, ServerProxy, DMu, StateMgr, SingleEngineTest;

type
  TLoginTest = class(TBaseTest)
  private
    procedure TryNamePw(const UserName, Password: string);
    procedure ExpectFailure(const UserName, Password: string);
    function CreateDM(TheState: TUQState): TDM;
    function GetPasswordForLogin(const LoginID: string): string;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestLoginFailsBlank;
    procedure TestLoginFailsUser;
    procedure TestLoginFailsPW;
    procedure TestLoginSuccess;
    procedure TestCachedLogin;
  end;

implementation

uses
  SyncEngineU;

{ TLoginTest }

procedure TLoginTest.ExpectFailure(const UserName, Password: string);
begin
  try
    TryNamePw(UserName, Password);
    Fail('Login succeeded when failure expected: ' + UserName + '/' + Password);
  except
    on E: ELoginFailure do
      Check(True);
  end;
end;

procedure TLoginTest.TestLoginFailsBlank;
begin
  ExpectFailure('', '');
end;

procedure TLoginTest.TestLoginFailsPW;
begin
  ExpectFailure('03636', 'zzz');
end;

procedure TLoginTest.TestLoginFailsUser;
begin
  ExpectFailure('csew', '1234');
end;

procedure TLoginTest.TestLoginSuccess;
begin
  TryNamePw('03636', GetPasswordForLogin('03636'));
end;

procedure TLoginTest.TryNamePw(const UserName, Password: string);
var
  D1: TDM;
  State: TUQState;
begin
  State := TUQState.Create;
  D1 := CreateDM(State);
  try
    D1.Connect(UserName, Password);
  finally
    D1.Free;
    State.Free;
  end;
end;

procedure TLoginTest.TestCachedLogin;
var
  D1: TDM;
  State: TUQState;

  procedure TryIt(ExpectedCount: Integer; ExpectedEmpID: Integer);
  begin
    D1 := CreateDM(State);
    try
      D1.Connect('207', '1234');
      CheckEquals(ExpectedCount, Server.RoundTripCount);
      CheckEquals(ExpectedEmpID, D1.EmpID);
    finally
      D1.Free;
    end;
  end;

begin
  // Preserve the state across connects
  State := TUQState.Create;
  try
    TryIt(1, 207);
    TryIt(1, 207);  // after the second login, there is still only one roundtrip
    TryIt(1, 207);  // after the third login, there is still only one roundtrip
  finally
    State.Free;
  end;
end;

function TLoginTest.CreateDM(TheState: TUQState): TDM;
begin
  Result := TDM.Create(nil);
  Result.UQState := TheState;
  Result.SetupQMService;
  Result.Engine := Engine;
  Result.TestMode := True;
end;

procedure TLoginTest.SetUp;
begin
  inherited;
end;

procedure TLoginTest.TearDown;
begin
  inherited;
end;

function TLoginTest.GetPasswordForLogin(const LoginID: string): string;
begin
  // TODO: This should read from the DB so we do not have to keep updating it
  Result := '1234';
  if LoginID = '00235' then
    Result := 'adak13'
  else if LoginID = '03636' then
    Result := '1234';
end;

initialization
  TestFramework.RegisterTest(TLoginTest.Suite);

{
both:
  validate on connect if not already validated.
  otherwise, cache that we have the same

both:
  validate on sync
}
end.

