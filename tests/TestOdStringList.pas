unit TestOdStringList;

interface

uses
  TestFramework, Classes, OdMiscUtils, SysUtils;

type
  TTestOdStringList = class(TTestCase)
  private
    OdStringList : TOdStringList;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestUnionStringArray;
    procedure TestUnionStrings;
    procedure TestIntersectStringsArray;
    procedure TestIntersectStrings;
    procedure TestDifferenceStringsArray;
    procedure TestDifferenceStrings;
  end;

implementation

procedure TTestOdStringList.SetUp;
begin
  OdStringList := TOdStringList.Create;
end;

procedure TTestOdStringList.TearDown;
begin
  FreeAndNil(OdStringList);
end;

procedure TTestOdStringList.TestUnionStringArray;
var
  AddStrings : TStringArray;
begin
  SetLength(AddStrings, 3);
  AddStrings[0] := 'C';
  AddStrings[1] := 'M';
  AddStrings[2] := 'N';

  OdStringList.CommaText := '-R,C,H,N,NC,NP,O,OH,ZZZ';
  OdStringList.Union(AddStrings);

  CheckTrue(OdStringList.CommaText = '-R,C,H,N,NC,NP,O,OH,ZZZ,M', Format('Union result contains %s when it should contain %s',[OdStringList.CommaText, '-R,C,H,N,NC,NP,O,OH,ZZZ,M']) );
end;

procedure TTestOdStringList.TestUnionStrings;
var
  AddStrings : TStringList;
begin
  OdStringList.CommaText := '-R,C,H,N,NC,NP,O,OH,ZZZ';

  AddStrings := TStringList.Create;
  try
    AddStrings.CommaText := 'C,M,N';
    OdStringList.Union(AddStrings);
  finally
    FreeAndNil(AddStrings);
  end;

  CheckTrue(OdStringList.CommaText = '-R,C,H,N,NC,NP,O,OH,ZZZ,M', Format('Union result contains %s when it should contain %s',[OdStringList.CommaText, '-R,C,H,N,NC,NP,O,OH,ZZZ,M']) );
end;

procedure TTestOdStringList.TestIntersectStringsArray;
var
  MatchStrings : TStringArray;
begin
  SetLength(MatchStrings, 5);
  MatchStrings[0] := 'C';
  MatchStrings[1] := 'M';
  MatchStrings[2] := 'N';
  MatchStrings[3] := 'X';
  MatchStrings[4] := '-R';

  OdStringList.CommaText := '-R,C,H,N,NC,NP,O,OH,ZZZ';
  OdStringList.Intersect(MatchStrings);

  CheckTrue(OdStringList.CommaText = 'C,N,-R', Format('Intersect result contains %s when it should contain %s',[OdStringList.CommaText,'C,N,-R']) );
end;

procedure TTestOdStringList.TestIntersectStrings;
var
  MatchStrings : TStringList;
begin
  OdStringList.CommaText := '-R,C,H,N,NC,NP,O,OH,ZZZ';

  MatchStrings := TStringList.Create;
  try
    MatchStrings.CommaText := 'C,M,N,X,-R';
    OdStringList.Intersect(MatchStrings);
  finally
    FreeAndNil(MatchStrings);
  end;

  CheckTrue(OdStringList.CommaText = 'C,N,-R', Format('Intersect result contains %s when it should contain %s',[OdStringList.CommaText,'C,N,-R']) );
end;

procedure TTestOdStringList.TestDifferenceStrings;
var
  StringsToDifference : TStringList;
begin
  OdStringList.CommaText := 'A,B,C';

  StringsToDifference := TStringList.Create;
  try
    StringsToDifference.CommaText := 'C,D,E';
    OdStringList.SymmetricDifference(StringsToDifference);
  finally
    FreeAndNil(StringsToDifference);
  end;

  CheckTrue(OdStringList.CommaText = 'A,B,D,E', Format('Difference result contains %s when it should contain %s',[OdStringList.CommaText,'A,B,D,E']) );
end;

procedure TTestOdStringList.TestDifferenceStringsArray;
var
  StringsToDifference : TStringArray;
begin
  SetLength(StringsToDifference, 3);
  StringsToDifference[0] := 'C';
  StringsToDifference[1] := 'D';
  StringsToDifference[2] := 'E';

  OdStringList.CommaText := 'A,B,C';
  OdStringList.SymmetricDifference(StringsToDifference);

  CheckTrue(OdStringList.CommaText = 'A,B,D,E', Format('Difference result contains %s when it should contain %s',[OdStringList.CommaText,'A,B,D,E']) );
end;

initialization
  RegisterTest(TTestOdStringList.Suite);

end.


