unit BillPeriodTest;

interface

uses
  TestFrameWork, Classes, SysUtils, BillPeriod;

type
  TBillPeriodTest = class(TTestCase)
  private
    P: TBillPeriod;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestMakeNew;
    procedure TestSetSaturday;
    procedure TestSetNonSaturday;
    procedure TestSetMonthly;
    procedure TestSetAnotherMonth;
    procedure TestSetNotMonthEnd;
    procedure TestSetSingleDay;
    procedure TestSetBiMonthly1;
    procedure TestSetBiMonthly2;
    procedure TestSetBiMonthlyWrongEnd;
    procedure TestSetBogusSpan;
  end;

implementation

uses
  OdIsoDates;

{ TBillPeriodTest }

procedure TBillPeriodTest.SetUp;
begin
  inherited;
  P := TBillPeriod.Create(IsoStrToDate('2005-03-29'));
end;

procedure TBillPeriodTest.TearDown;
begin
  inherited;
  FreeAndNil(P);
end;

procedure TBillPeriodTest.TestMakeNew;
begin
  CheckEquals('2005-03-20', IsoDateToStr(P.StartDate));
  CheckEquals('2005-03-06', IsoDateToStr(P.LookBackDate));
  CheckEquals('2005-03-26', IsoDateToStr(P.DisplayEndDate));
  CheckEquals('2005-03-27', IsoDateToStr(P.EndDate));
end;

procedure TBillPeriodTest.TestSetSaturday;
begin
  P.DisplayEndDate := IsoStrToDate('2005-02-26');
  CheckEquals('2005-02-20', IsoDateToStr(P.StartDate));
  CheckEquals('2005-02-06', IsoDateToStr(P.LookBackDate));
  CheckEquals('2005-02-26', IsoDateToStr(P.DisplayEndDate));
  CheckEquals('2005-02-27', IsoDateToStr(P.EndDate));
end;

procedure TBillPeriodTest.TestSetNonSaturday;
begin
  ExpectedException := EBillPeriodException;
  P.DisplayEndDate := IsoStrToDate('2005-02-23');
end;

procedure TBillPeriodTest.TestSetMonthly;
begin
  P.Span := PERIOD_MONTH;
  // It will go back to the prior month
  CheckEquals('2005-01-01', IsoDateToStr(P.LookBackDate));
  CheckEquals('2005-02-01', IsoDateToStr(P.StartDate));
  CheckEquals('2005-02-28', IsoDateToStr(P.DisplayEndDate));
  CheckEquals('2005-03-01', IsoDateToStr(P.EndDate));
end;

procedure TBillPeriodTest.TestSetAnotherMonth;
begin
  P.Span := PERIOD_MONTH;
  P.DisplayEndDate := IsoStrToDate('2005-06-30');
  CheckEquals('2005-05-01', IsoDateToStr(P.LookBackDate));
  CheckEquals('2005-06-01', IsoDateToStr(P.StartDate));
  CheckEquals('2005-06-30', IsoDateToStr(P.DisplayEndDate));
  CheckEquals('2005-07-01', IsoDateToStr(P.EndDate));
end;

procedure TBillPeriodTest.TestSetNotMonthEnd;
begin
  P.Span := PERIOD_MONTH;
  ExpectedException := EBillPeriodException;
  P.DisplayEndDate := IsoStrToDate('2005-02-23');
end;

procedure TBillPeriodTest.TestSetSingleDay;
begin
  P.Span := PERIOD_DAY;
  P.DisplayEndDate := IsoStrToDate('2005-05-15');
  CheckEquals('2005-05-15', IsoDateToStr(P.LookBackDate));
  CheckEquals('2005-05-15', IsoDateToStr(P.StartDate));
  CheckEquals('2005-05-15', IsoDateToStr(P.DisplayEndDate));
  CheckEquals('2005-05-16', IsoDateToStr(P.EndDate));
end;

procedure TBillPeriodTest.TestSetBiMonthly1;
begin
  P.Span := PERIOD_HALF;
  P.DisplayEndDate := IsoStrToDate('2005-06-15');
  CheckEquals('2005-05-16', IsoDateToStr(P.LookBackDate));
  CheckEquals('2005-06-01', IsoDateToStr(P.StartDate));
  CheckEquals('2005-06-15', IsoDateToStr(P.DisplayEndDate));
  CheckEquals('2005-06-16', IsoDateToStr(P.EndDate));
end;

procedure TBillPeriodTest.TestSetBiMonthly2;
begin
  P.Span := PERIOD_HALF;
  P.DisplayEndDate := IsoStrToDate('2005-06-30');
  CheckEquals('2005-06-01', IsoDateToStr(P.LookBackDate));
  CheckEquals('2005-06-16', IsoDateToStr(P.StartDate));
  CheckEquals('2005-06-30', IsoDateToStr(P.DisplayEndDate));
  CheckEquals('2005-07-01', IsoDateToStr(P.EndDate));
end;

procedure TBillPeriodTest.TestSetBiMonthlyWrongEnd;
begin
  P.Span := PERIOD_HALF;
  ExpectedException := EBillPeriodException;
  P.DisplayEndDate := IsoStrToDate('2005-02-23');
end;

procedure TBillPeriodTest.TestSetBogusSpan;
begin
  ExpectedException := EBillPeriodException;
  P.Span := 'YO MAMA';
end;

initialization
  TestFramework.RegisterTest(TBillPeriodTest.Suite);

end.

