object MainTestForm: TMainTestForm
  Left = 0
  Top = 0
  Caption = 'Test Event Store'
  ClientHeight = 304
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object CreateEventsBtn: TButton
    Left = 8
    Top = 8
    Width = 113
    Height = 25
    Caption = 'Create Events'
    TabOrder = 0
    OnClick = CreateEventsBtnClick
  end
  object ReceiveEventsBtn: TButton
    Left = 127
    Top = 8
    Width = 113
    Height = 25
    Caption = 'Receive Events'
    TabOrder = 1
    OnClick = ReceiveEventsBtnClick
  end
  object LogMemo: TMemo
    Left = 0
    Top = 49
    Width = 527
    Height = 255
    Align = alBottom
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
    WordWrap = False
  end
  object CommitChk: TCheckBox
    Left = 256
    Top = 12
    Width = 177
    Height = 17
    Caption = 'Commit DB Transaction'
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object DBConn: TADOConnection
    LoginPrompt = False
    Left = 16
    Top = 56
  end
  object EventQueue: TADODataSet
    Connection = DBConn
    Parameters = <>
    Left = 48
    Top = 56
  end
  object Events: TADOQuery
    Connection = DBConn
    Parameters = <>
    Left = 77
    Top = 56
  end
end
