unit MainFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, Contnrs,
  EventSource2007, MSSqlEventSource2007, QMEventsNoThrift;

type
  TMainTestForm = class(TForm)
    CreateEventsBtn: TButton;
    DBConn: TADOConnection;
    EventQueue: TADODataSet;
    ReceiveEventsBtn: TButton;
    LogMemo: TMemo;
    CommitChk: TCheckBox;
    Events: TADOQuery;
    procedure CreateEventsBtnClick(Sender: TObject);
    procedure ReceiveEventsBtnClick(Sender: TObject);
  private
    procedure TestStoreEvents;
    procedure CheckEvents;
    procedure TestReceiveEvents;
  end;

var
  MainTestForm: TMainTestForm;

implementation

uses
  OdADOUtils;

{$R *.dfm}

procedure TMainTestForm.CreateEventsBtnClick(Sender: TObject);
begin
  ConnectAdoConnectionWithIni(DBConn, ExtractFilePath(ParamStr(0)) + 'qmserver.ini');
  TestStoreEvents;
end;

procedure TMainTestForm.ReceiveEventsBtnClick(Sender: TObject);
begin
  ConnectAdoConnectionWithIni(DBConn, ExtractFilePath(ParamStr(0)) + 'qmserver.ini');
  TestReceiveEvents;
end;

procedure TMainTestForm.TestReceiveEvents;

  procedure ShowTheEvent;
  begin
    LogMemo.Lines.Add(EventQueue.FieldByName('new_event_id').AsString +
      #09' AggrType: ' + Events.FieldByName('aggregate_type').AsString +
      #09' AggrID: ' + Events.FieldByName('aggregate_id').AsString +
      #09' Version: ' + Events.FieldByName('version_num').AsString +
      #09' EventData: ' + Events.FieldByName('event_data').AsString);
  end;

const
  QUESQL = 'RECEIVE CONVERT(VARCHAR(MAX), message_body) AS new_event_id ' +
    'FROM NewEventReceiveQueue';
  LOGSQL = 'SELECT * FROM event_log WHERE event_id = :event_id';
begin
  EventQueue.CommandText := QUESQL;
  Events.SQL.Text := LOGSQL;

  DBConn.BeginTrans;
  try
    try
      repeat
        EventQueue.Close;
        EventQueue.Open;
        while not EventQueue.Eof do begin
          Events.Close;
          Events.Parameters.ParamByName('event_id').Value :=
            '{' + EventQueue.FieldByName('new_event_id').AsString + '}';
          Events.Open;
          ShowTheEvent;
          EventQueue.Next;
        end;
        if EventQueue.RecordCount = 0 then
          LogMemo.Lines.Add('No more pending events.');
      until EventQueue.RecordCount = 0;
    except
      on Exception do begin
        DBConn.RollbackTrans;
        raise;
      end;
    end;
  finally
    if CommitChk.Checked then
      DBConn.CommitTrans
    else
      // NOTE: Rolling back 5 consecutive times invokes POISON_MESSAGE_HANDLING
      //       which disables the NewEventReceiveQueue. Reenable it by using:
      //       alter queue NewEventReceiveQueue with status=ON
      DBConn.RollbackTrans;
  end;
end;

procedure TMainTestForm.TestStoreEvents;
var
  EventList: IEventList;
  Key: IAggrKey;
  Closed: TEventImpl;
  Reopened: TEventImpl;
  Store: IEventStore;
begin
  Store := TMSSQLEventStore.Create(nil, DBConn);
  EventList := TEventListImpl.Create;
  Key := TAggrKeyImpl.Create;
  Key.AggrType := atTicket;
  Key.AggrID := 12344321;

  // add a LocateClosed event
  Closed := TEventImpl.Create;
  Closed.LocateClosed := TLocateClosedImpl.Create;
  Closed.LocateClosed.AggregateKey := Key;
  Closed.LocateClosed.LocateID := 11111111;
  Closed.LocateClosed.ClosedByEmpID := 6002;
  Closed.LocateClosed.ClientCode := 'ATT-55';
  Closed.LocateClosed.CallCenter := 'OCC2';
  Closed.LocateClosed.ClosedDate := '2013-12-30T10:00:11.222';
  Closed.LocateClosed.StatusCode := 'M';
  EventList.Items.Add(Closed);

  // add a LocateReopened event
  Reopened := TEventImpl.Create;
  Reopened.LocateReopened := TLocateReopenedImpl.Create;
  Reopened.LocateReopened.AggregateKey := Key;
  Reopened.LocateReopened.LocateID := 11111111;
  Reopened.LocateReopened.ReopenedByEmpID := 6002;
  Reopened.LocateReopened.ClientCode := 'ATT-55';
  Reopened.LocateReopened.CallCenter := 'OCC2';
  Reopened.LocateReopened.ReopenedDate := '2013-12-30T11:11:11.111';
  Reopened.LocateReopened.StatusCode := '-R';
  EventList.Items.Add(Reopened);

  DBConn.BeginTrans;
  try
    Store.SaveEvents(Key, EventList, -1);
    if CommitChk.Checked then begin
      DBConn.CommitTrans;
      LogMemo.Lines.Add('Added 2 events');
    end else begin
      DBConn.RollbackTrans;
      LogMemo.Lines.Add('No events added');
    end;
    CheckEvents;
  except
    DBConn.RollbackTrans;
    raise;
  end;
end;

procedure TMainTestForm.CheckEvents;
const
  PeekSQL = 'select count(*) N from NewEventReceiveQueue';
begin
  EventQueue.CommandText := PeekSQL;
  EventQueue.Open;
  try
    LogMemo.Lines.Add(Format('%d pending event notices', [EventQueue.Fields[0].AsInteger]));
  finally
    EventQueue.Close;
  end;
end;

end.
