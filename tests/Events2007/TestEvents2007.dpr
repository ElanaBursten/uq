program TestEvents2007;

uses
  Forms,
  MainFormU in 'MainFormU.pas' {MainTestForm},
  EventSource2007 in '..\..\server\EventSource2007.pas',
  MSSqlEventSource2007 in '..\..\server\MSSqlEventSource2007.pas',
  QMEventsNoThrift in '..\..\server\QMEventsNoThrift.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainTestForm, MainTestForm);
  Application.Run;
end.
