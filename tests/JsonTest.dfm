object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Form4'
  ClientHeight = 300
  ClientWidth = 497
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    497
    300)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 46
    Height = 13
    Caption = 'Ticket ID:'
  end
  object Button1: TButton
    Left = 187
    Top = 11
    Width = 96
    Height = 25
    Caption = 'Get JSON'
    TabOrder = 0
    OnClick = Button1Click
  end
  object TicketIDEdit: TEdit
    Left = 78
    Top = 13
    Width = 101
    Height = 21
    NumbersOnly = True
    TabOrder = 1
    Text = '1103'
  end
  object JsonMemo: TMemo
    Left = 78
    Top = 42
    Width = 411
    Height = 250
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'JsonMemo')
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
  end
end
