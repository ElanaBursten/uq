unit HttpTest;

interface

uses
  Classes, TestFrameWork, SysUtils, OdWinInet;

type
  THttpTest = class(TTestCase)
  protected
    OdHttp: TOdHttp;
    MemStream: TMemoryStream;
    function Transfer(Threaded: Boolean; const URL: string): Boolean;
  published
    procedure TestDownloadThreaded;
    procedure TestDownloadBlocking;
    procedure TestBadHostNameThreaded;
    procedure TestBadHostNameBlocking;
    procedure TestBadFilePathThreaded;
    procedure TestBadFilePathBlocking;
  end;

implementation

uses Windows, OdHttpDialog;

{ THttpTest }

function THttpTest.Transfer(Threaded: Boolean; const URL: string): Boolean;
begin
  Result := True;
  FreeAndNil(MemStream);
  MemStream := TMemoryStream.Create;
  try
    if Threaded then
      OdHttpGetStream(URL, MemStream, DefaultDialogHttp)
    else
      OdHttpGetStream(URL, MemStream, DefaultBlockingHttp)
  except
    Result := False;
  end;
  if MemStream.Size = 0 then
    Result := False;
end;

procedure THttpTest.TestBadFilePathBlocking;
begin
  // Is there a less rude URL to use here?  Assume localhost has http running?
  Check(not Transfer(False, 'http://localhost/zzzLINUXRULEZ.html'));
end;

procedure THttpTest.TestBadFilePathThreaded;
begin
  Check(not Transfer(True, 'http://localhost/zzzLINUXRULEZ.html'));
end;

procedure THttpTest.TestBadHostNameBlocking;
begin
  Check(not Transfer(False, 'http://www.mzzzzicrosoft.com/'));
end;

procedure THttpTest.TestBadHostNameThreaded;
begin
  Check(not Transfer(True, 'http://www.mzzzzicrosoft.com/'));
end;

procedure THttpTest.TestDownloadBlocking;
begin
  Check(Transfer(False, 'http://localhost/'));
end;

procedure THttpTest.TestDownloadThreaded;
begin
  Check(Transfer(True, 'http://localhost/'));
end;

initialization
  TestFramework.RegisterTest(THttpTest.Suite);

end.

