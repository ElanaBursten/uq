unit DatabaseTest;

interface

uses
  Classes, TestFrameWork, SingleEngineTest, SysUtils, DB, BaseDMTest;

type
  TTransactionTest = class(TBaseDMTest)
  private
    Office: TDataSet;
    procedure DoEdits(Fail: Boolean);
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestTransactionSuccess;
    procedure TestTransactionFailure;
  end;

implementation

uses DMu, DBISAMTb, OdExceptions;

{ TTransactionTest }

procedure TTransactionTest.DoEdits(Fail: Boolean);
begin
  DMod.Database1.StartTransaction;
  try
    // Test a delete, edit, and insert/append
    CheckEquals(3, Office.RecordCount);
    Office.First;
    Office.Delete;
    CheckEquals(2, Office.RecordCount);
    CheckEquals(2, Office.FieldByName('office_id').AsInteger);
    Office.Edit;
    Office.FieldByName('office_name').AsString := 'new name';
    Office.Post;
    Office.AppendRecord([4, 4, 4, 4, nil, True]);
    (Office as TDBISAMDataSet).FlushBuffers;
    if Fail then
      raise EOdNonLoggedException.Create('The transaction should rollback now');
    DMod.Database1.Commit;
  except
    DMod.Database1.Rollback;
  end;
end;

procedure TTransactionTest.SetUp;
begin
  inherited;
  Assert(Assigned(DMod));
  Office := DMod.Engine.OpenWholeTable('office');
  while not Office.Eof do
    Office.Delete;
  Office.AppendRecord([1, 1, 1, 1, nil, True]);
  Office.AppendRecord([2, 2, 2, 2, nil, True]);
  Office.AppendRecord([3, 3, 3, 3, nil, True]);
end;

procedure TTransactionTest.TearDown;
begin
  inherited;
  FreeAndNil(Office);
end;

procedure TTransactionTest.TestTransactionFailure;
begin
  DoEdits(True);
  CheckEquals(3, Office.RecordCount);
  Check(not Office.Locate('office_id', 4, []));
  Check(Office.Locate('office_id', 1, []));
  Check(Office.Locate('office_id', 3, []));
  Check(Office.Locate('office_id', 2, []));
  Check(Office.FieldByName('office_name').AsString = '2');
end;

procedure TTransactionTest.TestTransactionSuccess;
begin
  DoEdits(False);
  CheckEquals(3, Office.RecordCount);
  Check(Office.Locate('office_id', 4, []));
  Check(not Office.Locate('office_id', 1, []));
  Check(Office.Locate('office_id', 3, []));
  Check(Office.Locate('office_id', 2, []));
  CheckEquals('new name', Office.FieldByName('office_name').AsString);
end;

initialization
  TestFramework.RegisterTest(TTransactionTest.Suite);

end.

