unit MainProj4TestForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Math;

type
  TForm3 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    XCoord: TEdit;
    YCoord: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
    LatLongMemo: TMemo;
    procedure Button1Click(Sender: TObject);
  end;

var
  Form3: TForm3;

implementation

uses Proj4, Points;

{$R *.dfm}

procedure TForm3.Button1Click(Sender: TObject);
const
  EPSG_26785 = '+title=Maryland_NAD27_SPCS+proj=lcc+lat_1=38.3+lat_2=39.45+lat_0=37.83333333333334+lon_0=-77+x_0=243840.4876809754+y_0=0+ellps=clrk66+datum=NAD27+to_meter=0.3048006096012192+no_defs';
var
  P4: TProj4;
  xy: T2DPoint;
  LatLong: RLatLon;
begin
  P4 := TProj4.create(EPSG_26785, True);
  try
    xy := T2DPoint.create;
    xy.x := StrToFloat(XCoord.Text);
    xy.y := StrToFloat(YCoord.Text);
    LatLong := P4.pjInv(xy.x, xy.y, True);
    LatLongMemo.Lines.Add(FloatToStr(LatLong.lat) + ', ' + FloatToStr(LatLong.lon));
  finally
    P4.Free;
    xy.Free;
  end;
end;

end.
