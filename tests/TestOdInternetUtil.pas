unit TestOdInternetUtil;

interface

uses
  TestFramework, ADODB, Classes, Contnrs, SysUtils, Variants, IniFiles,
  Forms, OdInternetUtil, Dialogs, IdException;

type
  TestEMailSender = class(TTestCase)
  private
    IniFilename: string;
    AttachedFiles: TStringList;
    Ini: TIniFile;
{todo review    function EmailFrom: string; }
    function EmailTo: string;
{todo review    function SMTPServer(Secure: Boolean=False): string;
    function SMTPUser: string;
    function SMTPPassword: string;}
    function FilesToAttach: string;
{todo review    function MultiTo: string;    }

  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestSendEMail;
    procedure TestSendEMailWithAttachments;
    procedure TestSendEMailWithIni;
    procedure TestSendEMailWithIniMissingSettings;
    procedure TestSendEMailWithIniIgnored;
    procedure TestSendEMailMultiDestination;
    procedure TestSecureSendEMail;
    procedure TestSecureSendEMailWithAttachments;
    procedure TestSecureSendEMailBadServer;
    procedure TestSecureSendEMailBadAuth;
  end;

implementation

procedure TestEMailSender.SetUp;
begin
  IniFilename := ExtractFilePath(Application.ExeName) + 'TestEmailSend.ini';
  Ini := TIniFile.Create(IniFilename);
  AttachedFiles := TStringList.Create;
end;

procedure TestEMailSender.TearDown;
begin
  FreeAndNil(Ini);
  FreeAndNil(AttachedFiles);
end;

procedure TestEMailSender.TestSendEmail;
begin
  SendEmailWithIniSettings(IniFilename, 'EmailSettings', EmailTo, 'TestSendEmail', 'Body text is here', nil, False, '');
  Check(True);
end;

procedure TestEMailSender.TestSecureSendEmail;
begin
//todo review  SendEmail(EmailTo, EmailFrom, 'TestSecureSendEmail', 'Body text is here', SMTPServer(True), nil, SMTPUser, SMTPPassword);
  SendEmailWithIniSettings(IniFilename, 'EmailSettings', EmailTo, 'TestSendEmail', 'Body text is here', nil, False, '');
  Check(True);
end;

procedure TestEMailSender.TestSendEMailWithAttachments;
begin
  AttachedFiles.Clear;
  AttachedFiles.DelimitedText := FilesToAttach;
  CheckTrue(AttachedFiles.Count > 0, 'No attached files listed in ini file');

//todo review  SendEmail(EmailTo, EmailFrom, 'TestSendEmailWithAttachments', 'Body text is here', SMTPServer, AttachedFiles, '', '');
  SendEmailWithIniSettings(IniFilename, 'EmailSettings', EmailTo, 'TestSendEmail', 'Body text is here', AttachedFiles, False, '');
  Check(True);
end;

procedure TestEMailSender.TestSendEMailWithIni;
begin
  SendEmailWithIniSettings(IniFileName, 'Email', EmailTo, 'TestSendEmailWithIni', 'Body text is here', nil);
  Check(True);
end;

procedure TestEMailSender.TestSendEMailWithIniIgnored;
begin
  SendEmailWithIniSettings(IniFileName, 'TestIgnored', EmailTo, 'FAILED-TestSendEmailWithIniIgnored', 'Body text is here', nil);
  Check(True);
end;

procedure TestEMailSender.TestSendEMailWithIniMissingSettings;
begin
  try
    SendEmailWithIniSettings(IniFileName, 'TestMissingSettings', EMailTo, 'TestMissingSettings', 'Body text is here', nil);
    Check(False);
  except
    on E: Exception do
      Check(True);
  end;
end;

procedure TestEMailSender.TestSecureSendEMailBadServer;
begin
  try
    SendEmailWithIniSettings(IniFileName, 'SecureSMTP-BadServer', EMailTo, 'SecureSMTP-BadServer', 'Body text is here', nil);
    Check(False);
  except
    // TODO : Narrow down expected results
    on E: EIdException do
      Check(True);
  end;
end;

procedure TestEMailSender.TestSecureSendEMailBadAuth;
begin
  try
    SendEmailWithIniSettings(IniFileName, 'SecureSMTP-BadAuth', EMailTo, 'SecureSMTP-BadServer', 'Body text is here', nil);
    Check(False);
  except
    // TODO : Narrow down expected results
    on E: EIdException do
      Check(True);
  end;
end;

procedure TestEMailSender.TestSecureSendEMailWithAttachments;
begin
  AttachedFiles.Clear;
  AttachedFiles.DelimitedText := FilesToAttach;
  CheckTrue(AttachedFiles.Count > 0, 'No attached files listed in ini file');

//todo review  SendEmail(EMailTo, EMailFrom, 'TestSecureSendEmailWithAttachments', 'Body text is here', SMTPServer(True), AttachedFiles, SMTPUser, SMTPPassword);
  SendEmailWithIniSettings(IniFilename, 'EmailSettings', EmailTo, 'TestSendEmail', 'Body text is here', nil, False, '');
  Check(True);
end;

procedure TestEMailSender.TestSendEMailMultiDestination;
begin
//todo review  SendEmail(MultiTo, EMailFrom, 'TestSendEmailMultiDestination', 'Body text is here', SMTPServer, nil, '', '');
  SendEmailWithIniSettings(IniFilename, 'EmailSettings', EmailTo, 'TestSendEmail', 'Body text is here', nil, False, '');
  Check(True);
end;

// Configuration readers:
{todo review
function TestEMailSender.EmailFrom: string;
begin
  Result := Ini.ReadString('Email', 'From', '');
end;
}
function TestEMailSender.EmailTo: string;
begin
  Result := Ini.ReadString('Email', 'To', '');
end;

{todo review
function TestEMailSender.MultiTo: string;
begin
  Result := Ini.ReadString('Email', 'MultiTo', '');
end;}

function TestEMailSender.FilesToAttach: string;
begin
  Result := Ini.ReadString('Email', 'FilesToAttach', '');
end;

{todo review
function TestEMailSender.SMTPUser: string;
begin
  Result := Ini.ReadString('SecureSMTP', 'SMTPUser', '');
end;

function TestEMailSender.SMTPPassword: string;
begin
  Result := Ini.ReadString('SecureSMTP', 'SMTPPassword', '');
end;

function TestEMailSender.SMTPServer(Secure: Boolean): string;
begin
  if Secure then
    Result := Ini.ReadString('SecureSMTP', 'SMTPServer', '')
  else
    Result := Ini.ReadString('Email', 'SMTPServer', '');
end;  }

initialization
  RegisterTest(TestEMailSender.Suite);

end.

