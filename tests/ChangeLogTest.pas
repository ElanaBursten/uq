unit ChangeLogTest;

// Unused test

interface

uses
  Classes, TestFrameWork, SysUtils, DB, SyncEngineU, TestConfig,
  SingleEngineTest;

type
  TChangeLogTest = class(TSingleEngineTest)
  published
    procedure TestChangeLog;
  public
    function TableName: string; override;
  end;

implementation

{ TChangeLogTest }

function TChangeLogTest.TableName: string;
begin
  Result := 'widget';
end;

procedure TChangeLogTest.TestChangeLog;
begin
  {
  add a record
  there should be nothing in the change log
  modify a field
  should be one in the change log
  modify a field
  should be 2 in the change log

  Look at old versions
  }

  with engine.OpenTableWithEditTracking(TableName) do try
    AppendRecord([-1, 'Kyle', 14]);
    engine.Sync(0);
    CheckRecordCount(1);
  finally
    Free;
  end;

  CheckEquals(0, Engine.TotalRecordCount('changelog'));

  with engine.OpenTableWithEditTracking(TableName) do try
    Edit;
    FieldByName('owner_name').AsString := 'Bob';
    Post;
    engine.Sync(0);
    CheckRecordCount(1);
  finally
    Free;
  end;

  CheckEquals(1, Engine.TotalRecordCount('changelog'));      // one change noted
end;

initialization
  //TestFramework.RegisterTest(TChangeLogTest.Suite);

end.

