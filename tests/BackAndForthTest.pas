unit BackAndForthTest;

interface

uses
  Classes, Contnrs, TestFrameWork, SysUtils, DB, SyncEngineU, SingleEngineTest;

type
  TBackAndForthTest = class(TBaseTest)
  private
    engine2: TLocalCache;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestInsert;
    procedure TestChange;
    procedure TestDelete;
  end;

implementation

{ TBackAndForthTest }

procedure TBackAndForthTest.SetUp;
begin
  inherited;
  InitAndEmptyLocalEngine(Engine2, 'data2');
  TestService.EmptyServerTables;
end;

procedure TBackAndForthTest.TearDown;
begin
  inherited;
  FreeAndNil(engine2);
end;

procedure TBackAndForthTest.TestChange;
var
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  TestInsert;

  with Engine.OpenTableWithEditTracking('office') do try
    Check(Locate('office_name', 'Sacremento', []));
    Edit;
    FieldByName('profit_center').AsString := 'BBB';
    Post;
  finally
    Free;
  end;

  Errors := TObjectList.Create;
  try
    Check(engine.TableHasPendingUpdates('office'));
    Engine.Sync(0, Errors, SyncRequestSource);
    Check(not engine.TableHasPendingUpdates('office'));

    engine2.Sync(0, Errors, SyncRequestSource);
  finally
    FreeAndNil(Errors);
  end;

  with engine2.OpenTableWithEditTracking('office') do try
    Check(Locate('office_name', 'Sacremento', []));
    CheckEquals('BBB', FieldByName('profit_center').AsString);
  finally
    Free;
  end;
end;

procedure TBackAndForthTest.TestDelete;
var
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  TestInsert;

  with Engine.OpenTableWithEditTracking('office') do try
    Check(Locate('office_name', 'Sacremento', []));
    Edit;
    FieldByName('active').AsBoolean := False;
    Post;
  finally
    Free;
  end;

  Errors := TObjectList.Create;
  try
    Engine.Sync(0, Errors, SyncRequestSource);

    Engine2.Sync(0, Errors, SyncRequestSource);
  finally
    FreeAndNil(Errors);
  end;

  CheckEquals(2, engine2.ActiveRecordCount('office'));
end;

procedure TBackAndForthTest.TestInsert;
var
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  // put them in engine 1
  with Engine.OpenTableWithEditTracking('office') do try
    AppendRecord([-1, 'Boise']);
    AppendRecord([-2, 'Sacremento', 'AAA']);
    AppendRecord([-3, 'Iowa City']);
  finally
    Free;
  end;

  Errors := TObjectList.Create;
  try
    Engine.Sync(0, Errors, SyncRequestSource);
    Engine2.Sync(0, Errors, SyncRequestSource);
  finally
    FreeAndNil(Errors);
  end;

  CheckEquals(3, engine2.ActiveRecordCount('office'));
end;

{
procedure TBackAndForthTest.TestLocatesFollowTicket;
begin
  LoadSampleTickets;

  Engine.Sync(0);
  engine2.Sync(KyleId);

  AssignTicket('2225', KyleId);

  with engine2.OpenTableWithEditTracking('ticket') do try
    Check(not Locate('ticket_number', '2225', []));  // not in Kyle's DB yet
  finally
    Free;
  end;

  Engine.Sync(0);
  engine2.Sync(KyleId);

  with engine2.OpenTableWithEditTracking('ticket') do try
    Check(Locate('ticket_number', '2225', []));      // make sure I got it.
  finally
    Free;
  end;

  with engine2.OpenQuery('select count(*) from locate inner join ticket on locate.ticket_id=ticket.ticket_id where ticket.ticket_number=''2225''') do try
    CheckEquals(6, Fields[0].AsInteger, 'locates came with the newly arrived ticket');
  finally
    Free;
  end;
end;
}

initialization
  TestFramework.RegisterTest(TBackAndForthTest.Suite);

end.

