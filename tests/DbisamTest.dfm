object DbisamTestDM: TDbisamTestDM
  OldCreateOrder = False
  Left = 215
  Top = 114
  Height = 480
  Width = 696
  object Table1: TDBISAMTable
    BeforePost = Table1BeforePost
    EngineVersion = '4.34 Build 7'
    FieldDefs = <
      item
        Name = 'Name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Address'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Age'
        DataType = ftInteger
      end
      item
        Name = 'Active'
        DataType = ftBoolean
      end>
    TableName = 'TestTable'
    StoreDefs = True
    Left = 136
    Top = 80
  end
  object Query1: TDBISAMQuery
    EngineVersion = '4.34 Build 7'
    RequestLive = True
    SQL.Strings = (
      'select * from TestTable'
      'where active=true')
    Params = <>
    Left = 216
    Top = 80
  end
end
