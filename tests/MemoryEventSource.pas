{
 This unit is only meant for testing. It stores all events in a TObjectDictionary
 so the persistence only lasts as long as the application is running.
}

unit MemoryEventSource;

interface

uses
  SysUtils, Generics.Collections, Thrift.Collections,
  QMLogic2ServiceLib, EventSource, EventUtils;

type
  TMemoryEventStore = class(TBaseEventStore, IEventStore)
  private
    Dictionary: TObjectDictionary<string, IThriftList<IEventDescriptor>>;
  protected
    function LoadEventDescriptorsForAggregate(Key: IAggrKey): IThriftList<IEventDescriptor>; override;
    procedure PersistEventDescriptors(Key: IAggrKey; NewEventDescriptors: IThriftList<IEventDescriptor>; ExpectedVersion: Integer); override;
    function LoadEventDescriptorsSince(const EventID: string; const Limit: Integer): IThriftList<IEventDescriptor>; override;
  public
    constructor Create(APublisher: IEventPublisher); override;
    destructor Destroy; override;
  end;

implementation

{ TMemoryEventStore }

constructor TMemoryEventStore.Create(APublisher: IEventPublisher);
begin
  inherited Create(APublisher);
  Dictionary := TObjectDictionary<string, IThriftList<IEventDescriptor>>.Create([]);
end;

destructor TMemoryEventStore.Destroy;
begin
  FreeAndNil(Dictionary);
  inherited;
end;

function TMemoryEventStore.LoadEventDescriptorsForAggregate(Key: IAggrKey): IThriftList<IEventDescriptor>;
var
  Events: IThriftList<IEventDescriptor>;
begin
  Result := nil;
  if Dictionary.TryGetValue(TAggrKeyImpl(Key).AsString, Events) then
    Result := Events as IThriftList<IEventDescriptor>;
end;

function TMemoryEventStore.LoadEventDescriptorsSince(const EventID: string;
  const Limit: Integer): IThriftList<IEventDescriptor>;
begin
  // Only here to fix compiler warnings.
  Result := nil;
end;

procedure TMemoryEventStore.PersistEventDescriptors(Key: IAggrKey;
  NewEventDescriptors: IThriftList<IEventDescriptor>; ExpectedVersion: Integer);
var
  EventListObj: TThriftListImpl<IEventDescriptor>;
  Ev: IEventDescriptor;
  EvObj: TEventDescriptor;
  KeyObj: TAggrKeyImpl;
begin
  KeyObj := TAggrKeyImpl(Key);
  EventListObj := TThriftListImpl<IEventDescriptor>.Create; // freed when Dictionary is destroyed
  for Ev in NewEventDescriptors do begin
    EvObj := TEventDescriptor.Create(Ev.AggKey, Ev.EventData, Ev.Version);
    EventListObj.Add(EvObj);
  end;
  // Key is a string to avoid the complexity of a custom compare function needed for an object key.
  Dictionary.Add(KeyObj.AsString, EventListObj);

  Assert(Dictionary.ContainsKey(KeyObj.AsString));
  Assert(Dictionary.ContainsValue(EventListObj));
end;

end.
