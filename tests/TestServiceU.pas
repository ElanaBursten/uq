unit TestServiceU;

interface

uses
  SysUtils, Classes, DB, ADODB, BetterADODataSet;

type
  TTestService = class(TDataModule)
    Query: TBetterADODataSet;
    Conn: TADOConnection;
    Command: TADOCommand;
    FindLocate: TBetterADODataSet;
    FindTicket: TBetterADODataSet;
    GetAssignment: TBetterADODataSet;
    DeleteTimesheetsCmd: TADOCommand;
    CountTimesheetQuery: TADODataSet;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    function AssignmentOf(const TicketNumber, ClientCode: string): Integer; overload;
    function RecordCount(const TableName: string): Integer;
    procedure RunSql(const Sql: string);
    function RunSqlReturnN(const Sql: string): Integer;
    function RunSqlReturnNAsDateTime(const Sql: string): TDateTime;
    function FindLocateID(const TicketNumber, ClientCode: string): Integer;
    function FindTicketID(const TicketNumber: string): Integer;
    procedure EmptyServerTable(const TableName: string);
    procedure InsertStatus(const Status, Name: string; Billable, Complete: Integer);
    procedure EmptyServerTables;
    procedure DeleteTimesheets;
    function NumTimesheets: Integer;
    function StatusOf(const TicketNumber, ClientCode: string): string;
  end;

implementation

{$R *.dfm}

uses
  TestConfig, OdAdoUtils;

procedure TTestService.DataModuleCreate(Sender: TObject);
begin
  ConnectAdoConnectionWithIni(Conn, GetTestIniFileName);
end;

function TTestService.AssignmentOf(const TicketNumber,
  ClientCode: string): Integer;
begin
  GetAssignment.Parameters.ParamValues['tn'] := TicketNumber;
  GetAssignment.Parameters.ParamValues['cc'] := ClientCode;
  GetAssignment.Open;
  try
    Result := GetAssignment.Fields[0].AsInteger;
  finally
    GetAssignment.Close;
  end;

//  CheckEquals(1, NumberOfAssignmentsFor(LocateID));
{
function TBaseTest.NumberOfAssignmentsFor(LocateID: Integer): Integer;
var
  Sql: string;
begin
  Sql := 'select count(*) AS N ' +
    'from assignment (NOLOCK) ' +
    'where locate_id=' + IntToStr(LocateID) + ' AND assignment.active=1 FOR XML RAW';

  Result := Server.RunSqlReturnN(Sql);
end;

}
end;

function TTestService.FindLocateID(const TicketNumber,
  ClientCode: string): Integer;
begin
  FindLocate.Parameters.ParamValues['tn'] := TicketNumber;
  FindLocate.Parameters.ParamValues['cc'] := ClientCode;
  FindLocate.Open;
  try
    Result := FindLocate.FieldByName('locate_id').AsInteger;
  finally
    FindLocate.Close;
  end;
end;

function TTestService.FindTicketID(const TicketNumber: string): Integer;
begin
  FindTicket.Parameters[0].Value := TicketNumber;
  FindTicket.Open;
  try
    Result := FindTicket.Fields[0].AsInteger;
  finally
    FindTicket.Close;
  end;
end;

function TTestService.RecordCount(const TableName: string): Integer;
begin
  Result := RunSqlReturnN('SELECT COUNT(*) AS N FROM ' + TableName);

end;

procedure TTestService.RunSql(const Sql: string);
begin
  Command.CommandText := Sql;
  Command.Execute;
end;

function TTestService.RunSqlReturnN(const Sql: string): Integer;
begin
  Query.CommandText := Sql;
  Query.Open;
  try
    Result := Query.Fields[0].AsInteger;
  finally
    Query.Close;
  end;
end;

function TTestService.RunSqlReturnNAsDateTime(
  const Sql: string): TDateTime;
begin
  Query.CommandText := Sql;
  Query.Open;
  try
    Result := Query.Fields[0].AsDateTime;
  finally
    Query.Close;
  end;
end;

procedure TTestService.EmptyServerTable(const TableName: string);
begin
  RunSql('DELETE FROM ' + TableName);
end;

procedure TTestService.EmptyServerTables;
begin
  EmptyServerTable('assignment');
  EmptyServerTable('locate_status');
  EmptyServerTable('locate');
  EmptyServerTable('ticket');
  EmptyServerTable('client');
  EmptyServerTable('office');
  EmptyServerTable('statuslist');
end;

procedure TTestService.InsertStatus(const Status, Name: string; Billable,
  Complete: Integer);
begin
  RunSql(Format('insert into statuslist (status, status_name, billable, complete) '+
                'values (''%s'', ''%s'', %d, %d)', [Status, Name, Billable, Complete]));

end;

procedure TTestService.DeleteTimesheets;
begin
  DeleteTimesheetsCmd.Execute;
end;

function TTestService.NumTimesheets: Integer;
begin
  with CountTimesheetQuery do begin
    Open;
    Result := Fields[0].AsInteger;
    Close;
  end;
end;

function TTestService.StatusOf(const TicketNumber,
  ClientCode: string): string;
begin
  FindLocate.Parameters.ParamValues['tn'] := TicketNumber;
  FindLocate.Parameters.ParamValues['cc'] := ClientCode;
  FindLocate.Open;
  try
    Result := FindLocate.FieldByName('status').AsString;
  finally
    FindLocate.Close;
  end;
end;

end.
