unit MultiSyncTest;

interface

uses
  Classes, Contnrs, TestFrameWork, SysUtils, DB, DBISAMTb, Dialogs, SyncEngineU,
  SingleEngineTest;

type
  TMultiSyncTest = class(TBaseTest)
  private
    procedure CheckRecordCount(Count: Integer; TableName: string);
    //procedure ShowStatusListContents;
  public
    procedure SetUp; override;
  published
    procedure TestClientInsert;
    procedure TestClientInsertNoRefetch;
    procedure TestClientInsertTwiceNoRefetch;
    procedure TestClientDeleteTwice;
    procedure TestMoneyType;
  end;

implementation

uses
  TestConfig;

{ TMultiSyncTest }

procedure TMultiSyncTest.CheckRecordCount(Count: Integer; TableName: string);
begin
  CheckEquals(Count, Engine.ActiveRecordCount(TableName));
end;

procedure TMultiSyncTest.SetUp;
begin
  inherited;
  TestService.EmptyServerTables;
end;

procedure TMultiSyncTest.TestClientInsert;
var
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  Errors := TObjectList.Create;
  try
    Engine.Sync(0, Errors, SyncRequestSource);

    CheckRecordCount(0, 'office');
    CheckRecordCount(0, 'statuslist');
    CheckRecordCount(0, 'client');

    TestService.RunSql('INSERT INTO office (office_name) VALUES (''North'')');
    TestService.RunSql('INSERT INTO office (office_name) VALUES (''South'')');

    TestService.InsertStatus('N', 'New', 0, 0);
    TestService.InsertStatus('NC', 'No Conflict', 0, 1);
    TestService.InsertStatus('M', 'Marked', 1, 1);
    TestService.InsertStatus('NIA', 'Not In Area', 0, 1);

    TestService.RunSql('insert into client (client_name, oc_code, call_center) values (''SGAS'', ''SGAS'', ''FCO1'')');

    Engine.Sync(0, Errors, SyncRequestSource);
  finally
    FreeAndNil(Errors);
  end;

  CheckRecordCount(2, 'office');
  CheckRecordCount(4, 'statuslist');
  CheckRecordCount(1, 'client');
end;

(*
procedure TMultiSyncTest.ShowStatusListContents;
var
  s: string;
begin
  with Engine.OpenQuery('select * from statuslist') do try
    while not EOF do begin
      s := s + FieldByName('status').AsString + ':' + FieldByName('active').AsString + '  ';
      Next;
    end;
    ShowMessage(s);
  finally
    Free;
  end;
end;
*)

procedure TMultiSyncTest.TestClientDeleteTwice;
var
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  Errors := TObjectList.Create;
  try
    Engine.Sync(0, Errors, SyncRequestSource);

    CheckEquals(0, Engine.ActiveRecordCount('statuslist'), 'before starting');

    with Engine.OpenTableWithEditTracking('statuslist') do try
      AppendRecord(['NW', 'New']);
      AppendRecord(['OD', 'Old']);
      AppendRecord(['DN', 'Done']);

      Engine.Sync(0, Errors, SyncRequestSource);
      CheckRecordCount(3, 'statuslist');

      // This is how clients will delete things.
      Check(Locate('status', 'NW', []));
      Edit;
      FieldByName('active').AsBoolean := False;
      Post;  // deltastatus will be set for us.
    finally
      Free;
    end;

    CheckEquals(2, Engine.ActiveRecordCount('statuslist'), 'before sync');

    with Engine.OpenQuery('select * from statuslist') do try
      // we have the whole thing, so we can find records where active=false
      Check(Locate('status', 'NW', []), 'finding record');
      CheckEquals('U', FieldByName('DeltaStatus').AsString);
    finally
      Free;
    end;

    Engine.Sync(0, Errors, SyncRequestSource);
    CheckEquals(1, Engine.RowsSent);
    CheckEquals(1, Engine.RowsReceived);   // got it back

    // ShowStatusListContents;

    CheckEquals(2, Engine.ActiveRecordCount('statuslist'), 'after sync');

    Engine.Sync(0, Errors, SyncRequestSource);
    CheckEquals(0, Engine.RowsSent);
    CheckEquals(0, Engine.RowsReceived);
    CheckEquals(2, Engine.ActiveRecordCount('statuslist'), 'after second sync');
  finally
    FreeAndNil(Errors);
  end;
end;

procedure TMultiSyncTest.TestClientInsertNoRefetch;
var
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  with Engine.OpenTableWithEditTracking('statuslist') do try
    AppendRecord(['NW', 'New']);
    AppendRecord(['OD', 'Old']);
    AppendRecord(['DN', 'Done']);
  finally
    Free;
  end;

  Errors := TObjectList.Create;
  try
    Engine.Sync(0, Errors, SyncRequestSource);
    CheckRecordCount(3, 'statuslist');

    Engine.EmptyAllTables;
    CheckRecordCount(0, 'statuslist');

    Engine.Sync(0, Errors, SyncRequestSource);
    CheckRecordCount(3, 'statuslist');
  finally
    FreeAndNil(Errors);
  end;
end;

procedure TMultiSyncTest.TestClientInsertTwiceNoRefetch;
var
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  Errors := TObjectList.Create;
  with Engine.OpenTableWithEditTracking('statuslist') do try
    AppendRecord(['NW', 'New']);
    AppendRecord(['OD', 'Old']);
    AppendRecord(['DN', 'Done']);

    Engine.Sync(0, Errors, SyncRequestSource);
    CheckEquals(3, Engine.RowsSent);
    CheckRecordCount(3, 'statuslist');

    AppendRecord(['TR', 'Trouble']);
    AppendRecord(['RP', 'Repair']);
    Engine.Sync(0, Errors, SyncRequestSource);
    CheckEquals(2, Engine.RowsSent);
    CheckRecordCount(5, 'statuslist');
  finally
    FreeAndNil(Errors);
    Free;
  end;
end;

procedure TMultiSyncTest.TestMoneyType;
var
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  LoadSampleTickets;

  Errors := TObjectList.Create;
  try
    Engine.Sync(KyleId, Errors, SyncRequestSource);

    with Engine.OpenTableWithEditTracking('locate') do try
      Edit;
      FieldByName('qty_marked').AsInteger := 5;
      Post;
    finally
      Free;
    end;

    Engine.Sync(KyleId, Errors, SyncRequestSource);
  finally
    FreeAndNil(Errors);
  end;
end;

initialization
  TestFramework.RegisterTest(TMultiSyncTest.Suite);

end.

