﻿unit TestThreadSafeLoggerU;

interface

uses
  ThreadSafeLoggerU, TestFramework, SysUtils, SyncObjs, Classes, OdHourglass, OdMiscUtils;

type

  TThreadThatLogs = class(TThread)
  strict private
    FLog: TThreadSafeLogger;
  protected
    constructor Create(Log: TThreadSafeLogger); overload;
    procedure Execute; override;
  end;

  // Test methods for class TThreadSafeLogger
  TestTThreadSafeLogger = class(TTestCase)
  strict private
    FThreadSafeLogger: TThreadSafeLogger;
    FStringsFileName: string;
    procedure TestLog{$IFDEF UNICODE}(QMEncoding: TQMEncodingType = etAnsi){$ENDIF};
  public
    WriteLogStrings: TStringList;
    ReadLogStrings: TStringList;
    procedure SetUp; override;
    procedure TearDown; override;
  published
{$IFDEF UNICODE}
    procedure TestLogUTF8;
{$ENDIF}
    procedure TestLogAnsi;
    procedure TestFileName;
    procedure TestMultipleLoggers;
    procedure TestMultiThreadLogging;
    procedure TestLogMoreData;
    procedure TestLog10k;
    procedure TestLogDelayedEnable;
  end;

  // Test methods for class TPrefixedWriter
  TestTPrefixedWriter = class(TTestCase)
  strict private
    FThreadSafeLogger : TThreadSafeLogger;
    FPrefixedWriter: TPrefixedWriter;
    WriteLogStrings: TStringList;
    ReadLogStrings: TStringList;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestLog;
  end;

const
  TESTLOGFILENAME = 'TestThreadSafeLogger.log';
  TESTRINGLOGFILENAME = 'RingTestThreadSafeLogger.log';
  STRINGSFILENAME = 'SourceStrings.txt';
  PREFIXTESTLOGFILENAME = 'PrefixTestThreadSafeLogger.log';
  PREFIXSTRINGSFILENAME = 'PrefixSourceStrings.txt';
  TESTPREFIX = 'TestPrefix: ';
  TEST10k = 10000;
  INSTANCEID = '9999';

implementation
  uses DateUtils, Windows, Messages{$IFDEF UNICODE},TestUnicodeData{$ENDIF};

function DirLocation: string;
begin
  Result := AddSlash(GetWindowsTempPath);
end;

procedure BuildLogStrings(Strings: TStrings; NumOfStrings: Integer=100);
var
  i: Integer;
begin
  Assert(Assigned(Strings));
  Strings.Add('This is the first line.');
  Strings.Add('Other text goes in the log');
  Strings.Add('More text goes in the log');
  Strings.Add(StringOfChar('a', 2000));
  Strings.Add('vfdvfdvd');
  Strings.Add('blah blah blahy.');
  Strings.Add('This is some more text going into the log.');
  Strings.Add('More stuff goes here.');
  Strings.Add('The Wheels on the bus go round and round round and round.');
{$IFDEF UNICODE}
  AddUnicodeStrings(Strings);
{$ENDIF}
  Strings.Add('If this file is in Ansi, then unicode characters will appear as blocks or question marks.');
  for i := 0 to NumOfStrings do
    Strings.Add(IntToStr(i) +
      ': Adding lots more data. Blah blah blah blah blah blah blah blah blah. ');
  Strings.Add('This is the last line.');
end;

procedure TestTThreadSafeLogger.SetUp;
begin
  FStringsFileName := DirLocation + STRINGSFILENAME;

  if FileExists(GetDailyFileName(DirLocation, TESTLOGFILENAME)) then
    SysUtils.DeleteFile(GetDailyFileName(DirLocation, TESTLOGFILENAME));
  if FileExists(GetDailyFileName(DirLocation, TESTRINGLOGFILENAME)) then
    SysUtils.DeleteFile(GetDailyFileName(DirLocation, TESTRINGLOGFILENAME));
  if FileExists(FStringsFileName) then
    SysUtils.DeleteFile(FStringsFileName);

  ReadLogStrings := TStringList.Create;
  WriteLogStrings := TStringList.Create;
  BuildLogStrings(WriteLogStrings, 200);
end;

procedure TestTThreadSafeLogger.TearDown;
begin
  FreeAndNil(ReadLogStrings);
  FreeAndNil(WriteLogStrings);
end;

procedure TestTThreadSafeLogger.TestLog{$IFDEF UNICODE}(QMEncoding: TQMEncodingType = etAnsi){$ENDIF};
var
  i: Integer;
{$IFDEF UNICODE}  TestEncoding: TEncoding;{$ENDIF}
begin
  //Test with smaller than default capacity to show it works under load.
  FThreadSafeLogger := TThreadSafeLogger.Create(DIRLOCATION, TESTLOGFILENAME{$IFDEF UNICODE},0,500000,True,QMEncoding{$ENDIF});

{$IFDEF UNICODE}
  if QMEncoding = etUTF8 then
    TestEncoding := TEncoding.UTF8
  else
    TestEncoding := TEncoding.ANSI;
{$ENDIF}

  WriteLogStrings.SaveToFile(FStringsFileName{$IFDEF UNICODE}, TestEncoding{$ENDIF});

  for i := 0 to WriteLogStrings.Count - 1 do begin
    Sleep(10);//slow writing to the log just a little bit to simulate more realistic logging. It is unlikely we would write 1000s of lines at a time.
    FThreadSafeLogger.Log(WriteLogStrings[i]);
  end;

  FreeAndNil(FThreadSafeLogger);

  // read the source string list back from the file so we have apples to apples
  WriteLogStrings.LoadFromFile(FStringsFileName{$IFDEF UNICODE}, TestEncoding{$ENDIF});
  // Read the log
  ReadLogStrings.LoadFromFile(GetDailyFileName(DIRLOCATION, TESTLOGFILENAME){$IFDEF UNICODE}, TestEncoding{$ENDIF});

  CheckEquals(WriteLogStrings.Text, ReadLogStrings.Text);
end;

procedure TestTThreadSafeLogger.TestLogAnsi;
begin
  TestLog;
end;

{$IFDEF UNICODE}
procedure TestTThreadSafeLogger.TestLogUTF8;
begin
  TestLog(etUTF8);
end;
{$ENDIF}

procedure TestTThreadSafeLogger.TestMultipleLoggers;
var
  i: Integer;
  Logger1: TThreadSafeLogger;
  Logger2: TThreadSafeLogger;
begin
  Logger1 := TThreadSafeLogger.Create(DIRLOCATION, TESTLOGFILENAME);

  //log strings for first logger
  for i := 0 to WriteLogStrings.Count - 1 do
    Logger1.Log(WriteLogStrings[i]);
  //Done with first logger
  FreeAndNil(Logger1);

  //Add and log more strings for the 2nd logger thread
  WriteLogStrings.Add('Logger 2 begins here.');
  WriteLogStrings.Add('Some more data.');
  WriteLogStrings.Add('blah blah blah blah.');
  WriteLogStrings.Add('Write something else.');
  WriteLogStrings.Add('Logger 2 ends here.');
  WriteLogStrings.SaveToFile(FStringsFileName);
  Logger2 := TThreadSafeLogger.Create(DIRLOCATION, TESTLOGFILENAME);
  Logger2.Log('Logger 2 begins here.');
  Logger2.Log('Some more data.');
  Logger2.Log('blah blah blah blah.');
  Logger2.Log('Write something else.');
  Logger2.Log('Logger 2 ends here.');
  FreeAndNil(Logger2);

  //Now were both loggers able to write the data to the same file as expected? i.e. is the appended 2nd logger data there
  // read the source string list back from the file so we have apples to apples
  WriteLogStrings.LoadFromFile(FStringsFileName);
  // Read the log
  ReadLogStrings.LoadFromFile(GetDailyFileName(DIRLOCATION, TESTLOGFILENAME));

  CheckEquals(WriteLogStrings.Text, ReadLogStrings.Text);
end;

procedure TestTThreadSafeLogger.TestMultiThreadLogging;
var
  I: Integer;
  Logger: TThreadSafeLogger;
  ThdArr: array[0..19] of TThreadThatLogs;
  Cursor: IInterface;

  function AllThreadsTerminated: Boolean;
  var
    J: Integer;
  begin
    Result := True;
    for J := Low(ThdArr) to High(ThdArr) do
      Result := Result and ThdArr[J].Terminated;
  end;

begin
  Cursor := ShowHourglass;
  Logger := TThreadSafeLogger.Create(DIRLOCATION, TESTLOGFILENAME{$IFDEF UNICODE},0,500000,True,etUTF8{$ENDIF});
  try
    for I := Low(ThdArr) to High(ThdArr) do begin
      ThdArr[I] := TThreadThatLogs.Create(Logger);
      ThdArr[I].Suspended := False;
    end;
    repeat
      Sleep(0); // let the threads write some strings
    until (AllThreadsTerminated);
  finally
    FreeAndNil(Logger);
    for I := Low(ThdArr) to High(ThdArr) do
      ThdArr[I].Free;
  end;

  // Each thread writes 100 lines + a Thead Started and Thread Finished line.
  ReadLogStrings.LoadFromFile(GetDailyFileName(DIRLOCATION, TESTLOGFILENAME){$IFDEF UNICODE}, TEncoding.UTF8{$ENDIF});
  CheckEquals((Length(ThdArr) * 102), ReadLogStrings.Count);
end;

procedure TestTThreadSafeLogger.TestFileName;
var
  Logger1: TThreadSafeLogger;
begin
  Logger1 := nil; //prevent warning
  try
    Logger1 := TThreadSafeLogger.Create('', '');
    Check(Logger1=nil, 'Logger creation should fail when no filename is passed to the logger.');
  except
    if Assigned(Logger1) then
      Logger1.Free;
  end;

  Logger1 := TThreadSafeLogger.Create(DIRLOCATION, TESTLOGFILENAME{$IFDEF UNICODE},0,500000,True,etUTF8{$ENDIF});
  try
    Check(Logger1<>nil, 'Logger should be created when valid parameters are passed.');
  finally
    Logger1.Free;
  end;
end;

procedure TestTThreadSafeLogger.TestLogMoreData;
begin
  BuildLogStrings(WriteLogStrings, 2000);//Use more lines to see missing data issue. Compare SourceStrings.txt and TestThreadSafeLogger.log to view the missing data.
  TestLog{$IFDEF UNICODE}(etUTF8){$ENDIF};
end;


procedure TestTThreadSafeLogger.TestLog10k;
var
  i: Integer;
  S10k: string;
begin
  WriteLogStrings.Clear;
  BuildLogStrings(WriteLogStrings, 125);
  WriteLogStrings.SaveToFile(FStringsFileName);

  FThreadSafeLogger := TThreadSafeLogger.Create(DIRLOCATION, TESTRINGLOGFILENAME, 0, TEST10k);
  try
    for i := 0 to WriteLogStrings.Count - 1 do begin
      Sleep(10);//slow writing to the log just a little bit to simulate more realistic logging. It is unlikely we would write 1000s of lines at a time.
      FThreadSafeLogger.Log(WriteLogStrings[i]);
    end;
  finally
    FreeAndNil(FThreadSafeLogger);
  end;

  // read the source string list back from the file so we have apples to apples
  WriteLogStrings.LoadFromFile(FStringsFileName);
  S10k := Copy(WriteLogStrings.Text, (Length(WriteLogStrings.Text) - (TEST10k)+1), TEST10k);

  // Read the log
  ReadLogStrings.LoadFromFile(GetDailyFileName(DIRLOCATION, TESTRINGLOGFILENAME));
  CheckEquals(S10K, ReadLogStrings.Text);
end;

procedure TestTThreadSafeLogger.TestLogDelayedEnable;
var
  i: Integer;
begin
  if FileExists(GetDailyFileName(DIRLOCATION, TESTRINGLOGFILENAME)) then
    SysUtils.DeleteFile(GetDailyFileName(DIRLOCATION, TESTRINGLOGFILENAME));

  WriteLogStrings.Clear;
  BuildLogStrings(WriteLogStrings, 50);
  WriteLogStrings.SaveToFile(FStringsFileName);

  FThreadSafeLogger := TThreadSafeLogger.Create(DIRLOCATION, TESTRINGLOGFILENAME, 0, 0, False);
  try
    for i := 0 to WriteLogStrings.Count - 1 do begin
      Sleep(10);//slow writing to the log just a little bit to simulate more realistic logging. It is unlikely we would write 1000s of lines at a time.
      FThreadSafeLogger.Log(WriteLogStrings[i]);
    end;

    FThreadSafeLogger.Enabled := True;

    for i := 0 to WriteLogStrings.Count - 1 do begin
      Sleep(10);//slow writing to the log just a little bit to simulate more realistic logging. It is unlikely we would write 1000s of lines at a time.
      FThreadSafeLogger.Log(WriteLogStrings[i]);
    end;

  finally
    FreeAndNil(FThreadSafeLogger);
  end;

  WriteLogStrings.LoadFromFile(FStringsFileName);
  ReadLogStrings.LoadFromFile(GetDailyFileName(DIRLOCATION, TESTRINGLOGFILENAME));
  CheckEquals(WriteLogStrings.Text, ReadLogStrings.Text);
end;


// Test methods for class TPrefixedWriter
procedure TestTPrefixedWriter.SetUp;
begin
  if FileExists(GetDailyFileName(DIRLOCATION, PREFIXTESTLOGFILENAME)) then
    SysUtils.DeleteFile(GetDailyFileName(DIRLOCATION, PREFIXTESTLOGFILENAME));
  if FileExists(PREFIXSTRINGSFILENAME) then
    SysUtils.DeleteFile(PREFIXSTRINGSFILENAME);

  FThreadSafeLogger := TThreadSafeLogger.Create(DIRLOCATION, PREFIXTESTLOGFILENAME);
  FPrefixedWriter := TPrefixedWriter.Create(FThreadSafeLogger, TESTPREFIX);
  ReadLogStrings := TStringList.Create;
  WriteLogStrings := TStringList.Create;
  BuildLogStrings(WriteLogStrings, 100);
end;

procedure TestTPrefixedWriter.TearDown;
begin
  FreeAndNil(WriteLogStrings);
  FreeAndNil(ReadLogStrings);
  FreeAndNil(FPrefixedWriter);
end;

procedure TestTPrefixedWriter.TestLog;
var
  i: Integer;
begin
  for i := 0 to WriteLogStrings.Count - 1 do
  begin
    FPrefixedWriter.Log(WriteLogStrings[i]);
    WriteLogStrings[i] := TESTPREFIX + WriteLogStrings[i];
  end;
  FreeAndNil(FThreadSafeLogger);

  WriteLogStrings.SaveToFile(PREFIXSTRINGSFILENAME);

  WriteLogStrings.LoadFromFile(PREFIXSTRINGSFILENAME);
  ReadLogStrings.LoadFromFile(GetDailyFileName(DIRLOCATION, PREFIXTESTLOGFILENAME));
  CheckEquals(WriteLogStrings.Text, ReadLogStrings.Text);
end;


{ TThreadThatLogs }

constructor TThreadThatLogs.Create(Log: TThreadSafeLogger);
begin
  inherited Create(True);
  FreeOnTerminate := False;
  FLog := Log;
end;

procedure TThreadThatLogs.Execute;
const
  SomeLongString = 'folding focus decoder genuine heist mountain jalapeño dexterity booth';
var
  StrCnt: Integer;
begin
  inherited;
  // Add 100 lines to FLog and quit
  StrCnt := 0;
  FLog.LogWithTime(Format('Thread Id: %d Started', [Self.Handle]));
  while not Terminated do begin
    Inc(StrCnt);
    Sleep(Random(500));
    FLog.LogWithTime(Format('Thread id: %d Line: %3.3d Nonsense: %s', [Self.Handle, StrCnt, SomeLongString]));
    if StrCnt >= 100 then
      Terminate;
  end;
  FLog.LogWithTime(Format('Thread Id: %d Finished', [Self.Handle]));
end;

initialization

// Register any test cases with the test runner
RegisterTest(TestTThreadSafeLogger.Suite);
RegisterTest(TestTPrefixedWriter.Suite);
Randomize;

end.
