program TestProj4Delphi;

uses
  Vcl.Forms,
  MainProj4TestForm in 'MainProj4TestForm.pas' {Form3},
  CStringList in '..\thirdparty\Proj4Delphi\CStringList.pas',
  Points in '..\thirdparty\Proj4Delphi\Points.pas',
  Proj4 in '..\thirdparty\Proj4Delphi\Proj4.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
