unit BrokenInterfacedDMu;

interface

uses
  System.SysUtils, System.Classes, MyDMIntf, OperationTracking;

type
  TBrokenInterfacedDM = class(TDataModule, IMyDMInterface)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  public
    function Echo(const s: string): string;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TBrokenInterfacedDM }

procedure TBrokenInterfacedDM.DataModuleCreate(Sender: TObject);
begin
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules + 1;
end;

procedure TBrokenInterfacedDM.DataModuleDestroy(Sender: TObject);
begin
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules - 1;
end;

function TBrokenInterfacedDM.Echo(const s: string): string;
begin
  Result := s;
end;

end.
