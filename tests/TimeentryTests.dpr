program TimeentryTests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options 
  to use the console test runner.  Otherwise the GUI test runner will be used by 
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  Forms,
  TestFramework,
  GUITestRunner,
  TextTestRunner,
  TimeClockEntry in '..\client\TimeClockEntry.pas',
  TestTimeClockEntry in 'TestTimeClockEntry.pas',
  OdDBISAMUtils in '..\common\OdDBISAMUtils.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  OdIsoDates in '..\common\OdIsoDates.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  OdMSXMLUtils in '..\common\OdMSXMLUtils.pas',
  MSXML2_TLB in '..\common\MSXML2_TLB.pas';

{$R *.RES}

begin
  Application.Initialize;
  if IsConsole then
    TextTestRunner.RunRegisteredTests
  else
    GUITestRunner.RunRegisteredTests;
end.

