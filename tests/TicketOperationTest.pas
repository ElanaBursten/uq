unit TicketOperationTest;

interface

uses
  Classes, Contnrs, TestFrameWork, SysUtils, DB, SyncEngineU, SingleEngineTest;

type
  TTicketOperationTest = class(TBaseTest)
  private
    function LocalAssignmentOf(LocateID: Integer): Integer;
  public
  protected
    procedure SetUp; override;
  published
    procedure TestAssignLocate;
    procedure TestAssignmentInLocalCache;
    procedure TestAssignmentInLocalCacheGet;
  end;

implementation

uses DMu, QMServerLibrary_Intf, TestServiceU;

procedure TTicketOperationTest.TestAssignLocate;
var
  Tic: TicketList;
begin
  CheckEquals(204, TestService.AssignmentOf('2249', 'ENTGY08'));

  Engine.GetTicketDataFromServer(TestService.FindTicketID('2249'));

  Tic := TicketList.Create;
  with Tic.Add do
    TicketId := TestService.FindTicketID('2249');

  QMS.MoveTickets(TestingSecSession, Tic, 204, 205);

  CheckEquals(205, TestService.AssignmentOf('2249', 'ENTGY08'));
end;

procedure TTicketOperationTest.TestAssignmentInLocalCache;
var
  LocateID: Integer;
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  LocateID := TestService.FindLocateID('2249', 'ENTGY08');

  Errors := TObjectList.Create;
  try
    Engine.Sync(204, Errors, SyncRequestSource);
  finally
    FreeAndNil(Errors);
  end;
  CheckEquals(204, LocalAssignmentOf(LocateID));
end;

function TTicketOperationTest.LocalAssignmentOf(
  LocateID: Integer): Integer;
begin
  with Engine.OpenQuery('select * from locate where active') do try
    Check(Locate('locate_id', LocateID, []));
    Result := FieldByName('locator_id').AsInteger;
  finally
    Free;
  end;
end;

procedure TTicketOperationTest.TestAssignmentInLocalCacheGet;
var
  LocateID: Integer;
begin
  LocateID := TestService.FindLocateID('2249', 'ENTGY08');
  Engine.GetTicketDataFromServer(TestService.FindTicketID('2249'));
  CheckEquals(204, LocalAssignmentOf(LocateID));
end;

procedure TTicketOperationTest.SetUp;
begin
  inherited;
  SetupServerTables;
end;

initialization
  TestFramework.RegisterTest(TTicketOperationTest.Suite);

end.

