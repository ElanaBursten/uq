unit DbisamTest;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TestFrameWork, Db, DBISAMTb;

type
  TDbisamTestDM = class(TDataModule)
    Table1: TDBISAMTable;
    Query1: TDBISAMQuery;
    procedure Table1BeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TDbisamTest = class(TTestCase)
  private
    //
    DM: TDbisamTestDM;
    procedure CreateTable1OnDisk;
    function MakeMemoryTable: TDBISAMTable;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestTable;
    procedure TestQuery;
    procedure TestStream;
    procedure TestMemoryStream;
  end;

implementation

{$R *.DFM}

{ TDbisamTest }

procedure TDbisamTest.SetUp;
begin
  inherited;
  FreeAndNil(DM);
  DM := TDbisamTestDM.Create(nil);
end;

procedure TDbisamTest.TearDown;
begin
  inherited;
  FreeAndNil(DM);
end;

procedure TDbisamTest.TestQuery;
begin
  TestTable;  // populate it

  with DM.Query1 do begin
    Open;
    First;
    Next;
    Edit;
    FieldByName('active').AsBoolean := False;
    CheckEquals(False, FieldByName('active').AsBoolean, 'Before');
    Post;
    // that record is gone, it did not meet the criteria!

    // so the one we are on now, does meet the criteria.
    CheckEquals(True, FieldByName('active').AsBoolean, 'After');
  end;
end;

procedure TDbisamTest.TestTable;
begin
  CreateTable1OnDisk;

  with DM.Table1 do begin
    First;
    Edit;
    FieldByName('active').AsBoolean := False;
    CheckEquals(False, FieldByName('active').AsBoolean, 'Before');
    Post;
    CheckEquals(False, FieldByName('active').AsBoolean, 'After');
  end;
end;

procedure TDbisamTestDM.Table1BeforePost(DataSet: TDataSet);
begin
  DataSet.FieldByName('Address').AsString := 'HELLO WORLD';
end;

procedure TDbisamTest.TestStream;
var
  Str: TMemoryStream;
  Table: TDBISAMTable;
begin
  CreateTable1OnDisk;
  DM.Table1.Close;

  Str := TMemoryStream.Create;
  Str.LoadFromFile('testtable.dat');

  Str.SaveToFile('testtable2.dat');

  Table := TDBISAMTable.Create(nil);

  Table.TableName := 'testtable2';
  Table.Open;
  CheckEquals(3, Table.RecordCount);
  CheckEquals(4, Table.Fields.Count);

  Table.Free;
end;

procedure TDbisamTest.CreateTable1OnDisk;
begin
  with DM.Table1 do begin
    DeleteFile('testtable.dat');
    DeleteFile('testtable.idx');
    CreateTable;
    Open;
    AppendRecord(['Kyle', 'Justice', 12, True]);

    CheckEquals('HELLO WORLD', FieldByName('address').AsString);

    AppendRecord(['Bob', 'West', 14, True]);
    AppendRecord(['Alan', 'Main St.', 16, True]);
  end;
end;

function TDbisamTest.MakeMemoryTable: TDBISAMTable;
var
  Table: TDBISAMTable;
begin
  Table := TDBISAMTable.Create(nil);
  Table.DatabaseName := 'Memory';
  Table.TableName := 'somename';
  Table.FieldDefs.Add('Name', ftString, 40);
  Table.FieldDefs.Add('Age', ftInteger, 0);
  Table.CreateTable;
  Table.Open;
  Table.AppendRecord(['Bob', 14]);
  Table.AppendRecord(['Jane', 16]);
  Table.AppendRecord(['Johcdn', 12]);
  Result := Table;
end;

procedure TDbisamTest.TestMemoryStream;
var
  Str: TMemoryStream;
  Table: TDBISAMTable;
begin
  Exit;
  Fail('DBISAM does not appear to support the required functionality for this test.');

  Table := MakeMemoryTable;
  Str := TMemoryStream.Create;
  Table.SaveToStream(Str);
  Table.Close;
  table.DeleteTable;
  FreeAndNil(Table);

  Table := TDBISAMTable.Create(nil);
  Table.DatabaseName := 'Memory';
  Table.TableName := 'newname';
  Table.Open;
  Table.LoadFromStream(Str);
  CheckEquals(3, Table.RecordCount);
  CheckEquals(2, Table.Fields.Count);
  FreeAndNil(Table);
end;

initialization
  TestFramework.RegisterTest(TDbisamTest.Suite);

end.

