unit TestQMTempFolder;
{
  Test the ITemporaryFolder interface
}

interface

uses
  TestFramework, QMTempFolder, SysUtils, Classes, OdMiscUtils;

type
  // Test methods for class ITemporaryFolder

  TestITemporaryFolder = class(TTestCase)
  {$IFNDEF ver140}
    {$IFNDEF ver150}
  strict private
    {$ELSE}
  private
    {$ENDIF}
  {$ENDIF}
    FITemporaryFolder: ITemporaryFolder;
  private
    procedure CauseDeleteFailure;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestFullPathName;
    procedure TestAutoDelete;
    procedure TestDeleteFailure;
  end;

implementation

procedure TestITemporaryFolder.SetUp;
begin
  FITemporaryFolder := NewTemporaryFolder;
end;

procedure TestITemporaryFolder.TearDown;
begin
  FITemporaryFolder := nil;
end;

procedure MakeTempFiles(const Dir, Prefix: string);
var
  I: Integer;
begin
  // put some files in the folder
  for I := 1 to 5 do
    CreateEmptyFile(Dir + 'UQTests-' + Prefix + '-' + IntToStr(I) + '.tmp');
end;

procedure TestITemporaryFolder.TestAutoDelete;
var
  DirName: string;

  procedure UseTempFolder;
  var
    TempDir: ITemporaryFolder;
  begin
    TempDir := NewTemporaryFolder;
    DirName := AddSlash(TempDir.FullPathName);
    MakeTempFiles(DirName, 'TestAutoDelete');
  end;

begin
  UseTempFolder;
  // TempDir loses scope, so DirName and contents are deleted
  Check(not DirectoryExists(DirName), 'Temp folder ' + DirName + ' still exists');
end;

procedure TestITemporaryFolder.CauseDeleteFailure;
var
  DirName: string;
  HoldFile: TFileStream;

  procedure UseTempFolder;
  var
    TempDir: ITemporaryFolder;
  begin
    TempDir := NewTemporaryFolder;
    DirName := AddSlash(TempDir.FullPathName);
    MakeTempFiles(DirName, 'TestDeleteFailure');
    // open one of the temp files, so it cannot be deleted
    HoldFile := TFileStream.Create(DirName + 'UQTests-TestDeleteFailure-1.tmp',
      fmOpenRead or fmShareExclusive);
  end;

begin
  try
    UseTempFolder;
    Check(False, 'An exception should prevent the code from continuing.');
  finally
    FreeAndNil(HoldFile);
  end;

  if DirectoryExists(DirName) then begin
    // manually clean up the left behind folder
    OdDeleteFile(AddSlash(DirName), '*.*');
    RemoveDir(DirName);
  end;
  Check(not DirectoryExists(DirName), 'Manual temp folder cleanup failed.');
end;

procedure TestITemporaryFolder.TestDeleteFailure;
begin
  CheckException(CauseDeleteFailure, Exception, 'Expected an exception.');
end;

procedure TestITemporaryFolder.TestFullPathName;
var
  ReturnValue: string;
begin
  ReturnValue := FITemporaryFolder.FullPathName;
  Check(DirectoryExists(ReturnValue), 'Temp folder does not exist');
end;

initialization
  // Register any test cases with the test runner
  RegisterTest(TestITemporaryFolder.Suite);
end.

