object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Reproject coordinates'
  ClientHeight = 230
  ClientWidth = 438
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 407
    Height = 73
    Caption = ' SPCS (Maryland NAD27) '
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 35
      Width = 6
      Height = 13
      Caption = 'X'
    end
    object Label2: TLabel
      Left = 205
      Top = 35
      Width = 6
      Height = 13
      Caption = 'Y'
    end
    object XCoord: TEdit
      Left = 34
      Top = 32
      Width = 138
      Height = 21
      TabOrder = 0
      Text = '696529.3010'
    end
    object YCoord: TEdit
      Left = 236
      Top = 32
      Width = 138
      Height = 21
      TabOrder = 1
      Text = '426623.9990'
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 112
    Width = 407
    Height = 110
    Caption = ' WGS84 '
    TabOrder = 1
    object Label3: TLabel
      Left = 8
      Top = 27
      Width = 49
      Height = 13
      Caption = 'Lat, Long:'
    end
    object LatLongMemo: TMemo
      Left = 63
      Top = 24
      Width = 330
      Height = 75
      ParentColor = True
      TabOrder = 0
    end
  end
  object Button1: TButton
    Left = 174
    Top = 85
    Width = 75
    Height = 25
    Caption = 'Convert'
    TabOrder = 2
    OnClick = Button1Click
  end
end
