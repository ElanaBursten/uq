unit TrivialTest;

interface

uses
  Classes, TestFrameWork, SysUtils, DB, SyncEngineU, SingleEngineTest;

type
  TTrivialTest = class(TBaseTest)
  published
    procedure TestInsertOffices;
  end;

implementation

{ TTrivialTest }

uses
  TestServiceU;

procedure TTrivialTest.TestInsertOffices;
var
  N: Integer;
  Name: string;
begin
  TestService.EmptyServerTable('office');
  CheckEquals(0, OfficeCount);

  for N := 1 to 20 do begin
    Name := 'Office' + IntToStr(N);
    TestService.RunSql('INSERT INTO office (office_name, hours_to_respond) VALUES (''' + Name + ''',' + intToStr(N) + ')');
    CheckEquals(N, OfficeCount);
  end;
end;

initialization
  TestFramework.RegisterTest(TTrivialTest.Suite);

end.

