unit TestMSSqlEventSource;
{

  Delphi DUnit Test Case
  ----------------------
  This unit contains a skeleton test case class generated by the Test Case Wizard.
  Modify the generated code to correctly setup and call the methods from the unit
  being tested.

}

interface

uses
  TestFramework, EventSource, MSSqlEventSource, Generics.Collections, SysUtils,
  QMLogic2ServiceLib, AdUtils, Variants, Data.DB, QMThriftUtils,
  FireDAC.Comp.DataSet, FireDAC.Stan.Error, FireDAC.Comp.Client, FireDAC.Stan.Def, FireDAC.Phys.MSSQL,
  FireDAC.Stan.Async, FireDAC.VCLUI.Wait, FireDAC.Stan.Intf, FireDAC.DApt, TestQMEvents;

type
  // Test methods for class TMSSQLEventStore

  TestTMSSQLEventStore = class(TTestCase)
  strict private
    Pub: IEventPublisher;
    DBConn: TADConnection;
    FMSSQLEventStore: IEventStore;
    Assigned: IEvent;
    Scheduled: IEvent;
    Closed: IEvent;
    Reopened: IEvent;
    procedure ClearEvents;
    procedure MakeBaselineEvents;
  private
    function GetBaselineKey: IAggrKey;
    procedure CheckDBEvents(EventsInDB: IEventList);
    function GetStartingGUID: string;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestStoreEvents;
    procedure TestGetEventsForAggr;
    procedure TestGetEventsSince;
  end;

implementation

uses
  IniFiles, Thrift.Collections;

procedure TestTMSSQLEventStore.SetUp;
var
  Ini: TIniFile;
begin
  Pub := TStringPublisher.Create;
  DBConn := TADConnection.Create(nil);
  DBConn.LoginPrompt := False;
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'qmserver.ini');
  try
    Assert(Ini.ReadString('Database', 'ADConnectionString', '') <> '',
      'Missing [Database] ADConnectionString ini setting');
    ConnectADConnectionWithIni(DBConn, Ini, 'Database');
  finally
    FreeAndNil(Ini);
  end;
  FMSSQLEventStore := TMSSQLEventStore.Create(Pub, DBConn);
  ClearEvents;
end;

function NewEvent: IEvent;
begin
  Result := TEventImpl.Create;
  Result.Union := TEventUnionImpl.Create;
end;

function CompareEvents(Ev1, Ev2: IEvent): Boolean;
begin
  Result := (Ev1.AggregateKey.ToString = Ev2.AggregateKey.ToString)
    and (Ev1.Union.ToString = Ev2.Union.ToString);
end;

procedure TestTMSSQLEventStore.MakeBaselineEvents;
var
  Key: IAggrKey;
begin
  Key := GetBaselineKey;
  Assigned := NewEvent;
  Assigned.AggregateKey := Key;
  Assigned.Union.LocateAssigned := TLocateAssignedImpl.Create;
  Assigned.Union.LocateAssigned.LocateID := 11111111;
  Assigned.Union.LocateAssigned.FromLocatorID := 6002;
  Assigned.Union.LocateAssigned.ToLocatorID := 3512;
  Assigned.Union.LocateAssigned.ChangedByID := 3510;
  Assigned.Union.LocateAssigned.ChangedDate := '2013-12-30T07:32:10.333';

  Scheduled := NewEvent;
  Scheduled.Union.TicketScheduled := TTicketScheduledImpl.Create;
  Scheduled.AggregateKey := Key;
  Scheduled.Union.TicketScheduled.WorkloadDate := '2013-12-30';
  Scheduled.Union.TicketScheduled.ChangedByID := 3510;
  Scheduled.Union.TicketScheduled.ChangedDate := '2013-12-30T07:45:00.000';

  Closed := NewEvent;
  Closed.Union.LocateClosed := TLocateClosedImpl.Create;
  Closed.AggregateKey := Key;
  Closed.Union.LocateClosed.LocateID := 11111111;
  Closed.Union.LocateClosed.ClosedByEmpID := 6002;
  Closed.Union.LocateClosed.ClientCode := 'ATT-55';
  Closed.Union.LocateClosed.CallCenter := 'OCC2';
  Closed.Union.LocateClosed.ClosedDate := '2013-12-30T10:00:11.222';
  Closed.Union.LocateClosed.StatusCode := 'M';
  Closed.Union.LocateClosed.CompletedTicket := True;

  Reopened := NewEvent;
  Reopened.Union.LocateReopened := TLocateReopenedImpl.Create;
  Reopened.AggregateKey := Key;
  Reopened.Union.LocateReopened.LocateID := 11111111;
  Reopened.Union.LocateReopened.ReopenedByEmpID := 6002;
  Reopened.Union.LocateReopened.ClientCode := 'ATT-55';
  Reopened.Union.LocateReopened.CallCenter := 'OCC2';
  Reopened.Union.LocateReopened.ReopenedDate := '2013-12-30T11:11:11.111';
  Reopened.Union.LocateReopened.StatusCode := '-R';
  Reopened.Union.LocateReopened.ReopenedTicket := True;
end;

procedure TestTMSSQLEventStore.ClearEvents;
const
  SQL = 'delete from event_log where aggregate_type = 1' +
    ' and aggregate_id = 12344321';
begin
  DBConn.ExecSQL(SQL);
end;

procedure TestTMSSQLEventStore.TearDown;
begin
  DBConn.Close;
  FreeAndNil(DBConn);
end;

procedure TestTMSSQLEventStore.TestGetEventsForAggr;
var
  EventsInDB: IEventList;
begin
  TestStoreEvents; // to add our events to the db
  EventsInDB := FMSSQLEventStore.GetEventsForAggregate(GetBaselineKey);
  CheckEquals(4, EventsInDB.Items.Count);
  CheckDBEvents(EventsInDB);
end;

function TestTMSSQLEventStore.GetStartingGUID: string;
const
  SQL = 'SELECT MAX(event_id) FROM event_log ' +
        'WHERE NOT (aggregate_type = 1 AND aggregate_id = 12344321)';
begin
  Result := DBConn.ExecSQLScalar(SQL);
end;

procedure TestTMSSQLEventStore.TestGetEventsSince;
var
  EventsInDB: IEventList;
  StartingID: string;
begin
  TestStoreEvents; // to add our 4 events to the db
  StartingID := GetStartingGUID;
  // get the first 3
  EventsInDB := FMSSQLEventStore.GetEventsSince(StartingID, 3);
  CheckEquals(3, EventsInDB.Items.Count);
  CheckDBEvents(EventsInDB);
  // get the last one
  StartingID := EventsInDB.Items[2].EventID;
  EventsInDB := FMSSQLEventStore.GetEventsSince(StartingID, 3);
  CheckEquals(1, EventsInDB.Items.Count);
  CheckDBEvents(EventsInDB);
end;

procedure TestTMSSQLEventStore.CheckDBEvents(EventsInDB: IEventList);
var
  Event: IEvent;
begin
  for Event in EventsInDB.Items do begin
    CheckNotEquals('', Event.EventID);
    if Event.Union.LocateAssigned <> nil then begin
      Check(CompareEvents(Assigned, Event));
      CheckNull(Event.Union.TicketScheduled);
      CheckNull(Event.Union.LocateClosed);
      CheckNull(Event.Union.LocateReopened);
    end;
    if Event.Union.TicketScheduled <> nil then begin
      Check(CompareEvents(Scheduled, Event));
      CheckNull(Event.Union.LocateAssigned);
      CheckNull(Event.Union.LocateClosed);
      CheckNull(Event.Union.LocateReopened);
    end;
    if Event.Union.LocateClosed <> nil then begin
      Check(CompareEvents(Closed, Event));
      CheckNull(Event.Union.TicketScheduled);
      CheckNull(Event.Union.LocateAssigned);
      CheckNull(Event.Union.LocateReopened);
    end;
    if Event.Union.LocateReopened <> nil then begin
      Check(CompareEvents(Reopened, Event));
      CheckNull(Event.Union.TicketScheduled);
      CheckNull(Event.Union.LocateAssigned);
      CheckNull(Event.Union.LocateClosed);
    end;
  end;
end;

function TestTMSSQLEventStore.GetBaselineKey: IAggrKey;
begin
  Result := TAggrKeyImpl.Create;
  Result.AggrType := TAggregateType.atTicket;
  Result.AggrID := 12344321;
end;

procedure TestTMSSQLEventStore.TestStoreEvents;
var
  EventList: IEventList;
  Key: IAggrKey;
begin
  MakeBaselineEvents;
  Key := GetBaselineKey;
  EventList := TEventListImpl.Create;
  EventList.Items := TThriftListImpl<IEvent>.Create;
  EventList.Items.Add(Assigned);
  EventList.Items.Add(Scheduled);
  EventList.Items.Add(Closed);
  EventList.Items.Add(Reopened);

  DBConn.StartTransaction;
  try
    FMSSQLEventStore.SaveEvents(Key, EventList, -1);
    DBConn.Commit;
    Check(True);
  except
    DBConn.Rollback;
    raise;
  end;
end;

initialization
  RegisterTest(TestTMSSQLEventStore.Suite);

end.

