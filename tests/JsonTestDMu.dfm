object TestJsonDM: TTestJsonDM
  OldCreateOrder = False
  Height = 150
  Width = 215
  object SP: TADStoredProc
    Connection = Conn
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    StoredProcName = 'QMReport.dbo.get_ticket6'
    Left = 80
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        ParamType = ptResult
      end
      item
        Position = 2
        Name = '@TicketID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = '@UpdatesSince'
        DataType = ftTimeStamp
        ParamType = ptInput
      end>
  end
  object Conn: TADConnection
    Params.Strings = (
      'Server=localhost'
      'OSAuthent=Yes'
      'Database=QMReport'
      'LoginTimeout=5'
      'MARS=yes'
      'DriverID=MSSQL')
    ResourceOptions.AssignedValues = [rvSilentMode, rvKeepConnection]
    ResourceOptions.SilentMode = True
    ResourceOptions.KeepConnection = False
    LoginPrompt = False
    Left = 24
    Top = 16
  end
end
