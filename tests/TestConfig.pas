unit TestConfig;

interface

uses
  SysUtils;

const
  KyleId = 206;

function GetTestIniFileName: string;

implementation

const
  TestDbIniName = 'TestConfig.ini';

function GetTestIniFileName: string;
begin
  Result := ExtractFilePath(ParamStr(0)) + TestDbIniName;
end;

end.

