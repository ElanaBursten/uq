unit TimeentryTestsDMu;

interface

uses
  SysUtils, Classes, DBISAMTb, DB, Variants, OdDbUtils;

type
  TTimeTestDM = class(TDataModule)
    TimesheetData: TDBISAMTable;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  public
    procedure MakeTimesheetData;
    procedure ClearTimesheetData;
    function FocusDay(AWorkEmp: Integer; AWorkDay: TDateTime): Integer;
  end;

implementation

{$R *.dfm}

{ TTimeTestDM }

procedure TTimeTestDM.ClearTimesheetData;
begin
  TimesheetData.Close;
  TimesheetData.EmptyTable;
  TimesheetData.Open;
end;

procedure TTimeTestDM.MakeTimesheetData;
begin
  ClearTimesheetData;

  with TimesheetData do begin
    // normal day
    Insert;
    FieldByName('entry_id').AsInteger := 20;
    FieldByName('work_emp_id').AsInteger := 3510;
    FieldByName('work_date').AsString := '1/25/2010';
    FieldByName('entry_by').AsInteger := 3510;
    FieldByName('entry_date').AsDateTime := Now;
    FieldByName('entry_date_local').AsDateTime := FieldByName('entry_date').AsDateTime;
    FieldByName('work_start1').AsDateTime := StrToTime('07:12');
    FieldByName('work_stop1').AsDateTime  := StrToTime('11:48');
    FieldByName('work_start2').AsDateTime := StrToTime('12:34');
    FieldByName('work_stop2').AsDateTime  := StrToTime('17:21');
    FieldByName('lunch_start').AsDateTime := FieldByName('work_stop1').AsDateTime;
    FieldByName('status').AsString := 'ACTIVE';
    Post;

    // early callout day
    Insert;
    FieldByName('entry_id').AsInteger := 30;
    FieldByName('work_emp_id').AsInteger := 3510;
    FieldByName('work_date').AsString := '1/26/2010';
    FieldByName('entry_by').AsInteger := 3510;
    FieldByName('entry_date').AsDateTime := Now;
    FieldByName('entry_date_local').AsDateTime := FieldByName('entry_date').AsDateTime;
    FieldByName('callout_start1').AsDateTime := StrToTime('01:11');
    FieldByName('callout_stop1').AsDateTime  := StrToTime('02:02');
    FieldByName('work_start1').AsDateTime := StrToTime('07:00');
    FieldByName('work_stop1').AsDateTime  := StrToTime('11:30');
    FieldByName('work_start2').AsDateTime := StrToTime('12:00');
    FieldByName('work_stop2').AsDateTime  := StrToTime('17:45');
    FieldByName('lunch_start').AsDateTime := FieldByName('work_stop1').AsDateTime;
    FieldByName('status').AsString := 'ACTIVE';
    Post;

    // day with some special hours & no lunch break
    Insert;
    FieldByName('entry_id').AsInteger := 40;
    FieldByName('work_emp_id').AsInteger := 3510;
    FieldByName('work_date').AsString := '1/27/2010';
    FieldByName('entry_by').AsInteger := 3510;
    FieldByName('entry_date').AsDateTime := Now;
    FieldByName('entry_date_local').AsDateTime := FieldByName('entry_date').AsDateTime;
    FieldByName('work_start1').AsDateTime := StrToTime('08:00');
    FieldByName('work_stop1').AsDateTime  := StrToTime('10:30');
    FieldByName('leave_hours').AsFloat := 5.5;
    FieldByName('status').AsString := 'ACTIVE';
    Post;

    // start time with no end time
    Insert;
    FieldByName('entry_id').AsInteger := 50;
    FieldByName('work_emp_id').AsInteger := 3510;
    FieldByName('work_date').AsString := '1/28/2010';
    FieldByName('entry_by').AsInteger := 3510;
    FieldByName('entry_date').AsDateTime := Now;
    FieldByName('entry_date_local').AsDateTime := FieldByName('entry_date').AsDateTime;
    FieldByName('work_start1').AsDateTime := StrToTime('07:12');
    FieldByName('status').AsString := 'ACTIVE';
    Post;

    // hours for same day, but someone else
    Insert;
    FieldByName('entry_id').AsInteger := 60;
    FieldByName('work_emp_id').AsInteger := 811;
    FieldByName('work_date').AsString := '1/25/2010';
    FieldByName('entry_by').AsInteger := 811;
    FieldByName('entry_date').AsDateTime := Now;
    FieldByName('entry_date_local').AsDateTime := FieldByName('entry_date').AsDateTime;
    FieldByName('work_start1').AsDateTime := StrToTime('05:00');
    FieldByName('work_stop1').AsDateTime  := StrToTime('12:00');
    FieldByName('work_start2').AsDateTime := StrToTime('12:30');
    FieldByName('work_stop2').AsDateTime  := StrToTime('16:30');
    FieldByName('status').AsString := 'ACTIVE';
    FieldByName('lunch_start').AsDateTime := FieldByName('work_stop1').AsDateTime;
    Post;

    // hours with a late lunch
    Insert;
    FieldByName('entry_id').AsInteger := 70;
    FieldByName('work_emp_id').AsInteger := 811;
    FieldByName('work_date').AsString := '1/26/2010';
    FieldByName('entry_by').AsInteger := 811;
    FieldByName('entry_date').AsDateTime := Now;
    FieldByName('entry_date_local').AsDateTime := FieldByName('entry_date').AsDateTime;
    FieldByName('work_start1').AsDateTime := StrToTime('05:00');
    FieldByName('work_stop1').AsDateTime  := StrToTime('09:00');
    FieldByName('work_start2').AsDateTime := StrToTime('10:30');
    FieldByName('work_stop2').AsDateTime  := StrToTime('13:40');
    FieldByName('work_start3').AsDateTime := StrToTime('14:20');
    FieldByName('work_stop3').AsDateTime  := StrToTime('18:30');
    FieldByName('status').AsString := 'ACTIVE';
    FieldByName('lunch_start').AsDateTime := FieldByName('work_stop2').AsDateTime;
    Post;
  end;
end;

procedure TTimeTestDM.DataModuleCreate(Sender: TObject);
begin
  if not TimesheetData.Exists then
    TimesheetData.CreateTable;  // in memory
end;

procedure TTimeTestDM.DataModuleDestroy(Sender: TObject);
begin
  TimesheetData.Close;
end;

function TTimeTestDM.FocusDay(AWorkEmp: Integer; AWorkDay: TDateTime): Integer;
begin
  if TimesheetData.Locate('work_emp_id;work_date', VarArrayOf([AWorkEmp, AWorkDay]), []) then
    Result := TimesheetData.FieldByName('entry_id').AsInteger
  else
    Result := 0;
end;

end.
