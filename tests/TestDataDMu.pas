unit TestDataDMu;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Variants;

type
  TTestDM = class(TDataModule)
    MemTable: TADMemTable;
    TicketData: TADMemTable;
  public
    function CreateCoordinateData: TDataSet;
    function CreateTicketData: TDataSet;
  end;

const
  CoordRow1ID = 1234;
  CoordRow1Lat = 38.788269;
  CoordRow1Long = -77.950899;
  CoordRow1Name = 'Point 1';
  CoordRow1Comment = 'A test comment with some escapable chars ... {} "" : , <>';
  CoordRow2ID = 4567;
  CoordRow2Lat = 34.084036;
  CoordRow2Long = -84.272890;
  CoordRow2Name = 'Point 2';
  CoordRow2Comment = 'A different comment.';

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TTestDM }

function TTestDM.CreateCoordinateData: TDataSet;

  procedure CreateTable;
  begin
    with MemTable.FieldDefs do begin
      with AddFieldDef do begin
        Name := 'point_id';
        DataType := ftInteger;
      end;
      with AddFieldDef do begin
        Name := 'latitude';
        DataType := ftFloat;
      end;
      with AddFieldDef do begin
        Name := 'longitude';
        DataType := ftFloat;
      end;
      with AddFieldDef do begin
        Name := 'name';
        DataType := ftString;
        Size := 50;
      end;
      with AddFieldDef do begin
        Name := 'comment';
        DataType := ftString;
        Size := 200;
      end;
    end;
  end;

  procedure AddData;
  begin
    with MemTable do begin
      Open;
      AppendRecord([CoordRow1ID, CoordRow1Lat, CoordRow1Long, CoordRow1Name, CoordRow1Comment]);
      AppendRecord([CoordRow2ID, CoordRow2Lat, CoordRow2Long, CoordRow2Name, CoordRow2Comment]);
    end;
  end;

begin
  MemTable.Close;

  CreateTable;
  AddData;
  Result := MemTable;
end;

function TTestDM.CreateTicketData: TDataSet;
  procedure CreateTable;
  begin
    with TicketData.FieldDefs do begin
      with AddFieldDef do begin Name := 'ticket_id'; DataType := ftInteger; end;
      with AddFieldDef do begin Name := 'ticket_number'; DataType := ftString; Size := 20; end;
      with AddFieldDef do begin Name := 'ticket_format'; DataType := ftString; Size := 20; end;
      with AddFieldDef do begin Name := 'kind'; DataType := ftString; Size := 20; end;
      with AddFieldDef do begin Name := 'due_date'; DataType := ftDateTime; end;
      with AddFieldDef do begin Name := 'work_adddress_street'; DataType := ftString; Size := 60; end;
      with AddFieldDef do begin Name := 'work_address_number'; DataType := ftString; Size := 10; end;
      with AddFieldDef do begin Name := 'work_city'; DataType := ftString; Size := 40; end;
      with AddFieldDef do begin Name := 'work_county'; DataType := ftString; Size := 40; end;
      with AddFieldDef do begin Name := 'work_state'; DataType := ftString; Size := 2; end;
      with AddFieldDef do begin Name := 'work_description'; DataType := ftString; Size := 3500; end;
      with AddFieldDef do begin Name := 'work_type'; DataType := ftString; Size := 90; end;
      with AddFieldDef do begin Name := 'map_page'; DataType := ftString; Size := 20; end;
      with AddFieldDef do begin Name := 'ticket_type'; DataType := ftString; Size := 38; end;
      with AddFieldDef do begin Name := 'alert'; DataType := ftString; Size := 1; end;
      with AddFieldDef do begin Name := 'work_priority_sort'; DataType := ftInteger; end;
      with AddFieldDef do begin Name := 'work_priority'; DataType := ftString; Size := 20; end;
      with AddFieldDef do begin Name := 'ticket_is_visible'; DataType := ftBoolean; end;
      with AddFieldDef do begin Name := 'est_start_time'; DataType := ftDateTime; end;
      with AddFieldDef do begin Name := 'workload_date'; DataType := ftDateTime; end;
      with AddFieldDef do begin Name := 'image_path'; DataType := ftString; Size := 500; end;
    end;
  end;

  procedure AddData;
  begin
    TicketData.Open;
    TicketData.AppendRecord([123456, 'TICKET01', 'OCC3', 'ROUTINE',
      EncodeDate(2012, 3, 4) + EncodeTime(5, 6, 7, 8), 'MAIN STREET', '123',
      'SOMEWHERE', 'THERE', 'GA', 'TESTING THAT TICKET SUMMARY LOADS OK',
      'TESTING', 'MAP PAGE 89', 'OCC3', 'N', 1, 'NORMAL', True, Null,
      EncodeDate(2012, 3, 4), '..\tickets\images\image-123456.png']);
    TicketData.AppendRecord([123457, 'TICKET02', 'OCC3', 'EMERGENCY',
      EncodeDate(2012, 3, 4) + EncodeTime(5, 6, 7, 89), 'DIRT RD', 'A',
      'ELSEWHERE', 'THERE', 'GA', 'STILL TESTING TICKET SUMMARY LOADS OK',
      'TESTING', 'MAP PAGE 91', 'OCC3', 'Y', 9, 'HIGH', True, Null,
      EncodeDate(2012, 3, 4), '..\tickets\images\image-123457.png']);
  end;

begin
  TicketData.Close;
  CreateTable;
  AddData;
  Result := TicketData;
end;

end.
