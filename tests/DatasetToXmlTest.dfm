object DatasetToXmlTestDM: TDatasetToXmlTestDM
  OldCreateOrder = False
  Left = 562
  Top = 473
  Height = 500
  Width = 582
  object Alpha: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.34 Build 7'
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'Name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Address'
        DataType = ftString
        Size = 20
      end>
    TableName = 'Alpha'
    StoreDefs = True
    Left = 48
    Top = 24
  end
  object Beta: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.34 Build 7'
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'Name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Address'
        DataType = ftString
        Size = 20
      end>
    TableName = 'Beta'
    StoreDefs = True
    Left = 48
    Top = 88
  end
end
