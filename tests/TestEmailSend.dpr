program TestEmailSend;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options 
  to use the console test runner.  Otherwise the GUI test runner will be used by 
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

{%File 'TestEmailSend.ini'}

uses
  Forms,
  TestFramework,
  GUITestRunner,
  TextTestRunner,
  OdUqInternet in '..\common\OdUqInternet.pas',
  TestOdInternetUtil in 'TestOdInternetUtil.pas',
  OdInternetUtil in '..\common\OdInternetUtil.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas';

{$R *.RES}

begin
  Application.Initialize;
  if IsConsole then
    TextTestRunner.RunRegisteredTests
  else
    GUITestRunner.RunRegisteredTests;
end.

