unit DatasetToXmlTest;

interface

uses
  SysUtils, Classes, TestFrameWork, DB, DbIsamTb;

type
  TDatasetToXmlTestDM = class(TDataModule)
    Alpha: TDBISAMTable;
    Beta: TDBISAMTable;
  private
  public
  end;

  TDatasetToXmlTest = class(TTestCase)
  private
    FDM: TDatasetToXmlTestDM;
    procedure Cleanup;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestRowsPresent;
    procedure TestOneToXml;
    procedure TestTwoToXml;
    procedure TestFirstNull;
    procedure TestOverlapRemoval;
  end;

implementation

uses
  OdDataSetToXml, MSXML2_TLB;

{$R *.dfm}

{ TSqlToXmlTest }

procedure TDatasetToXmlTest.SetUp;
begin
  inherited;
  Cleanup;
  FDM := TDatasetToXmlTestDM.Create(nil);

  with FDM do begin
    Alpha.CreateTable;
    Alpha.Open;
    Alpha.AppendRecord([10, 'Bob', 'Knollshire']);
    Alpha.AppendRecord([11, 'Bill', 'Craigshire']);
    Alpha.AppendRecord([12, 'Betty', 'Willowshire']);
    Alpha.First;

    Beta.CreateTable;
    Beta.Open;
    Beta.AppendRecord([11, 'Anne', 'Memory Lane']);
    Beta.AppendRecord([13, 'Jill', 'Brookhaven']);
    Beta.AppendRecord([15, 'Butch', 'Havenbrook']);
    Beta.First;
  end;
end;

procedure TDatasetToXmlTest.TearDown;
begin
  inherited;
  Cleanup;
end;

procedure TDatasetToXmlTest.TestRowsPresent;
begin
  CheckEquals(3, FDM.Alpha.RecordCount);
  CheckEquals(3, FDM.Beta.RecordCount);
end;

procedure TDatasetToXmlTest.TestOneToXml;
var
  X: IXMLDOMDocument;
const
  Expected = '<rows MaxRows="100" ActualRows="3" TotalRows="3"><schema>'+
  '<field name="ID" type="ftInteger" attributes="" size="0" precision="0"/>'+
  '<field name="Name" type="ftString" attributes="" size="20" precision="0"/>'+
  '<field name="Address" type="ftString" attributes="" size="20" precision="0"/>'+
  '</schema>'+
  '<row ID="10" Name="Bob" Address="Knollshire"/>'+
  '<row ID="11" Name="Bill" Address="Craigshire"/>'+
  '<row ID="12" Name="Betty" Address="Willowshire"/>'+
  '</rows>';
begin
  //X := ConvertDataSetToXML([FDM.Alpha], 100);
  CheckEquals( Expected  , Trim(X.xml));
end;

procedure TDatasetToXmlTest.TestTwoToXml;
var
  X: IXMLDOMDocument;
const
  Expected = '<rows MaxRows="100" ActualRows="6" TotalRows="6"><schema>'+
  '<field name="ID" type="ftInteger" attributes="" size="0" precision="0"/>'+
  '<field name="Name" type="ftString" attributes="" size="20" precision="0"/>'+
  '<field name="Address" type="ftString" attributes="" size="20" precision="0"/>'+
  '</schema>'+
  '<row ID="10" Name="Bob" Address="Knollshire"/>'+
  '<row ID="11" Name="Bill" Address="Craigshire"/>'+
  '<row ID="12" Name="Betty" Address="Willowshire"/>'+
  '<row ID="11" Name="Anne" Address="Memory Lane"/>'+
  '<row ID="13" Name="Jill" Address="Brookhaven"/>'+
  '<row ID="15" Name="Butch" Address="Havenbrook"/>'+
  '</rows>';
begin
  //X := ConvertDataSetToXML([FDM.Alpha, FDM.Beta], 100);
  CheckEquals( Expected  , Trim(X.xml));
end;

procedure TDatasetToXmlTest.TestFirstNull;
var
  X: IXMLDOMDocument;
const
  Expected = '<rows MaxRows="100" ActualRows="3" TotalRows="3"><schema>'+
  '<field name="ID" type="ftInteger" attributes="" size="0" precision="0"/>'+
  '<field name="Name" type="ftString" attributes="" size="20" precision="0"/>'+
  '<field name="Address" type="ftString" attributes="" size="20" precision="0"/>'+
  '</schema>'+
  '<row ID="11" Name="Anne" Address="Memory Lane"/>'+
  '<row ID="13" Name="Jill" Address="Brookhaven"/>'+
  '<row ID="15" Name="Butch" Address="Havenbrook"/>'+
  '</rows>';
begin
  //X := ConvertDataSetToXML([nil, FDM.Beta], 100);
  CheckEquals( Expected  , Trim(X.xml));
end;

procedure TDatasetToXmlTest.TestOverlapRemoval;
var
  X: IXMLDOMDocument;
const
  Expected = '<rows MaxRows="100" ActualRows="5" TotalRows="5"><schema>'+
  '<field name="ID" type="ftInteger" attributes="" size="0" precision="0"/>'+
  '<field name="Name" type="ftString" attributes="" size="20" precision="0"/>'+
  '<field name="Address" type="ftString" attributes="" size="20" precision="0"/>'+
  '</schema>'+
  '<row ID="10" Name="Bob" Address="Knollshire"/>'+
  '<row ID="11" Name="Bill" Address="Craigshire"/>'+
  '<row ID="12" Name="Betty" Address="Willowshire"/>'+
  '<row ID="13" Name="Jill" Address="Brookhaven"/>'+
  '<row ID="15" Name="Butch" Address="Havenbrook"/>'+
  '</rows>';
begin
  //X := ConvertDataSetToXML([FDM.Alpha, FDM.Beta], 100, 'ID');
  CheckEquals( Expected  , Trim(X.xml));
end;

procedure TDatasetToXmlTest.Cleanup;
begin
  if Assigned(FDM) then begin
    FDM.Alpha.Close;
    FDM.Alpha.DeleteTable;
    FDM.Beta.Close;
    FDM.Beta.DeleteTable;
  end;
  FreeAndNil(FDM);
end;

initialization
  TestFramework.RegisterTest(TDatasetToXmlTest.Suite);

end.
