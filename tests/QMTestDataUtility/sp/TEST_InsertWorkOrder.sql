if object_id('dbo.TEST_InsertWorkOrder') is not null
  drop procedure dbo.TEST_InsertWorkOrder
go

create proc TEST_InsertWorkOrder (
  @NumberToGenerate int,
  @WOKind varchar(40), 
  @AssignedToID int,
  @WOPostAppend varchar(10) = '' --some string to append to work_order.wo_number that's handy for testing
)
as
  set nocount on

  DECLARE @intFlag int
  set @intFlag = 1
  
  WHILE (@intFlag < (@NumberToGenerate + 1))
  BEGIN  
    --do insert
    insert into work_order (wo_number, assigned_to_id, client_id, parsed_ok, wo_source, kind, status,closed,transmit_date,due_date, work_type, work_address_number, work_address_number_2,
      work_address_street, work_cross, work_county, work_city, work_state, work_zip, work_lat, work_long, caller_name, caller_contact, caller_phone, caller_cellular,  caller_fax,
      caller_altcontact, caller_altphone, caller_email, client_wo_number, job_number, client_order_num, 
      client_master_order_num, wire_center , work_center , central_office, serving_terminal, circuit_number,f2_cable, terminal_port, f2_pair,
      image )
    values ('LAM' + CONVERT(varchar(5),@intFlag) + @WOPostAppend, @AssignedToID, 3185, 1, 'Atlanta',@WOKind,'-R',0,GETDATE(), DATEADD(DAY, 10,GETDATE()), 'This is a test work order', '100','300', 
      'State Bridge', 'Old Milton', 'Fulton', 'Alpharetta', 'GA', '30080', '30.229273', '-95.564872', 'Bob Smith', 'William Jones', '770-888-3645', '404-360-7654', '770-888-2341',
      'James Robinson', '404-374-3847', 'bsmith@gmail.com', 'CLIWON' + CONVERT(varchar(5),@intFlag), 'JOB' + CONVERT(varchar(5),@intFlag), 'CLION' + CONVERT(varchar(5),@intFlag),
      'CLIMON' + CONVERT(varchar(5),@intFlag),'301627', '22','1222', '53722', '11/BURY/QAWN00/VZMD', 'H2036','8', '136',
      '<?xml version="1.0" encoding="utf-8" standalone="yes"?> <XML Type="WorkOrder">   <INTERNALNUMBER>V'+CONVERT(varchar(5),@intFlag) + @WOPostAppend+'</INTERNALNUMBER>   <TYPE>NORMAL</TYPE>   <JOBID>E7018430</JOBID> '+
      '  <ORDERNUMBER>BMD442O00</ORDERNUMBER>   <DUEDATE>3/7/2011 11:00:00 PM</DUEDATE>   <MON>MD00091749136</MON>   <ISSUEDATE>2/28/2011 7:27:00 PM</ISSUEDATE> '+
      '  <CUSTOMERNAME>A WEBER</CUSTOMERNAME>   <CUSTOMERADDRESS>25 TURNHAM LN</CUSTOMERADDRESS>   <CUSTOMERCITY>GAITHERSBURG</CUSTOMERCITY>   <CUSTOMERSTATE>MD</CUSTOMERSTATE> '+
      '  <CUSTOMERZIP>20878</CUSTOMERZIP>   <CUSTOMERCONTACTNUMBER>(301) 975-2377</CUSTOMERCONTACTNUMBER>   <CUSTOMERBTN>(A03) 892-6460</CUSTOMERBTN>   <WIRECENTER>301251</WIRECENTER> '+
      '  <WORKCENTER>22</WORKCENTER>   <CO>1222</CO>   <SERVINGTERMINAL>HH F 21 TURNHAM LN</SERVINGTERMINAL>   <TERMINALGPS>39.106959 / -77.231314</TERMINALGPS>   <MAP>5163</MAP> '+
      '  <GRID>D3</GRID>   <CIRCUITID>11/BURY/442O00/ /VZMD</CIRCUITID>   <F2CABLE>H4091</F2CABLE>   <TERMINALPORT>2</TERMINALPORT>   <F2PAIR>266</F2PAIR> </XML>') 

    Set @intFlag = @intFlag + 1
    
    IF (@intFlag > @NumberToGenerate)
      Break
    else
      Continue
    
  END  

  --Generate the corresponding work_order_version records
  INSERT INTO work_order_version (wo_id,wo_revision, wo_number, work_type, transmit_date, processed_date,arrival_date,image, wo_source)
  select wo.wo_id, 'Generated', wo.wo_number, wo.work_type, wo.transmit_date, GETDATE(),wo.transmit_date,  wo.image, wo.wo_source 
    from work_order wo
    where wo.wo_id not in (select wo_id from work_order_version);

GO

grant execute on TEST_InsertWorkOrder to uqweb, QManagerRole
go


/*
execute dbo.TEST_InsertWorkOrder 1, 'NORMAL', 3510, '_TestImage'
execute dbo.TEST_InsertWorkOrder 2, 'PRIORITY', 3510
execute dbo.TEST_InsertWorkOrder 2, 'CANCEL', 3510
select wo.modified_date, * from work_order wo where work_type = 'This is a test work order' order by wo.modified_date desc

select * from client where call_center= 'Atlanta' and client_name like '%AGL%' and active =1
select * from call_center where cc_code = 'Atlanta'
*/



