--Note that this procedure to insert test tickets will clean up any prior test tickets that use the same @TicketPreAppend prefix
if object_id('dbo.TEST_InsertTicket') is not null
  drop procedure dbo.TEST_InsertTicket
go

create proc TEST_InsertTicket (
  @NumberToGenerate int,
  @TransmitDateTime datetime, -- Test tickets' transmit date/time
  @LocatorID int, -- Emp ID assigned to the locates
  @cc varchar(15), -- Call center for the tickets
  @TicketPreAppend varchar(10) = '' -- some string to append to ticket.ticket_number that's handy for testing
)
as
  set nocount on

  -- Clean data from prior test runs
  delete from assignment
    where locate_id in (select locate.locate_id from locate inner join ticket on locate.ticket_id=ticket.ticket_id where ticket.ticket_number like @TicketPreAppend + '%')
  delete from locate_hours
    where locate_id in (select locate.locate_id from locate inner join ticket on locate.ticket_id=ticket.ticket_id where ticket.ticket_number like @TicketPreAppend + '%')
  delete from locate_status
    where locate_id in (select locate.locate_id from locate inner join ticket on locate.ticket_id=ticket.ticket_id where ticket.ticket_number like @TicketPreAppend + '%')
  delete from locate_snap
    where locate_id in (select locate.locate_id from locate inner join ticket on locate.ticket_id=ticket.ticket_id where ticket.ticket_number like @TicketPreAppend + '%')
  delete from notes
    where foreign_type=5 and foreign_id in (select locate.locate_id from locate inner join ticket on locate.ticket_id=ticket.ticket_id where ticket.ticket_number like @TicketPreAppend + '%')
  delete from locate
    where ticket_id in (select ticket_id from ticket where ticket.ticket_number like @TicketPreAppend + '%')
  delete from ticket_snap
    where ticket_id in (select ticket_id from ticket where ticket.ticket_number like @TicketPreAppend + '%')
  delete from ticket_version
    where ticket_id in (select ticket_id from ticket where ticket.ticket_number like @TicketPreAppend + '%')
  delete from attachment
    where foreign_type=1 and foreign_id in (select ticket_id from ticket where ticket.ticket_number like @TicketPreAppend + '%')
  delete from notes
    where foreign_type=1 and foreign_id in (select ticket_id from ticket where ticket.ticket_number like @TicketPreAppend + '%')
  delete from ticket_activity_ack
    where ticket_id in (select ticket_id from ticket where ticket.ticket_number like @TicketPreAppend + '%')
  delete from jobsite_arrival
    where ticket_id in (select ticket_id from ticket where ticket.ticket_number like @TicketPreAppend + '%')
  update damage set ticket_id = null
    where ticket_id in (select ticket_id from ticket where ticket.ticket_number like @TicketPreAppend + '%')
  delete from ticket where ticket.ticket_number like @TicketPreAppend + '%'
  
  DECLARE @intFlag int 
  set @intFlag = 1
  
  WHILE (@intFlag < (@NumberToGenerate + 1))
  BEGIN  
    --insert the test tickets
    INSERT INTO ticket(ticket_number, work_lat, work_long, parsed_ok, ticket_format, kind, transmit_date, due_date, work_description, work_state, work_county, work_city, work_address_number,
    work_address_street, work_type, work_date, work_remarks, company, con_type, con_name, con_address, con_city, con_state, call_date, caller, caller_contact, caller_phone, image, modified_date,
    active, ticket_type, source)
    VALUES (@TicketPreAppend + CONVERT(varchar(5),@intFlag),39.5594,-76.2688,1,@cc,'NORMAL',@TransmitDateTime,DateAdd(Day, 2, @TransmitDateTime),'THIS IS A TEST WORK DESCRIPTION - MARK BOTH',
      'PA','MONTGOMERY','SALFORD TWP','7082','DIRT RD','DIGGING SOME TEST HOLES',DateAdd(Day, 3, @TransmitDateTime),'THESE ARE TEST WORK REMARKS','BIG SHOVEL CORP','EXCAVATOR','TROY ROCK','THE STREET',
      'BIG CITY','PA',DateAdd(Day, -1, @TransmitDateTime),'TROY ROCK','TROY ROCK','123-456-7890','THIS TEST TICKET DOES NOT HAVE AN IMAGE',GetDate(),1,'NEW GRID','TEST TICK' + CONVERT(varchar(5),@intFlag))
	  
	-- Add client locates for each test ticket --TODO add client code string, iterate it to add as many locates as desired.  
    -- Add client locates for each test ticket
    INSERT INTO locate (ticket_id, client_code, client_id, status) select ticket_id, 'KC',  2525, '-R' from ticket where ticket_number = @TicketPreAppend + CONVERT(varchar(5),@intFlag) and ticket.ticket_format = @cc and source = 'TEST TICK' + CONVERT(varchar(5),@intFlag)
    INSERT INTO locate (ticket_id, client_code, client_id, status) select ticket_id, 'KCG', 2533, '-R' from ticket where ticket_number = @TicketPreAppend + CONVERT(varchar(5),@intFlag) and ticket.ticket_format = @cc and source = 'TEST TICK' + CONVERT(varchar(5),@intFlag)

    Set @intFlag = @intFlag + 1
    
    IF (@intFlag > @NumberToGenerate)
      Break
    else
      Continue
    
  END  

  --Generate the corresponding ticket_version records
  INSERT INTO ticket_version (ticket_id, ticket_number, ticket_type, transmit_date, arrival_date, processed_date, image, ticket_format, source)
    SELECT ticket_id, ticket_number, ticket_type, transmit_date, transmit_date, transmit_date, image, ticket_format, ticket_format
    FROM ticket
    WHERE ticket_id not in (SELECT DISTINCT ticket_id FROM ticket_version)
      and ticket_number like @TicketPreAppend + '%'
  
  -- Assign the test tickets to @LocatorID
  declare @loc table (
    locate_id int not null primary key
  )
  insert into @loc
    select distinct locate.locate_id
    from locate
    inner join ticket on locate.ticket_id=ticket.ticket_id
    where locate.assigned_to_id is null
      and ticket.ticket_format = @cc
      and ticket.ticket_number like @TicketPreAppend + '%'

  DECLARE @LocID int
  DECLARE LocateCursor CURSOR FOR SELECT locate_id FROM @loc

  OPEN LocateCursor
  FETCH NEXT FROM LocateCursor into @LocID
  WHILE @@FETCH_STATUS = 0
  BEGIN
    exec assign_locate @LocID, @LocatorID, Null
    FETCH NEXT FROM LocateCursor into @LocID
  END

  CLOSE LocateCursor
  DEALLOCATE LocateCursor  

GO

grant execute on TEST_InsertTicket to uqweb, QManagerRole
go


/*
DECLARE @TransmitDateTime datetime
SET @TransmitDateTime = GETDATE()
execute dbo.TEST_InsertTicket 2, @TransmitDateTime, 3512, '7501', 'T2989-'

select * from ticket where ticket_number like 'T2989-%'
select * from locate where ticket_id in (select ticket_id from ticket where ticket_number like 'T2989-%')
*/