unit TestStringObjectList;

interface

uses
  TestFramework, Classes, SysUtils;

type
  IString = interface
    function GetText: string;
    procedure SetText(const Value: string);
    property Text: string read GetText write SetText;
  end;

  TStringObject = class(TInterfacedObject, IString)
  strict private
    FText: string;
    function GetText: string;
    procedure SetText(const Value: string);
  public
    property Text: string read GetText write SetText;
    constructor Create(const AText: string);
  end;

  TestTStringObjectList = class(TTestCase)
  strict private
    FList: TStringList;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestAddStrings;
    procedure TestNVStrings;
  end;

implementation

{ TStringObject }

constructor TStringObject.Create(const AText: string);
begin
  inherited Create;
  Text := AText;
end;

function TStringObject.GetText: string;
begin
  Result := FText;
end;

procedure TStringObject.SetText(const Value: string);
begin
  FText := Value;
end;

{ TestTStringObjectList }

procedure TestTStringObjectList.SetUp;
begin
  FList := TStringList.Create(True);
end;

procedure TestTStringObjectList.TearDown;
begin
  FreeAndNil(FList);
end;

procedure TestTStringObjectList.TestAddStrings;
var
  Str: TStringObject;
  I: Integer;
begin
  for I := 1 to 10 do begin
    Str := TStringObject.Create('Code #' + IntToStr(I));
    FList.AddObject('Desc #' + IntToStr(I), TStringObject(Str));
  end;

  CheckEquals(10, FList.Count);
  CheckEquals('Desc #1', FList.Strings[0]);
  CheckEquals('Code #1', TStringObject(FList.Objects[0]).Text);

  CheckEquals('Desc #9', FList.Strings[8]);
  CheckEquals('Code #9', TStringObject(FList.Objects[8]).Text);

  CheckEquals('Code #3', TStringObject(FList.Objects[FList.IndexOf('Desc #3')]).Text);
end;

procedure TestTStringObjectList.TestNVStrings;
var
  Str: string;
  I: Integer;
begin
  for I := 1 to 10 do begin
    Str := 'Code #' + IntToStr(I);
    FList.Add('Desc #' + IntToStr(I) + '=' + Str);
  end;

  CheckEquals(10, FList.Count);
  CheckEquals('Desc #1=Code #1', FList.Strings[0]);
  CheckEquals('Code #1', FList.Values[FList.Names[0]]);

  CheckEquals('Desc #9=Code #9', FList.Strings[8]);
  CheckEquals('Code #9', FList.Values[FList.Names[8]]);

  CheckEquals('Code #3', FList.Values['Desc #3']);
end;

initialization

// Register any test cases with the test runner
RegisterTest(TestTStringObjectList.Suite);

end.
