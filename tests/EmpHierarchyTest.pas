unit EmpHierarchyTest;

interface

uses
  Classes, TestFrameWork, SysUtils, DB, SyncEngineU;

type
  TEmpHierarchyTest = class(TTestCase)
  private
    procedure UpdateProfitCenterCacheOld;
    procedure EmployeePCCache(List: TStrings);
  public
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestHierarchy;
    procedure TestUpdatePCCacheAsLocator;
    procedure TestUpdatePCCacheAsSupervisor;
    procedure TestUpdatePCCacheAsSuperUser;
  end;

implementation

uses DMu, QMConst, StateMgr, OdMiscUtils, LocalEmployeeDMu;

const
  CorpRootEmpId = 2676;
  TonyAlvarez = 3510;
  LestineBrown = 304;
  EdwardMoyers = 4552;

{ TEmpHierarchyTest }

procedure TEmpHierarchyTest.SetUp;
begin
  inherited;
  DM := TDM.Create(nil);
  Assert((DM.Database1.EngineVersion = '3.19') or (DM.Database1.EngineVersion > '4.24'),
    'DBISAM versions 3.20 thru 4.24 have MAJOR performance problems with self-joining ' +
    'updates that cause these tests to run for hours.');

  DM.UQState := TUQState.Create;
  DM.SetupQMService; // create an Engine

  // hack to use some good known data
  DM.Database1.Close;
  DM.Database1.Directory := '..\client\data\';
  DM.Database1.Open;

  DM.TestMode := True;
end;

procedure TEmpHierarchyTest.TestHierarchy;
var
  Hier: TStringList;
  OldHier: TStringList;
begin
  OldHier := nil;
  Hier := TStringList.Create;
  try
    DM.UQState.EmpID := TonyAlvarez;
    DM.Engine.FProfitCentersDirty := True;
    DM.UpdateProfitCenterCache;

    EmployeeDM.EmployeeList(Hier, CorpRootEmpID, StatusAll);
    UpdateProfitCenterCacheOld;
    OldHier := TStringList.Create;
    EmployeeDM.EmployeeList(OldHier, CorpRootEmpID, StatusAll);

    Check(OldHier.Equals(Hier), 'OldHier does not match Hier');
  finally
    FreeAndNil(Hier);
    FreeAndNil(OldHier);
  end;
end;

procedure TEmpHierarchyTest.TestUpdatePCCacheAsSuperUser;
var
  Hier: TStringList;
  OldHier: TStringList;
begin
  OldHier := nil;
  Hier := TStringList.Create;
  try
    DM.UQState.EmpID := TonyAlvarez;
    DM.Engine.FProfitCentersDirty := True;
    DM.UpdateProfitCenterCache;
    EmployeePCCache(Hier);

    UpdateProfitCenterCacheOld;
    OldHier := TStringList.Create;
    EmployeePCCache(OldHier);
    Hier.SaveToFile('Hier-TestUpdatePCCacheAsSuperUser.txt');
    OldHier.SaveToFile('OldHier-TestUpdatePCCacheAsSuperUser.txt');

    Check(OldHier.Equals(Hier), 'OldHier does not match Hier');
  finally
    FreeAndNil(Hier);
    FreeAndNil(OldHier);
  end;
end;

procedure TEmpHierarchyTest.TestUpdatePCCacheAsLocator;
var
  Hier: TStringList;
  OldHier: TStringList;
begin
  OldHier := nil;
  Hier := TStringList.Create;
  try
    DM.UQState.EmpID := EdwardMoyers;
    DM.Engine.FProfitCentersDirty := True;
    DM.UpdateProfitCenterCache;
    EmployeePCCache(Hier);

    UpdateProfitCenterCacheOld;
    OldHier := TStringList.Create;
    EmployeePCCache(OldHier);
    Hier.SaveToFile('Hier-TestUpdatePCCacheAsLocator.txt');
    OldHier.SaveToFile('OldHier-TestUpdatePCCacheAsLocator.txt');

    Check(OldHier.Equals(Hier), 'OldHier does not match Hier');
  finally
    FreeAndNil(Hier);
    FreeAndNil(OldHier);
  end;
end;

procedure TEmpHierarchyTest.TestUpdatePCCacheAsSupervisor;
var
  Hier: TStringList;
  OldHier: TStringList;
begin
  OldHier := nil;
  Hier := TStringList.Create;
  try
    DM.UQState.EmpID := LestineBrown;
    DM.Engine.FProfitCentersDirty := True;
    DM.UpdateProfitCenterCache;
    EmployeePCCache(Hier);

    UpdateProfitCenterCacheOld;
    OldHier := TStringList.Create;
    EmployeePCCache(OldHier);
    Hier.SaveToFile('Hier-TestUpdatePCCacheAsSupervisor.txt');
    OldHier.SaveToFile('OldHier-TestUpdatePCCacheAsSupervisor.txt');

    Check(OldHier.Equals(Hier), 'OldHier does not match Hier');
  finally
    FreeAndNil(Hier);
    FreeAndNil(OldHier);
  end;
end;

procedure TEmpHierarchyTest.EmployeePCCache(List: TStrings);
const
  Select = 'select emp_id, short_name, effective_pc, effective_payroll_pc, ' +
    'under_me_reports, under_me_timesheets ' +
    'from employee order by emp_id';
var
  DataSet: TDataSet;
begin
  Assert(Assigned(List));
  DataSet := DM.Engine.OpenQuery(Select);
  try
    List.BeginUpdate;
    try
      while not DataSet.Eof do begin
        List.AddObject(Format('%s PC: %s Payroll PC: %s Rpts: %s TSE: %s', [
          DataSet.FieldByName('short_name').AsString,
          DataSet.FieldByName('effective_pc').AsString,
          DataSet.FieldByname('effective_payroll_pc').AsString,
          DataSet.FieldByName('under_me_reports').AsString,
          DataSet.FieldByName('under_me_timesheets').AsString]),
          TObject(DataSet.FieldByName('emp_id').AsInteger));
        DataSet.Next;
      end;
    finally
      List.EndUpdate;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;


// old adjacency way of doing it:
procedure TEmpHierarchyTest.UpdateProfitCenterCacheOld;
var
  I: Integer;
  IDs: TIntegerArray;
  StartTime: TDateTime;

  procedure ShowElapsedTime(const Description: string);
  begin
    DM.Engine.AddToLog(Format(Description + ', Elapsed time = %.2f seconds',
     [(Now - StartTime) * 24 * 60 * 60]));
  end;
begin
  SetLength(IDs, 0);  // Suppress compiler warning

  if not DM.CanAccessProfitCenterCache then Exit;
  // if not DM.Engine.FProfitCentersDirty then Exit;

  DM.Engine.AddToLog('Updating profit center cache...');
  StartTime := Now;

  // Clear out cache result fields:
  DM.Engine.ExecQuery('update employee set effective_pc=null where effective_pc is not null');
  DM.Engine.ExecQuery('update employee set effective_pc=repr_pc_code where repr_pc_code is not null');
  DM.Engine.ExecQuery('update employee set under_me_reports=false where under_me_reports');
  DM.Engine.ExecQuery('update employee set under_me_timesheets=false where under_me_timesheets');

  for I := 1 to 10 do
    DM.Engine.ExecQuery('update employee set effective_pc = e2.effective_pc' +
              ' from employee , employee e2' +
              '  where employee.report_to = e2.emp_id' +
              '   and employee.effective_pc is null' +
              '   and e2.effective_pc is not null');

  DM.Engine.ExecQuery('update employee set effective_payroll_pc = effective_pc');
  DM.Engine.ExecQuery('update employee set effective_payroll_pc = payroll_pc_code where payroll_pc_code is not null');

  ShowElapsedTime('Updated profit center cache');
  StartTime := Now;

  IDs := EmployeeDM.EffectiveManagerIDsForReports;
  for I := Low(IDs) to High(IDs) do
    DM.Engine.ExecQuery('update employee set under_me_reports=true where emp_id=' + IntToStr(IDs[I]));

  for I := 1 to 10 do
    DM.Engine.ExecQuery('update employee set under_me_reports = true' +
              ' from employee , employee e2' +
              '  where employee.report_to = e2.emp_id' +
              '   and (employee.under_me_reports is null or employee.under_me_reports=false)' +
              '   and e2.under_me_reports');

  IDs := EmployeeDM.EffectiveManagerIDsForTimeSheets(RightTimesheetsFinalApprove);
  for I := Low(IDs) to High(IDs) do
    DM.Engine.ExecQuery('update employee set under_me_timesheets=true where emp_id=' + IntToStr(IDs[I]));

  for I := 1 to 10 do
    DM.Engine.ExecQuery('update employee set under_me_timesheets = true' +
              ' from employee , employee e2' +
              '  where employee.report_to = e2.emp_id' +
              '   and (employee.under_me_timesheets is null or employee.under_me_timesheets=false)' +
              '   and e2.under_me_timesheets');

  ShowElapsedTime('Updated under_me cache');

  DM.Engine.FProfitCentersDirty := False;
end;

procedure TEmpHierarchyTest.TearDown;
begin
  FreeAndNil(DM);
  inherited;
end;

initialization
  TestFramework.RegisterTest(TEmpHierarchyTest.Suite);

end.

