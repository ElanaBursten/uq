program EmailSender;
//EB: Test App - Removing Oasis Email

{$APPTYPE CONSOLE}

{$R '..\QMIcon.res'}
{$R '..\QMVersion.res' '..\QMVersion.rc'}

uses
  SysUtils,
  OdInternetUtil in '..\common\OdInternetUtil.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdExceptions in '..\common\OdExceptions.pas';

var
  IniName: string;

begin
  IniName := ChangeFileExt(ParamStr(0), '.ini');

  SendEmailWithIniSettings(IniName, 'EmailDestination', 'larry.killen@xqtrix.com', 'Test Message', 'Message Body Here');
  { TODO -oUser -cConsole Main : Insert code here }
end.
