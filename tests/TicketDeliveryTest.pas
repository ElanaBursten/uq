unit TicketDeliveryTest;

interface

uses
  Classes, Contnrs, TestFrameWork, SysUtils, DB, SyncEngineU, SingleEngineTest;

type
  TTicketDeliveryTest = class(TBaseTest)
  private
    //KyleEngine: TLocalCache;
    //BillEngine: TLocalCache;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestNothingToDeliver;
  end;

implementation

{ TTicketDeliveryTest }

procedure TTicketDeliveryTest.SetUp;
begin
  inherited;

  //InitAndEmptyLocalEngine(KyleEngine, 'data2');
  //InitAndEmptyLocalEngine(BillEngine, 'data3');

  SetupServerTables;
end;

procedure TTicketDeliveryTest.TearDown;
begin
  inherited;
  //FreeAndNil(KyleEngine);
  //FreeAndNil(BillEngine);
end;

{

start with empty DB, empty cache
put 10 locates in DB, 5 old (closed more than N days ago) and 5 current,
 all of which are assigned to me

sync
 should get 5 locates synced; old ones should not sync

}

{
sync manager
assign ticket
sync loc 1
see we have ticket / locate
assign ticket to someone else
sync loc 1
see we don;'t have the ticket in our list to display
sync loc 2
see we do have it
}


{
What shall I test?

Basic operations are:

Locator syncs tickets down
Locator makes changes to locate status, sees ticket status (local shadow)
Locator saves changes to locate status (only)

Manager gets "locator summary" data for left pan
Manager gets "ticket list" for right pane; shows locator's status, overall status
Manager gets minimal ticket detail for bottom
Manager gets full ticket detail for ticket detail screen

Check that manager's list = locator's list
check this repeatedly

Underlying operation: run querys, get results local, display.

SP:  status a locate
}


procedure TTicketDeliveryTest.TestNothingToDeliver;
var
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  Errors := TObjectList.Create;
  try
    Engine.Sync(55, Errors, SyncRequestSource);  // bogus EmpID, nothing to receive
  finally
    FreeAndNil(Errors);
  end;
  Check(True);
end;

initialization
  TestFramework.RegisterTest(TTicketDeliveryTest.Suite);

end.

