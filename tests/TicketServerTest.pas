unit TicketServerTest;

interface

uses
  Classes, TestFrameWork, SysUtils, DB, SyncEngineU,
  SingleEngineTest, MSXML2_TLB,
  QMService_Impl, QMServerLibrary_Intf;

type
  TTicketServerTest = class(TBaseTest)
  private
  public
  protected
  published
    procedure TestReassign1Locate_New;
    procedure TestReassign3Locates_New;
    procedure TestSecurityFailure;
    procedure TestSecurityMissing;
    procedure TestSecurityOK;
    procedure TestStatusLocate;
  end;

implementation

uses DMu, TestServiceU;

{ TTicketServerTest }

procedure TTicketServerTest.TestReassign1Locate_New;
var
  Locates: LocateList;
begin
  SetupServerTables;
  CheckEquals(204, TestService.AssignmentOf('2249', 'ENTGY08'));

  Locates := LocateList.Create;
  with Locates.Add do begin
    TicketID := TestService.FindTicketID('2249');
    ClientCode := 'ENTGY08';
  end;

  QMS.MoveLocates(TestingSecSession, Locates, 205);

  CheckEquals(205, TestService.AssignmentOf('2249', 'ENTGY08'));
end;

procedure TTicketServerTest.TestReassign3Locates_New;
var
  Locates: LocateList;
begin
  SetupServerTables;

  CheckEquals(204, TestService.AssignmentOf('2249', 'JEFSCH01'));
  CheckEquals(204, TestService.AssignmentOf('2249', 'LAGEJ01'));
  CheckEquals(206, TestService.AssignmentOf('2224', 'LAFCAB01'));

  Locates := LocateList.Create;
  with Locates.Add do begin
    TicketID := TestService.FindTicketID('2249');
    ClientCode := 'JEFSCH01';
  end;
  with Locates.Add do begin
    TicketID := TestService.FindTicketID('2249');
    ClientCode := 'LAGEJ01';
  end;
  QMS.MoveLocates(TestingSecSession, Locates, 205);

  Locates := LocateList.Create;
  with Locates.Add do begin
    TicketID := TestService.FindTicketID('2224');
    ClientCode := 'LAFCAB01';
  end;
  QMS.MoveLocates(TestingSecSession, Locates, 204);

  CheckEquals(205, TestService.AssignmentOf('2249', 'JEFSCH01'));
  CheckEquals(205, TestService.AssignmentOf('2249', 'LAGEJ01'));
  CheckEquals(204, TestService.AssignmentOf('2224', 'LAFCAB01'));
end;

procedure TTicketServerTest.TestSecurityFailure;
begin
  ExpectedException := Exception;
  QMS.Ping('this/that', 'whatever')
end;

procedure TTicketServerTest.TestSecurityMissing;
begin
  ExpectedException := Exception;
  QMS.Ping('', 'whatever')
end;

procedure TTicketServerTest.TestSecurityOK;
begin
  QMS.Ping('004024/1234', 'whatever');   // a valid login from the ref data
  QMS.Ping('004024/1234/0.0.1.1', 'whatever');   // with version ID, no problem
end;

procedure TTicketServerTest.TestStatusLocate;
var
  CL: TicketChangeList14;
  GenKeys: GeneratedKeyList;
begin
  CL := TicketChangeList14.Create;
  with CL.Add do begin
    TicketID.TicketID := TestService.FindTicketID('2249');
    with LocateChanges.Add do begin    // add new
      LocateID := -111;
      ClientCode := 'TEST123';
      Status := 'M';
      QtyMarked := 1;
    end;
    with LocateChanges.Add do begin
      LocateID := TestService.FindLocateID('2249', 'JEFSCH01');
      ClientCode := 'JEFSCH01';
      Status := 'M';
      QtyMarked := 1;
    end;
  end;

  CheckEquals('N', TestService.StatusOf('2249', 'JEFSCH01'));
  QMS.StatusTicketsLocates14(TestingSecSession, CL, GenKeys );
  CheckEquals('M', TestService.StatusOf('2249', 'JEFSCH01'));

  CL.Free;
  GenKeys.Free;
end;

initialization
  TestFramework.RegisterTest(TTicketServerTest.Suite);

end.

