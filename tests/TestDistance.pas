unit TestDistance;

interface

uses
  TestFramework, Classes, OdMiscUtils, SysUtils;

type
  TTestDistance = class(TTestCase)
  published
    procedure TestHaversine;
    procedure TestVincenty;
  end;

implementation

const
  MetersPerKM = 1000;
  // Lat/Long data from: http://www.getty.edu/research/conducting_research/vocabularies/tgn/
  // St Louis
  STLLat =  38.6167;  //  38 37 00 N
  STLLon = -90.1833;  // 090 11 00 W
  // Off Center St Louis 1
  OSLLat =  38.6168;
  OSLLon = -90.1834;
  // Off Center St Louis 2
  OSMLat =  38.6166;
  OSMLon = -90.1832;
  // East St Louis
  ESLLat =  38.6167;  //  38 37 00 N
  ESLLon = -90.1500;  // 090 09 00 W
  // Chicago
  CHILat =  41.8500;  //  41 51 00 N
  CHILon = -87.6500;  // 087 39 00 W
  // New York
  NYCLat =  40.7000;  //  40 42 00 N
  NYCLon = -74.0000;  // 074 00 00 W
  // Los Angeles
  LACLat =  34.0500;  //  34 03 00 N
  LACLon = -118.2333; // 118 14 00 W
  // Atlanta
  ATLLat =  33.7333;  //  33 44 00 N
  ATLLon = -84.3833;  // 084 23 00 W
  // Athens, GA
  ATHLat =  33.9500;  //  33 57 00 N
  ATHLon = -83.3667;  // 083 22 00 W
  // Decatur, GA
  DECLat =  33.7667;  //  33 46 00 N
  DECLon = -84.2833;  // 084 17 00 W

  // Constants calculated using: http://williams.best.vwh.net/gccalc.htm
  // and: http://www.windreader.com/geodesy/
  STLToCHIDistWGS84KM =  418.72965295637977;
  STLToATLDistWGS84KM =  751.9195780282297;
  STLToESLDistWGS84KM =    2.900158100366997;
  STLToNYCDistWGS84KM = 1405.8523382961132;
  STLToLACDistWGS84KM = 2558.373876065499;
  STLToOSLDistWGS84KM =    0.014109475318982316;
  STLToOSMDistWGS84KM =    0.014109482634517683;
  ATLToATHDistWGS84KM =   97.11335263346449;
  ATLToCHIDistWGS84KM =  945.5197870243429;
  ATLToDECDistWGS84KM =    9.978633331398388;
  // FAI sphere estimation (Radius = 6371, Inverse Flattening 1/f = 1000000000)
  STLToCHIDistSphKM   =  418.88324497754303;
  STLToATLDistSphKM   =  751.9141145957327;
  STLToESLDistSphKM   =    2.893133553343925;
  STLToNYCDistSphKM   = 1402.5156720496475;
  STLToLACDistSphKM   = 2552.9478041335065;
  STLToOSLDistSphKM   =    0.014111197631504866;
  STLToOSMDistSphKM   =    0.014109482634517683;
  ATLToATHDistSphKM   =   96.93159329129016;
  ATLToCHIDistSphKM   =  946.8752363494731;
  ATLToDECDistSphKM   =    9.963571844367262;

{ TTestDistance }

procedure TTestDistance.TestHaversine;

  procedure TestSTLToDist(Lat, Lon, Expected: Extended; Msg: string);
  var
    Actual: Extended;
    ExpectedStr: string;
    ActualStr: string;
  begin
    Actual := LatLongDistanceKM(STLLat, STLLon, Lat, Lon);
    ActualStr := FormatFloat('0.0000', Actual);
    ExpectedStr := FormatFloat('0.0000', Expected);
    CheckEqualsString(ExpectedStr, ActualStr, Msg);
  end;

begin
  CheckTrue(LatLongDistanceKM(STLLat, STLLon, STLLat, STLLon) = 0, 'STL to STL');
  CheckTrue(LatLongDistanceKM(CHILat, CHILon, CHILat, CHILon) = 0, 'STL to STL');
  TestSTLToDist(CHILat, CHILon, STLToCHIDistSphKM, 'STL to CHI');
  TestSTLToDist(CHILat, CHILon, STLToCHIDistSphKM, 'STL to CHI');
  TestSTLToDist(ATLLat, ATLLon, STLToATLDistSphKM, 'STL to ATL');
  TestSTLToDist(ESLLat, ESLLon, STLToESLDistSphKM, 'STL to ESL');
  TestSTLToDist(NYCLat, NYCLon, STLToNYCDistSphKM, 'STL to NYC');
  TestSTLToDist(LACLat, LACLon, STLToLACDistSphKM, 'STL to LAC');
  TestSTLToDist(OSLLat, OSLLon, STLToOSLDistSphKM, 'STL to OSL');
  TestSTLToDist(OSMLat, OSMLon, STLToOSMDistSphKM, 'STL to OSM');

  CheckTrue(FloatEquals(LatLongDistanceKM(ATLLat, ATLLon, ATHLat, ATHLon), ATLToATHDistSphKM, 0.001), 'ATL to ATH');
  CheckTrue(FloatEquals(LatLongDistanceKM(ATLLat, ATLLon, CHILat, CHILon), ATLToCHIDistSphKM, 0.001), 'ATL to CHI');
  CheckTrue(FloatEquals(LatLongDistanceKM(ATLLat, ATLLon, DECLat, DECLon), ATLToDECDistSphKM, 0.001), 'ATL to DEC');
end;

procedure TTestDistance.TestVincenty;

  procedure TestSTLToDist(Lat, Lon, Expected: Extended; Msg: string);
  var
    Actual: Extended;
    ExpectedStr: string;
    ActualStr: string;
  begin
    Actual := LatLongDistanceMeters(STLLat, STLLon, Lat, Lon) / MetersPerKM;
    ActualStr := FormatFloat('0.0000', Actual);
    ExpectedStr := FormatFloat('0.0000', Expected);
    CheckEqualsString(ExpectedStr, ActualStr, Msg);
  end;

begin
  CheckTrue(LatLongDistanceMeters(STLLat, STLLon, STLLat, STLLon) = 0, 'STL to STL');
  CheckTrue(LatLongDistanceMeters(CHILat, CHILon, CHILat, CHILon) = 0, 'CHI to CHI');
  TestSTLToDist(CHILat, CHILon, STLToCHIDistWGS84KM, 'STL to CHI');
  TestSTLToDist(ATLLat, ATLLon, STLToATLDistWGS84KM, 'STL to ATL');
  TestSTLToDist(ESLLat, ESLLon, STLToESLDistWGS84KM, 'STL to ESL');
  TestSTLToDist(NYCLat, NYCLon, STLToNYCDistWGS84KM, 'STL to NYC');
  TestSTLToDist(LACLat, LACLon, STLToLACDistWGS84KM, 'STL to LAC');
  TestSTLToDist(OSLLat, OSLLon, STLToOSLDistWGS84KM, 'STL to OSL');
  TestSTLToDist(OSMLat, OSMLon, STLToOSMDistWGS84KM, 'STL to OSM');

  CheckTrue(FloatEquals(LatLongDistanceMeters(ATLLat, ATLLon, ATHLat, ATHLon) / MetersPerKM, ATLToATHDistWGS84KM, 0.001), 'ATL to ATH');
  CheckTrue(FloatEquals(LatLongDistanceMeters(ATLLat, ATLLon, CHILat, CHILon) / MetersPerKM, ATLToCHIDistWGS84KM, 0.001), 'ATL to CHI');
  CheckTrue(FloatEquals(LatLongDistanceMeters(ATLLat, ATLLon, DECLat, DECLon) / MetersPerKM, ATLToDECDistWGS84KM, 0.001), 'ATL to DEC');
end;

initialization
  RegisterTest(TTestDistance.Suite);

end.

