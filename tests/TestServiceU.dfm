object TestService: TTestService
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 248
  Top = 138
  Height = 391
  Width = 549
  object Query: TBetterADODataSet
    Connection = Conn
    Parameters = <>
    IndexDefs = <>
    Left = 56
    Top = 80
  end
  object Conn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=TestDB;Data Source=KYLENOTE'
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 56
    Top = 16
  end
  object Command: TADOCommand
    Connection = Conn
    Parameters = <>
    Left = 56
    Top = 152
  end
  object FindLocate: TBetterADODataSet
    Connection = Conn
    CommandText = 
      'select locate.*'#13#10' from locate inner join ticket on locate.ticket' +
      '_id=ticket.ticket_id'#13#10'where ticket.ticket_number=:tn'#13#10' and locat' +
      'e.client_code=:cc'
    Parameters = <
      item
        Name = 'tn'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'cc'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    IndexDefs = <>
    Left = 160
    Top = 24
  end
  object FindTicket: TBetterADODataSet
    Connection = Conn
    CommandText = 'select ticket_id  from ticket'#13#10'where ticket.ticket_number = :tn'
    Parameters = <
      item
        Name = 'tn'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    IndexDefs = <>
    Left = 160
    Top = 80
  end
  object GetAssignment: TBetterADODataSet
    Connection = Conn
    CommandText = 
      'select assignment.locator_id'#13#10' from locate'#13#10' inner join ticket o' +
      'n locate.ticket_id=ticket.ticket_id'#13#10' inner join assignment on l' +
      'ocate.locate_id=assignment.locate_id'#13#10'where ticket.ticket_number' +
      '=:tn'#13#10' and locate.client_code=:cc'#13#10' and assignment.active=1'
    Parameters = <
      item
        Name = 'tn'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'cc'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    IndexDefs = <>
    Left = 160
    Top = 152
  end
  object DeleteTimesheetsCmd: TADOCommand
    CommandText = 'delete from timesheet_detail; delete from timesheet;'
    Connection = Conn
    Parameters = <>
    Left = 304
    Top = 24
  end
  object CountTimesheetQuery: TADODataSet
    Connection = Conn
    CommandText = 'select count(*) as N from timesheet'
    Parameters = <>
    Left = 304
    Top = 88
  end
end
