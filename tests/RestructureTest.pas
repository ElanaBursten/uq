unit RestructureTest;

// TODO: Try to remove some of the code duplication in the tests
// somehow and add tests for damaged tables

interface

uses TestFramework, DBISAMTb, MSXML2_TLB;

type
  TRestructureTest = class(TTestCase)
  protected
    FTable: TDBISAMTable;
    FDatabase: TDBISAMDatabase;
    procedure CreateSimpleTable;
    procedure SetUp; override;
    procedure TearDown; override;
    function MakeXmlDoc(const XmlText: string): IXMLDOMDocument;
    function GetAndOpenPersonTable: TDBISAMTable;
  published
    procedure TestAddField;
    procedure TestRemoveField;
    procedure TestChangeField;
    procedure TestAddTable;
    procedure TestRemoveTable;
    procedure TestAddIndex;
    procedure TestChangeIndex;
    procedure TestRemoveIndex;
  end;

implementation

uses SysUtils, DB, OdDBISAMXml, OdMSXMLUtils, OdMiscUtils, Dialogs, ClipBrd;

const
  XmlHeader =
    '<?xml version="1.0"?>' +sLineBreak+
    '<ODXMLSchema version="1.0">' +sLineBreak;
  TableHeader =
    '  <table name="person">' +sLineBreak;
  TableFooter =
    '  </table>' +sLineBreak;
  XmlFooter =
    '</ODXMLSchema>';
  // <index name="default" fields="ref_id;ref_name" options="ixCaseInsensitive" />
  // <field name="ref_name" type="ftString" size="15" precision="0" attributes="faReadonly" />
  NameElement   = '    <field name="name"   type="ftString" size="20" attributes="faRequired"/>' +sLineBreak;
  AgeElement    = '    <field name="age"    type="ftInteger" />' +sLineBreak;
  SalaryElement = '    <field name="salary" type="ftFloat" />' +sLineBreak;
  ActiveElement = '    <field name="active" type="ftBoolean" />' +sLineBreak;
  NameIndex     = '    <index name="&lt;Primary&gt;" fields="name" options="ixPrimary,ixUnique" />' +sLineBreak;
  AgeIndex      = '    <index name="age_index" fields="age" options="ixDescending" />' +sLineBreak;

{ TRestructureTest }

procedure TRestructureTest.CreateSimpleTable;
const
  DBNameDir = 'restructure';
  SimpleTableName = 'person';
begin
  if not Assigned(FDatabase) then begin
    FDatabase := TDBISAMDatabase.Create(nil);
    FDatabase.DatabaseName := DBNameDir;
    FDatabase.Directory := DBNameDir;
  end;
  CreateDir(DBNameDir);
  FDatabase.Open;
  FreeAndNil(FTable);
  FTable := TDBISAMTable.Create(nil);
  FTable.DatabaseName := DBNameDir;
  if FDatabase.Handle.DataTableExists(SimpleTableName, False , False) then
    FDatabase.Handle.DeleteDataTable(SimpleTableName);
  if FDatabase.Handle.DataTableExists('new', False, False) then
    FDatabase.Handle.DeleteDataTable('new');
  FTable.TableName := SimpleTableName;
  FTable.FieldDefs.Add('name', ftString, 20, True);
  FTable.FieldDefs.Add('age', ftInteger, 0, False);
  FTable.FieldDefs.Add('salary', ftFloat, 0, False);
  FTable.IndexDefs.Add('<Primary>', 'name', [ixPrimary,ixUnique]);
  FTable.CreateTable;
end;

function TRestructureTest.GetAndOpenPersonTable: TDBISAMTable;
begin
  Result := TDBISAMTable.Create(nil);
  Result.DatabaseName := FDatabase.DatabaseName;
  Result.TableName := 'person';
  Result.Open;
end;

function TRestructureTest.MakeXmlDoc(const XmlText: string): IXMLDOMDocument;
begin
  Result := CoDOMDocument.Create;
  Result.async := False;
  if not Result.loadXML(XmlText) then begin
    Clipboard.AsText := XmlText;
    raise Exception.Create('Bad XML (see clipboard for details)!');
  end;
end;

procedure TRestructureTest.SetUp;
begin
  inherited;
  CreateSimpleTable;
end;

procedure TRestructureTest.TearDown;
begin
  inherited;
  FreeAndNil(FDatabase);
  FreeAndNil(FTable);
end;

procedure TRestructureTest.TestAddField;
var
  Table: TDBISAMTable;
  ActiveField: TField;
begin
  ApplyXMLSchemaToDatabase(FDatabase, MakeXmlDoc(
    XmlHeader +
    TableHeader +
      NameIndex +
      NameElement +
      AgeElement +
      SalaryElement +
      ActiveElement + // The new "active" field we are adding
    TableFooter +
    XMLFooter));
  Table := GetAndOpenPersonTable;
  try
    ActiveField := Table.Fields.FindField('active');
    Check(Assigned(ActiveField));
    Check(ActiveField.DataType = ftBoolean);
    Check(Table.Fields.Count = 4);
  finally
    Table.Free;
  end;
end;

procedure TRestructureTest.TestAddIndex;
var
  Table: TDBISAMTable;
  AgeIndexDef:TDBISAMIndexDef;
begin
  ApplyXMLSchemaToDatabase(FDatabase, MakeXmlDoc(
    XmlHeader +
    TableHeader +
      NameIndex +
      AgeIndex +  // New age index
      NameElement +
      AgeElement +
      SalaryElement +
    TableFooter +
    XMLFooter));
  Table := GetAndOpenPersonTable;
  try
    Table.IndexDefs.Update;
    AgeIndexDef := Table.IndexDefs.Find('age_index');
    Check(Assigned(AgeIndexDef));
    Check(AgeIndexDef.Fields = 'age');
    Check(AgeIndexDef.Options = [ixDescending]);
    Check(Table.Fields.Count = 3);
  finally
    Table.Free;
  end;
end;

procedure TRestructureTest.TestAddTable;
begin
  ApplyXMLSchemaToDatabase(FDatabase, MakeXmlDoc(
    XmlHeader +
    TableHeader +
      NameIndex +
      NameElement +
      AgeElement +
      SalaryElement +
    TableFooter +
      '  <table name="new">' +SLineBreak+
      '    <field name="string_field" type="ftString" size="10" />' +SLineBreak+
    TableFooter +
    XMLFooter));
  Check(FDatabase.Handle.DataTableExists('new', False, False));
end;

procedure TRestructureTest.TestChangeField;
var
  Table: TDBISAMTable;
begin
  ApplyXMLSchemaToDatabase(FDatabase, MakeXmlDoc(
    XmlHeader +
    TableHeader +
      NameIndex +
      // Change the length of the name field from 20 to 40
      StringReplace(NameElement, 'size="20"', 'size="40"', []) +
      AgeElement +
      SalaryElement +
    TableFooter +
    XMLFooter));
  Table := GetAndOpenPersonTable;
  try
    Check(Table.Fields.FindField('name').Size = 40);
    Check(Table.Fields.Count = 3);
  finally
    Table.Free;
  end;
end;

procedure TRestructureTest.TestChangeIndex;
var
  Table: TDBISAMTable;
  IndexXml: string;
  NameIndexDef: TDBISAMIndexDef;
begin
  IndexXml := StringReplace(NameIndex, 'fields="name"', 'fields="name;age"', [rfIgnoreCase]);
  IndexXml := StringReplace(IndexXml, 'ixUnique', 'ixUnique,ixDescending', [rfIgnoreCase]);
  ApplyXMLSchemaToDatabase(FDatabase, MakeXmlDoc(
    XmlHeader +
    TableHeader +
      IndexXml + // Modified index
      NameElement +
      AgeElement +
      SalaryElement +
    TableFooter +
    XMLFooter));
  Table := GetAndOpenPersonTable;
  try
    Table.IndexDefs.Update;
    Check(Table.IndexDefs.Count = 1);
    NameIndexDef := Table.IndexDefs.Find(''); // The primary index is named ''
    Check(Assigned(NameIndexDef));
    Check(NameIndexDef.Fields = 'name;age');
    Check(NameIndexDef.Options = [ixPrimary,ixUnique,ixDescending]);
    Check(Table.Fields.Count = 3);
  finally
    Table.Free;
  end;
end;

procedure TRestructureTest.TestRemoveField;
var
  Table: TDBISAMTable;
  SalaryField: TField;
begin
  ApplyXMLSchemaToDatabase(FDatabase, MakeXmlDoc(
    XmlHeader +
    TableHeader +
      NameIndex +
      NameElement +
      AgeElement + // Removed salary field
    TableFooter +
    XMLFooter));
  Table := GetAndOpenPersonTable;
  try
    SalaryField := Table.Fields.FindField('salary');
    Check(not Assigned(SalaryField));
    Check(Table.Fields.Count = 2);
  finally
    Table.Free;
  end;
end;

procedure TRestructureTest.TestRemoveIndex;
{Note from version 3 to 4 documentation:
http://www.elevatesoft.com/manual?action=viewtopic&id=dbisam4&product=kylix&version=3&topic=Changes
"In version 3.x and earlier you could have a table without a primary index. In version 4, if you do
 not define a primary index when creating or restructuring a table, DBISAM will automatically add a
  primary index on the system RecordID field mentioned above."}
var
  Table: TDBISAMTable;
begin
  TestAddIndex;
  ApplyXMLSchemaToDatabase(FDatabase, MakeXmlDoc(
    XmlHeader +
    TableHeader + // No indexes are present
      NameElement +
      AgeElement +
      SalaryElement +
    TableFooter +
    XMLFooter));
  Table := GetAndOpenPersonTable;
  try
    Table.IndexDefs.Update;
    //Primary index will always be present due to Auto Primary Index feature introduced in DBISAM 4, see above reference.
    Check(Table.IndexDefs.Count = 1);
    Check(Table.Fields.Count = 3);
  finally
    Table.Free;
  end;
end;

procedure TRestructureTest.TestRemoveTable;
begin
  ApplyXMLSchemaToDatabase(FDatabase, MakeXmlDoc(XmlHeader + XMLFooter));
  Check(not FDatabase.Handle.DataTableExists('person', False, False));
end;

initialization
  TestFramework.RegisterTest(TRestructureTest.Suite);

end.
