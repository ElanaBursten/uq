program TestQMAPI;

uses
  Vcl.Forms,
  JsonTest in 'JsonTest.pas' {Form4},
  JsonTestDMu in 'JsonTestDMu.pas' {TestJsonDM: TDataModule},
  SqlJsonOutput in '..\common\SqlJsonOutput.pas',
  AdUtils in '..\common\AdUtils.pas',
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  OdIsoDates in '..\common\OdIsoDates.pas',
  UQDbConfig in '..\common\UQDbConfig.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TTestJsonDM, TestJsonDM);
  Application.Run;
end.
