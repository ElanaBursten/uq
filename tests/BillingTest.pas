unit BillingTest;

interface

uses
  Classes, TestFrameWork, SysUtils, DB, SyncEngineU, TestConfig, SingleEngineTest,
  Contnrs, BillingEngine, BillingLocate, BillingTicketClientList;

type
  TRatingTest = class(TTestCase)
  private
    FLastBucket: string;
    BR: TLocate;
    TheCallCenter: string;
    procedure CheckRating(Expected: single; Status, ClientCode: string;
       Qty: Integer = 1; State: string=''; County: string=''; City: string='';
       TicketID: Integer = 0; TransmitDate: string=''; TicketType: string='';
       ClosedDate: string='');
    procedure CheckAfterHours(Expected: Boolean; DateString: string);
    procedure CheckPriceEquals(Price: Currency; Locate: TLocate);
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestSimple;
    procedure TestWithMappedCC;
    procedure TestClientMap;
    procedure TestRunBilling;
    procedure TestQty0;
    procedure TestQtyMore;
    procedure TestTwoParty;
    procedure TestAutoRoute;
    procedure TestEmergency;
    procedure TestDateRange;
    procedure TestAfterHoursRange;
    procedure TestDaytimeEmergency;
    procedure TestSjgNcOs;
    procedure TestUqNoNoCharge;
    procedure TestAe1PerLocate;
    procedure TestCciNcN;
    procedure TestAtlantaOS;
    procedure TestRxmtNC;
    procedure TestNoCharge;
    // This must be the last test
    procedure TestFreeDM;
  end;

  TBillingStorageTest = class(TTestCase)
  private
  protected
  published
    procedure TestLoadFromFile;
  end;

  TTicketClientListTest = class(TTestCase)
  private
    TCL: TTicketClientList;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestClients;
    procedure TestCount;
  end;

implementation

uses
  OdIsoDates, BillingEngineDMu, BillingConst, OdMiscUtils, QMConst;

var
  BE: TBillingEngine;

const
  NJLocatorID = 270;
  SWrongRate = 'Wrong rate';
  SWrongPrice = 'Wrong price';

{ TRatingTest }

procedure TRatingTest.CheckRating(Expected: single; Status,
  ClientCode: string; Qty: Integer = 1; State: string=''; County: string='';
   City: string=''; TicketID: Integer = 0; TransmitDate: string='';
   TicketType: string=''; ClosedDate: string='');
begin
  FreeAndNil(BR);
  BR := TLocate.Create;
  BR.Status := Status;
  BR.Closed := True;
  BR.ClientCode := ClientCode;
  BR.CallCenter := TheCallCenter;
  BR.WorkState := State;
  BR.WorkCounty := County;
  BR.WorkCity := City;
  BR.QtyMarked := Qty;
  BR.TicketID := TicketID;
  if TransmitDate = '' then
    BR.TransmitDate := IsoStrToDate('2002-03-15')
  else
    BR.TransmitDate := IsoStrToDateTimeDef(TransmitDate);

  if ClosedDate = '' then
    BR.ClosedDate := IsoStrToDateTime('2002-03-19T13:00:00')
  else
    BR.ClosedDate := IsoStrToDateTimeDef(ClosedDate);

  BR.TicketType := TicketType;

//  BE.Rate(BR);
  FLastBucket := BR.Bucket;
  CheckPriceEquals(Expected, BR);
end;

procedure TRatingTest.SetUp;
begin
  inherited;
  if not Assigned(BE) then begin
    BE := TBillingEngine.Create;
//    BE.TestMode := True;
//    BE.ConnectUsingIniParameters(GetTestIniFileName);
    //BE.SetCallCenters(CallCenterNewJersey + sLineBreak + 'FCL1' + sLineBreak + 'Atlanta');
//    BE.LoadData;
    BE.SetBillingTimeout(200);
  end;
  BE.EndingDate := IsoStrToDate('2002-03-20');
  TheCallCenter := CallCenterNewJersey;
end;

procedure TRatingTest.TearDown;
begin
  FreeAndNil(BR);
  inherited;
end;

procedure TRatingTest.TestClientMap;
begin

  CheckEquals('NJNB', BE.MapClientCode('NJ', 'MONMOUTH', 'ABERDEEN', 'NJN', CallCenterNewJersey));
  CheckEquals('NJNN', BE.MapClientCode('NJ', 'MORRIS', 'LEDGEWOOD', 'NJN', CallCenterNewJersey));
  CheckEquals('NJNC', BE.MapClientCode('NJ', 'MONMOUTH', 'WEST LONG BRANCH', 'NJN', CallCenterNewJersey));

  // don't mess with non AE1s
  CheckEquals('AE1', BE.MapClientCode('NJ', 'MONMOUTH', 'WEST LONG BRANCH', 'AE1', CallCenterNewJersey));
end;

procedure TRatingTest.TestQty0;
begin
  CheckRating(14.29, 'C', 'EG1', 0);   // 0 rates as 1.
end;

procedure TRatingTest.TestRunBilling;
var
  BillID: Integer;
  DM: TBillingEngineDM;
begin
  BE.EndingDate := IsoStrToDateTime('2002-04-11T08:30:00'));
  BillID := BE.RunBillingFromFile('test billing 1', 'sample_rawdata.txt');

  DM := ReachInAndGetDM(BE);
  DM.CheckHeader.Parameters[0].Value := BillID;
  DM.CheckHeader.Open;
  CheckEquals(1, DM.CheckHeader.RecordCount);

  DM.CheckDetail.Parameters[0].Value := BillID;
  DM.CheckDetail.Open;
  Check(DM.CheckDetail.RecordCount > 20);
end;

procedure TRatingTest.TestSimple;
begin
  CheckRating(14.29, 'C', 'EG1');
  CheckRating(14.29, 'C', 'EG3');
  CheckRating(12.73, 'C', 'SJG');
  CheckRating(8.46, 'C', 'AE1');
  CheckRating(8.46, 'M', 'AE1');
  CheckRating(13.80, 'M', 'UW8');
  CheckRating(13.80, 'M', 'UW4');
  CheckRating(12, 'M', 'CCI');
  CheckEquals('Normal Notice', FLastBucket);

  CheckRating(0, 'OS', 'AE1');
end;

procedure TRatingTest.TestWithMappedCC;
begin
  CheckRating(12.20, 'C', 'NJN', 1, 'NJ', 'MORRIS', 'LEDGEWOOD');
  CheckEquals('Normal Notice, One Party', FLastBucket);
end;

procedure TRatingTest.TestQtyMore;
begin
  CheckRating(14.29    , 'C', 'EG1', 1);
  CheckRating(14.29 * 2, 'C', 'EG1', 2);
  CheckRating(14.29 * 3, 'C', 'EG1', 3);
end;

procedure TRatingTest.TestTwoParty;
begin
  // populate with some data
  { The only client that has a different rate if multiple party
    charges (called subsequant locates) is NJN*.  If AE1 appears on
    the ticket with any NJN* client, we charge NJN 8.86 per unit.
    No other two party rate applies, depite the table that was sent.
  }

  CheckRating(12.20, 'C', 'NJN', 1, 'NJ', 'MORRIS', 'LEDGEWOOD', 1234);

  // add something bogus
  BE.TicketClientList.Add(1234, NJLocatorID, 'XYZ', 'C');
  CheckRating(12.20, 'C', 'NJN', 1, 'NJ', 'MORRIS', 'LEDGEWOOD', 1234);
  CheckEquals('Normal Notice, One Party', FLastBucket);

  // add AE1 on that one
  BE.TicketClientList.Add(1234, NJLocatorID, 'AE1', 'C');
  CheckRating(11.40, 'C', 'NJN', 1, 'NJ', 'MORRIS', 'LEDGEWOOD', 1234);
  CheckEquals('Normal Notice, Two Party', FLastBucket);
end;

procedure TRatingTest.TestAutoRoute;
const
  AutoRouteID = 285;
var
  BR: TLocate;
begin
  BR := TLocate.Create;
  BR.Closed := True;
  BR.CallCenter := CallCenterNewJersey;
  BR.WorkState := '';
  BR.WorkCounty := '';
  BR.WorkCity := '';
  BR.QtyMarked := 1;
  BR.TicketID := 1234;
  BR.TransmitDate := IsoStrToDate('2002-03-15');
  BR.ClosedDate := IsoStrToDateTime('2002-03-19T13:00:00');
  BR.LocatorID := AutoRouteID;

  BR.ClientCode := 'EG1';
  BR.Status := 'NC';
  BE.Rate(BR);
  CheckPriceEquals(4.08, BR);
  CheckEquals('OS', BR.Status, 'Status changed to OS for auto route NJ');
  CheckEquals('Office Screen', BR.Bucket);

  BR.ClientCode := 'EG3';
  BR.Status := 'NC';
  BE.Rate(BR);
  CheckPriceEquals(4.08, BR);
  CheckEquals('OS', BR.Status, 'Status changed to OS for auto route NJ');
  CheckEquals('Office Screen', BR.Bucket);

  BR.ClientCode := 'NJN';
  BR.Status := 'NC';
  BE.Rate(BR);
  CheckPriceEquals(4.25, BR);
  CheckEquals('OS', BR.Status, 'Status changed to OS for auto route NJ');
  Check(BR.Bucket = 'Office Screen');

  BR.ClientCode := 'NJN';
  BR.Status := 'OS';
  BE.Rate(BR);
  CheckPriceEquals(4.25, BR);
  CheckEquals('OS', BR.Status, 'NJN OS');
  Check(BR.Bucket = 'Office Screen');

  // now, even if it has an AE1 on it:
  BE.TicketClientList.Add(1234, NJLocatorID, 'AE1', 'OS');

  BR.ClientCode := 'NJN';
  BR.Status := 'OS';
  BR.TicketType := 'EMERGENCY';
  BE.Rate(BR);
  CheckPriceEquals(4.25, BR);
  CheckEquals('OS', BR.Status, 'NJN OS is 0');
  Check(BR.Bucket = 'Office Screen');

  BR.Free;
end;

procedure TRatingTest.TestEmergency;
begin
  // Emergency, but during the day:
  CheckRating(12, 'M', 'CCI', 1, '', '', '', 0, '2002-04-11T08:30:00', 'EMERGENCY');

  // Early morning, but Routine
  CheckRating(12, 'M', 'CCI', 1, '', '', '', 0, '2002-04-11T03:15:00', 'ROUTINE');
  CheckEquals('Normal Notice', FLastBucket);

  // Late, but Routine
  CheckRating(12, 'M', 'CCI', 1, '', '', '', 0, '2002-04-11T22:15:00', 'ROUTINE');
  CheckEquals('Normal Notice', FLastBucket);

  // CCI no longer has special emergency rates
  // Must be after 4PM or before 8AM, and (Emergency OR Update)
  CheckRating(12, 'M', 'CCI', 1, '', '', '', 0, '2002-04-11T02:15:00', 'EMERGENCY');
  CheckEquals('After Hours Notice', FLastBucket);
  CheckRating(12, 'M', 'CCI', 1, '', '', '', 0, '2002-04-11T20:15:00', 'UPDATE');
  CheckEquals('After Hours Notice', FLastBucket);

  // Make sure qty also works
  CheckRating(24, 'M', 'CCI', 2, '', '', '', 0, '2002-04-11T20:15:00', 'UPDATE');
end;

procedure TRatingTest.TestDateRange;
begin
  // pick an ugly date range:
  BE.EndingDate := IsoStrToDateTime('2002-01-09T00:00:00');
  try
    CheckRating(12, 'M', 'CCI', 1, '', '', '', 0, '', 'EMERGENCY', '2002-04-11T08:30:00');
    Fail('We should not have been able to rate a locate outside the date range');
  except
    on E: EBillingException do
      Check(True);
  end;
end;

procedure TRatingTest.TestAfterHoursRange;
begin
  CheckAfterHours(False, '2002-04-11T08:30:00');
  CheckAfterHours(True,  '2002-04-11T18:30:00');
  CheckAfterHours(True,  '2002-04-11T02:15:00');
end;

procedure TRatingTest.CheckAfterHours(Expected: Boolean; DateString: string);
begin
  //CheckEquals(Expected, BE.IsAfterHours(IsoStrToDateTime(DateString), CallCenterNewJersey));
end;

procedure TRatingTest.TestDaytimeEmergency;
begin
  // Non-emergency
  CheckRating(13.80, 'M', 'UW8');

  // There are no longer special emergency eates for the UW terms
  // Emergencies for several UW terms
  CheckRating(13.80, 'M', 'UW8', 1, '', '', '', 0, '2002-04-11T08:30:00', 'EMERGENCY');
  CheckRating(13.80, 'M', 'UW4', 1, '', '', '', 0, '2002-04-11T08:30:00', 'EMERGENCY');
  CheckRating(13.80, 'M', 'UW9', 1, '', '', '', 0, '2002-04-11T08:30:00', 'EMERGENCY');
  CheckRating(13.80, 'M', 'UW5', 1, '', '', '', 0, '2002-04-11T08:30:00', 'EMERGENCY');
  CheckEquals('Emergency Notice', FLastBucket);
end;

procedure TRatingTest.TestSjgNcOs;
begin
  CheckRating(3.59, 'NC', 'SJG');   // charge 3.50 and make it an OS
  CheckEquals('OS', BR.Status);
end;

procedure TRatingTest.TestUqNoNoCharge;
begin
  CheckRating(13.80, 'N', 'UW8');   // normal charge for N

  // These are no longer billed as N status for AE1
  CheckRating(0, 'NC', 'UW8');   // charge NC and make it an N
  CheckEquals('NC', BR.Status);
end;

procedure TRatingTest.TestAe1PerLocate;
begin
  CheckRating(8.46, 'M', 'AE1', 1);
  CheckRating(8.46, 'M', 'AE1', 2);  // 2 is charged the same as 1 for AE1
end;

procedure TRatingTest.TestCciNcN;
begin
  CheckRating(12, 'NC', 'CCI');   // charge and make it an N
  CheckEquals('N', BR.Status);
end;

procedure TRatingTest.TestAtlantaOS;
begin
  TheCallCenter := 'Atlanta';

  CheckRating(0, 'NC', 'ATL01', 1);
  CheckEquals('No Charge, No Conflict', FLastBucket);

  CheckRating(8.50, 'M', 'ATL01', 1);
  CheckEquals('Normal Notice, Marked, Marked', FLastBucket);

  CheckRating(3.75, 'NP', 'ATL01', 1);
  CheckEquals('Normal Notice, NP, Screened', FLastBucket);

  CheckRating(3.75, 'N', 'ATL01', 1);
  CheckEquals('Normal Notice, New, Screened', FLastBucket);


  CheckRating(0, 'NC', 'ATL01', 1, '', '', '', 0, '2002-04-11T08:30:00', 'EMERG');
  CheckEquals('After Hours Notice, No Charge, No Conflict', FLastBucket);

  CheckRating(8.50, 'M', 'ATL01', 1, '', '', '', 0, '2002-04-11T08:30:00', 'EMERG RESEND');
  CheckEquals('After Hours Notice, Marked', FLastBucket);

  CheckRating(3.75, 'N', 'ATL01', 1, '', '', '', 0, '2002-04-11T08:30:00', 'EMERG ETC.');
  CheckEquals('After Hours Notice, New, Screened', FLastBucket);
end;

procedure TRatingTest.TestRxmtNC;
begin
  TheCallCenter := 'FCL1';
  CheckRating(5.15, 'M', 'VCN01', 1, '', '', '', 0, '2002-06-11T08:30:00', 'NORMAL');
  CheckRating(0, 'M', 'VCN01', 1, '', '', '', 0, '2002-06-10T08:30:00', 'NORMAL RXMT', '2002-03-14T08:30:00');
  CheckRating(0, 'M', 'VCN01', 1, '', '', '', 0, '2002-05-23T08:30:00', 'NORMAL RXMT', '2002-03-16T08:30:00');
end;

procedure TRatingTest.TestNoCharge;
begin
  TheCallCenter := 'FCL1';
  CheckRating(0, 'NC', 'VCN01', 1, '', '', '', 0, '2002-06-11T08:30:00', 'NORMAL');
end;

procedure TRatingTest.TestFreeDM;
begin
  FreeAndNil(BE);
end;

procedure TRatingTest.CheckPriceEquals(Price: Currency; Locate: TLocate);
begin
  Check(FloatEquals(Price, Locate.Price), 'Bad price');
end;

{ TBillingStorageTest }

procedure TBillingStorageTest.TestLoadFromFile;
var
  Engine: TBillingEngine;

  function Locate(n: Integer): TLocate;
  begin
    Result := TLocate(Engine.Locates[n]);
  end;

begin
  Engine := TBillingEngine.Create;
  try
    Engine.TestMode := True;
    Engine.LoadUsageFromFile('sample_rawdata.txt');

    CheckEquals(100, Engine.Locates.Count);

    CheckEquals(136161, Locate(0).LocateId);
    CheckEquals(18711, Locate(1).TicketID);
    CheckEquals(True, Locate(2).Closed);
    CheckEquals(StrToDateTime('4/8/2002 14:09:56'), Locate(3).ClosedDate, 'dates');
    CheckEquals(CallCenterNewJersey, Locate(7).CallCenter);
  finally
    Engine.Free;
  end;
end;

{ TTicketClientListTest }

procedure TTicketClientListTest.SetUp;
begin
  inherited;
  TCL := TTicketClientList.Create(nil);
end;

procedure TTicketClientListTest.TearDown;
begin
  inherited;
  FreeAndNil(TCL);
end;

procedure TTicketClientListTest.TestClients;
begin
  TCL.Add(100, NJLocatorID, 'A', 'M');
  TCL.Add(100, NJLocatorID, 'B', 'M');
  TCL.Add(100, NJLocatorID, 'B', 'M');
  Check(TCL.Has(100, 'A'));
  Check(TCL.Has(100, 'B'));
  Check(not TCL.Has(100, 'F'));
end;

procedure TTicketClientListTest.TestCount;
begin
  TCL.Add(100, NJLocatorID, 'A', 'M');
  TCL.Add(100, NJLocatorID, 'B', 'M');
  TCL.Add(100, NJLocatorID, 'C', 'M');
  TCL.Add(200, NJLocatorID, 'C', 'M');
  CheckEquals(3, TCL.CountLocatesOnTicket(100));
  CheckEquals(1, TCL.CountLocatesOnTicket(200));
end;

initialization
  TestFramework.RegisterTest(TRatingTest.Suite);
  TestFramework.RegisterTest(TBillingStorageTest.Suite);
  TestFramework.RegisterTest(TTicketClientListTest.Suite);

end.

