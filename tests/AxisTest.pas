unit AxisTest;

interface

uses
  TestFrameWork, Classes, SysUtils, GridAxis;

type
  TAlphaAxisTest = class(TTestCase)
  public
    Axis: TAlphaAxis;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestAlphaAxisAZ;
    procedure TestAlphaAxisA_AA;
    procedure TestBAtoInt;
    procedure TestIntToBA;
  end;

  TQtrAxisTest = class(TTestCase)
  public
    Axis: TQtrAxis;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure Test1;
    procedure TestAtZero;
    procedure TestCommonValues;
    procedure TestIntToQtr;
    procedure TestBiDi;
  private
    procedure RoundTrip(const S: string);
  end;

implementation

{ TAlphaAxisTest }

procedure TAlphaAxisTest.SetUp;
begin
  inherited;
end;

procedure TAlphaAxisTest.TearDown;
begin
  inherited;
  FreeAndNil(Axis);
end;

procedure TAlphaAxisTest.TestAlphaAxisAZ;
begin
  Axis := TAlphaAxis.Create('A', 'D');
  CheckEquals(4, Axis.Size);
  CheckEquals(1, Axis.CodeToIndex('A'));
  CheckEquals(4, Axis.CodeToIndex('D'));
end;

procedure TAlphaAxisTest.TestAlphaAxisA_AA;
begin
  Axis := TAlphaAxis.Create('A', 'AA');
  CheckEquals(27, Axis.Size, 'size');
  CheckEquals(1, Axis.CodeToIndex('A'));
  CheckEquals(27, Axis.CodeToIndex('AA'));
end;

procedure TAlphaAxisTest.TestBAtoInt;
begin
  Axis := TAlphaAxis.Create('A', 'A');
  CheckEquals(1, Axis.LettersToInt('A'));
  CheckEquals(26, Axis.LettersToInt('Z'));
  CheckEquals(27, Axis.LettersToInt('AA'));
  CheckEquals(53, Axis.LettersToInt('BA'));
end;

procedure TAlphaAxisTest.TestIntToBA;
begin
  Axis := TAlphaAxis.Create('A', 'A');
  CheckEquals('A', Axis.IntToLetters(1));
  CheckEquals('Z', Axis.IntToLetters(26));
  CheckEquals('AA', Axis.IntToLetters(27));
  CheckEquals('BA', Axis.IntToLetters(53));
end;

{ TQtrAxisTest }

procedure TQtrAxisTest.SetUp;
begin
  inherited;

end;

procedure TQtrAxisTest.TearDown;
begin
  inherited;
  FreeAndNil(Axis);
end;

procedure TQtrAxisTest.Test1;
begin
  Axis := TQtrAxis.CreateSize('9025D', 15);
  CheckEquals(1, Axis.CodeToIndex('9025D'));
  CheckEquals(2, Axis.CodeToIndex('9024A'));
  CheckEquals(3, Axis.CodeToIndex('9024B'));
  CheckEquals(4, Axis.CodeToIndex('9024C'));
  CheckEquals(5, Axis.CodeToIndex('9024D'));
  CheckEquals(6, Axis.CodeToIndex('9023A'));
end;

procedure TQtrAxisTest.TestAtZero;
begin
  CheckEquals(0, TQtrAxis.QtrToInt('18000A'));
  CheckEquals(1, TQtrAxis.QtrToInt('18000B'));
  CheckEquals(2, TQtrAxis.QtrToInt('18000C'));
  CheckEquals(3, TQtrAxis.QtrToInt('18000D'));
  CheckEquals(4, TQtrAxis.QtrToInt('17959A'));
end;

procedure TQtrAxisTest.TestIntToQtr;
begin
  RoundTrip('9025B');
  RoundTrip('10200C');
  RoundTrip('3259A');
  RoundTrip('1402B');
end;

procedure TQtrAxisTest.TestBiDi;
var
  i: Integer;
  MaxVal: Integer;
  Sample: Integer;
begin
  MaxVal := TQtrAxis.QtrToInt('3200C');
  for i := 1 to 500 do begin
    Sample := Random(MaxVal);
    CheckEquals( Sample, TQtrAxis.QtrToInt(TQtrAxis.IntToQtr(Sample)));
  end;

end;

procedure TQtrAxisTest.TestCommonValues;
var
  x1, x2: Integer;
begin
  x1 := TQtrAxis.QtrToInt('9025A');
  x2 := TQtrAxis.QtrToInt('8925A');
  CheckEquals(60*4, x2 - x1);
end;

procedure TQtrAxisTest.RoundTrip(const S: string);
var
  x: Integer;
begin
  x := TQtrAxis.QtrToInt(s);
  CheckEquals(S,TQtrAxis.IntToQtr(x));
end;

initialization
  TestFramework.RegisterTest(TAlphaAxisTest.Suite);
  TestFramework.RegisterTest(TQtrAxisTest.Suite);

end.

