unit BusinessLogicTest;

interface

uses
  Classes, TestFrameWork, SingleEngineTest, SysUtils, DB, BaseDMTest;

type
  TBusinessLogicTest = class(TBaseDMTest)
  published
    procedure TestSetupData;
    procedure TestLocateModification;
    procedure TestLocateChangeDate;
  end;

implementation

uses DMu;

{ TBusinessLogicTest }

procedure TBusinessLogicTest.TestLocateModification;
var
  LocID: array [0..3] of Integer;
  LocStatus: array [0..1] of string;
  TicketID: Integer;
begin
  TestSetupData;

  TicketID := TestService.FindTicketID('2249');
  //Engine.GetTicketDataFromServer(TicketID, False);
  DMod.GoToTicket(TicketID);
  Check(DMod.LocateData.RecordCount = 4);

  // Modify the first two locates for this ticket
  DMod.LocateData.First;
  LocID[0] := DMod.LocateData.FieldByName('locate_id').AsInteger;
  LocStatus[0] := DMod.LocateData.FieldByName('status').AsString;
  Check(DMod.IsLocateEditable(LocID[0]));
  DMod.LocateData.Edit;
  DMod.LocateData.FieldByName('status').AsString := 'O';
  Check(DMod.IsLocateEditable(LocID[0]));
  DMod.LocateData.Post;
  DMod.LocateData.Next;
  DMod.LocateData.Edit;
  Check(DMod.IsLocateEditable(LocID[0]));
  LocID[1] := DMod.LocateData.FieldByName('locate_id').AsInteger;
  LocStatus[1] := DMod.LocateData.FieldByName('status').AsString;
  Check(DMod.IsLocateEditable(LocID[1]));
  DMod.LocateData.FieldByName('status').AsString := 'M';
  DMod.LocateData.Post;
  DMod.LocateData.Next;
  LocID[2] := DMod.LocateData.FieldByName('locate_id').AsInteger;
  DMod.LocateData.Next;
  LocID[3] := DMod.LocateData.FieldByName('locate_id').AsInteger;
  DMod.Locate.Open;
  Check(DMod.IsLocateEditable(LocID[1]));

  // Make sure no edits were applied
  DMod.Locate.SetRange([LocID[0]], [LocID[0]]);
  Check(DMod.Locate.FieldByName('status').AsString = LocStatus[0]);
  DMod.Locate.SetRange([LocID[1]], [LocID[1]]);
  Check(DMod.Locate.FieldByName('status').AsString = LocStatus[1]);
  Check(DMod.Locate.FieldByName('closed').AsBoolean = False);

  // Save the changes
  DMod.SaveTicket('Detail', False);

  // Make sure things were applied to only the first two locates
  DMod.Locate.Open;
  DMod.Locate.SetRange([LocID[0]], [LocID[0]]);
  Check(DMod.Locate.FieldByName('status').AsString = 'O');
  Check(DMod.Locate.FieldByName('closed').AsBoolean = False);
  DMod.Locate.SetRange([LocID[1]], [LocID[1]]);
  Check(DMod.Locate.FieldByName('status').AsString = 'M');
  Check(DMod.Locate.FieldByName('closed').AsBoolean);
  Check(DMod.Locate.FieldByName('qty_marked').AsFloat > 0);
  DMod.Locate.SetRange([LocID[2]], [LocID[2]]);
  Check(DMod.Locate.FieldByName('DeltaStatus').IsNull);
  DMod.Locate.SetRange([LocID[3]], [LocID[3]]);
  Check(DMod.Locate.FieldByName('DeltaStatus').IsNull);
  DMod.Locate.Close;
end;

procedure TBusinessLogicTest.TestSetupData;
begin
  SetupServerTables;
  DMod.SyncNow;
end;

// Test the trigger that sets the locaet.modified_date
procedure TBusinessLogicTest.TestLocateChangeDate;
var
  TicketID: Integer;
  LocateID: Integer;
  LastModified: TDateTime;

  function GetModifiedDate: TDateTime;
  begin
    Result := TestService.RunSqlReturnNAsDateTime(
      'select modified_date from locate where locate_id = ' + IntToStr(LocateID));
  end;

begin
  TestSetupData;

  TicketID := TestService.FindTicketID('2249');
  //Engine.GetTicketDataFromServer(TicketID, True);
  DMod.Locate.Open;
  Assert(DMod.Locate.Locate('ticket_id', TicketID, []));
  LocateId := DMod.Locate.FieldByName('locate_id').AsInteger;
  LastModified := GetModifiedDate;
  TestService.RunSql('update locate set status = ''NC'' where locate_id = ' + IntToStr(LocateID));
  Check(LastModified < GetModifiedDate);
  LastModified := GetModifiedDate;
  TestService.RunSql('update locate set status = ''-R'', invoiced=1 where locate_id = ' + IntToStr(LocateID));
  Check(LastModified < GetModifiedDate);
  // The modified_date should not change when only invoiced is edited
  LastModified := GetModifiedDate;
  TestService.RunSql('update locate set invoiced = 0 where locate_id = ' + IntToStr(LocateID));
  Check(LastModified = GetModifiedDate);
end;

initialization
  TestFramework.RegisterTest(TBusinessLogicTest.Suite);

end.

