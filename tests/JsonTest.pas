unit JsonTest;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm4 = class(TForm)
    Button1: TButton;
    TicketIDEdit: TEdit;
    Label1: TLabel;
    JsonMemo: TMemo;
    procedure Button1Click(Sender: TObject);
  end;

var
  Form4: TForm4;

implementation

uses JsonTestDMu, SqlJsonOutput, OdMiscUtils;

{$R *.dfm}

procedure TForm4.Button1Click(Sender: TObject);
const
  SkipFields: array[0..0] of string = ('tname');
begin
  TestJsonDM.SP.ParamByName('@TicketID').AsInteger := StrToInt(TicketIDEdit.Text);
  JsonMemo.Text := GetJsonFromDataSet(TestJsonDM.SP, SkipFields).AsJSon(True, False);
end;

end.
