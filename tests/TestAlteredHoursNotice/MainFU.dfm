object MainTestForm: TMainTestForm
  Left = 0
  Top = 0
  Caption = 'Test Time Altered HTML Notice'
  ClientHeight = 527
  ClientWidth = 647
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 128
    Top = 8
    Width = 88
    Height = 13
    Caption = 'Week Ending Date'
  end
  object WorkerIDEdit: TLabeledEdit
    Left = 17
    Top = 24
    Width = 96
    Height = 21
    EditLabel.Width = 72
    EditLabel.Height = 13
    EditLabel.Caption = 'Worker Emp ID'
    TabOrder = 0
  end
  object MakeNoticeButton: TButton
    Left = 260
    Top = 22
    Width = 157
    Height = 25
    Caption = 'Make Notice'
    TabOrder = 2
    OnClick = MakeNoticeButtonClick
  end
  object PageControl: TPageControl
    Left = 0
    Top = 64
    Width = 647
    Height = 463
    ActivePage = HTMLTab
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 3
    object HTMLTab: TTabSheet
      Caption = 'HTML Message'
      object WebBrowser: TWebBrowser
        Left = 0
        Top = 0
        Width = 639
        Height = 435
        Align = alClient
        TabOrder = 0
        ControlData = {
          4C0000000B420000F52C00000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
    object RawDataTab: TTabSheet
      Caption = 'Raw SP Data'
      ImageIndex = 1
      object Splitter1: TSplitter
        Left = 0
        Top = 158
        Width = 639
        Height = 9
        Cursor = crVSplit
        Align = alTop
      end
      object ChangesGrid: TDBGrid
        Left = 0
        Top = 0
        Width = 639
        Height = 158
        Align = alTop
        DataSource = ChangesSource
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object OriginalGrid: TDBGrid
        Left = 0
        Top = 167
        Width = 639
        Height = 268
        Align = alClient
        DataSource = OriginalSource
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
  end
  object WeekEndingDate: TDateTimePicker
    Left = 128
    Top = 24
    Width = 113
    Height = 21
    Date = 39908.442792083330000000
    Time = 39908.442792083330000000
    TabOrder = 1
  end
  object TSEAuditProc: TADOStoredProc
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=canucanoe;Persist Security Info=Tru' +
      'e;User ID=sa;Initial Catalog=TestDB;Data Source=CHINOOK'
    ProcedureName = 'get_tse_audit;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@WorkStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@WorkEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@SheetsForID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 464
    Top = 8
  end
  object OriginalTime: TADODataSet
    CommandText = 'select * from timesheet_entry where 0=1'
    Parameters = <>
    Left = 504
    Top = 8
  end
  object ChangesSource: TDataSource
    DataSet = TSEAuditProc
    Left = 424
    Top = 248
  end
  object OriginalSource: TDataSource
    DataSet = OriginalTime
    Left = 424
    Top = 360
  end
  object ChangeSummary: TADODataSet
    CommandText = 'select * from timesheet_entry where 0=1'
    Parameters = <>
    Left = 504
    Top = 40
  end
end
