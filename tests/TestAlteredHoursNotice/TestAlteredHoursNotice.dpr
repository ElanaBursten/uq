program TestAlteredHoursNotice;

uses
  Forms,
  MainFU in 'MainFU.pas' {MainTestForm},
  WeeklyTimesheet in '..\..\server\WeeklyTimesheet.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainTestForm, MainTestForm);
  Application.Run;
end.
