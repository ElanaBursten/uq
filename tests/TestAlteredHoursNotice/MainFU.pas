unit MainFU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ADODB, DB, ExtCtrls, OleCtrls, SHDocVw,
  Grids, DBGrids, ComCtrls;

type
  TMainTestForm = class(TForm)
    WorkerIDEdit: TLabeledEdit;
    TSEAuditProc: TADOStoredProc;
    OriginalTime: TADODataSet;
    MakeNoticeButton: TButton;
    PageControl: TPageControl;
    HTMLTab: TTabSheet;
    RawDataTab: TTabSheet;
    WebBrowser: TWebBrowser;
    ChangesGrid: TDBGrid;
    OriginalGrid: TDBGrid;
    Splitter1: TSplitter;
    ChangesSource: TDataSource;
    OriginalSource: TDataSource;
    Label1: TLabel;
    WeekEndingDate: TDateTimePicker;
    ChangeSummary: TADODataSet;
    procedure MakeNoticeButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure LoadHTML(Browser: TWebBrowser; const Content: string);
  end;

var
  MainTestForm: TMainTestForm;

implementation

{$R *.dfm}

uses DateUtils, OdAdoUtils, OdMiscUtils, WeeklyTimesheet;

procedure TMainTestForm.MakeNoticeButtonClick(Sender: TObject);
var
  HTML: string;
  Sheets: TWeeklyTimesheet;
  EndDate: TDateTime;
  BeginDate: TDateTime;
begin
  EndDate := Trunc(WeekEndingDate.Date);
  BeginDate := EndDate - 6;
  TSEAuditProc.Close;
  TSEAuditProc.Parameters.ParamByName('@SheetsForID').Value := StrToInt(WorkerIDEdit.Text);
  TSEAuditProc.Parameters.ParamByName('@WorkStart').Value := BeginDate;
  TSEAuditProc.Parameters.ParamByName('@WorkEnd').Value := EndDate;
  TSEAuditProc.Open;
  LoadNextRecordSet(TSEAuditProc, OriginalTime);
  LoadNextRecordSet(TSEAuditProc, ChangeSummary);

  Sheets := TWeeklyTimesheet.CreateFromDataSets(EndDate, TSEAuditProc, OriginalTime, ChangeSummary);
  try
    HTML := Sheets.GetTimeChangesAsHTML;
  finally
    FreeAndNil(Sheets);
  end;

  if IsEmpty(HTML) then
    HTML := '<p><b>No altered hours detected, so no notice is needed.</b></p>';
  LoadHTML(WebBrowser, HTML);
end;

procedure TMainTestForm.LoadHTML(Browser: TWebBrowser; const Content: string);
var
  V, vDocument, vMIMEType, vHTML: OleVariant;
begin
  Browser.Stop;                    // Stop any actions - this is important!
  V := Browser.document;           // Grab the document
  vDocument := V.script.document;  // Now get the script's document (like a JavaScript document)
  vMIMEType := 'text/html';        // IE 4.0 only deals with text/html on a consistent basis
  vHTML := Content;                // You can use any html content you want here
  vDocument.Open(vMIMEType);       // Re-open the page for writing
  vDocument.Clear;                 // Clear the existing content (or not)
  vDocument.Write(vHTML);          // Write our HTML
  vDocument.Close;                 // Closing the document sends the page to the browser
end;

procedure TMainTestForm.FormShow(Sender: TObject);
begin
  WeekEndingDate.Date := EndOfTheWeek(Today);
end;

end.
