use testdb
declare @StartDate datetime
declare @StopDate datetime
declare @CutoffDate datetime

set @StartDate = 'May 16, 2009'
set @StopDate = 'May 23, 2009'
set @CutoffDate = DateAdd(Day, 7, @StopDate)
/*
select * from timesheet_entry 
where work_emp_id <> entry_by and status <> 'old'
and work_date between @StartDate and @StopDate
order by work_emp_id, work_date 

select * from timesheet_entry where work_emp_id = 782 order by work_date, entry_id
*/
exec dbo.get_tse_audit @StartDate, @StopDate, 811
