unit RTBillingTest;

interface

uses
  TestFrameWork, Classes, SysUtils;

type
  TRTBTest = class(TTestCase)
  published
    procedure TestRateOneThing;
  end;

implementation

uses IncrementalBilling;

{ TRTLTest }

procedure TRTBTest.TestRateOneThing;
//var
//  Output: TBillingTransaction;
begin

  // Load the datasets from a file

  {
  Output := IncrementalRate();

  CheckEquals(34, Output.Rate);
  }

end;

initialization
  TestFramework.RegisterTest(TRTBTest.Suite);

end.
