unit SyncTest;

interface

uses
  Classes, Contnrs, TestFrameWork, SysUtils, DB, Dialogs, SyncEngineU,
  SingleEngineTest;

type
  TSyncTest = class(TSingleEngineTest)
  private
    procedure Sync;
  published
    procedure SyncTwice;
    procedure TestInsertion;
    procedure TestMultiInsertion;
    procedure TestChange;
    procedure TestDelete;
    procedure TestFieldValues;
    procedure TestNullValue;
    procedure TestLookup;
    procedure TestWhereClause;
    procedure TestClientInsert;
    procedure TestClientInsertTwice;
    procedure TestClientChange;
    procedure TestClientDelete;
    procedure TestLastSyncDate;
    procedure TestPopulatePK;
    procedure TestSyncOnlySendsOnce;
    procedure TestEmptySyncAtFirst;
  public
    function TableName: string; override;
  end;

implementation

uses OdIsoDates;

{ TSyncTest }

procedure TSyncTest.TestChange;
begin
  CheckRecordCount(0);
  TestService.EmptyServerTable(TableName);

  Sync;
  CheckRecordCount(0);

  TestService.RunSql('INSERT INTO office (office_name, hours_to_respond) VALUES (''North'',10)');     // put in one
  TestService.RunSql('INSERT INTO office (office_name, hours_to_respond) VALUES (''South'',11)');     // put in one
  TestService.RunSql('INSERT INTO office (office_name, hours_to_respond) VALUES (''East'',12)');     // put in one
  Sync;
  CheckEquals(3, Engine.RowsReceived);    // only 3 since last sync.
  CheckRecordCount(3);

  with Engine.OpenTableWithEditTracking(TableName) do try
    Locate('office_name', 'East', []);
    CheckEquals(12, FieldByName('hours_to_respond').AsInteger);
  finally
    Free;
  end;

  TestService.RunSql('UPDATE office SET hours_to_respond = 15 WHERE office_name=''East''');     // put in one
  Sync;
  CheckEquals(1, Engine.RowsReceived);  // only the one that changed.
  CheckRecordCount(3);   // should not add another.

  with Engine.OpenTableWithEditTracking(TableName) do try
    Check(Locate('office_name', 'East', []));
    CheckEquals(15, FieldByName('hours_to_respond').AsInteger);
  finally
    Free;
  end;
end;

procedure TSyncTest.TestFieldValues;
begin
  TestService.RunSql('INSERT INTO office (office_name, hours_to_respond) VALUES (''South'',11)');     // put in one
  Sync;
  CheckRecordCount(1);

  with Engine.OpenTableWithEditTracking(TableName) do try
    First;
    CheckEquals('South', FieldByName('office_name').AsString);
    CheckEquals(11, FieldByName('hours_to_respond').AsInteger);
  finally
    Free;
  end;

end;

procedure TSyncTest.TestInsertion;
begin
  CheckRecordCount(0);  // I have no offices
  Sync;
  CheckEquals(0, Engine.RowsSent);
  CheckRecordCount(0);  // I have no offices

  TestService.RunSql('INSERT INTO office (office_name) VALUES (''North'')');     // put in one
  CheckRecordCount(0);  // Still none

  Sync;
  CheckEquals(1, Engine.RowsReceived);
  CheckRecordCount(1);   // now it's here

  Sync;
  CheckEquals(0, Engine.RowsReceived);   // it does not come again
  CheckRecordCount(1);  // and we will only have one row
end;

procedure TSyncTest.TestLookup;
begin
  with Engine.OpenTableWithEditTracking(TableName) do try
    AppendRecord([10, 'Ten']);
    AppendRecord([15, 'Ten']);
    AppendRecord([14, 'Ten']);
    AppendRecord([11, 'Ten']);
    AppendRecord([12, 'Ten']);
    Check(Locate('office_id', 14, []));
  finally
    Free;
  end;
end;

procedure TSyncTest.TestDelete;
begin
  CheckRecordCount(0);

  TestService.RunSql('INSERT INTO office (office_name, hours_to_respond) VALUES (''North'',10)');     // put in one
  TestService.RunSql('INSERT INTO office (office_name, hours_to_respond) VALUES (''South'',11)');     // put in one
  TestService.RunSql('INSERT INTO office (office_name, hours_to_respond) VALUES (''East'',12)');     // put in one
  Sync;
  CheckRecordCount(3);

  TestService.RunSql('UPDATE office SET active=0 WHERE office_name=''East''');     // put in one

  Sync;
  CheckEquals(1, Engine.RowsReceived);  // just the delete
  CheckRecordCount(2);
end;

procedure TSyncTest.TestMultiInsertion;
begin
  TestService.RunSql('INSERT INTO office (office_name) VALUES (''North'')');
  TestService.RunSql('INSERT INTO office (office_name) VALUES (''Upper'')');
  TestService.RunSql('INSERT INTO office (office_name) VALUES (''Atlanta'')');

  CheckRecordCount(0);   // not local yet

  Sync;
  CheckRecordCount(3, 'records arrived from server');   // now it's here - more than one

  Engine.EmptyAllTables;  // and if I lose my brain...
  Sync;
  CheckRecordCount(3, 'records re-fetched to blank local db');   // they all come back.
end;

procedure TSyncTest.TestWhereClause;
begin
  with Engine.OpenTableWithEditTracking(TableName) do try
    AppendRecord([10, 'Ten1', 'prof1', 4, Date, True]);
    CheckRecordCount(1);
    AppendRecord([15, 'Ten2']);
    AppendRecord([14, 'Ten3']);
    AppendRecord([11, 'Ten4']);
    AppendRecord([12, 'Ten5']);
    CheckRecordCount(5);

    Check(Locate('office_id', 14, []));
    Edit;
    FieldByName('Active').AsBoolean := False;
    Post;
  finally
    Free;
  end;
  CheckRecordCount(4);
end;

procedure TSyncTest.TestClientChange;
var
  ID1: Integer;
begin
  Sync;

  TestMultiInsertion;  // to get populated easily
  // we now have three rows, both there and here.
  CheckRecordCount(3);  // the three we put in there are still there

  with Engine.OpenTableWithEditTracking(TableName) do try
    Edit;
    FieldByName('profit_center').AsString := 'ABC001';
    Post;
    CheckEquals('U', FieldByName('DeltaStatus').AsString);
    ID1 := FieldByName('office_id').AsInteger;
    Next;

    Edit;
    FieldByName('profit_center').AsString := 'XYZ05';
    Post;
    CheckEquals('U', FieldByName('DeltaStatus').AsString);
    Next;

  finally
    Free;
  end;

  Sync;  // save changes to server
  CheckEquals(2, Engine.RowsSent);
  CheckEquals(2, Engine.RowsReceived);  // I get my changes back.

  Engine.EmptyAllTables;  // throw them away
  Sync;            // get them back
  CheckRecordCount(3);    // make sure they came back

  // are those changes here?
  with Engine.OpenTableWithEditTracking(TableName) do try
    Check(Locate('office_id', ID1, []));
    CheckEquals('ABC001', FieldByName('profit_center').AsString);
  finally
    Free;
  end;
end;

procedure TSyncTest.TestClientDelete;
begin
  TestMultiInsertion;  // to get populated easily
  // we now have three rows, both there and here.
  CheckRecordCount(3);  // the three we put in there are still there

  with Engine.OpenTableWithEditTracking(TableName) do try
    First;

    Edit;
    FieldByName('active').AsBoolean := False;
    Post;  // the non-active record will no longer be current, it will
           // disapper from view!
    Next;

    Edit;
    FieldByName('active').AsBoolean := False;
    Post;
  finally
    Free;
  end;

  CheckRecordCount(1);    // immediately only see one.
  Sync;  // save changes to server
  CheckRecordCount(1);    // still only see one.

  Engine.EmptyAllTables;  // throw them away
  Sync;            // get them back
  CheckRecordCount(1);    // make sure they came back - only one
end;

procedure TSyncTest.TestClientInsert;
begin
  with Engine.OpenTableWithEditTracking(TableName) do try
    AppendRecord([-1, 'Boise']);
    AppendRecord([-2, 'Sacremento']);
    AppendRecord([-3, 'Iowa City']);

    First;
    CheckEquals('I', FieldByName('DeltaStatus').AsString);  // should be an insert
    Next;
    CheckEquals('I', FieldByName('DeltaStatus').AsString);  // should be an insert
    Next;
    CheckEquals('I', FieldByName('DeltaStatus').AsString);  // should be an insert
  finally
    Free;
  end;

  Sync;
  CheckRecordCount(3);

  Engine.EmptyAllTables;
  CheckRecordCount(0);

  Sync;
  CheckRecordCount(3, 'records come back from the server');
end;

procedure TSyncTest.TestLastSyncDate;
var
  LastSync: string;
begin
  // initally, we should have no sync date
  Check(Engine.GetLastSyncDate = '', 'start with no sync date');
  Sync;

  LastSync := Engine.GetLastSyncDate;
  Check(LastSync > IsoDateTimeToStr( Now-0.1));  // we have a recent sync now
end;

procedure TSyncTest.TestPopulatePK;
begin
  with Engine.OpenTableWithEditTracking(TableName) do try
    AppendRecord([-10, 'Boise']);
    AppendRecord([-22, 'Sacremento']);
    AppendRecord([-3, 'Iowa City']);

    Engine.PopulatePK(TableName, '-22', '22');  // server made it 22

    Check(Locate('office_id', '22', []));  // found it
  finally
    Free;
  end;
end;

procedure TSyncTest.TestSyncOnlySendsOnce;
begin
  TestMultiInsertion;

  Sync; // there should be nothing to do.
  CheckEquals(0, Engine.RowsReceived);
  CheckEquals(0, Engine.RowsSent);

  Sync; // there should be nothing to do.
  CheckEquals(0, Engine.RowsReceived);
  CheckEquals(0, Engine.RowsSent);
end;

procedure TSyncTest.TestEmptySyncAtFirst;
begin
  Sync; // the other tables will sync some rows.
  Sync; // but only once.
  CheckEquals(0, Engine.RowsReceived);
  CheckEquals(0, Engine.RowsSent);
end;

procedure TSyncTest.TestClientInsertTwice;
begin
  with Engine.OpenTableWithEditTracking(TableName) do try
    AppendRecord([-1, 'Boise']);
    AppendRecord([-2, 'Sacremento']);
    AppendRecord([-3, 'Iowa City']);

    Sync;
    CheckEquals(3, Engine.RowsSent);

    AppendRecord([-4, 'Iowa City']);
    AppendRecord([-5, 'Iowa City']);

    Sync;
    CheckEquals(2, Engine.RowsSent);
    CheckRecordCount(5);
  finally
    Free;
  end;
end;

procedure TSyncTest.Sync;
var
  Errors: TObjectList;
  SyncRequestSource: string;
begin
  Errors := TObjectList.Create;
  try
    Engine.Sync(0, Errors, SyncRequestSource);
  finally
    FreeAndNil(Errors);
  end;
end;

function TSyncTest.TableName: string;
begin
  Result := 'office';
end;

procedure TSyncTest.SyncTwice;
begin
  Sync;
  Sync;                   // I do another
  CheckEquals(0, Engine.RowsReceived);
  // nothing should come in the second time.
end;

procedure TSyncTest.TestNullValue;
var
  Office: TDataSet;
begin
  Office := Engine.OpenTableWithEditTracking(TableName);
  try
    CheckRecordCount(0);
    Office.AppendRecord([-1, 'Office Name']);
    Check(Office.FieldByName('profit_center').IsNull);
    CheckRecordCount(1);
    Sync;
    CheckRecordCount(1);
    Check(Office.FieldByName('profit_center').IsNull);
  finally
    Office.Free;
  end;
end;

initialization
  TestFramework.RegisterTest(TSyncTest.Suite);

end.

