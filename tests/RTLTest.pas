unit RTLTest;

interface

uses
  TestFrameWork, Classes, SysUtils;

type
  TRTLTest = class(TTestCase)
  published
    procedure TestIntegerList;
  end;

implementation

uses OdContainer;

{ TRTLTest }

procedure TRTLTest.TestIntegerList;
var
  List: TIntegerList;
begin
  List := TIntegerList.Create;
  try
    List.Add(5);
    List.Add(3);
    List.Add(1);
    List.Add(11);
    List.Add(7);
    List.Add(9);

    Check(List.Count = 6);
    Check(List.Has(1));
    Check(List.Has(3));
    Check(List.Has(5));
    Check(List.Has(7));
    Check(List.Has(9));
    Check(List.Has(11));

    Check(not List.Has(0));
    Check(not List.Has(2));
    Check(not List.Has(4));
    Check(not List.Has(-1));
    Check(not List.Has(-999));

    CheckEquals(List.IndexOf(5), 0);
    CheckEquals(List.IndexOf(3), 1);
    CheckEquals(List.IndexOf(1), 2);
    CheckEquals(List.IndexOf(11), 3);
    CheckEquals(List.IndexOf(7), 4);
    CheckEquals(List.IndexOf(9), 5);

    CheckEquals(List.Count, 6);
    List.Add(13);
    CheckEquals(List.Count, 7);

    Check(List.Has(1));
    Check(List.Has(3));
    Check(List.Has(5));
    Check(List.Has(7));
    Check(List.Has(9));
    Check(List.Has(11));
    Check(List.Has(13));
    List.Sort;
    CheckEquals(List.IndexOf(1), 0);
    CheckEquals(List.IndexOf(3), 1);
    CheckEquals(List.IndexOf(5), 2);
    CheckEquals(List.IndexOf(7), 3);
    CheckEquals(List.IndexOf(9), 4);
    CheckEquals(List.IndexOf(11), 5);
    CheckEquals(List.IndexOf(13), 6);

    CheckEquals(List.SortedIndexOf(1), 0);
    CheckEquals(List.SortedIndexOf(3), 1);
    CheckEquals(List.SortedIndexOf(5), 2);
    CheckEquals(List.SortedIndexOf(7), 3);
    CheckEquals(List.SortedIndexOf(9), 4);
    CheckEquals(List.SortedIndexOf(11), 5);
    CheckEquals(List.SortedIndexOf(13), 6);

    List.Add(14);
    List.Add(0);
    List.Add(2);
    List.Sort;

    CheckEquals(List.SortedIndexOf(14), 9);
    CheckEquals(List.SortedIndexOf(0), 0);
    CheckEquals(List.IndexOf(14), 9);
    CheckEquals(List.IndexOf(0), 0);

    CheckEquals(List.Count, 10);
    Check(List.Has(1));
    Check(List.Has(3));
    Check(List.Has(5));
    Check(List.Has(7));
    Check(List.Has(9));
    Check(List.Has(11));
    Check(List.Has(13));
    Check(List.Has(14));
    Check(List.Has(0));
    Check(List.Has(2));

    Check(not List.Has(4));
    Check(not List.Has(-1));
    Check(not List.Has(-999));

    List.Clear;
    CheckEquals(List.Count, 0);
    CheckEquals(List.SortedIndexOf(0), -1);
    CheckEquals(List.IndexOf(0), -1);
    Check(not List.Has(0));
  finally
    FreeAndNil(List);
  end;
end;

initialization
  TestFramework.RegisterTest(TRTLTest.Suite);

end.
