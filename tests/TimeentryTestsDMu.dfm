object TimeTestDM: TTimeTestDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 103
  Width = 116
  object TimesheetData: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.34 Build 7'
    FieldDefs = <
      item
        Name = 'entry_id'
        DataType = ftInteger
      end
      item
        Name = 'work_emp_id'
        DataType = ftInteger
      end
      item
        Name = 'work_date'
        DataType = ftDateTime
      end
      item
        Name = 'modified_date'
        DataType = ftDateTime
      end
      item
        Name = 'status'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'entry_date'
        DataType = ftDateTime
      end
      item
        Name = 'entry_date_local'
        DataType = ftDateTime
      end
      item
        Name = 'entry_by'
        DataType = ftInteger
      end
      item
        Name = 'approve_date'
        DataType = ftDateTime
      end
      item
        Name = 'approve_date_local'
        DataType = ftDateTime
      end
      item
        Name = 'approve_by'
        DataType = ftInteger
      end
      item
        Name = 'final_approve_date'
        DataType = ftDateTime
      end
      item
        Name = 'final_approve_date_local'
        DataType = ftDateTime
      end
      item
        Name = 'final_approve_by'
        DataType = ftInteger
      end
      item
        Name = 'work_start1'
        DataType = ftDateTime
      end
      item
        Name = 'work_stop1'
        DataType = ftDateTime
      end
      item
        Name = 'work_start2'
        DataType = ftDateTime
      end
      item
        Name = 'work_stop2'
        DataType = ftDateTime
      end
      item
        Name = 'work_start3'
        DataType = ftDateTime
      end
      item
        Name = 'work_stop3'
        DataType = ftDateTime
      end
      item
        Name = 'work_start4'
        DataType = ftDateTime
      end
      item
        Name = 'work_stop4'
        DataType = ftDateTime
      end
      item
        Name = 'work_start5'
        DataType = ftDateTime
      end
      item
        Name = 'work_stop5'
        DataType = ftDateTime
      end
      item
        Name = 'callout_start1'
        DataType = ftDateTime
      end
      item
        Name = 'callout_stop1'
        DataType = ftDateTime
      end
      item
        Name = 'callout_start2'
        DataType = ftDateTime
      end
      item
        Name = 'callout_stop2'
        DataType = ftDateTime
      end
      item
        Name = 'callout_start3'
        DataType = ftDateTime
      end
      item
        Name = 'callout_stop3'
        DataType = ftDateTime
      end
      item
        Name = 'callout_start4'
        DataType = ftDateTime
      end
      item
        Name = 'callout_stop4'
        DataType = ftDateTime
      end
      item
        Name = 'callout_start5'
        DataType = ftDateTime
      end
      item
        Name = 'callout_stop5'
        DataType = ftDateTime
      end
      item
        Name = 'callout_start6'
        DataType = ftDateTime
      end
      item
        Name = 'callout_stop6'
        DataType = ftDateTime
      end
      item
        Name = 'miles_start1'
        DataType = ftInteger
      end
      item
        Name = 'miles_stop1'
        DataType = ftInteger
      end
      item
        Name = 'vehicle_use'
        DataType = ftString
        Size = 5
      end
      item
        Name = 'reg_hours'
        DataType = ftBCD
        Size = 2
      end
      item
        Name = 'ot_hours'
        DataType = ftBCD
        Size = 2
      end
      item
        Name = 'dt_hours'
        DataType = ftBCD
        Size = 2
      end
      item
        Name = 'callout_hours'
        DataType = ftBCD
        Size = 2
      end
      item
        Name = 'vac_hours'
        DataType = ftBCD
        Size = 2
      end
      item
        Name = 'leave_hours'
        DataType = ftBCD
        Size = 2
      end
      item
        Name = 'br_hours'
        DataType = ftBCD
        Size = 2
      end
      item
        Name = 'hol_hours'
        DataType = ftBCD
        Size = 2
      end
      item
        Name = 'jury_hours'
        DataType = ftBCD
        Size = 2
      end
      item
        Name = 'floating_holiday'
        DataType = ftBoolean
      end
      item
        Name = 'rule_ack_date'
        DataType = ftDateTime
      end
      item
        Name = 'rule_id_ack'
        DataType = ftInteger
      end
      item
        Name = 'emp_type_id'
        DataType = ftInteger
      end
      item
        Name = 'work_pc_code'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'reason_changed'
        DataType = ftInteger
      end
      item
        Name = 'source'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'lunch_start'
        DataType = ftDateTime
      end
      item
        Name = 'DeltaStatus'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LocalKey'
        DataType = ftDateTime
      end
      item
        Name = 'LocalStringKey'
        DataType = ftString
        Size = 20
      end>
    TableName = 'timesheet_entry'
    StoreDefs = True
    Left = 40
    Top = 19
  end
end
