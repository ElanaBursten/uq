set dest=C:\uq

rem Get deployed file versions using vbscript
rem Alternatively, the Microsoft Support tool Filever.exe could be used, but requires installation on the build machine.
rem More information: http://support.microsoft.com/kb/913111

ECHO OFF
echo Getting deployed file versions...

cd\
cd %USERPROFILE%\Desktop

echo QManager.exe file version:
cscript //nologo %dest%\VersionInfo.vbs %dest%\QManager\QManager.exe

echo QManagerAllInOne.exe file version:
cscript //nologo %dest%\VersionInfo.vbs %dest%\QManager\QManagerAllInOne.exe

echo QManagerAdmin.exe file version:
cscript //nologo %dest%\VersionInfo.vbs %dest%\Admin\QManagerAdmin.exe

echo ReportEngineCGI.exe file version:
cscript //nologo %dest%\VersionInfo.vbs %dest%\TestServerApp\ReportEngineCGI.exe

echo QMLogic.dll file version:
cscript //nologo %dest%\VersionInfo.vbs %dest%\TestServerApp\QMLogic.dll

echo Please verify these file versions match the build you are testing.

pause