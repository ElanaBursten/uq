ECHO OFF
rem cscript should be in the system PATH

echo Getting deployed file versions...

echo ReportEngineCGI.exe file version:
cscript //nologo "%CD%"\VersionInfo.vbs "%CD%"\ReportEngineCGI.exe

echo QMLogic.dll file version:
cscript //nologo "%CD%"\VersionInfo.vbs "%CD%"\QMLogic.dll

echo Please verify these file versions match the build you are testing.

pause