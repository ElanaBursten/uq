ECHO OFF

rem Refreshes the test environment from latest build.

set src=C:\QMBuildServer\QManager\build
set dest=C:\uq

set qmsrc=C:\QMBuildServer\qmanager\client
set adminsrc=C:\QMBuildServer\qmanager\admin
set reportsrc=C:\QMBuildServer\qmanager\reportengine

set deskpath="%USERPROFILE%\Desktop"

copy %src%\QM-?.?.?.*.zip         %dest%\setup
copy %src%\QManagerSetup.exe      %dest%\setup
copy %src%\QManagerAdminSetup.exe %dest%\setup

copy %qmsrc%\QManager.exe %dest%\QManager
copy %qmsrc%\QManagerAllInOne.exe %dest%\QManager
copy %src%\SampleTicketAttachmentPlugin.exe %dest%\QManager\Addins

copy %adminsrc%\QManagerAdmin.exe %dest%\Admin

rem These may fail if loaded in IIS
copy %src%\QMLogic.dll %dest%\TestServerApp
copy %reportsrc%\ReportEngineCGI.exe %dest%\TestServerApp


call %dest%\CheckVersion.bat




