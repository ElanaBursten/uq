unit SingleEngineTest;

interface

uses
  Classes, TestFrameWork, SyncEngineU, SysUtils,
  ServerProxy, DMu, QMService_Impl, QMServerLibrary_Intf, TestServiceU, Forms;

type
  TBaseTest = class(TTestCase)
  private
    function ServerRecordCount(const TableName: string): Integer;
  protected
    Engine: TLocalCache;
    Server: TServer;
    DMod: TDM;
    function NumberOfTickets: Integer;
    function OfficeCount: Integer;
    procedure LoadSampleTickets;
    procedure SetUp; override;
    procedure TearDown; override;
    procedure SetupServerTables;
    procedure InitAndEmptyLocalEngine(var Engine: TLocalCache; Directory: string);
  public
    constructor Create(MethodName: string); override;
  end;

  TSingleEngineTest = class(TBaseTest)
  protected
    procedure CheckRecordCount(Count: Integer); overload;
    procedure CheckRecordCount(Count: Integer; Mes: string); overload;
  public
    procedure SetUp; override;
    function TableName: string; virtual; abstract;
  end;

const
  TestingSecSession = 'testing_sec_id_string_2348323r34';

var
  QMS: QMService;
  TestService: TTestService;

implementation

{ TBaseTest }

procedure TBaseTest.InitAndEmptyLocalEngine(var Engine: TLocalCache; Directory: string);
begin
  FreeAndNil(Engine);

  if Assigned(DMod) then begin
    Engine := TLocalCache.CreateWithDatabase(DMod.Database1);
  end else begin
    Engine := TLocalCache.Create(Directory);
  end;
  Engine.Server := Server;
  Engine.Service := QMS;
  Engine.SecurityInfo := TestingSecSession;
  Engine.EmptyAllTables;
end;

procedure TBaseTest.SetUp;
begin
  inherited;
  FreeAndNil(Server);

  // TODO: look in the INI file, [SqlXml]  BaseUrl
  Server := CreateRemoteServer(nil, 'http://localhost/TestDB');

  InitAndEmptyLocalEngine(Engine, 'data1');

  if not Assigned(QMS) then
    QMS := TQMService.Create;
  if not Assigned(TestService) then
    TestService := TTestService.Create(Application);
end;

procedure TBaseTest.TearDown;
begin
  inherited;
  FreeAndNil(Engine);
  FreeAndNil(Server);
end;

function TBaseTest.NumberOfTickets: Integer;
begin
  Result := ServerRecordCount('Ticket');
end;

function TBaseTest.OfficeCount: Integer;
begin
  Result := ServerRecordCount('OFFICE');
end;

function TBaseTest.ServerRecordCount(const TableName: string): Integer;
begin
  Result := TestService.RecordCount(TableName);
end;

procedure TBaseTest.LoadSampleTickets;
begin
  Assert(Assigned(Server));
//todo: line below commented out to get compiling; InsertSampleTickets missing
//todo   Server.InsertSampleTickets;
end;

constructor TBaseTest.Create(MethodName: string);
begin
  inherited;
end;

procedure TBaseTest.SetupServerTables;
begin
  TestService.EmptyServerTables;
  TestService.InsertStatus('M',  'Marked', 1, 1);
  TestService.InsertStatus('O',  'Ongoing', 0, 0);
  TestService.InsertStatus('-R', 'New', 0, 0);
  TestService.InsertStatus('-P', 'Internal P', 0, 0);
  TestService.InsertStatus('-N', 'Internal N', 0, 0);
  LoadSampleTickets;
end;

{ TSingleEngineTest }

procedure TSingleEngineTest.CheckRecordCount(Count: Integer; Mes: string);
begin
  Assert(Assigned(Engine));
  CheckEquals(Count, Engine.ActiveRecordCount(TableName), Mes);
end;

procedure TSingleEngineTest.CheckRecordCount(Count: Integer);
begin
  Assert(Assigned(Engine));
  CheckEquals(Count, Engine.ActiveRecordCount(TableName));
end;

procedure TSingleEngineTest.SetUp;
begin
  inherited;
  Assert(Assigned(Engine));
  TestService.RunSql('DELETE FROM assignment');
  TestService.RunSql('DELETE FROM locate');
  TestService.RunSql('DELETE FROM ticket');
  TestService.RunSql('DELETE FROM client');
  TestService.RunSql('DELETE FROM ' + TableName);
end;

end.

