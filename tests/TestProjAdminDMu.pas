unit TestProjAdminDMu;

interface

uses
  TestFramework, Data.DB, FireDAC.Phys.Intf, FireDAC.Stan.Pool, FireDAC.Comp.Client, FireDAC.Phys.MSSQL,
  FireDAC.Stan.Def, System.SysUtils, System.Classes, FireDAC.Phys, FireDAC.Stan.Error,
  ProjAdminDmu, FireDAC.Stan.Intf, FireDAC.UI.Intf, FireDAC.Stan.Option, FireDAC.Stan.Async, AdUtils;

type
  TestTProjAdminDM = class(TTestCase)
  strict private
    FProjAdminDM: TProjAdminDM;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestReloadMetricAndHierarchyData;
  end;

implementation

procedure TestTProjAdminDM.SetUp;
begin
  FProjAdminDM := TProjAdminDM.Create(nil);
end;

procedure TestTProjAdminDM.TearDown;
begin
  FreeAndNil(FProjAdminDM);
end;

procedure TestTProjAdminDM.TestReloadMetricAndHierarchyData;
var
  MetricsConn: TADConnection;
begin
  MetricsConn := FProjAdminDM.Conn;
  FProjAdminDM.ReloadMetricAndHierarchyData(1);
  Check((MetricsConn.ExecSQLScalar('select COUNT(*) from metric_entity') > 0), 'Table metric_entity is not populated as expected');
  Check((MetricsConn.ExecSQLScalar('select COUNT(*) from hierarchy') > 0), 'Table hierarchy is not populated as expected');
  Check((MetricsConn.ExecSQLScalar('select COUNT(*) from hclos') > 0), 'Table hclos is not populated as expected');
  Check((MetricsConn.ExecSQLScalar('select COUNT(*) from employee') > 0), 'Table employee is not populated as expected');
  Check((MetricsConn.ExecSQLScalar('select COUNT(*) from eclos') > 0), 'Table eclos is not populated as expected');
  Check((MetricsConn.ExecSQLScalar('select COUNT(*) from emp_allowed_node') > 0), 'Table emp_allowed_node is not populated as expected');
end;

initialization
  RegisterTest(TestTProjAdminDM.Suite);

end.

