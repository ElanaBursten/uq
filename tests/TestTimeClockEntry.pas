unit TestTimeClockEntry;

interface

uses
  TestFramework, Classes, SysUtils, Contnrs, TimeClockEntry;

type
  // Test methods for class TTimeClockTranslator

  TestTTimeClockTranslator = class(TTestCase)
  private
    TT: TTimeClockTranslator;
    SDS: TStringDataSaver;
    SRW: TStringRowWriter;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestAddEmptyEntryDay;
    procedure TestAddTypicalClockDay;
    procedure TestNoEndTime;
    procedure TestMultipleBreaks;
    procedure TestAddCalloutTimesDay;
    procedure TestUnsubmittedTime1;
    procedure TestUnsubmittedTime2;
    procedure TestSaveEntry;
    procedure TestSaveBlankDay;
    procedure TestSaveNoEndTime;
    procedure TestSaveMultipleBreaks;
    procedure TestSaveCalloutTimesDay;
    procedure TestPreventConsecutiveStarts;
    procedure TestPreventConsecutiveStops;
    procedure TestPreventTooManyWorkTimes;
    procedure TestPreventTooManyCalloutTimes;
    procedure TestPreventDuplicateTimes;
    procedure TestDetectHoursChange1;
    procedure TestDetectHoursChange2;
    procedure TestDetectHoursChange3;
    procedure TestDetectHoursChange4;
  end;

implementation

uses DateUtils, OdMiscUtils;

const
  AEmpID = 123;
  AWorkDate = 43022.0;  // 2017-10-14

procedure TestTTimeClockTranslator.SetUp;
begin
  SDS := TStringDataSaver.Create;
  SRW := TStringRowWriter.Create;
  TT  := TTimeClockTranslator.Create(SDS, SRW);
  TT.Initialize(AEmpID, AWorkDate, True);
end;

procedure TestTTimeClockTranslator.TearDown;
begin
  FreeAndNil(TT);
end;

procedure TestTTimeClockTranslator.TestAddTypicalClockDay;
begin
  TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('11:30'), LunchStart);
  TT.AddTimeClockTime(StrToTime('12:00'), LunchStop);
  TT.AddTimeClockTime(StrToTime('17:00'), WorkStop);
  TT.ExportTabularForm;
  CheckEquals('123:20171014:0700:Work In|123:20171014:1130:Lunch Out|123:20171014:1200:Lunch In|123:20171014:1700:Work Out|', SRW.AsString);
end;

function CalcTotalHours(StartTimes, StopTimes: array of TDateTime): Double;
var
  I: Integer;
begin
  Result := 0;
  for I := Low(StartTimes) to High(StartTimes) do
    Result := Result + SecondsBetween(StopTimes[I], StartTimes[I]) / (60 * 60);
  Result := FloatRound(Result, 4);
end;

procedure TestTTimeClockTranslator.TestDetectHoursChange1;
var
  ExpectedHours, TotalHours: Double;
  StartTimes: array[1..4] of TDateTime;
  StopTimes: array[1..4] of TDateTime;
begin
  StartTimes[1] := StrToTime('07:46:03');
  StopTimes[1]  := StrToTime('11:07:08');
  StartTimes[2] := StrToTime('11:08:06');
  StopTimes[2]  := StrToTime('11:09:03');
  StartTimes[3] := StrToTime('11:10:19');
  StopTimes[3]  := StrToTime('11:11:02');
  StartTimes[4] := StrToTime('11:12:22');
  StopTimes[4]  := StrToTime('11:13:01');

  ExpectedHours := 3.39;
  TotalHours := CalcTotalHours(StartTimes, StopTimes);
  Check(FloatEquals(ExpectedHours, TotalHours), Format('%f <> %f', [ExpectedHours, TotalHours]));
end;

procedure TestTTimeClockTranslator.TestDetectHoursChange2;
var
  ExpectedHours, TotalHours: Double;
  StartTimes: array[1..3] of TDateTime;
  StopTimes: array[1..3] of TDateTime;
begin
  StartTimes[1] := StrToTime('07:45:00');
  StopTimes[1]  := StrToTime('11:45:00');
  StartTimes[2] := StrToTime('11:46:00');
  StopTimes[2]  := StrToTime('11:46:59');
  StartTimes[3] := StrToTime('12:00:00');
  StopTimes[3]  := StrToTime('15:33:00');

  ExpectedHours := 7.57;
  TotalHours := CalcTotalHours(StartTimes, StopTimes);
  Check(FloatEquals(ExpectedHours, TotalHours), Format('%f <> %f', [ExpectedHours, TotalHours]));
end;

procedure TestTTimeClockTranslator.TestDetectHoursChange3;
var
  ExpectedHours, TotalHours: Double;
  StartTimes: array[1..2] of TDateTime;
  StopTimes: array[1..2] of TDateTime;
begin
  StartTimes[1] := StrToTime('10:20:57');
  StopTimes[1]  := StrToTime('15:10:14');
  StartTimes[2] := StrToTime('15:12:12');
  StopTimes[2]  := StrToTime('15:14:14');

  ExpectedHours := 4.86;
  TotalHours := CalcTotalHours(StartTimes, StopTimes);
  Check(FloatEquals(ExpectedHours, TotalHours), Format('%f <> %f', [ExpectedHours, TotalHours]));
end;

procedure TestTTimeClockTranslator.TestDetectHoursChange4;
var
  ExpectedHours, TotalHours: Double;
  StartTimes: array[1..3] of TDateTime;
  StopTimes: array[1..3] of TDateTime;
begin
  StartTimes[1] := StrToTime('10:20:57');
  StopTimes[1]  := StrToTime('15:10:14');
  StartTimes[2] := StrToTime('15:12:12');
  StopTimes[2]  := StrToTime('15:14:14');
  StartTimes[3] := StrToTime('15:30:00');
  StopTimes[3]  := StrToTime('15:33:00');
  ExpectedHours := 4.91;
  TotalHours := CalcTotalHours(StartTimes, StopTimes);
  Check(FloatEquals(ExpectedHours, TotalHours), Format('%f <> %f', [ExpectedHours, TotalHours]));
end;

procedure TestTTimeClockTranslator.TestAddCalloutTimesDay;
begin
  TT.AddTimeClockTime(StrToTime('02:20'), CalloutStart);
  TT.AddTimeClockTime(StrToTime('04:30'), CalloutStop);
  TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('11:30'), LunchStart);
  TT.AddTimeClockTime(StrToTime('12:00'), LunchStop);
  TT.AddTimeClockTime(StrToTime('17:00'), WorkStop);
  TT.AddTimeClockTime(StrToTime('21:10'), CalloutStart);
  TT.AddTimeClockTime(StrToTime('22:20'), CalloutStop);
  TT.ExportTabularForm;
  CheckEquals('123:20171014:0220:Call In|123:20171014:0430:Call Out|123:20171014:0700:Work In|123:20171014:1130:Lunch Out|123:20171014:1200:Lunch In|123:20171014:1700:Work Out|123:20171014:2110:Call In|123:20171014:2220:Call Out|', SRW.AsString);
end;

procedure TestTTimeClockTranslator.TestMultipleBreaks;
begin
  TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('09:05'), PersonalStart);
  TT.AddTimeClockTime(StrToTime('10:10'), PersonalStop);
  TT.AddTimeClockTime(StrToTime('11:40'), LunchStart);
  TT.AddTimeClockTime(StrToTime('12:00'), LunchStop);
  TT.AddTimeClockTime(StrToTime('17:00'), WorkStop);
  TT.ExportTabularForm;

  CheckEquals('123:20171014:0700:Work In|123:20171014:0905:Personal Out|123:20171014:1010:Personal In|123:20171014:1140:Lunch Out|123:20171014:1200:Lunch In|123:20171014:1700:Work Out|', SRW.AsString);
end;

procedure TestTTimeClockTranslator.TestNoEndTime;
begin
  TT.Initialize(AEmpID, AWorkDate, False);
  TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('09:05'), PersonalStart);
  TT.AddTimeClockTime(StrToTime('10:10'), PersonalStop);
  TT.ExportTabularForm;

  CheckEquals('123:20171014:0700:Work In|123:20171014:0905:Personal Out|123:20171014:1010:Personal In|', SRW.AsString);
end;

procedure TestTTimeClockTranslator.TestUnsubmittedTime1;
var
  TodayStr: string;
begin
  TodayStr := FormatDateTime('yyyymmdd', Today);
  TT.Initialize(AEmpID, Today, False);
  TT.AddTimeClockTime(StrToTime('02:20'), CalloutStart);
  TT.AddTimeClockTime(StrToTime('04:30'), CalloutStop);
  TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('11:30'), LunchStart);
  TT.AddTimeClockTime(StrToTime('12:00'), LunchStop);
  TT.AddTimeClockTime(StrToTime('17:00'), WorkStop);
  TT.AddTimeClockTime(StrToTime('21:10'), CalloutStart);
  TT.AddTimeClockTime(StrToTime('22:20'), CalloutStop);
  TT.ExportTabularForm;
  CheckEquals('123:'+TodayStr+':0220:Call In|123:'+TodayStr+':0430:Call Out|123:'+
    TodayStr+':0700:Work In|123:'+TodayStr+':1130:Lunch Out|123:'+TodayStr+
    ':1200:Lunch In|123:'+TodayStr+':1700:Work Out|123:'+TodayStr+':2110:Call In|123:'+
    TodayStr+':2220:Call Out|', SRW.AsString);
end;

procedure TestTTimeClockTranslator.TestUnsubmittedTime2;
var
  TodayStr: string;
begin
  TodayStr := FormatDateTime('yyyymmdd', Today);
  TT.Initialize(AEmpID, Today, False);
  TT.AddTimeClockTime(StrToTime('02:20'), CalloutStart);
  TT.AddTimeClockTime(StrToTime('04:30'), CalloutStop);
  TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('11:30'), LunchStart);
  TT.AddTimeClockTime(StrToTime('12:00'), LunchStop);
  TT.AddTimeClockTime(StrToTime('17:00'), WorkStop);
  TT.ExportTabularForm;
  CheckEquals('123:'+TodayStr+':0220:Call In|123:'+TodayStr+':0430:Call Out|123:'+
    TodayStr+':0700:Work In|123:'+TodayStr+':1130:Lunch Out|123:'+TodayStr+
    ':1200:Lunch In|123:'+TodayStr+':1700:Personal Out|', SRW.AsString);
end;

procedure TestTTimeClockTranslator.TestPreventConsecutiveStops;
begin
  try
    TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
    TT.AddTimeClockTime(StrToTime('11:30'), PersonalStart);
    TT.AddTimeClockTime(StrToTime('12:00'), LunchStart);
    TT.AddTimeClockTime(StrToTime('17:00'), WorkStop);
    TT.CompleteSave;
    Fail('Expected exception to prevent consecutive stops');
  except
    on EInvalidTimeDataError do begin end; // expected
  end;
end;

procedure TestTTimeClockTranslator.TestPreventConsecutiveStarts;
begin
  try
    TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
    TT.AddTimeClockTime(StrToTime('11:30'), WorkStart);
    TT.CompleteSave;
    Fail('Expected exception to prevent consecutive starts');
  except
    on EInvalidTimeDataError do begin end; // expected
  end;
end;

procedure TestTTimeClockTranslator.TestPreventTooManyCalloutTimes;
var
  i: Integer;
  CalloutTime: TDateTime;
begin
  CalloutTime := StrToTime('01:00');

  for i := 1 to 15 do begin // only 6 pairs are allowed
    if i mod 2 = 0 then
      TT.AddTimeClockTime(Time, CalloutStop)
    else
      TT.AddTimeClockTime(Time, CalloutStart);
    IncMinute(CalloutTime, 30);
  end;

  try
    TT.CompleteSave;
    Fail('Expected an exception');
  except
    on EInvalidTimeDataError do begin end; // expected
  end;
end;

procedure TestTTimeClockTranslator.TestPreventTooManyWorkTimes;
var
  i: Integer;
  ClockTime: TDateTime;
begin
  ClockTime := StrToTime('08:00');
  for i := 1 to 10 do begin // only 4 pairs are allowed
    if i mod 2 = 0 then
      TT.AddTimeClockTime(ClockTime, WorkStop)
    else
      TT.AddTimeClockTime(ClockTime, WorkStart);
    IncMinute(ClockTime, 60);
  end;

  try
    TT.CompleteSave;
    Fail('Expected an exception');
  except
    on EInvalidTimeDataError do begin end; // expected
  end;
end;

procedure TestTTimeClockTranslator.TestPreventDuplicateTimes;
begin
  TT.Initialize(AEmpID, AWorkDate, False);
  TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('11:30'), LunchStart);
  TT.AddTimeClockTime(StrToTime('11:30'), LunchStop);
  TT.AddTimeClockTime(StrToTime('11:30'), WorkStop);
  try
    TT.CompleteSave;
    Fail('Expected an exception');
  except
    on EInvalidTimeDataError do begin end; // expected
  end;
end;

procedure TestTTimeClockTranslator.TestAddEmptyEntryDay;
begin
  TT.AddTimeEntryTimes;
  TT.ExportTabularForm;
  CheckEquals('', SRW.AsString);
end;

procedure TestTTimeClockTranslator.TestSaveBlankDay;
begin
  TT.CompleteSave;
  CheckEquals('C:clock:123:20171014|I:123:20171014:ws1=null:we1=null:ws2=null:we2=null:ws3=null:we3=null:' +
    'ws4=null:we4=null:cs1=null:ce1=null:cs2=null:ce2=null:cs3=null:ce3=null:' +
    'cs4=null:ce4=null:cs5=null:ce5=null:cs6=null:ce6=null:ls=null|', SDS.AsString);
end;

procedure TestTTimeClockTranslator.TestSaveCalloutTimesDay;
begin
  TT.AddTimeClockTime(StrToTime('02:20'), CalloutStart);
  TT.AddTimeClockTime(StrToTime('04:30'), CalloutStop);
  TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('11:30'), LunchStart);
  TT.AddTimeClockTime(StrToTime('12:00'), LunchStop);
  TT.AddTimeClockTime(StrToTime('17:00'), WorkStop);
  TT.AddTimeClockTime(StrToTime('21:10'), CalloutStart);
  TT.AddTimeClockTime(StrToTime('22:20'), CalloutStop);
  TT.CompleteSave;
  CheckEquals('C:clock:123:20171014|I:123:20171014:ws1=0700:we1=1130:ws2=1200:we2=1700:' +
    'ws3=null:we3=null:ws4=null:we4=null:cs1=0220:ce1=0430:cs2=2110:ce2=2220:' +
    'cs3=null:ce3=null:cs4=null:ce4=null:cs5=null:ce5=null:cs6=null:ce6=null:ls=1130|',
    SDS.AsString);
end;

procedure TestTTimeClockTranslator.TestSaveEntry;
begin
  TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('11:30'), LunchStart);
  TT.AddTimeClockTime(StrToTime('12:00'), LunchStop);
  TT.AddTimeClockTime(StrToTime('17:00'), WorkStop);
  TT.CompleteSave;
  CheckEquals('C:clock:123:20171014|I:123:20171014:ws1=0700:we1=1130:ws2=1200:we2=1700:' +
    'ws3=null:we3=null:ws4=null:we4=null:cs1=null:ce1=null:cs2=null:ce2=null:' +
    'cs3=null:ce3=null:cs4=null:ce4=null:cs5=null:ce5=null:cs6=null:ce6=null:ls=1130|',
    SDS.AsString);
end;

procedure TestTTimeClockTranslator.TestSaveMultipleBreaks;
begin
  TT.AddTimeClockTime(StrToTime('07:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('09:05'), PersonalStart);
  TT.AddTimeClockTime(StrToTime('10:10'), PersonalStop);
  TT.AddTimeClockTime(StrToTime('11:40'), LunchStart);
  TT.AddTimeClockTime(StrToTime('12:00'), LunchStop);
  TT.AddTimeClockTime(StrToTime('17:00'), WorkStop);
  TT.CompleteSave;
  CheckEquals('C:clock:123:20171014|I:123:20171014:ws1=0700:we1=0905:ws2=1010:we2=1140:' +
    'ws3=1200:we3=1700:ws4=null:we4=null:cs1=null:ce1=null:cs2=null:ce2=null:' +
    'cs3=null:ce3=null:cs4=null:ce4=null:cs5=null:ce5=null:cs6=null:ce6=null:ls=1140|',
    SDS.AsString);
end;

procedure TestTTimeClockTranslator.TestSaveNoEndTime;
begin
  TT.AddTimeClockTime(StrToTime('06:00'), WorkStart);
  TT.AddTimeClockTime(StrToTime('11:50'), LunchStart);
  TT.AddTimeClockTime(StrToTime('12:30'), LunchStop);
  TT.CompleteSave;
  CheckEquals('C:clock:123:20171014|I:123:20171014:ws1=0600:we1=1150:ws2=1230:we2=null:' +
    'ws3=null:we3=null:ws4=null:we4=null:cs1=null:ce1=null:cs2=null:ce2=null:' +
    'cs3=null:ce3=null:cs4=null:ce4=null:cs5=null:ce5=null:cs6=null:ce6=null:ls=1150|',
    SDS.AsString);
end;

initialization
  // Register any test cases with the test runner
  RegisterTest(TestTTimeClockTranslator.Suite);
end.

