program QMTests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$WARN SYMBOL_PLATFORM OFF}
{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

{$R 'QMProjAdminResources.res' '..\projections\QMProjAdminResources.rc'}

uses
  DUnitTestRunner,
  FireDAC.Stan.Pool,
  TestQMEvents in 'TestQMEvents.pas',
  QMLogic2ServiceLib in '..\server2\gen-delphi\QMLogic2ServiceLib.pas',
  TestQMThriftUtils in 'TestQMThriftUtils.pas',
  EventSource in '..\server2\EventSource.pas',
  QMCommands in '..\server2\QMCommands.pas',
  MSSqlEventSource in '..\server2\MSSqlEventSource.pas',
  MemoryEventSource in 'MemoryEventSource.pas',
  QMThriftUtils in '..\common\QMThriftUtils.pas',
  TestDataDMu in 'TestDataDMu.pas' {TestDM: TDataModule},
  TestMSSqlEventSource in 'TestMSSqlEventSource.pas',
  TestQMJSonFormat in 'TestQMJSonFormat.pas',
  InterfacedDataModuleU in '..\common\InterfacedDataModuleU.pas' {InterfacedDM: TDataModule},
  TestInterfacedDataModuleU in 'TestInterfacedDataModuleU.pas',
  OperationTracking in '..\server\OperationTracking.pas',
  BrokenInterfacedDMu in 'BrokenInterfacedDMu.pas' {BrokenInterfacedDM: TDataModule},
  MyDMIntf in 'MyDMIntf.pas',
  TestThreadSafeLoggerU in 'TestThreadSafeLoggerU.pas',
  QMApi in '..\server2\gen-delphi\QMApi.pas',
  QMApiCommand in '..\server2\gen-delphi\QMApiCommand.pas',
  QMApiQuery in '..\server2\gen-delphi\QMApiQuery.pas',
  QMApiShared in '..\server2\gen-delphi\QMApiShared.pas',
  TestQMCommands in 'TestQMCommands.pas',
  EventUtils in '..\server2\EventUtils.pas',
  CommandUtils in '..\server2\CommandUtils.pas',
  RestructureTest in 'RestructureTest.pas',
  AxisTest in 'AxisTest.pas',
  AdminDMu in '..\admin\AdminDMu.pas' {AdminDM: TDataModule},
  ConfigDMu in '..\admin\ConfigDMu.pas' {ConfigDM: TDataModule},
  GridAxis in '..\admin\GridAxis.pas',
  RTBillingTest in 'RTBillingTest.pas',
  IncrementalBilling in '..\billing\IncrementalBilling.pas',
  BillingConfiguration in '..\billing\BillingConfiguration.pas' {BillingConfig: TDataModule},
  BillingConst in '..\billing\BillingConst.pas',
  BillingRates in '..\billing\BillingRates.pas' {BillingRateTable: TDataModule},
  BillingShared in '..\billing\BillingShared.pas',
  BillingTicketClientList in '..\billing\BillingTicketClientList.pas' {TicketClientList: TDataModule},
  BillingLocate in '..\billing\BillingLocate.pas',
  QMBillingDMu in '..\billing\QMBillingDMu.pas' {QMBillingDM: TDataModule},
  BillPeriodTest in 'BillPeriodTest.pas',
  BillPeriod in '..\billing\BillPeriod.pas',
  QMConst in '..\client\QMConst.pas',
  OdHourglass in '..\common\OdHourglass.pas',
  TestStreamHelper in 'TestStreamHelper.pas',
  TestStringObjectList in 'TestStringObjectList.pas',
  ProjAdminDMu in '..\projections\ProjAdminDMu.pas' {ProjAdminDM: TDataModule},
  TestProjAdminDMu in 'TestProjAdminDMu.pas',
  TestUQDbConfig in 'TestUQDbConfig.pas',
  UQDbConfig in '..\common\UQDbConfig.pas';

{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

begin
// Uncomment below to report memory leaks
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  DUnitTestRunner.RunRegisteredTests;
end.

