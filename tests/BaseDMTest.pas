unit BaseDMTest;

interface

uses TestFramework, DMu, StateMgr, SingleEngineTest;

type
  TBaseDMTest = class(TBaseTest)
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  end;

implementation

uses SysUtils;

{ TBaseDMTest }

procedure TBaseDMTest.SetUp;
begin
  DMod := TDM.Create(nil);
  DMod.UQState := TUQState.Create;
  DMod.SetupQMService;
  DMod.TestMode := True;
  inherited;  // get the engine
  DMod.Engine := Engine;  // so we can connect
  DMod.Connect('00235', 'adak13');
end;

procedure TBaseDMTest.TearDown;
begin
  FreeAndNil(DMod);
  inherited;
end;

initialization

finalization

end.

