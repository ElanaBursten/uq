program Wedge;

{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

uses
  Vcl.Forms,
  mainWedge in 'mainWedge.pas' {frmWedge};



begin
  Application.Initialize;
  Application.MainFormOnTaskbar := False;
  Application.ShowMainForm:=False;
  Application.CreateForm(TfrmWedge, frmWedge);
  Application.Run;
end.
