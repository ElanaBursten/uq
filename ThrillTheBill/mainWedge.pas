unit mainWedge;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TfrmWedge = class(TForm)
    Memo1: TMemo;
    btnRun: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
  private
    function ExecuteProcess(const FileName, Params: string; Folder: string;
      WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean;
      var ErrorCode: integer): Boolean;
    procedure ProcessParameters(CurrentDate:TDate);
    { Private declarations }
  public
    { Public declarations }
    period:string;
    centerGroup:string;
    periodEndDate:TDate;
    periodDateStr:string;
    InvoicePath:string;
  end;

var
  frmWedge: TfrmWedge;

implementation

{$R *.dfm}
uses DateUtils, StrUtils;

procedure TfrmWedge.btnRunClick(Sender: TObject);
var
  Params: string;
  ErrorCode: integer;
const
  BILLING_EXE = 'QManagerBilling.exe';
  RUN_CALL='runbilling';
begin
  ErrorCode :=0;
  Params := RUN_CALL+' '+period+' '+periodDateStr+' '+centerGroup+' '+InvoicePath;
  Memo1.Lines.Add(Params);
  ExecuteProcess(BILLING_EXE,Params, '',true,false,false, ErrorCode);
end;

function TfrmWedge.ExecuteProcess(const FileName, Params: string; Folder: string;
  WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean; var ErrorCode: integer): Boolean;
var
  CmdLine: string;
  WorkingDirP: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  Result := true;
  CmdLine := '"' + FileName + '" ' + Params;
  if Folder = '' then
    Folder := ExcludeTrailingPathDelimiter(ExtractFilePath(FileName));
  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(StartupInfo);
  if RunMinimized then
  begin
    StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartupInfo.wShowWindow := SW_SHOWMINIMIZED;
  end;
  if Folder <> '' then
    WorkingDirP := PChar(Folder)
  else
    WorkingDirP := nil;
  if not CreateProcess(nil, PChar(CmdLine), nil, nil, False, 0, nil, WorkingDirP, StartupInfo, ProcessInfo) then
  begin
    Result := False;
    ErrorCode := GetLastError;
    RaiseLastOSError;
    exit;
  end;
  with ProcessInfo do
  begin
    CloseHandle(hThread);
    if WaitUntilIdle then
      WaitForInputIdle(hProcess, INFINITE);
    if WaitUntilTerminated then
      repeat
        Application.ProcessMessages;
      until MsgWaitForMultipleObjects(1, hProcess, False, INFINITE, QS_ALLINPUT) <> WAIT_OBJECT_0 + 1;
    CloseHandle(hProcess);
  end;
end;

procedure TfrmWedge.FormCreate(Sender: TObject);
var
  testDate:TDate;
begin
//  testDate  := StrToDate('3/5/2022');
//  ProcessParameters(testDate);
  ProcessParameters(today);
  btnRunClick(Sender);
  application.terminate;
end;

procedure TfrmWedge.ProcessParameters(CurrentDate:TDate);
var
  monDay:integer;
  Year, Month, Day : word;
begin
  period := paramstr(1);
  centerGroup := paramstr(2);
  InvoicePath := paramstr(3);

  if uppercase(period) ='DAY'
  then
  begin
    periodEndDate := DateOf(CurrentDate);
  end
  else
  if LeftStr(uppercase(period), 4) ='WEEK'  // 'WEEK_A', 'WEEK_B'
  then
  begin
    periodEndDate := DateOf(EndOfTheWeek(CurrentDate));
  end
  else
  if LeftStr(uppercase(period), 5) ='MONTH' // 'MONTH_A', 'MONTH_B']
  then
  begin
    periodEndDate := DateOf(EndOfTheMonth(CurrentDate))+1;
  end
  else
  if uppercase(period) = 'HALF'
  then
  begin
    monDay := DayOfTheMonth(CurrentDate);
    DecodeDate(DateOf(CurrentDate),Year, Month, Day);
    if (monDay>16) then  //15th
    begin
        Day :=16;
        periodEndDate := EncodeDate(Year, Month, Day);
    end
    else
    begin
      if Day<=16 then
      begin
        Day:=1;
        periodEndDate := EncodeDate(Year, Month, Day);
      end
      else
      periodEndDate := DateOf(EndOfTheMonth(CurrentDate))+1;
    end;
  end
  else
  if uppercase(period) = 'HALF2'
  then
  begin
    periodEndDate := DateOf(EndOfTheMonth(CurrentDate))+1; //qm-640
  end;

  periodDateStr := FormatDateTime('yyyy-mm-dd', periodEndDate);
  Memo1.Lines.add(period+' '+ DateTimeToStr(periodEndDate));
  Memo1.Lines.add(period +'string '+ periodDateStr);
end;

end.
