object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'NiSource Importer'
  ClientHeight = 571
  ClientWidth = 847
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 847
    Height = 25
    Align = alTop
    TabOrder = 0
  end
  object pnlBody: TPanel
    Left = 0
    Top = 25
    Width = 847
    Height = 460
    Align = alClient
    TabOrder = 1
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 845
      Height = 458
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 485
    Width = 847
    Height = 67
    Align = alBottom
    TabOrder = 2
    object Label1: TLabel
      Left = 375
      Top = 18
      Width = 50
      Height = 13
      Caption = 'How Many'
    end
    object Label2: TLabel
      Left = 2
      Top = 48
      Width = 63
      Height = 13
      Caption = 'Connected '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 154
      Top = 48
      Width = 42
      Height = 13
      Caption = 'Version'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 256
      Top = 48
      Width = 44
      Height = 13
      Caption = 'Process'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 451
      Top = 48
      Width = 60
      Height = 13
      Caption = 'Start Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 572
      Top = 48
      Width = 60
      Height = 13
      Caption = 'Done Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 512
      Top = 18
      Width = 40
      Height = 13
      Caption = 'Op Area'
    end
    object Label8: TLabel
      Left = 704
      Top = 48
      Width = 86
      Height = 13
      Caption = 'Operating Area'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnProcess: TButton
      Left = 121
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Process'
      TabOrder = 0
      OnClick = btnProcessClick
    end
    object btnOpen: TButton
      Left = 40
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Open'
      TabOrder = 1
      OnClick = btnOpenClick
    end
    object btnAddTicket: TButton
      Left = 202
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Add to Ticket'
      TabOrder = 2
      OnClick = btnAddTicketClick
    end
    object edtHowMany: TEdit
      Left = 431
      Top = 10
      Width = 57
      Height = 21
      NumbersOnly = True
      TabOrder = 3
      Text = '0'
    end
    object cbUseCount: TCheckBox
      Left = 298
      Top = 14
      Width = 73
      Height = 17
      Caption = 'Use Count'
      TabOrder = 4
    end
    object btnHotStop: TBitBtn
      Left = 726
      Top = 6
      Width = 75
      Height = 25
      Caption = ' Hot Stop'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btnHotStopClick
    end
    object cbOperArea: TComboBox
      Left = 560
      Top = 10
      Width = 145
      Height = 21
      TabOrder = 6
      OnChange = cbOperAreaChange
      OnCloseUp = cbOperAreaCloseUp
      Items.Strings = (
        '<<ALL>>'
        'Fort Wayne Locates_220'
        'Goshen Locates_110'
        'Peru Locates_180'
        'Plymouth Locates_070'
        'South Bend Locates_060'
        'Angola Locates_090')
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 552
    Width = 847
    Height = 19
    Panels = <
      item
        Text = 'Not Connected'
        Width = 150
      end
      item
        Text = 'Unknown'
        Width = 100
      end
      item
        Text = 'Process'
        Width = 200
      end
      item
        Text = 'Start Time'
        Width = 120
      end
      item
        Text = 'TimeFinish'
        Width = 120
      end
      item
        Text = 'OpArea'
        Width = 50
      end>
  end
  object OpenTextFileDialog1: TOpenTextFileDialog
    Filter = 'Comma Delimited|*.csv'
    Left = 48
    Top = 160
  end
  object ADOConn: TADOConnection
    LoginPrompt = False
    Provider = 'SQLNCLI11'
    Left = 40
    Top = 57
  end
  object insRecord: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'service_number'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'status'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'customer_first_name'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'customer_middle_initial'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'customer_last_name'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'rural_route_address_number'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'street_number'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'street_name'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'street_direction'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'street_suffix'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'apartment_number'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'lot_number'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'subdivision'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'city'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'zip_code'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'tax_district'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'grid_number'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'township'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'county'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'location_description'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'gis_id'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'maximo_assetnum'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'unlocatable'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'no_access_to_site'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'comments'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'card_created'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'lat'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'lon'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'operating_area'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'USE [QM]'
      ''
      'INSERT INTO [dbo].[nisource_source_staging]'
      '           ([service_number]'
      '           ,[status]'
      '           ,[customer_first_name]'
      '           ,[customer_middle_initial]'
      '           ,[customer_last_name]'
      '           ,[rural_route_address_number]'
      '           ,[street_number]'
      '           ,[street_name]'
      '           ,[street_direction]'
      '           ,[street_suffix]'
      '           ,[apartment_number]'
      '           ,[lot_number]'
      '           ,[subdivision]'
      '           ,[city]'
      '           ,[zip_code]'
      '           ,[tax_district]'
      '           ,[grid_number]'
      '           ,[township]'
      '           ,[county]'
      '           ,[location_description]'
      '           ,[gis_id]'
      '           ,[maximo_assetnum]'
      '           ,[unlocatable]'
      '           ,[no_access_to_site]'
      '           ,[comments]'
      '           ,[card_created]'
      '           ,[lat]'
      '           ,[lon]'
      '           ,[needs_Conversion]'
      '           ,[converted]'
      '           ,[operating_area])'
      '     VALUES'
      '           ('
      '            :service_number'
      '           ,:status'
      '           ,:customer_first_name'
      '           ,:customer_middle_initial'
      '           ,:customer_last_name'
      '           ,:rural_route_address_number'
      '           ,:street_number'
      '           ,:street_name'
      '           ,:street_direction'
      '           ,:street_suffix'
      '           ,:apartment_number'
      '           ,:lot_number'
      '           ,:subdivision'
      '           ,:city'
      '           ,:zip_code'
      '           ,:tax_district'
      '           ,:grid_number'
      '           ,:township'
      '           ,:county'
      '           ,:location_description'
      '           ,:gis_id'
      '           ,:maximo_assetnum'
      '           ,:unlocatable'
      '           ,:no_access_to_site'
      '           ,:comments'
      '           ,:card_created'
      '           ,:lat'
      '           ,:lon'
      '           ,1'
      '           ,0'
      '           ,:operating_area'
      '           )'
      ''
      ''
      '')
    Left = 176
    Top = 57
  end
  object insTicket: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticket_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'parsed_ok'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'ticket_format'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'kind'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'map_page'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'revision'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'transmit_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'due_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_description'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3500
        Value = Null
      end
      item
        Name = 'work_state'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'work_county'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_city'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_address_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'work_address_number_2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'work_address_street'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end
      item
        Name = 'work_cross'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'work_subdivision'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'work_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 90
        Value = Null
      end
      item
        Name = 'work_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_notc'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_remarks'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1200
        Value = Null
      end
      item
        Name = 'priority'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'legal_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'legal_good_thru'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'legal_restake'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'respond_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'duration'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'company'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 80
        Value = Null
      end
      item
        Name = 'con_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'con_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'con_address'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'con_city'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'con_state'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'con_zip'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'call_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'caller'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'caller_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'caller_phone'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'caller_cellular'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'caller_fax'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'caller_altcontact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'caller_altphone'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'caller_email'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'operator'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'channel'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'work_lat'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 6
        Precision = 9
        Size = 19
        Value = Null
      end
      item
        Name = 'work_long'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 6
        Precision = 9
        Size = 19
        Value = Null
      end
      item
        Name = 'image'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'parse_errors'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'modified_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'active'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'ticket_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 38
        Value = Null
      end
      item
        Name = 'legal_due_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'parent_ticket_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'do_not_mark_before'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'route_area_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'watch_and_protect'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'service_area_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'explosives'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'serial_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'map_ref'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end
      item
        Name = 'followup_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'do_not_respond_before'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'recv_manager_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ward'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'source'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = Null
      end
      item
        Name = 'update_of'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'alert'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'work_priority_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'work_extent'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'geocode_precision'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'wo_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'boring'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'alt_due_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'route_order'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 5
        Precision = 9
        Size = 19
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'INSERT INTO [dbo].[ticket]'
      '           ('
      '           [ticket_number]'
      '           ,[parsed_ok]'
      '           ,[ticket_format]'
      '           ,[kind]'
      '           ,[status]'
      '           ,[map_page]'
      '           ,[revision]'
      '           ,[transmit_date]'
      '           ,[due_date]'
      '           ,[work_description]'
      '           ,[work_state]'
      '           ,[work_county]'
      '           ,[work_city]'
      '           ,[work_address_number]'
      '           ,[work_address_number_2]'
      '           ,[work_address_street]'
      '           ,[work_cross]'
      '           ,[work_subdivision]'
      '           ,[work_type]'
      '           ,[work_date]'
      '           ,[work_notc]'
      '           ,[work_remarks]'
      '           ,[priority]'
      '           ,[legal_date]'
      '           ,[legal_good_thru]'
      '           ,[legal_restake]'
      '           ,[respond_date]'
      '           ,[duration]'
      '           ,[company]'
      '           ,[con_type]'
      '           ,[con_name]'
      '           ,[con_address]'
      '           ,[con_city]'
      '           ,[con_state]'
      '           ,[con_zip]'
      '           ,[call_date]'
      '           ,[caller]'
      '           ,[caller_contact]'
      '           ,[caller_phone]'
      '           ,[caller_cellular]'
      '           ,[caller_fax]'
      '           ,[caller_altcontact]'
      '           ,[caller_altphone]'
      '           ,[caller_email]'
      '           ,[operator]'
      '           ,[channel]'
      '           ,[work_lat]'
      '           ,[work_long]'
      '           ,[image]'
      '           ,[parse_errors]'
      '           ,[modified_date]'
      '           ,[active]'
      '           ,[ticket_type]'
      '           ,[legal_due_date]'
      '           ,[parent_ticket_id]'
      '           ,[do_not_mark_before]'
      '           ,[route_area_id]'
      '           ,[watch_and_protect]'
      '           ,[service_area_code]'
      '           ,[explosives]'
      '           ,[serial_number]'
      '           ,[map_ref]'
      '           ,[followup_type_id]'
      '           ,[do_not_respond_before]'
      '           ,[recv_manager_id]'
      '           ,[ward]'
      '           ,[source]'
      '           ,[update_of]'
      '           ,[alert]'
      '           ,[work_priority_id]'
      '           ,[work_extent]'
      '           ,[geocode_precision]'
      '           ,[wo_number]'
      '           ,[boring]'
      '           ,[alt_due_date]'
      '           ,[route_order]'
      '           )'
      '     VALUES'
      '           ('
      #9#9'        :ticket_number'
      '           ,:parsed_ok'
      '           ,:ticket_format'
      '           ,:kind'
      '           ,:status'
      '           ,:map_page'
      '           ,:revision'
      '           ,:transmit_date'
      '           ,:due_date'
      '           ,:work_description'
      '           ,:work_state'
      '           ,:work_county'
      '           ,:work_city'
      '           ,:work_address_number'
      '           ,:work_address_number_2'
      '           ,:work_address_street'
      '           ,:work_cross'
      '           ,:work_subdivision'
      '           ,:work_type'
      '           ,:work_date'
      '           ,:work_notc'
      '           ,:work_remarks'
      '           ,:priority'
      '           ,:legal_date'
      '           ,:legal_good_thru'
      '           ,:legal_restake'
      '           ,:respond_date'
      '           ,:duration'
      '           ,:company'
      '           ,:con_type'
      '           ,:con_name'
      '           ,:con_address'
      '           ,:con_city'
      '           ,:con_state'
      '           ,:con_zip'
      '           ,:call_date'
      '           ,:caller'
      '           ,:caller_contact'
      '           ,:caller_phone'
      '           ,:caller_cellular'
      '           ,:caller_fax'
      '           ,:caller_altcontact'
      '           ,:caller_altphone'
      '           ,:caller_email'
      '           ,:operator'
      '           ,:channel'
      '           ,:work_lat'
      '           ,:work_long'
      '           ,:image'
      '           ,:parse_errors'
      '           ,:modified_date'
      '           ,:active'
      '           ,:ticket_type'
      '           ,:legal_due_date'
      '           ,:parent_ticket_id'
      '           ,:do_not_mark_before'
      '           ,:route_area_id'
      '           ,:watch_and_protect'
      '           ,:service_area_code'
      '           ,:explosives'
      '           ,:serial_number'
      '           ,:map_ref'
      '           ,:followup_type_id'
      '           ,:do_not_respond_before'
      '           ,:recv_manager_id'
      '           ,:ward'
      '           ,:source'
      '           ,:update_of'
      '           ,:alert'
      '           ,:work_priority_id'
      '           ,:work_extent'
      '           ,:geocode_precision'
      '           ,:wo_number'
      '           ,:boring'
      '           ,:alt_due_date'
      '           ,:route_order'
      '           )')
    Left = 280
    Top = 57
  end
  object insLocate: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticket_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'client_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'client_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'status'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'high_profile'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'high_profile_reason'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'qty_marked'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'closed'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'closed_by_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'closed_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'modified_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'active'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'invoiced'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'assigned_to'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'added_by'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'assigned_to_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'status_changed_by_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'INSERT INTO [dbo].[locate]'
      '           ('
      '             ticket_id'
      '            ,client_code'
      '            ,client_id'
      '            ,status'
      '            ,high_profile'
      '            ,high_profile_reason'
      '            ,qty_marked'
      '            ,closed'
      '            ,closed_by_id'
      '            ,closed_date'
      '            ,modified_date'
      '            ,active'
      '            ,invoiced'
      '            ,assigned_to'
      '            ,added_by'
      '            ,assigned_to_id'
      '            ,status_changed_by_id'
      '           )'
      '     VALUES'
      '           ('
      '             :ticket_id'
      '            ,:client_code'
      '            ,:client_id'
      '            ,:status'
      '            ,:high_profile'
      '            ,:high_profile_reason'
      '            ,:qty_marked'
      '            ,:closed'
      '            ,:closed_by_id'
      '            ,:closed_date'
      '            ,:modified_date'
      '            ,:active'
      '            ,:invoiced'
      '            ,:assigned_to'
      '            ,:added_by'
      '            ,:assigned_to_id'
      '            ,:status_changed_by_id'
      '           )')
    Left = 384
    Top = 57
  end
  object getNextManNumber: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      ' use QM'
      ' declare @NextRawNumber int;'
      ' set @NextRawNumber =  (select [dbo].[GetNextManTicketNumber]())'
      ''
      
        ' select  '#39'MAN-1851-'#39'+cast(@NextRawNumber as varchar(12)) as Tick' +
        'et_number, @NextRawNumber as RawNumber')
    Left = 496
    Top = 57
  end
  object qryNiSourceData: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'OpArea'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Declare @OpArea varchar(4);'
      'set @OpArea = :OpArea'
      'select *'
      'from nisource_source_staging'
      'where converted = 0'
      'and needs_conversion= 1'
      'and '#39'All'#39'= @OpArea'
      'or operating_area = @OpArea'
      'order by stageID desc')
    Left = 176
    Top = 161
  end
  object qryTicket: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticketNumber'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'Select *'
      'From ticket'
      'where ticket_number = :ticketNumber'
      'and ticket_format = '#39'1851'#39
      '')
    Left = 280
    Top = 136
  end
  object updNiSourceData: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'TicketID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'stageID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      '  Update nisource_source_staging'
      '  set ticketId = :TicketID,'
      '      needs_Conversion = 0,'
      #9'    converted = 1,'
      '      converted_date = getDate()'
      '  where stageId = :stageID')
    Left = 176
    Top = 208
  end
  object updTicketImage: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'tktImage'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'ticketID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'update ticket'
      'set image = :tktImage'
      'where ticket_id = :ticketID')
    Left = 280
    Top = 208
  end
  object qryDupServiceNumber: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ServiceNumber'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'select service_number'
      'FROM [QM].[dbo].[nisource_source_staging]'
      'where service_number = :ServiceNumber')
    Left = 176
    Top = 112
  end
  object updDateConfigurationData: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'NewTicketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Update configuration_data  '
      'set value =  :NewTicketID'
      'where name = '#39'NextNewTicketID'#39)
    Left = 416
    Top = 264
  end
  object qryLastLocate: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'TicketID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select locate_id '
      'from locate'
      'where ticket_id = :TicketID')
    Left = 384
    Top = 120
  end
  object spAssignLocate: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'assign_locate'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocateID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LocatorID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@AddedBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@WorkloadDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 384
    Top = 193
  end
  object qryCheckForLocate: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ticketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      'from locate'
      'where ticket_id = :ticketID')
    Left = 200
    Top = 305
  end
  object qryServiceNumber: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ServiceNumber'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      'from ticket'
      'where ticket_type='#39'NIPSCO SERVICE CARD'#39
      'and wo_number = :ServiceNumber')
    Left = 80
    Top = 305
  end
end
