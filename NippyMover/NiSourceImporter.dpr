program NiSourceImporter;
{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}
uses
  Vcl.Forms,
  uMain in 'uMain.pas' {frmMain};

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
