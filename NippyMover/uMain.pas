unit uMain;     // qm-254

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.ComCtrls, WinAPI.Activex,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtDlgs, Vcl.Buttons;
type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning);
  TSeverity = (sEmerging, sSerious, sCritical, sYouGottaBeShittingMe);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[90];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;
type
  TfrmMain = class(TForm)
    pnlTop: TPanel;
    pnlBody: TPanel;
    pnlBottom: TPanel;
    OpenTextFileDialog1: TOpenTextFileDialog;
    Memo1: TMemo;
    ADOConn: TADOConnection;
    insRecord: TADOQuery;
    btnProcess: TButton;
    btnOpen: TButton;
    btnAddTicket: TButton;
    insTicket: TADOQuery;
    insLocate: TADOQuery;
    getNextManNumber: TADOQuery;
    qryNiSourceData: TADOQuery;
    qryTicket: TADOQuery;
    updNiSourceData: TADOQuery;
    updTicketImage: TADOQuery;
    qryDupServiceNumber: TADOQuery;
    updDateConfigurationData: TADOQuery;
    edtHowMany: TEdit;
    Label1: TLabel;
    cbUseCount: TCheckBox;
    qryLastLocate: TADOQuery;
    spAssignLocate: TADOStoredProc;
    StatusBar1: TStatusBar;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    btnHotStop: TBitBtn;
    qryCheckForLocate: TADOQuery;
    qryServiceNumber: TADOQuery;
    cbOperArea: TComboBox;
    Label7: TLabel;
    Label8: TLabel;
    procedure btnOpenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnProcessClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnAddTicketClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnHotStopClick(Sender: TObject);
    procedure cbOperAreaCloseUp(Sender: TObject);
    procedure cbOperAreaChange(Sender: TObject);
  private
     LogResult: TLogResults;
     FilePath  :string;
     csvFileName  :string;
     OpArea : string;
    function OpenFile: String;
    function ConnectDB: boolean;
    function AddLocateRecord(ticketID: integer): boolean;
    function DataSetToXML(DataSet: TDataSet): AnsiString;
    procedure WriteLog;
    procedure clearLogRecord;
    procedure ProcessSheet;
    procedure ProcessTicket;
    { Private declarations }
  public
    { Public declarations }
    fileData: TStrings;
    HotStop: boolean;
     excelPage: TFilename;
     ToProcess:integer;
    function GetAppVersionStr: string;
  end;
    function GetGeoCode(StrAddress: Shortstring): Shortstring; stdcall; external 'Geocode.dll';
// select  'MAN-1851-'+cast([dbo].[GetNextManTicketNumber]() as varchar(12)) as Ticket_number
var
  frmMain: TfrmMain;

implementation
uses StrUtils,System.IniFiles;
{$R *.dfm}

procedure TfrmMain.btnAddTicketClick(Sender: TObject);
begin
  CoInitialize(nil);
  TThread.CreateAnonymousThread(
    procedure
    begin
      ProcessTicket;
    end).Start;
end;

procedure TfrmMain.ProcessTicket;
const
  WORK_TYPE = 'LOCATE GAS SERVICE';

  NISOURCE_STATUS = 'MANUAL APPROVED';
  TICKET_TYPE = 'NIPSCO SERVICE CARD';
  CON_NAME = 'UTILIQUEST';
  WORK_DESCRIP = 'SERVICE CARD FOR ';

var
  ManTktNo: string;
  SvrCardNo: string;
  myYear, myMonth, myDay: Word;
  plus_two_Year: TDate;
  ticketID, locateID: integer;
  ticketImage: string;
  RawNumber :integer;
  cnt, howmany:integer;
begin
  try
      StatusBar1.Panels[2].Text := 'Adding Tickets';
      StatusBar1.Panels[3].Text := TimeToStr(Time);
      Memo1.Clear;
      DecodeDate(NOW, myYear, myMonth, myDay);
      plus_two_Year := EncodeDate(myYear + 2, myMonth, myDay);
      qryNiSourceData.Close;
      qryNiSourceData.Parameters.ParamByName('OpArea').Value:= OpArea;
      qryNiSourceData.Open;

      cnt:=0;
      while not qryNiSourceData.eof do
      begin
        try
          inc(cnt);
          Adoconn.BeginTrans;
          getNextManNumber.Close;
          getNextManNumber.Open;
          RawNumber := -1;
          ManTktNo := '';
          ManTktNo  := getNextManNumber.FieldByName('Ticket_number').AsString;
          RawNumber := getNextManNumber.FieldByName('RawNumber').AsInteger;
          getNextManNumber.Close;
          SvrCardNo := qryNiSourceData.FieldByName('service_number').AsString;

          insTicket.Parameters.ParamByName('ticket_number').Value := ManTktNo;
          insTicket.Parameters.ParamByName('parsed_ok').Value := 1;
          insTicket.Parameters.ParamByName('ticket_format').Value := '1851';
          insTicket.Parameters.ParamByName('kind').Value := 'NORMAL';
          insTicket.Parameters.ParamByName('status').Value := NISOURCE_STATUS;
          insTicket.Parameters.ParamByName('map_page').Value :=
            qryNiSourceData.FieldByName('gis_id').AsString;
          insTicket.Parameters.ParamByName('revision').Value := 'MANUAL';
          insTicket.Parameters.ParamByName('transmit_date').Value := NOW;
          insTicket.Parameters.ParamByName('due_date').Value := plus_two_Year;
          insTicket.Parameters.ParamByName('legal_due_date').Value := plus_two_Year;
          insTicket.Parameters.ParamByName('work_description').Value := WORK_DESCRIP
            + SvrCardNo;
          insTicket.Parameters.ParamByName('work_state').Value := 'IN';
          insTicket.Parameters.ParamByName('work_county').Value :=
            qryNiSourceData.FieldByName('county').AsString;
          insTicket.Parameters.ParamByName('work_city').Value :=
            qryNiSourceData.FieldByName('city').AsString;
          insTicket.Parameters.ParamByName('work_address_number').Value :=
            qryNiSourceData.FieldByName('street_number').AsString;
          insTicket.Parameters.ParamByName('work_address_number_2').Value :=
            qryNiSourceData.FieldByName('apartment_number').AsString + ' ' +
            qryNiSourceData.FieldByName('lot_number').AsString;
          insTicket.Parameters.ParamByName('work_address_street').Value :=
            qryNiSourceData.FieldByName('street_name').AsString + ' ' +
            qryNiSourceData.FieldByName('street_suffix').AsString;
          insTicket.Parameters.ParamByName('work_cross').Value := '';
          insTicket.Parameters.ParamByName('work_type').Value := WORK_TYPE;
          insTicket.Parameters.ParamByName('work_remarks').Value :=
            qryNiSourceData.FieldByName('maximo_assetnum').AsString;
          insTicket.Parameters.ParamByName('company').Value := TICKET_TYPE;
          insTicket.Parameters.ParamByName('con_name').Value := CON_NAME;
          insTicket.Parameters.ParamByName('call_date').Value := NOW;
          insTicket.Parameters.ParamByName('caller_contact').Value :=
            qryNiSourceData.FieldByName('customer_first_name').AsString + ' ' +
            qryNiSourceData.FieldByName('customer_last_name').AsString;
          insTicket.Parameters.ParamByName('channel').Value := '3512';
          if not qryNiSourceData.FieldByName('lat').IsNull then
          insTicket.Parameters.ParamByName('work_lat').Value :=
            qryNiSourceData.FieldByName('lat').AsFloat;

          if not qryNiSourceData.FieldByName('lon').IsNull then
          insTicket.Parameters.ParamByName('work_long').Value :=
            qryNiSourceData.FieldByName('lon').AsFloat;
          insTicket.Parameters.ParamByName('image').Value := '';
          insTicket.Parameters.ParamByName('active').Value := 1;
          insTicket.Parameters.ParamByName('modified_date').Value := NOW;

          insTicket.Parameters.ParamByName('ticket_type').Value := TICKET_TYPE;

          insTicket.Parameters.ParamByName('recv_manager_id').Value := 21687;
          insTicket.Parameters.ParamByName('wo_number').Value := SvrCardNo;

          Assert(insTicket.ExecSQL > 0, 'Failed to add Ticket');

          try
            qryTicket.Close;
            qryTicket.Parameters.ParamByName('ticketNumber').Value := ManTktNo;
            qryTicket.Open;
            ticketID := -1;
            ticketID := qryTicket.FieldByName('ticket_id').AsInteger;
            ticketImage := '';
            ticketImage := DataSetToXML(qryTicket);

            updTicketImage.Parameters.ParamByName('tktImage').Value := ticketImage;
            updTicketImage.Parameters.ParamByName('ticketID').Value := ticketID;
            updTicketImage.ExecSQL;
          finally
            qryTicket.Close;
          end;

          try
            qryCheckForLocate.Parameters.ParamByName('TicketID').Value := ticketID;
            qryCheckForLocate.Open;
            If qryCheckForLocate.IsEmpty then
               Assert(AddLocateRecord(ticketID), 'Failed to add Locate');
          finally
            qryCheckForLocate.close;
          end;

          updNiSourceData.Parameters.ParamByName('TicketID').Value := ticketID;
          updNiSourceData.Parameters.ParamByName('stageID').Value :=
            qryNiSourceData.FieldByName('stageID').AsInteger;
          updNiSourceData.ExecSQL;

          Memo1.Lines.Add('update NiSource Data for ticket: '+IntToStr(ticketID));
          LogResult.LogType := ltInfo;
          LogResult.MethodName := 'AddTicket';
          LogResult.Status:= 'Added ticket '+ManTktNo+' to ticket table';
          WriteLog;
          qryNiSourceData.Next;
          updDateConfigurationData.Parameters.ParamByName('NewTicketID').Value := RawNumber;
          updDateConfigurationData.ExecSQL;
          AdoConn.CommitTrans;
          memo1.Lines.Add(TimetoStr(time)+' '+IntToStr(cnt)+' Committed Ticket No number: '+ManTktNo+ ' added in Ticket Table');
          howmany := strToInt(edtHowMany.text);
          if (cbUseCount.Checked) and (cnt>howmany-1) then
          begin
            Memo1.lines.Add('Reached assigned limit of '+edtHowmany.text);
            exit;
          end;
          sleep(10);
          if HotStop then
          begin
            memo1.Lines.Add('Ticket Add Process Hot Stopped');
            HotStop := false;
            Exit;
          end;
          if cnt mod 100 = 0 then application.ProcessMessages;
        except
          on E: Exception do
          begin
            AdoConn.RollbackTrans;
            LogResult.LogType := ltError;
            LogResult.MethodName := 'AddTicket';
            LogResult.ExcepMsg := e.Message;
            Memo1.Lines.Add( 'AddTicket failed-Rolling back '+ManTktNo+': '+LogResult.ExcepMsg );
            WriteLog;
            continue;
          end;
        end;
      end;
      Memo1.Lines.Add('Finished processing NiSource data');
  finally
     StatusBar1.Panels[4].Text := TimeToStr(Time);
     CoUnInitialize;
  end;
end;


procedure TfrmMain.btnHotStopClick(Sender: TObject);
begin
  HotStop:=True;
end;

function TfrmMain.DataSetToXML(DataSet: TDataSet): AnsiString;
const
  ROOT = 'NewTicket';
var
  i: integer;
  function MakeTag(TagName, Value: String): string;
  begin
    result := '<' + TagName + '>' + Value + '</' + TagName + '>';
  end;

begin
  try
    result := '';
    if (not DataSet.Active) or (DataSet.IsEmpty) then
      Exit;
    result := result + '<' + ROOT + '>';
    DataSet.First;
    while not DataSet.eof do
    begin
      result := result + '<Record>';
      for i := 0 to DataSet.Fields.Count - 1 do
      begin
        if ((DataSet.Fields[i].Value <> null) or
          (DataSet.Fields[i].DisplayName <> 'image')) then
          result := result + MakeTag(DataSet.Fields[i].DisplayName,
            DataSet.Fields[i].Text);
      end;
      result := result + '</Record>';
      DataSet.Next;
    end;
    result := result + '</' + ROOT + '>';
  finally
    Memo1.Lines.Add('Created XML image');
  end;
end;

function TfrmMain.AddLocateRecord(ticketID: integer): boolean;
var
  locateID: integer;
begin
  try
    locateID:=0;
    with insLocate.Parameters do
    begin
      ParamByName('ticket_id').Value := ticketID;
      ParamByName('client_code').Value := 'SVC';
      ParamByName('client_id').Value := 4834;
      ParamByName('status').Value := '-R';
      ParamByName('high_profile').Value := 0;
      ParamByName('high_profile_reason').Value := 0;
      ParamByName('qty_marked').Value := 0;
      ParamByName('closed').Value := 0;
      ParamByName('closed_by_id').Value := 3512;
      ParamByName('closed_date').Value := NOW;
      ParamByName('modified_date').Value := NOW;
      ParamByName('active').Value := 1;
      ParamByName('invoiced').Value := 0;
      ParamByName('assigned_to').Value := 21687;
      ParamByName('added_by').Value := '3512';
      ParamByName('assigned_to_id').Value := 21687;
      ParamByName('status_changed_by_id').Value := 21687;
    end;
    try
      If insLocate.ExecSQL > 0 then
      begin
        qryLastLocate.Parameters.ParamByName('ticketID').Value := ticketID;
        qryLastLocate.Open;
        locateID := qryLastLocate.FieldByName('locate_id').AsInteger;

        spAssignLocate.Parameters.ParamByName('@LocateID').Value := locateID;
        spAssignLocate.Parameters.ParamByName('@LocatorID').Value := '21687';
        spAssignLocate.Parameters.ParamByName('@AddedBy').Value := 3512;
        spAssignLocate.Parameters.ParamByName('@WorkloadDate').Value := NOW;
        spAssignLocate.ExecProc;
        result:=true;
      end;

    finally
      qryLastLocate.Close;
    end;

  except
    on E: Exception do
    begin
      LogResult.LogType := ltError;
      LogResult.MethodName := 'AddLocateRecord';
      LogResult.DataStream := insLocate.SQL.text;
      LogResult.ExcepMsg := 'Ticket: ' + IntToStr(ticketID) + ' ' + E.Message;
      Memo1.Lines.Add(LogResult.ExcepMsg);
      WriteLog;
      result:=false;
    end;
  end;
end;

procedure TfrmMain.btnOpenClick(Sender: TObject);
begin
  OpenFile;

  self.Caption := self.Caption+'  '+ csvFileName;
end;

procedure TfrmMain.FormActivate(Sender: TObject);
begin
  HotStop:=False;
  StatusBar1.Panels[1].Text :=  GetAppVersionStr;
  cbOperArea.ItemIndex := 0;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  ConnectDB;
  fileData := TStringList.Create;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  fileData.Free;
  ADOConn.Close;
end;

function TfrmMain.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
  begin
    showMessage(SysErrorMessage(GetLastError));
    RaiseLastOSError;
  end;

  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

function TfrmMain.OpenFile: String;
var
  cnt: integer;
begin
  if OpenTextFileDialog1.Execute(self.Handle) then
  begin
    try
      excelPage   := OpenTextFileDialog1.FileName;
      FilePath    := ExtractFilePath(excelPage);
      csvFileName := ExtractFileName(excelPage);

      fileData.LoadFromFile(excelPage);
      cnt := fileData.Count;
    except
      On E: Exception do
        result := E.Message;
    end;

  end;
end;

procedure TfrmMain.WriteLog;
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
  SEP = '\';
  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, now);

  LogName := 'NiSource_'+ FormatDateTime('yyyy-mm-dd', Date) + FILE_EXT;

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);
  if LogResult.MethodName <> '' then
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  if LogResult.ExcepMsg <> '' then
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  if LogResult.Status <> '' then
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  if LogResult.DataStream <> '' then
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TfrmMain.cbOperAreaChange(Sender: TObject);
begin
  StatusBar1.Panels[5].Text := cbOperArea.text;
end;

procedure TfrmMain.cbOperAreaCloseUp(Sender: TObject);
begin
  case cbOperArea.ItemIndex of
    0: OpArea := 'All';
    1: OpArea := '220';
    2: OpArea := '110';
    3: OpArea := '180';
    4: OpArea := '070';
    5: OpArea := '060';
    6: OpArea := '090';
  end;
end;

procedure TfrmMain.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TfrmMain.btnProcessClick(Sender: TObject);
begin
  CoInitialize(nil);
  TThread.CreateAnonymousThread(
    procedure
    begin
      ProcessSheet;
    end).Start;
end;


procedure TfrmMain.ProcessSheet;
const
  SPC = ' ';
  REM_LIST = 'NiSourceCleanUp.txt';
var
  myFile: TextFile;
  i,cnt,howmany: integer;
  tmpLine: TStringList;
  stAddress: string;
  LatLng, sLat, sLng: String;
  savedPath:string;
  excludeStatusList:TStringList;  //QM-280
  //  0          1                2                   3                     4                   5          6
  // STATUS	SERVICE_NUMBER	CUSTOMER_FIRST_NAME	CUSTOMER_MIDDLE_INITIAL	CUSTOMER_LAST_NAME	RURAL_ROUTE	ADDRESS_NUMBER
  //        7              8           9           10                11             12      13     14        15
  // STREET_DIRECTION	STREET_NAME	STREET_SUFFIX	APARTMENT_NUMBER	LOT_NUMBER	SUBDIVISION	CITY	ZIP_CODE	TAX_DISTRICT
  //       16           17    18           19              20       21               22            23           24
  // GRID_NUMBER	TOWNSHIP	COUNTY	LOCATION_DESCRIPTION	GISID	MAXIMO_ASSETNUM	UNLOCATABLE	NO_ACCESS_TO_SITE	COMMENTS
  //
  // OPERATING_AREA
  procedure GeoCodeAddress(sAddress: string);
  begin
    sLat:='';
    sLng:='';
    if sAddress <> '' then
    begin
      LatLng := GetGeoCode(sAddress);
      if LeftStr(LatLng, 7) = '[ERROR]' then
      begin
        sLat := '0';
        sLng := '0';
      end
      else
      begin
        sLat := copy(LatLng, 0, pos(',', LatLng) - 1);
        sLng := copy(LatLng, (pos(',', LatLng) + 1), maxChar);
      end;

    end;
  end;
begin
  howmany :=0;
  cnt := 0;
  i := 0;
  ToProcess := 0;
  StatusBar1.Panels[2].Text := 'Loading '+csvFileName;
  StatusBar1.Panels[3].Text := TimeToStr(Time);
  try
    excludeStatusList:= TStringList.Create;  //QM-280
    excludeStatusList.Add('condemned');
    excludeStatusList.Add('rmvd');
    excludeStatusList.Add('decommissioned');
    tmpLine := TStringList.Create;
    tmpLine.Delimiter := #44;
    tmpLine.StrictDelimiter := true;
    ToProcess:=  fileData.Count-1;
    self.Caption := self.Caption+' Records to process:  '+IntToStr(ToProcess);

    if FileExists(REM_LIST) then
    begin
      AssignFile(myFile, REM_LIST);
      Append(myFile);
      writeLn(myFile, StatusBar1.Panels[2].text+'  ------------------------------------');
    end
    else
    begin
      AssignFile(myFile, REM_LIST);
      Rewrite(myFile);
    end;

  try
      for i := 1 to fileData.Count-1 do  //skip header record
      begin
         tmpLine.DelimitedText := fileData[i];
         qryDupServiceNumber.close;
         qryDupServiceNumber.Parameters.ParamByName('ServiceNumber').Value := StrToInt(tmpLine[1]);
         qryDupServiceNumber.open;
         if not qryDupServiceNumber.IsEmpty then
         begin
            qryDupServiceNumber.close;
            LogResult.LogType := ltWarning;
            LogResult.MethodName := 'Duplicate Check';
            LogResult.Status := tmpLine[1]+ ' already in Nisource Table';
            WriteLog;
            memo1.Lines.Add(TimetoStr(time)+' Service number: '+tmpLine[1]+ ' already in Nisource Table');
            writeLn(myFile, tmpLine[1]+',');
            howmany := strToInt(edtHowMany.text);
            if (cbUseCount.Checked) and (cnt>howmany-1) then
            begin
              Memo1.lines.Add('Reached assigned limit of '+edtHowmany.text);
              exit;
            end;
            continue
         end;

         stAddress:='';
                      // num           st. name                         city                  zip
         stAddress  :=tmpLine[6] +SPC+ tmpLine[8]+SPC+tmpLine[9]+#44+ tmpLine[13]+#44+'IN'+SPC+tmpLine[14];
         try
           GeoCodeAddress(stAddress);
         except
         on E : exception do
         begin
          LogResult.LogType := ltError;
          LogResult.MethodName := 'GeoCodeAddress';
          LogResult.DataStream := stAddress;
          LogResult.ExcepMsg := e.Message;
          WriteLog;
         end;
         end;

         if sLat<>'0' then
         insRecord.Parameters.ParamByName('lat').Value  :=  sLat;

         if sLng<>'0' then
         insRecord.Parameters.ParamByName('lon').Value  :=  sLng;


  		   insRecord.Parameters.ParamByName('service_number').Value  :=  StrToInt(tmpLine[1]);
         if excludeStatusList.IndexOf(tmpLine[0])>-1 then //QM-280
         insRecord.Parameters.ParamByName('status').Value  := NULL
         else
         insRecord.Parameters.ParamByName('status').Value  := tmpLine[0];
         insRecord.Parameters.ParamByName('customer_first_name').Value  :=  tmpLine[2];
         insRecord.Parameters.ParamByName('customer_middle_initial').Value  :=  tmpLine[3];
         insRecord.Parameters.ParamByName('customer_last_name').Value  :=  tmpLine[4];
         if tmpLine[5]<>'' then
         insRecord.Parameters.ParamByName('rural_route_address_number').Value  :=  tmpLine[5];
         insRecord.Parameters.ParamByName('street_number').Value  :=  tmpLine[6];
         insRecord.Parameters.ParamByName('street_name').Value  :=  tmpLine[8];
         insRecord.Parameters.ParamByName('street_direction').Value  :=  tmpLine[7];
         insRecord.Parameters.ParamByName('street_suffix').Value  :=  tmpLine[9];
         insRecord.Parameters.ParamByName('apartment_number').Value  :=  tmpLine[10];
         insRecord.Parameters.ParamByName('lot_number').Value  :=  tmpLine[11];
         insRecord.Parameters.ParamByName('subdivision').Value  :=  tmpLine[12];
         insRecord.Parameters.ParamByName('city').Value  :=  tmpLine[13];
         insRecord.Parameters.ParamByName('zip_code').Value  :=  tmpLine[14];
         insRecord.Parameters.ParamByName('tax_district').Value  :=  tmpLine[15];
         insRecord.Parameters.ParamByName('grid_number').Value  :=  tmpLine[16];
         insRecord.Parameters.ParamByName('township').Value  :=  tmpLine[17];
         insRecord.Parameters.ParamByName('county').Value  :=  tmpLine[18];
         insRecord.Parameters.ParamByName('location_description').Value  :=  tmpLine[19];
         insRecord.Parameters.ParamByName('gis_id').Value  :=  tmpLine[20];
         insRecord.Parameters.ParamByName('maximo_assetnum').Value  :=  tmpLine[21];
         insRecord.Parameters.ParamByName('unlocatable').Value  :=  tmpLine[22];
         insRecord.Parameters.ParamByName('no_access_to_site').Value  :=  tmpLine[23];
         insRecord.Parameters.ParamByName('comments').Value  :=  tmpLine[24];
         insRecord.Parameters.ParamByName('card_created').Value  :=  NULL;    //qm-278
         insRecord.Parameters.ParamByName('OPERATING_AREA').Value  :=  tmpLine[25];

         insRecord.ExecSQL;
         inc(cnt);
         memo1.Lines.Add(TimetoStr(time)+' '+IntToStr(cnt)+' Service number: '+tmpLine[1]+ ' added in Nisource Table');
         tmpLine.Clear;
          if HotStop then
          begin
            memo1.Lines.Add('Nisource Staging Process Hot Stopped');
            HotStop := false;
            Exit;
          end;
         if (cnt mod 10 = 0) then
         application.ProcessMessages;
          howmany := strToInt(edtHowMany.text);
          if (cbUseCount.Checked) and (cnt>howmany-1) then
          begin
            Memo1.lines.Add('Reached assigned limit of '+edtHowmany.text);
            exit;
          end;

      end;
  except on E: Exception do
    begin
      memo1.Lines.add(tmpLine.Commatext);
      memo1.Lines.add(insRecord.SQL.text);
      LogResult.LogType := ltError;
      LogResult.MethodName := 'ProcessRecord';
      LogResult.DataStream := insRecord.SQL.text;
      LogResult.ExcepMsg := e.Message;
      WriteLog;
    end;
  end;
  finally
    CloseFile(myFile);
    StatusBar1.Panels[4].Text := TimeToStr(Time);
    tmpLine.Free;
    excludeStatusList.Free;
    qryDupServiceNumber.close;
    memo1.Lines.add(IntToStr(i)+' NiSource records added');
    ForceDirectories(IncludeTrailingBackSlash(FilePath)+'Done\');
    savedPath:= IncludeTrailingBackSlash(FilePath)+'Done\'+csvFileName;
    If not cbUseCount.Checked then
    begin
      if MoveFile(PChar(excelPage), PChar(savedPath))
      then
      memo1.Lines.add(excelPage+'  moved to '+savedPath);
    end;
    CoUnInitialize;
  end;

end;

function TfrmMain.connectDB: boolean;
var
  myPath:string;
  connStr, LogConnStr: String;
  aDatabase: String;
  aServer: String;
  ausername: string;
  apassword: string;
  BillingIni: TIniFile;

begin
  Result := false;
  myPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
  try
    BillingIni := TIniFile.Create(myPath + 'QManagerBilling.ini');
//    connStr := BillingIni.ReadString('Database', 'ConnectionString', '');   //local machine

    aServer := BillingIni.ReadString('Database', 'Server', '');
    aDatabase := BillingIni.ReadString('Database', 'DB', 'QM');
    ausername := BillingIni.ReadString('Database', 'UserName', '');
    apassword := BillingIni.ReadString('Database', 'Password', '');
    LogConnStr := connStr;

    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    memo1.Lines.Add(LogConnStr);
    ADOConn.ConnectionString := connStr;
    ADOConn.Open;
    Result := ADOConn.Connected;

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'Connect DB';
    LogResult.DataStream := connStr;
    WriteLog;

    if result then
    begin
      memo1.Lines.Add('Connected to database');
      StatusBar1.Panels[0].Text := aServer;
    end
    else
      memo1.Lines.Add('Could not connect to DB');
  finally
    freeandnil(BillingIni);
  end;
end;



end.
