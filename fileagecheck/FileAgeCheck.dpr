program FileAgeCheck;

{$APPTYPE CONSOLE}

{$R '..\QMIcon.res'}
{$R '..\QMVersion.res' '..\QMVersion.rc'}

uses
  SysUtils,
  IdMessage,
  IdSMTP;

var
  FileIsThere: Boolean;
  FileAgeInMinutes: Integer;

  FileToCheck: string;
  Threshold: Integer;
  EmailDest, EmailFrom, SmtpServer: string;

procedure ReadConfig;
begin
  FileToCheck := ParamStr(1);
  Threshold := StrToInt(ParamStr(2));
  EmailDest := ParamStr(3);
  EmailFrom := ParamStr(4);
  SmtpServer := ParamStr(5);
end;

procedure PrintUsage;
begin
  WriteLn('Usage: Filename Minutes EmailDest EmailFrom SMTPServer');
  WriteLn('Example: \\server\c$\some\file.txt 10 joe@hotmail.com server@mycomputer.com mail.myisp.com');
end;

procedure GetFileAge;
var
  FileDateTime: TDateTime;
  NowDate: TDateTime;
begin
  FileAgeInMinutes := 0;
  FileIsThere := FileExists(FileToCheck);
  if not FileIsThere then
    Exit;

  FileAge(FileToCheck, FileDateTime);
  NowDate := Now;

  WriteLn('File Date   : ' + DateTimeToStr(FileDateTime));
  WriteLn('Current Date: ' + DateTimeToStr(NowDate));

  FileAgeInMinutes := Round( (Now - FileDateTime) * 24.0 * 60.0 );
  WriteLn('Age (min.)  : ' + IntToStr(FileAgeInMinutes));
end;

procedure SetWarningEmail(Msg: string);
var
  SMTP: TIdSMTP;
  Notify: TIdMessage;
begin
  WriteLn('Sending email message...');
  WriteLn('File:    ' + FileToCheck);
  WriteLn('Message: ' + Msg);
  WriteLn('To:      ' + EmailDest);
  WriteLn('Server:  ' + SmtpServer);


  SMTP := TIdSMTP.Create(nil);
  Notify := TIdMessage.Create(nil);

  Notify.Subject := FileToCheck + ' - ' + Msg;
  Notify.Body.text := FileToCheck + ' - ' + Msg;
  Notify.From.Text := EmailFrom;

  Notify.Recipients.EMailAddresses := EmailDest;

  SMTP.host := SmtpServer;
  SMTP.Connect;
  SMTP.Send(Notify);
  SMTP.Disconnect;
end;

begin
  try
    WriteLn('File Age Check');
    WriteLn('Oasis Digital Solutions Inc.');

    if ParamCount<>5 then begin
      PrintUsage;
      Exit;
    end;

    ReadConfig;

    try
      GetFileAge;

      if not FileIsThere then
        SetWarningEmail('file does not exist!')

      else if FileAgeInMinutes > Threshold then
        SetWarningEmail('file is older than ' + IntToStr(Threshold) + ' minutes!');
    except
      on E: Exception do
        SetWarningEmail('Error occurred while checking file: ' + E.Message);
    end;
  except
    on E: Exception do
      WriteLn(E.Message);
  end;
end.
