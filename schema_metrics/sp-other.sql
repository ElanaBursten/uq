
if object_id('dbo.IdListToTable') is not null
	drop function dbo.IdListToTable
go

create function IdListToTable(@S varchar(8000))
returns @Result table (ID int not null)
as
begin
  declare @SBgn int
  declare @SEnd int

  set @SBgn = 1
  set @SEnd = 1

  while (@SBgn <= len(@S)) begin
    /* find the next comma */
    while (@SEnd <= len(@S)) and (substring(@S, @SEnd, 1) <> ',')
      set @SEnd = @SEnd + 1

    /* convert substring to integer and add result to the table */
    insert into @Result(ID)
    select cast(substring(@S, @SBgn, @SEnd-@SBgn) as int)

    /* skip over the comma */
    set @SEnd = @SEnd + 1
    set @SBgn = @SEnd
  end

  return
end
go


if object_id('dbo.StringListToTable') is not null
	drop function dbo.StringListToTable
go

create function StringListToTable(@S varchar(8000))
returns @Result table (S varchar(80) not null)
as
begin
  declare @SBgn int
  declare @SEnd int

  set @SBgn = 1
  set @SEnd = 1

  while (@SBgn <= len(@S)) begin
    /* find the next comma */
    while (@SEnd <= len(@S)) and (substring(@S, @SEnd, 1) <> ',')
      set @SEnd = @SEnd + 1

    /* add substring to the table */
    insert into @Result(S)
    select substring(@S, @SBgn, @SEnd-@SBgn)

    /* skip over the comma */
    set @SEnd = @SEnd + 1
    set @SBgn = @SEnd
  end

  return
end
go



if object_id('dbo.DateSpan') is not null
	drop function dbo.DateSpan
go

create function DateSpan(
	@StartDate datetime,
	@EndDate datetime
)
returns @Result table (D datetime not null)
as
begin
  declare @x datetime
  set @x = @StartDate
  while (@x <= @EndDate) begin
    insert into @Result(D) values (@x)
    set @x = dateadd(d, 1, @x)
  end
  return
end
go


if object_id('dbo.TimeSpan') is not null
 drop function dbo.TimeSpan
go
create function TimeSpan(
 @StartTime datetime,
 @EndTime datetime,
 @Granularity varchar(5) -- HOUR,MIN,SEC
)
returns @Result table (T datetime not null)
as
begin
  declare @x datetime
  set @x = @StartTime
  while (@x <= @EndTime) begin
    insert into @Result(T) values (@x)
    if @Granularity = 'HOUR' 
      set @x = dateadd(HOUR, 1, @x)
    else if @Granularity = 'MIN'
	  set @x = dateadd(MINUTE, 1, @x)
	else if @Granularity = 'SEC'
	  set @x = dateadd(SECOND, 1, @x)
  end
  return
end

