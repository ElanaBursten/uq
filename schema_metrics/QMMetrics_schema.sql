/* Utiliquest Q Manager Metrics Database Create Script */

/* KEEP THIS SCRIPT up to date manually - do not replace it with
   a generated script.  This script should contain comments that explain
   the meaning behind the tables, fields, relationships, etc.  It is the
   canonical specification of the schema.

   Never add new columns in the middle of an existing table definition.
   Our test databases need to match the production server in the
   order of the columns in the physical table, or lots of strange
   problems will occur in triggers and selects into temporary tables.

   We always use the varchar data type instead of char, even if the
   field is a fixed length.
*/

/* ********************************** Domain related tables  ****** */

-- Metric entity: trackable areas of the business... call centers, profit centers, employees, etc.
CREATE TABLE metric_entity (
  ent_cat_code varchar(30) NOT NULL,
  ent_cat_name varchar(30) NOT NULL,
  ent_code varchar(50) NOT NULL,
  ent_name varchar(100) NOT NULL,
  constraint pk_metric_entity primary key clustered (ent_cat_code, ent_code)
)
go

-- Metric class: things that can be tracked... tickets received, tickets closed, last ticket received, response queue length, etc
CREATE TABLE metric_class (
  metric_code varchar(10) NOT NULL,
  metric_name varchar(100) NOT NULL,
  metric_type varchar(10) NOT NULL, -- flow, state
  constraint pk_metric_class primary key clustered (metric_code)
)
go

-- Metric granularity: day, hour, minute, second
CREATE TABLE metric_granularity (
  gran_code varchar(10) NOT NULL,
  gran_name varchar(100) NOT NULL,
  constraint pk_metric_granularity primary key clustered (gran_code)
)
go

-- Metrics data
CREATE TABLE metric_data (
  metric_data_id uniqueidentifier NOT NULL DEFAULT NewSequentialID(),
  ent_cat_code varchar(30) NOT NULL,
  ent_code varchar(50) NOT NULL,
  metric_code varchar(10) NOT NULL,
  total bit NOT NULL DEFAULT 0,
  as_of datetime NOT NULL,
  data_point int NOT NULL,
  constraint pk_metric_data primary key nonclustered (metric_data_id)
)
go
CREATE CLUSTERED INDEX metric_data_i1 ON metric_data(as_of, ent_cat_code, ent_code, metric_code)
CREATE INDEX metric_data_i2 ON metric_data(as_of, metric_code, ent_cat_code, ent_code)
go

-- Choices, based on what data is present
CREATE TABLE metric_choice (
  metric_choice_id uniqueidentifier NOT NULL DEFAULT NewSequentialID(),
  ent_cat_code varchar(30) NOT NULL,
  ent_cat_name varchar(30) NOT NULL,
  ent_code varchar(50) NOT NULL,
  ent_name varchar(100) NOT NULL,
  metric_code varchar(10) NOT NULL,
  metric_name varchar(100) NOT NULL,
  metric_type varchar(10) NOT NULL,
  gran_code varchar(10) NOT NULL,
  gran_name varchar(100) NOT NULL,
  total bit NOT NULL,
  oldest_date datetime NULL,
  newest_date datetime NULL,
  dataURL varchar(99) NOT NULL,
  constraint pk_metric_choice primary key nonclustered (metric_choice_id)
)
GO
CREATE UNIQUE CLUSTERED INDEX metric_choice_i1 ON metric_choice(dataURL)


-- Employee hierarchy data (partially duplicated from OLTP QM db)
CREATE TABLE hierarchy (
  node_id int NOT NULL,
  node_name varchar(50) NULL,
  report_to_node_id int NULL,
  [depth] int NULL,
  constraint pk_hierarchy primary key clustered (node_id)
)
GO

-- Flat employee list
CREATE TABLE employee (
  emp_id int NOT NULL,
  short_name varchar(50) NULL,
  report_to int NULL,
  constraint pk_employee primary key clustered (emp_id)
)
GO

-- Employee hierarchy closure table
CREATE TABLE eclos (
  pid int NOT NULL,
  cid int NOT NULL,
  distance int NOT NULL,
  constraint pk_eclos primary key clustered (pid, cid)
)
GO

-- Hierarchy closure table
CREATE TABLE hclos (
  pid int NOT NULL,
  cid INT NOT NULL,
  distance INT NOT NULL,
  constraint pk_hclos primary key clustered (pid, cid)
)
GO

-- Employee workload by emp, due date, and due hour
CREATE TABLE emp_workload (
  emp_id int NOT NULL,
  due_date date NOT NULL,
  due_hour int NOT NULL,
  tickets_due int NULL,
  locates_due int NULL,
  damages_due int NULL,
  work_orders_due int NULL,
  constraint pk_emp_workload primary key clustered (emp_id, due_date, due_hour)
)
GO

-- List of nodes (employees) that emp_id can see based on "Tickets - Management" right Limitation.
CREATE TABLE emp_allowed_node (
  emp_id int NOT NULL,
  node_id int NOT NULL,
  distance int NOT NULL,
  can_manage bit NOT NULL,
  constraint pk_emp_allowed_node primary key clustered (emp_id, node_id)
)
GO

-- Ticket cache
CREATE TABLE ticket (
  ticket_id int NOT NULL,
  ticket_number varchar(20) NOT NULL,
  ticket_format varchar(20) NOT NULL,
  ticket_type varchar(38) NULL,
  kind varchar(20) NOT NULL,
  transmit_date datetime NOT NULL,
  due_date datetime NOT NULL,
  parent_ticket_id int NULL,
  source varchar(12) NULL,
  constraint PK_ticket_ticketid primary key clustered (ticket_id))
GO

-- Locate cache (ticket info is duplicated to avoid joins)
CREATE TABLE locate (
  locate_id int NOT NULL,
  ticket_id int NOT NULL,
  ticket_number varchar(20) NOT NULL,
  ticket_format varchar(20) NOT NULL,
  ticket_type varchar(38) NULL,
  kind varchar(20) NOT NULL,
  transmit_date datetime NOT NULL,
  due_date datetime NOT NULL,
  parent_ticket_id int NULL,
  source varchar(12) NULL,
  assigned_to int NULL,
  client_id integer NOT NULL,
  client_code varchar(10) NULL,
  client_name varchar(40) NOT NULL,
  status varchar(5) NOT NULL,
  closed BIT NOT NULL,
  closed_by_id integer NULL,
  status_date datetime NULL,
  status_changed_by_id int NULL,
  constraint PK_locate_locateid primary key nonclustered (locate_id))
GO

CREATE UNIQUE CLUSTERED INDEX locate_i1 ON locate(ticket_id, locate_id)
GO

CREATE TABLE configuration_data (
  name varchar(50) NOT NULL,
  value varchar(1000) NOT NULL,
  modified_date datetime NOT NULL,
  constraint PK_configuration_data primary key clustered (name))
GO

if not exists (select * from master.dbo.syslogins where name = 'uqweb')
begin
  execute sp_addlogin 'uqweb', 'uqweb_password_zzz55'
end
go
if not exists (select * from master.dbo.syslogins where name = 'no_trigger')
begin
  execute sp_addlogin 'no_trigger', 'no_trigger_password_zzz55'
end
go

exec sp_grantdbaccess 'uqweb'
go
-- Logging in as this user make s the DB skip running a few table change triggers
-- Search the schema files for no_trigger for full details
exec sp_grantdbaccess 'no_trigger'
go

sp_addrole 'QManagerRole'
go
sp_addrolemember 'QManagerRole', 'uqweb'
go
sp_addrolemember 'db_datareader', 'uqweb'
go
sp_addrolemember 'db_datawriter', 'uqweb'
go
