if exists (select ROUTINE_NAME from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'get_metric_end_date') 
  drop function dbo.get_metric_end_date
go

create function get_metric_end_date (
  @date_start datetime,
  @gran_code varchar(10)
)

returns datetime
as
begin
  declare @date_end datetime
  -- for hour / minute / sec, the end date is the end of the day
  if @gran_code = 'SEC' or @gran_code = 'MIN' or @gran_code = 'HOUR'
    set @date_end = DATEADD(SECOND, -1, DATEADD(DAY, DATEDIFF(DAY, 0, @date_start)+1, 0)) 
  else
	-- otherwise it is the current sever time
    set @date_end = GETDATE()
  
  return @date_end
end
go
