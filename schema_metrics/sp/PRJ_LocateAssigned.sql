if object_id('dbo.PRJ_LocateAssigned') is not null
  drop procedure dbo.PRJ_LocateAssigned
go

create procedure PRJ_LocateAssigned(@LocateID integer,
  @ToLocatorID integer
)
as

set nocount on

declare @FromLocatorID integer
declare @DueDate datetime
declare @UpdateTicketCnt bit
declare @DueHour integer
declare @DueDateOnly date
declare @TicketID integer

-- Get some cached locate values
select @DueDate = due_date, 
  @FromLocatorID = assigned_to,
  @TicketID = ticket_id
from locate 
where locate_id = @LocateID

set @DueHour = DatePart(Hour, @DueDate)
set @DueDateOnly = Convert(date, @DueDate)

-- Update ticket counts if none of the locates for the ticket are assigned to @ToLocatorID
if not exists (select locate_id from locate where ticket_id = @TicketID and assigned_to = @ToLocatorID)
  set @UpdateTicketCnt = 1
else
  set @UpdateTicketCnt = 0

-- Increment @ToLocatorID's workload counts  
if not exists (select emp_id from emp_workload 
  where emp_id = @ToLocatorID and due_date = @DueDateOnly and due_hour = @DueHour)
  insert into emp_workload(emp_id, due_date, due_hour, locates_due, tickets_due) 
  values (@ToLocatorID, @DueDateOnly, @DueHour, 1, 1)
else
  update emp_workload set locates_due = coalesce(locates_due, 0) + 1, 
    tickets_due = coalesce(tickets_due, 0) + convert(integer, @UpdateTicketCnt)
  where emp_id = @ToLocatorID
    and due_date = @DueDateOnly 
    and due_hour = @DueHour  

-- Decrement @FromLocatorID's workload counts  
update emp_workload set locates_due = coalesce(locates_due, 0) - 1, 
  tickets_due = coalesce(tickets_due, 0) - convert(integer, @UpdateTicketCnt)
where emp_id = @FromLocatorID 
  and due_date = @DueDateOnly 
  and due_hour = @DueHour 

-- Update the locate cache  
update locate set assigned_to = @ToLocatorID where locate_id = @LocateID

go

grant execute on PRJ_LocateAssigned to uqweb, QManagerRole

/*
exec PRJ_LocateAssigned 1234, 3512
exec PRJ_LocateAssigned 1234, 811
*/
