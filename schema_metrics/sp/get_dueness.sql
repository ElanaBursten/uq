if object_id('dbo.get_dueness') is not null
  drop function dbo.get_dueness
go

create function get_dueness(
  @CurrTime datetime, 
  @DueDate date,
  @DueHour integer
)
returns varchar(15) 
as begin

declare @DueTime datetime
declare @Dueness varchar(15)
set @DueTime = DateAdd(hour, @DueHour, Convert(datetime, @DueDate, 120))
select @Dueness = case 
  when @DueDate is null then null
  when DateDiff(hour, @CurrTime, Convert(datetime, @DueTime, 120)) < 0 then '1-Late'
  when DateDiff(hour, @CurrTime, Convert(datetime, @DueTime, 120)) <= 1 then '2-Soon'
  when DateDiff(day,  @CurrTime, Convert(datetime, @DueTime, 120)) = 0 then '3-Today'
  when DateDiff(day,  @CurrTime, Convert(datetime, @DueTime, 120)) = 1 then '4-Tomorrow'
  when DateDiff(day,  @CurrTime, Convert(datetime, @DueTime, 120)) = 2 then '5-NextDay'
  else '6-Later'
end

return (@Dueness)

end

go

/*
declare @CurrTime datetime    = '2014-03-20T14:17:33.910'
declare @DueEarlier datetime  = '2014-03-20T11:00:00.000'
declare @DueNow datetime      = '2014-03-20T14:17:33.910'
declare @DueSoon datetime     = '2014-03-20T15:00:00.000'
declare @DueToday datetime    = '2014-03-20T17:17:33.000'
declare @DueTomorrow datetime = '2014-03-21T15:00:00.333'
declare @DueNextDay datetime  = '2014-03-22T23:59:59.000'
declare @DueFuture datetime   = '2014-03-25T10:20:45.543'

select @CurrTime CurrTime, @DueEarlier DueTime, dbo.get_dueness(@CurrTime, @DueEarlier, 11) Dueness
union select @CurrTime CurrTime, @DueNow DueTime, dbo.get_dueness(@CurrTime, @DueNow, 14) Dueness
union select @CurrTime CurrTime, @DueSoon DueTime, dbo.get_dueness(@CurrTime, @DueSoon, 15) Dueness
union select @CurrTime CurrTime, @DueToday DueTime, dbo.get_dueness(@CurrTime, @DueToday, 17) Dueness
union select @CurrTime CurrTime, @DueTomorrow DueTime, dbo.get_dueness(@CurrTime, @DueTomorrow, 15) Dueness
union select @CurrTime CurrTime, @DueNextDay DueTime, dbo.get_dueness(@CurrTime, @DueNextDay, 23) Dueness
union select @CurrTime CurrTime, @DueFuture DueTime, dbo.get_dueness(@CurrTime, @DueFuture, 10) Dueness
*/