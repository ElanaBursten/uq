if object_id('dbo.PRJ_LocateReceived') is not null
  drop procedure dbo.PRJ_LocateReceived
go

create procedure PRJ_LocateReceived(@LocateID int,
  @ClientID int,
  @ClientCode varchar(10),
  @ClientName varchar(40),
  @Status varchar(5),
  @Closed bit,
  @StatusDate datetime,
  @TicketID int,
  @TicketNumber varchar(20),
  @TicketFormat varchar(20),
  @Source varchar(12),
  @Kind varchar(20),
  @TicketType varchar(38),
  @TransmitDate datetime,
  @DueDate datetime
  )
as

set nocount on

-- TODO: This crashes if @LocateID has multiple TicketReceived events projected
insert into locate (locate_id, client_id, client_code, client_name,
  status, closed, status_date, ticket_id, ticket_number, ticket_format, 
  ticket_type, kind, source, transmit_date, due_date)
values (@LocateID, @ClientID, @ClientCode, @ClientName,
  @Status, @Closed, @StatusDate, @TicketID, @TicketNumber, @TicketFormat, 
  @TicketType, @Kind, @Source, @TransmitDate, @DueDate)

go

grant execute on PRJ_LocateReceived to uqweb, QManagerRole