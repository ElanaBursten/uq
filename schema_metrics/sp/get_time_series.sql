IF OBJECT_ID('dbo.get_time_series') IS NOT NULL
  DROP PROCEDURE dbo.get_time_series
go

CREATE PROCEDURE get_time_series
  @ent_cat_code varchar(30),  
  @ent_code varchar(50),
  @metric_code varchar(10),
  @gran_code varchar(10),
  @date_start datetime,
  @total bit  -- 0=Get data only for one @ent_code; 1=Get data for @ent_code and all under them
WITH RECOMPILE
AS
BEGIN
  SET NOCOUNT ON  
  
  begin try
    if ((@total = 1) and (@ent_cat_code <> 'EMP')) 
      throw 50001, 'Invalid category/total combination', 1 

    if not exists (select ent_cat_code from metric_entity where ent_cat_code = @ent_cat_code and ent_code = @ent_code)
      throw 50001, 'Invalid metric_entity category/entity combination', 1

    if not exists (select metric_code from metric_class where metric_code = @metric_code)
      throw 50001, 'Invalid metric_class metric code', 1

    if not exists (select gran_code from metric_granularity where gran_code = @gran_code)
      throw 50001, 'Invalid metric_granularity gran_code', 1 
  end try

  begin catch
    throw
  end catch


  -- how many characters of the events' date/time string to use for grouping
  declare @date_trim smallint
  if @gran_code = 'SEC' set @date_trim = 19
  if @gran_code = 'MIN' set @date_trim = 16
  if @gran_code = 'HOUR' set @date_trim = 13
  if @gran_code = 'DAY' set @date_trim = 10

  declare @date_end datetime
  set @date_end = dbo.get_metric_end_date(@date_start, @gran_code)

  -- result set 1: metadata lookup
  IF EXISTS (SELECT TOP 1 ent_code
   FROM metric_choice
   WHERE metric_code = @metric_code
    AND ent_cat_code = @ent_cat_code
    AND ent_code = @ent_code
    AND gran_code = @gran_code
    AND (total = @total OR @total = 1))

    SELECT ent_cat_code, ent_code, ent_name,
      metric_code, metric_name, metric_type,
      gran_code, gran_name,
      @date_start as date_start,
      @date_end as date_end
     FROM metric_choice
     WHERE metric_code = @metric_code
      AND ent_cat_code = @ent_cat_code
      AND ent_code = @ent_code
      AND gran_code = @gran_code
      AND (total = @total OR @total = 1)
  ELSE
    SELECT @ent_cat_code as ent_cat_code,
      @ent_code as ent_code, 
      (select ent_name from metric_entity 
       where ent_cat_code = @ent_cat_code 
         and ent_code = @ent_code) as ent_name,
      @metric_code as metric_code, 
      (select metric_name from metric_class
       where metric_code = @metric_code) as metric_name, 
      (select metric_type from metric_class
       where metric_code = @metric_code) as metric_type,
      @gran_code as gran_code, 
      (select gran_name from metric_granularity
       where gran_code = @gran_code) as gran_name, 
      @date_start as date_start,
      @date_end as date_end
  
  declare @details table (
    period_start datetime not null primary key,
    period_end datetime not null,
    value int not null)

  -- result set 2: actual data
  -- one row summed for each granularity over the selected period
  IF @gran_code = 'DAY'
    INSERT @details 
      SELECT span.D, DATEADD(DAY, 1, span.D), 0
      FROM dbo.DateSpan(@date_start, @date_end) span
  ELSE IF @gran_code = 'HOUR'
    INSERT @details 
      SELECT span.T, DATEADD(HOUR, 1, span.T), 0
      FROM dbo.TimeSpan(@date_start, @date_end, @gran_code) span
  ELSE IF @gran_code = 'MIN'
    INSERT @details 
      SELECT span.T, DATEADD(MINUTE, 1, span.T), 0
      FROM dbo.TimeSpan(@date_start, @date_end, @gran_code) span
  ELSE IF @gran_code = 'SEC'
    INSERT @details 
      SELECT span.T, DATEADD(SECOND, 1, span.T), 0
      FROM dbo.TimeSpan(@date_start, @date_end, @gran_code) span

  ;WITH TOTALS AS (
    SELECT MIN(as_of) as_of, 
      COALESCE(SUM(data_point), 0) data_point_sum
    FROM metric_data 
    WHERE (as_of >= @date_start AND as_of < @date_end)
      AND (metric_code = @metric_code)
      AND (ent_cat_code = @ent_cat_code)
      AND (ent_code = @ent_code)
      AND (total = @total OR @total = 1)
    GROUP BY SUBSTRING(CONVERT(VARCHAR(20), as_of, 120), 1, @date_trim)
  )
  UPDATE @details SET value = data_point_sum
  FROM TOTALS 
  WHERE as_of >= period_start AND as_of < period_end
      
  SELECT period_start as_of, value 
  FROM @details 
  ORDER BY 1
END
GO

grant execute on get_time_series to uqweb, QManagerRole

/*
SELECT * FROM metric_choice

exec get_time_series 'CC', 'OCC2', 'EREC', 'HOUR', '2013-04-02', 0
exec get_time_series 'CC', '3003', 'TREC', 'SEC', '2013-04-02', 0

exec get_time_series 'EMP', '811', 'TCLEMP', 'HOUR', '2013-04-02', 0
exec get_time_series 'EMP', '811', 'TCLEMP', 'HOUR', '2013-04-02', 1
exec get_time_series 'EMP', '1239', 'TCLEMP', 'MIN', '2013-04-02', 1
exec get_time_series 'EMP', '2676', 'TCLEMP', 'MIN', '2013-04-02', 1
exec get_time_series 'EMP', '2676', 'TCLEMP', 'HOUR', '2013-04-02', 1
exec get_time_series 'EMP', '2676', 'TCLEMP', 'DAY', '2014-02-02', 1
*/
