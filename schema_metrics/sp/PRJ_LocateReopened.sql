if object_id('dbo.PRJ_LocateReopened') is not null
  drop procedure dbo.PRJ_LocateReopened
go

create procedure PRJ_LocateReopened(@LocateID int,
  @ReopenedByEmpID int,
  @ReopenedDate datetime,
  @ReopenedTicket bit,
  @StatusCode varchar(5)
  )
as

set nocount on

declare @CatCC varchar(30) = 'CC'
declare @CatEmp varchar(30) = 'EMP'
declare @DueHour integer
declare @DueDateOnly date
declare @DueDate datetime
declare @AssignedEmpID int
declare @CallCenter varchar(20)

-- Get some cached locate values
select @DueDate = due_date,
  @AssignedEmpID = assigned_to,
  @CallCenter = ticket_format
from locate
where locate_id = @LocateID

if (@DueDate is null) begin
  -- TODO: Events for manually added tickets cannot be projected currently,
  -- so if the locate is not in our metrics cache, quit.
  return
end

set @DueHour = DatePart(Hour, @DueDate)
set @DueDateOnly = Convert(date, @DueDate)

-- Update the ticket and locate state tables
update locate set
  status = @StatusCode,
  closed = 0,
  status_date = @ReopenedDate
where locate_id = @LocateID

update emp_workload set
  locates_due = coalesce(locates_due, 0) + 1,
  tickets_due = case @ReopenedTicket
    when 1 then coalesce(tickets_due, 0) + 1
    else tickets_due
  end
where emp_id = @AssignedEmpID
  and due_date = @DueDateOnly
  and due_hour = @DueHour

-- LCLOSE metric for the call center
insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
values (@CatCC, @CallCenter, 'LCLOSE', @ReopenedDate, -1, 0)

-- LCLEMP metric for the affected employees
insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
select @CatEmp, pid, 'LCLEMP', @ReopenedDate, -1, case when distance = 0 then 0 else 1 end
  from hclos
 where cid = @ReopenedByEmpID

-- TCLOSE metric for the call center if the ticket is reopened
if (@ReopenedTicket = 1) begin
  insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
  values (@CatCC, @CallCenter, 'TCLOSE', @ReopenedDate, -1, 0)

  -- TCLEMP metric for the affected employees
  insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
  select @CatEmp, pid, 'TCLEMP', @ReopenedDate, -1, case when distance = 0 then 0 else 1 end
    from hclos
    where cid = @ReopenedByEmpID
end;

exec add_metric_choices @CatCC, @CallCenter, @ReopenedDate, 'TCLOSE,LCLOSE', '*'
exec add_metric_choices @CatEmp, @ReopenedByEmpID, @ReopenedDate, 'TCLEMP,LCLEMP', '*'
go

grant execute on PRJ_LocateReopened to uqweb, QManagerRole

/*

exec PRJ_LocateReopened @CallCenter='OCC2', @ReopenedByEmpID=3512, @ReopenedDate='Jan 17, 2014 09:25:33', @ReopenedTicket=1
exec PRJ_LocateReopened @CallCenter='OCC2', @ReopenedByEmpID=3512, @ReopenedDate='Jan 17, 2014 09:25:33', @ReopenedTicket=0

select top 100 * from metric_data
select top 100 * from metric_choice
*/
