if object_id('dbo.add_hierarchy') is not null
  drop procedure dbo.add_hierarchy
go

-- TODO: This contains explicit references to TestDB. To work for UQ, those need to reference a QM db.

create procedure add_hierarchy as 

set nocount on

-- The columns of this intermediate table must match those returned from QM's hier_display3 sp
declare @hier table (
  tname varchar(50) not null,
  emp_tree_id varchar(15) not null,
  emp_id integer not null,
  short_name varchar(30) not null,
  emp_number varchar(15) null, 
  node_name varchar(50) not null,
  type_id integer not null,
  report_level integer not null,
  report_to varchar(10) null, 
  sort integer not null,
  under_me bit not null,
  is_manager bit not null,
  work_status_now varchar(3) not null
)

declare @dist int
declare @hier_root_id varchar(10) = '2676'

-- Reload emp hierarchy from QM's hier_display3 stored proc results
insert into @hier exec TestDB..hier_display3 @hier_root_id, 0
truncate table hierarchy
insert hierarchy
select
  convert(integer, emp_tree_id) as node_id,
  short_name as node_name,
  case 
    when report_to is null or report_to='NULL' then 0
    else convert(integer, report_to) 
  end as report_to_node_id,
  convert(integer, report_level) as [depth]
from @hier
where is_manager = 1

-- Recreate transitive closure on the hierarchy
truncate table hclos
insert into hclos
  select node_id, node_id, 0 from hierarchy

set @dist = 1
while @dist < 20 begin
  insert into hclos
  select uc.pid, u.node_id, @dist
  from hclos uc, hierarchy u
  where uc.cid = u.report_to_node_id
    and uc.distance = @dist - 1
  set @dist = @dist + 1
end

-- bring in employees
truncate table employee
insert employee
select
  convert(integer, emp_id) emp_id,
  short_name,
  report_to
from TestDB..employee
where type_id is not null

-- Recreate transitive closure on employees
truncate table eclos
insert into eclos
  select emp_id, emp_id, 0 from employee

set @dist = 1
while @dist < 20 begin
  insert into eclos
  select uc.pid, u.emp_id, @dist
    from eclos uc, employee u
    where uc.cid = u.report_to
      and uc.distance = @dist - 1
  set @dist = @dist + 1
end

go

grant execute on add_hierarchy to uqweb, QManagerRole

/*
exec add_hierarchy

select top 100 * from hierarchy
select top 100 * from hclos
select top 100 * from employee
select top 100 * from eclos
*/
