if object_id('dbo.add_metric_choices') is not null
  drop procedure dbo.add_metric_choices
go

create procedure add_metric_choices(@EntityCategory varchar(30),
  @EntityCode varchar(50),
  @AsOfDate datetime,
  @MetricCodeList varchar(3000),
  @GranCodeList varchar(3000) -- * means all
)
as

set nocount on

-- Add metric_choice based on what data is present
declare @MetricDate varchar(10)
set @MetricDate = convert(varchar(10), @AsOfDate, 120)

declare @choices table (
  ecat varchar(30),
  ecatname varchar(30),
  ecode varchar(50),
  ename varchar(100),
  mcode varchar(10),
  mname varchar(100),
  mtype varchar(10),
  gcode varchar(10),
  gname varchar(100),
  tot bit,
  asof datetime,
  URL varchar(99))


if @EntityCategory = 'EMP' begin -- include all parents of the emp_id in @EntityCode
  insert into @choices
    select ent_cat_code, ent_cat_name, ent_code, ent_name,
      metric_code, metric_name, metric_type,
      gran_code, gran_name,
      case when ent_code = @EntityCode then 0 else 1 end as tot,
      @AsOfDate,
      '/api/v1/metric/' +
      case when ent_code = @EntityCode then 'single/' else 'total/' end +
      ent_cat_code + '/' + ent_code + '/' + metric_code + '/' + gran_code as URL
    from metric_entity, metric_class, metric_granularity
    where metric_entity.ent_cat_code = @EntityCategory
      and metric_code in (select S from dbo.StringListToTable(@MetricCodeList))
      and (@GranCodeList = '*' or gran_code in (select S from dbo.StringListToTable(@GranCodeList)))
      and ent_code in (select pid from hclos where cid = convert(integer, @EntityCode))
end else begin
  -- Add the Call Center metric_entity if it doesn't exist
  insert into metric_entity (ent_cat_code, ent_cat_name, ent_code, ent_name)
    select @EntityCategory, 'Call Center', @EntityCode, @EntityCode + ' Call Center'
    where not exists (select ent_code from metric_entity
      where ent_cat_code = @EntityCategory and ent_code = @EntityCode)

  insert into @choices
    select ent_cat_code, ent_cat_name, ent_code, ent_name,
      metric_code, metric_name, metric_type,
      gran_code, gran_name,
      0 as tot,
      @AsOfDate,
      '/api/v1/metric/single/' + ent_cat_code + '/' +
      ent_code + '/' + metric_code + '/' + gran_code as URL
    from metric_entity, metric_class, metric_granularity
    where metric_entity.ent_cat_code = @EntityCategory
      and metric_code in (select S from dbo.StringListToTable(@MetricCodeList))
      and (@GranCodeList = '*' or gran_code in (select S from dbo.StringListToTable(@GranCodeList)))
      and ent_code = @EntityCode
end

-- Update any existing metric_choice rows with oldest/newest dates
update metric_choice set
  oldest_date = case when oldest_date > asof then asof else oldest_date end,
  newest_date = case when newest_date < asof then asof else newest_date end
from @choices
where dataURL = URL


-- Now add the ones that are not already there
insert into metric_choice (ent_cat_code, ent_cat_name, ent_code, ent_name, metric_code,
  metric_name, metric_type, gran_code, gran_name, total, dataURL, oldest_date, newest_date)

  select ecat, ecatname, ecode, ename, mcode, mname, mtype, gcode, gname, tot, URL, asof, asof
  from @choices
  where not exists (select dataURL from metric_choice where dataURL = URL)


go

grant execute on add_metric_choices to uqweb, QManagerRole

/*

exec add_metric_choices 'CC', 'OCC2', 'Jan 19, 2014 11:14:59', 'TCLOSE,LCLOSE', '*'
exec add_metric_choices 'EMP', '3510', 'Jan 19, 2014 11:14:59', 'TCLEMP,LCLEMP', '*'

select top 100 * from metric_choice
*/
