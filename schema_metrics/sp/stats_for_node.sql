IF OBJECT_ID('dbo.stats_for_node') IS NOT NULL
  DROP PROCEDURE dbo.stats_for_node
go

/* *** TODO ***

   - Currently this gets all emp_workload rows for the requested nodes b/c 
     the test ticket data has due dates that are all several years old. 
     Either restrict emp_workload by due_date age or add a cleanup process
     to remove old stats.

*/

CREATE PROCEDURE stats_for_node
  @node_id integer,
  @viewer_id integer,
  @as_of_dt datetime
AS
BEGIN
SET NOCOUNT ON

set @as_of_dt = coalesce(@as_of_dt, GetDate())

declare @allowed table (node_id int not null primary key, can_manage bit not null)
insert @allowed select node_id, can_manage from dbo.get_emp_allowed_nodes(@viewer_id)

select 'node.due.' +
    coalesce(dbo.get_dueness(@as_of_dt, due_date, due_hour), '1-Late') label,
  convert(integer, NULL) node_id,
  convert(varchar(50), NULL) as short_name,
  NULL as image,
  NULL as clocked_in,
  sum(coalesce(tickets_due, 0)) N
from eclos 
inner join employee on employee.emp_id=@node_id
inner join @allowed a on employee.emp_id = a.node_id
left join emp_workload due on eclos.cid=due.emp_id
where eclos.pid = @node_id 
  and a.can_manage = 1
-- Assume emp_workload stays small
--   and (due.due_date > DateAdd(day, -60, @as_of_dt) or due.due_date is null)
group by coalesce(dbo.get_dueness(@as_of_dt, due_date, due_hour), '1-Late') 

UNION ALL

select 'node.due.NA' label,
  convert(integer, NULL) node_id,
  convert(varchar(50), NULL) as short_name,
  NULL as image,
  NULL as clocked_in,
  0 as N
from eclos 
inner join employee on employee.emp_id=@node_id
inner join @allowed a on employee.emp_id = a.node_id
where eclos.pid = @node_id 
  and a.can_manage = 0

UNION ALL 

select 'node' label,
  @node_id node_id,
  MAX(employee.short_name) as short_name,
  -- TODO: image should be a ref to an image of the employee
  NULL as image, 
  -- TODO: clocked_in needs to come from QM.dbo.get_current_work_status(node_id)
  0 as clocked_in,
  NULL
from employee
join @allowed a on employee.emp_id = a.node_id
where employee.emp_id=@node_id

UNION ALL

select 'child.'+ convert(varchar(50), employee.emp_id) + '.due.' + 
    coalesce(dbo.get_dueness(@as_of_dt, due_date, due_hour), '1-Late') label,
  NULL,
  NULL,
  NULL,
  NULL,
  sum(coalesce(tickets_due, 0)) N
from employee
inner join @allowed a on a.node_id = employee.emp_id
inner join eclos on eclos.pid = employee.emp_id
left join emp_workload due on (eclos.cid = due.emp_id)
where employee.report_to = @node_id
  and a.can_manage = 1
-- Assume emp_workload stays small
--  and (due.due_date > DateAdd(day, -60, @as_of_dt) or due.due_date is null)
group by employee.emp_id, coalesce(dbo.get_dueness(@as_of_dt, due_date, due_hour), '1-Late') 

UNION ALL

select 'child.'+ convert(varchar(50), employee.emp_id) + '.due.NA' label,
  NULL,
  NULL,
  NULL,
  NULL,
  0 AS N
from employee
inner join @allowed a on a.node_id = employee.emp_id
inner join eclos on eclos.pid = employee.emp_id
where employee.report_to = @node_id
  and a.can_manage = 0

UNION ALL 

select 'child.'+ convert(varchar(50), employee.emp_id) label,
  employee.emp_id node_id,
  employee.short_name,
  -- TODO: image should be a ref to an image of the employee
  NULL as image,
  -- TODO: clocked_in needs to come from QM.dbo.get_current_work_status(node_id)
  0 as clocked_in,
  NULL
from employee
inner join @allowed a on a.node_id = employee.emp_id
where employee.report_to = @node_id
group by employee.emp_id, employee.short_name
order by 1,2,3,4

/* 2nd result set is the selected node's ancestry */
select e.emp_id, e.short_name
from employee e
inner join eclos c on c.pid = e.emp_id
inner join @allowed a on a.node_id = e.emp_id
where c.cid = @node_id
  and c.distance > 0
order by c.distance desc

/* TODO: 2nd result set should be ancestry restricted to only the allowed nodes
select emp_id, short_name
from employee 
inner join @allowed a on a.node_id = emp_id
inner join eclos on pid = emp_id
where cid = @node_id
  and distance > 0
order by distance desc
*/

END


grant execute on stats_for_node to uqweb, QManagerRole
GO

/*
exec stats_for_node 2676, 3512, null
exec stats_for_node 697, 3512, null

exec stats_for_node 697, 811, null
exec stats_for_node 811, 811, null
exec stats_for_node 2263, 811, 'Mar 24, 2014 09:23'
*/
