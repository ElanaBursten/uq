if object_id('dbo.PRJ_LocateClosed') is not null
  drop procedure dbo.PRJ_LocateClosed
go

create procedure PRJ_LocateClosed(@LocateID int, 
  @ClosedByEmpID int,
  @ClosedDate datetime, 
  @CompletedTicket bit,
  @StatusCode varchar(5)
  )
as

set nocount on

declare @CatCC varchar(30) = 'CC'
declare @CatEmp varchar(30) = 'EMP'
declare @DueHour integer
declare @DueDateOnly date
declare @DueDate datetime
declare @AssignedEmpID int
declare @CallCenter varchar(20)

-- Get some cached locate values
select @DueDate = due_date,
  @AssignedEmpID = assigned_to,
  @CallCenter = ticket_format
from locate
where locate_id = @LocateID

if (@DueDate is null) begin
  -- TODO: Events for manually added tickets cannot be projected currently,
  -- so if the locate is not in our metrics cache, quit.
  return
end

set @DueHour = DatePart(Hour, @DueDate)
set @DueDateOnly = Convert(date, @DueDate)

-- Update the ticket and locate state tables
update locate set
  status = @StatusCode,
  closed = 1,
  status_date = @ClosedDate
where locate_id = @LocateID

update ticket set kind = 'DONE'
from locate
where @CompletedTicket = 1
  and locate.ticket_id = ticket.ticket_id
  and locate.locate_id = @LocateID

-- Reduce open locates and tickets counts for the right emp/day/hour bucket
update emp_workload set
  locates_due = locates_due - 1,
  tickets_due = case @CompletedTicket
    when 1 then tickets_due - 1
    else tickets_due
  end
where emp_id = @AssignedEmpID
  and due_date = @DueDateOnly
  and due_hour = @DueHour

-- LCLOSE metric for the call center
insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
values (@CatCC, @CallCenter, 'LCLOSE', @ClosedDate, 1, 0)

-- LCLEMP metric for the affected employees
insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
select @CatEmp, pid, 'LCLEMP', @ClosedDate, 1, case when distance = 0 then 0 else 1 end
  from hclos
 where cid = @ClosedByEmpID

-- TCLOSE metric for the call center if the ticket is completed
if (@CompletedTicket = 1) begin
  insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
  values (@CatCC, @CallCenter, 'TCLOSE', @ClosedDate, 1, 0)

  -- TCLEMP metric for the affected employees
  insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
  select @CatEmp, pid, 'TCLEMP', @ClosedDate, 1, case when distance = 0 then 0 else 1 end
    from hclos
    where cid = @ClosedByEmpID
end;

exec add_metric_choices @CatCC, @CallCenter, @ClosedDate, 'TCLOSE,LCLOSE', '*'
exec add_metric_choices @CatEmp, @ClosedByEmpID, @ClosedDate, 'TCLEMP,LCLEMP', '*'

go

grant execute on PRJ_LocateClosed to uqweb, QManagerRole

/*

exec PRJ_LocateClosed @CallCenter='OCC2', @ClosedByEmpID=3512, @ClosedDate='Jan 17, 2014 09:25:33', @CompletedTicket=1
exec PRJ_LocateClosed @CallCenter='OCC2', @ClosedByEmpID=3512, @ClosedDate='Jan 17, 2014 09:25:33', @CompletedTicket=0

select top 100 * from metric_data
select top 100 * from metric_choice
*/
