if object_id('dbo.PRJ_TicketReceived') is not null
  drop procedure dbo.PRJ_TicketReceived
go

create procedure PRJ_TicketReceived(@TicketID int,
  @TicketNumber varchar(20),
  @TicketFormat varchar(20),
  @Source varchar(12),
  @Kind varchar(20),
  @TicketType varchar(38),
  @TransmitDate datetime,
  @DueDate datetime
  )
as

set nocount on

-- TODO: This crashes if @TicketID has multiple TicketReceived events projected
insert into ticket (ticket_id, ticket_number, ticket_format, ticket_type, kind, 
  source, transmit_date, due_date)
values (@TicketID, @TicketNumber, @TicketFormat, @TicketType, @Kind, 
  @Source, @TransmitDate, @DueDate)

declare @CatCC varchar(30) = 'CC'

-- TREC metric for the call center
insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
values (@CatCC, @TicketFormat, 'TREC', @TransmitDate, 1, 0)

-- EREC metric for the call center if the ticket is an Emergency
if (@Kind = 'EMERGENCY') begin
  insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
  values (@CatCC, @TicketFormat, 'EREC', @TransmitDate, 1, 0)
end;

exec add_metric_choices @CatCC, @TicketFormat, @TransmitDate, 'TREC,EREC', '*'

go

grant execute on PRJ_TicketReceived to uqweb, QManagerRole

/*
exec PRJ_TicketReceived 123456, '2014-123456', 'CC1', 'CC2', 'NORMAL', 'ROUTINE', 'Apr 17, 2014 9:00:00', 'Apr 19, 2014 23:59:59'*/
