if object_id('dbo.add_metric_entities') is not null
  drop procedure dbo.add_metric_entities
go

-- TODO: This contains explicit references to TestDB. To work for UQ, those need to reference a QM db.

create procedure add_metric_entities
as

set nocount on

-- Populate entities from Call Centers in the QM OLTP db
insert into metric_entity
  select 'Call Center' as ent_category, 
    cc_code as ent_code, 
    cc_name as ent_name
  from TestDB..call_center
  where active = 1 
    and cc_code not in (
      select ent_code from metric_entity
      where ent_category = 'Call Center')

-- Populate entities from Employees 
insert into metric_entity
  select 'Employee' as ent_category,
    convert(varchar(20), employee.emp_id) as ent_code,
    employee.short_name as ent_name
  from employee
  where short_name is not null
    and convert(varchar(20), employee.emp_id) not in (
      select ent_code from metric_entity
      where ent_category = 'Employee') 
  order by 2

go

grant execute on add_metric_entities to uqweb, QManagerRole

/*
exec add_metric_entities

select * from metric_entity
*/
