if object_id('dbo.add_ticket_metrics') is not null
  drop procedure dbo.add_ticket_metrics
go

-- TODO: Running this sp multiple times for the same dates inserts duplicate metric_data
-- TODO: This contains explicit references to TestDB. To work for UQ, those need to reference a QM db.

create procedure add_ticket_metrics(@StartDate datetime, @StopDate datetime, @CurrentDate datetime)
as

set nocount on

-- Recreate due_stats (dueness is based on current system date)
truncate table due_stats; -- ";" is needed so the CTE used below will work
with
d (assigned_to, hr_due, day_due, kind, ticket_id) as
  (select l.assigned_to,
     DateDiff(hour, @CurrentDate, Convert(datetime, t.due_date, 120)) as hr,
     DateDiff(day,  @CurrentDate, Convert(datetime, t.due_date, 120)) as day,
     t.kind,
     l.ticket_id
   from TestDB..locate l with(index(locate_assigned_to))
   inner join TestDB..ticket t on l.ticket_id = t.ticket_id
   where l.assigned_to IS NOT NULL),
lness as
  (select
    assigned_to,
    case 
      when hr_due < 0 then '1-Late'
      when hr_due = 1 then '2-Soon'
      when day_due = 0 then '3-Today'
      when day_due = 1 then '4-Tomorrow'
      when day_due = 2 then '5-NextDay'
      else '6-Later' 
    end as dueness,
    ticket_id
  from d)

insert due_stats
  select assigned_to as emp_id, dueness, count(distinct ticket_id) as N
  from lness
  group by assigned_to, dueness
  order by 1,2

-- All tickets received by call center
insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
  select
    'CC',
    ticket.ticket_format as ent_code, -- call center
    'TREC' as metric_code,
    convert(datetime, tv.transmit_date, 120) as as_of,
    1 as data_point,
    0 as total
  from TestDB..ticket_version tv
    inner join TestDB..ticket on tv.ticket_id = ticket.ticket_id
  where tv.transmit_date >= @StartDate
    and tv.transmit_date < @StopDate

-- Emergency tickets received by call center
insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
  select
    'CC',
    ticket.ticket_format as ent_code,
    'EREC' as metric_code,
    convert(datetime, tv.transmit_date, 120) as as_of,
    1 as data_point,
    0 as total
  from TestDB..ticket_version tv
    inner join TestDB..ticket on tv.ticket_id = ticket.ticket_id
  where tv.transmit_date >= @StartDate
    and tv.transmit_date < @StopDate
    and tv.ticket_type like 'EMER%'

-- Tickets closed by call center
insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
  select
    'CC',
    ticket.ticket_format as ent_code,
    'TCLOSE' as metric_code,
    convert(datetime, ts.first_close_date, 120) as as_of,
    1 as data_point,
    0 as total
  from TestDB..ticket_snap ts
    inner join TestDB..ticket on ts.ticket_id = ticket.ticket_id
  where first_close_date >= @StartDate
    and first_close_date < @StopDate

-- Tickets closed, by employee
insert into metric_data (ent_cat_code, ent_code, metric_code, as_of, data_point, total)
  select
    'EMP',
    convert(varchar(10), first_close_emp_id) as ent_code,
    'TCLEMP' as metric_code,
    convert(datetime, ts.first_close_date, 120) as as_of,
    1 as data_point,
    0 as total
  from TestDB..ticket_snap ts
  where first_close_date >= @StartDate
    and first_close_date < @StopDate


-- Reload metric_choice based on what data is present
truncate table metric_choice
insert into metric_choice (ent_cat_code, ent_cat_name, ent_code, ent_name,
  metric_code, metric_name, metric_type, gran_code, gran_name,
  oldest_date, newest_date, total, dataURL)
  select ent_cat_code, ent_cat_name, ent_code, ent_name,
    metric_code, metric_name, metric_type,
    gran_code, gran_name,
    @StartDate as oldest_date,
    @StopDate as newest_date,
    0 as total,
    '/api/v1/metric/single/CC/' + ent_code + '/' + metric_code + '/' +
    gran_code as dataURL
  from metric_entity, metric_class, metric_granularity
  where metric_entity.ent_cat_code = 'CC'
    and metric_code in ('TREC', 'EREC', 'TROUTE', 'RESPQ', 'TCLOSE')

insert into metric_choice (ent_cat_code, ent_cat_name, ent_code, ent_name,
  metric_code, metric_name, metric_type, gran_code, gran_name,
  oldest_date, newest_date, total, dataURL)
  select ent_cat_code, ent_cat_name, ent_code, ent_name,
    metric_code, metric_name, metric_type,
    gran_code, gran_name,
    @StartDate as oldest_date,
    @StopDate as newest_date,
    0 as total,
    '/api/v1/metric/single/EMP/' + ent_code + '/' + metric_code + '/' +
    gran_code as dataURL
  from metric_entity, metric_class, metric_granularity
  where metric_entity.ent_cat_code = 'EMP'
    and metric_code='TCLEMP'
    and gran_code in ('HOUR', 'MIN')

go

grant execute on add_ticket_metrics to uqweb, QManagerRole

/*
delete from metric_data where as_of >= '2013-01-01' and as_of < '2013-11-20'

exec add_ticket_metrics @StartDate='2013-01-01', @StopDate='2013-11-20', @CurrentDate='2013-11-20T10:15:00.000'
exec add_ticket_metrics '2013-05-11T00:00:00.000', '2013-05-12T00:00:00.000', GetDate()

select top 100 * from metric_data
select top 100 * from metric_choice
*/
