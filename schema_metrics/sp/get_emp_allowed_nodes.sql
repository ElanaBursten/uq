if object_id('dbo.get_emp_allowed_nodes') is not null
  drop function dbo.get_emp_allowed_nodes
go

create function get_emp_allowed_nodes(@EmpID int) 
returns @Result table (distance int not null, node_id int not null, can_manage bit not null)
as 
begin

insert into @Result 
  select distance, node_id, can_manage
  from emp_allowed_node
  where emp_id = @EmpID
return

end

/*
select * from dbo.get_emp_allowed_nodes(811)
select * from dbo.get_emp_allowed_nodes(3512) order by distance

select * from dbo.get_emp_allowed_nodes(5116)
*/