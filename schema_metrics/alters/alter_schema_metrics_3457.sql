-- Add new DAY metric granularity code to the metrics database
if not exists (select gran_code from metric_granularity where gran_code = 'DAY') 
  insert metric_granularity values ('DAY', 'Day')
  
-- Add new TimeSpan SQL function
if object_id('dbo.TimeSpan') is not null
 drop function dbo.TimeSpan
go
create function TimeSpan(
 @StartTime datetime,
 @EndTime datetime,
 @Granularity varchar(5) -- HOUR,MIN,SEC
)
returns @Result table (T datetime not null)
as
begin
  declare @x datetime
  set @x = @StartTime
  while (@x <= @EndTime) begin
    insert into @Result(T) values (@x)
    if @Granularity = 'HOUR' 
      set @x = dateadd(HOUR, 1, @x)
    else if @Granularity = 'MIN'
      set @x = dateadd(MINUTE, 1, @x)
    else if @Granularity = 'SEC'
      set @x = dateadd(SECOND, 1, @x)
  end
  return
end

/* Also be sure to deploy these 3 updated stored procedure scripts in the metrics db:
PRJ_LocateClosed.sql
PRJ_LocateReopened.sql
get_time_series.sql
stats_for_node.sql
*/
