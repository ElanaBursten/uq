-- create emp_workload table
if not exists (select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'emp_workload') 
  CREATE TABLE emp_workload (
    emp_id int NOT NULL,
    due_date date NOT NULL,
    due_hour int NOT NULL,
    tickets_due int NULL,
    locates_due int NULL,
    damages_due int NULL,
    work_orders_due int NULL,
    constraint pk_emp_workload primary key clustered (emp_id, due_date, due_hour)
  )
GO

-- create emp_allowed_node table
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'emp_allowed_node')
  CREATE TABLE emp_allowed_node (
    emp_id int NOT NULL,
    node_id int NOT NULL,
    distance int NOT NULL,
    can_manage bit NOT NULL,
    constraint pk_emp_allowed_node primary key clustered (emp_id, node_id)
  )
GO

-- Ticket cache
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ticket')
  CREATE TABLE ticket (
    ticket_id int NOT NULL,
    ticket_number varchar(20) NOT NULL,
    ticket_format varchar(20) NOT NULL, 
    ticket_type varchar(38) NULL,
    kind varchar(20) NOT NULL,
    transmit_date datetime NOT NULL,
    due_date datetime NOT NULL,
    parent_ticket_id int NULL,
    source varchar(12) NULL,
    constraint PK_ticket_ticketid primary key clustered (ticket_id))
GO
  
-- Locate cache (ticket info is duplicated to avoid joins)
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'locate')
  CREATE TABLE locate (
    locate_id int NOT NULL,
    ticket_id int NOT NULL,
    ticket_number varchar(20) NOT NULL,
    ticket_format varchar(20) NOT NULL, 
    ticket_type varchar(38) NULL,
    kind varchar(20) NOT NULL,
    transmit_date datetime NOT NULL,
    due_date datetime NOT NULL,
    parent_ticket_id int NULL,
    source varchar(12) NULL,
    assigned_to int NULL,
    client_id integer NOT NULL, 
    client_code varchar(10) NULL,
    client_name varchar(40) NOT NULL,
    status varchar(5) NOT NULL,
    closed BIT NOT NULL,
    closed_by_id integer NULL,
    status_date datetime NULL,
    status_changed_by_id int NULL,
    constraint PK_locate_locateid primary key nonclustered (locate_id))
GO

if not exists (select * from sys.indexes where name = 'locate_i1')
  CREATE UNIQUE CLUSTERED INDEX locate_i1 ON locate (ticket_id, locate_id)
GO

if not exists (select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'configuration_data')
  CREATE TABLE configuration_data (
    name varchar(50) NOT NULL,
    value varchar(1000) NOT NULL,
    modified_date datetime NOT NULL,
    constraint PK_configuration_data primary key clustered (name))
 GO

-- drop obsolete due_stats table
if exists (select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'due_stats') 
  drop table due_stats
GO