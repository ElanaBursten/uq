SET NOCOUNT ON

-- metric classes
INSERT INTO metric_class VALUES ('TREC',   'Tickets Received',              'flow')
INSERT INTO metric_class VALUES ('EREC',   'Emergencies Received',          'flow')
INSERT INTO metric_class VALUES ('TCLEMP', 'Ticket Closing',                'flow')
INSERT INTO metric_class VALUES ('TCLOSE', 'Ticket Closing',                'flow')
INSERT INTO metric_class VALUES ('LCLEMP', 'Locate Closing',                'flow')
INSERT INTO metric_class VALUES ('LCLOSE', 'Locate Closing',                'flow')
INSERT INTO metric_class VALUES ('TROUTE', 'Tickets Routed (no demo)',      'flow')
INSERT INTO metric_class VALUES ('RESPQ',  'Response Queue Size (no demo)', 'state')
go

-- metric granularity
INSERT INTO metric_granularity VALUES ('DAY',  'Day')
INSERT INTO metric_granularity VALUES ('HOUR', 'Hour')
INSERT INTO metric_granularity VALUES ('MIN',  'Minute')
INSERT INTO metric_granularity VALUES ('SEC',  'Second')
go
