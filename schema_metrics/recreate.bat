@echo on
osql -S %SVR% -E -n -Q "drop database %DB_METRICS%"
osql -S %SVR% -E -n -Q "CREATE database %DB_METRICS%"
osql -S %SVR% -b -E -n -d %DB_METRICS% -i QMMetrics_schema.sql
pause
if errorlevel 1 goto :failed

osql -S %SVR% -b -E -n -d %DB_METRICS% -i sp-other.sql || goto :failed

@REM add some reference data
osql -S %SVR% -b -E -n -d %DB_METRICS% -i ref_data.sql || goto :failed

@REM run all sql scripts in the .\sp folder
FOR %%v IN (sp\*.sql) DO osql -S %SVR% -b -E -n -d %DB_METRICS% -i %%v || goto :failed

pause
exit

:failed
echo ***** FAILED ****
pause
