object frmPlatsParser: TfrmPlatsParser
  Left = 0
  Top = 0
  Caption = 'frmPlatsParser'
  ClientHeight = 463
  ClientWidth = 867
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 371
    Top = 32
    Width = 11
    Height = 431
    Align = alRight
    Color = clBtnFace
    ParentColor = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 867
    Height = 32
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 461
      Top = 6
      Width = 47
      Height = 14
      Caption = 'Search '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 144
      Top = 8
      Width = 3
      Height = 13
    end
    object cboSheets: TComboBox
      Left = 666
      Top = 6
      Width = 145
      Height = 21
      ParentCustomHint = False
      TabOrder = 0
      OnCloseUp = cboSheetsCloseUp
    end
    object edtStreetSearch: TEdit
      Left = 514
      Top = 6
      Width = 121
      Height = 21
      AutoSelect = False
      TabOrder = 1
      OnChange = edtStreetSearchChange
    end
    object btnClientDir: TButton
      Left = 52
      Top = 3
      Width = 105
      Height = 23
      Caption = 'Client Directory'
      TabOrder = 2
      OnClick = btnClientDirClick
    end
    object btnIndexFile: TButton
      Left = 201
      Top = 4
      Width = 108
      Height = 22
      Caption = 'Browse'
      TabOrder = 3
      OnClick = btnIndexFileClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 32
    Width = 371
    Height = 431
    Align = alClient
    TabOrder = 1
    object ListView1: TListView
      Left = 1
      Top = 1
      Width = 369
      Height = 429
      Align = alClient
      Columns = <>
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      ViewStyle = vsList
      OnDblClick = ListView1DblClick
    end
  end
  object Panel4: TPanel
    Left = 382
    Top = 32
    Width = 485
    Height = 431
    Align = alRight
    TabOrder = 2
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 483
      Height = 429
      Align = alClient
      DataSource = DataSource1
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDrawColumnCell = DBGrid1DrawColumnCell
    end
  end
  object DataSource1: TDataSource
    DataSet = qryVerizonTiffIndex
    Left = 720
    Top = 312
  end
  object FileADOConnection: TADOConnection
    ConnectionString = 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source=;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.ACE.OLEDB.12.0'
    Left = 807
    Top = 312
  end
  object qryVerizonTiffIndex: TADOQuery
    Connection = FileADOConnection
    CursorType = ctStatic
    OnFilterRecord = qryVerizonTiffIndexFilterRecord
    Parameters = <>
    SQL.Strings = (
      '')
    Left = 619
    Top = 313
  end
end
