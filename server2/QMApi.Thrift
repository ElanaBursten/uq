//*********************************************************
//                   QMApi.thrift                    
//*********************************************************

# Thrift IDL file for a service. This file contains the shared types, structs, etc.
#
# This file is used by the Thrift compiler to generate Delphi code shared
# between servers and clients.
#
# Before running this file, you will need to have installed the thrift compiler
# (the version we are using is located in \bin).
#
# Sample command to generate the source:
# ..\bin\thrift-0.9.1.exe -verbose -r -o .\ --gen delphi:register_types QMApi.thrift
#
# Follow Thrift best practices here: http://diwakergupta.github.io/thrift-missing-guide/#_best_practices
# Most importantly, do not change the numeric tags for any existing fields!

/*
 * Thrift files can reference other Thrift files to include common struct
 * and service definitions. These are found using the current path, or by
 * searching relative to any paths specified with the -I compiler flag.
 *
 * Included objects are accessed using the name of the .thrift file as a
 * prefix. i.e. MyServiceShared.SharedObject
 */
include "QMApiShared.thrift"
include "QMApiCommand.thrift"
include "QMApiQuery.thrift"


//*********************************************************
# Manually edited structures
//*********************************************************

// ForeignKey is used for child data where each row can have a parent from one of these tables
enum ForeignType {
  qmftTicket = 1
  qmftDamage = 2
  qmft3rdParty = 3
  qmftLitigation = 4
  qmftLocate = 5
  qmftDamageNarrative = 6
  qmftWorkOrder = 7
}

struct ForeignKey {
  1: ForeignType ForeignType;
  2: i32 ID;
}

//Replaces RO operation SaveNewTickets and it's supporting structures
/*Purpose: Save a new Ticket */
struct AddTicketCommand {
  1: i32 TicketID; 
  2: string TicketFormat; 
  3: string Kind; 
  4: string MapPage; 
  5: QMApiShared.datetimestring TransmitDate; 
  6: QMApiShared.datetimestring CallDate; 
  7: string WorkDescription; 
  8: string WorkState; 
  9: string WorkCounty; 
  10: string WorkCity; 
  11: string WorkAddressNumber; 
  12: string WorkAddressStreet; 
  13: string WorkCross; 
  14: string WorkType; 
  15: string WorkRemarks; 
  16: string Company; 
  17: string ConName; 
  18: string CallerContact; 
  19: string TicketType; 
}

// Replaces RemObjects Struct: LocateHour3
/*Purpose: Adds the number of hours an employee spent working on an individual locate for a ticket.*/
struct AddLocateHoursCommand {
  1: i32 LocateID;
  2: i32 LocateHoursID; 
  3: string ClientCode; 
  4: QMApiShared.datetimestring WorkDate; 
  5: double RegHours; 
  6: double OTHours; 
  7: string Status; 
}

/*Purpose: Adds the number of actual and billable units marked by an employee working on an individual locate for a ticket.*/
struct AddLocateUnitsCommand {
  1: i32 LocateID;
  2: i32 LocateHoursID; 
  3: string ClientCode; 
  4: QMApiShared.datetimestring WorkDate; 
  5: i32 UnitsMarked; 
  6: string Status; 
  7: i32 UnitConversionID; 
}

/*Purpose: Inactivates a previously added plat.*/
struct DeleteLocatePlatCommand {
  1: i32 LocatePlatID; 
}

// Also replaces RemObjects Struct: LocateFacilityChange. 
/*Purpose: Inactivate an existing facility.*/
struct DeleteLocateFacilityCommand {
  1: i32 LocateFacilityID; 
}

struct WorkPeriod {
  1: QMApiShared.datetimestring WorkStart;
  2: QMApiShared.datetimestring WorkStop;
}

struct CalloutPeriod {
  1: QMApiShared.datetimestring CalloutStart;
  2: QMApiShared.datetimestring CalloutStop;
}

//Replaces RemObjects Operation: PostTimesheets
struct AddTimesheetCommand {
  1: i32 EntryID;
  2: i32 WorkEmpID;
  3: QMApiShared.datetimestring WorkDate;
  4: string Status;
  5: QMApiShared.datetimestring EntryDateLocal;
  6: QMApiShared.datetimestring ApproveDate;
  7: i32 ApproveBy;
  8: QMApiShared.datetimestring FinalApproveDate;
  9: i32 FinalApproveBy;
  10: list<WorkPeriod> WorkPeriods;
  11: list<CalloutPeriod> CalloutPeriods;
  12: i32 MilesStart1;
  13: i32 MilesStop1;
  14: string VehicleUse;
  15: double RegHours;
  16: double OTHours;
  17: double DTHours;
  18: double CalloutHours;
  19: double VacHours;
  20: double LeaveHours;
  21: double PTOHours;
  22: double BRHours;
  23: double HolHours;
  24: double JuryHours;
  25: bool FloatingHoliday;
  26: QMApiShared.datetimestring RuleAckDate;
  27: i32 RuleIDAck;
  28: i32 EmpTypeID;
  29: i32 ReasonChanged;
  30: QMApiShared.datetimestring LunchStart;
  31: string Source;
}

// Replaces RemObjects Operation: StatusWorkOrders2 and RO Struct WorkOrderChange2
struct StatusWorkOrderCommand {
  1: i32 WorkOrderID; 
  2: string WorkDescription; 
  3: string WorkCross; 
  4: string StateHwyROW; 
  5: i32 RoadBoreCount; 
  6: i32 DrivewayBoreCount; 
  7: i32 AssignedToID; 
  8: string Status; 
  9: bool Closed; 
  10: string StatusedHow; 
}

// Replaces RemObjects Operation: StatusWorkOrderInspections and RO Struct WOInspectionChange
struct StatusWorkOrderInspectionCommand {
  1: i32 WorkOrderID; 
  2: i32 AssignedToID; 
  3: string Status; 
  4: bool Closed; 
  5: string StatusedHow; 
  6: string ActualMeterNumber; 
  7: string ActualMeterLocation; 
  8: string BuildingType; 
  9: i32 CGAVisits; 
  10: string MapStatus; 
  11: string MercuryRegulator; 
  12: string VentClearanceDist; 
  13: string VentIgnitionDist; 
  14: string AlertFirstName; 
  15: string AlertLastName; 
  16: string AlertOrderNumber; 
  17: string CGAReason; 
  18: string GasLight;
}

// Replaces RemObjects Struct: JobsiteArrivalChange
/*Purpose: Records the date and time the employee indicates they arrived at a jobsite where they plan to work. Only Ticket and Damage 
  jobsite arrivals are currently tracked. Users with sufficient access can add arrivals for another user.*/ 
struct ArriveAtJobsiteCommand {
  1: i32 ArrivalID;
  2: i32 EmpID;   
  3: ForeignKey ForeignKey; 
  4: QMApiShared.datetimestring ArrivalDate; 
  5: string EntryMethod; 
  6: string LocationStatus; 
}

// This struct also replaces RemObjects Struct: JobsiteArrivalChange
/*Purpose: Inactivates an existing jobsite arrival row. */
struct DeleteJobsiteArrivalCommand {
  1: i32 ArrivalID; 
}

//Replaces Remobjects Struct: SaveNewNotes2, NotesChange, NotesChangeList
/*Purpose: Add a new note up to 8000 characters to either the whole ticket or an individual locate on the ticket. ParentKey determines what type of item the parent is. */
struct AddNoteCommand{
  1: i32 NotesID; 
  2: ForeignKey ForeignKey;
  3: string Note; 
  4: i32 UID; 
}

//Also replaces Remobjects Struct: SaveNewNotes2, NotesChange, NotesChangeList
/*Purpose:  Delete an existing note. Restricted to users with permission to delete notes.*/
struct DeleteNoteCommand{
  1: i32 NotesID; 
}

// Along with StatusLocateCommand, also replaces RemObjects Struct: LocateChange6
struct FlagHighProfileLocateCommand {
  1: i32 LocateID; 
  2: bool HighProfile; 
  3: i32 HighProfileReason; 
}

//Replaces RO Struct NewAttachment and RO Operation SaveNewAttachments
/*Purpose: Attaches a file (photo, doc, video, etc) to a ticket. This only inserts the row in the attachment table, 
  it is the client application�s responsibility to upload the file to the correct FTP site. */
struct AttachFileCommand {
  1: i32 AttachmentID; 
  2: ForeignKey ForeignKey; 
  3: i32 Size; 
  4: string Filename; 
  5: string OrigFileName; 
  6: QMApiShared.datetimestring OrigFileModDate; 
  7: string Source; 
  8: string FileHash; 
  9: string UploadMachineName; 
}

//Response structure for AttachFileCommand
struct AttachFileResponse {
  1: QMApiShared.GeneratedKey GeneratedKey; 
}

/*Purpose: Sets the comment for an attachment.*/
struct AttachFileCommentCommand {
  1: i32 AttachmentID; 
  2: string Comment;
}

// Replaces RemObjects Struct: AttachmentUpdate 
/*Purpose: Sets the document type for an attachment.*/
struct SetAttachmentDocTypeCommand {
  1: i32 AttachmentID; 
  2: string DocumentType;
}

// Also replaces RemObjects Struct: AttachmentUpdate 
/*Purpose: Inactivates an attachment.*/
struct DeleteAttachmentCommand {
  1: i32 AttachmentID; 
}

// Replaces RemObjects Operation: SaveAttachmentUploads and RemObjects Struct: AttachmentUpload 
/*Purpose: Set the date/time the client successfully uploaded the attachment to the FTP server.*/
struct UploadAttachmentCommand {
  1: i32 AttachmentID; 
  2: bool BackgroundUpload; 
}

// Replaces RemObjects Struct: AddinInfoChange 
/*Purpose: Saves any information added to the ticket when the employee uses the e-Sketch addin application.*/
struct AddAddinInfoCommand {
  1: i32 AddinInfoID; 
  2: ForeignKey ForeignKey;
  3: double Latitude; 
  4: double Longitude; 
}

// Replaces RemObjects Struct: TaskScheduleChange
/*Purpose: Records the first ticket that will be worked on by the employee the next working day, along with 
  the date and time they expect to start. Users with sufficient rights can schedule work for other employees.*/ 
struct ScheduleFirstWorkCommand {
  1: i32 TaskScheduleID; 
  2: i32 EmpID; 
  3: ForeignKey ForeignKey; 
  4: QMApiShared.datetimestring EstStartDate; 
}

/*Purpose: Mark a previously scheduled work event as done.*/
struct PerformScheduledWorkCommand {
  1: i32 TaskScheduleID; 
}

/*Purpose: Inactivate a previously scheduled work event. This can only be done by users with sufficent rights.*/
struct DeleteScheduledWorkCommand {
  1: i32 TaskScheduleID; 
}


//***************
#  TODO Thrift Exceptions. Not sure what exceptions we will need, here is an example to start with.
//***************
exception InvalidOperation {
  1: i32 what,
  2: string why
}


//*********************************************************
# Main structures used to wrap commands
//*********************************************************

struct ServerError {
  1: string Description;
}

//Note that when a union field is set, all other fields are cleared.
union CommandUnion {
  1: QMApiCommand.AddEmployeeActivityCommand AddEmployeeActivity;
  2: QMApiCommand.StatusLocateCommand StatusLocate;
  3: AddNoteCommand AddNote;
  4: DeleteNoteCommand DeleteNote;
  5: AddLocateUnitsCommand AddLocateUnits;
  6: AddLocateHoursCommand AddLocateHours;
  7: QMApiCommand.AddGPSPositionCommand AddGPSPosition;
  8: QMApiCommand.AddTicketInfoCommand AddTicketInfo;
  9: QMApiCommand.AddLocatePlatCommand AddLocatePlat;
  10: DeleteLocatePlatCommand DeleteLocatePlat;
  11: ArriveAtJobsiteCommand ArriveAtJobsite;
  12: DeleteJobsiteArrivalCommand DeleteJobsiteArrival;
  13: FlagHighProfileLocateCommand FlagHighProfileLocate;
  14: AttachFileCommand AttachFile;
  15: AttachFileCommentCommand AttachFileComment;
  16: SetAttachmentDocTypeCommand SetAttachmentDocType;
  17: DeleteAttachmentCommand DeleteAttachment;
  18: UploadAttachmentCommand UploadAttachment;
  19: QMApiCommand.AddLocateFacilityCommand AddLocateFacility;
  20: DeleteLocateFacilityCommand DeleteLocateFacility;
  21: AddAddinInfoCommand AddAddinInfo;
  22: ScheduleFirstWorkCommand ScheduleFirstWork;
  23: PerformScheduledWorkCommand PerformScheduledWork;
  24: DeleteScheduledWorkCommand DeleteScheduledWork;
// The design for the commands below is still in progress.
//  1: QMApiCommand.SendMessageCommand SendMessage;
//  5: SaveTicketAreaCommand SaveTicketArea;
//  8: SaveAreaCommand SaveArea;
//  10: SaveWorkRemedyCommand SaveWorkRemedy;
//  11: MoveLocateCommand MoveLocate;
//  12: MoveTicketsCommand MoveTickets;
//  13: CompletionReportCommand CompletionReport;
//  14: ChangePasswordCommand ChangePassword;
//  15: AckTicketCommand AckTicket;
//  16: AckDamageTicketCommand AckDamageTicket;
//  17: AddEmployeeAssetCommand AddEmployeeAsset;
//  18: ReportErrorCommand ReportError;
//  19: ApproveTimesheetCommand ApproveTimesheet;
//  20: ReportStatusCommand ReportStatus;
//  21: StartReportCommand StartReport;
//  22: CheckReportCommand CheckReport;
//  23: SaveBillingAdjustmentCommand SaveBillingAdjustment;
//  24: StartBillingCommand StartBilling;
//  25: BillingActionCommand BillingAction;
//  26: CancelBillingRunCommand CancelBillingRun;
//  27: SaveDamageBillCommand SaveDamageBill;
//  28: SaveDamageCommand SaveDamage;
//  29: SaveDamageThirdPartyCommand SaveDamageThirdParty;
//  30: SaveDamageEstimateCommand SaveDamageEstimate;
//  31: SaveDamageInvoiceCommand SaveDamageInvoice;
//  32: SaveDamageLitigationCommand SaveDamageLitigation;
//  33: ReassignAssetCommand ReassignAsset;
//  34: AckMessageCommand AckMessage;
//  35: UpdateRestrictedUseExemptionCommand UpdateRestrictedUseExemption;
//  36: SaveTicketPriorityCommand SaveTicketPriority;
//  37: SaveDamagePriorityCommand SaveDamagePriority;
//  38: SaveEmployeeTicketViewLimitCommand SaveEmployeeTicketViewLimit;
//  39: AckTicketActivityCommand AckTicketActivity;
//  40: AckAllTicketActivitiesForManagerCommand AckAllTicketActivitiesForManager;
//  41: SaveUploadQueueStatsCommand SaveUploadQueueStats;
//  42: SaveRequiredDocumentsCommand SaveRequiredDocuments;
//  43: MoveDamageCommand MoveDamage;
//  44: SaveEmployeeShowFutureWorkCommand SaveEmployeeShowFutureWork;
//  45: MoveWorkOrderCommand MoveWorkOrder;
//  46: AddFollowupTicketCommand AddFollowupTicket;
//  47: ApproveDamageCommand ApproveDamage;
//  48: SetMessageActiveCommand SetMessageActive;
//  49: SaveBreakRuleResponseCommand SaveBreakRuleResponse;
//  50: AddTimesheetCommand AddTimesheet;
//  51: AddTicketCommand AddTicket;
//  53: StatusWorkOrderCommand StatusWorkOrder;
//  54: StatusWorkOrderInspectionCommand StatusWorkOrderInspection;
}

union CommandSuccessUnion {
  1: QMApiShared.GeneratedKey GeneratedKey;

// TODO: The design for the command responses below is still in progress.
//  1: QMApiCommand.CompletionReportResponse ResponseCompletionReport;
//  2: QMApiCommand.ApproveTimesheetResponse ResponseApproveTimesheet;
//  3: StartReportResponse ResponseStartReport;
//  4: CheckReportResponse ResponseCheckReport;
//  5: SaveBillingAdjustmentResponse ResponseSaveBillingAdjustment;
//  6: BillingActionResponse ResponseBillingAction;
//  7: SaveDamageBillResponse ResponseSaveDamageBill;
//  8: SaveDamageResponse ResponseSaveDamage;
//  9: SaveDamageThirdPartyResponse ResponseSaveDamageThirdParty;
//  10: SaveDamageEstimateResponse ResponseSaveDamageEstimate;
//  11: SaveDamageInvoiceResponse ResponseSaveDamageInvoice;
//  12: SaveDamageLitigationResponse ResponseSaveDamageLitigation;
//  13: AckMessageResponse ResponseAckMessage;
//  14: AckTicketActivityResponse ResponseAckTicketActivity;
//  15: AckAllTicketActivitiesForManagerResponse ResponseAckAllTicketActivitiesForManager;
//  16: SaveUploadQueueStatsResponse ResponseSaveUploadQueueStats;
//  17: SaveRequiredDocumentsResponse ResponseSaveRequiredDocuments;
//  18: ApproveDamageResponse ResponseApproveDamage;
//  19: SaveBreakRuleResponseResponse ResponseSaveBreakRuleResponse;
}

union CommandFailureUnion {
  1: list<ServerError> ValidationErrors;
  2: list<ServerError> ServerErrors;
}

//Note that when a union field is set, all other fields are cleared.
union CommandResponseUnion {
  1: CommandSuccessUnion Success;
  2: CommandFailureUnion Failure;
}

struct Command {
  1: i32 EmpID; 
  2: QMApiShared.datetimestring CreateDateTime; 
  3: CommandUnion Union; 
}

struct QMApiCommands {
  1: list<Command> CommandList;
}

struct QMApiResponses {
  1: bool Success; 
  2: list<CommandResponseUnion> ResponseList; 
}
