inherited NotesCommandDM: TNotesCommandDM
  object AddNotesSP: TADStoredProc
    StoredProcName = 'CMD_add_notes'
    Left = 24
    Top = 14
    ParamData = <
      item
        Position = 1
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        ParamType = ptResult
      end
      item
        Position = 2
        Name = '@NotesID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = '@ForeignType'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 4
        Name = '@ForeignId'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 5
        Name = '@UID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 6
        Name = '@EmpID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 7
        Name = '@CreateDateTime'
        DataType = ftDateTime
        ParamType = ptInput
      end
      item
        Position = 8
        Name = '@Note'
        DataType = ftString
        ParamType = ptInput
        Size = 8000
      end>
  end
end
