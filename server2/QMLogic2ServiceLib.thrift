//*********************************************************
//                   QMLogic2ServiceLib.thrift
//*********************************************************

# Thrift IDL file for a service. This file contains the shared types, structs, etc.
#
# This file is used by the Thrift compiler to generate Delphi code shared
# between servers and clients.
#
# Before running this file, you will need to have installed the thrift compiler
# into /usr/local/bin.//TODO (update with final location in source.)
#
# Sample command to generate the source:
# thrift-0.9.1.exe -verbose --gen delphi:register_types AService.thrift
#
# Follow Thrift best practices here: http://diwakergupta.github.io/thrift-missing-guide/#_best_practices
# Most importantly, do not change the numeric tags for any existing fields!

/*
 * Thrift files can reference other Thrift files to include common struct
 * and service definitions. These are found using the current path, or by
 * searching relative to any paths specified with the -I compiler flag.
 *
 * Included objects are accessed using the name of the .thrift file as a
 * prefix. i.e. MyServiceShared.SharedObject
 */
//It is possible to move common typedefs, structs,containers to a shared file and reference it from each service thrift file.
//include "MyServiceShared.thrift"

//*********************************************************
# Typedefs (type translations)
//*********************************************************

// Thrift does not have a DateTime data type, so we will use a string and create
// an alternate name for the type that makes it easier to read

typedef string DateTimeString

enum AggregateType {
  atTicket = 1
  atDamage = 2
  atWorkOrder = 3
}

struct AggrKey {
  1: AggregateType AggrType;
  2: i32 AggrID;
}

# Command structs
struct AssignTicketCommand {
  1: i32 TicketID;
  2: i32 FromLocatorID;
  3: i32 ToLocatorID;
  4: i32 ChangedByID;
  5: DateTimeString ChangedDate;
}

struct ScheduleTicketCommand {
  1: i32 TicketID;
  2: DateTimeString WorkloadDate;
  3: i32 ChangedByID;
  4: DateTimeString ChangedDate;
}

# Query structs
struct DueStatistic {
  1: string Category;
  2: i32 Count;
}

struct DueList {
  1: list<DueStatistic> Items;
}

struct LocateSummary {
  1: i32 ID;
  2: i32 ClientID;
  3: string ClientCode;
  4: string ClientName;
  5: string Status;
  6: bool Closed;
  7: DateTimeString StatusDate;
}

struct LocateList {
  1: list<LocateSummary> Items;
}

struct Coordinate {
  1: double Latitude;
  2: double Longitude;
  3: string Name;
  4: string Comment;
}

struct StringList {
  1: list<string> Items;
}

struct TicketSummary {
  1: i32 TicketID;
  2: string TicketNumber;
  3: string TicketFormat;
  4: string Kind;
  5: DateTimeString DueDate;
  6: string WorkAdddressStreet;
  7: string WorkAddressNumber;
  8: string WorkCity;
  9: string WorkCounty;
  10: string WorkState;
  11: string WorkDescription;
  12: string WorkType;
  13: string MapPage;
  14: string TicketType;
  15: string Alert;
  16: i32 WorkPrioritySort;
  17: string WorkPriority;
  18: bool TicketIsVisible;
  19: DateTimeString EstStartTime;
  20: DateTimeString WorkloadDate;
  21: string ImagePath;
  22: Coordinate Pin;
  23: LocateList Locates;
  24: StringList ImportantFields;
}

# TODO: This struct is incomplete
struct DamageSummary {
  1: i32 DamageID;
  2: string UQDamageID;
  3: string DamageType;
  4: DateTimeString DamageDate;
}

# TODO: This struct is incomplete
struct WorkOrderSummary {
  1: i32 ID;
}

union Task {
  1: TicketSummary Ticket
  2: DamageSummary Damage
  3: WorkOrderSummary WorkOrder
}

struct TaskList {
  1: list<Task> Items;
}

struct Ancestor {
  1: i32 Id;
  2: string ShortName;
}

struct AncestorList {
  1: list<Ancestor> Items;
}

struct Node {
  1: i32 NodeId;
  2: string ShortName;
  3: string ImagePath;
  4: DueList TasksDue;
  5: TaskList Tasks;
  6: AncestorList Ancestors;
}

# Event structs
struct TicketReceived {
  1: string TicketNumber;
  2: string TicketFormat;
  3: string Source;
  4: string Kind;
  5: string TicketType;
  6: DateTimeString TransmitDate;
  7: DateTimeString DueDate;
  8: list<LocateSummary> LocateList;
}

struct LocateAssigned {
  1: i32 LocateID;
  2: i32 FromLocatorID;
  3: i32 ToLocatorID;
  4: i32 ChangedByID;
  5: DateTimeString ChangedDate;
}

struct TicketScheduled {
  1: DateTimeString WorkloadDate;
  2: i32 ChangedByID;
  3: DateTimeString ChangedDate;
}

struct LocateClosed {
  1: i32 LocateID;
  2: DateTimeString ClosedDate;
  3: i32 ClosedByEmpID;
  4: string ClientCode;
  5: string CallCenter;
  6: string StatusCode;
  7: bool CompletedTicket;
  8: string TicketType;
  9: DateTimeString WorkloadDate;
}

struct LocateReopened {
  1: i32 LocateID;
  2: DateTimeString ReopenedDate;
  3: i32 ReopenedByEmpID;
  4: string ClientCode;
  5: string CallCenter;
  6: string StatusCode;
  7: bool ReopenedTicket;
  8: string TicketType;
  9: DateTimeString WorkloadDate;
}

union EventUnion {
  1: LocateAssigned LocateAssigned
  2: TicketScheduled TicketScheduled
  3: LocateClosed LocateClosed
  4: LocateReopened LocateReopened
  5: TicketReceived TicketReceived
}

struct Event {
  1: string EventID;
  2: AggrKey AggregateKey;
  3: EventUnion Union;
}

struct EventList {
  1: list<Event> Items;
}
