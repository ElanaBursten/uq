inherited APIDM: TAPIDM
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 440
  Width = 477
  object Conn: TADConnection
    Params.Strings = (
      'DriverID=MSSQL')
    FormatOptions.AssignedValues = [fvMapRules]
    FormatOptions.OwnMapRules = True
    FormatOptions.MapRules = <
      item
        SourceDataType = dtDateTimeStamp
        TargetDataType = dtDateTime
      end>
    ResourceOptions.AssignedValues = [rvKeepConnection]
    ResourceOptions.KeepConnection = False
    LoginPrompt = False
    Left = 32
    Top = 16
  end
  object LoginUser: TADStoredProc
    Connection = Conn
    StoredProcName = 'login_user2'
    Left = 136
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        ParamType = ptResult
      end
      item
        Position = 2
        Name = '@UserName'
        DataType = ftString
        ParamType = ptInput
        Size = 25
      end
      item
        Position = 3
        Name = '@PW'
        DataType = ftString
        ParamType = ptInput
        Size = 40
      end>
  end
  object CheckAPIKey: TADQuery
    Connection = Conn
    SQL.Strings = (
      
        'select emp_id, chg_pwd, chg_pwd_date  from users where login_id=' +
        ':login_id and api_key=:api_key')
    Left = 136
    Top = 72
    ParamData = <
      item
        Name = 'LOGIN_ID'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'API_KEY'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object CheckRights: TADQuery
    Connection = Conn
    SQL.Strings = (
      
        'select emp_id, chg_pwd, chg_pwd_date  from users where login_id=' +
        ':login_id and api_key=:api_key')
    Left = 136
    Top = 128
    ParamData = <
      item
        Name = 'LOGIN_ID'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'API_KEY'
        DataType = ftString
        ParamType = ptInput
      end>
  end
end
