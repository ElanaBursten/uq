unit APIDevFormU;

interface

uses System.SysUtils, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.AppEvnts,
  Vcl.StdCtrls, Vcl.ExtCtrls, System.Classes;

type
  TAPIDevForm = class(TForm)
    ButtonStart: TButton;
    ButtonStop: TButton;
    EditPort: TEdit;
    Label1: TLabel;
    ApplicationEvents1: TApplicationEvents;
    Timer1: TTimer;
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure ButtonStartClick(Sender: TObject);
    procedure ButtonStopClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  end;

var
  APIDevForm: TAPIDevForm;

implementation

{$R *.dfm}

uses
  ServerU;

procedure TAPIDevForm.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
  ButtonStart.Enabled := not ServerDM.Active;
  ButtonStop.Enabled := ServerDM.Active;
  EditPort.Enabled := not ServerDM.Active;
end;

procedure TAPIDevForm.ButtonStartClick(Sender: TObject);
begin
  ServerDM.ConfigureFromIni;
  EditPort.Text := IntToStr(ServerDM.Port);
  ServerDM.Start;
end;

procedure TAPIDevForm.ButtonStopClick(Sender: TObject);
begin
  ServerDM.Stop;
end;

procedure TAPIDevForm.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  ButtonStartClick(nil);
end;

end.
