unit SyncDMu;

interface

uses
  System.SysUtils, System.Classes, BaseQueryDMu, uADStanIntf, uADStanOption,
  uADStanParam, uADStanError, uADDatSManager, uADPhysIntf, uADDAptIntf,
  uADStanAsync, uADDAptManager, Data.DB, uADCompDataSet, uADCompClient;

type
  TSyncDM = class(TBaseQueryDM)
    SyncSP: TADStoredProc;
  public
    procedure Sync;
  end;

implementation

uses
  AdUtils, OdMiscUtils, OdIsoDates;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TSyncDM }

procedure TSyncDM.Sync;
var
  Params: array[0..4] of TParamRec;
  Values: array[0..4] of Variant;
  QryFields: TStrings;
  I: Integer;
  EmpID: Integer;
begin
  MainDM.SetOpName('Sync');
  with Params[0] do begin
    Name := '@EmployeeID';
    ParamDataType := ftInteger;
  end;
  with Params[1] do begin
    Name := '@UpdatesSinceS';
    ParamDataType := ftString;
  end;
  with Params[2] do begin
    Name := '@ClientTicketListS';
    ParamDataType := ftMemo;
  end;
  with Params[3] do begin
    Name := '@ClientDamageListS';
    ParamDataType := ftMemo;
  end;
  with Params[4] do begin
    Name := '@ClientWorkOrderListS';
    ParamDataType := ftMemo;
  end;
  for I := Low(Params) to High(Params) do
    Params[I].ParamType := ptInput;

  QryFields := MainDM.Request.QueryFields;
  Assert(QryFields.Count = 4, 'Sync expected 4 query fields on the request');
  EmpID := StrToIntDef(MainDM.Params[0], 0);
  if EmpID <> MainDM.GetLoggedInEmpID then
    raise Exception.Create('The Employee ID parameter does not match the logged in Employee ID.');

  Values[0] := EmpID;
  Values[1] := QryFields.Values['last_sync'];
  Values[2] := QryFields.Values['tickets'];
  Values[3] := QryFields.Values['damages'];
  Values[4] := QryFields.Values['wos'];

  // Validate the params
  if NotEmpty(Values[1]) and (not IsoIsValidDateTime(Values[1])) then
    raise Exception.Create('The @UpdatesSinceS param should be empty or an ISO date time string.');
  for I := 2 to 4 do begin
    if NotEmpty(Values[I]) and (not ValidateIntegerCommaSeparatedList(Values[I])) then
      raise Exception.CreateFmt('The %s param should be empty or a comma separated list of integers.',
        [Params[I].Name]);
  end;

  MainDM.RespondWithJsonFromMultiParamSP('sync_6', Params, Values);
end;

end.
