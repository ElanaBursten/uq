unit APIDMu;

interface

uses
  System.SysUtils, System.Classes, System.Types, System.StrUtils, Data.DB,
  HttpApp, Wessy, uADStanIntf, uADStanOption, uADStanError, uADGUIxIntf,
  uADPhysIntf, uADStanDef, uADStanPool, uADStanAsync, uADPhysManager,
  uADStanParam, uADDatSManager, uADDAptIntf, uADDAptManager, uADCompClient,
  uADCompDataSet, uADPhysODBCBase, uADPhysMSSQL,
  OperationTracking, AdUtils, OdMiscUtils, QueryDatabase, InterfacedDataModuleU, EventSourceDMu;

type
  TAPIDM = class;

  TLoginData = class(TObject)
    EmpID: Integer;
    UserID: Integer;
    LoginID: string;
    NeedPasswordChange: Integer;
    ShortName: string;
    UserRole: string;
    PasswordExpirationDate: string;
    APIKey: string;
  end;

  TProjectionMetricOperationTrackingData = class(TObject)
    TotalEvents: Integer;
    ProjectedEvents: Integer;
    UnprojectedEvents: Integer;
    ProjectorStatus: string;
  private
    procedure GetData(dm: TAPIDM);
  end;

  TAPIDM = class(TInterfacedDM, IWebDataModule)
    Conn: TADConnection;
    LoginUser: TADStoredProc;
    CheckAPIKey: TADQuery;
    CheckRights: TADQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    QueryDM: TQueryDM;
    SessionVars: TStrings;
    DesiredIsolation: TADTxIsolation;
    DefaultIsolation: TADTxIsolation;
    LoginData: TLoginData;
    function GetShortNameForUser(const LoginID: string): string;
    function GetRightID(RightName: string): Integer;
    procedure SetupQueryDM(const DBName: string);
    procedure StoreClientErrorInternal;
    procedure ConnectMainDB;
  public
    Op: TOpInProgress;
    IniFileName: string;
    DBName: string;
    Request: TWebRequest;
    Response: TWebResponse;
    Params: TStrings;
    EventDM: TEventSourceDM;

    procedure SetOpName(const OpName: string);
    function GetIniBoolSetting(const Section, Ident: string;
      Default: Boolean): Boolean;
    function GetStringIniSetting(const Section, Ident, Default: string): string;
    function CanI(const RightName: string; var LimitationString: string; EmpID: Integer): Boolean; overload;
    function CanI(const RightName: string; EmpID: Integer): Boolean; overload;
    function CanI(const RightName: string): Boolean; overload;
    function CanI(const RightName: string; var LimitationString: string): Boolean; overload;
    function GetWorkManagementLimit: TIntegerArray;
    function CanAccessAncestor(AllowedAncestors: TIntegerArray; AncestorID: Integer): Boolean;
    function CreateDataSetForSQL(const SelectSQL: string): TADQuery;
    procedure RespondWithJsonFromSP(const StoredProcName, ParamName: string; const Value: Integer);
    procedure RespondWithJsonFromMultiParamSP(const StoredProcName: string; Parameters: array of TParamRec; Values: array of Variant);
    procedure RespondWithJsonFromDataSet(Data: TADRdbmsDataSet);

    { IWebDataModule interfaced methods }
    procedure UseContext(ARequest: TWebRequest; AResponse: TWebResponse;
      AParams: TStrings; ASessionVars: TStrings);
    function CheckLogin(const Username, Password: string): Boolean;
    function CheckAuthorization(const Username, APIKey: string): Boolean;
    procedure ReturnUserInformation;
    procedure ReturnPingInformation;
    function GetLoggedInEmpID(const Required: Boolean=False): Integer;
  published
    // TODO: This function should be removed for QMAN-3487
    function IsEventStoreEnabled: Boolean;

    procedure ConnectToDB(DBName: string; DB: TADConnection);
    procedure Disconnect(DB: TADConnection);
    procedure ExecOperation(OpHandler: TProc; DBToUse: TADConnection=nil);

    procedure GetTicketSummary;
    procedure GetTicketDetail;
    procedure ReassignTickets;
    procedure StoreClientError;
    procedure Sync;
    procedure ExecCommandBatch;

    procedure MonitorServer;
    procedure GetCpuGraph;

    // Metrics features
    procedure GetMetricChoices;
    procedure GetMetric;
    procedure GetHierarchy;
    procedure GetStatsForNode;
    procedure GetDefaultStatsForNode;

  end;

implementation

uses
  IniFiles, QMConst, SuperObject, SqlJsonOutput, PasswordRules, Variants, OdIsoDates,
  TicketCommandDMu, TicketQueryDMu, ServerU, MetricsQueryDMu, OdExceptions, SyncDMu,
  ODBCCmdDispatcher, QMApi, CommandUtils, QMThriftUtils, QMCommands;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

const
  DBMetrics = 'MetricsDatabase';

function GetTicketCmdDM(MainDM: TAPIDM): TTicketCmdDM;
begin
  Result := TTicketCmdDM.Create(MainDM, MainDM);
end;

function GetTicketQryDM(MainDM: TAPIDM): TTicketQryDM;
begin
  Result := TTicketQryDM.Create(MainDM, MainDM);
end;

function GetMetricsQryDM(MainDM: TAPIDM; DB: TADConnection): TMetricsQryDM;
begin
  Result := TMetricsQryDM.Create(MainDM, MainDM, DB);
end;

function GetSyncDM(MainDM: TAPIDM): TSyncDM;
begin
  Result := TSyncDM.Create(MainDM, MainDM);
end;

{ TAPIDM }

procedure TAPIDM.ConnectMainDB;
begin
  ConnectToDB('Database', Conn);
end;

procedure TAPIDM.ExecOperation(OpHandler: TProc; DBToUse: TADConnection);
var
  Connection: TADConnection;
begin
  Op := GetOperationTracker.AddOperation;
  try
    Op.Phase := 'Connecting to DB';
    ConnectMainDB; // always need a connection to the main db
    try
      if Assigned(DBToUse) then
        Connection := DBToUse
      else
        Connection := Conn;
    except
      on E1: Exception do begin
        try
          // Log the real error
          Op.Log(E1.Message);
        except
          // There is nothing we can do, if we get an error while trying to log the error.
        end;
        // Then raise a friendlier one
        raise Exception.Create('The Q Manager system encountered an error connecting to the database.' +
          ' Please try again in a few minutes.');
      end;
    end;
    Op.EmpID := GetLoggedInEmpID;
    Op.Phase := 'Setting up transaction';
    try  // except handle error by logging it
      try
        Connection.TxOptions.Isolation := DesiredIsolation;
        Connection.StartTransaction;
        try // commit or roll back
          Op.Phase := 'Processing';
          OpHandler; // Call the proc to do the real work
          Connection.Commit;
          Op.Succeeded := True;
          Op.Log(Op.Detail);  // successful
        except
          Op.Log('Rolling back');
          try
            Connection.Rollback;
          except
            on RE: Exception do
              Op.Log('Exception occured while rolling back: ' + RE.Message);
          end;
          raise;
        end;
      finally
        Disconnect(Connection);
        Disconnect(Conn);
        // put back the default isolation
        DesiredIsolation := DefaultIsolation;
      end;
    except
      on E: ECommandDispatchError do begin
        Op.Log(E.Message);
      end;

      on E: Exception do begin
        E.Message := Format('QM Server Error: %s (%s: %s: %s)', [E.Message, Op.OpName, Op.Phase, Op.Detail]);
        try
          Op.Log(E.Message);
        except
          // There is nothing we can do, if we get an error while
          // trying to log the error.
        end;
        raise;
      end;
    end;
  finally
    GetOperationTracker.FinishOperation(Op);
  end;
end;

function TAPIDM.CheckAuthorization(const Username, APIKey: string): Boolean;
var
  ChgPwd: Boolean;
  ChgPwdDate: TDateTime;
begin
  ConnectMainDB;
  Result := False;
  Assert(not CheckAPIKey.Active);
  CheckAPIKey.Params.ParamValues['login_id'] := UserName;
  CheckAPIKey.Params.ParamValues['api_key'] := APIKey;
  CheckAPIKey.Open;
  try
    if not CheckAPIKey.Eof then begin
      ChgPwd := CheckAPIKey.FieldByName('chg_pwd').AsBoolean;
      ChgPwdDate := CheckAPIKey.FieldByName('chg_pwd_date').AsDateTime;
      if ChgPwd or ((ChgPwdDate > 1) and (Now > ChgPwdDate)) then begin
        //TODO : Modify function to return a message indicating password is expired
        Result := False;
      end else
        Result := True;
    end;
    if Result then
      SessionVars.Values['EmpID'] := CheckAPIKey.FieldByName('emp_id').AsString;
  finally
    CheckAPIKey.Close;
  end;
end;

function TAPIDM.CheckLogin(const Username, Password: string): Boolean;
var
  LoginFailed: Boolean;
  LoggedInChgPwd: Boolean;
  LoggedInChgPwdDate: TDateTime;
begin
  ConnectMainDB;
  Assert(not LoginUser.Active);
  // check for client sending hash to encrypted db:
  LoginUser.Params.ParamValues['@UserName'] := UserName;
  LoginUser.Params.ParamValues['@PW'] := Password;
  LoginUser.Open;
  try
    LoginFailed := LoginUser.EOF;
    if LoginFailed and (not PasswordIsHashed(Password)) then begin
      // check for client sending non-hash pw to encrypted db:
      LoginUser.Close;
      LoginUser.Params.ParamValues['@UserName'] := UserName;
      LoginUser.Params.ParamValues['@PW'] := GeneratePasswordHash(UserName, Password);
      LoginUser.Open;
      LoginFailed := LoginUser.EOF;
    end;
    if LoginFailed and PasswordIsHashed(Password) then begin
      // check for client sending hash to unencrypted db:
      LoginUser.Close;
      LoginUser.Params.ParamValues['@UserName'] := UserName;
      LoginUser.Params.ParamValues['@PW'] := Null;
      LoginUser.Open;
      LoginFailed := LoginUser.EOF;
      if not LoginFailed then
        LoginFailed := (GeneratePasswordHash(UserName, LoginUser.FieldByName('password').AsString) <> Password);
    end;
    if not LoginFailed then begin
      SessionVars.Values['EmpID'] := LoginUser.FieldByName('emp_id').AsString;
      with LoginData do begin
        EmpID := LoginUser.FieldByName('emp_id').AsInteger;
        UserID := LoginUser.FieldByName('uid').AsInteger;
        LoginID := UserName;
        ShortName := LoginUser.FieldByName('short_name').AsString;
        UserRole := LoginUser.FieldByName('TypeCode').AsString;
        PasswordExpirationDate := IsoDateTimeToStr(LoginUser.FieldByName('chg_pwd_date').AsDateTime);
        APIKey := LoginUser.FieldByName('api_key').AsString;
        LoggedInChgPwd := LoginUser.FieldByName('chg_pwd').AsBoolean;
        LoggedInChgPwdDate := LoginUser.FieldByName('chg_pwd_date').AsDateTime;
        NeedPasswordChange := PasswordChangeNone;
        if LoggedInChgPwd or ((LoggedInChgPwdDate > 1) and (LoggedInChgPwdDate < Now)) then
          NeedPasswordChange := PasswordChangeForce;

      end;
    end;
  finally
    LoginUser.Close;
  end;
  Result := not LoginFailed;
end;

procedure TAPIDM.ConnectToDB(DBName: string; DB: TADConnection);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(IniFileName);
  try
    if IsEmpty(DBName) then
      DBName := 'Database';
    if (not DB.Connected) then
      ConnectADConnectionWithIni(DB, Ini, DBName);
  finally
    FreeAndNil(Ini);
  end;
end;

procedure TAPIDM.Disconnect(DB: TADConnection);
begin
  DB.Connected := False;
end;

procedure TAPIDM.DataModuleCreate(Sender: TObject);
begin
  EventDM := TEventSourceDM.Create(Self);
  EventDM.Setup(Conn);
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules + 1;
  IniFileName := ExtractFilePath(GetModuleName(HInstance)) + 'QMServer.ini';
  DefaultIsolation := xiReadCommitted;
  DesiredIsolation := DefaultIsolation;
  LoginData := TLoginData.Create;
end;

procedure TAPIDM.DataModuleDestroy(Sender: TObject);
begin
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules - 1;
  LoginData.Free;
end;

procedure TAPIDM.SetupQueryDM(const DBName: string);
begin
  if IsEmpty(DBName) then
    Exit;
  if not Assigned(QueryDM) then
    QueryDM := TQueryDM.Create(Self);
  ConnectToDB(DBName, QueryDM.Conn);
end;

procedure TAPIDM.ExecCommandBatch;
var
  Dispatcher: ICommandDispatcher;
begin
  Dispatcher := TODBCCmdDispatcher.Create(Self);
  ExecOperation(TODBCCmdDispatcher(Dispatcher).Execute);
end;

procedure TAPIDM.Sync;
begin
  ExecOperation(GetSyncDM(Self).Sync);
end;

procedure TAPIDM.GetTicketSummary;
begin
  ExecOperation(GetTicketQryDM(Self).GetTicketSummary);
end;

procedure TAPIDM.GetTicketDetail;
begin
  ExecOperation(GetTicketQryDM(Self).GetTicketDetail);
end;

procedure TAPIDM.ReassignTickets;
begin
  ExecOperation(GetTicketCmdDM(Self).ReassignTickets);
end;

procedure TAPIDM.GetStatsForNode;
begin
  SetupQueryDM(DBMetrics);
  ExecOperation(GetMetricsQryDM(Self, QueryDM.Conn).GetStatsForNode, QueryDM.Conn);
end;

procedure TAPIDM.GetDefaultStatsForNode;
begin
  SetupQueryDM(DBMetrics);
  ExecOperation(GetMetricsQryDM(Self, QueryDM.Conn).GetDefaultStatsForNode, QueryDM.Conn);
end;

procedure TAPIDM.GetHierarchy;
begin
  SetupQueryDM(DBMetrics);
  ExecOperation(GetMetricsQryDM(Self, QueryDM.Conn).GetHierarchy, QueryDM.Conn);
end;

procedure TAPIDM.GetMetric;
begin
  SetupQueryDM(DBMetrics);
  ExecOperation(GetMetricsQryDM(Self, QueryDM.Conn).GetMetric, QueryDM.Conn);
end;

procedure TAPIDM.GetMetricChoices;
begin
  SetupQueryDM(DBMetrics);
  ExecOperation(GetMetricsQryDM(Self, QueryDM.Conn).GetMetricChoices, QueryDM.Conn);
end;

procedure TAPIDM.StoreClientError;
begin
  ExecOperation(StoreClientErrorInternal);
end;

procedure TProjectionMetricOperationTrackingData.GetData(dm: TAPIDM);
var
  EventIdMin: Integer;
  EventIdMax: Integer;
  LastProjectedEventID: Integer;
const
  SQL1 = 'select count(*) from event_log';
  SQL2 = 'select min(event_id) from event_log';
  SQL3 = 'select max(event_id) from event_log';
  SQL4 = 'SELECT cast(value as int) FROM configuration_data where name = ''LastProjectedEventID''';
  SQL5 = 'SELECT value FROM configuration_data where name = ''ProjectorStatus''';
begin
  with dm do begin
    ConnectMainDB;
    TotalEvents := Conn.ExecSQLScalar(SQL1);
    EventIdMin := Conn.ExecSQLScalar(SQL2);
    EventIdMax := Conn.ExecSQLScalar(SQL3);
    SetupQueryDM(DBMetrics);
    LastProjectedEventID := QueryDM.Conn.ExecSQLScalar(SQL4);
    ProjectorStatus := QueryDM.Conn.ExecSQLScalar(SQL5);
    ProjectedEvents := (LastProjectedEventID - EventIdMin) + 1;
    UnprojectedEvents := EventIdMax - LastProjectedEventID;
  end;
end;

procedure TAPIDM.MonitorServer;
begin
  Response.ContentType := 'text/html';
  if SameText(Params[0], 'server') then
    Response.Content := GetOperationTracker.GetHtml(ServerDM.StartupFailureMessage,
      ServerDM.InstanceID)
  else if SameText(Params[0], 'projection') then begin
    with TProjectionMetricOperationTrackingData.Create do begin
      try
        GetData(Self);
        Response.Content := GetProjectionTracker.GetHTML(ProjectorStatus, TotalEvents, ProjectedEvents, UnprojectedEvents);
      finally
        Free;
      end;
    end;
  end
  else
    raise Exception.Create('Unknown monitor request');
end;

procedure TAPIDM.GetCpuGraph;
begin
  Response.ContentType := 'image/bmp';
  Response.ContentStream := GetOperationTracker.GetBitmapStream;
end;

procedure TAPIDM.StoreClientErrorInternal;
begin
  // This is a temporary implemenation; it just logs errors generated by clients.
  SetOpName('StoreClientError');
  Op.EmpID := GetLoggedInEmpID;
  Op.Log(Request.Content);
end;

function TAPIDM.GetShortNameForUser(const LoginID: string): string;
const
  SQL = 'select coalesce(e.short_name, ''User '' + u.login_id) short_name ' +
    'from users u left join employee e on u.emp_id = e.emp_id ' +
    'where u.login_id = %s';
begin
  Result := Conn.ExecSQLScalar(Format(SQL, [QuotedStr(LoginID)]));
end;

function TAPIDM.GetLoggedInEmpID(const Required: Boolean): Integer;
begin
  Result := StrToIntDef(SessionVars.Values['EmpID'], 0);
  if Required and (Result < 1) then
    raise EOdNotAllowed.CreateFmt('The requested operation is not allowed for ' +
      'non-employee users [%s].', [SessionVars.Values['LoggedIn']]);
end;

procedure TAPIDM.ReturnUserInformation;
var
  R: ISuperObject;
begin
  R := SO;
  R.S['result'] := 'ok';
  R.I['userID'] := LoginData.UserID;
  R.S['loginID'] := LoginData.LoginID;
  R.S['fullName'] := LoginData.ShortName;
  R.I['empID'] := LoginData.EmpID;
  R.I['needPasswordChange'] := LoginData.NeedPasswordChange;
  R.S['userRole'] := LoginData.UserRole;
  R.S['passwordExpirationDate'] := LoginData.PasswordExpirationDate;
  R.S['apiKey'] := LoginData.APIKey;
  Response.Content := R.AsJSon(True, False);
  Response.ContentType := 'application/json';
end;

procedure TAPIDM.ReturnPingInformation;
var
  R: ISuperObject;
  LoginID: string;
begin
  ConnectMainDB;
  LoginID := SessionVars.Values['LoggedIn'];
  R := SO;
  R.S['result'] := 'ok';
  R.S['loginID'] := LoginID;
  R.S['fullName'] := GetShortNameForUser(LoginID);
  Response.Content := R.AsJSon(True, False);
  Response.ContentType := 'application/json';
end;

procedure TAPIDM.UseContext(ARequest: TWebRequest; AResponse: TWebResponse;
  AParams, ASessionVars: TStrings);
begin
  Request := ARequest;
  Response := AResponse;
  Params := AParams;
  SessionVars := ASessionVars;
end;

procedure TAPIDM.SetOpName(const OpName: string);
begin
  Assert(Assigned(Op), 'No active Operation defined');
  Op.OpName := OpName;
end;

function TAPIDM.GetIniBoolSetting(const Section, Ident: string; Default: Boolean): Boolean;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(IniFileName);
  try
    Result := Ini.ReadBool(Section, Ident, Default);
  finally
    FreeAndNil(Ini);
  end;
end;

function TAPIDM.GetStringIniSetting(const Section, Ident, Default: string): string;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(IniFileName);
  try
    Result := Ini.ReadString(Section, Ident, Default);
  finally
    FreeAndNil(Ini);
  end;
end;

function TAPIDM.CreateDataSetForSQL(const SelectSQL: string): TADQuery;
begin
  // This TDataSet gets freed when the TAPIDM is freed.
  Result := TADQuery.Create(Self);
  Result.Connection := Conn;
  Result.SQL.Text := SelectSQL;
  Result.Open;
end;

function TAPIDM.GetRightID(RightName: string): Integer;
const
  SQL = 'select right_id from right_definition where entity_data = ''%s''';
var
  DataSet: TDataSet;
begin
  Result := 0;
  DataSet := CreateDataSetForSQL(Format(SQL, [RightName]));
  if Dataset.RecordCount > 0 then
    Result := Dataset.FieldByName('right_id').AsInteger;
end;

function TAPIDM.CanI(const RightName: string; var LimitationString: string; EmpID: Integer): Boolean;
const
  SQL = 'select allowed, limitation from dbo.get_effective_rights(%d) where right_id = %d';
var
  RightID: Integer;
begin
  RightID := GetRightID(RightName);
  Result := False;
  CheckRights.Close;
  CheckRights.SQL.Text := Format(SQL, [EmpID, RightID]);
  CheckRights.Open;
  if (not CheckRights.Eof) and (CheckRights.FieldByName('allowed').AsString = 'Y') then begin
    Result := True;
    LimitationString := CheckRights.FieldByName('limitation').AsString;
  end;
end;

function TAPIDM.CanI(const RightName: string; EmpID: Integer): Boolean;
var
  Limitation: string;
begin
  Result := CanI(RightName, Limitation, EmpID);
end;

function TAPIDM.CanI(const RightName: string): Boolean;
begin
  Result := CanI(RightName, GetLoggedInEmpID(True));
end;

function TAPIDM.CanI(const RightName: string; var LimitationString: string): Boolean;
begin
  Result := CanI(RightName, LimitationString, GetLoggedInEmpID(True));
end;

function TAPIDM.GetWorkManagementLimit: TIntegerArray;
var
  Limit: string;
  Arr: TStringDynArray;
  Str: string;
begin
  if not CanI(RightTicketManagement, Limit) then
    raise EOdNotAllowed.Create('You do not have work management permission.');

  SetLength(Result, 0);
  if (Limit = '') or (Limit = '-') then
    Limit := IntToStr(GetLoggedInEmpID(True));

  Arr := SplitString(Limit, ',');
  for str in Arr do
    AddIntToArray(StrToIntDef(Trim(str), 0), Result);

  Assert((Length(Result) > 0), 'At least one element expected in GetWorkManagmentLimit result.');
end;

function TAPIDM.IsEventStoreEnabled: Boolean;
begin
  Result := GetIniBoolSetting('Features', 'StoreEvents', False);
end;

function TAPIDM.CanAccessAncestor(AllowedAncestors: TIntegerArray; AncestorID: Integer): Boolean;
begin
  Result := IntegerInArray(AncestorID, AllowedAncestors);
end;

procedure TAPIDM.RespondWithJsonFromSP(const StoredProcName, ParamName: string;
  const Value: Integer);
var
  Param: TParamRec;
begin
  Param.Name := ParamName;
  Param.ParamDataType := ftInteger;
  Param.ParamType := ptInput;
  RespondWithJsonFromMultiParamSP(StoredProcName, [Param], [Value]);
end;

procedure TAPIDM.RespondWithJsonFromMultiParamSP(const StoredProcName: string;
  Parameters: array of TParamRec; Values: array of Variant);
var
  I: Integer;
  SP: TADStoredProc;
begin
  Assert(Length(Values) = Length(Parameters));

  SP := TADStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.StoredProcName := StoredProcName;
    SP.FetchOptions.AutoClose := False;
    SP.FetchOptions.Mode := fmAll;
    SP.FetchOptions.AutoFetchAll := afAll;
    SP.FetchOptions.Unidirectional := True;
    for I := Low(Parameters) to High(Parameters) do
      SP.Params.CreateParam(Parameters[I].ParamDataType, Parameters[I].Name, Parameters[I].ParamType).Value := Values[I];

    RespondWithJsonFromDataSet(SP);
  finally
    FreeAndNil(SP);
  end;
end;

procedure TAPIDM.RespondWithJsonFromDataSet(Data: TADRdbmsDataSet);
begin
  Data.Open;
  Response.ContentType := 'application/json';
  Response.Content := FormatSqlJson(Data, []);
end;

end.
