unit EventSourceDMu;

interface

uses
  System.SysUtils, System.Classes, uADStanIntf, uADStanOption,
  uADStanParam, uADStanError, uADDatSManager, uADPhysIntf, uADDAptIntf,
  uADStanAsync, uADDAptManager, Data.DB, uADCompDataSet, uADCompClient,
  Variants, QMLogic2ServiceLib, EventSource, MSSQLEventSource,
  QMCommands, Thrift.Collections;

type
  TEventSourceDM = class(TDataModule)
  private
    {
    Do not assume Connection will always be a QM db connection. For now it is, but
    in the future it might change to store events in some other kind of database.
    }
    Connection: TADConnection;
    Store: IEventStore;
    function GetEventList: IEventList;
  public
    procedure Setup(Conn: TADConnection);
    procedure LogTicketAssignedEvent(Key: IAggrKey; Assignment: ILocateAssigned);
    procedure LogTicketScheduledEvent(Key: IAggrKey; Scheduled: ITicketScheduled);
  end;

implementation

uses AdUtils;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TEventSourceDM }
procedure TEventSourceDM.Setup(Conn: TADConnection);
begin
  Assert(Assigned(Conn));
  Connection := Conn;
  Store := nil;
  Store := TMSSQLEventStore.Create(nil, Connection);
end;

function TEventSourceDM.GetEventList: IEventList;
begin
  Result := TEventListImpl.Create;
  Result.Items := TThriftListImpl<IEvent>.Create;
end;

procedure TEventSourceDM.LogTicketAssignedEvent(Key: IAggrKey; Assignment: ILocateAssigned);
var
  EventList: IEventList;
  Event: IEvent;
begin
  EventList := GetEventList;
  Event := TEventImpl.Create;
  Event.Union := TEventUnionImpl.Create;
  Event.AggregateKey := Key;
  Event.Union.LocateAssigned := Assignment;
  EventList.Items.Add(Event);
  Store.SaveEvents(Event.AggregateKey, EventList, -1);
end;

procedure TEventSourceDM.LogTicketScheduledEvent(Key: IAggrKey; Scheduled: ITicketScheduled);
var
  EventList: IEventList;
  Event: IEvent;
begin
  EventList := GetEventList;
  Event := TEventImpl.Create;
  Event.Union := TEventUnionImpl.Create;
  Event.AggregateKey := Key;
  Event.Union.TicketScheduled := Scheduled;
  EventList.Items.Add(Event);
  Store.SaveEvents(Event.AggregateKey, EventList, -1);
end;

end.
