unit CommandBus;

interface

uses
  QMCommands;

type
  ICommandSender = interface
    procedure Send(Command: ICommand);
  end;

implementation

end.
