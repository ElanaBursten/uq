inherited TicketQryDM: TTicketQryDM
  OldCreateOrder = True
  object TicketSchedule: TADQuery
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    SQL.Strings = (
      
        'SELECT t.ticket_id,t.kind, l.assigned_to, a.workload_date, l.cli' +
        'ent_code'
      'FROM ticket t'
      'JOIN locate l on l.ticket_id = t.ticket_id'
      
        'JOIN assignment a on (a.locate_id = l.locate_id and a.locator_id' +
        ' = l.assigned_to)'
      'WHERE t.ticket_id = :ticket_id and a.active = 1 ')
    Left = 31
    Top = 24
    ParamData = <
      item
        Name = 'TICKET_ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
end
