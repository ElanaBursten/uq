unit CommandUtils;

interface

uses System.Rtti, System.TypInfo, Thrift.Protocol, Thrift.Collections,
  QMThriftUtils, QMApi, QMApiCommand;

function GetCommandType(Command: ICommand; out CommandType: string): IBase;
function NewCommand(const EmpID: Integer; const CreateDateTime: TDateTime): ICommand;
function NewCommandList: IQMApiCommands;
function NewCommandResponse: ICommandResponseUnion;
function NewSimpleCommandResponse(const Success: Boolean): ICommandResponseUnion;
function NewUnhandledCommandError(const CmdType: string): ICommandResponseUnion;
function NewCommandResponseList: IQMApiResponses;
function NewForeignKey(const ForeignType: TForeignType; const ForeignID: Integer): IForeignKey;

implementation

uses
  OdIsoDates;

function GetCommandType(Command: ICommand; out CommandType: string): IBase;
var
  CTX: TRttiContext;
  RT: TRttiType;
  PropArray: TArray<TRttiProperty>;
  Prop: TRttiProperty;
  IsSet: Boolean;
begin
  Result := nil;
  RT := CTX.GetType(TCommandUnionImpl.ClassInfo);
  PropArray := RT.GetProperties;
  for Prop in PropArray do begin
    if IsValueProperty(Prop, TCommandUnionImpl) then begin
      IsSet := TCommandUnionImpl(Command.Union).GetPropValue(EVENT_IS_SET + Prop.Name);
      if IsSet then begin
        CommandType := Prop.Name;
        Result := TCommandUnionImpl(Command.Union).GetPropValueAsIBase(CommandType);
        Break;
      end;
    end;
  end;
end;

function NewCommand(const EmpID: Integer; const CreateDateTime: TDateTime): ICommand;
begin
  Result := TCommandImpl.Create;
  Result.EmpID := EmpID;
  Result.CreateDateTime := IsoDateTimeToStr(CreateDateTime);
  Result.Union := TCommandUnionImpl.Create;
end;

function NewCommandResponse: ICommandResponseUnion;
begin
  Result := TCommandResponseUnionImpl.Create;
end;

function NewSimpleCommandResponse(const Success: Boolean): ICommandResponseUnion;
begin
  Result := NewCommandResponse;
  if Success then
    Result.Success := TCommandSuccessUnionImpl.Create
  else
    Result.Failure := TCommandFailureUnionImpl.Create;
end;

function NewUnhandledCommandError(const CmdType: string): ICommandResponseUnion;
var
  Error: IServerError;
begin
  Error := TServerErrorImpl.Create;
  Error.Description := CmdType + ' is an unrecognized command.';
  Result := NewCommandResponse;
  Result.Failure := TCommandFailureUnionImpl.Create;
  Result.Failure.ServerErrors := TThriftListImpl<IServerError>.Create;
  Result.Failure.ServerErrors.Add(Error);
end;

function NewCommandList: IQMApiCommands;
begin
  Result := TQMApiCommandsImpl.Create;
  Result.CommandList := TThriftListImpl<ICommand>.Create;
end;

function NewCommandResponseList: IQMApiResponses;
begin
  Result := TQMApiResponsesImpl.Create;
  Result.ResponseList := TThriftListImpl<ICommandResponseUnion>.Create;
end;

function NewForeignKey(const ForeignType: TForeignType; const ForeignID: Integer): IForeignKey;
begin
  Result := TForeignKeyImpl.Create;
  Result.ForeignType := ForeignType;
  Result.ID := ForeignID;
end;

end.
