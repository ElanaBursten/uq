@REM This runs the Thrift IDL compiler to create a gen-delphi\QMLogic2ServiceLib.pas file from QMLogic2ServiceLib.thrift.
@REM It should be run from the server2 dir. 
@REM If you want to know what the params do, type thrift-0.9.1.exe --help.

..\bin\thrift-0.9.1.exe -verbose -o .\ --gen delphi:register_types QMLogic2ServiceLib.thrift


@REM This runs the Thrift IDL compiler to create these Delphi source files from the thrift file (and referenced includes) being compiled below:
@REM gen-delphi\QMApiShared.pas
@REM gen-delphi\QMApiCommand.pas
@REM gen-delphi\QMApiQuery.pas 
@REM gen-delphi\QMApi.pas

..\bin\thrift-0.9.1.exe -verbose -r -o .\ --gen delphi:register_types QMApi.thrift

pause