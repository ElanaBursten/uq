//*********************************************************
//                   QMApiShared.thrift                    
//*********************************************************

# Thrift IDL file for a service. This file contains the shared types, structs, etc.
#
# This file is used by the Thrift compiler to generate Delphi code shared
# between servers and clients.
#
# Before running this file, you will need to have installed the thrift compiler
# (the version we are using is located in \bin).
#
# Sample command to generate the source:
# ..\bin\thrift-0.9.1.exe -verbose -r -o .\ --gen delphi:register_types QMApi.thrift
#
# Follow Thrift best practices here: http://diwakergupta.github.io/thrift-missing-guide/#_best_practices
# Most importantly, do not change the numeric tags for any existing fields!


//*********************************************************
# Custom Typedefs(type translations), Structs, and Containers
//*********************************************************

//Thrift does not have a DateTime data type, so we will use a string and create
// an alternate name for the type that makes it easier to read
typedef string datetimestring

//Thrift does not have a Currency data type, so we will use i64 and create
// an alternate name for the type that makes it easier to read
typedef i64 CurrencyScaled

// Replaces RemObjects Struct: LocateKey 
struct LocateKey {
  1: i32 TicketID; 
  2: string ClientCode; 
}
// Replaces RemObjects Struct: TicketKey 
struct TicketKey {
  1: i32 TicketID; 
}
// Replaces RemObjects Struct: GeneratedKey 
struct GeneratedKey {
  1: string TableName; 
  2: i32 TempKeyValue; 
  3: i32 KeyValue; 
}
// Replaces RemObjects Struct: ErrorReport 
struct ErrorReport {
  1: datetimestring OccurredWhen; 
  2: i32 Severity; 
  3: string ErrorText; 
  4: string Details; 
}
// Replaces RemObjects Struct: NVPair 
struct NVPair {
  1: string Name; 
  2: string Value; 
}
// Replaces RemObjects Array: NVPairList 
typedef list<NVPair> NVPairList
// Replaces RemObjects Struct: TimesheetRecord 
struct TimesheetRecord {
  1: i32 EntryID; 
  2: i32 EmpID; 
  3: datetimestring WorkDate; 
}
// Replaces RemObjects Struct: RequiredDocument 
struct RequiredDocument {
  1: i32 DocID; 
  2: i32 ForeignType; 
  3: i32 ForeignID; 
  4: i32 RequiredDocID; 
  5: string Comment; 
  6: i32 AddedByID; 
  7: datetimestring AddedDate; 
  8: bool Active; 
}
// Replaces RemObjects Struct: SyncError 
struct SyncError {
  1: string Message; 
  2: string TableName; 
  3: i32 PrimaryKeyID; 
}
// Replaces RemObjects Struct: BreakRuleResponse 
struct BreakRuleResponse {
  1: i32 ResponseID; 
  2: i32 EmpID; 
  3: i32 RuleID; 
  4: datetimestring ResponseDate; 
  5: string Response; 
  6: string ContactPhone; 
  7: i32 MessageDestID; 
}
// Replaces RemObjects Struct: NotesChange5 
struct NotesChange5 {
  1: i32 NotesID; 
  2: i32 ForeignID; 
  3: i32 ForeignType; 
  4: string Note; 
  5: datetimestring EntryDate; 
  6: i32 UID; 
  7: bool Active; 
  8: i32 SubType; 
}
// Replaces RemObjects Array: NotesChangeList5 
typedef list<NotesChange5> NotesChangeList5
// Replaces RemObjects Struct: WorkOrderChange5 
struct WorkOrderChange5 {
  1: i32 WorkOrderID; 
  2: string WorkDescription; 
  3: string WorkCross; 
  4: string StateHwyROW; 
  5: i32 RoadBoreCount; 
  6: i32 DrivewayBoreCount; 
  7: i32 AssignedToID; 
  8: string Status; 
  9: bool Closed; 
  10: datetimestring StatusDate; 
  11: string StatusedHow; 
  12: i32 StatusedByID; 
  13: NotesChangeList5 NotesChanges; 
  14: AddinInfoChangeList AddinInfoChanges; 
}
// Replaces RemObjects Array: WorkOrderChangeList5 
typedef list<WorkOrderChange5> WorkOrderChangeList5
// Replaces RemObjects Struct: TicketNote5 
struct TicketNote5 {
  1: i32 NoteID; 
  2: string ClientCode; 
  3: datetimestring EntryDate; 
  4: i32 EnteredByUID; 
  5: string NoteText; 
  6: i32 SubType; 
}
// Replaces RemObjects Array: TicketNoteList5 
typedef list<TicketNote5> TicketNoteList5
// Replaces RemObjects Struct: TicketChange15 
struct TicketChange15 {
  1: TicketKey TicketID; 
  2: i32 FollowupTypeID; 
  3: LocateChange6 LocateChange6; 
  4: TicketNoteList5 NewNotes; 
  5: LocateHour3 LocateHour3; 
  6: TicketAreaChange TicketAreaChange; 
  7: LocatePlatChange LocatePlatChange; 
  8: TicketInfoChange TicketInfoChange; 
  9: JobsiteArrivalChangeList ArrivalTimeChanges; 
  10: LocateFacilityChange LocateFacilityChange; 
  11: AddinInfoChangeList AddinInfoChanges; 
  12: GPSChange GPSChange; 
  13: datetimestring FollowupTransmitDate; 
  14: datetimestring FollowupDueDate; 
}
// Replaces RemObjects Array: TicketChangeList15 
typedef list<TicketChange15> TicketChangeList15
// Replaces RemObjects Struct: LocateFacilityChange2 
struct LocateFacilityChange2 {
  1: i32 LocateFacilityID; 
  2: i32 LocateID; 
  3: i32 Offset; 
  4: string FacilityType; 
  5: string FacilityMaterial; 
  6: string FacilitySize; 
  7: string FacilityPressure; 
  8: datetimestring AddedDate; 
  9: i32 AddedBy; 
  10: bool Active; 
  11: i32 ModifiedBy; 
  12: datetimestring ModifiedDate; 
  13: i32 DeletedBy; 
  14: datetimestring DeletedDate; 
}
// Replaces RemObjects Array: LocateFacilityChangeList2 
typedef list<LocateFacilityChange2> LocateFacilityChangeList2
// Replaces RemObjects Struct: EmployeeActivityChange3 
struct EmployeeActivityChange3 {
  1: i32 EmpActivityID; 
  2: i32 EmpID; 
  3: datetimestring ActivityDate; 
  4: string ActivityType; 
  5: string Details; 
  6: string ExtraDetails; 
  7: double Lat; 
  8: double Lng; 
}
// Replaces RemObjects Array: EmployeeActivityChangeList3 
typedef list<EmployeeActivityChange3> EmployeeActivityChangeList3
// Replaces RemObjects Struct: BreadCrumb 
struct BreadCrumb {
  1: i32 EmpID; 
  2: NVPairList BreadCrumbData; 
}
// Replaces RemObjects Array: BreadcrumbList 
typedef list<BreadCrumb> BreadcrumbList
// Replaces RemObjects Struct: WorkOrderOHMChange 
struct WorkOrderOHMChange {
  1: i32 WorkOrderID; 
  2: NVPairList OHMDetailsData; 
}
// Replaces RemObjects Array: WorkOrderOHMChangeList 
typedef list<WorkOrderOHMChange> WorkOrderOHMChangeList
// Replaces RemObjects Struct: EPRHistory 
struct EPRHistory {
  1: i32 history_id; 
  2: i32 ticket_id; 
  3: NVPairList EPRHistoryData; 
}
// Replaces RemObjects Array: EPRHistoryList 
typedef list<EPRHistory> EPRHistoryList
// Replaces RemObjects Struct: RiskHistory 
struct RiskHistory {
  1: i32 risk_history_id; 
  2: i32 ticket_id; 
  3: NVPairList RiskHistoryData; 
}
// Replaces RemObjects Array: RiskHistoryList 
typedef list<RiskHistory> RiskHistoryList
// Replaces RemObjects Struct: CustomForrmAnswer 
struct CustomForrmAnswer {
  1: NVPairList AnswerData; 
}
// Replaces RemObjects Array: CustomFormAnswerList 
typedef list<CustomForrmAnswer> CustomFormAnswerList
// Replaces RemObjects Struct: LoginData 
struct LoginData {
  1: i32 EmpID; 
  2: i32 UserID; 
  3: i32 NeedPasswordChange; 
  4: i32 EmpTypeID; 
  5: i32 ReportToEmpID; 
  6: string ShortName; 
  7: string UserRole; 
  8: string DisplayMessage; 
  9: datetimestring PasswordExpirationDate; 
}
// Replaces RemObjects Struct: VehicleUse 
struct VehicleUse {
  1: i32 VehicleUseID; 
  2: string SunType; 
  3: string MonType; 
  4: string TueType; 
  5: string WedType; 
  6: string ThuType; 
  7: string FriType; 
  8: string SatType; 
}
// Replaces RemObjects Struct: ComputerInformation 
struct ComputerInformation {
  1: string WindowsUser; 
  2: i32 OsPlatform; 
  3: i32 OsMajorVersion; 
  4: i32 OsMinorVersion; 
  5: i32 OsServicePackVersion; 
  6: string ComputerName; 
  7: string DellServiceTag; 
  8: string HotFixList; 
}
// Replaces RemObjects Struct: PendingUploadsStats 
struct PendingUploadsStats {
  1: i32 FileCount; 
  2: i32 FileTotalKB; 
  3: i32 LongestWaitMinutes; 
}
// Replaces RemObjects Struct: LoginData2 
struct LoginData2 {
  1: i32 EmpID; 
  2: i32 UserID; 
  3: i32 NeedPasswordChange; 
  4: i32 EmpTypeID; 
  5: i32 ReportToEmpID; 
  6: string ShortName; 
  7: string UserRole; 
  8: string DisplayMessage; 
  9: datetimestring PasswordExpirationDate; 
  10: string APIKey; 
}
// Replaces RemObjects Struct: NewClient 
struct NewClient {
  1: i32 parent_client_id; 
  2: NVPairList ClientData; 
}
