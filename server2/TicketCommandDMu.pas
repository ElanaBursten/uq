unit TicketCommandDMu;

interface

uses
  System.SysUtils, System.Classes, uADStanIntf, uADStanOption,
  uADStanParam, uADStanError, uADDatSManager, uADPhysIntf, uADDAptIntf,
  uADStanAsync, uADDAptManager, uADCompClient, Data.DB, uADCompDataSet,
  QMLogic2ServiceLib, BaseCommandDMu;

type
  TTicketCmdDM = class(TBaseCommandDM)
    UpdateAssignments: TADStoredProc;
    Locate: TADQuery;
  private
    function NewAggregateKey(const ID: Integer): IAggrKey;
  public
    procedure ReassignTickets;
  end;

implementation

uses
  IniFiles, QMConst, SQLJsonOutput, SuperObject, Variants, AdUtils, OdIsoDates;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TTicketCmdDM }
function TTicketCmdDM.NewAggregateKey(const ID: Integer): IAggrKey;
begin
  Result := TAggrKeyImpl.Create;
  Result.AggrType := TAggregateType.atTicket;
  Result.AggrID := ID;
end;

procedure TTicketCmdDM.ReassignTickets;
{TODO Kyle noted:
 Rather than a pluralized call for each kind of operation, it would save a lot of
 code and improve a lot of things to instead make it possible to call any set of ops.
}
var
  i, TicketID, FromLocatorID, ToLocatorID, ChangedBy, LocateID: Integer;
  WorkloadDate: Variant;
  MainObj: ISuperObject;
  TicketArray: TSuperArray;
  Assignment: ILocateAssigned;
begin

  { TODO: Once the QManager client is upgraded to support Thirft, this needs major refactoring! }

  MainDM.SetOpName('ReassignTickets');
  MainObj := SO(MainDM.Request.Content);
  TicketArray := MainObj.A['tickets'];
  Assignment := TLocateAssignedImpl.Create;

  for i := 0 to (TicketArray.Length - 1) do begin
    SetPhase(Format('Reassign Ticket %d of %d', [i+1, TicketArray.Length]));

    // TODO: All of these hardcoded Json* field names need to be replaced. They need to match the param names.
    {
    JsonTicketId = 'ticket_id';
    JsonFromLocator = 'from_locator';
    JsonToLocator = 'to_locator';
    JsonWorkDate = 'workload_date';
    JsonChangedBy = 'changed_by';
    JsonAssignedTo = 'assigned_to';
    JsonTicketKind = 'kind';
    }
    TicketID := TicketArray[i].AsObject.I[JsonTicketId];
    FromLocatorID := TicketArray[i].AsObject.I[JsonFromLocator];
    ToLocatorID := TicketArray[i].AsObject.I[JsonToLocator];
    ChangedBy := TicketArray[i].AsObject.I[JsonChangedBy];

    // TODO: Move this to a ScheduleTicketCommand instead of bundling scheduling and assigning together.
    if TicketArray[i].AsObject.Exists(JsonWorkDate) then
      WorkloadDate := IsoStrToDateTime(TicketArray[i].AsObject.S[JsonWorkDate])
    else
      WorkloadDate := Null;

    Op.Detail := Format('Get Locates for Ticket %d', [TicketID]);
    Locate.Close;
    Locate.Params.ParamValues['ticket_id'] := TicketID;
    Locate.Open;
    Op.Detail := Format('Update Locates for Ticket %d', [TicketID]);
    while not Locate.Eof do begin
      if not Locate.FieldByName('closed').AsBoolean and
         ((Locate.FieldByName('assigned_to').AsInteger = FromLocatorID) or (FromLocatorID = -1)) and
         (Locate.FieldByName('status').AsString <> NotAClientStatus) then begin

        LocateID := Locate.FieldByName('locate_id').AsInteger;
        UpdateAssignments.Params.ParamValues['@LocateID'] := LocateID;
        UpdateAssignments.Params.ParamValues['@LocatorID'] := ToLocatorID;
        UpdateAssignments.Params.ParamValues['@AddedBy'] := ChangedBy;
        UpdateAssignments.Params.ParamValues['@WorkloadDate'] := WorkloadDate;
        UpdateAssignments.ExecProc;
        if StoreEvents then begin
          Op.Detail := Format('Log Assigned Event for Ticket %d', [TicketID]);
          // TODO: Need a generic way to transform a Command Object into an Event Object
          Assignment.LocateID := LocateID;
          Assignment.FromLocatorID := FromLocatorID;
          Assignment.ToLocatorID := ToLocatorID;
          Assignment.ChangedByID := ChangedBy;
          Assignment.ChangedDate := IsoDateTimeToStr(Now);
          EventSourceDM.LogTicketAssignedEvent(NewAggregateKey(TicketID), Assignment);
        end;
      end;
      Locate.Next;
    end;
    Locate.Close;
  end;
end;

end.
