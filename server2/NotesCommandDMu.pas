unit NotesCommandDMu;

interface

uses
  System.SysUtils, System.Classes, InterfacedDataModuleU, BaseCommandDMu, QMCommands, QMApi,
  QMApiShared, QMApiCommand, ODBCCommandHandler, uADStanIntf, uADStanOption, uADStanParam,
  uADStanError, uADDatSManager, uADPhysIntf, uADDAptIntf, uADStanAsync, uADDAptManager, Data.DB,
  uADCompDataSet, uADCompClient;

type
  TAddNoteValidator = class(TBaseCommandValidator, ICommandValidator)
  protected
    procedure ValidateCommand; override;
  end;

  TAddNoteHandler = class(TODBCCommandHandler, ICommandHandler)
  protected
    function RunCommand(Cmd: ICommand): ICommandResponseUnion; override;
  end;

  TDeleteNoteHandler = class(TODBCCommandHandler, ICommandHandler)
    function RunCommand(Cmd: ICommand): ICommandResponseUnion; override;
  end;

  TNotesCommandDM = class(TBaseCommandDM)
    AddNotesSP: TADStoredProc;
  end;

implementation

uses
  OdMiscUtils, QMThriftUtils, CommandUtils;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TAddNoteValidator }

procedure TAddNoteValidator.ValidateCommand;
var
  AddNote: IAddNoteCommand;
begin
  inherited;
  Assert(Assigned(Cmd.Union.AddNote), 'AddNote command object is undefined.');

  AddNote := Cmd.Union.AddNote;
  if AddNote.NotesID > 0 then
    AddValidationError('Editing existing notes is not supported.');
  if AddNote.ForeignKey.ID < 0 then
    AddValidationError('Notes Foreign ID values cannot be less than zero.');
  if (AddNote.ForeignKey.ForeignType < TForeignType.qmftTicket) or
    (AddNote.ForeignKey.ForeignType > TForeignType.qmftWorkOrder) then
    AddValidationError('Notes Foreign Type is not within the allowed range.');
  if IsEmpty(AddNote.Note) then
    AddValidationError('Notes Note text cannot be blank.');
end;

{ TAddNoteHandler }

function TAddNoteHandler.RunCommand(Cmd: ICommand): ICommandResponseUnion;
begin
  Assert(Assigned(Cmd.Union.AddNote), 'AddNote command object is undefined.');
  with CmdDM as TNotesCommandDM do begin
    Op.Phase := 'Setting params';
    AssignParamsFromCommand(AddNotesSP.Params, Cmd);
    Op.Phase := 'Execute SP';
    AddNotesSP.Open; // uses Open instead of ExecProc so we can get its returned Dataset
    Op.Phase := 'Generate Key';
    Result := NewGeneratedKeyResponse(AddNotesSP);
  end;
end;

{ TDeleteNoteHandler }

function TDeleteNoteHandler.RunCommand(Cmd: ICommand): ICommandResponseUnion;
begin
  Assert(Assigned(Cmd.Union.DeleteNote), 'DeleteNote command object is undefined.');
  InactivateRow('notes', 'notes_id', Cmd.Union.DeleteNote.NotesID);
  Result := NewSimpleCommandResponse(True);
end;

end.
