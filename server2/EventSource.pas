unit EventSource;

interface

uses
  SysUtils, Generics.Collections,
  Thrift.Collections, QMLogic2ServiceLib, EventUtils;

type
  IEventPublisher = interface
    procedure Publish(Event: IEvent);
  end;

  IEventStore = interface
    procedure SaveEvents(Key: IAggrKey; EventList: IEventList; ExpectedVersion: Integer);
    function GetEventsForAggregate(Key: IAggrKey): IEventList;
    function GetEventsSince(const EventID: string; const Limit: Integer): IEventList;
  end;

  IEventDescriptor = interface
    function GetEventData: IEvent;
    procedure SetEventData(const Value: IEvent);
    function GetAggKey: IAggrKey;
    procedure SetAggKey(const Value: IAggrKey);
    function GetVersion: Integer;
    procedure SetVersion(const Value: Integer);
    property EventData: IEvent read GetEventData write SetEventData;
    property AggKey: IAggrKey read GetAggKey write SetAggKey;
    property Version: Integer read GetVersion write SetVersion;
  end;

  TEventDescriptor = class(TInterfacedObject, IEventDescriptor)
  strict private
    FEventData: IEvent;
    FAggKey: IAggrKey;
    FVersion: Integer;
  protected
    function GetEventData: IEvent;
    procedure SetEventData(const Value: IEvent);
    function GetAggKey: IAggrKey;
    procedure SetAggKey(const Value: IAggrKey);
    function GetVersion: Integer;
    procedure SetVersion(const Value: Integer);
  public
    property EventData: IEvent read GetEventData write SetEventData;
    property AggKey: IAggrKey read GetAggKey write SetAggKey;
    property Version: Integer read GetVersion write SetVersion;
    constructor Create(AKey: IAggrKey; AEventData: IEvent; AVersion: Integer = -1);
  end;

  TBaseEventStore = class(TInterfacedObject, IEventStore)
  private
    Publisher: IEventPublisher;
  protected
    function LoadEventDescriptorsForAggregate(Key: IAggrKey): IThriftList<IEventDescriptor>; virtual; abstract;
    function LoadEventDescriptorsSince(const EventID: string; const Limit: Integer): IThriftList<IEventDescriptor>; virtual; abstract;
    procedure PersistEventDescriptors(Key: IAggrKey; NewEventDescriptors: IThriftList<IEventDescriptor>; ExpectedVersion: Integer); virtual; abstract;

    procedure SaveEvents(Key: IAggrKey; EventList: IEventList; ExpectedVersion: Integer=-1); // -1 means no ExpectedVersion check.
    function GetEventsForAggregate(Key: IAggrKey): IEventList;
    function GetEventsSince(const EventID: string; const Limit: Integer = 0): IEventList;
  public
    constructor Create(APublisher: IEventPublisher); virtual;
  end;

  // Exceptions:
  EEventStoreError = class(Exception);
  EAggregateNotFound = class(EEventStoreError);
  EConcurrencyException = class(EEventStoreError);
  EBadAggregateKey = class(EEventStoreError);

implementation

constructor TEventDescriptor.Create(AKey: IAggrKey; AEventData: IEvent; AVersion: Integer);
begin
  inherited Create();
  AggKey := TAggrKeyImpl(AKey);
  EventData := AEventData;
  Version := AVersion;
end;

constructor TBaseEventStore.Create(APublisher: IEventPublisher);
begin
  Publisher := APublisher;
end;

function TBaseEventStore.GetEventsForAggregate(Key: IAggrKey): IEventList;
var
  EventDescriptors: IThriftList<IEventDescriptor>;
  EvDesc: IEventDescriptor;
begin
  EventDescriptors := LoadEventDescriptorsForAggregate(Key);
  if (EventDescriptors = nil) or (EventDescriptors.Count < 1) then
    raise EAggregateNotFound.CreateFmt('No events for aggregate %s', [TAggrKeyImpl(Key).AsString]);

  Result := TEventListImpl.Create;
  Result.Items := TThriftListImpl<IEvent>.Create;
  for EvDesc in EventDescriptors do
    Result.Items.Add(EvDesc.EventData);
end;

function TBaseEventStore.GetEventsSince(const EventID: string; const Limit: Integer): IEventList;
var
  EventDescriptors: IThriftList<IEventDescriptor>;
  EvDesc: IEventDescriptor;
begin
  Result := TEventListImpl.Create;
  Result.Items := TThriftListImpl<IEvent>.Create;
  EventDescriptors := LoadEventDescriptorsSince(EventID, Limit);
  if EventDescriptors = nil then
    Exit;

  for EvDesc in EventDescriptors do
    Result.Items.Add(EvDesc.EventData);
end;

procedure TBaseEventStore.SaveEvents(Key: IAggrKey;
  EventList: IEventList; ExpectedVersion: Integer);
var
  EventDescriptors: IThriftList<IEventDescriptor>;
  I: Integer;
  Event: IEvent;
begin
  EventDescriptors := TThriftListImpl<IEventDescriptor>.Create;
  I := ExpectedVersion;
  for Event in EventList.Items do begin
    Inc(I);
    EventDescriptors.Add(TEventDescriptor.Create(Key, Event, I));
  end;

  PersistEventDescriptors(Key, EventDescriptors, ExpectedVersion);
  // The Events were saved, so publish their availabilty.
  if Assigned(Publisher) then
    for Event in EventList.Items do
      Publisher.Publish(Event);
end;

function TEventDescriptor.GetAggKey: IAggrKey;
begin
  Result := FAggKey;
end;

function TEventDescriptor.GetEventData: IEvent;
begin
  Result := FEventData;
end;

function TEventDescriptor.GetVersion: Integer;
begin
  Result := FVersion;
end;

procedure TEventDescriptor.SetAggKey(const Value: IAggrKey);
begin
  if Value <> FAggKey then
    FAggKey := Value;
end;

procedure TEventDescriptor.SetEventData(const Value: IEvent);
begin
  if Value <> FEventData then
    FEventData := Value;
end;

procedure TEventDescriptor.SetVersion(const Value: Integer);
begin
  if Value <> FVersion then
    FVersion := Value;
end;

end.
