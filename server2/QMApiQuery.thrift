//*********************************************************
//                   QMApiQuery.thrift                    
//*********************************************************

# Thrift IDL file for a service. This file contains the shared types, structs, etc.
#
# This file is used by the Thrift compiler to generate Delphi code shared
# between servers and clients.
#
# Before running this file, you will need to have installed the thrift compiler
# (the version we are using is located in \bin).
#
# Sample command to generate the source:
# ..\bin\thrift-0.9.1.exe -verbose -r -o .\ --gen delphi:register_types QMApi.thrift
#
# Follow Thrift best practices here: http://diwakergupta.github.io/thrift-missing-guide/#_best_practices
# Most importantly, do not change the numeric tags for any existing fields!

/*

 * Thrift files can reference other Thrift files to include common struct
 * and service definitions. These are found using the current path, or by
 * searching relative to any paths specified with the -I compiler flag.
 *
 * Included objects are accessed using the name of the .thrift file as a
 * prefix. i.e. MyServiceShared.SharedObject
 */
include "QMApiShared.thrift"

// Replaces RemObjects Operation: Ping 
struct Ping {
  1: string AnyText; 
}
// Replaces RemObjects Operation: TicketSearch6 
struct TicketSearch {
  1: string TicketNumber; 
  2: string Street; 
  3: string City; 
  4: string State; 
  5: string County; 
  6: string WorkType; 
  7: string TicketType; 
  8: string DoneFor; 
  9: string Company; 
  10: QMApiShared.datetimestring DueDateFrom; 
  11: QMApiShared.datetimestring DueDateTo; 
  12: QMApiShared.datetimestring RecvDateFrom; 
  13: QMApiShared.datetimestring RecvDateTo; 
  14: string CallCenter; 
  15: string TermCode; 
  16: i32 Attachments; 
  17: i32 ManualTickets; 
  18: string Priority; 
  19: i32 UtilityCo; 
}
// Replaces RemObjects Operation: WorkOrderSearch 
struct WorkOrderSearch {
  1: string TicketNumber; 
  2: string WorkOrderNumber; 
  3: string Street; 
  4: string City; 
  5: string State; 
  6: string County; 
  7: string WOSource; 
  8: string ClientName; 
  9: string Company; 
  10: QMApiShared.datetimestring DueDateFrom; 
  11: QMApiShared.datetimestring DueDateTo; 
  12: QMApiShared.datetimestring TransmitDateFrom; 
  13: QMApiShared.datetimestring TransmitDateTo; 
  14: string Kind; 
  15: i32 Tickets; 
}
// Replaces RemObjects Operation: DamageInvoiceSearch3 
struct DamageInvoiceSearch {
  1: string InvoiceNumber; 
  2: string Company; 
  3: string City; 
  4: string State; 
  5: i32 UQDamageID; 
  6: QMApiShared.CurrencyScaled Amount; 
  7: string Comment; 
  8: QMApiShared.datetimestring RecvDateFrom; 
  9: QMApiShared.datetimestring RecvDateTo; 
  10: QMApiShared.datetimestring InvoiceDateFrom; 
  11: QMApiShared.datetimestring InvoiceDateTo; 
  12: string InvoiceCode; 
}
// Replaces RemObjects Operation: DamageLitigationSearch3 
struct DamageLitigationSearch {
  1: i32 UQDamageID; 
  2: i32 LitigationID; 
  3: string FileNumber; 
  4: string Plaintiff; 
  5: i32 CarrierID; 
  6: string Attorney; 
  7: string Party; 
  8: string Demand; 
  9: string Accrual; 
  10: string Settlement; 
  11: QMApiShared.datetimestring ClosedDateFrom; 
  12: QMApiShared.datetimestring ClosedDateTo; 
}
// Derived from Result Flag in RemObjects Operation: Login 
struct LoginResponse {
  1: QMApiShared.LoginData Result; 
}
// Replaces RemObjects Operation: Login 
struct Login {
  1: string UserName; 
  2: string Password; 
  3: string AppVersion; 
}
// Replaces RemObjects Operation: GetHierarchy2 
struct Hierarchy {
  1: bool IncludePeers; 
  2: bool IncludeInactive; 
}
// Replaces RemObjects Operation: MultiOpenTotals6 
struct MultiOpenTotals {
  1: string Managers; 
}
// Replaces RemObjects Operation: GetTableData 
struct TableData {
  1: string TableName; 
  2: QMApiShared.datetimestring UpdatesSince; 
}
// Replaces RemObjects Operation: GetAssets 
struct Assets {
  1: i32 EmpID; 
}
// Replaces RemObjects Operation: GetRights 
struct Rights {
  1: i32 EmpID; 
}
// Replaces RemObjects Operation: SyncDown6 
struct SyncDown {
  1: string UpdatesSince; 
  2: string SyncRequestID; 
  3: string ClientVersion; 
  4: QMApiShared.datetimestring LocalTime; 
  5: i32 UTCBias; 
  6: string TicketInCachePacked; 
  7: QMApiShared.ComputerInformation ComputerInfo; 
  8: i32 SentBytes; 
  9: QMApiShared.NVPairList AdditionalInfo; 
  10: string DamageInCachePacked; 
  11: string WorkOrderInCachePacked; 
}
// Replaces RemObjects Operation: GetTicket2
/* Purpose: Get all details (other than history) about a single ticket. The response contains data 
from the QM ticket table as well as these related tables:
locate
response_log
notes
locate_hours
attachment
locate_plat
ticket_version
jobsite_arrival
locate_facility.
It also includes data from the following reference tables that changed on the server since the 
client application�s last successful sync operation:
client
call_center
employee
reference
statuslist
status_group
status_group_item.*/ 
struct Ticket {
  1: i32 TicketID; 
  2: QMApiShared.datetimestring UpdatesSince; 
}
// Replaces RemObjects Operation: GetTicketHistory
/* Purpose: Get all history about a single ticket. The response contains data related to the 
specified ticket_id from these QM tables:
ticket_version
ticket_ack
ticket_info
assignment
locate_status
locate_facility
jobsite_arrival
response_log
email_queue_result
billing_header
billing_detail
billing_invoice
*/ 
struct TicketHistory {
  1: i32 TicketID; 
}
// Replaces RemObjects Operation: GetWorkOrderHistory 
struct WorkOrderHistory {
  1: i32 WorkOrderID; 
}
// Replaces RemObjects Operation: GetDamage2 
struct Damage {
  1: i32 DamageID; 
}
// Replaces RemObjects Operation: GetDamageInvoice 
struct DamageInvoice {
  1: i32 InvoiceID; 
}
// Replaces RemObjects Operation: GetDamageLitigation 
struct DamageLitigation {
  1: i32 LitigationID; 
}
// Replaces RemObjects Operation: GetWorkOrder 
struct WorkOrder {
  1: i32 WorkOrderID; 
}
// Replaces RemObjects Operation: GetTicketsForLocator 
struct TicketsForLocator {
  1: i32 LocatorID; 
  2: i32 NumDays; 
  3: bool OpenOnly; 
  4: bool Unacknowledged; 
}
// Replaces RemObjects Operation: GetDamagesForLocator 
struct DamagesForLocator {
  1: i32 LocatorID; 
  2: i32 NumDays; 
  3: bool OpenOnly; 
}
// Replaces RemObjects Operation: GetWorkOrdersForEmployee 
struct WorkOrdersForEmployee {
  1: i32 LocatorID; 
  2: i32 NumDays; 
  3: bool OpenOnly; 
  4: bool Unacknowledged; 
}
// Replaces RemObjects Operation: ExportTimesheets 
struct ExportTimesheets {
  1: string ProfitCenter; 
  2: QMApiShared.datetimestring WeekEnding; 
}
// Replaces RemObjects Operation: GetColumnData 
struct ColumnData {
  1: string TableName; 
  2: string KeyField; 
  3: string Columns; 
  4: QMApiShared.datetimestring UpdatesSince; 
  5: bool NonNullOnly; 
}
// Replaces RemObjects Operation: GetBillingAdjustment 
struct BillingAdjustment {
  1: i32 ID; 
}
// Replaces RemObjects Operation: BillingRunSearch 
struct BillingRunSearch {
  1: QMApiShared.NVPairList Params; 
}
// Replaces RemObjects Operation: DamageThirdPartySearch 
struct DamageThirdPartySearch {
  1: i32 UQDamageID; 
  2: i32 ThirdPartyID; 
  3: i32 CarrierID; 
  4: string Claimant; 
  5: string Address; 
  6: string City; 
  7: string State; 
  8: string ClaimStatus; 
  9: string ProfitCenter; 
  10: string UtilityCoDamaged; 
  11: QMApiShared.datetimestring NotifiedFromDate; 
  12: QMApiShared.datetimestring NotifiedToDate; 
}
// Replaces RemObjects Operation: GetDamageThirdParty 
struct DamageThirdParty {
  1: i32 ThirdPartyID; 
}
// Replaces RemObjects Operation: GetMessageRecip 
struct MessageRecip {
  1: i32 MessageID; 
}
// Replaces RemObjects Operation: DamageSearch9 
struct DamageSearch {
  1: string TicketNumber; 
  2: i32 UQDamageID; 
  3: QMApiShared.datetimestring DamageDateFrom; 
  4: QMApiShared.datetimestring DamageDateTo; 
  5: QMApiShared.datetimestring DueDateFrom; 
  6: QMApiShared.datetimestring DueDateTo; 
  7: string Investigator; 
  8: string Excavator; 
  9: string Location; 
  10: string City; 
  11: string State; 
  12: string ProfitCenter; 
  13: string DamageType; 
  14: string UtilityCoDamaged; 
  15: string ClientClaimID; 
  16: string WorkToBeDone; 
  17: bool IncludeUQResp; 
  18: bool IncludeExcvResp; 
  19: bool IncludeUtilityResp; 
  20: i32 InvestigatorID; 
  21: i32 Attachments; 
  22: i32 Invoices; 
  23: string ClaimStatus; 
  24: bool IncludeEstimates; 
  25: string InvoiceCode; 
  26: string ExcavationType; 
  27: string Locator; 
}
// Replaces RemObjects Operation: GetAttachmentList 
struct AttachmentList {
  1: i32 ForeignType; 
  2: i32 ForeignID; 
  3: bool ImagesOnly; 
}
// Replaces RemObjects Operation: GetTicketID 
struct TicketID {
  1: string TicketNumber; 
  2: string CallCenter; 
  3: QMApiShared.datetimestring TransmitDateFrom; 
  4: QMApiShared.datetimestring TransmitDateTo; 
  5: i32 DamageId; 
}
// Replaces RemObjects Operation: GetDamageIDForUQDamageID 
struct DamageIDForUQDamageID {
  1: i32 UQDamageID; 
}
// Replaces RemObjects Operation: GetPrintableAttachmentList 
struct PrintableAttachmentList {
  1: i32 ForeignType; 
  2: i32 ForeignID; 
}
// Replaces RemObjects Operation: GetTicketActivities 
struct TicketActivities {
  1: i32 EmpID; 
  2: i32 NumDays; 
}
// Replaces RemObjects Operation: SearchTimesheetsForProfitCenter 
struct SearchTimesheetsForProfitCenter {
  1: i32 SearchType; 
  2: QMApiShared.datetimestring DateStart; 
  3: QMApiShared.datetimestring DateStop; 
  4: i32 StartEmpId; 
  5: i32 LevelLimit; 
  6: string ProfitCenter; 
}
// Replaces RemObjects Operation: GetEmployeeFloatingHolidays 
struct EmployeeFloatingHolidays {
  1: i32 EmployeeID; 
  2: i32 Year; 
  3: QMApiShared.datetimestring WeekStart; 
  4: QMApiShared.datetimestring WeekEnd; 
}
// Derived from Result Flag in RemObjects Operation: Login2 
struct Login2Response {
  1: QMApiShared.LoginData2 Result; 
}
// Replaces RemObjects Operation: Login2 
struct Login2 {
  1: string UserName; 
  2: string Password; 
  3: string AppVersion; 
}
struct QueryStringResponse {
  1: string Result; 
}
struct QueryIntegerResponse {
  1: i32 Result; 
}
