@echo off
rem
rem This test script needs curl (http://curl.haxx.se/download.html)
rem and expects it to be in the c:\curl dir. It also expects the web service
rem to be on localhost:5041. You can change the settings below to match your environment.
rem If you get 403 errors, you need a valid AUTH; get one with a query like this:
rem select login_id + ':' + api_key as auth from users where login_id = 'TAlvarez' -- or some other login
rem

setlocal
set URL=http://localhost:5041/api/v1/ticket/reassign
set AUTH=TAlvarez:A4FD894064A8BDCB927C6023F7CB241BE96D6DBA
set CURLCMD=C:\curl\curl.exe --user %AUTH% --data

echo Try without a user (it should return an error message)
C:\curl\curl --data {"tickets":[{"ticket_id":1125,"changed_by":3512,"to_locator":6098,"from_locator":6037}]} %URL% || goto :error 
echo .
echo Try an invalid user auth (it should return an error message)
C:\curl\curl --data {"tickets":[{"ticket_id":1125,"changed_by":3512,"to_locator":6098,"from_locator":6037}]} --user SHull:50AC39C16447027D511DD3E595D7CDFAE08409C2 %URL% || goto :error 

echo .
echo Reassign 100 tickets to employee 6037
%CURLCMD% {"tickets":[{"ticket_id":1003,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1004,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1005,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1008,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1009,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1011,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1012,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1013,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1014,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1016,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1017,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1018,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1019,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1020,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1021,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1022,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1023,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1024,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1025,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1026,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1027,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1028,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1029,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1030,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1031,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1032,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1033,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1034,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1035,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1036,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1037,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1038,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1039,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1040,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1041,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1042,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1043,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1044,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1045,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1046,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1047,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1048,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1049,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1050,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1051,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1052,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1053,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1054,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1055,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1056,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1057,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1058,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1059,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1060,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1061,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1062,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1063,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1064,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1065,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1066,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1067,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1068,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1069,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1070,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1071,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1072,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1073,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1074,"changed_by":3512,"to_locator":6037,"from_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1082,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1084,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1088,"changed_by":3512,"to_locator":6037,"from_locator":6062}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1122,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1123,"changed_by":3512,"to_locator":6037,"from_locator":14052}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1132,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1133,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1136,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1152,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1159,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1160,"changed_by":3512,"to_locator":6037,"from_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1177,"changed_by":3512,"to_locator":6037,"from_locator":7084}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1180,"changed_by":3512,"to_locator":6037,"from_locator":10934}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1181,"changed_by":3512,"to_locator":6037,"from_locator":8100}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1182,"changed_by":3512,"to_locator":6037,"from_locator":7084}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1183,"changed_by":3512,"to_locator":6037,"from_locator":7072}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1184,"changed_by":3512,"to_locator":6037,"from_locator":7192}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1185,"changed_by":3512,"to_locator":6037,"from_locator":7072}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1186,"changed_by":3512,"to_locator":6037,"from_locator":7072}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1230,"changed_by":3512,"to_locator":6037,"from_locator":9764}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1239,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1240,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1242,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1243,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1244,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1245,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1246,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1247,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1255,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1256,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1258,"changed_by":3512,"to_locator":6037,"from_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1276,"changed_by":3512,"to_locator":6037,"from_locator":3886}]} %URL% || goto :error

echo .
echo Reassign those 100 tickets from 6037 back to the original employee
%CURLCMD% {"tickets":[{"ticket_id":1003,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1004,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1005,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1008,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1009,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1011,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1012,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1013,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1014,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1016,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1017,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1018,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1019,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1020,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1021,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1022,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1023,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1024,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1025,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1026,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1027,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1028,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1029,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1030,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1031,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1032,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1033,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1034,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1035,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1036,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1037,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1038,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1039,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1040,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1041,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1042,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1043,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1044,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1045,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1046,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1047,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1048,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1049,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1050,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1051,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1052,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1053,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1054,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1055,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1056,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1057,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1058,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1059,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1060,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1061,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1062,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1063,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1064,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1065,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1066,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1067,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1068,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1069,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1070,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1071,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1072,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1073,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1074,"changed_by":3512,"from_locator":6037,"to_locator":10432}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1082,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1084,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1088,"changed_by":3512,"from_locator":6037,"to_locator":6062}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1122,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1123,"changed_by":3512,"from_locator":6037,"to_locator":14052}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1132,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1133,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1136,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1152,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1159,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1160,"changed_by":3512,"from_locator":6037,"to_locator":6098}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1177,"changed_by":3512,"from_locator":6037,"to_locator":7084}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1180,"changed_by":3512,"from_locator":6037,"to_locator":10934}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1181,"changed_by":3512,"from_locator":6037,"to_locator":8100}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1182,"changed_by":3512,"from_locator":6037,"to_locator":7084}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1183,"changed_by":3512,"from_locator":6037,"to_locator":7072}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1184,"changed_by":3512,"from_locator":6037,"to_locator":7192}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1185,"changed_by":3512,"from_locator":6037,"to_locator":7072}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1186,"changed_by":3512,"from_locator":6037,"to_locator":7072}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1230,"changed_by":3512,"from_locator":6037,"to_locator":9764}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1239,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1240,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1242,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1243,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1244,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1245,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1246,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1247,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1255,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1256,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1258,"changed_by":3512,"from_locator":6037,"to_locator":1006}]} %URL% || goto :error
%CURLCMD% {"tickets":[{"ticket_id":1276,"changed_by":3512,"from_locator":6037,"to_locator":3886}]} %URL% || goto :error

:success
echo .
echo Success
pause
exit

:error
echo .
echo Error
pause
exit /b 1
