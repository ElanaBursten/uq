unit MSSqlEventSource;

interface

uses
  SysUtils, Generics.Collections, Variants, Data.DB,
  uADCompClient, uADCompDataSet, uADStanError, Thrift.Collections,
  QMLogic2ServiceLib, EventSource, AdUtils, QMThriftUtils, EventUtils;

type
  TMSSQLEventStore = class(TBaseEventStore, IEventStore)
  private
    Conn: TADConnection;
  protected
    function GetMaxVersion(Key: IAggrKey): Integer;
    function LoadEventDescriptorsForAggregate(Key: IAggrKey): IThriftList<IEventDescriptor>; override;
    function LoadEventDescriptorsSince(const EventID: string; const Limit: Integer = 0): IThriftList<IEventDescriptor>; override;
    procedure PersistEventDescriptors(Key: IAggrKey; NewEventDescriptors: IThriftList<IEventDescriptor>; ExpectedVersion: Integer); override;
  public
    constructor Create(APublisher: IEventPublisher; AConnection: TADConnection); reintroduce; overload; virtual;
  end;

implementation

{ TMSSQLEventStore }

constructor TMSSQLEventStore.Create(APublisher: IEventPublisher;
  AConnection: TADConnection);
begin
  inherited Create(APublisher);
  Conn := AConnection;
end;

function TMSSQLEventStore.LoadEventDescriptorsSince(const EventID: string;
  const Limit: Integer): IThriftList<IEventDescriptor>;
const
  SQL = 'SELECT %s * FROM event_log WHERE (event_id > :EventID OR ' +
        ':EventID is null) ORDER BY event_id';
var
  Data: TADQuery;
  LimitStr: string;
  Ev: IEvent;
  Key: IAggrKey;
begin
  Result := nil;
  LimitStr := '';
  if Limit > 0 then
    LimitStr := 'TOP(' + IntToStr(Limit) + ')';
  Data := TADQuery.Create(nil);
  try
    Data.Connection := Conn;
    Data.SQL.Text := Format(SQL, [LimitStr]);
    Data.ParamByName('EventID').DataType := ftInteger;
    if EventID = '' then
      Data.ParamByName('EventID').Clear
    else
      Data.ParamByName('EventID').AsInteger := StrToInt(EventID);
    Data.Open;
    if not Data.Eof then
      Result := TThriftListImpl<IEventDescriptor>.Create;
    while not Data.Eof do begin
      Ev := TEventImpl.Create;
      Key := TAggrKeyImpl.Create;
      Key.AggrType := TAggregateType(Data.FieldByName('aggregate_type').AsInteger);
      Key.AggrID := Data.FieldByName('aggregate_id').AsInteger;
      DeserializeFromJSON(Data.FieldByName('event_data').AsString, Ev);
      Ev.EventID := Data.FieldByName('event_id').AsString;
      Result.Add(TEventDescriptor.Create(Key, Ev, Data.FieldByName('version_num').AsInteger));
      Data.Next;
    end;
  finally
    FreeAndNil(Data);
  end;
end;

function TMSSQLEventStore.GetMaxVersion(Key: IAggrKey): Integer;
const
  SQL = 'SELECT MAX(version_num) FROM event_log '+
        'WHERE aggregate_type=%d and aggregate_id=%d';
var
  MaxVer: Variant;
begin
  Result := 0;
  MaxVer := Conn.ExecSQLScalar(Format(SQL, [Integer(Key.AggrType), Key.AggrID]));
  if MaxVer <> Null then
    Result := MaxVer;
end;

function TMSSQLEventStore.LoadEventDescriptorsForAggregate(Key: IAggrKey): IThriftList<IEventDescriptor>;
const
  SQL = 'SELECT * FROM event_log ' +
        'WHERE aggregate_type = :AggrType AND aggregate_id = :AggrID ' +
        'ORDER BY version_num';
var
  Data: TADQuery;
  Ev: IEvent;
begin
  Result := nil;
  Data := TADQuery.Create(nil);
  try
    Data.Connection := Conn;
    Data.Open(SQL, [Integer(Key.AggrType), Key.AggrID]);
    if not Data.Eof then
      Result := TThriftListImpl<IEventDescriptor>.Create;
    while not Data.Eof do begin
      Ev := TEventImpl.Create;
      DeserializeFromJSON(Data.FieldByName('event_data').AsString, Ev);
      Ev.EventID := Data.FieldByName('event_id').AsString;
      Result.Add(TEventDescriptor.Create(Key, Ev,
        Data.FieldByName('version_num').AsInteger));
      Data.Next;
    end;
  finally
    FreeAndNil(Data);
  end;
end;

procedure TMSSQLEventStore.PersistEventDescriptors(Key: IAggrKey;
  NewEventDescriptors: IThriftList<IEventDescriptor>; ExpectedVersion: Integer);
const
  SQL = 'INSERT INTO event_log (aggregate_type, aggregate_id, version_num, event_data) ' +
        'VALUES (:aggregate_type, :aggregate_id, :version_num, :event_data)';
var
  Event: IEventDescriptor;
  Data: TADQuery;
  Version: Integer;
begin
  Assert(Conn.InTransaction, 'PersistEventDescriptors can only be executed in an active transaction.');
  Assert(Assigned(NewEventDescriptors), 'NewEventDescriptors must be assigned.');

  Version := GetMaxVersion(Key);
  if (ExpectedVersion > -1) and (ExpectedVersion <> Version) then
    raise EConcurrencyException.CreateFmt('Event store concurrency ' +
      'error for Aggregate: %s. Expected Ver: %d; Actual Ver: %d.',
      [TAggrKeyImpl(Key).AsString, ExpectedVersion, Version]);

  Data := TADQuery.Create(nil);
  try
    Data.Connection := Conn;
    Data.SQL.Text := SQL;
    for Event in NewEventDescriptors do begin
      try
        Inc(Version);
        Data.ExecSQL('', [Integer(Key.AggrType), Key.AggrID, Version,
          SerializeToJSON(Event.EventData)]);
      except
        on E: EADDBEngineException do begin
        { If we get a duplicate key error here, that means there was
          a concurrency error (an existing row for the same Aggregate & Version).
          Raise EConcurrencyException instead. Handle the exception by trying to
          save the events again.
        }
          if E.Kind = ekUKViolated then
            raise EConcurrencyException.CreateFmt('Event store concurrency ' +
              'error for Aggregate: %s, Ver: %d. %s',
              [TAggrKeyImpl(Key).AsString, Version, E.Message])
          else
            raise;
        end;
      end;
    end;
  finally
    FreeAndNil(Data);
  end;
end;

end.
