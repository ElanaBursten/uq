unit ODBCCommandHandler;

interface

uses System.SysUtils, System.Classes, System.Types, System.StrUtils, Data.DB, uADCompClient,
  uADStanParam, InterfacedDataModuleU, QMApi, QMApiShared, QMCommands, OperationTracking,
  BaseCommandDMu, Thrift.Protocol, APIDMu;

type
  TODBCCommandHandler = class(TBaseCommandHandler, ICommandHandler)
  strict private
    DMClass: TCommandDMClass;
  protected
    MainDM: TAPIDM;
    CmdDM: TBaseCommandDM;

    function GetDeleteSQL(const TableName, IDColName: string; const ID: Integer): string; virtual;
    procedure InactivateRow(const TableName, IDColName: string; const ID: Integer);
    procedure AssignParamsFromCommand(Parameters: TADParams; Command: ICommand); virtual;
    function NewGeneratedKeyResponse(SP: TADStoredProc): ICommandResponseUnion;
  public
    constructor Create(APIDM: TAPIDM; CmdName: string; CmdDMClass: TCommandDMClass; CmdValidator: ICommandValidator=nil); reintroduce; overload; virtual;
    function Execute(Command: ICommand): ICommandResponseUnion; override;
  end;

implementation

uses
  CommandUtils, QMThriftUtils, OdDbUtils, OdIsoDates, EventSourceDMu;

{ TODBCCommandHandler }

constructor TODBCCommandHandler.Create(APIDM: TAPIDM; CmdName: string; CmdDMClass: TCommandDMClass; CmdValidator: ICommandValidator);
begin
  inherited Create(CmdName, CmdValidator);
  DMClass := CmdDMClass;
  MainDM := APIDM;
end;

function TODBCCommandHandler.Execute(Command: ICommand): ICommandResponseUnion;
begin
  CmdDM := DMClass.Create(nil, MainDM);
  try
    Result := inherited Execute(Command);
  finally
    FreeAndNil(CmdDM);
  end;
end;

function TODBCCommandHandler.GetDeleteSQL(const TableName, IDColName: string; const ID: Integer): string;
const
  SQL = 'update %s set active = 0, modified_date = GetDate() where %s = %d';
begin
  // Some tables also have deleted_by and deleted_date columns to update.
  Result := Format(SQL, [TableName, IDColName, ID]);
end;

procedure TODBCCommandHandler.InactivateRow(const TableName, IDColName: string; const ID: Integer);
var
  RowCnt: Integer;
begin
  Assert(Assigned(CmdDM), 'CmdDM is undefined.');

  RowCnt := CmdDM.Conn.ExecSQL(GetDeleteSQL(TableName, IDColName, ID));
  if RowCnt > 1 then
    raise Exception.CreateFmt('Unable to inactivate %s with %s = %d. More than one row was found.',
      [TableName, IDColName, ID]);
  if RowCnt < 1 then
    raise Exception.CreateFmt('Unable to inactivate %s with %s = %d. No matching row was found.',
      [TableName, IDColName, ID])
end;

function TODBCCommandHandler.NewGeneratedKeyResponse(SP: TADStoredProc): ICommandResponseUnion;
begin
  if SP.Eof then
    raise Exception.Create('No GeneratedKey results returned from ' + SP.StoredProcName);

  Result := GeneratedKeyResponse(SP.FieldByName('table_name').AsString,
    SP.FieldByName('old_id').AsInteger, SP.FieldByName('new_id').AsInteger);
end;

procedure TODBCCommandHandler.AssignParamsFromCommand(Parameters: TADParams; Command: ICommand);
var
  CmdType: string;
  CmdBase: IBase;
  CmdObj: TInterfacedObject;
  FK: IForeignKey;
  Param: TADParam;
  I: Integer;
begin
  CmdBase := GetCommandType(Command, CmdType);
  AssignParamsFromThriftObj(Parameters, CmdBase);

  CmdObj := TInterfacedObject(CmdBase);
  // This code assumes ForeignKey is an IForeignKey implemented by a TForeignKeyImpl object.
  if CmdObj.HasProperty('ForeignKey') then
    FK := TForeignKeyImpl(CmdObj.GetPropValueAsIBase('ForeignKey'));

  // TODO: A good enhancement is to validate the ForeignKey.ID actually exists in the
  //       table its ForeignKey.ForeignType references. But that belongs in an ICommandValidator,
  //       not in an ICommandHandler.

  for I := 0 to Parameters.Count - 1 do begin
    Param := Parameters[I];
    // Set the params that are common to all commands
    if SameText(ColumnNameToThriftName(Param.Name), 'EmpID') then
      Param.Value := Command.EmpID;
    if SameText(ColumnNameToThriftName(Param.Name), 'CreateDateTime') then
      Param.Value := IsoStrToDateTime(Command.CreateDateTime);
    // Round milliseconds for date time values for SQL Server storage compatibility
    if (Param.DataType = ftDateTime) and (not Param.IsNull) then
      Param.AsDateTime := RoundSQLServerTimeMS(Param.AsDateTime);
    // Assigns ForeignKey parameters if the Command object contains a ForeignKey property.
    if SameText(ColumnNameToThriftName(Param.Name), 'ForeignType') and Assigned(FK) then
      Param.Value := FK.ForeignType;
    if SameText(ColumnNameToThriftName(Param.Name), 'ForeignID') and Assigned(FK) then
      Param.Value := FK.ID;
  end;
end;

end.

