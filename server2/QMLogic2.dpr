program QMLogic2;

uses
  Vcl.SvcMgr,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  QMAPIServiceU in 'QMAPIServiceU.pas' {QMLogicWebService: TService},
  ServerU in 'ServerU.pas' {ServerDM: TDataModule},
  APIWebModule in 'APIWebModule.pas' {APIWebMod: TWebModule},
  APIDMu in 'APIDMu.pas' {APIDM: TDataModule},
  TicketCommandDMu in 'TicketCommandDMu.pas' {TicketCmdDM: TDataModule},
  TicketQueryDMu in 'TicketQueryDMu.pas' {TicketQryDM: TDataModule},
  BaseQueryDMu in 'BaseQueryDMu.pas' {BaseQueryDM: TDataModule},
  UQDbConfig in '..\common\UQDbConfig.pas',
  QMLogic2ServiceLib in 'gen-delphi\QMLogic2ServiceLib.pas',
  MSSqlEventSource in 'MSSqlEventSource.pas',
  BaseCommandDMu in 'BaseCommandDMu.pas' {BaseCommandDM: TDataModule},
  MetricsQueryDMu in 'MetricsQueryDMu.pas' {MetricsQryDM: TDataModule},
  InterfacedDataModuleU in '..\common\InterfacedDataModuleU.pas' {InterfacedDM: TDataModule},
  CommandUtils in 'CommandUtils.pas',
  EventSource in 'EventSource.pas',
  EventSourceDMu in 'EventSourceDMu.pas' {EventSourceDM: TDataModule},
  EventUtils in 'EventUtils.pas',
  NotesCommandDMu in 'NotesCommandDMu.pas' {NotesCommandDM: TDataModule},
  ODBCCmdDispatcher in 'ODBCCmdDispatcher.pas',
  ODBCCommandHandler in 'ODBCCommandHandler.pas',
  QMCommands in 'QMCommands.pas',
  QueryDatabase in 'QueryDatabase.pas' {QueryDM: TDataModule},
  SyncDMu in 'SyncDMu.pas' {SyncDM: TDataModule},
  ThreadSafeLoggerU in '..\common\ThreadSafeLoggerU.pas';

{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

begin
  // Windows 2003 Server requires StartServiceCtrlDispatcher to be
  // called before CoRegisterClassObject, which can be called indirectly
  // by Application.Initialize. TServiceApplication.DelayInitialize allows
  // Application.Initialize to be called from TService.Main (after
  // StartServiceCtrlDispatcher has been called).
  //
  // Delayed initialization of the Application object may affect
  // events which then occur prior to initialization, such as
  // TService.OnCreate. It is only recommended if the ServiceApplication
  // registers a class object with OLE and is intended for use with
  // Windows 2003 Server.
  //
  // Application.DelayInitialize := True;
  //
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  if not Application.DelayInitialize or Application.Installing then
    Application.Initialize;
  Application.CreateForm(TServerDM, ServerDM);
  QMLogicWebService := TQMLogicWebService.Create(Application, GetServiceName);
  Application.Run;
end.
