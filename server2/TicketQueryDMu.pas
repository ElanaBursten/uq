unit TicketQueryDMu;

interface

uses
  System.SysUtils, System.Classes, uADStanIntf, uADStanOption,
  uADStanParam, uADStanError, uADDatSManager, uADPhysIntf, uADDAptIntf,
  uADStanAsync, uADDAptManager, Data.DB, uADCompDataSet, uADCompClient,
  BaseQueryDMu, AdUtils, OdIsoDates, OdMiscUtils;

type
  TTicketQryDM = class(TBaseQueryDM)
    TicketSchedule: TADQuery;
  public
    procedure GetTicketDetail;
    procedure GetTicketSummary;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TTicketQryDM.GetTicketSummary;
begin
  TicketSchedule.Params.ParamValues['ticket_id'] := MainDM.Params[0];
  MainDM.RespondWithJsonFromDataSet(TicketSchedule);
end;

procedure TTicketQryDM.GetTicketDetail;
var
  Params: array[0..1] of TParamRec;
  Values: array[0..1] of Variant;
  QryFields: TStrings;
begin
  MainDM.SetOpName('GetTicketDetail');

  Params[0].Name := '@TicketID';
  Params[0].ParamDataType := ftInteger;
  Params[0].ParamType := ptInput;

  Params[1].Name := '@UpdatesSince';
  Params[1].ParamDataType := ftDateTime;
  Params[1].ParamType := ptInput;

  QryFields := MainDM.Request.QueryFields;
  Assert(QryFields.Count = 1, 'GetTicketDetails expected ''last_sync'' query field on the request');
  Values[0] := StrToIntDef(MainDM.Params[0], 0);
  Assert(Values[0] > 0, 'TicketID must be a number greater than 0.');
  try
    Values[1] := IsoStrToDateTime(QryFields.Values['last_sync']);
  except
    if QryFields.Values['last_sync'] <> '' then
      raise Exception.Create('The ''last_sync'' param should be empty or an ISO date time string.');
  end;

  MainDM.RespondWithJsonFromMultiParamSP('dbo.get_ticket5', Params, Values);
end;

end.
