unit QueryDatabase;

interface

uses
  System.SysUtils, System.Classes, uADStanIntf, uADStanOption, uADStanError,
  uADGUIxIntf, uADPhysIntf, uADStanDef, uADStanPool, uADStanAsync,
  uADPhysManager, Data.DB, uADCompClient, uADPhysODBCBase, uADPhysMSSQL,
  UQDbConfig;

type
  TQueryDM = class(TDataModule)
    Conn: TADConnection;
  public
    function DBName: string;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TQueryDM }

function TQueryDM.DBName: string;
var
  DbConfig: TODBCDatabaseConfig;
begin
  DbConfig := TODBCDatabaseConfig.Create;
  try
    DbConfig.Server := Conn.Params.Values['Server'];
    DbConfig.DB := Conn.Params.Values['Database'];
    DbConfig.Trusted := SameText(Conn.Params.Values['OSAuthent'], 'yes');
    DbConfig.Username := Conn.Params.Values['User_name'];
    Result := DbConfig.Description;
  finally
    FreeAndNil(DbConfig);
  end;
end;

end.
