unit QMCommands;

interface

uses
  SysUtils, Classes, QMApi, QMApiShared, QMApiCommand, Thrift,
  Thrift.Collections, Generics.Collections, Thrift.Protocol;

type
  // manages a list of commands
  ICommandBatchController = interface
    procedure Add(CommandName: string; Command: IBase; const EmpID: Integer; const CreateDateTime: TDateTime);
    function BatchCount: Integer;
    procedure SubmitBatch;
    function GetCommandResponses: IQMApiResponses;
  end;

  // transfers a command batch to a dispatcher
  ICommandSender = interface
    procedure Send(Commands: IQMApiCommands; var Responses: IQMApiResponses);
  end;

  // handles commands in a batch and returns a response for each
  ICommandDispatcher = interface
    function ProcessBatch(Commands: IQMApiCommands): IQMApiResponses;
    function RunCommand(Command: ICommand): ICommandResponseUnion;
  end;

  // validates a command
  ICommandValidator = interface
    function IsValid(Command: ICommand): Boolean;
    function ValidationErrors: IThriftList<IServerError>;
  end;

  // executes a valid command, creating events, and reporting the result
  ICommandHandler = interface
    function Validate(Command: ICommand): ICommandResponseUnion;
    function Execute(Command: ICommand): ICommandResponseUnion;
  end;

  TCommandBatchController = class(TInterfacedObject, ICommandBatchController)
  private
    Commands: IQMApiCommands;
    CommandSender: ICommandSender;
    CommandResponses: IQMApiResponses;
  public
    procedure Add(CommandName: string; Command: IBase; const EmpID: Integer; const CreateDateTime: TDateTime);
    function BatchCount: Integer;
    function GetCommandResponses: IQMApiResponses;
    procedure SubmitBatch;
    procedure AfterConstruction; override;
    constructor Create(CmdSender: ICommandSender);
  end;

  TBaseCommandHandler = class(TInterfacedObject, ICommandHandler)
  protected
    CommandName: string;
    CmdResponse: ICommandResponseUnion;
    Validator: ICommandValidator;
    function RunCommand(Cmd: ICommand): ICommandResponseUnion; virtual; abstract;
    function GeneratedKeyResponse(const Table: string; const TempID, NewID: Integer): ICommandResponseUnion;
  public
    constructor Create(CmdName: string; CmdValidator: ICommandValidator=nil); virtual;
    function Validate(Command: ICommand): ICommandResponseUnion; virtual;
    function Execute(Command: ICommand): ICommandResponseUnion; virtual;
  end;

  TBaseCommandValidator = class(TInterfacedObject, ICommandValidator)
  protected
    ErrorList: IThriftList<IServerError>;
    Cmd: ICommand;
    procedure AddValidationError(const Desc: string);
    function NewError(const Desc: string): IServerError;
    procedure ValidateCommand; virtual;
    procedure CheckEmpPermissions; virtual;
  public
    function IsValid(Command: ICommand): Boolean;
    function ValidationErrors: IThriftList<IServerError>;
  end;

  TBaseCommandDispatcher = class(TInterfacedObject, ICommandDispatcher)
  private
    Handlers: TObjectDictionary<string, ICommandHandler>;
    procedure SetCommandBatchSuccessFlag(Responses: IQMApiResponses);
    function RunCommand(Command: ICommand): ICommandResponseUnion;
    function ValidateCommands(Commands: IQMApiCommands; Responses: IQMApiResponses;
      const StopOnError: Boolean): Boolean;
    function GetHandler(Command: ICommand; var CmdType: string): ICommandHandler;
  protected
    CmdNum: Integer;
    CmdTotal: Integer;
    procedure RegisterHandlers; virtual; abstract;
    procedure RegisterHandler(const CommandName: string; const Handler: ICommandHandler);
    procedure AfterExecuteCommand(const CmdType: string; Command: ICommand); virtual;
    procedure BeforeExecuteCommand(const CmdType: string; Command: ICommand); virtual;
  public
    function ProcessBatch(Commands: IQMApiCommands): IQMApiResponses;
    constructor Create;
    destructor Destroy; override;
  end;

  // get all error messages as CRLF delimited strings.
  TQMApiResponsesHelper = class helper for TQMApiResponsesImpl
    function GetErrorStrings: string;
  end;

implementation

uses
  StrUtils, CommandUtils, QMThriftUtils, OdMiscUtils;

{ TQMApiResponsesHelper }
function TQMApiResponsesHelper.GetErrorStrings: string;

  function FlattenErrorList(Failures: ICommandFailureUnion): string;
  var
    I: Integer;
    ErrorList: IThriftList<IServerError>;
  begin
    Result := '';
    if Failures.__isset_ValidationErrors then
      ErrorList := Failures.ValidationErrors
    else if Failures.__isset_ServerErrors then
      ErrorList := Failures.ServerErrors
    else begin
      Result := 'unknown';
      Exit;
    end;
    for I := 0 to ErrorList.Count - 1 do
      Result := Result + IfThen(I > 0, sLineBreak) + ErrorList[I].Description;
  end;

var
  I: Integer;
  Resp: ICommandResponseUnion;
begin
  Result := '';
  if Success then
    Exit;

  for I := 0 to ResponseList.Count - 1 do begin
    Resp := ResponseList[I];
    if Resp.__isset_Failure then
      Result := Result + IfThen(I > 0, sLineBreak) + FlattenErrorList(Resp.Failure);
  end;
end;

{ TCommandBatchManager }

procedure TCommandBatchController.Add(CommandName: string; Command: IBase; const EmpID: Integer;
  const CreateDateTime: TDateTime);
var
  Cmd: ICommand;

  procedure AssignCommandToUnion;
  begin
    if CommandName = 'AddNote' then
      Cmd.Union.AddNote := IAddNoteCommand(Command)
    else if CommandName = 'DeleteNote' then
      Cmd.Union.DeleteNote := IDeleteNoteCommand(Command)

    // TODO: Add other command type assignments here. But....
    // It would be a lot less maintenance if this proc did not need to be a long list of ifs.

    else
      raise Exception.Create('Unknown command name ' + CommandName);
  end;

begin
  Cmd := NewCommand(EmpID, CreateDateTime);
  AssignCommandToUnion;
  Commands.CommandList.Add(Cmd);
end;

procedure TCommandBatchController.AfterConstruction;
begin
  inherited;
  // these are automatically destroyed when TCommandBatchManager is freed
  Commands := NewCommandList;
  CommandResponses := NewCommandResponseList;
end;

function TCommandBatchController.BatchCount: Integer;
begin
  Result := Commands.CommandList.Count;
end;

constructor TCommandBatchController.Create(CmdSender: ICommandSender);
begin
  inherited Create;
  Assert(Assigned(CmdSender), 'CmdSender is not assigned');
  CommandSender := CmdSender;
end;

function TCommandBatchController.GetCommandResponses: IQMApiResponses;
begin
  Result := CommandResponses;
end;

procedure TCommandBatchController.SubmitBatch;
begin
  CommandSender.Send(Commands, CommandResponses);
end;

{ TBaseCommandHandler }

constructor TBaseCommandHandler.Create(CmdName: string; CmdValidator: ICommandValidator);
begin
  inherited Create;
  CommandName := CmdName;
  Validator := CmdValidator;
  // Always need to check common command properties, so assign a simple one if none is assigned
  if not Assigned(Validator) then
    Validator := TBaseCommandValidator.Create;
end;

function TBaseCommandHandler.Validate(Command: ICommand): ICommandResponseUnion;
begin
  Result := NewCommandResponse;
  if Validator.IsValid(Command) then
    Result.Success := TCommandSuccessUnionImpl.Create
  else begin
    Result.Failure := TCommandFailureUnionImpl.Create;
    Result.Failure.ValidationErrors := Validator.ValidationErrors;
  end;
end;

function TBaseCommandHandler.Execute(Command: ICommand): ICommandResponseUnion;
var
  OkToRun: Boolean;
begin
  // recheck validity in case something changed since Validate was called.
  OkToRun := Validator.IsValid(Command);
  if OkToRun then
    Result := RunCommand(Command)
  else begin
    Result.Failure := TCommandFailureUnionImpl.Create;
    Result.Failure.ValidationErrors := Validator.ValidationErrors;
  end;
end;

function TBaseCommandHandler.GeneratedKeyResponse(const Table: string; const TempID, NewID: Integer): ICommandResponseUnion;
var
  GenKey: IGeneratedKey;
begin
  CmdResponse := NewCommandResponse;
  CmdResponse.Success := TCommandSuccessUnionImpl.Create;
  CmdResponse.Success.GeneratedKey := TGeneratedKeyImpl.Create;
  GenKey := CmdResponse.Success.GeneratedKey;
  GenKey.TableName := Table;
  GenKey.TempKeyValue := TempID;
  GenKey.KeyValue := NewID;
  Result := CmdResponse;
end;

{ TBaseCommandValidator }

procedure TBaseCommandValidator.CheckEmpPermissions;
begin
  // override in validators that require a permission check.
end;

function TBaseCommandValidator.IsValid(Command: ICommand): Boolean;
begin
  ErrorList := nil;
  ErrorList := TThriftListImpl<IServerError>.Create;
  Cmd := Command;
  ValidateCommand;
  Result := ErrorList.Count = 0;
end;

function TBaseCommandValidator.NewError(const Desc: string): IServerError;
begin
  Assert(NotEmpty(Desc), 'An error must have a description.');
  Result := TServerErrorImpl.Create;
  Result.Description := Desc;
end;

procedure TBaseCommandValidator.AddValidationError(const Desc: string);
begin
  ErrorList.Add(NewError(Desc));
end;

procedure TBaseCommandValidator.ValidateCommand;
begin
  if Cmd.EmpID < 1 then
    AddValidationError('All commands require a valid EmpID');
  if Cmd.CreateDateTime = '' then
    AddValidationError('All commands require a CreateDateTime');

  CheckEmpPermissions;
end;

function TBaseCommandValidator.ValidationErrors: IThriftList<IServerError>;
begin
  Result := ErrorList;
end;

constructor TBaseCommandDispatcher.Create;
begin
  inherited Create;
  Handlers := TObjectDictionary<string, ICommandHandler>.Create;
  RegisterHandlers;
end;

procedure TBaseCommandDispatcher.SetCommandBatchSuccessFlag(Responses: IQMApiResponses);
var
  Resp: ICommandResponseUnion;
begin
  Responses.Success := Responses.ResponseList.Count > 0;
  // If any command fails, the entire batch fails.
  for Resp in Responses.ResponseList do
    Responses.Success := Responses.Success and Resp.__isset_Success;
end;

function TBaseCommandDispatcher.ValidateCommands(Commands: IQMApiCommands; Responses: IQMApiResponses;
  const StopOnError: Boolean): Boolean;
var
  I: Integer;
  CmdType: string;
  Command: ICommand;
  Resp: ICommandResponseUnion;
  Handler: ICommandHandler;
begin
  Assert(Assigned(Commands));
  Assert(Assigned(Responses));
  for I := 0 to Commands.CommandList.Count - 1 do begin
    CmdNum := I + 1;
    Command := Commands.CommandList[I];
    Handler := GetHandler(Command, CmdType);
    Resp := Handler.Validate(Command);
    Responses.ResponseList.Add(Resp);
    if StopOnError and Resp.__isset_Failure then
      Break;
  end;
  SetCommandBatchSuccessFlag(Responses);
  Result := Responses.Success;
end;

function TBaseCommandDispatcher.ProcessBatch(Commands: IQMApiCommands): IQMApiResponses;
var
  I: Integer;
  Resp: ICommandResponseUnion;
begin
  Assert(Assigned(Commands));
  Result := NewCommandResponseList;
  CmdTotal := Commands.CommandList.Count;
  if not ValidateCommands(Commands, Result, False) then
    Exit;

  Assert(Commands.CommandList.Count = Result.ResponseList.Count,
    'Command and Response lists are different sizes.');
  for I := 0 to Commands.CommandList.Count - 1 do begin
    CmdNum := I + 1;
    Resp := RunCommand(Commands.CommandList[I]);
    // Replace the command's initial response created by ValidateCommands with the final response.
    Result.ResponseList.Items[I] := Resp;
    if Resp.__isset_Failure then
      Break;
  end;
  SetCommandBatchSuccessFlag(Result);
end;

procedure TBaseCommandDispatcher.RegisterHandler(const CommandName: string;
  const Handler: ICommandHandler);
begin
  Handlers.Add(CommandName, Handler);
end;

destructor TBaseCommandDispatcher.Destroy;
begin
  FreeAndNil(Handlers);

  inherited;
end;

function TBaseCommandDispatcher.GetHandler(Command: ICommand; var CmdType: string): ICommandHandler;
begin
  Result := nil;
  GetCommandType(Command, CmdType);
  Handlers.TryGetValue(CmdType, Result);
end;

function TBaseCommandDispatcher.RunCommand(Command: ICommand): ICommandResponseUnion;
var
  CmdType: string;
  Handler: ICommandHandler;
begin
  Result := NewCommandResponse;
  Handler := GetHandler(Command, CmdType);
  if Assigned(Handler) then begin
    BeforeExecuteCommand(CmdType, Command);
    Result := Handler.Execute(Command);
    AfterExecuteCommand(CmdType, Command);
  end
  else // unhandled command
    Result := NewUnhandledCommandError(CmdType);
end;

procedure TBaseCommandDispatcher.BeforeExecuteCommand(const CmdType: string; Command: ICommand);
begin
  // Child classes can do something extra before each command in a batch is executed.
end;

procedure TBaseCommandDispatcher.AfterExecuteCommand(const CmdType: string; Command: ICommand);
begin
  // Child classes can do something extra after each command in a batch is executed.
end;

end.
