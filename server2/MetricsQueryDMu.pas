unit MetricsQueryDMu;

interface

uses
  System.SysUtils, System.Classes, Data.DB,
  uADStanIntf, uADStanOption, uADStanError, uADGUIxIntf, uADPhysIntf,
  uADStanDef, uADStanPool, uADStanAsync, uADPhysManager, uADStanParam,
  uADDatSManager, uADDAptIntf, uADDAptManager, uADCompClient, uADCompDataSet,
  uADPhysODBCBase, uADPhysMSSQL, BaseQueryDMu, QMConst, OdExceptions,
  OdMiscUtils;

type
  TMetricsQryDM = class(TBaseQueryDM)
    MetricChoicesData: TADQuery;
    GetTimeSeriesData: TADStoredProc;
    HierarchyData: TADQuery;
    StatsForNodeProc: TADStoredProc;
    DetailData: TADMemTable;
    AncestorData: TADQuery;
  private
    function TimeSeriesDataToJSon: string;
    procedure GetAccessibleNodes(var AllowedEmpIDs: TIntegerArray);
  published
    procedure GetMetricChoices;
    procedure GetMetric;
    procedure GetHierarchy;
    procedure GetStatsForNode;
    procedure GetDefaultStatsForNode;
  end;

implementation

uses
  SuperObject, Math, AdUtils, SqlJsonOutput;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

const
  RootNodeID = 2676;

procedure TMetricsQryDM.GetMetricChoices;
begin
  MainDM.RespondWithJsonFromDataSet(MetricChoicesData);
end;

function TMetricsQryDM.TimeSeriesDataToJSon: string;
var
  D: TADStoredProc;
  Granularity: string;
  WriteCount: Integer;
  MetricType: string;
  SB: TStringBuilder;
  ValueField: TField;
  EntityDescription: string;
begin
  SetPhase('TimeSeriesDataToJSon');
  D := GetTimeSeriesData;
  if D.EOF then begin
    Result := '{"Data": "No results found"}';
    Exit;
  end;

  MetricType := D.FieldByName('metric_type').AsString;
  Granularity := D.FieldByName('gran_code').AsString;

  EntityDescription := D.FieldByName('ent_name').AsString;
  if D.FieldByName('ent_cat_code').AsString = 'CC' then
    EntityDescription := D.FieldByName('ent_code').AsString + ' ' + EntityDescription;

  // SuperObject currently has no way to add an array, and is likely
  // quite inefficient for constructing something big, so a
  // TStringBuilder will do.
  SB := TStringBuilder.Create(1024 * 1024);
  try
    SB.Append('{ "Category": "' + D.FieldByName('ent_cat_code').AsString + '",'#10'');
    SB.Append('  "Entity": "' + EntityDescription + '",'#10'');
    SB.Append('  "Metric": "' + D.FieldByName('metric_name').AsString + '",'#10'');
    SB.Append('  "MetricType": "' + MetricType + '",'#10'');
    SB.Append('  "Granularity": "' + D.FieldByName('gran_name').AsString + '",'#10'');
    SB.Append('  "DateStart": "' + D.FieldByName('date_start').AsString + '",'#10'');
    SB.Append('  "DateEnd": "' + D.FieldByName('date_end').AsString + '",'#10'');

    LoadNextRecordset(D, DetailData);
    WriteCount := 0;
    ValueField := DetailData.Fields[1];
    SB.Append('  "Data": ['#10'');
    while not DetailData.Eof do begin
      SB.Append(ValueField.AsInteger);
      SB.Append(',');
      Inc(WriteCount);
      if (WriteCount mod 50) = 0 then
        SB.Append(''#10'');
      DetailData.Next;
    end;

    // remove ending comma or newline
    while CharInSet(SB.Chars[SB.Length-1], [#10, ',']) do
      SB.Remove(SB.Length - 1, 1);

    SB.Append(']}');
    Result := SB.ToString;
  finally
    FreeAndNil(SB);
  end;
end;

procedure TMetricsQryDM.GetHierarchy;
begin
  MainDM.SetOpName('GetHierarchy');
  MainDM.RespondWithJsonFromDataSet(HierarchyData);
end;

procedure TMetricsQryDM.GetMetric;
begin
  MainDM.SetOpName('GetMetric');
  GetTimeSeriesData.Close;
  GetTimeSeriesData.Params.ParamValues['@total'] := SameText(MainDM.Params[0], 'total');
  GetTimeSeriesData.Params.ParamValues['@ent_cat_code'] := MainDM.Params[1];
  GetTimeSeriesData.Params.ParamValues['@ent_code'] := MainDM.Params[2];
  GetTimeSeriesData.Params.ParamValues['@metric_code'] := MainDM.Params[3];
  GetTimeSeriesData.Params.ParamValues['@gran_code'] := MainDM.Params[4];
  GetTimeSeriesData.Params.ParamValues['@date_start'] := MainDM.Params[5];
  try
    GetTimeSeriesData.Open;
    MainDM.Response.Content := TimeSeriesDataToJSon;
    MainDM.Response.ContentType := 'application/json';
  except
    on E: EMSSQLNativeException do begin
      if E.ErrorCode = 50001 then begin
        MainDM.Response.StatusCode := 404;
        MainDM.Response.Content := E.Message;
      end
      else
        raise;
    end;
  end;
end;

procedure TMetricsQryDM.GetAccessibleNodes(var AllowedEmpIDs: TIntegerArray);
const
  SQL = 'select node_id from dbo.get_emp_allowed_nodes(%d) order by distance';
begin
  SetLength(AllowedEmpIDs, 0);
  AncestorData.Close;
  AncestorData.Open(Format(SQL, [MainDM.GetLoggedInEmpID(True)]));
  while not AncestorData.Eof do begin
    AddIntToArray(AncestorData.Fields[0].Value, AllowedEmpIDs);
    AncestorData.Next;
  end;
  if Length(AllowedEmpIDs) = 0 then
    raise EOdNotAllowed.Create('You do not have access to any employee nodes.');
end;

procedure TMetricsQryDM.GetDefaultStatsForNode;
var
  NewURL: AnsiString;
begin
  MainDM.SetOpName('GetDefaultStatsForNode');
  NewURL := AnsiString(QMLogic2BaseURL + '/v1/nodestat/' + IntToStr(RootNodeID));
  MainDM.Response.SendRedirect(NewURL);
end;

procedure TMetricsQryDM.GetStatsForNode;
var
  Stats: ISuperObject;
  AncestorSO: ISuperObject;
  AllowedEmpIDs: TIntegerArray;
  NodeID: Integer;
begin
  MainDM.SetOpName('GetStatsForNode');
  Op.Phase := 'GetWorkManagementLimit';
  GetAccessibleNodes(AllowedEmpIDs);
  NodeID := StrToInt(MainDM.Params[0]);
  if NodeID = RootNodeID then
    NodeID := AllowedEmpIDs[0];
  if not IntegerInArray(NodeID, AllowedEmpIDs) then
    raise EOdNotAllowed.Create('You do not have permission to access the requested node.');

  Op.Phase := 'Exec stats_for_node sp';
  Op.Detail := 'Setting parameters';
  StatsForNodeProc.Params.ParamValues['@node_id'] := NodeID;
  StatsForNodeProc.Params.ParamValues['@viewer_id'] := MainDM.GetLoggedInEmpID(True);

  // TODO: The web client does not send an @as_of_dt param yet.
  StatsForNodeProc.Params.ParamValues['@as_of_dt'] := Now;
  if MainDM.Params.Count > 1 then
    StatsForNodeProc.Params.ParamValues['@as_of_dt'] := MainDM.Params[1];

  Op.Detail := 'Opening stored procedure';
  StatsForNodeProc.Open;
  Op.Phase := 'Formatting results';
  try
    Op.Detail := 'Adding node stats JSON';
    Stats := CreatePathedJsonFromDataSet(StatsForNodeProc);
    Op.Detail := 'Adding ancestors';
    LoadNextRecordset(StatsForNodeProc, DetailData);
    Stats.O['node.ancestors'] := SA([]);
    while not DetailData.Eof do begin
      AncestorSO := SO;
      AncestorSO.I['id'] := DetailData.FieldByName('emp_id').AsInteger;
      AncestorSO.S['name'] := DetailData.FieldByName('short_name').AsString;
      Stats.A['node.ancestors'].Add(AncestorSO);
      DetailData.Next;
    end;
    Op.Phase := 'Setting response content';
    MainDM.Response.Content := Stats.AsJson(True, True);
  finally
    StatsForNodeProc.Close;
    DetailData.Close;
  end;
end;

end.
