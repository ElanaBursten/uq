program QMLogic2DevServer;
{$APPTYPE GUI}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  APIDevFormU in 'APIDevFormU.pas' {APIDevForm},
  APIWebModule in 'APIWebModule.pas' {APIWebMod: TWebModule},
  APIDMu in 'APIDMu.pas' {APIDM: TDataModule},
  ServerU in 'ServerU.pas' {ServerDM: TDataModule},
  BaseQueryDMu in 'BaseQueryDMu.pas' {BaseQueryDM: TDataModule},
  TicketCommandDMu in 'TicketCommandDMu.pas' {TicketCmdDM: TDataModule},
  TicketQueryDMu in 'TicketQueryDMu.pas' {TicketQryDM: TDataModule},
  EventSourceDMu in 'EventSourceDMu.pas' {EventSourceDM: TDataModule},
  QMLogic2ServiceLib in 'gen-delphi\QMLogic2ServiceLib.pas',
  MSSqlEventSource in 'MSSqlEventSource.pas',
  QMThriftUtils in '..\common\QMThriftUtils.pas',
  OperationTracking in '..\server\OperationTracking.pas',
  MetricsQueryDMu in 'MetricsQueryDMu.pas' {MetricsQryDM: TDataModule},
  BaseCommandDMu in 'BaseCommandDMu.pas' {BaseCommandDM: TDataModule},
  QueryDatabase in 'QueryDatabase.pas' {QueryDM: TDataModule},
  SyncDMu in 'SyncDMu.pas' {SyncDM: TDataModule},
  InterfacedDataModuleU in '..\common\InterfacedDataModuleU.pas' {InterfacedDM: TDataModule},
  EventUtils in 'EventUtils.pas',
  NotesCommandDMu in 'NotesCommandDMu.pas' {NotesCommandDM: TDataModule},
  ODBCCommandHandler in 'ODBCCommandHandler.pas',
  ODBCCmdDispatcher in 'ODBCCmdDispatcher.pas',
  CommandUtils in 'CommandUtils.pas',
  QMCommands in 'QMCommands.pas';

{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TServerDM, ServerDM);
  Application.CreateForm(TAPIDevForm, APIDevForm);
  Application.Run;
end.
