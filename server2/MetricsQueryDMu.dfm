inherited MetricsQryDM: TMetricsQryDM
  OldCreateOrder = True
  Height = 408
  Width = 540
  object MetricChoicesData: TADQuery
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    SQL.Strings = (
      'select * from metric_choice')
    Left = 40
    Top = 96
  end
  object GetTimeSeriesData: TADStoredProc
    FetchOptions.AssignedValues = [evMode, evAutoClose, evUnidirectional, evCursorKind]
    FetchOptions.Mode = fmAll
    FetchOptions.CursorKind = ckForwardOnly
    FetchOptions.Unidirectional = True
    FetchOptions.AutoClose = False
    StoredProcName = 'get_time_series'
    Left = 48
    Top = 160
    ParamData = <
      item
        Position = 1
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        ParamType = ptResult
      end
      item
        Position = 2
        Name = '@ent_cat_code'
        DataType = ftString
        ParamType = ptInput
        Size = 30
      end
      item
        Position = 3
        Name = '@ent_code'
        DataType = ftString
        ParamType = ptInput
        Size = 50
      end
      item
        Position = 4
        Name = '@metric_code'
        DataType = ftString
        ParamType = ptInput
        Size = 10
      end
      item
        Position = 5
        Name = '@gran_code'
        DataType = ftString
        ParamType = ptInput
        Size = 10
      end
      item
        Position = 6
        Name = '@date_start'
        DataType = ftDateTime
        ParamType = ptInput
      end
      item
        Position = 7
        Name = '@total'
        DataType = ftBoolean
        ParamType = ptInput
      end>
  end
  object HierarchyData: TADQuery
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    SQL.Strings = (
      'select * from hierarchy order by 4')
    Left = 280
    Top = 16
  end
  object StatsForNodeProc: TADStoredProc
    FetchOptions.AssignedValues = [evMode, evAutoClose, evUnidirectional, evCursorKind]
    FetchOptions.Mode = fmAll
    FetchOptions.CursorKind = ckForwardOnly
    FetchOptions.Unidirectional = True
    FetchOptions.AutoClose = False
    StoredProcName = 'stats_for_node'
    Left = 280
    Top = 72
    ParamData = <
      item
        Position = 1
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        ParamType = ptResult
      end
      item
        Name = '@node_id'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = '@viewer_id'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = '@as_of_dt'
        DataType = ftDateTime
        ParamType = ptInput
      end>
  end
  object DetailData: TADMemTable
    FetchOptions.AssignedValues = [evMode, evUnidirectional, evCursorKind, evAutoFetchAll]
    FetchOptions.Mode = fmAll
    FetchOptions.CursorKind = ckForwardOnly
    FetchOptions.Unidirectional = True
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 136
    Top = 160
  end
  object AncestorData: TADQuery
    FetchOptions.AssignedValues = [evMode, evAutoClose, evCursorKind, evAutoFetchAll]
    FetchOptions.Mode = fmAll
    FetchOptions.CursorKind = ckForwardOnly
    FetchOptions.AutoClose = False
    Left = 352
    Top = 16
  end
end
