inherited TicketCmdDM: TTicketCmdDM
  OldCreateOrder = True
  Height = 393
  Width = 549
  object UpdateAssignments: TADStoredProc
    StoredProcName = 'assign_locate'
    Left = 64
    Top = 32
    ParamData = <
      item
        Position = 1
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        ParamType = ptResult
      end
      item
        Position = 2
        Name = '@LocateID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = '@LocatorID'
        DataType = ftString
        ParamType = ptInput
        Size = 20
      end
      item
        Position = 4
        Name = '@AddedBy'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 5
        Name = '@WorkloadDate'
        DataType = ftDateTime
        ParamType = ptInput
      end>
  end
  object Locate: TADQuery
    SQL.Strings = (
      'select ticket_id, locate_id, closed, assigned_to, status'
      '  from locate where ticket_id = :ticket_id')
    Left = 64
    Top = 88
    ParamData = <
      item
        Name = 'ticket_id'
        ParamType = ptInput
      end>
  end
end
