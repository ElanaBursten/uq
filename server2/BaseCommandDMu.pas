unit BaseCommandDMu;

interface

uses
  SysUtils, Classes, APIDMu, uADCompClient, OperationTracking, EventSourceDMu, QMCommands;

type
  TBaseCommandDM = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  protected
    StoreEvents: Boolean;
    MainDM: TAPIDM;
    EventSourceDM: TEventSourceDM;
    function Op: TOpInProgress;
    procedure SetPhase(const Phase: string);
  public
    Conn: TADConnection;
    constructor Create(Owner: TComponent; DM: TAPIDM); reintroduce; overload; virtual;
  end;

  TCommandDMClass = class of TBaseCommandDM;

implementation

{$R *.dfm}

uses AdUtils;

constructor TBaseCommandDM.Create(Owner: TComponent; DM: TAPIDM);
begin
  inherited Create(Owner);
  MainDM := DM;
  Assert(Assigned(DM), 'No MainDM assigned');
  Assert(Assigned(MainDM.Op), 'SetName not called before creating op DataModule');
  Assert(Assigned(MainDM.EventDM), 'No Event Source assigned in command DataModule');

  Conn := MainDM.Conn;
  SetConnections(Conn, Self);

  EventSourceDM := DM.EventDM;
  StoreEvents := MainDM.IsEventStoreEnabled;
end;

procedure TBaseCommandDM.DataModuleCreate(Sender: TObject);
begin
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules + 1;
end;

procedure TBaseCommandDM.DataModuleDestroy(Sender: TObject);
begin
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules - 1;
end;

function TBaseCommandDM.Op: TOpInProgress;
begin
  Result := MainDM.Op;
end;

procedure TBaseCommandDM.SetPhase(const Phase: string);
begin
  Assert(Assigned(MainDM.Op), 'No operation assigned');
  Op.Phase := Phase;
end;

end.
