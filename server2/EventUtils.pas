unit EventUtils;

interface

uses SysUtils, System.Rtti, System.TypInfo, Thrift.Protocol, QMThriftUtils,
  QMLogic2ServiceLib;

type
  // get a readable IAggrKey description
  TAggrKeyHelper = class helper for TAggrKeyImpl
    function AsString: string;
  end;

function GetEventType(Event: IEvent; out EventType: string): IBase;

implementation

{ TAggrKeyHelper }
function TAggrKeyHelper.AsString: string;
const
  AggrPrefixes: array[1..3] of string = ('T','D','W'); // (Ticket, Damage, Work Order)
var
  AType: Integer;
begin
  AType := Integer(AggrType);
  Assert((AType >= Low(AggrPrefixes)) and (AType <= High(AggrPrefixes)), 'Aggregate Type is out of expected range.');
  Result := Format('%s-%d', [AggrPrefixes[AType], AggrID]);
end;

function GetEventType(Event: IEvent; out EventType: string): IBase;
var
  CTX: TRttiContext;
  RT: TRttiType;
  PropArray: TArray<TRttiProperty>;
  Prop: TRttiProperty;
  IsSet: Boolean;
begin
  Result := nil;
  RT := CTX.GetType(TEventUnionImpl.ClassInfo);
  PropArray := RT.GetProperties;
  for Prop in PropArray do begin
    if IsValueProperty(Prop, TEventUnionImpl) then begin
      IsSet := TEventUnionImpl(Event.Union).GetPropValue(EVENT_IS_SET + Prop.Name);
      if IsSet then begin
        EventType := Prop.Name;
        Result := TEventUnionImpl(Event.Union).GetPropValueAsIBase(EventType);
        Break;
      end;
    end;
  end;
end;

end.
