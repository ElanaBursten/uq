unit APIWebModule;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses System.SysUtils, System.Classes, Web.HTTPApp, Wessy, Sessy;

type
  TAPIWebMod = class(TWebModule)
    procedure WebModule1DefaultHandlerAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModuleCreate(Sender: TObject);
  private
    FRouter: TWebRouter;
  end;

var
  WebModuleClass: TComponentClass = TAPIWebMod;

implementation

uses
  APIDMu, ServerU, OdMiscUtils, QMConst;

{$R *.dfm}

procedure TAPIWebMod.WebModule1DefaultHandlerAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  {Assume things will work okay}
  Response.StatusCode := 200;
  FRouter.Route(Request, Response, Handled);
  if not Handled then begin
    Response.StatusCode := 404;
    Response.Content := 'Unhandled request';
  end;
end;

procedure TAPIWebMod.WebModuleCreate(Sender: TObject);
begin
  FRouter := TWebRouter.Create(Self);
  FRouter.AddAppRoute('/');
  {TODO: These AppRoutes should be reviewed}

  FRouter.AddAppRoute('/allocation');
  FRouter.AddAppRoute('/approval');
  FRouter.AddAppRoute('/areaeditor');
  FRouter.AddAppRoute('/assets');
  FRouter.AddAppRoute('/billing');
  FRouter.AddAppRoute('/damage');
  FRouter.AddAppRoute('/findticket');
  FRouter.AddAppRoute('/findwo');
  FRouter.AddAppRoute('/help');
  FRouter.AddAppRoute('/manage');
  FRouter.AddAppRoute('/managedash');
  FRouter.AddAppRoute('/messages');
  FRouter.AddAppRoute('/mystats');
  FRouter.AddAppRoute('/mywork');
  FRouter.AddAppRoute('/newticket');
  FRouter.AddAppRoute('/oldticket');
  FRouter.AddAppRoute('/options');
  FRouter.AddAppRoute('/reports');
  FRouter.AddAppRoute('/settings');
  FRouter.AddAppRoute('/sync');
  FRouter.AddAppRoute('/ticketdetail');
  FRouter.UseDefaultLoggedInRoute('/managedash');
  FRouter.ServeFiles(ServerDM.DevWWWRoot);
  FRouter.UseDataModuleMaker(function: IWebDataModule begin Result := TAPIDM.Create(nil) end);
  FRouter.UseSessionManager(TSessy.Create('',  // This domain
     '/', // the application lives at the root of the URL space
     'QMANSESS', // A cookie name specific to this application
     '44152D00-961B-490A-8C7D-DE812C9782D3}', // secret key, DIFFERENT for each app
     30*60,  // 30 minute session expiration
     Self));

  // TODO: This ReassignTickets handler will go away once the operation is converted
  // to a Thrift command structure (likely not until QManager gets upgraded to XE3).
  FRouter.AddHandler(mtPost, QMLogic2BaseURL + ReassignTicketURL, 'ReassignTickets');

  FRouter.AddHandler(mtGet,  QMLogic2BaseURL + '/v1/ticket/summary/(\d+)', 'GetTicketSummary');
  FRouter.AddHandler(mtGet,  QMLogic2BaseURL + '/v1/ticket/(\d+)',  'GetTicketDetail');

  // Metric features
  FRouter.AddHandler(mtGet, QMLogic2BaseURL + '/v1/metric', 'GetMetricChoices');
  // /metric/single|total/EntCategory/EntCode/MetricCode/GranularityCode/Date/
  FRouter.AddHandler(mtGet, QMLogic2BaseURL + '/v1/metric/(single|total)/(\w+\s?\w+)/(\w+)/(\w+)/(\w+)/(.+)', 'GetMetric');

  // Hierarchy and Workload /api
  FRouter.AddHandler(mtGet, QMLogic2BaseURL + '/v1/hierarchy', 'GetHierarchy');
  FRouter.AddHandler(mtGet, QMLogic2BaseURL + '/v1/nodestat/(\d+)', 'GetStatsForNode');
  FRouter.AddHandler(mtGet, QMLogic2BaseURL + '/v1/nodestat[/]?', 'GetDefaultStatsForNode');

  // Log client errors
  FRouter.AddHandler(mtPost, QMLogic2BaseURL + '/v1/error', 'StoreClientError');
  // Sync
  FRouter.AddHandler(mtGet, QMLogic2BaseURL + '/v1/sync/(\d+)', 'Sync');

  // Monitor server operations
  FRouter.AddHandler(mtGet, '/monitor/cpu.bmp', 'GetCpuGraph', False);
  FRouter.AddHandler(mtGet, '/monitor/(\w+)', 'MonitorServer', False);

  // Command handler
  FRouter.AddHandler(mtPost, QMLogic2BaseURL + '/v1/command', 'ExecCommandBatch');
end;

end.
