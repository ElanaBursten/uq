unit QMAPIServiceU;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs, WinSvc;

type
  TQMLogicWebService = class(TService)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceAfterInstall(Sender: TService);
  public
    constructor Create(Owner: TComponent; const ServiceName: string); reintroduce;
    function GetServiceController: TServiceController; override;
  end;

type
  ServiceDescriptionRec = packed record
    lpDescription: PChar;
  end;
  PServiceDescriptionRec = ^ServiceDescriptionRec;

const
  SERVICE_CONFIG_DESCRIPTION = 1;

function GetServiceName: string;
function ChangeServiceConfig2(hService: SC_HANDLE; dwInfoLevel: DWORD; lpInfo: Pointer): BOOL; stdcall; external 'advapi32.dll' name 'ChangeServiceConfig2W';

var
  QMLogicWebService: TQMLogicWebService;

implementation

{$R *.DFM}

uses
  ServerU, ComObj;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  QMLogicWebService.Controller(CtrlCode);
end;

function GetServiceName: string;
var
  ServiceName: string;
begin
  // Return value of "/name:" command line switch; fail if missing or blank.
  if not FindCmdLineSwitch('name', ServiceName, True, [clstValueAppended]) then
    ServiceName := '';
  if ServiceName = '' then
    raise Exception.Create('Missing required /name: switch. Each instance of ' +
      'the service needs a unique name.');
  Result := ServiceName;
end;

constructor TQMLogicWebService.Create(Owner: TComponent; const ServiceName: string);
begin
  inherited Create(Owner);

  Name := ServiceName;
  DisplayName := 'Q Manager Logic Service [' + ServiceName + ']';
end;

function TQMLogicWebService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TQMLogicWebService.ServiceAfterInstall(Sender: TService);
var
  SvcMgr, Svc: SC_HANDLE;
  Desc: ServiceDescriptionRec;
begin
  // Update the service's Registry settings to add the /name switch to the start
  // command and include the name in its description.
  SvcMgr := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SvcMgr = 0 then
    raise Exception.Create('Unable to access Windows Service Manager.');

  try
    Svc := OpenService(SvcMgr, PChar(Name), SERVICE_ALL_ACCESS);
    if Svc = 0 then
      RaiseLastOSError;
    try
      Desc.lpDescription := PWideChar(ParamStr(0) + ' /name:' + Name);
      if not ChangeServiceConfig(Svc, SERVICE_NO_CHANGE, SERVICE_NO_CHANGE,
        SERVICE_NO_CHANGE, Desc.lpDescription, nil, nil, nil, nil, nil, nil) then
        raise Exception.CreateFmt('Unable to set the service BinaryPathName to %s.',
          [Desc.lpDescription]);
      Desc.lpDescription := PWideChar('Q Manager Logic Service [' + Name + ']');
      if not ChangeServiceConfig2(Svc, SERVICE_CONFIG_DESCRIPTION,
        @Desc.lpDescription) then
        raise Exception.CreateFmt('Unable to set the service Description to %s.',
          [Desc.lpDescription]);
    finally
      CloseServiceHandle(Svc);
    end;
  finally
    CloseServiceHandle(SvcMgr);
  end;
end;

procedure TQMLogicWebService.ServiceStart(Sender: TService; var Started: Boolean);
begin
  ServerDM.ConfigureFromIni;
  ServerDM.Start;
end;

procedure TQMLogicWebService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  ServerDM.Stop;
end;

end.
