unit BaseQueryDMu;

interface

uses
  SysUtils, Classes, APIDMu, uADCompClient, OperationTracking, QueryDatabase;

type
  TBaseQueryDM = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
  protected
    MainDM: TAPIDM;
    QueryDM: TQueryDM;
    function Op: TOpInProgress;
    procedure SetPhase(const Phase: string);
{ Not sure if we'll use these or not, but keep them here for now.
    function ForceMainDBSearch(const FeatureName: string): Boolean; virtual;
    function SearchIsDisabled(const FeatureName: string): Boolean; virtual;
}
  public
    constructor Create(Owner: TComponent; DM: TAPIDM; QryDB: TADConnection=nil); reintroduce; overload; virtual;
  end;

implementation

{$R *.dfm}

uses AdUtils, IniFiles;

constructor TBaseQueryDM.Create(Owner: TComponent; DM: TAPIDM; QryDB: TADConnection);
begin
  inherited Create(Owner);

  Assert(Assigned(DM), 'No MainDM assigned');
  Assert(Assigned(DM.Op), 'SetName not called before creating op DataModule');
  MainDM := DM;
  if Assigned(QryDB) then
    SetConnections(QryDB, Self)
  else
    SetConnections(DM.Conn, Self);
end;

procedure TBaseQueryDM.DataModuleCreate(Sender: TObject);
begin
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules + 1;
end;

procedure TBaseQueryDM.DataModuleDestroy(Sender: TObject);
begin
  GetOperationTracker.NumDataModules := GetOperationTracker.NumDataModules - 1;
end;

function TBaseQueryDM.Op: TOpInProgress;
begin
  Result := MainDM.Op;
end;

procedure TBaseQueryDM.SetPhase(const Phase: string);
begin
  Assert(Assigned(MainDM.Op), 'No operation assigned');
  Op.Phase := Phase;
end;

{
function TBaseQueryDM.ForceMainDBSearch(const FeatureName: string): Boolean;
begin
  Assert(FeatureName <>  '', 'Missing FeatureName');
  Result := MainDM.GetIniBoolSetting('Features', FeatureName+'SearchMainDB', False);
end;

function TBaseQueryDM.SearchIsDisabled(const FeatureName: string): Boolean;
begin
  Assert(FeatureName <>  '', 'Missing FeatureName');
  Result := MainDM.GetIniBoolSetting('Features', 'Disable'+FeatureName+'Search', False);
end;
}
end.
