unit ServerU;

interface

uses
  System.SysUtils, System.Classes, Winapi.Messages,
  IdHTTPWebBrokerBridge, Web.HTTPApp, ThreadSafeLoggerU, uADStanIntf,
  uADStanOption, uADStanError, uADGUIxIntf, uADPhysIntf, uADStanDef,
  uADPhysManager, uADCompClient;

type
  TServerDM = class;

  TServerLogNotifyEvent = procedure (Sender: TServerDM; const ALogMessage: string) of object;

  TServerDM = class(TDataModule)
    ADManager: TADManager;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FServer: TIdHTTPWebBrokerBridge;
    FOnLogMessage: TServerLogNotifyEvent;
    FLogDir: string;
    FLogEnabled: Boolean;
    FLogger: TThreadSafeLogger;
    procedure CloseAllConnections;
  public
    InstanceID: Integer;
    Port: Integer;
    DevWWWRoot: string;
    StartupFailureMessage: string;
    procedure Start;
    procedure Stop;
    function Active: Boolean;
    procedure ConfigureFromIni;
    procedure AddLogMessage(const AIsSuccess: Boolean; const AMessage: string);
    property OnLogMessage: TServerLogNotifyEvent read FOnLogMessage write FOnLogMessage;
  end;

var
  ServerDM: TServerDM;

implementation

uses Windows, OdMiscUtils, StrUtils, IniFiles, OperationTracking;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

function TServerDM.Active: Boolean;
begin
  Result := FServer.Active;
end;

procedure TServerDM.ConfigureFromIni;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(ExtractFilePath(GetModuleName(HInstance)) + 'QMServer.ini');
  try
    Port := Ini.ReadInteger('Server2', 'Port', 5041);
    DevWWWRoot := Ini.ReadString('Server2', 'DevWWWRoot', '.\www');
    FLogDir := AddSlash(GetLocalAppDataFolder) + 'Q Manager\logs\';
    FLogDir := AddSlash(Ini.ReadString('LogFile', 'Path', FLogDir));
    FLogEnabled := (Ini.ReadInteger('LogFile', 'LogicLogEnabled', 1) = 1);
  finally
    FreeAndNil(Ini);
  end;

  Randomize;
  InstanceID := Random(8999) + 1000;
  ForceDirectories(FLogDir);
  FLogger := TThreadSafeLogger.Create(FLogDir, 'QMLogic2.log', InstanceID);
  FLogger.Enabled := FLogEnabled;
  OperationTracking.RawLogger := FLogger;
end;

procedure TServerDM.DataModuleCreate(Sender: TObject);
begin
  FServer := TIdHTTPWebBrokerBridge.Create(Self);
  FOnLogMessage := nil;
end;

procedure TServerDM.DataModuleDestroy(Sender: TObject);
begin
  Stop;
  FreeAndNil(FLogger);
end;

procedure TServerDM.AddLogMessage(const AIsSuccess: Boolean; const AMessage: string);
var
  Mess: string;
begin
  Mess := IfThen(not AIsSuccess, '[Error] ', '') + AMessage;
  GetOperationTracker.Log(Mess);
  if Assigned(FOnLogMessage) then
    FOnLogMessage(Self, Mess);
end;

procedure TServerDM.Start;
begin
  AddLogMessage(True, 'Starting ' + ParamStr(0) + ' Build ' + AppVersionLong);
  try
    if not FServer.Active then begin
      FServer.Bindings.Clear;
      FServer.DefaultPort := Port;
      FServer.Active := True;
    end;
    AddLogMessage(True, 'Started on port ' + IntToStr(Port));
  except
    on E: Exception do begin
      AddLogMessage(False, 'Failed to start web service on port ' +
        IntToStr(Port) + '. ' + E.Message);
      StartupFailureMessage := E.Message;
      raise;
    end;
  end;
end;

procedure TServerDM.CloseAllConnections;
var
  I: Integer;
begin
  for I := 0 to ADManager.ConnectionCount-1 do
    ADManager.CloseConnectionDef(ADManager.Connections[I].ConnectionDefName);
end;

procedure TServerDM.Stop;
begin
  AddLogMessage(True, 'Stopping');
  try
    FServer.Active := False;
    FServer.Bindings.Clear;
    CloseAllConnections;
    AddLogMessage(True, 'Stopped');
  except
    on E: Exception do begin
      AddLogMessage(False, 'Failed to stop web service. ' + E.Message);
      raise;
    end;
  end;
end;

end.

