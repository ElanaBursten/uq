unit ODBCCmdDispatcher;

interface

uses
  System.SysUtils, System.Classes, APIDMu, QMCommands, QMApi, QMThriftUtils;

type
  ECommandDispatchError = class(Exception);

  TODBCCmdDispatcher = class(TBaseCommandDispatcher, ICommandDispatcher)
  private
    MainDM: TAPIDM;
  protected
    procedure RegisterHandlers; override;
    procedure AfterExecuteCommand(const CmdType: string; Command: ICommand); override;
    procedure BeforeExecuteCommand(const CmdType: string; Command: ICommand); override;
  public
    constructor Create(DM: TAPIDM);
    procedure Execute;
  end;

implementation

uses
  CommandUtils, NotesCommandDMu;

procedure TODBCCmdDispatcher.Execute;
var
  Commands: IQMApiCommands;
  Responses: IQMApiResponses;
begin
  Assert(Assigned(MainDM), 'MainDM is not assigned.');
  Assert(Assigned(MainDM.Op), 'No active Op');
  MainDM.SetOpName('ExecCommandBatch');
  Commands := NewCommandList;
  MainDM.Op.Phase := 'Deserialize command objects';
  try
    DeserializeFromJSON(MainDM.Request.Content, Commands);
  except
    on E: Exception do
      raise Exception.CreateFmt('Unable to deserialize command batch. Contents: %s %s Error: %s',
        [MainDM.Request.Content, sLineBreak, E.Message]);
  end;
  MainDM.Op.Phase := 'Process batch';
  Responses := ProcessBatch(Commands);
  MainDM.Response.ContentType := 'application/json';
  MainDM.Op.Phase := 'Serialize responses';
  try
    MainDM.Response.Content := SerializeToJSON(Responses);
  except
    on E: Exception do
      raise Exception.CreateFmt('Unable to serialize batch results. Contents: %s %s Error: %s',
        [Responses.ToString, sLineBreak, E.Message]);
  end;
  MainDM.Op.Phase := 'Check batch status';
  if not Responses.Success then begin
    MainDM.Response.StatusCode := 500;
    raise ECommandDispatchError.CreateFmt('Command batch execution errors detected: %s',
      [TQMApiResponsesImpl(Responses).GetErrorStrings]);
  end;
end;

procedure TODBCCmdDispatcher.AfterExecuteCommand(const CmdType: string; Command: ICommand);
begin
end;

procedure TODBCCmdDispatcher.BeforeExecuteCommand(const CmdType: string; Command: ICommand);
begin
  MainDM.Op.Detail := CmdType + ' ' + IntToStr(CmdNum) + ' of ' + IntToStr(CmdTotal) {+ Command.ToString};
end;

constructor TODBCCmdDispatcher.Create(DM: TAPIDM);
begin
  // Set MainDM before calling inherited Create so it is available to RegisterHandlers.
  MainDM := DM;
  inherited Create;
end;

procedure TODBCCmdDispatcher.RegisterHandlers;
begin
  // TODO: Come up with a more automated way of registering command handlers.
  RegisterHandler('AddNote', TAddNoteHandler.Create(MainDM, 'AddNote', TNotesCommandDM, TAddNoteValidator.Create));
  RegisterHandler('DeleteNote', TDeleteNoteHandler.Create(MainDM, 'DeleteNote', TNotesCommandDM));
end;

end.
